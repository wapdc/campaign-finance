<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="committee_contact")
 */
#[ORM\Entity]
#[ORM\Table(name: 'committee_contact')]
class CommitteeContact
{

  /** @Id @Column(type="integer") @GeneratedValue */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $contact_id;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $registration_id;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $committee_id;

  /** @Column */
  #[ORM\Column]
  public $role;

  /** @Column */
  #[ORM\Column]
  public $email;

  /** @Column */
  #[ORM\Column]
  public $name;

  /** @Column */
  #[ORM\Column]
  public $title;

  /** @Column(type="boolean") */
  #[ORM\Column(type: 'boolean')]
  public $treasurer;

  /** @Column(type="boolean")
   *    Person performs only ministerial functions
   */
  #[ORM\Column(type: 'boolean')]
  public $ministerial;

  /** @Column
   *    Street address
   */
  #[ORM\Column]
  public $address;

  /** @Column
   *    Suite, apartment, etc.
   */
  #[ORM\Column]
  public $premise;

  /** @Column
   *    Dependent locality - neighborhood, suburb
   */
  #[ORM\Column]
  public $city_area;

  /** @Column
   *    Locality - city, town, etc.
   */
  #[ORM\Column]
  public $city;

  /** @Column
   *    Administrative area - state, province, etc.
   */
  #[ORM\Column]
  public $state;

  /** @Column
   *    Postal code, zip code, etc.
   */
  #[ORM\Column]
  public $postcode;

  /** @Column
   *    Sorting code, used by some countries - https://www.grcdi.nl/gsb/world%20address%20formats.html
   */
  #[ORM\Column]
  public $sortcode;

  /** @Column
   *    Country code
   */
  #[ORM\Column]
  public $country;

  /** @Column
   *    Phone number
   */
  #[ORM\Column]
  public $phone;

}