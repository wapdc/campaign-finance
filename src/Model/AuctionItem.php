<?php


namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AuctionItem
 * @Entity
 * @Table(name="auction_item")
 */
#[ORM\Entity]
#[ORM\Table(name: 'auction_item')]
class AuctionItem {
  /** @Column(type="integer") @Id */
  #[ORM\Id]
  #[ORM\Column(type: 'integer')]
  public $donation_transaction_id;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $buy_transaction_id;

  /** @Column */
  #[ORM\Column]
  public $item_description;

  /** @Column(type="decimal") */
  #[ORM\Column(type: 'decimal')]
  public $fair_market_value;

  /** @Column(type="decimal") */
  #[ORM\Column(type: 'decimal')]
  public $sale_amount;
}