<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="committee")
 */
#[ORM\Entity]
#[ORM\Table(name: "committee")]
class Committee {

  /**
   * Committee constructor.
   * @param string $name
   *   Name of the committee
   */
  public function __construct($name, $committee_type=null) {
    $this->name = $name;
    $this->committee_type = $committee_type;
  }

  /**
   * @Column @Id @GeneratedValue
   * @var int
   */
  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column]
  public $committee_id;

  /**
   * @Column
   * @var string
   *   The name of the committee. Must be unique among currently-open committees.
   */
  #[ORM\Column]
  public $name;

  /**
   * @Column
   * @var string
   *   The name of the sponsor of the committee
   */
  #[ORM\Column]
  public $sponsor;

  /**
   * @Column
   * @var string
   *   Legacy filer type. Possible values: CA, CO
   */
  #[ORM\Column]
  public $committee_type;

  /**
   * @Column
   * @var string
   *   The fundamental committee type. Possible values: candidate, surplus, bonafide, caucus, pac
   */
  #[ORM\Column]
  public $pac_type;

  /**
   * @Column
   * @var string
   *   Bonafide party type. Possible values: state, county, district, caucus
   */
  #[ORM\Column]
  public $bonafide_type;

  /**
   * @Column(type="boolean")
   * @var boolean
   *   Bonafide party exempt or non-exempt account. True, false or null.
   */
  #[ORM\Column(type: 'boolean')]
  public $exempt;

  /**
   * @var string
   *   Code of that the committee is pointed at.
   * @Column
   *   Election year for single-year committees. Null or empty string for continuing committees.
   */
  #[ORM\Column]
  public $election_code;

  /**
   * @var int
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $start_year;

  /**
   * @var int
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $end_year;


  /**
   * @Column
   * @var string
   *   What contribution limits apply to this committee. With types fleshed out, is this redundant?
   */
  #[ORM\Column]
  public $limits_type;


  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *   Date registration submitted.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $registered_at;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *   Date record updated.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;

  /**
   * @Column
   * @var string
   *   Filer ID used for the committee for the year.
   */
  #[ORM\Column]
  public $filer_id;

  /**
   * @Column(type="integer")
   * @var int
   *   The jurisdiction that the committee is in.
   */
  #[ORM\Column(type: 'integer')]
  public $jurisdiction_id;

  /**
   * @Column
   * @var string
   *   Full reporting or mini-reporting.
   */
  #[ORM\Column]
  public $reporting_type;

  /**
   * @Column(type="integer")
   * @var int
   *   Political party. Value: null or party.party_id
   */
  #[ORM\Column(type: 'integer')]
  public $party;

  /**
   * @Column
   * @var string
   *   Committee financial depository.
   */
  #[ORM\Column]
  public $bank_name;

  /**
   * @Column
   * @var string
   *   Committee financial depository branch.
   */
  #[ORM\Column]
  public $bank_branch;

  /**
   * @Column
   * @var string
   *   Committee financial depository branch city.
   */
  #[ORM\Column]
  public $bank_city;

  /**
   * @Column(type="boolean")
   * @var bool
   *   Committee intends to support/oppose one or more candidates
   */
  #[ORM\Column(type: 'boolean')]
  public $pc_stance_candidate;

  /**
   * @Column(type="boolean")
   * @var bool
   *   Committee intends to support/oppose the entire ticket of a political party
   */

  #[ORM\Column(type: 'boolean')]
  public $pc_stance_party;

  /**
   * @Column(type="boolean")
   * @var bool
   *   Acknowledgement about eligibility to donate to other committees.
   */

  #[ORM\Column(type: 'boolean')]
  public $pc_donation_ack;

  /**
   * @Column
   * @var string
   *   Committee county. (Legacy data?)
   */
  #[ORM\Column]
  public $county;

  /**
   * @Column
   * @var string
   *   Web address
   */
  #[ORM\Column]
  public $url;

  /**
   * @Column
   * @var string
   *   Shortened or alternate name of committee
   */
  #[ORM\Column]
  public $acronym;

  /**
   * @Column
   * @var string
   *   Internal notes.
   */
  #[ORM\Column]
  public $memo;

  /**
   * @Column(type="integer")
   * @var int
   *   Registration ID
   */
  #[ORM\Column(type: 'integer')]
  public $registration_id;

  /**
   * @Column(type="boolean")
   * @var bool
   *   Is this a continuing committee?
   */
  #[ORM\Column(type: 'boolean')]
  public $continuing;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *   Date record updated.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $c1_sync;

  /**
   * @Column(type="boolean")
   *   Is this committee eligible to verify its own registrations?
   */
  #[ORM\Column(type: 'boolean')]
  public $autoverify;

  /**
   * @Column(type="boolean")
   */
  #[ORM\Column(type: 'boolean')]
  public $mini_full_permission;

  /**
   * @Column(type="integer")
   * @var int
   * person_id of associated candidate
   */
  #[ORM\Column(type: 'integer')]
  public $person_id;

  /**
   * @Column
   * @var string
   *   Category is the committee type.
   */
  #[ORM\Column]
  public $category;
}