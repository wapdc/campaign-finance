<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="transaction")
 */
#[ORM\Entity]
#[ORM\Table(name: 'transaction')]
class Transaction
{

  /**
   * @Column(type="integer") @id @GeneratedValue
   * @var int
   *   Transaction id.
   */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $transaction_id;

  /**
   * @Column(type="integer")
   * @var int
   *   Fund id.
   */
  #[ORM\Column(type: 'integer')]
  public $fund_id;

  /**
   * @Column(type="integer")
   * @var int
   *   Report id.
   */
  #[ORM\Column(type: 'integer')]
  public $report_id;

  /**
   * @Column
   * @var string
   *   Transaction origin.
   */
  #[ORM\Column]
  public $origin;

  /**
   * @Column
   * @var string
   *   Transaction category.
   */
  #[ORM\Column]
  public $category;

  /**
   * @Column
   * @var string
   *   Transaction action, e.g. receipt, payment, refund, forgiveness.
   */
  #[ORM\Column]
  public $act;

  /**
   * @Column(type="decimal")
   * @var float
   *   Transaction amount.
   */
  #[ORM\Column(type: 'decimal')]
  public $amount;

  /**
   * @Column(type="boolean")
   * @var boolean
   *   In-kind transaction.
   */
  #[ORM\Column(type: 'boolean')]
  public $inkind = FALSE;

  /**
   * @Column(type="boolean")
   * @var boolean
   *   Transaction is not a grouped transaction.
   */
  #[ORM\Column(type: 'boolean')]
  public $itemized = TRUE;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Transaction date.
   */
  #[ORM\Column(type: 'date')]
  public $transaction_date;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *   Date transaction created.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $created_at;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *   Date transaction last updated.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;

  /**
   * @Column(type="integer")
   * @var int
   *   Parent id.
   */
  #[ORM\Column(type: 'integer')]
  public $parent_id;

  /**
   * @Column
   * @var string
   *   External guid.
   */
  #[ORM\Column]
  public $external_guid;

  /**
   * @Column
   * @var string
   *   External type.
   */
  #[ORM\Column]
  public $external_type;

  /**
   * @Column(type="integer")
   * @var int
   *   Legacy id.
   */
  #[ORM\Column(type: 'integer')]
  public $legacy_id;

  /**
   * @Column
   * @var string
   *   Legacy table.
   */
  #[ORM\Column]
  public $legacy_table;

  /**
   * @Column
   * @var string
   *   Form type from legacy system.
   */
  #[ORM\Column]
  public $legacy_form_type;

  /**
   * @Column
   * @var string
   *   Expense description.
   */
  #[ORM\Column]
  public $description;

  /**
   * @Column
   * @var int
   *   Expense type (0, 1, -1) based on how it affects total expenditures for the campaign.
   */
  #[ORM\Column]
  public $expense_type = 0;
}
