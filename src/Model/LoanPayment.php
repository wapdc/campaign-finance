<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="payment")
 */
#[ORM\Entity]
#[ORM\Table(name: 'payment')]
class LoanPayment
{

  /**
   * @Column(type="integer") @id
   * @var int
   *   Transaction id.
   */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  public $transaction_id;

  /**
   * @Column(type="float")
   * @var float
   *   Amount paid on principle
   */
  #[ORM\Column(type: 'float')]
  public $principle_paid;

  /**
   * @Column(type="float")
   * @var float
   *   Amount forgiven.
   */
  #[ORM\Column(type: 'float')]
  public $forgiven_amount;

  /**
   * @Column(type="float")
   * @var float
   *   Interest paid
   */
  #[ORM\Column(type: 'float')]
  public $interest_paid;

  /**
   * @Column(type="float")
   * @var float
   *   Balance
   */
  #[ORM\Column(type: 'float')]
  public $balance_owed;

  /**
   * @Column(type="float")
   * @var float
   *   Original loan amount
   */
  #[ORM\Column(type: 'float')]
  public $original_amount;

}
