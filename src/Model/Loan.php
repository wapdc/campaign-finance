<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="loan")
 */
#[ORM\Entity]
#[ORM\Table(name: 'loan')]
class Loan
{

  /**
   * @Column(type="integer") @id
   * @var int
   *   Transaction id.
   */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  public $transaction_id;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Loan date.
   */
  #[ORM\Column(type: 'date')]
  public $loan_date;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Due date.
   */
  #[ORM\Column(type: 'date')]
  public $due_date;

  /**
   * @Column (type="float")
   * @var float
   *   Interest rate.
   */
  #[ORM\Column(type: 'float')]
  public $interest_rate;

  /**
   * @Column
   * @var string
   *   Payment schedule.
   */
  #[ORM\Column]
  public $payment_schedule;

  /**
   * @Column(type="float")
   * @var float
   *   Liable amount.
   */
  #[ORM\Column(type: 'float')]
  public $liable_amount;

  /**
   * @Column
   * @var string
   *   Lender or endorser
   */
  #[ORM\Column]
  public $lender_endorser;

}
