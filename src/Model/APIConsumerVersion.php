<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="api_consumer_version")
 */
#[ORM\Entity]
#[ORM\Table(name: 'api_consumer_version')]
class APIConsumerVersion
{
  /**
   * @Column(type="integer") @id @GeneratedValue
   * @var int
   *   api id.
   */
  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(type: 'integer')]
  public $version_id;

  /**
   * @Column(type="integer")
   * @var int
   *   consumer  of the api.
   */
  #[ORM\Column(type: 'integer')]
  public $consumer_id;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Release date.
   */
  #[ORM\Column(type: 'date')]
  public $release_date;

  /**
   * @Column(type="string")
   * @var string
   *   version number.
   */
  #[ORM\Column(type: 'string')]
  public $version;

  /**
   * @Column(type="boolean")
   * @var boolean
   *   mandatory.
   */
  #[ORM\Column(type: 'boolean')]
  public $mandatory_update;

  /**
   * @Column
   * @var string
   *   release notes.
   */
  #[ORM\Column]
  public $fix;
}
