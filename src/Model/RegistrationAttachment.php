<?php


namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class RegistrationAttachment
 *
 * @Entity
 * @Table (name = "registration_attachment")
 */
#[ORM\Entity]
#[ORM\Table(name: "registration_attachment")]
class RegistrationAttachment {

  /**
   * @param mixed $access_mode
   *
   * @return RegistrationAttachment
   */
  public function setAccessMode($access_mode, $file_service_url) {
    $this->access_mode = trim($access_mode);
    $this->file_service_url = trim($file_service_url);
    return $this;
  }

  /** @Id @Column(type="string") */
  #[ORM\Id]
  #[ORM\Column(type: "string")]
  public $alias;

  /** @Column(type="integer") */
  #[ORM\Column(type: "integer")]
  public $registration_id;

  /** @Column(type="string") */
  #[ORM\Column(type: "string")]
  public $description;

  /** @Column(type="string") */
  #[ORM\Column(type: "string")]
  public $document_type;

  /** @Column(type="string") */
  #[ORM\Column(type: "string")]
  public $access_mode;

  /** @Column(type="string") */
  #[ORM\Column(type: "string")]
  public $file_service_url;

  public function __construct($alias, $file_service_url, $registration_id, $access_mode) {
    $this->alias = $alias;
    $this->file_service_url = $file_service_url;
    $this->registration_id = $registration_id;
    $this->access_mode = $access_mode;
  }

  public function __get($name) {
    return $this->$name;
  }
}
