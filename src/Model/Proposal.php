<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Proposal
 * @Entity
 * @Table(name="proposal")
 */
#[ORM\Entity]
#[ORM\Table(name: 'proposal')]
class Proposal
{

  /**
   * @Column(type="integer") @Id @GeneratedValue
   * @var int
   */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $proposal_id;

  /**
   * @Column(type="integer")
   * @var int
   *   Jurisdiction ID.
   */
  #[ORM\Column(type: 'integer')]
  public $jurisdiction_id;

  /**
   * @Column
   * @var string
   *   Initiative (includes referendum), levy (includes bond and advisory vote), recall.
   */
  #[ORM\Column]
  public $proposal_type;

  /**
   * @Column
   * @var string
   *   Ballot number.
   */
  #[ORM\Column]
  public $number;

  /**
   * @Column
   * @var string
   *   Ballot title.
   */
  #[ORM\Column]
  public $title;

  /**
   * @Column
   * @var string
   *   Target election code.
   */
  #[ORM\Column]
  public $election_code;

  /**
   * @Column(type="boolean")
   * @var boolean
   *   Internal staff verification.
   */
  #[ORM\Column(type: 'boolean')]
  public $verified;
}