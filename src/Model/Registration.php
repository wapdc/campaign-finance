<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="registration")
 */
#[ORM\Entity]
#[ORM\Table(name: 'registration')]
class Registration
{

  /**
   * @Id @Column(type="integer") @GeneratedValue
   * @var int
   *   Unique ID.
   */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $registration_id;

  /**
   * @Column(type="integer")
   * @var int
   *   Committee ID.
   */
  #[ORM\Column(type: 'integer')]
  public $committee_id;

  /**
   * @Column
   * @var string
   *   Committee name used in this registration.
   */
  #[ORM\Column]
  public $name;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Date registration became current for its committee.
   */
  #[ORM\Column(type: 'date')]
  public $valid_from;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Date registration stopped being current for committee.
   */
  #[ORM\Column(type: 'date')]
  public $valid_to;

  /**
   * @Column
   * @var string
   *   Submission data as submitted by the user. The most authoritative data.
   */
  #[ORM\Column]
  public $user_data;

  /**
   * @Column
   * @var string
   *   Metadata submitted by the admin (e.g. mappings).
   */
  #[ORM\Column]
  public $admin_data;

  /**
   * @Column
   * @var string
   *   HTML representation of the submission used primarily for display purposes.
   */
  #[ORM\Column]
  public $html;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *   Submission date.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $submitted_at;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *   Update time
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;
  

  /**
   * @Column
   * @var string
   *   Who submitted this registration.
   */
  #[ORM\Column]
  public $username;

  /**
   * @Column
   * @var string
   *   Source, e.g. ORCA, web, third-party software, etc.
   */
  #[ORM\Column]
  public $source;

  /**
   * @Column
   * @var string
   *   Version number of XML schema.
   */
  #[ORM\Column]
  public $version;

  /**
   * @Column
   * @var string
   *   Optional internal note for the registration log.
   */
  #[ORM\Column]
  public $memo;

  /**
   * @Column
   * @var string
   *   Certification text.
   */
  #[ORM\Column]
  public $certification;

  /**
   * @Column
   * @var string
   *   Certification email address.
   */
  #[ORM\Column]
  public $certification_email;

  /**
   * @Column
   * @var string
   *   Scanned document identifier.
   */
  #[ORM\Column]
  public $attachment_reference;

  /**
   * @Column(type="boolean")
   * @var boolean
   *   Internal verification.
   */
  #[ORM\Column(type: 'boolean')]
  public $verified;

  /**
   * @Column
   * @var string
   *  Committee reporting type when registered
   */
  #[ORM\Column]
  public $reporting_type;

  /**
   * @Column
   * @var int
   *  Legacy document id from paper registration/amendment filing
   */
  #[ORM\Column]
  public $legacy_doc_id;

}
