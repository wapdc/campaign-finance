<?php


namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CommitteeLog
 * @Entity
 * @Table(name="committee_log")
 */
#[ORM\Entity]
#[ORM\Table(name: 'committee_log')]
class CommitteeLog {

  /** @Column @Id @GeneratedValue */
  #[ORM\Column]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  private $log_id;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $committee_id;

  /** @Column */
  #[ORM\Column]
  public $transaction_type;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $transaction_id;

  /** @Column */
  #[ORM\Column]
  public $action;

  /** @Column */
  #[ORM\Column]
  public $message;

  /** @Column */
  #[ORM\Column]
  public $user_name;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;


}