<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CommitteeAuthorization
 * @Entity
 * @Table(name="committee_authorization")
 */
#[ORM\Entity]
#[ORM\Table(name: 'committee_authorization')]
class CommitteeAuthorization {

  /**
   * @Id @Column(type="integer")
   * @var Int
   */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  public $committee_id;

  /**
   * @Id
   * @Column
   */
  #[ORM\Column]
  #[ORM\Id]
  public $realm;

  /**
   * @Id
   * @Column
   * @var int
   *   Id of user with granted access
   */
  #[ORM\Column]
  #[ORM\Id]
  public $uid;

  /**
   * @Column
   * @var string
   *   Role that the user has within the committee. Could be 'full' or just 'filer', meaning that
   *   the user cannot change the campaign registration but can file expenses, etc.
   */
  #[ORM\Column]
  public $role;

  /**
   * @Column(type="boolean")
   * @var boolean
   *   Whether the role access has been approved.
   */
  #[ORM\Column(type: 'boolean')]
  public $approved=FALSE;

    /**
     * @Column(type="date")
     * @var \DateTime
     * Date of allowed access from the committee
     */
   #[ORM\Column(type: 'date')]
   public $granted_date;

    /**
     * @Column(type="date")
     * @var \DateTime
     * Date of allowed access from the committee
     */
   #[ORM\Column(type: 'date')]
   public $expiration_date;

    public function __construct($committee_id, $realm, $uid, $granted_date = null, $expiration_date = null) {
    $this->committee_id = $committee_id;
    $this->realm = $realm;
    $this->uid = $uid;
    $this->granted_date = $granted_date;
    $this->expiration_date = $expiration_date;
  }

}