<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="api_consumer")
 */
#[ORM\Entity]
#[ORM\Table(name: 'api_consumer')]
class APIConsumer
{
  /**
   * @Column(type="integer") @id @GeneratedValue
   * @var int
   *   consumer id.
   */
  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(type: 'integer')]
  public $consumer_id;

  /**
   * @Column
   * @var string
   *   application name.
   */
  #[ORM\Column]
  public $application;

  /**
   * @Column
   * @var string
   *   Organization name.
   */
  #[ORM\Column]
  public $vendor;

  /**
   * @Column
   * @var string
   *   email.
   */
  #[ORM\Column]
  public $vendor_email;

  /**
   * @Column
   * @var string
   *   phone.
   */
  #[ORM\Column]
  public $vendor_phone;

  /**
   * @Column
   * @var string
   *   name of the person to contact.
   */
  #[ORM\Column]
  public $vendor_contact_name;

  /**
   * @Column
   * @var string
   *   consumer api key.
   */
  #[ORM\Column]
  public $api_key;
}
