<?php


namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Sum
 * @Entity
 * @Table(name="report_sum")
 */
#[ORM\Entity]
#[ORM\Table(name: 'report_sum')]
class Sum {

  /**
   * @Column(type="integer") @Id
   * @var int
   */
  #[ORM\Column(type:'integer')]
  #[ORM\Id]
  public $transaction_id;

  /**
   * @Column(type="decimal")
   * @var float
   */
  #[ORM\Column(type: 'decimal')]
  public $aggregate_amount;

  /**
   * @Column
   */
  #[ORM\Column]
  public $legacy_line_item;

  /**
   * @Column(type="integer")
   * @var int
   *   Number of transactions included in this sum.
   */
  #[ORM\Column(type: 'integer')]
  public $tx_count;

}