<?php


namespace WAPDC\CampaignFinance\Model\Legacy;
use Doctrine\ORM\Mapping as ORM;


/**
 * @Entity
 * @Table(name="wapdc.dbo.registration_entry")
 */
#[ORM\Entity]
#[ORM\Table(name: 'wapdc.dbo.registration_entry')]
class RegistrationEntry {
  /** @Column @Id*/
  #[ORM\Column]
  #[ORM\Id]
  public $registration_id;

  /** @Column */
  #[ORM\Column]
  public $filer_id;

  /** @Column */
  #[ORM\Column]
  public $election_year;

  /** @Column */
  #[ORM\Column]
  public $receipt_date;

  /** @Column */
  #[ORM\Column]
  public $report_number;

  /** @Column */
  #[ORM\Column]
  public $source;
}