<?php


namespace WAPDC\CampaignFinance\Model\Legacy;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="wapdc.dbo.c1")
 */
#[ORM\Entity]
#[ORM\Table(name: 'wapdc.dbo.c1')]
class C1 {

  /** @Column @Id @GeneratedValue*/
  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column]
  public $c1_id;

  /** @Column */
  #[ORM\Column]
  public $filer_id;

  /** @Column */
  #[ORM\Column]
  public $election_year;

  /** @Column */
  #[ORM\Column]
  public $filer_type;

  /** @Column */
  #[ORM\Column]
  public $req3;

  /** @Column */
  #[ORM\Column]
  public $postc1;

  /** @Column */
  #[ORM\Column]
  public $elyb;

  /** @Column */
  #[ORM\Column]
  public $ro;

  /** @Column */
  #[ORM\Column]
  public $acro;

  /** @Column */
  #[ORM\Column]
  public $name;

  /** @Column */
  #[ORM\Column]
  public $en;

  /** @Column */
  #[ORM\Column]
  public $cur;

  /** @Column */
  #[ORM\Column]
  public $email;

  /** @Column */
  #[ORM\Column]
  public $addr;

  /** @Column */
  #[ORM\Column]
  public $city;

  /** @Column */
  #[ORM\Column]
  public $st;

  /** @Column */
  #[ORM\Column]
  public $zip;

  /** @Column */
  #[ORM\Column]
  public $fonec1;

  /** @Column */
  #[ORM\Column]
  public $cand_comm_phone;

  /** @Column */
  #[ORM\Column]
  public $type;

  /** @Column */
  #[ORM\Column]
  public $a1;

  /** @Column */
  #[ORM\Column]
  public $b1;

  /** @Column */
  #[ORM\Column]
  public $c1;

  /** @Column */
  #[ORM\Column]
  public $treaf;

  /** @Column */
  #[ORM\Column]
  public $treal;

  /** @Column */
  #[ORM\Column]
  public $bname;

  /** @Column */
  #[ORM\Column]
  public $bnum;

  /** @Column */
  #[ORM\Column]
  public $fa;

  /** @Column */
  #[ORM\Column]
  public $offs1;

  /** @Column */
  #[ORM\Column]
  public $diss1;

  /** @Column */
  #[ORM\Column]
  public $poss1;

  /** @Column */
  #[ORM\Column]
  public $jurisdiction;

  /** @Column */
  #[ORM\Column]
  public $jurisdiction_co;

  /** @Coumn */
  #[ORM\Column]
  public $com_type;

  /**
   * @Column
   */
  #[ORM\Column]
  public $stat;

  /**
   * @Column
   */
  #[ORM\Column]
  public $decl;

  /**
   * @Column
   */
  #[ORM\Column]
  public $dec;

  /**
   * @Column
   */
  #[ORM\Column]
  public $prim;

  /**
   * @Column
   */
  #[ORM\Column]
  public $genr;

  /**
   * @Column
   */
  #[ORM\Column]
  public $pty;

  /**
   * @Column
   */
  #[ORM\Column]
  public $caddr;

  /**
   * @Column
   */
  #[ORM\Column]
  public $ccity;

  /**
   * @Column
   */
  #[ORM\Column]
  public $cst;

  /**
   * @Column
   */
  #[ORM\Column]
  public $czip;

  /**
   * @Column
   */
  #[ORM\Column]
  public $candidate_email;

  /** @Column */
  #[ORM\Column]
  public $sey_cand_yes;

  /** @Column */
  #[ORM\Column]
  public $sey_cand_no;

  /** @Column */
  #[ORM\Column]
  public $sey_ticket_yes;

  /** @Column */
  #[ORM\Column]
  public $sey_ticket_no;

  /** @Column */
  #[ORM\Column]
  public $memo;

  /** @Column */
  #[ORM\Column]
  public $registration_id;

  /** @Column */
  #[ORM\Column]
  public $position;

  /** @Column */
  #[ORM\Column]
  public $bonafide_type;

  /** @Column */
  #[ORM\Column]
  public $ldis;
}