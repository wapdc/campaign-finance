<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @Entity
 * @Table(name="report")
 */
#[ORM\Entity]
#[ORM\Table(name: 'report')]
class Report
{

  /**
   * @Column(type="integer") @id @GeneratedValue
   * @var int
   *   Report primary key.
   */
  #[ORM\Column(type:'integer')]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $report_id;

  /**
   * @Column(type="integer")
   * @var int
   *   Legacy report number
   */
  #[ORM\Column(type:'integer')]
  public $repno;

  /**
   * @Column
   * @var int
   *   Superseded by report.
   */
  #[ORM\Column]
  public $superseded_id;

  /**
   * @Column
   * @var int
   *   Superseded by legacy report.
   */
  #[ORM\Column]
  public $superseded_repno;

  /**
   * @Column
   * @var string
   *   Report type.
   */
  #[ORM\Column]
  public $report_type;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *   Date report submitted.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $submitted_at;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Report start date.
   */
  #[ORM\Column(type: 'date')]
  public $period_start;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Report end date.
   */
  #[ORM\Column(type: 'date')]
  public $period_end;

  /**
   * @Column(type="integer")
   * @var int
   *   Fund id.
   */
  #[ORM\Column(type: 'integer')]
  public $fund_id;

  /**
   * @Column
   * @var string
   *   Record type.
   */
  #[ORM\Column]
  public $record_type;

  /**
   * @Column
   * @var string
   *   Record type.
   */
  #[ORM\Column]
  public $form_type;

  /**
   * @Column
   * @var string
   *   Election year.
   */
  #[ORM\Column]
  public $election_year;

  /**
   * @Column
   * @var string
   *   Entity CD.
   */
  #[ORM\Column]
  public $entity_code;

  /**
   * @Column
   * @var string
   *   Filer name.
   */
  #[ORM\Column]
  public $filer_name;

  /**
   * @Column
   * @var string
   *   Address 1.
   */
  #[ORM\Column]
  public $address1;

  /**
   * @Column
   * @var string
   *   Address 2.
   */
  #[ORM\Column]
  public $address2;

  /**
   * @Column
   * @var string
   *   City.
   */
  #[ORM\Column]
  public $city;

  /**
   * @Column
   * @var string
   *   State.
   */
  #[ORM\Column]
  public $state;

  /**
   * @Column
   * @var string
   *   Zip code.
   */
  #[ORM\Column]
  public $postcode;

  /**
   * @Column
   * @var string
   *   Office code.
   */
  #[ORM\Column]
  public $office_code;

  /**
   * @Column
   * @var string
   *   Treasurer name.
   */
  #[ORM\Column]
  public $treasurer_name;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Treasurer date.
   */
  #[ORM\Column(type: 'date')]
  public $treasurer_date;

  /**
   * @Column
   * @var string
   *   Treasurer phone.
   */
  #[ORM\Column]
  public $treasurer_phone;

  /**
   * @Column(type="boolean")
   * @var bool
   *   Final report.
   */
  #[ORM\Column(type: 'boolean')]
  public $final_report;

  /**
   * @Column
   * @var string
   *   Candidate name.
   */
  #[ORM\Column]
  public $candidate_name;

  /**
   * @Column(type="boolean")
   * @var bool
   *   Ind and ppc_so_yn from original tables.
   */
  #[ORM\Column(type: 'boolean')]
  public $ind_exp;

  /**
   * @Column
   * @var string
   *   How report was filed (electronic or paper)
   */
  #[ORM\Column]
  public $filing_method;

  /**
   * @Column
   * @var string
   *  The JSON representation of the submission data
   */
  #[ORM\Column]
  public $user_data;

  /**
   * @Column
   * @var string
   *  The current version of the user_data json schema
   */
  #[ORM\Column]
  public $version;

  /**
   * @Column
   * @var string
   *  The calculated sums in JSON
   * If vendor, then the associated metadata as well.
   */
  #[ORM\Column]
  public $metadata;

  /**
   * @Column
   * @var integer
   *  The calculated sums in JSON
   */
  #[ORM\Column]
  public $external_id;

  /**
   * @Column
   * @var string
   *  The vendor supplied in the submission
   */
  #[ORM\Column]
  public $source;

}
