<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="expense")
 */
#[ORM\Entity]
#[ORM\Table(name: 'expense')]
class Expense
{

  /**
   * @Column(type="integer") @id
   * @var int
   *   Transaction id.
   */
  #[ORM\Column( type: 'integer' )]
  #[ORM\Id]
  public $transaction_id;

  /**
   * @Column(type="boolean")
   * @var bool
   */
  #[ORM\Column(type: 'boolean')]
  public $anonymous=TRUE;

  /**
   * @Column
   * @var string
   *   Contact guid.
   */
  #[ORM\Column]
  public $contact_key;

  /**
   * @Column
   * @var string
   *   Expense name.
   */
  #[ORM\Column]
  public $name;

  /**
   * @Column
   * @var string
   *   Expense address.
   */
  #[ORM\Column]
  public $address;

  /**
   * @Column
   * @var string
   *   Expense city.
   */
  #[ORM\Column]
  public $city;

  /**
   * @Column
   * @var string
   *   Expense state.
   */
  #[ORM\Column]
  public $state;

  /**
   * @Column
   * @var string
   *   Expense postcode.
   */
  #[ORM\Column]
  public $postcode;

  /**
   * @Column
   * @var string
   *   Expense category.
   */
  #[ORM\Column]
  public $category;

  /**
   * @Column
   * @var string
   *   Expense address geocoded.
   */
  #[ORM\Column]
  public $geocode_hash_key;

  /**
   * @Column
   * @var string
   *  Expense payee name
   */
  #[ORM\Column]
  public $payee;

  /**
   * @Column
   * @var string
   */
  #[ORM\Column]
  public $creditor;

}
