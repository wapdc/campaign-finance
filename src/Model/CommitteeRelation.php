<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="committee_relation")
 */
#[ORM\Entity]
#[ORM\Table(name: 'committee_relation')]
class CommitteeRelation {

  public function __construct($committee_id, $relation_type) {
    $this->committee_id = $committee_id;
    $this->relation_type = $relation_type;
  }

  /**
   * @Id @Column @GeneratedValue
   * @var int
   *   ID of the relationship.
   */
  #[ORM\Column]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $relation_id;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $committee_id;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $registration_id;

  /**
   * @Column
   * @var string
   *   Indicates the type of the relation and has the following potential values
   *   "candidacy" - Candidacy
   *   "committee" - Political committee
   *   "ticket" - Political party ticket
   *   "proposal" - Ballot proposition
   *   "issue" - General topic
   *   "interest" - ?
   */
  #[ORM\Column]
  public $relation_type;


  /**
   * @Column(type="integer")
   * @var int
   *   ID of target Committee, Proposal, etc.
   */
  #[ORM\Column(type: 'integer')]
  public $target_id;

  /**
   * @Column
   * @var string
   *   Text name when no target ID.
   */
  #[ORM\Column]
  public $target_name;

  /**
   * @Column
   * @var string
   *   Indicates for or against.
   */
  #[ORM\Column]
  public $stance;

  /**
   * @Column(type="datetimetz")
   * @var \DateTimeZone
   *   Datetime record updated.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;

}