<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="contribution")
 */
#[ORM\Entity]
#[ORM\Table(name: 'contribution')]
class Contribution
{


  /**
   * @Column(type="integer") @id
   * @var int
   *   Transaction id.
   */
  #[ORM\Column( type: 'integer')]
  #[ORM\Id]
  public $transaction_id;

  /**
   * @Column(type="boolean")
   * @var bool
   */
  #[ORM\Column(type: 'boolean')]
  public $anonymous=TRUE;

  /**
   * @Column
   * @var string
   *   Contact guid.
   */
  #[ORM\Column]
  public $contact_key;

  /**
   * @Column
   * @var string
   *   Contributor name.
   */
  #[ORM\Column]
  public $name;

  /**
   * @Column
   * @var string
   *   Contributor address.
   */
  #[ORM\Column]
  public $address;

  /**
   * @Column
   * @var string
   *   Contributor city.
   */
  #[ORM\Column]
  public $city;

  /**
   * @Column
   * @var string
   *   Contributor state.
   */
  #[ORM\Column]
  public $state;

  /**
   * @Column
   * @var string
   *   Contributor postcode.
   */
  #[ORM\Column]
  public $postcode;

  /**
   * @Column
   * @var string
   *   Contributor occupation.
   */
  #[ORM\Column]
  public $occupation;

  /**
   * @Column
   * @var string
   *   Contributor employer name.
   */
  #[ORM\Column]
  public $employer;

  /**
   * @Column
   * @var string
   *   Contributor employer address.
   */
  #[ORM\Column]
  public $employer_address;

  /**
   * @Column
   * @var string
   *   Contributor employer city.
   */
  #[ORM\Column]
  public $employer_city;

  /**
   * @Column
   * @var string
   *   Contributor employer state.
   */
  #[ORM\Column]
  public $employer_state;

  /**
   * @Column
   * @var string
   *   Contributor employer postcode.
   */
  #[ORM\Column]
  public $employer_postcode;

  /**
   * @Column
   * @var string
   *   Primary or general election.
   */
  #[ORM\Column]
  public $prim_gen;

  /**
   * @Column
   * @var string
   *   Contributor type.
   */
  #[ORM\Column]
  public $contributor_type;

  /**
   * @Column
   * @var string
   *   Contributor address geocoded.
   */
  #[ORM\Column]
  public $geocode_hash_key;

}
