<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="registration_change")
 */
#[ORM\Entity]
#[ORM\Table(name: 'registration_change')]
class RegistrationChange
{

  /**
   * @Id @Column(type="integer")
   *   Registration ID
   */
  #[ORM\Id]
  #[ORM\Column(type: 'integer')]
  public $registration_id;

  /**
   * @Id @Column
   *   Change category - address, bank, contact, type
   */
  #[ORM\Column]
  public $change_type;
}