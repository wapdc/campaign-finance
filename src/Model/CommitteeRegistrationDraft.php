<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="committee_registration_draft")
 */
#[ORM\Entity]
#[ORM\Table(name: 'committee_registration_draft')]
class CommitteeRegistrationDraft
{
  /**
   * @Column
   * @Id
   * @var int
   *    Committee ID
   */
  #[ORM\Column]
  #[ORM\Id]
  public $committee_id;

  /**
   * @Column
   * @var string
   *    JSON data representation of submission data
   */
  #[ORM\Column]
  private $registration_data;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   *    Update time
   */
  #[ORM\Column(type: 'datetimetz')]

  public $updated_at;
  /**
   * @Column
   * @var string
   *    Who created this draft
   */
  #[ORM\Column]
  public $username;
  /**
   * @Column
   * @var string
   *    Scanned document identifier
   */
  #[ORM\Column]
  public $attachment_reference;
  /**
   * @Column(type="boolean")
   * @var boolean
   *    Internal verification
   */
  #[ORM\Column(type: 'boolean')]
  public $verified;

  public function getRegistrationData() {
    $jsonDecoded = json_decode($this->registration_data);
    if (isset($jsonDecoded->committee->candidacy)) {
      $candidacy = $jsonDecoded->committee->candidacy;
      if (isset($candidacy->location) && !isset($candidacy->jurisdiction)) {
        $candidacy->jurisdiction = $candidacy->location;
        unset($candidacy->location);
      }
      if (isset($candidacy->location_id) && !isset($candidacy->jurisdiction_id)) {
        $candidacy->jurisdiction_id = $candidacy->location_id;
        unset($candidacy->location_id);
      }
    }
    return $jsonDecoded;
  }

  /**
   * @param string|\stdClass registration_data
   *   Data for the registrationDraft.
   */
  public function setRegistrationData($registration_data) {
    if (is_object($registration_data)){
      $registration_data = json_encode($registration_data);
    }
    $this->registration_data = $registration_data;
  }

  public function __construct($committee_id) {
    $this->committee_id = $committee_id;
  }
}