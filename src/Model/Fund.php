<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class Fund
 * @Entity
 * @Table(name="fund")
 */
#[ORM\Entity]
#[ORM\Table(name: 'fund')]
class Fund
{

  /**
   * @Column(type="integer") @id @GeneratedValue
   * @var int
   *   Fund primary key.
   */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $fund_id;

  /**
   * @Column(type="integer")
   * @var int
   *   Committee id.
   */
  #[ORM\Column(type: 'integer')]
  public $committee_id;

  /**
   * @Column
   * @var string
   *   Legacy filer_id.
   */
  #[ORM\Column]
  public $filer_id;

  /**
   * @Column
   * @var string
   *   Election code.
   */
  #[ORM\Column]
  public $election_code;

  /**
   * @Column
   * @var string
   *   $vendor
   */
  #[ORM\Column]
  public $vendor;

    /**
     * @Column
     * @var string
     *   $target_type
     *   default value is committee
     */
    #[ORM\Column]
    public $target_type = 'committee';

    /**
     * @Column
     * @var integer
     *   $target_id
     */
    #[ORM\Column]
    public $target_id;
}
