<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

class Organization
{

  /**
   * @Column(type="integer") @id @GeneratedValue
   * @var int
   *   Organization primary key.
   */
  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(type: "integer")]
  public $organization_id;

  /**
   * @Column
   * @var string
   *   Organization name.
   */
  #[ORM\Column]
  public $name;

}