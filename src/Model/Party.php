<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class Party
 * @Entity
 * @Table(name="party")
 */
#[ORM\Entity]
#[ORM\Table(name: 'party')]
class Party
{

  /**
   * @Column(type="integer") @Id @GeneratedValue
   * @var int
   */
  #[ORM\Column]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $party_id;

  /**
   * @Column
   * @var string
   *   Party name.
   */
  #[ORM\Column]
  public $name;

  /**
   * @Column
   * @var string
   *   Party short code.
   */
  #[ORM\Column]
  public $code;

  /**
   * @Column(type="date")
   * @var \DateTime
   *   Party valid starting date.
   */
  #[ORM\Column(type: 'date')]
  public $valid_from;

  /**
  /**
   * @Column(type="date")
   * @var \DateTime
   *   Party valid ending date.
   */
  #[ORM\Column(type: 'date')]
  public $valid_to;

  /**
   * @Column(type="datetimetz")
   * @var \DateTimeZone
   *   Datetime record updated.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;

}
