<?php

namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CommitteeAPIAuthorization
 * @Entity
 * @Table(name="committee_api_key")
 */
#[ORM\Entity]
#[ORM\Table(name: 'committee_api_key')]
class CommitteeAPIAuthorization {

  /** @Id @Column(type="integer") @GeneratedValue */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $api_key_id;

  /**
   * @Column(type="integer")
   * @var Int
   */
  #[ORM\Column(type: 'integer')]
  public $committee_id;

  /**
   * @Column
   * @var string
   *   Filers personal token
   */
  #[ORM\Column]
  public $hash_value;

  /**
   * @Column
   * @var string
   *   Represents the token purpose
   */
  #[ORM\Column]
  public $memo;

  /**
   * @Column(type="datetimetz")
   * @var \DateTimeZone
   *   Datetime record updated.
   */
  #[ORM\Column(type: 'datetimetz')]
  public $date_generated;


  public function __construct($committee_id, $hash_value) {
    $this->committee_id = $committee_id;
    $this->hash_value = $hash_value;
  }

}