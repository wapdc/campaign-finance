<?php


namespace WAPDC\CampaignFinance\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Correction
 * @Entity
 * @Table(name="correction")
 */
#[ORM\Entity]
#[ORM\Table(name: 'correction')]
class Correction {
  /** @var int @Column(type="integer") @Id */
  #[ORM\Column]
  #[ORM\Id]
  public $transaction_id;

  /**
   * @Column(type="decimal")
   * @var float
   */
  #[ORM\Column(type: 'decimal')]
  public $original_amount;

  /**
   * @Column(type="decimal")
   * @var float
   */
  #[ORM\Column(type: 'decimal')]
  public $corrected_amount;

}