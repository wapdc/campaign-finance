<?php


namespace WAPDC\CampaignFinance;

use WAPDC\CampaignFinance\Model\APIConsumer;
use WAPDC\CampaignFinance\Model\APIConsumerVersion;
use WAPDC\CampaignFinance\Model\Report;
use \PDO;
use Doctrine\DBAL\DBALException;
use \DateTime;
use \Exception;


class FinanceReportingProcessor {

  /** @var \WAPDC\CampaignFinance\CFDataManager $_dataManager */
  private $_dataManager;

  private $_committeeManager;

  public function __construct(CFDataManager $dataManager, CommitteeManager $committeeManager) {
    $this->_dataManager = $dataManager;
    $this->_committeeManager = $committeeManager;
  }

  public function submittedReports($committee_id) {
    $reports = $this->_dataManager->db->fetchAllAssociative("select 
      r.period_start, r.period_end, r.version, r.fund_id, r.external_id, r.report_id, r.report_type, 
        r.version ,r.submitted_at, r.superseded_id,
        cfs.processed, cfs.message, ra.report_id as amends
       from fund f join report r ON r.fund_id = f.fund_id
          JOIN election e ON F.election_code = e.election_code
          LEFT JOIN campaign_finance_submission cfs ON r.report_id = cfs.report_id
          LEFT JOIN report ra ON ra.superseded_id = r.report_id
        where
          f.committee_id = :committee_id
          order by r.period_end desc, r.repno desc, r.submitted_at desc",
    ['committee_id' => $committee_id]);
    return $reports ? $reports : [];
  }

  /**
   * @param string $payload
   * @param $version
   * @param int $committee_id
   *
   * @throws \Doctrine\DBAL\DBALException
   */
  function logSubmission($type, $payload, $source, $version, $committee_id, $ip_address=null, $submission_version = null) {
      if (!trim($type)) {
        $type = NULL;
      }
      if (!trim($source)) {
        $source = NULL;
      }

      $parms = [
        'submitted_at' => (new DateTime())->format('Y-m-d H:i:s'),
        'version' => $version,
        'type' => $type,
        'payload' => $payload,
        'source' => $source,
        'committee_id' => $committee_id,
        'ip_address' => $ip_address,
        'submission_version' => $submission_version,

      ];
      $this->_dataManager->db->executeStatement('INSERT INTO campaign_finance_submission ( submitted, submitted_at, source, version, type, payload, committee_id, ip_address, submission_version ) VALUES ( true, :submitted_at, :source, :version, :type, :payload, :committee_id, :ip_address, :submission_version)', $parms);
      return $this->_dataManager->db->lastInsertId();
  }

  /**
   * @param $report_id
   *
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   *
   * Set superseded field to null following run of submission comparison script so that the script can be rerun when needed
   */
  function clearSupercededId ($report_id) {
    $source_report = $this->_dataManager->em->find(Report::class, $report_id);
    if (!$source_report) {
        throw new \Exception('Report with matching id not found.');
    }
    $source_report->superseded_id = NULL;
    $this->_dataManager->em->persist($source_report);
    $this->_dataManager->em->flush();
  }

/**
   * @throws \Doctrine\DBAL\DBALException
   */
  function logProcessResults($submission_id, $processed, $message, $report_id=NULL, $fund_id=null) {
    if (stripos($message, 'warning:') !== FALSE) {
      $has_warnings = TRUE;
    }
    else {
      $has_warnings = FALSE;
    }
    $parms = [
      'submission_id' => $submission_id,
      'processed' => $processed,
      'message' => $message,
      'report_id' => $report_id,
      'has_warnings' => (int)$has_warnings,
      'fund_id' => $fund_id,
      'updated' => (new DateTime())->format('Y-m-d H:i:s')
    ];
    $types = [
      'processed'=>PDO::PARAM_BOOL
    ];
    $this->_dataManager->db->executeStatement('UPDATE campaign_finance_submission SET report_id = :report_id, fund_id = :fund_id, processed = :processed, message = :message, updated_at = :updated, has_warnings=:has_warnings WHERE submission_id = :submission_id', $parms, $types);
  }

  /**
   * Creates a API Consumer record
   * @param $consumer
   *
   * @return int
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Exception
   */
  function createConsumer($consumer) {
    if ($consumer->vendor && $consumer->vendor_email) {

      $api = new APIConsumer();
      $api->api_key = $consumer->api_key;
      $api->application = $consumer->application;
      $api->vendor = $consumer->vendor;
      $api->vendor_email = $consumer->vendor_email;
      $api->vendor_phone = $consumer->vendor_phone;
      $api->vendor_contact_name = $consumer->vendor_contact_name;

      $this->_dataManager->em->persist($api);
      $this->_dataManager->em->flush();
      return $api->consumer_id;
    } else {
      throw new Exception('API Consumer already exists');
    }
  }

  /**
   * Creates a version entry into the tbl for a API Consumer
   *
   * @param $version
   *
   * @return int
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Exception
   */
  function createVersion($version) {
    if ($version->consumer_id && $version->application
      && $version->version && $version->fix) {
      $result = $committee = $this->_dataManager->em->getRepository(APIConsumer::class)
        ->findOneBy([
          'application' => $version->application,
        ]);;

      if (!empty($result)) {
        $vers = new APIConsumerVersion();
        // The consumer's identifier
        $vers->consumer_id = $result->consumer_id;
        $vers->release_date = !empty($version->release_date) ? new DateTime($version->release_date) : null;
        $vers->version = $version->version;
        $vers->mandatory_update = $version->mandatory_update;
        $vers->fix = $version->fix;
        $this->_dataManager->em->persist($vers);
        $this->_dataManager->em->flush();
        return $vers->version_id;
      } else {
        throw new Exception('Missing API consumer. Cannot insert new API Version for ' . $version->application . " application.");
      }
    } else {
      throw new Exception('Required parameters are missing');
    }
  }


  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  function getApplicationUpdates($version, $application, $vendor) {
    $parms = [
      'version' => $version,

      'application' => $application,
      'vendor' => $vendor,
    ];

    try {
      $query = $this->_dataManager->db->executeQuery('select
      ver.version,
      ver.mandatory_update,
      ver.fix
  from api_consumer_version ver
  join api_consumer con on con.consumer_id = ver.consumer_id
  where cast(version as float) > :version
  and
      con.vendor = :vendor
  and
      con.application = :application', $parms);
    } catch (DBALException $e) {
    }
    return $query->fetchAllAssociative();
  }
}
