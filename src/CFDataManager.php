<?php


namespace WAPDC\CampaignFinance;


use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\Mapping\ClassMetadata;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\CommitteeAPIAuthorization;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\Core\RDSDataManager;

class CFDataManager extends RDSDataManager {

  /**
   * CaseTrackDataManager constructor.
   *
   * @param string $app_name
   *
   * @throws \Exception
   */
  public function __construct($app_name = 'campaign-finance', $migration=FALSE) {
    parent::__construct($app_name, $migration);
    $this->getConnection();
  }

  /**
   * Add the model directories for Campaign Finance
   */
  protected function modelDirectories(): array {
    $dirs = parent::modelDirectories();
    $dirs[] =  __DIR__ . '/Model';
    return $dirs;
  }

  /**
   * @param LoadClassMetadataEventArgs $eventArgs
   */
  public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs) {
    $class_metadata = $eventArgs->getClassMetadata();
    switch ($class_metadata->name) {
      case Committee::class:
      case Registration::class:
      case CommitteeAPIAuthorization::class:
        if ($this->migration_mode) {
          // Force the data to not generate an id but only when the data provider is in migration mode.
          $class_metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
          $class_metadata->setIdGenerator(new AssignedGenerator());
        }
        break;
      default:
        parent::loadClassMetadata($eventArgs);
    }
  }

}