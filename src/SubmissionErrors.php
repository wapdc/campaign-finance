<?php

namespace WAPDC\CampaignFinance;

class SubmissionErrors {

  private $errors = [];

  public function addError($id, $message) {
    $this->errors[$id] = $message;
  }

  public function hasErrors() {
    return !empty($this->errors);
  }

  public function getErrors() {
    return ($this->errors);
  }

  /**
   * @param $value
   * @return bool
   */
  public function hasErrorId($id){
    return array_key_exists($id, $this->errors);
  }

}