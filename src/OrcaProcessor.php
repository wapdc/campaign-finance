<?php

namespace WAPDC\CampaignFinance;

use Doctrine\DBAL\ConnectionException;
use Exception;
use stdClass;
use WAPDC\Core\Model\User;
use WAPDC\Core\TokenProcessor;
use WAPDC\CampaignFinance\OrcaProcessorTraits\AccountTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\AdvertisementTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\AuctionTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\CampaignTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\ContactTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\ContributionTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\CorrectionTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\CreditCardTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\DebtTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\DepositTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\ElectionTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\ExpenditureTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\FundraiserTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\FundTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\LoanTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\MiscTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\OrcaTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\PledgeTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\ReportTrait;
use WAPDC\CampaignFinance\OrcaProcessorTraits\TransactionTrait;

class OrcaProcessor {

  use AccountTrait;
  use AdvertisementTrait;
  use AuctionTrait;
  use CampaignTrait;
  use ContactTrait;
  use ContributionTrait;
  use CorrectionTrait;
  use CreditCardTrait;
  use DebtTrait;
  use DepositTrait;
  use ElectionTrait;
  use ExpenditureTrait;
  use FundraiserTrait;
  use FundTrait;
  use LoanTrait;
  use MiscTrait;
  use OrcaTrait;
  use PledgeTrait;
  use ReportTrait;
  use TransactionTrait;

  /** @var CFDataManager $_dataManager */
  private $_dataManager;

  /** @var FundProcessor  */
  private $fundProcessor;

  /** @var CommitteeManager */
  private $committeeManager;

  private $project_directory = '.';


  private $fileService;

  public function __construct(CFDataManager $dataManager) {
    $this->_dataManager = $dataManager;
    $this->fundProcessor = new FundProcessor($dataManager);
    $this->committeeManager = new CommitteeManager($dataManager);
    $this->project_directory = dirname(__DIR__);
  }

  /**
   * Save any database changes from ORCA directly to the database.
   *
   * @param integer $fund_id
   *   Fund to perform the sync against
   * @param string $payload
   *   An unencoded json string containing the changes that need to be synced.
   * @param float $version
   *   ORca version number being used to perform the sync.
   * @return string
   *   A string representation of the timestamp of the last change pushed to the RDS.
   *
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws ConnectionException
   * @throws \Doctrine\DBAL\Exception
   */
  public function pushChanges($fund_id, $payload, $version=null) {
    $this->_dataManager->beginTransaction();
    $results = $this->_dataManager->db->executeQuery("SELECT private.cf_push_changes(:fund_id, :json, :version)", ['fund_id' => $fund_id, 'json' => $payload, 'version' => $version]);
    $this->_dataManager->endTransaction();
    if ($results) {
      return $results->fetchOne();
    }
    else return NULL;
  }

  /**
   * Save validation data for a fund.
   * @param $fund_id
   * @param $payload
   * @throws \Doctrine\DBAL\Exception
   */
  public function saveValidations($fund_id, $payload) {
    $this->_dataManager->beginTransaction();
    $this->_dataManager->db->executeQuery("SELECT private.cf_save_validations(fund_id, :json) FROM fund where fund_id=:fund_id", ['fund_id' => $fund_id, 'json' => $payload]);
    $this->_dataManager->endTransaction();
  }

  /**
   * Create an orca property from a key/value pair
   * @param string $name
   * @param mixed $value
   * @return stdClass
   */
  public function generateProperty($name, $value) {
    $property = new stdClass();
    $property->name = (string)$name;
    $property->value = !empty($value) ? (string)$value : NULL;
    return $property;
  }

  /**
   * @param $amount
   * @throws Exception
   *
   */
  public function validatePositiveAmount($amount){
    if(empty($amount)){
      throw new Exception('Amount not set.');
    }
    else if(!is_numeric($amount)){
      throw new Exception('Amount must be a number.');
    }
    else if($amount < 0){
      throw new Exception('Amount must be greater than 0.');
    }
  }

  /**
   * @param $functionSignature
   * @param $parameters
   * @return mixed
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function executeJsonFunction($functionSignature, $parameters)
  {
    $data = $this->_dataManager->db->executeQuery("select private.$functionSignature as json", $parameters)->fetchOne();

    return json_decode($data);
  }

  /** @noinspection SqlResolve */
  private function validateTrankeygen($fund_id, $table, $trankeygen_id) {
    if (!is_int($trankeygen_id)) {
      return false;
    }
    try {
      $record = $this->_dataManager->db->executeQuery(
        'select * from '.$table.' x join private.trankeygen t on t.trankeygen_id = x.trankeygen_id where fund_id = :fund_id and x.trankeygen_id = :trankeygen_id',
        ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id])->fetchOne();
    } /** @noinspection PhpUnusedLocalVariableInspection */ catch (\Doctrine\DBAL\Exception $e) {
      return false;
    }
    if (!$record) {
      return false;
    }
    return true;
  }

  /**
   * @param $fund_id
   * @param $id
   * @return stdClass
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function getAttachedPage($fund_id, $id, $target_type) : stdClass
  {
    $response = new stdClass();
    $attachedPage = $this->_dataManager->db->executeQuery("SELECT private.cf_get_attachment_page_data(:fund_id, :target_id, :target_type)", ['fund_id' => $fund_id, 'target_id' => $id, 'target_type' => $target_type])->fetchOne();
    if (!$attachedPage) {
      throw new Exception('Attachment not found');
    }
    $response->attachedPageData = json_decode($attachedPage);
    $response->success = TRUE;
    return $response;
  }

  /**
   * @return stdClass
   * @throws \Doctrine\DBAL\Exception
   */
  public function getCommitteeTreasurers($committee_id) : stdClass{
    $response = new stdClass();
    $treasurers = $this->_dataManager->db->executeQuery('select * from private.cf_get_committee_treasurers(:committee_id) as treasurers',
        ['committee_id' => $committee_id])->fetchAssociative();
    $response->success = true;
    $response->treasurers = $treasurers;
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   */
  public function setDerbyUpdated($fund_id, $update_id): stdClass
  {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;
    $this->_dataManager->db->executeQuery('update private.derby_update set run_date = now() where update_id = :update_id and fund_id = :fund_id', ['update_id' => $update_id, 'fund_id' => $fund_id]);
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function deleteVendorRefund($fund_id, $id): stdClass {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    $vendorRefund = json_decode($this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $id])->fetchOne());
    if(empty($vendorRefund)){
      throw new Exception('Vendor refund not found.');
    }
    $this->_dataManager->db->executeStatement('select private.cf_delete_vendor_refund(:id)', ['id' => $id]);
    $response->success = false;
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function getBackupAPI($fund_id, User $user) {
      $tp = new TokenProcessor();
      $api = new stdClass();
      $api->url = $this->_dataManager->db->fetchOne(
        "SELECT p.value from wapdc_settings p join wapdc_settings stage ON p.stage=stage.value and p.property='campaign_finance_aws_gateway'");
      $claims = [
        'fund_id' => $fund_id,
        'user_name' => $user->user_name ,
        'uid' => $user->uid,
        'realm' => $user->realm,
      ];
      $api->token = $tp->generateToken($claims);
      return $api;
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function getCommitteeAPI($committee_id, User $user){
      $tp = new TokenProcessor();
      $api = new stdClass();
      $api->url = $this->_dataManager->db->fetchOne("SELECT p.value from wapdc_settings p join wapdc_settings stage on p.stage=stage.value and p.property='campaign_finance_aws_gateway'");
      $claims = [
          'committee_id' => $committee_id,
          'user_name' => $user->user_name,
          'uid' => $user->uid,
          'realm' => $user->realm
      ];
      $api->token = $tp->generateToken($claims);
      return $api;
  }

  /**
   * Assembles and returns a secure S3 url to the orcaController
   *
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function getS3BackupUrl($fund_id):String {
    $info = $this->_dataManager->db->executeQuery(
        'SELECT cf.archive_file, f.election_code, c.name  FROM private.campaign_fund cf 
             JOIN fund f on f.fund_id = cf.fund_id 
             JOIN committee c ON f.committee_id=c.committee_id
           WHERE f.fund_id = :fund_id and archive_expiration > NOW()', ['fund_id' => $fund_id])->fetchAssociative();
    if(empty($info)){
        throw new Exception('Archive entry not found in campaign');
    }
    $fileService = new CommercialAWSFileService();
    $stage = ($_ENV['PANTHEON_ENVIRONMENT'] ?? 'dev') === 'live' ? 'prod' : 'demo';
    $fileService->setBucket("campaign-finance-$stage-backup");
    $s3_url = $fileService->assembleS3Url($info['archive_file']);
    $downloadName = $info['name'] . ' ' . $info['election_code']. ' ' .  date('Y-m-d'). '.gz';
    return $fileService->getPubliclyAccessibleURL($s3_url, time() + 60,$downloadName);
  }
}
