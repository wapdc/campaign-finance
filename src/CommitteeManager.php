<?php

namespace WAPDC\CampaignFinance;

use Doctrine\Persistence\Mapping\MappingException;
use \stdClass;
use \SimpleXMLElement;
use \DateTime;
use \Exception;
use Doctrine\ORM\Query\Expr\Join;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\CommitteeAPIAuthorization;
use WAPDC\CampaignFinance\Model\CommitteeAuthorization;
use WAPDC\CampaignFinance\Model\CommitteeContact;
use WAPDC\CampaignFinance\Model\CommitteeLog;
use WAPDC\CampaignFinance\Model\CommitteeRegistrationDraft;
use WAPDC\CampaignFinance\Model\CommitteeRelation;
use WAPDC\CampaignFinance\Model\Fund;
use WAPDC\CampaignFinance\Model\Party;
use WAPDC\CampaignFinance\Model\Proposal;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\Core\Model\Candidacy;
use WAPDC\Core\Model\Jurisdiction;
use WAPDC\Core\Model\Office;
use WAPDC\Core\Model\Person;
use WAPDC\Core\Model\Position;
use WAPDC\Core\Model\User;
use WAPDC\Core\UserManager;
use WAPDC\Core\EntityProcessor;
use WAPDC\Core\CandidacyProcessor;

/**
 * Class CommitteeManager
 * Manages permissions on committees.
 */
class CommitteeManager {

  /**
   * @var CFDataManager
   */
  private const API_KEY_LENGTH = 20;
  private $_dataManager;
  private $_currentUser;

  public function __construct(CFDataManager $dataManager) {
    $this->_dataManager = $dataManager;
  }

  /**
   * Get an authorization object for the user from the database.
   * @param int $committee_id
   *   ID of the committee to retrieve
   * @param string $realm
   *   Realm representing the user authentication source.
   * @param string $uid
   *   ID of the user within the realm
   * @return CommitteeAuthorization
   */
  protected function getAuthorization($committee_id, $realm, $uid) {
    /** @var CommitteeAuthorization $auth */
    $auth = $this->_dataManager->em->getRepository(CommitteeAuthorization::class)
      ->findOneBy(['committee_id' => $committee_id, 'realm' => $realm, 'uid' => $uid]);

    if (!$auth) {
      $auth = new CommitteeAuthorization($committee_id, $realm, $uid);
    }
    return $auth;
  }

  /**
   * Determine if the user provided has a role within the committee
   *
   * @param Committee $committee
   * @param $user
   * @param $role
   * @return bool
   */
  public function userHasRole(Committee $committee, $user, $role) {
    $auth = $this->getAuthorization($committee->committee_id, $user->realm, $user->uid);
    return $auth->role == $role && $auth->approved && (is_null($auth->expiration_date) || $auth->expiration_date > new DateTime());
  }

  // set the current user from outside the class as this one doesn't have access to drupal
  public function setCurrentUser($current_user) {
    $this->_currentUser = $current_user;
  }

  /**
   * Request access for a particular role within a committee.
   * @param Committee $committee
   * @param $user
   * @param $role
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws Exception
   */
  public function requestAccess(Committee $committee, $user, $role) {
    $auth = $this->getAuthorization($committee->committee_id, $user->realm, $user->uid);
    if ($auth->role != $role) {
      $auth->role = "$role";
      $this->_dataManager->em->persist($auth);
      $this->_dataManager->em->flush($auth);
    }
    $this->logCommitteeAction($committee->committee_id, $user->user_name, "Requesting access to committee", "Requesting access to a committee.");
  }

  /**
   * @param Committee $committee
   * @param $user
   * @param $role
   * @throws Exception
   */
  public function grantAccess(Committee $committee, $user, $role, $expiration_date = null, $reason = null) {
    $auth = $this->getAuthorization($committee->committee_id, $user->realm, $user->uid);
    $auth->role = "$role";
    $auth->granted_date = new \DateTime();
    $auth->expiration_date = $expiration_date;
    $auth->approved = TRUE;
    $this->_dataManager->em->persist($auth);
    $this->_dataManager->em->flush($auth);

    if ($committee->person_id) {
      $this->_dataManager->db->executeStatement("insert into entity_authorization(entity_id, realm, role, uid) 
          VALUES (:entity_id, :realm, :role, :uid) on conflict do nothing",
        [
          'entity_id' => $committee->person_id,
          'realm' => $user->realm,
          'uid' => $user->uid,
          'role' => 'cf_reporter',
        ]
      );
    }

    $message = 'Granting access to ' . $user->user_name;

    $this->logCommitteeAction(
      $committee->committee_id,
      ($this->_currentUser === null) ? 'unknown' : $this->_currentUser->user_name,
      'Granting access to a committee',
      ($reason === null) ? $message : $message . ('. Reason being: ' . $reason),
    );
  }

  /**
   * @param $committee_id
   * @param $user
   * @param $role
   * @throws Exception
   */
  public function grantAccessByCommitteeId($committee_id, $user, $role, $reason = null) {
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    if ($committee){
      $this->grantAccess($committee, $user, $role, null, $reason);
    }
  }

 /**
  * Update the entity table to have the security of the committee.
  *
  * @param $entity_id
  * @throws \Doctrine\DBAL\Exception
  */
  public function synchronizeEntityAccess($entity_id) {
    $this->_dataManager->db->executeStatement("INSERT INTO entity_authorization(entity_id, uid, realm, role) 
       select distinct person_id, uid, realm, 'cf_reporter' from committee_authorization ca join committee c on c.committee_id=ca.committee_id where person_id=:entity_id
       on conflict do nothing",
    ['entity_id' => $entity_id]);
  }

  /**
   * @param Committee $committee
   * @param $user
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws Exception
   */
  public function revokeAllAccess(Committee $committee, $user) {
    $auth = $this->_dataManager->em->getRepository(CommitteeAuthorization::class)
      ->findOneBy(['committee_id' => $committee->committee_id, 'realm' => $user->realm, 'uid' => $user->uid]);
    if ($auth) {
        $today = new DateTime();
        $auth->expiration_date = $today;
        $this->_dataManager->em->flush($auth);
    }

    $this->logCommitteeAction(
      $committee->committee_id,
      ($this->_currentUser === null) ? 'unkown' : $this->_currentUser->user_name,
      'Revoking access to a committee',
      'Revoked access to ' . $user->user_name
    );
  }

  /**
   * @param $committee_id
   * @param $user
   * @throws Exception
   */
  public function revokeAllAccessByCommitteeId($committee_id, $user) {
    /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    if ($committee){
      $this->revokeAllAccess($committee, $user);
    }
  }

  /**
   * @param $realm
   * @param $user_id
   * @param $email
   * @param $user_name
   * @return null|object
   * @throws Exception
   */
  public function getUser($realm, $user_id, $email, $user_name){
    if ($realm == null){
      throw new Exception("Realm should not be empty");
    }
    if ($user_id == null ){
      throw new Exception("User ID should not be empty");
    }
    $userManager = new UserManager($this->_dataManager, $realm);
    $user = $userManager->getRegisteredUser($user_id);
    if (!$user && $user_name!=null){
      $userManager->registerUser($user_id, $user_name, $email);
      $user = $userManager->getRegisteredUser($user_id);
    }
    return $user;
  }

  /**
   * @param $committee_id
   * @return Committee
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function getCommittee($committee_id) {
    /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    return $committee;
  }

  /**
   * Retrieve general information about a committee sufficient for routing around through the application
   * @param $commmittee_id
   * @return stdClass
   *   Representation of committee data with
   */
  public function getCommitteeFinanceInfo($commmittee_id) {
    $data = $this->_dataManager->db->fetchOne(
      "select row_to_json(j) 
                 from (
                   select 
                       c.start_year, c.committee_id, c.name, e.title as election, c.filer_id, 
                       c.pac_type, c.committee_type, c.continuing, fi.funds, c.registered_at
          from committee c left join election e on e.election_code=c.election_code
             left join (
               select 
                   f.committee_id,  
                   json_agg(
                     json_build_object('election_code', f.election_code, 'vendor', f.vendor, 'orcaVersion', cf.version, 'fund_id', f.fund_id, 'restore_request_id', rr.restore_request_id, 'updated', case when cf.version > 1.5 then cf.updated else p.value::date end) 
                     order by f.election_code desc  
                   ) funds
                 from fund f 
                   left join private.campaign_fund cf on cf.fund_id=f.fund_id
                   left join private.restore_request rr on rr.fund_id = f.fund_id  
                   left join private.properties p on p.fund_id = f.fund_id
                   and p.name = 'DERBY_TO_RDS'
                 where f.committee_id = :committee_id
                 group by f.committee_id
             ) fi on fi.committee_id = c.committee_id
          where c.committee_id=:committee_id ) j",
      ['committee_id' => $commmittee_id],
    );
    return $data ? json_decode($data) : new stdClass();
  }

  public function getCommitteeByFund($fund_id) {
    $committee = null;
    /** @var Fund $fund */
    $fund = $this->_dataManager->em->find(Fund::class, $fund_id);
    if ($fund && $fund->committee_id) {
      $committee = $this->_dataManager->em->find(Committee::class, $fund->committee_id);
    }
    return $committee;
  }

  /**
   * @param $committee_id
   * @param $user_name
   * @throws Exception
   */
  public function deleteCommittee($committee_id, $user_name) {
    $committee = $this->getCommittee($committee_id);
    $this->_dataManager->em->remove($committee);
    $this->_dataManager->em->flush();
    $this->logCommitteeAction($committee_id, $user_name, "Committee deletion", "Deleting a committee associated with the removed draft.");
  }

  /**
   * @param $data
   * @param $user
   * @return Committee
   * @throws Exception
   */
  public function createCommitteeFromData($data, $user) {
    if (!$user){
      throw new Exception("User should not be null");
    }
    $name = $data['name'];
    if (!$name) {
      throw new Exception("Committee name should not be null");
    }
    $committee_type = $data['committee_type'];
    $pac_type = $data['pac_type'];
    if (!empty($data['category'])) {
      $category = $data['category'];
    }
    if ($committee_type == 'CA') {
      if (!in_array($pac_type, array('candidate', 'surplus'))) {
        throw new Exception("Expected PAC type of 'candidate', 'surplus'");
      }
      if (empty($data['election_code']) && $pac_type == 'candidate') {
        throw new Exception("Expected election code for PAC type of 'candidate'");
      }
    }
    elseif($committee_type == 'CO'){
      if (!in_array($pac_type, array('bonafide', 'caucus', 'pac'))) {
        throw new Exception("Expected PAC type of 'bonafide', 'caucus' or 'pac'");
      }
      if (!empty($data['election_code']) && $pac_type != 'pac') {
        throw new Exception("No election code should be set for committee type $pac_type");
      }
    }
    else {
      throw new Exception("Expected committee type of 'CA' or 'CO'");
    }
    $bonafide_type = NULL;
    $exempt = NULL;
    if (in_array($pac_type, array('bonafide', 'caucus'))) {
      $bonafide_type = $data['bonafide_type'];
      $data_pac_type = $data['pac_type'];
      if (!in_array($bonafide_type, array('state', 'county', 'district')) && !in_array($data_pac_type, array('caucus'))) {
        throw new Exception("Expected bonafide type of 'state', 'county', 'district'");
      }
      if (!empty($data['exempt'])) {
        $exempt = true;
      }
    }
    $committee = new Committee($name);
    $committee->committee_type = $committee_type;
    $committee->pac_type = $pac_type;
    if (!empty($category)) {
      $committee->category = $category;
    }
    $committee->bonafide_type = $bonafide_type;
    $committee->exempt = $exempt;
    $election_code = $data['election_code'] ?? null;
    if ($election_code) {
      $committee->election_code = $election_code;
      $committee->start_year = (int)$election_code;
      $committee->end_year = (int)$election_code;
      $committee->continuing = false;
    }
    else {
      $create_date = new DateTime();
      $committee->start_year = (int)$create_date->format('Y');
      $committee->continuing = true;
    }


    // Check to make sure we have an election.
    if ($committee->pac_type == 'candidate') {
      if (!$election_code) {
        throw new Exception("An election is required for this type of registration");
      }
      $committee->election_code = $election_code;
    }

    $this->_dataManager->em->persist($committee);
    $this->_dataManager->em->flush($committee);
    $newDraft = new CommitteeRegistrationDraft($committee->committee_id);
    $newDraft->setRegistrationData($this->getCommitteeSubmission($committee->committee_id));
    $regData =  $newDraft->getRegistrationData();
    if ($committee_type == 'CA' && empty($regData->committee->candidacy->person)){
      if(property_exists($regData->committee, "candidacy")){
        $regData->committee->candidacy->person = $committee->name;
      }
      $newDraft->setRegistrationData($regData);
    }
    $this->_dataManager->em->persist($newDraft);
    $this->_dataManager->em->flush();
    $this->grantAccess($committee, $user, 'owner');
    return $committee;
  }


  /**
   * Get the XML document that represents a registration.
   *
   * @param int $registration_id
   *   ID of the registration to retrieve
   * @return null|\SimpleXMLElement
   *   XML representing the registration detail.
   *
   * @throws Exception
   */
  public function getRegistrationXml($registration_id){
  	/** @var Registration $registration */
		$registration = $this->_dataManager->em->find(Registration::class, $registration_id);
		if($registration){
			$xml = new SimpleXMLElement($registration->user_data);
			$xml->committee['id'] = $registration->committee_id;
      $xml->committee['root_committee_name'] = $this->_dataManager->db->executeQuery('SELECT name FROM committee WHERE committee_id=?', [$registration->committee_id])->fetchOne();
      $xml->addChild('registration');
			$xml->registration['id'] = $registration->registration_id;
      $xml->registration['name'] = $registration->name;
      $xml->registration['submitted'] = $registration->submitted_at ? $registration->submitted_at->format('Y-m-d') : NULL;
			$xml->registration['source'] = $registration->source;
			$xml->registration['version'] = $registration->version;
			$xml->registration['verified'] = $registration->verified;
			return $xml;
		}else{
			return NULL;
		}
	}

  /**
   * Move a property from an orm object to the stdClass variant
   * @param $object1
   * @param $object2
   * @param $property
   */
	private function _transferPropertyToOrm($object1, $object2, $property) {
    if (isset($object1->$property) && !is_object($object1->$property)) {
      if (empty($object1->$property) && $object1->$property!==0) {
        $object2->$property = NULL;
      }
      else {
        $object2->$property = trim($object1->$property);
      }
    }
  }

  /**
   * Transfers an orm property onto a standard object.
   */
  private function _transferPropertyFromOrm($object1, $object2, $property) {
    $value = $object1->$property;
    switch (TRUE) {
      case (is_object($value) && ($value instanceof DateTime)):
        $object2->$property = $value->format('Y-m-d H:i:s');
        break;
      default:
        $object2->$property = $value;
    }
  }

  /**
   * @param $jurisdiction_id
   * @param $proposal_type
   * @param null $number
   * @param null $title
   * @param null $election_date
   * @return Proposal
   */
  private function _createProposal($jurisdiction_id, $proposal_type, $number=NULL, $title=NULL, $election_code=NULL) {
    $proposals = $this->_dataManager->em->getRepository(Proposal::class);
    $filter = [];
    $filter['jurisdiction_id'] = $jurisdiction_id ?? NULL;
    $filter['proposal_type'] = $proposal_type;
    $filter['number'] = $number ?? NULL;
    $filter['title'] = $title ?? NULL;
    $filter['election_code'] = $election_code ?? NULL;
    $p = $proposals->findOneBy($filter);

    if (!$p) {
      $p = new Proposal();
      $p->jurisdiction_id = $jurisdiction_id ?? NULL;
      $p->proposal_type = $proposal_type;
      $p->number = $number ?? NULL;
      $p->title = $title ?? NULL;
      $p->election_code = $election_code ?? NULL;
      $this->_dataManager->em->persist($p);
    }
    return $p;
  }

  /**
   * Given a committee, delete all the relations for the committee
   * that are from the current year, the future, or don't have a year
   *
   * @param $singleElection
   * @param $committee
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  private function _deleteRelations($singleElection, $committee) {
    $committee_id = $committee->committee_id;
    //for single election committees delete all old relations
    $relations = $this->_dataManager->em->getRepository(CommitteeRelation::class)->findBy(['committee_id' => $committee_id]);
    if ($singleElection){
      foreach($relations as $relation){
        $this->_dataManager->em->remove($relation);
      }
    }
    //for continuing elections delete proposals from present or future years and any old relation that is not a proposal
    foreach($relations as $relation){
      if ($relation->relation_type == 'proposal'){
        $proposal = null;
        if ($relation->target_id) {
          $proposal = $this->_dataManager->em->find(Proposal::class, $relation->target_id);
        }
        if (!$proposal || date('Y') <= $proposal->election_code){
          $this->_dataManager->em->remove($relation);
        }
      }
      else {
        $this->_dataManager->em->remove($relation);
      }
    }
    $this->_dataManager->em->flush();
  }

  /**
   * Save a relation, translating from submission data.
   *
   * @param Committee $committee
   * @param $relation_type
   * @param stdClass $relation_data
   *
   * @throws Exception
   *
   */
  private function _saveRelation(Committee $committee, $relation_type, stdClass $relation_data) {
    // sanity checks
    if (!empty($relation_data->proposal_issue) && strlen($relation_data->proposal_issue) > 255) {
      throw new Exception('Proposal description is greater than 255 characters. A bit more brevity here is appreciated.');
    }
    /** @var CommitteeRelation $relation */
    $relation = NULL;
    if (!empty($relation_data->relation_id)) {
      $relation = $this->_dataManager->em->find(CommitteeRelation::class, $relation_data->relation_id);
    }
    if (!$relation) {
      $relation  = new CommitteeRelation($committee->committee_id, $relation_type);
    }

    $relation->relation_type = $relation_type;
    switch ($relation_type) {
      case "candidacy":
        $relation->stance = $relation_data->stance ?? 'for';
        if (!empty($relation_data->candidacy_id)) {
          $relation->target_id = $relation_data->candidacy_id;
        }
        $relation->target_name = $relation_data->person . ' for ' . $relation_data->office;
        break;
      case "proposal":
        $relation->stance = $relation_data->stance;
        if (!empty($relation_data->proposal_id)) {
          $relation->target_id = $relation_data->proposal_id;
        }
        else {
          if ($relation_data->jurisdiction_id) {
            $proposal = $this->_createProposal(
              $relation_data->jurisdiction_id ?? NULL,
              $relation_data->proposal_type,
              $relation_data->ballot_number ?? NULL,
              $relation_data->proposal_issue ?? "",
              $relation_data->election_code ?? $committee->election_code
            );
            $relation->target_id = $proposal->proposal_id;
          }
          else {
            $relation->target_name = $relation->jurisdiction . " - " . $relation->proposal_issue;
          }
        }
        break;
      case "committee":
        if (!empty($relation_data->affiliate_id)){
          $relation->target_id = $relation_data->affiliate_id;
        }
        else {
          $relation->target_name = $relation_data->affiliate_name;
        }
        break;
      case "ticket":
        $relation->target_name = $relation_data->name;
        $relation->target_id = !empty($relation_data->party_id) ? $relation_data->party_id: null;
        $relation->stance = $relation_data->stance;
    }
    $relation->committee_id = $committee->committee_id;
    $relation->updated_at = new DateTime();
    $this->_dataManager->em->persist($relation);
  }

  /**
   * Get all relations for a committee data.
   *
   * @param Committee $committee
   * @param stdClass $committee_data
   * @throws Exception
   */
  private function _getRelations(Committee $committee, stdClass $committee_data) {
    /** @var CommitteeRelation[] $relations */
    $relations = $this->_dataManager->em->getRepository(CommitteeRelation::class)
      ->findBy(['committee_id' => $committee->committee_id]);

    foreach ($relations as $relation) {
      switch ($relation->relation_type) {
        case "proposal":
          /** @var Proposal $proposal */
          $proposal = NULL;
          if (!empty($relation->target_id)) {
            $proposal = $this->_dataManager->em->find(Proposal::class, $relation->target_id);
          }
          if ($proposal) {
            $committee_data->proposals[] = $p = new stdClass();
            $p->proposal_id =  $proposal->proposal_id;
            $p->proposal_issue = $proposal->title;
            $p->jurisdiction_id = $proposal->jurisdiction_id;
            $p->ballot_number = $proposal->number;
            $p->proposal_type = $proposal->proposal_type;
            $p->election_code = $proposal->election_code;
            $p->stance = $relation->stance;
          }
          break;
        case "candidacy":
          if ($committee->committee_type != "CA") {
            $committee_data->candidates[] = $c = new stdClass();
            [$person, $office] = explode(' for ', $relation->target_name);
            $c->person = $person;
            $c->office = $office;
            $c->stance = $relation->stance;
          }
          break;
        case "committee":
          $affiliatedCommittee = null;
          $committee_data->affiliations[] = $a = new stdClass();

          if (!empty($relation->target_id)) {
            $affiliatedCommittee = $this->_dataManager->em->find(Committee::class, $relation->target_id);
          }
          if ($affiliatedCommittee) {
            $a->affiliate_id = $affiliatedCommittee->committee_id;
            $a->affiliate_name = $affiliatedCommittee->name;
          }
          else {
            $a->affiliate_name = $relation->target_name;
          }
          break;
        case "ticket":
          $party = null;
          $committee_data->ticket = $p = new stdClass();
          if (!empty($relation->target_id)) {
            $party = $this->_dataManager->em->find(Party::class, $relation->target_id);
          }
          if ($party) {
            $p->party_id = $party->party_id;
            $p->name = $party->name;
            $p->stance = $relation->stance;
          }
          break;
      }
    }
  }

  /**
   * Given a committee, delete all the officers for the committee
   *
   * @param Committee $committee
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  private function _deleteOfficers(Committee $committee) {
    $committee_id = $committee->committee_id;
    $officers = $this->_dataManager->em->getRepository(CommitteeContact::class)->findBy(['committee_id' => $committee_id, 'role' => 'officer']);
    foreach($officers as $officer){
      $this->_dataManager->em->remove($officer);
    }
    $this->_dataManager->em->flush();
  }

  /**
   * Save contact information from submission
   * @param Committee $committee
   *   Committee on which to save a contact.
   * @param stdClass $contact_data
   *   The contact data
   * @param string $single_role
   *   Single role is used for books or committee contacts
   *
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException]
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
 private function _saveContact(Committee $committee, stdClass $contact_data, $single_role='') {
    // contact data and copy ballot_name to name before saving candidacy contact
    $contact_clone = clone($contact_data);
    // We need to make sure that candidacy records in committee_contact always contain a name and telephone number.
    if ($single_role === 'candidacy' && empty($contact_clone->name)) {
      if ($contact_clone->ballot_name) {
        $contact_clone->name = $contact_clone->ballot_name;
      }
      else {
        $contact_clone->name = $this->_dataManager->db->executeQuery(
          "select name from entity where entity_id = :entity_id",
          ['entity_id' => $committee->person_id]
        )->fetchOne();
      }
    }

    /** @var CommitteeContact $contact */
    $contact = null;
    if ($single_role) {
      $contact = $this->_dataManager->em->getRepository(CommitteeContact::class)
        ->findOneBy(['committee_id' => $committee->committee_id, 'role' => $single_role]);
    }
    elseif(!empty($contact_clone->contact_id)) {
      $contact = $this->_dataManager->em->find(CommitteeContact::class, $contact_clone->contact_id);
    }
    if (!$contact) $contact = new CommitteeContact();

    foreach (get_object_vars($contact) as $property => $value) {
      switch ($property) {
        // Ignore contact id sets because they cannot be changed by submissions
        case 'contact_id':
          break;
        default:
          $this->_transferPropertyToOrm($contact_clone, $contact, $property);
      }
    }
    $contact->committee_id = $committee->committee_id;
    if ($single_role) {
      $contact->role = $single_role;
    }
    $this->_dataManager->em->persist($contact);
  }

  /**
   * Retrieve data for a contact that has only one occurance for a committee.
   *
   * Used for both books contact as well as primary committee contact.
   * @param Committee $committee
   * @param string $role
   * @return stdClass
   */
  private function _getSingleContactData(Committee $committee, $role) {
    $contact = $this->_dataManager->em->getRepository(CommitteeContact::class)
      ->findOneBy(['committee_id' => $committee->committee_id, 'role' => $role]);
    if (!$contact) {
      $contact = new CommitteeContact();
    }

    $contact_data = new stdClass();
    foreach(get_object_vars($contact) as $property => $value) {
      $this->_transferPropertyFromOrm($contact, $contact_data, $property);
    }
    return $contact_data;
  }

  /**
   * Return the data for the committee officers.
   *
   * @param Committee $committee
   * @return stdClass[]
   */
  private function _getOfficerData(Committee $committee) {
    /** @var CommitteeContact[] $contacts */
    $data = [];
    $contacts = $this->_dataManager->em->getRepository(CommitteeContact::class)
      ->findBy(['committee_id' => $committee->committee_id], ['contact_id' => 'asc']);
    foreach ($contacts as $contact) {
      if ($contact->role == 'officer') {
        $contact_data = new stdClass();
        foreach (get_object_vars($contact) as $property => $value) {
          $this->_transferPropertyFromOrm($contact, $contact_data, $property);
        }
        $data[] = $contact_data;
      }
    }
    return $data;
  }


  /**
   * Create a candidacy
   * @param Committee $committee ;
   * @param stdClass $data ;
   * @throws Exception*@throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  private function _saveCandidacy(Committee $committee, stdClass $data) {
    // trim some input
    $data->ballot_name = !empty($data->ballot_name) ? trim($data->ballot_name) : null;
    $data->person = !empty($data->person) ? trim($data->person) : null;

    $rows = $this->_dataManager->db->executeQuery('select * from candidacy where committee_id = :committee_id', ['committee_id' => $committee->committee_id])
      ->fetchAllAssociative();
    if ($rows) {
      $candidacy = new stdClass();
      $existing_candidacy = $rows[0];

      /*
       * If the candidacy was made in error and amended to reflect the appropriate candidacy and there is an existing committee and there is an existing candidacy with sos_data we need to delete the current candidacy being amended and move committee to the right candidacy
       */
      $sos_existing_candidacy = $this->_dataManager->db->executeQuery('
                                select * from candidacy 
                                where jurisdiction_id = :jurisdiction_id 
                                  and office_code = :office_code
                                  and person_id = :person_id 
                                  and election_code = :election_code
                                  and sos_candidate_code is not null
                                  and candidacy_id <> :candidacy_id',
                ['jurisdiction_id' => $data->jurisdiction_id ?? $existing_candidacy['jurisdiction_id'],
                 'office_code' => $data->offcode ?? ($data->office_code ?? $existing_candidacy['office_code']),
                 'person_id' => $existing_candidacy['person_id'],
                 'election_code' => $existing_candidacy['election_code'],
                 'candidacy_id' => $existing_candidacy['candidacy_id']])
            ->fetchAssociative();
      if ($sos_existing_candidacy) {
          $candidacy->candidacy_id = $sos_existing_candidacy['candidacy_id'];
          //Update the name if changed
          $candidacy->ballot_name = $data->ballot_name ?? $sos_existing_candidacy['ballot_name'];
          //Attach the committee that was causing this case
          $candidacy->committee_id = $existing_candidacy['committee_id'];
          //campaign start date should be set to earliest value
          $candidacy->campaign_start_date = min($existing_candidacy['campaign_start_date'], $sos_existing_candidacy['campaign_start_date']);
          //delete the candidacy that isn't needed
          $this->_dataManager->db->executeQuery('delete from candidacy 
          where candidacy_id = :candidacy_id'
              , ['candidacy_id' => $existing_candidacy['candidacy_id']]);
      } else {
          // If we have an sos_candidate_code we should assume that office_code and jurisdiction_id are immutable
          // so we need to load the from the existing record.  Otherwise we can carry forward this data.
          $candidacy->candidacy_id = $existing_candidacy['candidacy_id'];
          $candidacy->election_code = $existing_candidacy['election_code'];
          $candidacy->person_id = $existing_candidacy['person_id'];
          if (empty($existing_candidacy->sos_candidate_code)) {
              $candidacy->office_code = $data->offcode ?? ($data->office_code ?? $existing_candidacy['office_code']);
              $candidacy->jurisdiction_id = $data->jurisdiction_id ?? $existing_candidacy['jurisdiction_id'];
              $candidacy->position_id = $data->position_id ?? NULL;
          }
          else {
              $candidacy->office_code = $existing_candidacy['office_code'];
              $candidacy->jurisdiction_id = $existing_candidacy['jurisdiction_id'];
              $candidacy->position_id = $existing_candidacy['position_id'];
          }
      }
    }

    if (empty($candidacy)) {

      /** @var Office $office */
      if (!empty($data->office_code)) {
        $office = $this->_dataManager->em->find(Office::class, $data->office_code);
        if (empty($office)) {
          throw new Exception('Invalid Office Code: ' . $data->office_code);
        }
      } else if (!empty($data->office)) {
        $office = $this->_dataManager->em->createQuery("select o from " . Office::class . " o where lower(o.title)=lower(:office)")
          ->setParameter('office', $data->office)
          ->getOneOrNullResult();
        if (empty($office)) {
          throw new Exception('Invalid Office Title: ' . $data->office);
        }
      }

      /** @var Jurisdiction $jurisdiction */
      if (!empty($data->jurisdiction_id)) {
        $jurisdiction = $this->_dataManager->em->find(Jurisdiction::class, $data->jurisdiction_id);
        if (empty($jurisdiction)) {
          throw new Exception("Invalid Jurisdiction ID: " . $data->jurisdiction_id);
        }
      }
      elseif (!empty($data->jurisdiction)) {
        $jurisdiction = $this->_dataManager->em->createQuery("select j from " . Jurisdiction::class . " j where lower(j.name)=lower(:name)")
          ->setParameter('name', $data->jurisdiction)
          ->getOneOrNullResult();
        if (empty($jurisdiction)) {
          throw new Exception("Invalid Jurisdiction: " . $data->jurisdiction);
        }
      }

      $c = $this->_dataManager->db->executeQuery('select * from candidacy where person_id = :person_id and jurisdiction_id = :jurisdiction_id and office_code = :office_code and election_code = :election_code',
        ['person_id' => $committee->person_id, 'jurisdiction_id' => $jurisdiction->jurisdiction_id, 'office_code' => $office->offcode, 'election_code' => $committee->election_code])->fetchAssociative();
      $person_id = $committee->person_id;
      $candidacy = $c ? (object)$c : null;
      if (empty($candidacy)) {
        $candidacy = new StdClass();
        $candidacy->person_id = $person_id;
        $candidacy->jurisdiction_id = $jurisdiction->jurisdiction_id;
        $candidacy->office_code = $office->offcode;
        $candidacy->election_code = $committee->election_code;
      }
    }

    /** @var Party $party */
    if (!empty($data->party_id)) {
      $party = $this->_dataManager->em->find(Party::class, $data->party_id);
    }
    if (empty($party) && !empty($data->party)) {
      $party = $this->_dataManager->em->createQuery("select p from " . Party::class . " p where lower(p.name)=lower(:name)")
        ->setParameter('name', str_replace("-", " ", $data->party))
        ->getOneOrNullResult();
    }
    if (empty($party) && (!empty($data->party_id) || !empty($data->party))) {
      $party = $this->_dataManager->em
        ->getRepository(Party::class)
        ->findOneBy(['name' => "OTHER"]);
        // perhaps we should allow the admin user to map the party when not auto-mapped?
    }
    $candidacy->party_id = $party->party_id ?? null;

    /** @var Position $position_id */
    if (!empty($data->position_id)) {
      $position = $this->_dataManager->em->find(Position::class, $data->position_id);
    }
    if (empty($position) && !empty($data->position)) {
      $position = $this->_dataManager->em->createQuery("select p from " . Position::class . " p where lower(p.title)=lower(:title)")
        ->setParameter('title', $data->position)
        ->getOneOrNullResult();
    }
    $candidacy->position_id = $position->position_id ?? null;
    // trim currently-set ballot name for easier testing
    $candidacy->ballot_name = !empty($candidacy->ballot_name) ? trim($candidacy->ballot_name) : null;
    // prefer existing ballot name, which is updated by the SOS declarations process, but make sure it is non-blank
    $candidacy->ballot_name = $candidacy->ballot_name ?: $data->ballot_name ?: $data->person ?: $committee->name;
    $candidacy->address = $data->address ?? null;
    $candidacy->city = $data->city ?? null;
    $candidacy->state = $data->state ?? null;
    $candidacy->postcode = $data->postcode ?? null;
    $candidacy->committee_id = $committee->committee_id;
    $candidacy->filer_id = $committee->filer_id;
    $candidacy->updated_at = date('Y-m-d H:i:m');
    if ($committee->registration_id) {
      $registration = $this->_dataManager->em->find(Registration::class, $committee->registration_id);
      if (!$registration) {
        throw new Exception('Invalid registration_id on committee');
      }
      if (!empty($candidacy->campaign_start_date)) {
        $candidacy->campaign_start_date = min($candidacy->campaign_start_date, $registration->submitted_at);
      }
      else {
        $candidacy->campaign_start_date = $registration->submitted_at->format('Y-m-d H:i:s');
      }
    }
    $cp = new CandidacyProcessor($this->_dataManager);
    $data->person_id = $committee->person_id;
    $data->candidacy_id = $cp->saveCandidacy($candidacy, false, true);
  }

  /**
   * Populate a committee record from a submission.
   *
   * @param stdClass $submission
   * @param boolean $migration
   *   When true assume we are in migration mode, so we're going to premark the c1 records as created
   *   so that we do not update C1 records for verified registration.
   * @return bool
   * @throws Exception
   */
	public function saveCommitteeFromSubmission(stdClass $submission, $migration=FALSE) {
	  $entityProcessor = new EntityProcessor($this->_dataManager);
    $committee_data = $submission->committee;
    if (isset($submission->registration_id)) {
      $committee_data->registration_id = $submission->registration_id;
    }
    /** @var Committee $committee */
    $committee = null;

    // If we were passed a committee id find it so we can update it.
    if (!empty($committee_data->committee_id)) {
      $committee = $this->_dataManager->em->find(Committee::class, $committee_data->committee_id);
    }

    if (!$committee) {
      $committee = new Committee($committee_data->name, $committee_data->committee_type);
      $this->_dataManager->em->persist($committee);
    }

    // Transfer allowable properties from the object to the committee
    foreach(get_object_vars($committee) as $property => $value) {

      // Only set the properties that are set in the submission so we can do partial updates.
      if (isset($committee_data->$property)) {
        switch($property) {
          // Ignore these fields because they should not be settable directly from a submission.
          case "committee_id":
          case "submitted_at":
          case "registered_at":
          case "c1_sync":
          case "verified":
          case "party":
          // Ignore this field because we aren't going to use the old updated_at date
          case "updated_at":
            break;
          default:
            $this->_transferPropertyToOrm($committee_data, $committee, $property);
        }

      }
    }

    // Check to make sure we have a person_id on the committee pointing to the organizational enity
    if (!$committee->person_id) {
      $committee->person_id = $submission->admin_data->entity_id ?? null;
    }

    // Create person record for committee if it does not exist.
    if (!$committee->person_id) {
      $entity_info = new stdClass();
      $entity_info->is_person = $committee->committee_type === 'CA';
      $entity_info->name = $committee->name;
      if ($entity_info->is_person && !empty($committee_data->candidacy)) {
        if (empty($committee_data->candidacy->person)) {
          throw new Exception('Person name is empty. Please contact IT to research this registration.');
        }
        $entity_info->name = $committee_data->candidacy->person;
        $entity_info->email = $committee_data->candidacy->email;
      }
      else {
        $entity_info->address = $committee_data->contact->address;
        $entity_info->city = $committee_data->contact->city;
        $entity_info->state = $committee_data->contact->state;
        $entity_info->postcode = $committee_data->contact->postcode;
        $entity_info->email = $committee_data->contact->email;
      }
      $entity_info->filer_id = $committee->filer_id;
      $committee->person_id = $entityProcessor->saveEntity($entity_info);
    }

    $committee_data->person_id = $committee->person_id;

    $party = null;
    //If the party_id is set, set the committee party
    if (in_array($committee_data->pac_type, ['bonafide', 'caucus', 'pac'])) {
      /** @var Party $party */
      if (!empty($committee_data->party_id)) {
        $party = $this->_dataManager->em->find(Party::class, $committee_data->party_id);
      }
      if (empty($party) && !empty($committee_data->party)) {
        $party = $this->_dataManager->em->createQuery("select p from " . Party::class . " p where lower(p.name)=lower(:name)")
          ->setParameter('name', str_replace("-", " ", $committee_data->party))
          ->getOneOrNullResult();
      }
      if (empty($party) && (!empty($committee_data->party_id) || !empty($committee_data->party))) {
        $party = $this->_dataManager->em
          ->getRepository(Party::class)
          ->findOneBy(['name' => "OTHER"]);
      }
    }

    if (!empty($party->party_id)) {
      $committee->party = $party->party_id;
    }

    // If the committee is continuing, set committee end year to null.
    if ((isset($committee_data->continuing) && $committee_data->continuing)){
      $committee->end_year = null;
    }

    $jurisdiction_id = null;
    if (!empty($committee_data->proposals[0]->jurisdiction_id) && count($committee_data->proposals) == 1) {
      $jurisdiction_id = $committee_data->proposals[0]->jurisdiction_id;
    }

    // If the committee has no category set it, otherwise leave what has been set in case of manual updates
    if (empty($committee->category)) {
      $sql = 'select expected_committee_category(:pac_type, :bonafide_type, :party_id, :jurisdiction_id, :continuing)';
      $params = [
        'pac_type' => $committee->pac_type,
        'bonafide_type' => $committee->bonafide_type,
        'party_id' => $committee->party,
        'jurisdiction_id' => $jurisdiction_id,
        'continuing' => ($committee->continuing) ? 1 : 0
      ];
      $result = $this->_dataManager->db->executeQuery($sql, $params);
      $committee->category = $result->fetchOne();
    }

    // Save the modified committees.
    $committee->updated_at = new DateTime();
    if ($migration) {
      $committee->c1_sync = $committee->updated_at;
    }
    if (empty($committee->registered_at) && $committee->registration_id) {
      /** @var Registration $registration */
      $registration = $this->_dataManager->em->find(Registration::class, $committee->registration_id);
      if (empty($registration)) {
        throw new Exception("Missing Registration: $submission->registration_id");
      }
      $committee->registered_at = $registration->submitted_at;
    }

    $this->_dataManager->em->flush($committee);
    $committee_data->committee_id = $committee->committee_id;

    //delete officers from last registration if they exist
      $this->_deleteOfficers($committee);

    // Save the committee contact
    if (isset($committee_data->contact)) {
      $this->_saveContact($committee, $committee_data->contact, 'committee');
    }

    if (isset($committee_data->bank)) {
      $this->_saveContact($committee, $committee_data->bank, 'bank');
    }

    if (isset($committee_data->books_contact)) {
      $this->_saveContact($committee, $committee_data->books_contact, 'books');
    }

    // Save other officers
    if (isset($committee_data->officers)) {
      foreach($committee_data->officers as $officer) {
        $this->_saveContact($committee, $officer);
      }
    }

    $singleElection = isset($committee_data->election_code) || $committee_data->start_year == $committee_data->end_year || empty($committee_data->continuing);
    //delete relations from last registration if they exist
    $this->_deleteRelations($singleElection, $committee);

    // Save proposals initiatives and referendum.
    if (isset($committee_data->proposals)) {
      foreach ($committee_data->proposals as $proposal) {
        $this->_saveRelation($committee, 'proposal', $proposal);
      }
    }

    if (!empty($committee_data->candidacy) && $committee->pac_type == 'surplus') {
      $this->_saveContact($committee, $committee_data->candidacy , 'candidacy');
    }

    if (!empty($committee_data->candidacy) && $committee->pac_type == 'candidate') {
      // cloning to avoid modifying the submission data so tests that compare objects will pass
      $candidacy_data = clone $committee_data->candidacy;
      $candidacy_data->address = $committee_data->contact->address ?? null;
      $candidacy_data->city = $committee_data->contact->city ?? null;
      $candidacy_data->state = $committee_data->contact->state ?? null;
      $candidacy_data->postcode = $committee_data->contact->postcode ?? null;
      $candidacy_data->phone = $committee_data->contact->phone ?? null;
      $this->_saveContact($committee, $candidacy_data , 'candidacy');
      $this->_saveCandidacy($committee, $candidacy_data);
      $committee_data->candidacy->person_id = $candidacy_data->person_id;
      $committee_data->candidacy->candidacy_id = $candidacy_data->candidacy_id;
      $this->_saveRelation($committee, 'candidacy', $candidacy_data);
    }

    // Save Candidates
    if (isset($committee_data->candidates) && is_array($committee_data->candidates)) {
      foreach ($committee_data->candidates as $candidate) {
        $this->_saveRelation($committee, 'candidacy', $candidate);
      }
    }

    // Save Party Ticket
    if (!empty($committee_data->ticket) && $committee_data->ticket->name && $committee_data->ticket->stance) {
      $this->_saveRelation($committee, 'ticket', $committee_data->ticket);
    }

    //Save Committee Affiliations
    if (isset($committee_data->affiliations) && !empty($committee_data->affiliations)){
      foreach($committee_data->affiliations as $affiliate) {
        $this->_saveRelation($committee, 'committee', $affiliate);
      }
    }

    $this->_dataManager->em->flush();
    return TRUE;
  }

  /**
   * @param $filerId
   * @param $user
   * The user object is used to get the users realm and user id
   * @return stdClass
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   * @throws \Doctrine\DBAL\Exception
   */
  public function authorizeCommitteesByFilerID($filerId, $user) {
    $matchingCommittees = $this->_dataManager->db->fetchAllAssociative('SELECT committee_id FROM committee WHERE filer_id = :filer_id', ['filer_id' => strtoupper($filerId)]);
    $data = new stdClass();

    if (count($matchingCommittees) > 0) {
      for ($x = 0; $x <= count($matchingCommittees) - 1; $x++) {
        $committee = $this->getCommittee($matchingCommittees[$x]);
        $this->grantAccess($committee, $user, 'owner');
      }
      $data->success = true;
    } else {
      $data->success = false;
    }
    return $data;
  }


  /**
   * Returns a registration submission object suitable for use with json
   *
   * @param $committee_id
   *   The id of the committee to retrieve
   * @param $admin
   *   This is an authorization variable to hide admin specific stuff from the public
   * @throws Exception
   * @return stdClass
   */
	public function getCommitteeSubmission($committee_id, $admin = false) {
	  $submission = new stdClass();
	  /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    $verified = false;
    if ($committee->registration_id) {
      $registration = $this->_dataManager->em->find(Registration::class, $committee->registration_id);
      if ($registration) {
        $submission->registration_id = $registration->registration_id;
        $submission->certification_email = $registration->certification_email;
        $submission->verified = $registration->verified;
        $submission->reporting_type = $registration->reporting_type ?? $committee->reporting_type;
        if (empty($submission->admin_data)) {
           $submission->admin_data = new stdClass();
        }
        $submission->admin_data->mini_full_permission = $committee->mini_full_permission ?? false;
        $submission->submitted_at = $registration->submitted_at ? $registration->submitted_at->format('Y-m-d H:i:s') : null;
        $submission->updated_at = $registration->updated_at ? $registration->updated_at->format('Y-m-d H:i:s') : null;
        $submission->registered_at = $committee->registered_at ? $committee->registered_at->format('Y-m-d H:i:s') : null;
        $is_json = $registration->user_data && !strpos($registration->user_data, '<') ===0;

        // Need local filing options for setting SEEC filing requirements in ORCA.
        if (!$is_json) {
          $json = json_decode($registration->user_data);
          $submission->local_filing = $json->local_filing ?? null;
        }
        else {
          $submission->local_filing = null;
        }
      }
    }
    $submission->verified = $verified;

    if (!$committee) {
      throw new Exception("No Committee exists for $committee_id");
    }

    $submission->committee = new stdClass();
    foreach (get_object_vars($committee) as $key => $value) {
      switch (true) {
        case ($value instanceof DateTime):
          $submission->committee->$key = $value->format('Y-m-d H:i:s');
          break;
        default:
          $submission->committee->$key = $value;
      }
    }
    if ($verified) {
      $submission->committee->has_sponsor = !empty($committee->sponsor);
    }
    else {
      $submission->committee->has_sponsor="";
    }
    $submission->committee->filer_id = $committee->filer_id;
    $submission->committee->contact = $this->_getSingleContactData($committee, 'committee');
    $submission->committee->books_contact = $this->_getSingleContactData($committee, 'books');
    $submission->committee->bank = $this->_getSingleContactData($committee, 'bank');
    // Get the candidacy record
    if ($committee->pac_type == 'candidate') {
      /** @var Candidacy $c */
      $c = $this->_dataManager->em->getRepository(Candidacy::class)->findOneBy(['committee_id' => $committee->committee_id]);
      if ($c) {
        if (!property_exists($submission->committee, "candidacy")){
          $submission->committee->candidacy = new stdClass();
        }
        $submission->committee->candidacy->candidacy_id = $c->candidacy_id;
        $submission->committee->candidacy->person_id = $c->person_id;
        $submission->committee->candidacy->ballot_name = $c->ballot_name;
        $submission->committee->candidacy->is_active = $c->is_active;
        $submission->committee->candidacy->is_winner = $c->is_winner;
        $submission->committee->candidacy->primary_result = $c->primary_result;
        $submission->committee->candidacy->general_result = $c->general_result;
        $submission->committee->candidacy->exit_reason = $c->exit_reason;

        // get email from candidacy contact
        $cc = $this->_getSingleContactData($committee, 'candidacy');
        $submission->committee->candidacy->email = $cc->email;
        $submission->committee->candidacy->treasurer = $cc->treasurer;

        /** @var Person $p */
        $p = $this->_dataManager->em
          ->find(Person::class, $c->person_id);
        if (!$p) {
          throw new Exception("Candidacy points to invalid person");
        }
        $submission->committee->candidacy->person = $p->voter_name ?? $p->name;

        // put full position title & number on submission
        if($c->position_id) {
          $pos = $this->_dataManager->em->getRepository(Position::class)
            ->findOneBy(['position_id' => $c->position_id]);
          $submission->committee->candidacy->position = $pos->title;
        }
        /** @var Party $pp */
        if ($c->party_id) {
          $pp = $this->_dataManager->em->find(Party::class, $c->party_id);
          if ($pp) {
            $submission->committee->candidacy->party = $pp->name;
            $submission->committee->candidacy->party_id = $pp->party_id;
          }
        }

        /** @var Jurisdiction $j */
        if ($c->jurisdiction_id)  {
          $j = $this->_dataManager->em->find(Jurisdiction::class, $c->jurisdiction_id);
          if ($j) {
            $submission->committee->candidacy->jurisdiction = $j->name;
            $submission->committee->candidacy->jurisdiction_id = $j->jurisdiction_id;
          }
        }

        /** @var Office $o */
        if ($c->office_code) {
          $o = $this->_dataManager->em->find(Office::class, $c->office_code);
          if ($o) {
            $submission->committee->candidacy->office = $o->title;
            $submission->committee->candidacy->office_code = $o->offcode;
          }
        }

        /** @var Position $p */
        $submission->committee->candidacy->position_id = $c->position_id ?? null;;
        if ($c->position_id) {
          $p = $this->_dataManager->em->getRepository(Position::class)->findOneBy(['position_id' => $c->position_id]);
          if ($p) {
            $submission->committee->candidacy->position = $p->title;
          }
        }
      }

    }


    // Get the officers
    $submission->committee->officers = $this->_getOfficerData($committee);

    //Get the relations
    $this->_getRelations($committee, $submission->committee);
    if (!empty($submission->committee->proposals)) foreach ($submission->committee->proposals as $proposal){
      if ($proposal->jurisdiction_id) {
        $jurisdiction = $this->_dataManager->em->find(Jurisdiction::class, $proposal->jurisdiction_id);
        $proposal->jurisdiction = $jurisdiction->name;
      }
      else {
        $proposal->jurisdiction = "";
      }
    }
    $submission->type = 'committee';
    if ($verified) {
      $affiliations = $submission->committee->affiliations ?? [];
      $submission->committee->affiliations_declared = count($affiliations) ? 'yes' : 'no';
    }
    else {
      $submission->committee->affiliations_declared = "";
    }

    //We are calling this to add meta data for extra information
    $submission = $this->metaData($submission);
    //We want to set memo to null in the case that the user is not an admin
    if (!$admin) {
      $submission->committee->memo = null;
    }
    return $submission;
  }

  /**
   * @param $data
   *
   * @return object|null
   *
   * @throws Exception
   */
  public function updateCandidacyStatus($data) {
    $cp = new CandidacyProcessor($this->_dataManager);
    $candidacy = $this->_dataManager->em->getRepository(Candidacy::class)->findOneBy(["committee_id" => $data->committee_id]);
    if ($candidacy) {
      $data->candidacy_id = $candidacy->candidacy_id;
      $cp->saveCandidacy($data);
      // update committee
      $committee = $this->_dataManager->em->find(Committee::class, $data->committee_id);
      $committee->updated_at = new DateTime();
      $this->_dataManager->em->flush($committee);
      return $data;
    } else {
      throw new Exception("No Candidacy found");
    }
  }

  public function updateAdminMemo($data) {
    $committee = $this->_dataManager->em->find(Committee::class, $data->committee_id);
    $committee->memo = $data->memo;
    $this->_dataManager->em->flush($committee);
    return $committee->memo;
  }

  /**
   * perform an edit on a committee.
   * @param $data
   * @return mixed
   * @throws \Exception
   */
  public function updateCommittee($data) {
    $response = NULL;
    $committee = $this->_dataManager->em->find(Committee::class, $data->committee_id);
    if (empty($committee)) {
      throw new Exception("No committee found for committee_id: $data->committee_id");
    }

    // Change committee category
    if (isset($data->category)) {
      $response = $this->validateCommitteeCategory($data->category, $committee->pac_type);
      if ($response->success) {
        $committeeCategory = (object) $this->_dataManager->db->fetchAssociative('select * from committee_category where name = :name', ['name' => $data->category]);
        if ($committeeCategory->party_id) {
          $committee->party = $committeeCategory->party_id;
        }
        $committee->category = $data->category;
      }
    }
    if (!$response) {
      $response = new stdClass();
      $response->success = true;
    }

    if ($committee->continuing && isset($data->end_year)) {
      if (!empty($data->end_year)) {
        if (is_numeric($data->end_year) && $data->end_year >= $committee->start_year) {
          $committee->end_year = (int)$data->end_year;
        }
        else {
          $response->success = false;
          $response->message = "Invalid end year!";
        }
      }
      else {
        $committee->end_year = NULL;
      }
    }


    if (isset($data->memo)) {
      $committee->memo = $data->memo;
    }

    if ($response->success) {
      $this->_dataManager->em->flush($committee);
    }
    $response->data = $data->category;
    return $response;
  }

  /**
   * @param $submittedCategory
   * @param $pac_type
   * @throws Exception
   * @return \stdClass
   */
  private function validateCommitteeCategory($submittedCategory, $pac_type): stdClass
  {
    $response = new stdClass();
    $response->success = false;
    $response->message = null;

    if ($submittedCategory) {
      $category = $this->_dataManager->db->fetchAssociative('select * from committee_category where name = :name', ['name' => $submittedCategory]);
      if (!$category) {
        $response->message = "Invalid category selected: $submittedCategory";
      }
      else if ($category['pac_type'] != $pac_type) {
        $response->message = "Invalid pac type for selected category";

      }
      else {
        $response->success = true;
      }
    }
    else {
      $response->success = true;
      $response->message = null;
    }
    return $response;
  }

  /**
   * @param $submission
   *
   * @return mixed
   */
  public function metaData($submission) {
    $c = $this->_dataManager->em
      ->getRepository(Candidacy::class)
      ->findOneBy(['committee_id' => $submission->committee->committee_id]);
    $submission->meta_data = new stdClass();
    if ($c) {
      $submission->meta_data->candidacy = new stdClass();
      $submission->meta_data->candidacy->primary_result = $c->primary_result;
      $submission->meta_data->candidacy->general_result = $c->general_result;
      $submission->meta_data->candidacy->declared_date = !empty($c->declared_date) ? $c->declared_date->format('Y-m-d') : null;
      $submission->meta_data->candidacy->exit_date = !empty($c->exit_date) ? $c->exit_date->format('Y-m-d') : null;
      $submission->meta_data->candidacy->exit_reason = $c->exit_reason;
      $submission->meta_data->candidacy->office_code = $c->office_code;
      $submission->meta_data->candidacy->jurisdiction_id = $c->jurisdiction_id;
      $submission->meta_data->candidacy->candidacy_id = $c->candidacy_id;
      $submission->meta_data->candidacy->office = $this->_dataManager->em
          ->getRepository(Office::class)
          ->findOneBy(['offcode' => $c->office_code])->title;
      $submission->meta_data->candidacy->jurisdiction = $this->_dataManager->em
          ->getRepository(Jurisdiction::class)
          ->findOneBy(['jurisdiction_id' => $c->jurisdiction_id])->name;
    }
    return $submission;
  }

  /**
   * @param $source_committee_id
   * @param $target_committee_id
   * @param $user_name
   * @throws Exception
   * @return bool Whether the merged committee is in a state that needs registration.
   */
  public function mergeCommittees($source_committee_id, $target_committee_id, $user_name) {
    $registrations = $this->_dataManager->em->getRepository(Registration::class)->findBy(['committee_id' => $source_committee_id]);
    if ($source_committee_id == $target_committee_id) {
      throw new Exception("Cannot merge a committee onto itself:  ". $source_committee_id);
    }

    /** @var Committee $source_committee */
    $source_committee = $this->_dataManager->em->find(Committee::class, $source_committee_id);
    if (!$source_committee) {
      throw new Exception("Cannot find source committee to merge.");
    }

    /** @var Committee $target_committee */
    $target_committee = $this->_dataManager->em->find(Committee::class, $target_committee_id);
    if (!$target_committee) {
      throw new Exception('Cannot find target committee to merge. ');
    }
    $date = null;
    if (!empty($target_committee->registration_id)) {
      $target_registration = $this->_dataManager->em->find(Registration::class, $target_committee->registration_id);
      if ($target_registration) {
        $date = $target_registration->submitted_at;
      }
    }

    $needs_verify = false;
    foreach ($registrations as $registration) {
      if ($registration->submitted_at > $date) {
        $target_committee->registration_id = $registration->registration_id;
        $date = $registration->submitted_at;
        $needs_verify = !$registration->verified;
      }
      else {
        $registration->verified = true;
      }
      $registration->committee_id = $target_committee_id;
      $this->_dataManager->em->persist($registration);
      $this->_dataManager->em->flush();
    }

    $authorizations = $this->_dataManager->em->getRepository(CommitteeAuthorization::class)->findBy(['committee_id' => $source_committee_id]);
    foreach ($authorizations as $authorization) {
      // check for duplicate key
      $targetAuthorization = $this->_dataManager->em->find(CommitteeAuthorization::class, ["committee_id" => $target_committee_id, "uid" => $authorization->uid, "realm" => $authorization->realm]);
      // if no key conflict, persist -- otherwise the authorization will be cascade deleted when committee deletes
      if (empty($targetAuthorization)) {
        $authorization->committee_id = $target_committee_id;
        $this->_dataManager->em->persist($authorization);
        $this->_dataManager->em->flush();
      }
    }

    $draft = $this->_dataManager->em->find(CommitteeRegistrationDraft::class, $source_committee_id);
    if ($draft) {
      $targetDraft = $this->_dataManager->em->find(CommitteeRegistrationDraft::class, $target_committee_id);
      // if no key conflict, persist -- otherwise the draft will be cascade deleted when committee deletes
      if (empty($targetDraft)) {
        $draft->committee_id = $target_committee_id;
        $this->_dataManager->em->persist($draft);
        $this->_dataManager->em->flush();
      }
    }

    $api_keys = $this->_dataManager->em->getRepository(CommitteeAPIAuthorization::class)->findBy(['committee_id' => $source_committee_id]);
    foreach ($api_keys as $api_key) {
      $api_key->committee_id = $target_committee_id;
      $this->_dataManager->em->persist($api_key);
      $this->_dataManager->em->flush();
    }

    $target_committee->updated_at = new DateTime();
    $this->_dataManager->em->persist($target_committee);
    $this->_dataManager->em->remove($source_committee);
    $this->_dataManager->em->flush();
    $this->logCommitteeAction($source_committee_id, $user_name, "Merging committees source", "Merging two committees together based on source committee_id.");
    $this->logCommitteeAction($target_committee_id, $user_name, "Merging committees target", "Merging two committees together based on target committee_id.");
    return $needs_verify;
  }

  /**
   * Generate a token for a committee
   * store it!!!!!!
   * @param $committee_id
   * @param $memo
   * @param $user_name
   *
   * $hash_key = stored in the database
   * $gen_key  = string sent to the ORCA filer
   *
   * @return string
   * @throws Exception
   */
  public function generateAPIToken($committee_id, $memo, $user_name){
    $counter = 0;
    do {
      $counter++;
      $keys = $this->_dataManager->em->getRepository(CommitteeAPIAuthorization::class);

      // generates random characters
      $gen_key = $this->_createApiKey();
      // store hash into the database
      $hash_key = $this->hashKey($gen_key);
      $existing_key = $keys->findOneBy(['hash_value' => $hash_key]);

    } while ($existing_key && $counter < 1000);

    $obj = new CommitteeAPIAuthorization($committee_id, $hash_key);
    $obj->date_generated = new DateTime();
    $obj->memo = $memo;

    $this->_dataManager->em->persist($obj);
    $this->_dataManager->em->flush();
    $this->logCommitteeAction($committee_id, $user_name, "Generating API token", "Generating API token for the committee.");
    return $gen_key;
  }

  /**
   * @throws MappingException
   * @throws \Doctrine\DBAL\Exception
   */
  public function createTestingTokens($election_year) {
    $committees = $this->_dataManager->db->fetchAllAssociative("select c.committee_id, c.filer_id from committee c where :election_year >= c.start_year and (:election_year <= c.end_year or c.end_year is null)  order by c.filer_id", ['election_year' => $election_year]);
    $committee_count = count($committees);
    $cur_count = 0;
    foreach ($committees as $committee) {
      $this->saveAPIToken($committee['committee_id'], 'Validation Token', 'test_user', strtoupper($committee['filer_id']));
      if ($cur_count % 50 == 0) {
        echo "$cur_count / $committee_count \n";
        $this->_dataManager->em->clear();
      }
      $cur_count++;
    }
  }

  /**
   * @param $committee_id
   * @param $memo
   * @param $user_name
   * @param $token
   * @return string
   * @throws Exception
   */
  public function saveAPIToken($committee_id, $memo, $user_name, $token) {
    $hash_key = $this->hashKey($token);

    $existingAPIToken = $this->_dataManager->em->getRepository(CommitteeAPIAuthorization::class)->findOneBy(['hash_value' => $hash_key]);
    if (!$existingAPIToken) {
      $obj = new CommitteeAPIAuthorization($committee_id, $hash_key);
      $obj->date_generated = new DateTime();
      $obj->memo = $memo;

      $this->_dataManager->em->persist($obj);
      $this->_dataManager->em->flush();
      $this->logCommitteeAction($committee_id, $user_name, "Saving API token", "Saving API token for test committee, not as secure should only be used in test!");
    }
    return $hash_key;
  }

  /**
   * @param $api_key_id
   * @param $user_name
   * @throws Exception
   */
  public function revokeAPIToken($api_key_id, $user_name) {
    $token = $this->_dataManager->em->find(CommitteeAPIAuthorization::class, $api_key_id);

    if ($token) {
      $this->_dataManager->em->remove($token);
      $this->_dataManager->em->flush($token);
    }
    $this->logCommitteeAction($token->committee_id, $user_name, "Revoking API token", "Revoking API token for the committee.");
  }

  /**
   * Given a token, send it through the validation
   * and return the committee filer_id if the token is valid.
   *
   * @param $key
   * @return bool
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function getTokenFiler($key) {
    $committee_id = $this->validateAPIToken($key);
    if ($committee_id) {
      $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
      if ($committee) {
        return $committee->filer_id;
      }
    }
    return false;
  }

  /**
   * Token validation
   * Validate it!!!
   * @param $key
   *
   * @return bool|integer
   * @return string
   */
  public function validateAPIToken($key){
    $hashed_key = $this->hashKey($key);
    $keys = $this->_dataManager->em->getRepository(CommitteeAPIAuthorization::class);
    $existing_key = $keys->findOneBy(['hash_value' => $hashed_key]);
    if($existing_key){
      return $existing_key->committee_id;
    }else{
      return false;
    }
  }

  /**
   * Generates a random string
   * Generate it!!
   *
   * @return string
   */
  private function _createApiKey(){
    return substr(str_shuffle(
      str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
      ceil(static::API_KEY_LENGTH/strlen($x)) )),1,static::API_KEY_LENGTH);
  }

  /**
   * Hashes a random key
   * hashing it!!   *
   * @param $user_token
   *
   * @return string
   */
  protected function hashKey($user_token){
    // ENV for prod or SERVER for testing
    $hmac_key = $_ENV["HMAC_KEY"] ?? $_SERVER["HMAC_KEY"] ?? '';
    $hash_key = hash_hmac("sha256", mb_convert_encoding($user_token, 'UTF-8', 'ISO-8859-1'), mb_convert_encoding($hmac_key, 'UTF-8', 'ISO-8859-1'), FALSE);
    // Convert hexadecimal string to binary and then to base64 for .NET compatibility
    $hash_key = hex2bin($hash_key);
    $hash_key = base64_encode($hash_key);
    return $hash_key;
  }

  /**
   * @param string $user_name
   *   User performing the action
   * @param $action
   *   The action being performed
   * @param $message
   *   Message related to the action
   * @param $committee_id
   *   Receiving the id in case we dont have a registration
   * @throws Exception
   */
  public function logCommitteeAction($committee_id, $user_name, $action, $message) {
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    $log = new CommitteeLog();
    $log->committee_id = $committee_id;
    $log->transaction_type = 'committee';
    if ($committee) {
      $log->transaction_id = $committee->registration_id;
    }
    $log->user_name = $user_name;
    $log->action = $action;
    $log->message = $message;
    $log->updated_at = new DateTime();
    $this->_dataManager->em->persist($log);
    $this->_dataManager->em->flush();
  }

  function autoGrantCommitteeAuthorization($uid, $user_name, $realm) {
    $data_sources = [
      'contacts' => $this->_dataManager->em->createQuery("select cc from " . CommitteeContact::class . " cc where lower(cc.email)=lower(:email) and ((cc.treasurer=true and cc.role='officer') or (cc.role='candidacy') or (cc.role='committee'))")
        ->setParameter('email', $user_name)
        ->getResult(),
      'candidacies' => $this->_dataManager->em->createQuery("select c from " . Candidacy::class . " c where lower(c.declared_email)=lower(:email)")
        ->setParameter('email', $user_name)
        ->getResult(),
      'pdc_user' => $this->_dataManager->em->createQueryBuilder()
        ->select('ca')
        ->from(CommitteeAuthorization::class, 'ca')
        ->join(User::class, 'u', Join::WITH, 'u.uid = ca.uid')
        ->where('u.user_name = ?1 and u.uid != ?2')
        ->setParameter('1', $user_name)
        ->setParameter('2', $uid)
        ->getQuery()
        ->getResult()
    ];
    $checked = [];

    foreach ($data_sources as $key => $data_source) {
      foreach ($data_source as $data) {
        // only check committee ids we haven't checked in this loop
        if ($data->committee_id && !in_array($data->committee_id, $checked)) {
          $checked[] = $data->committee_id;
          if (!$this->_dataManager->em->getRepository(CommitteeAuthorization::class)->findBy(['committee_id' => $data->committee_id, 'realm' => $realm, "uid" => $uid])) {
            $committee = $this->_dataManager->em->find(Committee::class, $data->committee_id);
            $auth = new CommitteeAuthorization($data->committee_id, $realm, $uid);
            $auth->role = 'owner';
            $auth->approved = true;
            $this->_dataManager->em->persist($auth);
            $this->_dataManager->em->flush();
            $this->logCommitteeAction($committee->committee_id, $uid, 'AutoGrant Committee Access', 'AutoGrant committee access triggered on ' . $key);
          }
        }
      }
    }
  }

  /**
   * @param $committee
   * @param $election_code
   * @return array
   * @throws Exception
   */
  public function getCommitteeCreationData($committee, $election_code){
    if (!$committee){
      throw new Exception("No Committee");
    }
    $data = ['name' => $committee->name, 'committee_type' => $committee->committee_type, 'pac_type' => $committee->pac_type,
      'bonafide_type' => $committee->bonafide_type, 'exempt' => $committee->exempt, 'election_code' => $election_code];
    return $data;
  }


  /**
   * @param $committee_id
   * @param $user
   * @param $election_code
   * @return int
   * @throws Exception
   */
  public function copyCommittee($committee_id, $election_code, $user) {
    $c = $this->_dataManager->em->find(Committee::class, $committee_id);
    $committee_data = $this->getCommitteeCreationData($c, $election_code);
    $committee = $this->createCommitteeFromData($committee_data, $user);
    return $committee->committee_id;
  }

  /**
   * @param $submission
   * @throws Exception
   */
  public function createCommitteeFromFile($submission) {
    $entityProcessor = new EntityProcessor($this->_dataManager);
    $candidacyProcessor = new CandidacyProcessor($this->_dataManager);

    $found_entity_id = null;
    if (!empty($submission->person->filer_id)) {
      $found_entity_id = $this->_dataManager->db->executeQuery("SELECT entity_id from entity where filer_id=:filer_id", ['filer_id' => $submission->person->filer_id])
        ->fetchOne();
    }
    elseif (!empty($submission->committee->filer_id)) {
      $found_entity_id = $this->_dataManager->db->executeQuery("SELECT entity_id from entity where filer_id=:filer_id", ['filer_id' => $submission->committee->filer_id])
        ->fetchOne();
    }
    if (!$found_entity_id) {
      if (!empty($submission->person)) {
        $entity = $submission->person;
        $entity->is_person = true;
      }
      else {
        $entity = $submission->committee->contact ?? new stdClass();
        $entity->name = $submission->committee->name;
        $entity->is_person = ($submission->committee->committee_type == 'CA');
      }

      if (empty($entity->filer_id)) {
          $entity->filer_id = $submission->committee->filer_id ?? NULL;
      }
      $found_entity_id = $entityProcessor->saveEntity($entity);
    }
    $submission->committee->person_id = $found_entity_id;

    if (!empty($submission->candidacy)) {
      $submission->candidacy->person_id = $found_entity_id;
      $found_candidacy_id = $this->_dataManager->db->executeQuery("SELECT candidacy_id FROM candidacy WHERE person_id = :person_id AND election_code=:election_code", ['person_id' => $found_entity_id, 'election_code' => $submission->candidacy->election_code])
        ->fetchOne();
      if ($found_candidacy_id) $submission->candidacy->candidacy_id = $found_candidacy_id;
      $candidacyProcessor->saveCandidacy($submission->candidacy);
    }

    if (!empty($submission->committee->filer_id)) {

      /** @var Committee $existingCommittee */
      $existingCommittee = $this->_dataManager->em->getRepository(Committee::class)
        ->findOneBy(['start_year' => $submission->committee->start_year, 'filer_id' => $submission->committee->filer_id]);
    }
    else {
      $existingCommittee  = NULL;
    }

    if (!$existingCommittee) {
      $entity_id = null;
      if ($submission->committee->committee_type === 'CO') {
        $entity = new stdClass();
        $entity->name = $submission->committee->name;
        $entity->filer_id = $submission->committee->filer_id ?? null;
        $contact = $submission->committee->contact ?? null;
        $entity->is_person = false;
        if ($contact) {
          $entity->email = $contact->email ?? null;
          $entity->phone = $contact->phone ?? null;
          $entity->address = $contact->address ?? null;
          $entity->premise = $contact->premise ?? null;
          $entity->city = $contact->city ?? null;
          $entity->state = $contact->state ?? null;
          $entity->postcode = $contact->postcode ?? null;
        }
        $entityProcessor = new EntityProcessor($this->_dataManager);
        $entity_id = $entityProcessor->saveEntity($entity);

      }
      $submission_json = json_encode($submission);
      $verification = new stdClass();
      $verification->committee = new stdClass();

      $committee_name = $name ?? $submission->committee->name;
      $committee_type = $submission->committee->committee_type;
      $election_code = $submission->committee->election_code ?? NULL;
      $submitted_at = new DateTime();
      $submitted_at->modify('-1 year');
      $committee = new Committee($committee_name, $committee_type);
      $committee->updated_at = new DateTime();
      $committee->person_id = $entity_id;
      $committee->election_code = $election_code;
      $committee->filer_id = $submission->committee->filer_id ?? NULL;
      $committee->start_year = $submission->committee->start_year ?? (int)$committee->updated_at->format('Y');
      $committee->end_year = $submission->committee->end_year ?? NULL;
      $committee->pac_type = $submission->committee->pac_type;
      $committee->continuing = $committee->start_year != $committee->end_year;
      $committee->reporting_type = $submission->committee->reporting_type ?? null;
      $committee->autoverify = !empty($submission->committee->filer_id);
      $committee->registered_at = $submitted_at;
      $committee->person_id = $found_entity_id;
      $this->_dataManager->em->persist($committee);
      $this->_dataManager->em->flush();

      $registration = new Registration();
      $registration->committee_id = $committee->committee_id;
      $registration->user_data = $submission_json;
      $registration->admin_data = json_encode($verification);

      $registration->submitted_at = $submitted_at;
      $registration->verified = $committee->filer_id ? true : false;
      $registration->reporting_type = $committee->reporting_type;
      $submission->admin_data = $verification;
      $registration->source = 'test';
      $this->_dataManager->em->persist($registration);
      $this->_dataManager->em->flush();
      $committee->registration_id = $registration->registration_id;
      $this->_dataManager->em->persist($committee);
      $this->_dataManager->em->flush();

      $submission->committee->committee_id = $committee->committee_id;

      $this->_dataManager->em->persist($registration);
      $this->_dataManager->em->flush();
      $submission->registration_id = $registration->registration_id;

      if (!empty($submission->committee->candidacy) && $committee->pac_type == 'candidate') {
        $this->_saveContact($committee, $submission->committee->candidacy , 'candidacy');
        $this->_saveCandidacy($committee, $submission->committee->candidacy);
      }

      // Save the committee contact
      if (isset($submission->committee->contact)) {
        $this->_saveContact($committee, $submission->committee->contact, 'committee');
      }

      // Save other officers
      if (isset($submission->committee->officers)) {
        foreach($submission->committee->officers as $officer) {
          $this->_saveContact($committee, $officer);
        }
      }

      $users = [];
      if (!empty($submission->user)) foreach ($submission->user as $userData) {
        $existingUser = $this->_dataManager->em->getRepository(User::class)->findOneBy(['uid' => $userData->uid, 'realm' => $userData->realm]);
        if (!$existingUser) {
          $user = new User();
          $user->user_name = $userData->user_name;
          $user->uid = $userData->uid;
          $user->realm = $userData->realm;
          $this->_dataManager->em->persist($user);
          $this->_dataManager->em->flush();
          $this->grantAccess($committee, $user, 'owner');
          array_push($users, $user);
        } else {
          $this->grantAccess($committee, $existingUser, 'owner');
          array_push($users, $existingUser);
        }
      }

      if (!empty($submission->api_token)) foreach ($submission->api_token as $tokens) {
        $this->saveAPIToken($committee->committee_id, $tokens->memo, $users[0]->user_name, $tokens->token);
      }
      return $submission->committee->committee_id;
    }

    $users = [];
    if (!empty($submission->user)) foreach ($submission->user as $userData) {
      /** @var User $existingUser */
      $existingUser = $this->_dataManager->em->getRepository(User::class)->findOneBy(['uid' => $userData->uid, 'realm' => $userData->realm]);
      if (!$existingUser) {
        $user = new User();
        $user->user_name = $userData->user_name;
        $user->uid = $userData->uid;
        $user->realm = $userData->realm;
        $this->_dataManager->em->persist($user);
        $this->_dataManager->em->flush();
        $this->grantAccess($existingCommittee, $user, 'owner');
        array_push($users, $user);
      } else {
        $this->grantAccess($existingCommittee, $existingUser, 'owner');
        array_push($users, $existingUser);
      }
    }

    if (!empty($submission->api_token)) foreach ($submission->api_token as $tokens) {
      $this->saveAPIToken($existingCommittee->committee_id, $tokens->memo, $users[0]->user_name, $tokens->token);
    }
    return $existingCommittee->committee_id;
  }

  /**
   * @param $committee_id
   * @param $boolean
   * @param $user
   * @throws Exception
   */
  public function miniFullPermission($committee_id, $boolean, $user) {
    if (!is_bool($boolean)) {
      throw new Exception('The miniFullPermission function was called with a non-boolean boolean: ' . $boolean);
    }
    /** @var Committee $committee */
    if ($committee_id) {
      $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    }
    if (empty($committee->committee_id)) {
      throw new Exception('The miniFullPermission function could not find committee #' . $committee_id);
    }
    $committee->mini_full_permission = $boolean;
    $log_action = 'mini-full';
    $log_msg = $boolean ? 'Granting mini-full permission' : 'Revoking mini-full permission';
    $this->_dataManager->em->flush($committee);
    $this->logCommitteeAction($committee->committee_id, $user->user_name, $log_action, $log_msg);
  }

}
