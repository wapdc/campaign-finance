<?php

namespace WAPDC\CampaignFinance;

use DateTime;
use stdClass;
use Exception;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\CommitteeContact;
use WAPDC\CampaignFinance\Model\CommitteeRegistrationDraft;
use WAPDC\CampaignFinance\Model\Party;
use WAPDC\CampaignFinance\Model\Proposal;
use WAPDC\Core\Model\Jurisdiction;
use WAPDC\Core\Model\JurisdictionOffice;
use WAPDC\Core\Model\Office;
use WAPDC\Core\Model\Person;
use WAPDC\Core\Model\Position;
use WAPDC\Core\Model\Candidacy;

class RegistrationDraftManager {

  /**
   * @var CFDataManager
   */
  private $_dataManager;

  /**
   * @var CommitteeManager
   */
  private $_committeeManager;

  /**
   * RegistrationManager constructor.
   *
   * @param CFDataManager $dataManager
   * @param CommitteeManager $committeeManager
   */
  public function __construct(CFDataManager $dataManager, CommitteeManager $committeeManager) {
    $this->_dataManager = $dataManager;
    $this->_committeeManager = $committeeManager;
  }

    /**
     * Load a draft registration for a committee.
     *
     * @param $committee_id
     * @return \stdClass
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Doctrine\DBAL\Exception
     */
  public function getCommitteeRegistrationDraft($committee_id) {
    /** @var CommitteeRegistrationDraft $draft */
    $draft = $this->_dataManager->em->find(CommitteeRegistrationDraft::class, $committee_id);
    /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    // this is used to display the proper mini or full threshold during registration.
    $threshold = $this->_dataManager->db->executeQuery('select amount from thresholds where property = :type', ['type' => "mini_reporting"])->fetchOne();
    if ($draft) {
      $registrationData = $draft->getRegistrationData();
      $registrationData->type = $committee && ($committee->c1_sync || $committee->registration_id) ? 'amendment' : 'draft';
      $registrationData->committee->committee_id = $committee_id;
      $registrationData->committee->pac_type = $committee->pac_type;
      $registrationData->committee->election_code = $committee->election_code;
      $registrationData->committee->exempt = $committee->exempt;
      $registrationData->committee->start_year = $committee->start_year;
      $registrationData->committee->end_year = $committee->end_year;
      $registrationData->committee->bonafide_type = $committee->bonafide_type;
      // Override when the continuing committee value is not changeable.
      if ($committee->continuing || $committee->pac_type == 'candidate') {
        $registrationData->committee->continuing = $committee->continuing;
      }
      $registrationData->submitted_at = null;
      // reset admin_data
      if (!empty($registrationData->admin_data)) {
        unset($registrationData->admin_data);
      }

      $registrationData->admin_data = (object)[
          'reporting_type' => $committee->reporting_type,
          'mini_full_permission' => $committee->mini_full_permission,
          'mini_reporting' => $threshold
      ];
      return $registrationData;
    }
    else {
      return null;
    }
  }


  /**
   * @param $data
   * @param $user
   * @return mixed
   * @throws \Exception
   */
  public function saveCommitteeRegistrationDraft($data, $user) {
    $draftErrors = new SubmissionErrors();
    $this->validateDraft($data, $draftErrors);
    $draft = $this->_dataManager->em->find(CommitteeRegistrationDraft::class, $data->committee->committee_id);
    if (!$draft) {
      $draft = new CommitteeRegistrationDraft($data->committee->committee_id);
    }
    $draft->setRegistrationData($data);
    $draft->updated_at = new DateTime();
    $draft->username = $user->user_name;
    $this->_dataManager->em->persist($draft);
    $this->_dataManager->em->flush();
    return $draftErrors;
  }


  /**
   * @param $submission
   * @param $user
   * @param $election_code
   * @return int
   * @throws \Exception
   */
  public function copyCommitteeRegistrationDraftToNewCommittee($submission, $election_code, $user) {
    $cid = $submission->committee->committee_id;
    $new_cid = $this->_committeeManager->copyCommittee($cid, $election_code, $user);
    $new_sub = $this->getCommitteeRegistrationDraft($new_cid);

    $sc = $submission->committee;
    $new_sc = $new_sub->committee;

    //Copy name, sponsor
    if (isset($sc->has_sponsor)) {
      $new_sc->has_sponsor = $sc->has_sponsor;
      if ($sc->has_sponsor=='yes'){
        $new_sc->sponsor = $sc->sponsor;
        $new_sc->base_name = $sc->base_name;
      }
    }
    $new_sc->name = $sc->name;
    if (isset($sc->acronym)) {
      $new_sc->acronym = $sc->acronym;
    }
    //committee contact
    $this->copyContact($new_sc->contact, $sc->contact);
    //reporting_type
    $new_sc->reporting_type = $sc->reporting_type;
    //bank
    $this->copyContact($new_sc->bank, $sc->bank);
    //books
    $new_sc->books_contact->email = $sc->books_contact->email;
    //officers
    for ($i=0; $i<count($sc->officers); $i++) {
      array_push($new_sc->officers, new stdClass());
      $this->copyContact($new_sc->officers[$i], $sc->officers[$i]);
    }
    if ($sc->committee_type == 'CA') {
      //candidacy
      if (!property_exists($new_sc, "candidacy")){
        $new_sc->candidacy = new stdClass();
      }
      $this->copyContact($new_sc->candidacy, $sc->candidacy);
    }
    else {
      //affiliations
      if (isset($sc->affiliations_declared)) {
        $new_sc->affiliations_declared = $sc->affiliations_declared;
        if ($sc->affiliations_declared=='yes'){
          $new_sc->affiliations = [];
          for ($i=0; $i<count($sc->affiliations); $i++) {
            array_push($new_sc->affiliations, new stdClass());
            $this->copyContact($new_sc->affiliations[$i], $sc->affiliations[$i]);
          }
        }
      }
    }
    $this->saveCommitteeRegistrationDraft($new_sub, $user);
    return $new_cid;
  }

  private function copyContact($target, $source){
    foreach($source as $property=>$value){
      switch ($property){
        case 'email':
        case'role':
        case 'title':
        case 'treasurer':
        case 'name':
        case 'person':
        case 'ballot_name':
        case 'address':
        case 'city':
        case 'state':
        case 'postcode':
        case 'phone':
        case 'jurisdiction':
        case 'jurisdiction_id':
        case 'office':
        case 'offcode':
        case 'position':
        case 'position_id':
        case 'affiliate_name':
        case 'affiliate_id':
          $target->$property = $value;
          break;
        default:
          break;
      }
    }
  }

  /**
   *  Given a submission, delete the associated Committee Registration Draft
   *  If the submission is for a committee that only has a draft and has never
   *  been certified, delete the Committee as well. If the committee still exists
   *  it will return the committee_id, otherwise it will return null.
   *
   * @return mixed
   * @param int $committee_id
   *   integer of committee.
   * @param $user_name
   * @throws \Exception
   */
  public function deleteDraft($committee_id, $user_name = null){
    $draft = $this->_dataManager->em->find(CommitteeRegistrationDraft::class, $committee_id);
    if ($draft) {
      $this->_dataManager->em->remove($draft);
      $this->_dataManager->em->flush();
      $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
      if ($committee){
        if (!$committee->registration_id && !$committee->c1_sync) {
          $this->_committeeManager->deleteCommittee($committee_id, $user_name);
          $committee_id = null;
        }
      }
      else {
        throw new Exception("Committee Not Found");
      }
    }
    return $committee_id;
  }

  /**
   * Given a draft (or draft portion) and an array of required fields,
   * verify that the draft contains all required fields and that they are not null
   *
   * @param \stdClass $draft
   * @param SubmissionErrors $errors
   * @param $fields
   */
  private function validateRequiredFields(stdClass $draft, SubmissionErrors $errors, $fields){
    foreach ($fields as $field){
      if (!isset($draft->$field) || $draft->$field==null || trim($draft->$field, ' ') == ""){
        $errors->addError("{$field}.required", "Missing {$field}");
      }
    }
  }

  /**
   * Given a draft (or draft portion), and a properties array of [property_id, class]
   * verify that the property_id is a valid number and is a valid id in the class table.
   *
   * @param \stdClass $draft
   * @param $object
   * @param SubmissionErrors $errors
   * @param $properties
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  private function validateIDs(stdClass $draft, $object, SubmissionErrors $errors, $properties, $fix_only = false) {
    foreach ($properties as list($property, $class)) {
      if (isset($draft->$property) && $draft->$property) {
        if ($property != 'office_code' && !ctype_digit((string) $draft->$property) && !is_int($draft->$property)) {
          $errors->addError("{$object}.{$property}.invalid", "An invalid {$property} has been specified for {$object}.");
          return;
        }
        if (!$this->_dataManager->em->find($class, $draft->$property)) {
          if ($fix_only === true) {
            unset($draft->$property);
          } else {
            $errors->addError("{$object}.{$property}.invalid", "An invalid {$property} has been specified for {$object}.");
          }
        }
      }
    }
    return;
  }

  /**
   * @param $object
   * @param $property
   * @param $value
   * @param SubmissionErrors $errors
   */
  private function validateProperty($object, $property, $value, SubmissionErrors $errors){
    switch ($property){
      case 'email':
        if ($value && !strpos($value, '@')) {
          $errors->addError("{$object}.invalid", "Invalid {$object} information.");
        }
        break;
    }
  }

  /**
   * @param \stdClass $draft
   * @param SubmissionErrors $errors
   * @return SubmissionErrors
   * @throws \Exception
   */
  public function validateDraft(stdClass $draft, SubmissionErrors $errors) {

    // if no draft committee, return error immediately
    if (!isset($draft->committee)) {
      $errors->addError("committee.required", "Missing required committee.");
      return $errors;
    }

    // draft committee
    $committee = $draft->committee;
    // database committee
    $saved_committee = !empty($committee->committee_id) ? $this->_dataManager->em->find(Committee::class, $committee->committee_id) : null;
    // treasurer check
    $hasTreasurer = false;

    // local filing
    if(isset($draft->local_filing->seec->required) && $draft->local_filing->seec->required == TRUE){
      if(!isset($draft->local_filing->seec->books_available) ||
        $draft->local_filing->seec->books_available==null ||
        trim($draft->local_filing->seec->books_available, ' ') == ""){
        $errors->addError('local_books_available.required', 'Missing local filing books inspection hours.');
      }
    }

    // name, committee_type, pac_type
    $this->validateRequiredFields($committee, $errors, ['name', 'committee_type', 'pac_type']);

    // jurisdiction_id validation
    $this->validateIDs($committee, 'committee', $errors, [['jurisdiction_id', Jurisdiction::class]]);

    $committee_type = $committee->committee_type ?? '';
    $pac_type = isset($committee->pac_type)? $committee->pac_type : '';
    $bonafide_type = isset($committee->bonafide_type)? $committee->bonafide_type : '';

    // committee type validation
    if (!in_array($committee_type, array('CA', 'CO'))) {
      $errors->addError("committee_type.invalid", "This is not a valid type of committee.");
    }

    // reporting type validation
    if (!isset($committee->reporting_type) || !in_array($committee->reporting_type, array('mini', 'full'))){
      $errors->addError("reporting_type.invalid", "You must specify either Mini or Full reporting.");
    }
    # mini to full permission validation
    if ($saved_committee && $saved_committee->reporting_type === 'mini' && $committee->reporting_type === 'full' && empty($saved_committee->mini_full_permission)) {
      $errors->addError("reporting_type.minifull", "A committee must get permission to switch from mini to full reporting.");
    }


    // committee contact validation
    if (!isset($committee->contact)) {
      $errors->addError("contact.incomplete", "Committee contact information is incomplete");
    }
    else {
      $contact = $committee->contact;
      $contactErrors = new SubmissionErrors();
      $this->validateRequiredFields($contact, $contactErrors, ['email', 'address', 'city', 'postcode', 'state']);
      if ($contactErrors->hasErrors()){
        $errors->addError('contact.incomplete', 'Committee contact information is incomplete');
      }
      if (isset($contact->email) && !$contactErrors->hasErrorId('email.required')){
        $this->validateProperty('contact', 'email', $contact->email, $errors);
      }
    }

    // bank validation
    $bankErrors = new SubmissionErrors();
    $this->validateRequiredFields($committee->bank, $bankErrors, ['name', 'city', 'address', 'state', 'postcode']);
    if ($bankErrors->hasErrors()){
      $errors->addError('bank.incomplete', 'Bank information is incomplete');
    }

    // books validation
    $bookErrors = new SubmissionErrors();
    $this->validateRequiredFields($committee->books_contact, $bookErrors, ['email']);
    if ($bookErrors->hasErrors()){
      $errors->addError('books.incomplete', 'Contact information for records inspection is incomplete.');
    }
    if (isset($committee->books_contact->email) && !$bookErrors->hasErrors()) {
      $this->validateProperty('books', 'email', $committee->books_contact->email, $errors);
    }

    // candidate specific validations
    if ($committee_type == 'CA'){
      if (!in_array($pac_type, array('candidate', 'surplus'))) {
        $errors->addError("pac_type.invalid", "The pac_type for candidate committees must be 'candidate' or 'surplus'");
      }
      if ($pac_type == 'candidate') {
        $candidacyErrors = new SubmissionErrors();
        if (empty($committee->candidacy)) {
          $errors->addError("candidacy.incomplete", "Candidacy is incomplete");
        }
        else {
          $candidacy = $committee->candidacy;
          $this->validateRequiredFields($candidacy, $candidacyErrors, ['jurisdiction', 'office', 'person', 'email']);
          if ($candidacyErrors->hasErrors()) {
            $errors->addError('candidacy.incomplete', 'Candidacy is incomplete');
          }
          if (isset($candidacy->email) && !$candidacyErrors->hasErrorId('email.required')) {
            $this->validateProperty('candidacy', 'email', $candidacy->email, $errors);
          };
          $this->validateIDs($candidacy, 'candidacy', $errors, [['jurisdiction_id', Jurisdiction::class], ['position_id', Position::class], ['person_id', Person::class], ['party_id', Party::class], ['office_code', Office::class]]);
          if (isset($candidacy->treasurer)) {
            $hasTreasurer = $candidacy->treasurer;
          }

          // office check
          $draft_oid = $candidacy->office_code ?? null;
          if ($draft_oid) {
            $o = $this->_dataManager->em->find(Office::class, $draft_oid);
          }
          if (empty($o->offcode) && !empty($candidacy->office)) {
            $o = $this->_dataManager->em->getRepository(Office::class)->findOneBy(['title' => $candidacy->office]);
          }
          if (empty($o->offcode)) {
            $errors->addError('candidacy.office.invalid', "Unable to find office. Try re-selecting the candidacy information..");
          }
          else {
            // jurisdiction check
            $draft_jid = $candidacy->jurisdiction_id ?? null;
            if (filter_var($draft_jid, FILTER_VALIDATE_INT)) {
              $j = $this->_dataManager->em->find(Jurisdiction::class, $draft_jid);
            } elseif (!empty($draft_jid)) {
              $errors->addError('candidacy.jurisdiction_id.invalid', "Unable to find jurisdiction. Try re-selecting the candidacy information.");
            }
            if (empty($j->jurisdiction_id) && !empty($candidacy->jurisdiction)) {
              $j = $this->_dataManager->em->getRepository(Jurisdiction::class)->findOneBy(['name' => $candidacy->jurisdiction]);
            }
            if (empty($j->jurisdiction_id)) {
              $errors->addError('candidacy.jurisdiction.invalid', "Unable to find jurisdiction. Try re-selecting the candidacy information.");
            }
          }

          // checks that need jid and oid
          if (!empty($j->jurisdiction_id) && !empty($o->offcode)) {
            // jurisdiction office check
            $jo = $this->_dataManager->em->getRepository(JurisdictionOffice::class)->findOneBy(['jurisdiction_id' => $j->jurisdiction_id, 'offcode' => $o->offcode]);
            $now = new DateTime();
            if (empty($jo->jurisdiction_office_id)) {
              $errors->addError('candidacy.jurisdictionoffice.invalid', "Unable to find jurisdiction-office combination. Try re-selecting the candidacy information to fix this.");
            }
            // temporally invalid
            elseif ((!empty($jo->valid_to) && ($jo->valid_to < $now)) || (!empty($jo->valid_from) && ($jo->valid_from > $now))) {
              $errors->addError('candidacy.jurisdictionoffice.invalid', "The selected jurisdiction-office combination is not presently allowed. Try re-selecting the candidacy information to fix this.");
            }
            // partisan check
            elseif (!$jo->partisan && !empty($candidacy->party) && strtolower($candidacy->party) != 'non partisan') {
              $errors->addError('candidacy.party.invalid', "You may not designate a political party for a non-partisan office.");
            }
            else {
              // no changing jurisdiction office
              $committeeCandidacy = $this->_dataManager->em->getRepository(Candidacy::class)->findOneBy(['committee_id' => $draft->committee->committee_id]);
              $jurisdiction_id = $committeeCandidacy->jurisdiction_id ?? null;
              if (is_object($committeeCandidacy) && $committeeCandidacy->sos_candidate_code && ($j->jurisdiction_id != $jurisdiction_id || $o->offcode != $committeeCandidacy->office_code)) {
                $errors->addError('candidacy.jurisdictionoffice.change', "You may not amend the jurisdiction or office on a candidacy committee after the candidacy has been declared to the office of the Secretary of State. If you need to correct your candidacy, please contact the PDC.");
              }
            }
          }
        }
        if (isset($committee->continuing) && $committee->continuing) {
          $errors->addError('continuing.invalid', "Candidate committees cannot continue");
        }
      }
    }

    // committee specific validations
    elseif ($committee_type=='CO'){

      if (!in_array($pac_type, array('pac', 'bonafide', 'caucus'))) {
        $errors->addError("pac_type.invalid", "The pac_type for political committees must be 'pac', 'bonafide' or 'caucus'.");
      }

      if (empty($committee->election_code) && !$committee->continuing) {
         $errors->addError("continuing.invalid", 'Only a committee formed in anticipation of a particular election can "not continue"');
      }
      elseif (!empty($saved_committee->continuing) && !$committee->continuing) {
        $errors->addError("continuing.invalid", "A committee cannot go back to being a single election committee after being registered as a continuing committee ");
      }

      if (!isset($committee->has_sponsor) || empty($committee->has_sponsor)) {
        $errors->addError('sponsor.undeclared', "You must declare whether or not the committee is sponsored.");
      }
      elseif (strtolower($committee->has_sponsor)=='yes'){
        $this->validateRequiredFields($committee, $errors, ['sponsor']);
      }

      if ($pac_type == 'bonafide') {
        if (!in_array($bonafide_type, array('state', 'county', 'district'))) {
          $errors->addError("bonafide_type.invalid", "The bonafide_type for bonafide committees must be 'state', 'county', or 'district'");
        }
      }

      if ($pac_type == 'bonafide' || $pac_type == 'caucus'){
        $this->validateIDs($committee, 'committee', $errors, [['party_id', Party::class]]);
        if (!isset($committee->party_id) || !$committee->party_id) {
          $errors->addError('committee.party_id.required', 'You must declare which political party you are filing for');
        }
      }

      if (!isset($committee->affiliations_declared) || !$committee->affiliations_declared || !in_array(strtolower($committee->affiliations_declared), ['yes', 'no'])){
        $errors->addError('affiliations.undeclared', 'You must declare whether the committee has affiliations with other committees.');
      }

      // Make sure single election committee has data that indicates its purpose
      if ($pac_type == "pac" && !$committee->continuing && !empty($committee->election_code)) {
        if (empty($committee->candidates) && empty($committee->proposals) && (empty($committee->ticket->name) || empty($committee->ticket->stance))) {
          $errors->addError('proposal.incomplete', "You must state the purpose of the committee when it is established in anticipation of an election");
        }

      }

      //committee tickets
      if (!empty($committee->pc_stance_party)) {
        $ticketErrors = new SubmissionErrors();
        $validateFields = ['name', 'stance'];
        $this->validateRequiredFields($committee->ticket, $ticketErrors, $validateFields);

        if ($ticketErrors->hasErrors()) {
          $errors->addError('ticket.incomplete', 'Ticket information is incomplete');
        }
      }

      //committee candidates
      if (!empty($committee->pc_stance_candidate) && empty($committee->candidates)) {
        $errors->addError('ticket.incomplete', 'Candidate information is incomplete');
      }
      if (!empty($committee->candidates)) foreach($committee->candidates as $candidate) {
        $candidateErrors = new SubmissionErrors();
        $validateFields = ['stance', 'office', 'person'];
        $this->validateRequiredFields($candidate, $candidateErrors, $validateFields);

        if ($candidateErrors->hasErrors()){
          $errors->addError('ticket.incomplete', 'Candidate information is incomplete');
        }
      }

      //proposals
      if (!empty($committee->proposals)) foreach($committee->proposals as $proposal) {
        $proposalErrors = new SubmissionErrors();
        if (!empty($proposal->jurisdiction_id)) {
          $jurisdiction = $this->_dataManager->em->find(Jurisdiction::class, $proposal->jurisdiction_id);
          $proposal_type_support = $jurisdiction->{strtolower($proposal->proposal_type)};
          if (!$proposal_type_support) {
            $errors->addError('proposal.incomplete',"error this jurisdiction doesn't support $proposal->proposal_type proposals.");
          }
        }
        $validateFields = ['stance', 'jurisdiction'];
        if ($proposal->proposal_type == "recall") {
          $otherFields = ['proposal_issue', 'offcode'];
          $validateFields = array_merge($validateFields, $otherFields);
        }
        else {
          $otherFields = ['election_code'];
          $validateFields = array_merge($validateFields, $otherFields);
        }
        $this->validateRequiredFields($proposal, $proposalErrors, $validateFields);

        if ($proposalErrors->hasErrors()){
          $errors->addError('proposal.incomplete', 'Proposal information is incomplete');
        }
        $this->validateIDs($proposal, 'proposal', $errors, [['proposal_id', Proposal::class]]);
      }

      //affiliations
      if (!empty($committee->affiliations)) foreach ($committee->affiliations as $affiliation) {
        $this->validateIDs($affiliation, 'affiliation', $errors, [['affiliate_id', Committee::class]]);
        $this->validateRequiredFields($affiliation, $errors, ['affiliate_name']);
      }
    }

    // Assume we have only ministerial treasurers.
    $ministerial_only = $committee_type == 'CO';

    //officers
    foreach ($committee->officers as $officer){
      $officerErrors = new SubmissionErrors();
      $this->validateRequiredFields($officer, $officerErrors, ['name', 'title', 'address', 'city', 'postcode', 'email']);
      if ($officerErrors->hasErrors()){
        $errors->addError('officer.incomplete', 'Officer information is incomplete');
      }
      $this->validateProperty('officer', 'email', $officer->email, $errors);
      $this->validateIDs($officer, 'officer', $errors, [['contact_id', CommitteeContact::class]], true);
      if (empty($officer->ministerial)) {
        $ministerial_only = false;
      }

      if (isset($officer->treasurer) && $officer->treasurer){
        $hasTreasurer = true;
      }
    }

    //treasurer
    if (!$hasTreasurer){
      $errors->addError('treasurer.required', "Committee must have at least one treasurer.");
    }
    else if ($ministerial_only) {
      $errors->addError('treasurer.ministerial-only', "Committee must have at least one non-ministerial officer.");
    }

    return $errors;
  }
}