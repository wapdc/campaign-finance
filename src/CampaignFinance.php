<?php

namespace WAPDC\CampaignFinance;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\ORMException;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\SchemaManager;

class CampaignFinance {

  /**
   * @var CFDataManager
   */
  protected $_dataManager;

  /** @var \WAPDC\CampaignFinance\FinanceReportingProcessor $_financeReportingProcessor */
  protected $_financeReportingProcessor;

  private $fileService;

  private $migration_mode;

  public $project_dir = '.';

  /**
   * CampaignFinance constructor.
   * @throws \Exception
   */
  public function __construct($migration = FALSE) {
    $this->migration_mode = $migration;
    $this->_dataManager = new CFDataManager('campaign-finance', $migration);
    $this->project_dir = dirname(__DIR__);
  }

  static private $_service;

  /**
   * Singleton Factory
   *
   * @return CampaignFinance
   * @throws \Exception
   */
  static public function service($migration = FALSE) {
    if (!static::$_service) {
      static::$_service = new static($migration);
    }
    return static::$_service;
  }

    /**
     * @return \WAPDC\CampaignFinance\FinanceReportingProcessor
     * @throws DBALException
     * @throws ORMException
     */
  public function getFinanceReportingProcessor() {
      if(!$this->_financeReportingProcessor) {
          $this->_financeReportingProcessor = new FinanceReportingProcessor($this->_dataManager, $this->getCommitteeManager());
      }
      return $this->_financeReportingProcessor;
  }
  /**
   * Returns a Committee Manager
   * @return \WAPDC\CampaignFinance\CommitteeManager
   */
  public function getCommitteeManager() {
    return new CommitteeManager($this->_dataManager);
  }


  /**
   * Perform schema upgrades.
   * @throws \Exception
   */
  public function rdsUpgrade()
  {
    $this->_dataManager->generateProxyFiles();

    // Run the postgres RDS schema revisions
    $revision_dir = dirname(__DIR__) . '/db/postgres/revisions';
    $s = new SchemaManager($revision_dir, $this->_dataManager);
    $s->runRevScripts();
  }

  /**
   * @return RegistrationManager
   */
  public function getRegistrationManager() {
    $filerProcessor = CoreDataService::service()->getFilerProcessor();
    return new RegistrationManager($this->_dataManager, $this->getCommitteeManager(), $this->getRegistrationDraftManager(), $filerProcessor, $this->getFileService());
  }

  /**
   * @return RegistrationDraftManager
   */
  public function getRegistrationDraftManager() {
    return new RegistrationDraftManager($this->_dataManager, $this->getCommitteeManager());
  }

  public function getOrcaRegistrationManager() {
    return new OrcaRegistrationManager($this->_dataManager, $this->getCommitteeManager(), $this->getRegistrationManager());
  }

  public function adminQuery($sql_file, $parms=[]) {
    $dir = $this->project_dir . '/data/admin';
    return $this->_dataManager->executeQueryFromFile($dir, $sql_file, $parms);
  }

  public function campaignRegistrationQuery($sql_file, $parms=[]) {
    $dir = $this->project_dir . '/data/campaign_registration';
    return $this->_dataManager->executeQueryFromFile($dir, $sql_file, $parms);
  }

  /**
   * @return RegistrationRestorer
   * @throws DBALException
   * @throws ORMException
   */
  public function getRestorer() {
    return new RegistrationRestorer(
      $this->_dataManager,
      new RestoreDataManager()
    );
  }

  public function getFileService(){
    if(!$this->fileService){
      $this->fileService = new CFAWSFileService();
    }
    return $this->fileService;
  }

  /**
   * Check if a user has owner access to a committee
   *
   * @param $committee_id
   * @param $user \WAPDC\Core\Model\User A user model object from the pdc_user table
   * @param string $role
   *
   * @return bool
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function CommitteeAuthorizationCheck($committee_id, $user, $role = "owner"){
    $committeeManager = $this->getCommitteeManager();
    $committee = $committeeManager->getCommittee($committee_id);
    if($committee){
        return $committeeManager->userHasRole($committee, $user, $role);
    }else{
      return FALSE;
    }
  }

  public function reportAuthorizationCheck($report_id, $user, $role = 'owner')
  {
    $q = "select f.committee_id from report r join fund f on r.fund_id = f.fund_id where report_id=?";
    $committee_id = $this->_dataManager->db->executeQuery($q, [$report_id])->fetchFirstColumn()[0];
    return $this->CommitteeAuthorizationCheck($committee_id, $user, $role);
  }

    /**
     * @param $fund_id
     * @param $user
     * @param string $role
     * @return bool
     */
  public function fundAuthorizationCheck($fund_id, $user, $role = 'owner'): bool
  {
    $committeeManager = $this->getCommitteeManager();
    /** @var Committee $committee */
    $committee =  $committeeManager->getCommitteeByFund($fund_id);
    if ($committee) {
      return $committeeManager->userHasRole($committee, $user, $role);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the fund processor.
   * @return FundProcessor
   */
  public function getFundProcessor() {
    return new FundProcessor($this->_dataManager);
  }

  /**
   * @param $param
   *
   * @return array
   * @throws \Doctrine\DBAL\DBALException
   * @throws Exception
   */
  public function candidatesXML($days) {
    $result = $this->_dataManager->db->executeQuery('select candidate_registrations_as_xml(cast(:days as integer))', ["days" => $days], ['days' => 'integer'] );
    return $result->fetchOne();
  }

  /**
   * Orca processor factory.
   * @return OrcaProcessor
   */
  public function getOrcaProcessor() {
    return new OrcaProcessor($this->_dataManager);
  }

  /**
   * @return IEProcessor
   */
  public function getIEProcessor() {
    return new IEProcessor($this->_dataManager);
  }

  /**
   * @return VendorSubmissionProcessor
   */
  public function getVendorSubmissionProcessor(){
    return new VendorSubmissionProcessor($this->_dataManager);
  }

}
