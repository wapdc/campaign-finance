<?php

namespace WAPDC\CampaignFinance\SubmissionDecoder;


use WAPDC\CampaignFinance\Model\Registration;
use \stdClass;
use \Exception;

class RegistrationLegacyDecoder implements SubmissionDecoderInterface
{
  /**
   * @throws \Exception
   */
  public function generateSubmission(Registration $registration)
  {
    $xml = simplexml_load_string($registration->user_data);


    $submission = new stdClass();
    $submission->committee = new stdClass();
    $submission->committee->contact = new stdClass();
    $submission->committee->bank = new stdClass();
    $submission->committee->books_contact = new stdClass();
    $submission->committee->candidacy = new stdClass();
    $submission->committee->officers=[];
    $submission->committee->proposals=[];
    $submission->committee->affiliations=[];
    $submission->registration_id = $registration->registration_id;
    $submission->committee->committee_id = $registration->committee_id;
    $submission->source = $registration->source;
    $submission->verified = $registration->verified;
    $submission->updated_at = $registration->updated_at?$registration->updated_at->format("Y-m-d") : '';
    $submission->submitted_at = $registration->submitted_at?$registration->submitted_at->format("Y-m-d") : '';
    if ($xml->xpath('//c1')) {
      $submission->committee->committee_type = 'CA';
    }
    else if ($xml->xpath('//c1pc')){
      $submission->committee->committee_type = 'CO';
    }
    else {
      throw new Exception("Unknown committee type in registration: ". $registration->registration_id);
    }

    foreach ($xml as $key=>$value){
      switch ($key){
        case 'addr':
          $submission->committee->contact->state = (string) $value['st'];
          $submission->committee->contact->postcode = (string) $value['zip'];
          $submission->committee->contact->city = (string) $value['city'];
          $submission->committee->contact->address = (string) $value['str'];
          break;
        case 'bank':
          $submission->committee->bank->name = (string) $value['name'];
          $submission->committee->bank->branch = (string) $value['branch'];
          $submission->committee->bank->city = (string) $value['city'];
          $submission->committee->bank->state = (string) $value['state'];
          $submission->committee->bank->address = (string) $value['address'];
          $submission->committee->bank->postcode = (string) $value['postcode'];
          break;
        case 'campaignBooks':
          $submission->committee->books_contact->email = (string) $value['email'];
          break;
        case 'candidate':
          $submission->committee->candidacy->person = (string) $value['name']? : (string) $value['fst'].' '.(string) $value['lst'];
          break;
        case 'committee':
          $lower_name = strtolower((string) $value['name']);
          // Some committees have exempt indicator in the name only.
          if (
            strpos($lower_name, 'non-exempt')
            || strpos($lower_name, 'non exempt')
            || strpos($lower_name, 'nonexempt')) {
            $submission->committee->exempt = FALSE;
          }
          elseif (strpos($lower_name, 'exempt')) {
            $submission->committee->exempt = TRUE;
          }

          $submission->committee->name = (string) $value['name'];
          $submission->committee->contact->email = (string) $value['email'];
          $submission->committee->contact->phone = (string) $value['phone']? : (string) $value['phone1']? : '';
          $submission->committee->candidacy->email = (string) $value['candidateemail'];
          break;
        case 'committeeOfficer':
          $officer = new stdClass();
          $officer->title = (string) $value['title']? : 'Officer';
          $officer->role = (string) $value['title'] =='CANDIDATE'? 'candidacy' : 'officer';
          $officer->name = (string) $value['name'];
          $officer->address = (string) $value['str'];
          $officer->city = (string) $value['city'];
          $officer->state = (string) $value['st'];
          $officer->postcode = (string) $value['zip'];
          $officer->email = (string) $value['email'];
          $officer->phone = (string) $value['phone'];
          array_push($submission->committee->officers, $officer);
          break;
        case 'committeeStatus':
          $submission->committee->election_code = (string) $value['year'];
          $submission->committee->continuing = (string) $value['status']=='continue'? true: false;
          $submission->committee->party = (string) $value['party'];
          $exempt = (string) $value['exempt'];
          if (!empty($exempt)) {
            $submission->committee->exempt = ($exempt == 'exempt');
          }
          break;
        case 'deputytreasurer':
          $treasurer = new stdClass();
          $treasurer->name = (string) $value['name']? : (string) $value['fst'].' '.(string) $value['lst'];
          $treasurer->role = 'officer';
          $treasurer->treasurer = true;
          $treasurer->title = (string) $value['title']? : 'Deputy Treasurer';
          $treasurer->ministerial = strtolower((string) $value['ministerial']) == 'true' ? TRUE : FALSE ;
          $treasurer->email = (string) $value['email'];
          $treasurer->phone = (string) $value['phone'];
          $treasurer->state = (string) $value['st'];
          $treasurer->postcode = (string) $value['zip'];
          $treasurer->city = (string) $value['city'];
          $treasurer->address = (string) $value['str'];
          array_push($submission->committee->officers, $treasurer);
          break;
        case 'generalElectionDate':
          $submission->committee->election_code = date("Y", strtotime((string) $value[0]));
          break;
        case 'manager':
          $manager = new stdClass();
          $manager->title = (string) $value['title']? : 'Manager';
          $manager->role = 'officer';
          $manager->name = (string) $value['name']? : (string) $value['fst'].' '.(string) $value['lst'];
          $manager->address = (string) $value['str'];
          $manager->city = (string) $value['city'];
          $manager->state = (string) $value['st'];
          $manager->postcode = (string) $value['zip'];
          $manager->email = (string) $value['email'];
          $manager->phone = (string) $value['phone'];
          array_push($submission->committee->officers, $manager);
          break;
        case 'ministerial':
          $treasurer = new stdClass();
          $treasurer->name = (string) $value['name']? : (string) $value['fst'].' '.(string) $value['lst'];
          $treasurer->role = 'officer';
          $treasurer->treasurer = true;
          $treasurer->title = (string) $value['title']? : 'Treasurer';
          $treasurer->ministerial = true;
          $treasurer->email = (string) $value['email'];
          $treasurer->phone = (string) $value['phone'];
          $treasurer->state = (string) $value['st'];
          $treasurer->postcode = (string) $value['zip'];
          $treasurer->city = (string) $value['city'];
          $treasurer->address = (string) $value['str'];
          array_push($submission->committee->officers, $treasurer);
          break;
        case 'officeSought':
          $submission->committee->candidacy->office = (string) $value['office'];
          $submission->committee->candidacy->position = (string) $value['position'];
          $submission->committee->candidacy->jurisdiction = (string) $value['jurisdiction'];
          $submission->committee->candidacy->incumbent = (string) $value['incumbent'];
          break;
        case 'purpose':
          switch ((string) $value['type']){
            case 'ballot':
              $proposal = new stdClass();
              $proposal->ballot_number= (string) $value['ballotNum'];
              $proposal->proposal_type= (string) $value['ballottype'];
              $proposal->proposal_issue= (string) $value['descr'];
              $proposal->jurisdiction= (string) $value['jurisdiction'];
              $proposal->stance= (string) $value['standing'];
              if ($proposal->ballot_number == '?') $proposal->ballot_number = "";
              array_push($submission->committee->proposals, $proposal);
              break;
            case 'party':
              $submission->committee->bonafide_type = (string) $value['bonafidetype'];
              break;
            case 'sey':
              //TODO add supported candidates/tickets
              $submission->committee->pac_type = 'PAC';
              $submission->committee->continuing = false;
              break;
            default:
              break;
          }
          break;
        case 'reporting':
          $submission->committee->reporting_type =  (string) $value['type'];
          break;
        case 'treasurer':
          $treasurer = new stdClass();
          $treasurer->name = (string) $value['name']? : (string) $value['fst'].' '.(string) $value['lst'];
          $treasurer->role = 'officer';
          $treasurer->treasurer = true;
          $treasurer->title = (string) $value['title']? : 'Treasurer';
          $treasurer->ministerial = strtolower((string) $value['ministerial']) == 'true' ? TRUE : FALSE ;
          $treasurer->email = (string) $value['email'];
          $treasurer->phone = (string) $value['phone'];
          $treasurer->state = (string) $value['st'];
          $treasurer->postcode = (string) $value['zip'];
          $treasurer->city = (string) $value['city'];
          $treasurer->address = (string) $value['str'];
          array_push($submission->committee->officers, $treasurer);
          break;
        case 'party':
          $submission->committee->candidacy->party = (string) $value[0];
          break;
        case 'form':
        default:
          break;
      }
    }
    return $submission;
  }
}