<?php

namespace WAPDC\CampaignFinance\SubmissionDecoder;

use WAPDC\CampaignFinance\Model\Registration;
use \stdClass;

class PaperRegistrationDecoder implements SubmissionDecoderInterface
{
    /**
     * @param Registration $registration
     * @throws \Exception
     */
    public function generateSubmission(Registration $registration)
    {
        $submission = new stdClass();
        $submission->committee = new stdClass();
        return $submission;
    }
}