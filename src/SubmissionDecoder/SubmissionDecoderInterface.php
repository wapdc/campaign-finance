<?php

namespace WAPDC\CampaignFinance\SubmissionDecoder;


use WAPDC\CampaignFinance\Model\Registration;

interface SubmissionDecoderInterface
{

  /**
   * Build a submission object from a registration
   * @param Registration $registration
   * @return \stdClass
   */
  public function generateSubmission(Registration $registration);
}
