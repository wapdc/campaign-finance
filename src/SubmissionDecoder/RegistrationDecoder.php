<?php


namespace WAPDC\CampaignFinance\SubmissionDecoder;


use WAPDC\CampaignFinance\Model\Registration;

class RegistrationDecoder implements SubmissionDecoderInterface
{
  /**
   * @param Registration $registration
   * @return \stdClass
   */
  public function generateSubmission(Registration $registration)
  {
    $submission = json_decode($registration->user_data);
    if (isset($submission->committee->candidacy)) {
      $candidacy = $submission->committee->candidacy;
      if (isset($candidacy->location) && !isset($candidacy->jurisdiction)) {
        $candidacy->jurisdiction = $candidacy->location;
        unset($candidacy->location);
      }
      if (isset($candidacy->location_id) && !isset($candidacy->jurisdiction_id)) {
        $candidacy->jurisdiction_id = $candidacy->location_id;
        unset($candidacy->location_id);
      }
    }
    return $submission;
  }
}