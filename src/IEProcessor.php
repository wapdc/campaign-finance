<?php

namespace WAPDC\CampaignFinance;


use stdClass;
use Exception;
use Doctrine\DBAL\Exception as DBALException;
use WAPDC\Core\SendGrid\MailerInterface;
use WAPDC\Core\TokenProcessor;
use WAPDC\Core\EntityProcessor;
use WAPDC\Core\Model\User;

class IEProcessor
{
  static array $SPONSOR_TYPES = [
       "A" => "Individual using own funds",
       "B" => "Individual receiving donations",
       "C" => "Business, union, group or association using own funds",
       "D" =>  "Business union, group or association receiving donations",
       "E" =>  "Political committee filing C3 and C4 reports",
       "F" =>  "Political committee filing C5 reports",
       "G" =>  "Other",
  ];

  protected CFDataManager $dataManager;

  private string $project_directory;

  public function __construct(CFDataManager $dataManager)
  {
    $this->dataManager = $dataManager;
    $this->project_directory = dirname(__DIR__);
  }

  /**
   * Execute a query form the data/IE directory using dynamic query processing.
   * @param $file
   * @param array $parameters
   * @return array
   * @throws Exception
   */
  public function IEQuery($file, array $parameters = []): array
  {
    $dir = $this->project_directory . '/data/ie';
    if (!strpos($file, '.sql')) {
      $file .= ".sql";
    }
    if (!file_exists("$dir/$file")) {
      throw new Exception("Invalid Query: $dir/$file");
    }
    return $this->dataManager->executeQueryFromFile("$dir","$file", $parameters);
  }

  /**
   * Execute a query form the data/IE/admin directory using dynamic query processing.
   * @param $file
   * @param array $parameters
   * @return array
   * @throws Exception
   */
  public function IEAdminQuery($file, array $parameters = []): array {
    $dir = $this->project_directory . '/data/ie_admin';
    if (!strpos($file, '.sql')) {
      $file .= ".sql";
    }
    if (!file_exists("$dir/$file")) {
      throw new Exception("Invalid Query: $dir/$file");
    }
    return $this->dataManager->executeQueryFromFile("$dir","$file", $parameters);
  }

  /**
   * @param $entity_id
   * @param $selected_sponsor_id
   * @return string
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function redirectToLegacyC6($entity_id, $selected_sponsor_id) :string {
    if (!$entity_id) {
      throw new Exception("Valid entity_id is required");
    }

    $sponsor = (object) $this->dataManager->db->executeQuery(
      "select sponsor_id, name, addr, city, state, zip, email, phone, sponsor_desc
           from c6_sponsor where entity_id = :entity_id and sponsor_id=:sponsor_id",
      ["entity_id" => $entity_id, "sponsor_id" => $selected_sponsor_id])->fetchAllAssociative()[0];
    if (!$sponsor || !$sponsor->sponsor_id) {
      return '';
    }

    $TokenProcessor = new TokenProcessor();
    $jwt = $TokenProcessor->generateToken(['gov_wa_pdc' => [
      'sponsor_id' => $sponsor->sponsor_id,
      'name' => $sponsor->name,
      'address' => $sponsor->addr ?? '',
      'city' => $sponsor->city ?? '',
      'state' => $sponsor->state ?? '',
      'zipcode' => $sponsor->zip ?? '',
      'email' => $sponsor->email ?? '',
      'phone' => $sponsor->phone ?? '',
      'typeCode' => $sponsor->sponsor_desc
    ]], 90);
    $url = '';

    if (empty($_ENV['PANTHEON_ENVIRONMENT'])) {
      $url = 'http://localhost:56498/Account/LogOn?jwt=' . $jwt;
    } elseif ($_ENV['PANTHEON_ENVIRONMENT'] === 'demo') {
      $url = 'https://web.pdc.wa.gov/c6mvcdemo/Account/LogOn?jwt=' . $jwt;
    } elseif ($_ENV['PANTHEON_ENVIRONMENT'] === 'live') {
      $url = 'https://web.pdc.wa.gov/c6mvc/Account/LogOn?jwt=' . $jwt;
    } elseif ($_ENV['PANTHEON_ENVIRONMENT'] === 'lando') {
      $url = 'http://localhost:56498/Account/LogOn?jwt=' . $jwt;
    }

    return $url;
  }

  /**
   * @param $repno
   * @return string|array
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   * returns any reports that are not part of the sequence of amendments to which the $repno argument belongs and which have a date_filed date after the first report in the sequence.
   */
  public function fetchUnrelatedSubsequentReports($repno): string|array
  {
    $report_chain = []; // [1,2,3] => [1,3]
    $this->_getAmendedReportGroup($repno, $report_chain);
    if (!count($report_chain)) return $report_chain;
    $exclude_string = implode(",",$report_chain);
    [ $first_repno ] = $report_chain;
    $q = "select r.* from c6_reports r join (select sponsor_id, election_year, date_filed from c6_reports where repno = $first_repno ) v on r.sponsor_id = v.sponsor_id and r.election_year = v.election_year where r.date_filed > v.date_filed and r.repno not in ($exclude_string)";

    try {
      $reports = $this->dataManager->db->executeQuery($q)->fetchAllAssociative();
    } catch (Exception $e) {
      return $e->getMessage();
    }

    return $reports;
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function _getAmendedReportGroup($repno, Array &$report_chain): void
  {
    $superseded = $this->dataManager->db->executeQuery("select * from c6_reports where superseded = :repno", ['repno' => $repno])->fetchAllAssociative();
    $superseded = array_map(function ($report) { return (object) $report; }, $superseded);
    $latest = count($superseded) ? $superseded[0]->repno : null;
    if ($latest) {
      $report_chain[] = $latest;
      $this->_getAmendedReportGroup($latest, $report_chain);
      if (!in_array($repno, $report_chain)) {
        $report_chain[] = $repno;
      }
    }
  }

  /**
   * @param $request_id
   * @return void
   * @throws \Doctrine\DBAL\Exception
   */
  public function denySponsorRequest($request_id): void
  {
    $this->dataManager->db->executeStatement("delete from c6_sponsor_request where sponsor_request_id = :request_id",
      ['request_id' => $request_id]);
  }

  /**
   * @param stdClass $request
   * @return int ID of inserted record.
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function requestAccess(stdClass $request):int {
    if (empty($request->uid) || empty($request->name)) {
      throw new Exception("Missing user context");
    }
    if (empty($request->email)) {
      throw new Exception('Missing required fields (email,name)');
    }

    if (empty($request->address) || empty($request->city) || empty($request->state) || empty($request->postcode)) {
      throw new Exception("Missing contact address information");
    }

    $this->dataManager->db->executeStatement("INSERT INTO c6_sponsor_request(uid, realm, sponsor_id, sponsor_type, name, email, address, premise, city, state, postcode, phone) 
           values(:uid, :realm, :sponsor_id, :sponsor_type, :name, :email, :address, :premise, :city, :state, :postcode, :phone )",
           [
             'uid' => $request->uid,
             'realm' => $request->realm,
             'sponsor_id' => $request->sponsor_id ?? null,
             'sponsor_type' => $request->sponsor_type ?? null,
             'name' => $request->name,
             'email' => $request->email,
             'address' => $request->address,
             'premise' => $request->premise ?? null,
             'city' => $request->city,
             'state' => $request->state,
             'postcode' => $request->postcode,
             'phone' => $request->phone ?? null
           ]);
    $request->sponsor_request_id = $this->dataManager->db->lastInsertId();
    return $request->sponsor_request_id ?? 0;
  }

  /**
   * @param int $entity_id
   *   Entity id to grant access
   * @param string $sponsor_type
   *   Type of sponsor to establish.
   * @return int
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function enableC6ForEntity(int $entity_id, string $sponsor_type): int
  {
    $entityProcessor = new EntityProcessor($this->dataManager);
    if (!$entity_id) {
      throw new Exception("Missing or invalid entity_id:" . $entity_id );
    }
    $entity = $entityProcessor->getEntity($entity_id);
    if (!$entity) {
      throw new Exception("Cannot find entity!");
    }

    // Sanitize the state, force to 2 characters so legacy stops breaking
    $entity->state = $this->getStateAbbreviation($entity->state);

    if (empty(IEProcessor::$SPONSOR_TYPES[$sponsor_type])) {
      throw new Exception("Invalid Sponsor type");
    }

    $sponsor_id = $this->dataManager->db->executeQuery(
      "select sponsor_id from c6_sponsor where entity_id = :entity_id and sponsor_desc = :sponsor_type",
      [
        'entity_id' => $entity_id,
        'sponsor_type' => $sponsor_type
      ]
    )->fetchOne();

    if (!$sponsor_id) {
      $this->dataManager->db->executeQuery("insert into c6_sponsor(entity_id, name, sponsor_desc, email, addr, city, state, zip, phone)
         values(:entity_id, :name, :sponsor_desc, :email, :addr, :city, :state, :zip, :phone)",
        [
          'entity_id' => $entity_id,
          'name' => $entity->name,
          'sponsor_desc' => $sponsor_type,
          'email' => $entity->email ?? null,
          'addr' => $entity->address ?? null,
          'city' => $entity->city ?? null,
          'state' => $entity->state,
          'zip' => $entity->postcode ?? null,
          'phone' => $entity->phone ?? null,
        ]
      );
      $sponsor_id = $this->dataManager->db->lastInsertId();
    } else {
      $this->dataManager->db->executeStatement(
        "UPDATE c6_sponsor SET 
                sponsor_desc = :sponsor_desc, 
                name = :name, 
                fname = null,       
                email = :email,       
                addr = :addr, 
                city = :city, 
                state = :state,
                zip = :zip,
                phone = :phone
            WHERE sponsor_id = :sponsor_id",
        [
          'sponsor_id' => $sponsor_id,
          'sponsor_desc' => $sponsor_type,
          'name' => $entity->name,
          'email' => $entity->email,
          'addr' => $entity->address,
          'city' => $entity->city,
          'state' => $entity->state,
          'zip' => $entity->postcode,
          'phone' => $entity->phone,
        ]
      );
    }

    return (int)$sponsor_id;
  }

  /**
   * @param INt $entity_id
   *   ID of entity to check
   * @param String $role
   *   Role of entity to check
   * @param User $user
   * @return false
   * @throws \Doctrine\DBAL\Exception
   */
  public function checkC6Access(int $entity_id, string $role, User $user): bool
  {
    $userRole = $this->dataManager->db->executeQuery("select role from entity_authorization where entity_id=:entity_id and uid=:uid and realm=:realm",
      [
        'entity_id' => $entity_id,
        'uid' => $user->uid,
        'realm' => $user->realm
      ]
    )->fetchOne();
    switch ($role) {
      case 'owner':
        return $userRole == 'owner';
      case 'cf_reporter':
        return $userRole == 'owner' || $userRole == 'cf_reporter';
      case 'cf_editor':
        return $userRole == 'owner' || $userRole == 'cf_reporter' || $userRole == 'cf_editor';
      default:
        return false;
    }
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function verifySponsor(int $request_id, stdClass $adminData, MailerInterface $mailer, $testEmail = null): int
  {
    $request = $this->dataManager->db->executeQuery(
      "select sr.*, pu.user_name from c6_sponsor_request sr join pdc_user pu on sr.realm = pu.realm and sr.uid = pu.uid 
        where sponsor_request_id=:request_id",
      ['request_id' => $request_id])->fetchAllAssociative();
    if (!$request) {
      throw new Exception("Request not found to verify");
    }
    [$request] = $request;

    // Create a new entity if it does not exist.
    $entityProcessor = new EntityProcessor($this->dataManager);
    if (empty($adminData->entity_id)) {
      $entity = new stdClass();
      $entity->name = $request['name'];
      $entity->address = $request['address'];
      $entity->premise = $request['premise'];
      $entity->city = $request['city'];
      $entity->state = $request['state'];
      $entity->postcode = $request['postcode'];
      $entity->email = $request['email'];
      $entity->phone = $request['phone'];
      $entity->is_person = $request['sponsor_type'] === 'A' || $request['sponsor_type'] === 'B';
      $entity_id = $entityProcessor->saveEntity($entity);
    }
    else {
      $entity_id = $adminData->entity_id;
    }

    $mailer->setTestModeEmail($testEmail);
    $entityProcessor->grantRoleToEmail($entity_id, $request['user_name'], 'cf_reporter', $mailer);

    // Finally enable access for c6.
    $sponsor_id = $this->enableC6ForEntity($entity_id, $adminData->sponsor_type);

    if ($sponsor_id) {
      $this->dataManager->db->executeStatement("DELETE FROM c6_sponsor_request where sponsor_request_id=:request_id",['request_id' => $request_id]);
    }

    return $sponsor_id;
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function updateSponsorInfo(stdClass $sponsor): int
  {
    if (empty($sponsor->entity_id)) {
      throw new Exception("Could not find entity to update");
    }
    if (empty($sponsor->sponsor_type)) {
      throw new Exception("Missing sponsor type");
    }

    $entityProcessor = new EntityProcessor($this->dataManager);
    $entityProcessor->saveEntity($sponsor);

    return $this->enableC6ForEntity($sponsor->entity_id, $sponsor->sponsor_type);

  }

  /**
   * @param $state
   * @return bool|int|string
   */
  public function getStateAbbreviation($state): bool|int|string
  {
    $STATES = [
      'AL' => 'Alabama',
      'AK' => 'Alaska',
      'AZ' => 'Arizona',
      'AR' => 'Arkansas',
      'CA' => 'California',
      'CO' => 'Colorado',
      'CT' => 'Connecticut',
      'DE' => 'Delaware',
      'FL' => 'Florida',
      'GA' => 'Georgia',
      'HI' => 'Hawaii',
      'ID' => 'Idaho',
      'IL' => 'Illinois',
      'IN' => 'Indiana',
      'IA' => 'Iowa',
      'KS' => 'Kansas',
      'KY' => 'Kentucky',
      'LA' => 'Louisiana',
      'ME' => 'Maine',
      'MD' => 'Maryland',
      'MA' => 'Massachusetts',
      'MI' => 'Michigan',
      'MN' => 'Minnesota',
      'MS' => 'Mississippi',
      'MO' => 'Missouri',
      'MT' => 'Montana',
      'NE' => 'Nebraska',
      'NV' => 'Nevada',
      'NH' => 'New Hampshire',
      'NJ' => 'New Jersey',
      'NM' => 'New Mexico',
      'NY' => 'New York',
      'NC' => 'North Carolina',
      'ND' => 'North Dakota',
      'OH' => 'Ohio',
      'OK' => 'Oklahoma',
      'OR' => 'Oregon',
      'PA' => 'Pennsylvania',
      'RI' => 'Rhode Island',
      'SC' => 'South Carolina',
      'SD' => 'South Dakota',
      'TN' => 'Tennessee',
      'TX' => 'Texas',
      'UT' => 'Utah',
      'VT' => 'Vermont',
      'VA' => 'Virginia',
      'WA' => 'Washington',
      'WV' => 'West Virginia',
      'WI' => 'Wisconsin',
      'WY' => 'Wyoming',
    ];

    // Remove special characters except for spaces between words
    $state = preg_replace('/[^\w\s]/', '', $state);

    // Check if the input is a state abbreviation
    if (strlen($state) === 2 && array_key_exists(strtoupper($state), $STATES)) {
      return strtoupper($state);
    }

    // Normalize the input
    $state = ucwords(strtolower($state));

    // Check if the input is a state name
    if (in_array($state, $STATES)) {
      return array_search($state, $STATES);
    }

    // Find the closest state name using the similar_text function
    $bestMatch = -1;
    foreach ($STATES as $key => $value) {
      // Check if the input matches the beginning of any state name
      if (stripos($value, $state) === 0) {
        return $key;
      }
      // Check if the input is a state abbreviation that starts with the input string
      if (stripos($key, $state) === 0) {
        return $key;
      }
      // If prior checks fail, use the closest state name
      $closestState = '';
      similar_text($state, $value, $score);
      if ($score >= 80 && $score > $bestMatch) {
        $closestState = $key;
        $bestMatch = $score;
      }
      if ($closestState) {
        return $closestState;
      }
    }

    // The alternative would be to just truncate the input to the first 2 chars but then something like WaterTonia
    // becomes WA and seems valid.
    return '--';
  }

  /**
   * @param string|null $election_code
   * @return array[]
   * @throws DBALException
   */
  public function getReports(string $election_code = null): array
  {
    return $this->dataManager->db->executeQuery(
      "select r.repno, s.fname, s.name, r.election_year, r.rpt_type, r.date_filed from c6_sponsor s
        join c6_reports r on s.sponsor_id = r.sponsor_id
        where r.superseded is null
        and :election_code::text is null or :election_code::text = r.election_year",
      ['election_code' => $election_code])->fetchAllAssociative();
  }


  /**
   * Returns information about an independent expenditures report and a list of all the identified entity records
   * associated with it.
   * @param $repno
   * @return object[]
   * @throws DBALException
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function getAllReportData($repno)
  {
    $report_query = "select * from c6_sponsor s join c6_reports r on r.sponsor_id = s.sponsor_id where r.repno = :repno";
    $entity_query = "select * from c6_identified_entity ie where ie.repno = :repno order by case when ie.filer_id is null and ie.cand_lname is not null then 1 else 2 end, ie.cand_lname, ie.ballot_name";

    $report  = (object)$this->dataManager->db->fetchAllAssociative($report_query, ['repno' => $repno])[0];
    $report->entities = $this->dataManager->db->fetchAllAssociative($entity_query, ['repno' => $repno]);
    $report->subsequentReports = $this->fetchUnrelatedSubsequentReports($repno);
    $report->deletable = !count($report->subsequentReports);

    return [$report];
  }

}
