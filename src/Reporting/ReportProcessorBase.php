<?php


namespace WAPDC\CampaignFinance\Reporting;


use \Exception;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\Model\Contribution;
use WAPDC\CampaignFinance\Model\Fund;
use WAPDC\CampaignFinance\Model\Report;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\Sum;
use WAPDC\CampaignFinance\Model\Transaction;
use WAPDC\CampaignFinance\Model\Loan;
use WAPDC\CampaignFinance\Validator;
use \DateTime;
use \stdClass;

abstract class ReportProcessorBase
{

    const CONTRIBUTOR_TYPES = [
        'B',
        'C',
        'F',
        'I',
        'L',
        'O',
        'P',
        'S',
        'T',
        'U',
    ];

    const PRIM_GEN_VALUES = [
        'P',
        'G',
        'N'
    ];

    /** @var CFDataManager */
    protected $dataManager;

    /** @var Validator */
    protected $validator;

    /** @var Committee */
    protected $committee;

    /** @var Fund */
    protected $fund;

    /** @var Report */
    protected $report;

    /** @var stdClass[] */
    protected $contacts = [];

    protected $loans_sum = 0.0;

    protected $loans_count = 0;

    protected $misc_min_auction_date = '';

    protected $misc_max_auction_date = '';

    protected $loans_min_date ='';

    protected $loans_max_date = '';

    protected $contributions_min_date = '';

    protected $contributions_max_date = '';

    public $sums;

    protected $nonEmployedOccupations;

    public $thresholds;

    public $version;

    public function __construct(CFDataManager $dataManager, $version)
    {
        $this->dataManager = $dataManager;
        $this->validator = new Validator($dataManager);
        $this->nonEmployedOccupations = $this->getNonEmployedOccupations();
        $this->thresholds = $this->getThresholds();
        $this->version = (float)$version;
    }

    /**
     * Generate a transaction with appropriate data from the fund/report.
     * @param $category
     * @param $act
     * @param $amount
     * @param $transaction_date
     * @return Transaction
     * @throws \Exception
     */
    protected function generateTransaction($category, $act, $amount, $transaction_date, $itemized = true, $anonymous = false)
    {
        $transaction = new Transaction();
        $transaction->fund_id = $this->fund->fund_id;
        $transaction->created_at = new DateTime();
        $transaction->updated_at = new DateTime();
        $transaction->itemized = $itemized;
        $transaction->category = $category;
        $transaction->act = $act;
        $transaction->amount = (float)$amount;
        if (empty($transaction_date)) {
          throw new Exception("Transaction $category $act for $amount is missing transaction date.");
        }
        $transaction->transaction_date = new DateTime($transaction_date);
        $transaction->report_id = $this->report->report_id;
        return $transaction;
    }
    protected function generateSumTransaction($amount, $transaction_date) {
      $transaction = new Transaction();
      $transaction->fund_id = $this->fund->fund_id;
      $transaction->created_at = new DateTime();
      $transaction->updated_at = new DateTime();
      $transaction->itemized = true;
      $transaction->category = "contribution";
      $transaction->act = "sum";
      $transaction->amount = (float)$amount;
      $transaction->transaction_date = $transaction_date ? new DateTime($transaction_date) : null;
      $transaction->report_id = $this->report->report_id;
      return $transaction;
    }
    /**
     * @param $contact
     * @param $election
     *
     * @return Contribution
     */
    protected function generateContribution($contact, $election)
    {
        $contribution = new Contribution();
        $contribution->name = $this->replaceSpecialChars($contact->name ?? NULL);
        $contribution->address = $this->replaceSpecialChars($contact->address ?? NULL);
        $contribution->city = $this->replaceSpecialChars($contact->city ?? NULL);
        $contribution->state = $this->replaceSpecialChars($contact->state ?? NULL);
        $contribution->postcode = $this->replaceSpecialChars($contact->postcode ?? NULL);
        $contribution->contact_key = $this->replaceSpecialChars($contact->contact_key ?? NULL);
        $contribution->contributor_type = $this->replaceSpecialChars($contact->contributor_type ?? NULL);
        $contribution->prim_gen = $election;
        if (!empty($contact->employer)) {
            $e = $contact->employer;
            $contribution->employer = $this->replaceSpecialChars($e->name ?? NULL);
            $contribution->employer_address = $this->replaceSpecialChars($e->address ?? NULL);
            $contribution->employer_city = $this->replaceSpecialChars($e->city ?? NULL);
            $contribution->employer_state = $this->replaceSpecialChars($e->state ?? NULL);
            $contribution->occupation = $this->replaceSpecialChars($e->occupation ?? NULL);
            $contribution->employer_postcode = $this->replaceSpecialChars($e->postcode ?? NULL);
        }
        return $contribution;
    }

    protected function addContributionContactInformation($contribution, $contact, $election)
    {
      $contribution->name = $this->replaceSpecialChars($contact->name ?? NULL);
      $contribution->address = $this->replaceSpecialChars($contact->address ?? NULL);
      $contribution->city = $this->replaceSpecialChars($contact->city ?? NULL);
      $contribution->state = $this->replaceSpecialChars($contact->state ?? NULL);
      $contribution->postcode = $this->replaceSpecialChars($contact->postcode ?? NULL);
      $contribution->contact_key = $this->replaceSpecialChars($contact->contact_key ?? NULL);
      $contribution->contributor_type = $this->replaceSpecialChars($contact->contributor_type ?? NULL);
      $contribution->prim_gen = $election;
      $contribution->aggregate_primary = $contact->aggregate_primary ?? NULL;
      $contribution->aggregate_general = $contact->aggregate_general ?? NULL;
      if (!empty($contact->employer)) {
        $contribution->employer = new stdClass();
        $e = $contact->employer;
        $contribution->employer->name = $this->replaceSpecialChars($e->name ?? NULL);
        $contribution->employer->address = $this->replaceSpecialChars($e->address ?? NULL);
        $contribution->employer->city = $this->replaceSpecialChars($e->city ?? NULL);
        $contribution->employer->state = $this->replaceSpecialChars($e->state ?? NULL);
        $contribution->employer->postcode = $this->replaceSpecialChars($e->postcode ?? NULL);
        $contribution->occupation = $this->replaceSpecialChars($e->occupation ?? NULL);
      }
    }

    protected function addExpenseContactInformation($expense, $contact) {
      $expense->name = $this->replaceSpecialChars($contact->name ?? null);
      $expense->address = $this->replaceSpecialChars($contact->address ?? null);
      $expense->city = $this->replaceSpecialChars($contact->city ?? null);
      $expense->state = $this->replaceSpecialChars($contact->state ?? null);
      $expense->postcode = $this->replaceSpecialChars($contact->postcode ?? null);
      $expense->contact_key = $this->replaceSpecialChars($contact->contact_key ?? null);
      $expense->payee = $this->replaceSpecialChars($contact->payee ?? null);
      $expense->creditor = $this->replaceSpecialChars($contact->creditor ?? null);
      return $expense;
    }

    protected function setGlobalContacts(&$contacts) {
      foreach ($contacts as $contact) {
        $this->contacts[$contact->contact_key] = $contact;
      }
    }

  /**
   * @param null $contact_key
   * @param bool $check_employer Check if employer is required by evaluating rules. If false, the rules are skipped
   * @param bool $require_address Always require the address
   * @param string $transaction_type Kludge to control the text of the error message. Anything except contribution gets a generic message.
   * @return bool|void
   */
    protected function validateContact($contact_key = null, $check_employer = true, $require_address = true, $transaction_type = "contribution") {
      if (empty($contact_key) or empty($this->contacts[$contact_key])) {
        $this->validator->addError("contact.contact_key.invalid", "Could not find contact record for contact key '$contact_key'.");
        return;
      }
      if (isset($this->contacts[$contact_key]->valid)) {
        return $this->contacts[$contact_key]->valid;
      }
      $contact = $this->contacts[$contact_key];
      $contact_array_key = array_search($contact_key, array_keys($this->contacts));
      $this->validator->validateRequired($contact, ['name'], "contact.$contact_array_key", "Error: Name for contact key $contact->contact_key");
      $this->validator->validateContactProperties($contact);
      if ($this->validator->isValid()) {
        $this->validator->validateList($contact, 'contributor_type', static::CONTRIBUTOR_TYPES, "contact.$contact_array_key", "Error: Contributor type for " . $contact->name);
        if (!empty($contact->aggregate_general) || !empty($contact->aggregate_primary)) {
          if ($contact->contributor_type != 'S' && ($require_address === true || (float)$contact->aggregate_general > $this->thresholds->itemized_contributions || (float)$contact->aggregate_primary > $this->thresholds->itemized_contributions)
            && (empty($contact->address) || empty($contact->city) || empty($contact->state) || empty($contact->postcode))) {
            $message = '%s requires address when aggregate contributions exceed $' . $this->thresholds->itemized_contributions . '.';
            if ($transaction_type <> 'contribution') {
              $message = 'Address required for %s';
            }
            $this->validator->addMessage("contact.$contact_array_key" . ".address.incomplete", 'Warning: ' . sprintf($message, $contact->name));
          }
          $occupation = !empty($contact->employer->occupation) ? strtoupper($contact->employer->occupation) : null;
          if ((strtoupper($contact->contributor_type) == 'I')
            && $check_employer === true
            && ((float)$contact->aggregate_general > $this->thresholds->report_occupation || (float)$contact->aggregate_primary > $this->thresholds->report_occupation)
            && (!in_array($occupation, $this->nonEmployedOccupations))) {
            $employer_name = !empty($contact->employer->name) ? $contact->employer->name : null;
            $employer_city = !empty($contact->employer->city) ? $contact->employer->city : null;
            $employer_state = !empty($contact->employer->state) ? $contact->employer->state : null;
            if (empty($occupation)) {
              $this->validator->addMessage("contact.$contact_array_key" . ".employer.incomplete", "Warning: " . $contact->name . " requires occupation information when contributions exceed $" . $this->thresholds->report_occupation . '.');
            }
            elseif ((empty($employer_name) || empty($employer_city) || empty($employer_state))) {
              $this->validator->addMessage("contact.$contact_array_key" . ".employer.incomplete", "Warning: " . $contact->name . " requires employer city and state when contributions exceed $" . $this->thresholds->report_occupation . '.');
            }
          }
        }
        $contact->valid = true;
        return $contact->valid;
      }
    }

  /**
   * @param null $name
   * @return string
   */
    protected function validateCandidateName($name = null) {
        $elements = explode(' ', $name);
        foreach ($elements as $idx => $element) {
            strlen($element) > 1 ?: array_splice($elements, $idx, 1);
        }
        $validated_name = join(" ", $elements);
        return $validated_name;
    }

  /**
   * @param array $loans
   * @param string $prefix
   */
   protected function validateLoans(array $loans, $prefix = "loans", $require_address = false) {
     foreach ($loans as $l => $loan) {
       $this->loans_sum += !empty($loan->amount) ? (float)$loan->amount : 0;
       $this->loans_count++;
       $loan->in_kind = $loan->in_kind ?? false;
       $loan->carry_forward = $loan->carry_forward ?? false;
       if($loan->carry_forward) {
           if(empty($loan->description)){
               $this->validator->addMessage("", "Warning: carry forward loan for $" . $loan->amount . " requires description.");
           }
       }
       $date = $loan->date ?? NULL;
       $this->checkMinMaxDate($date, 'loans');
       $l_msg = "The $date loan";
       $this->validator->validateRequired($loan, ['amount', 'contact_key', 'date', 'election'], "$prefix.$l", $l_msg);
       if ($this->validator->isValid()) {
         if (empty($this->contacts[$loan->contact_key])) {
           $this->validator->addError("loan.$l.contact_key.invalid", "Contact information not provided for loan $l for date $date");
         }
         else {
          $this->validateContact($loan->contact_key, true, $require_address);
         }
         $this->validator->validateMoney($loan->amount, "$prefix.$l.amount", "Error: $l_msg amount is invalid");
         $this->validator->validateMoney($loan->interest_rate, "$prefix.$l.interest_rate", "Error: $l_msg interest rate is invalid");
         $this->validator->validateDate($loan->date, "$prefix.$l.date", "The loan date must be a valid date");
         $this->validator->validateDate($loan->due_date, "$prefix.$l.due_date", "The due date must be a valid date");
         if (strlen($loan->payment_schedule) > 1000) {
           $this->validator->addError("loans.$l.payment_schedule.length", "Description of repayment schedule must be less than 1000 characters");
         }
         $this->validator->validateList($loan, 'election', static::PRIM_GEN_VALUES, "$prefix.$l", "Error: $l_msg election is not valid");
         if (!empty($loan->endorser)) {
           foreach ($loan->endorser as $e => $endorser) {
             $this->validator->validateRequired($endorser, ['contact_key', 'liable_amount'], "$prefix.$l.endorser.$e", "$l_msg");
             $this->validator->validateMoney($endorser->liable_amount, "$prefix.$l.endorser.$e.liable_amount", "Error: $l_msg endorser liable amount is required");
             $this->validator->validateList($endorser, 'election', static::PRIM_GEN_VALUES, "$prefix.$l.endorser.$e", "Error: $l_msg endorser election is not valid");
           }
        }
      }
    }
  }

  protected function checkMinMaxDate($date, $category) {
    switch($category) {
      case 'misc_auction':
        $this->misc_min_auction_date = ($date < $this->misc_min_auction_date || empty($this->misc_min_auction_date)) ? $date : $this->misc_min_auction_date;
        $this->misc_max_auction_date = ($date > $this->misc_max_auction_date || empty($this->misc_max_auction_date)) ? $date : $this->misc_max_auction_date;
        break;
      case 'loans':
        $this->loans_min_date = ($date < $this->loans_min_date || empty($this->loans_min_date)) ? $date : $this->loans_min_date;
        $this->loans_max_date = ($date > $this->loans_max_date || empty($this->loans_max_date)) ? $date : $this->loans_max_date;
        break;
      case 'itemized_contributions':
        $this->contributions_min_date = ($date < $this->contributions_min_date || empty($this->contributions_min_date)) ? $date : $this->contributions_min_date;
        $this->contributions_max_date = ($date < $this->contributions_max_date || empty($this->contributions_min_date)) ? $date : $this->contributions_max_date;
        break;
    }
  }

    /**
     * @return stdClass
     *
     * This function has access to sums and validator messages can is called to add these to C3/C4 reports
     */
    public function getValidatorInfo() {
        $response_data = new stdClass();
        $response_data->metadata = json_encode(['sums' => $this->sums]);
        $response_data->messages = $this->validator->getMessages();
        $response_data->message = $this->validator->getMessageText();
        return $response_data;
    }

  /**
   * Saved report.
   * @param $report_data
   * @param Fund $fund
   * @return Report
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
    protected function saveReport($report_data, Fund $fund)
    {
        // Save the report
        $report = $this->report = new Report();
        // Type and coverage
        $report->report_type = strtoupper($report_data->report_type);
        $report->fund_id = $fund->fund_id;
        $report->period_start = new DateTime($report_data->period_start);
        $report->period_end = new DateTime($report_data->period_end);
        $report->submitted_at = new DateTime($report_data->submitted_at);
        $report->election_year = $report_data->election_year ?? $report_data->election_code;
        $report->filer_name = $report_data->committee->name;
        $report->office_code = $report_data->committee->office_code ?? NULL;
        $report->final_report = $report_data->final_report ?? FALSE;
        $report->version = $this->version;
        $report->user_data = json_encode($report_data);
        $report->source = $report_data->vendor_name ?? $fund->vendor ?? 'unknown vendor';
        $report->metadata = json_encode(['sums' => $this->sums]);
        $report->external_id = $report_data->external_id ?? NULL;

        // obtain candidate name if pac_type is 'candidate'
        if ($this->committee->pac_type === 'candidate') {
            $row = $this->dataManager->db
              ->executeQuery('select p.name, ca.office_code from candidacy ca join person p on ca.person_id = p.person_id where ca.committee_id = :committee_id', ['committee_id' => $this->committee->committee_id])
              ->fetchAssociative();
            if (empty($row)) {
              throw new Exception('Error: Unable to locate a candidacy for committee ' . $this->committee->committee_id);
            }
            $report->candidate_name = !empty($row['name']) ? $this->validateCandidateName($row['name']) : null;
            $report->office_code = !empty($row['office_code']) ? $this->validateCandidateName($row['office_code']) : null;
        }

        // Committee contact
        $report->address1 = $report_data->committee->address ?? NULL;
        $report->city = $report_data->committee->city ?? NULL;
        $report->state = $report_data->committee->state ?? NULL;
        $report->postcode = $report_data->committee->postcode ?? NULL;
        $report->treasurer_name = $report_data->treasurer->name ?? NULL;
        $report->treasurer_phone = $report_data->treasurer->phone ?? NULL;
        $report->treasurer_date = !empty($report_data->treasurer->date) ? new DateTime($report_data->treasurer->date) : NULL;

        // Save
        $this->dataManager->em->persist($report);
        $this->dataManager->em->flush($report);
        // Amendment Processing
        if (!empty($report_data->amends)) {
            /** @var Report $amended_report */
            $amended_report = $this->dataManager->em->find(Report::class, $report_data->amends);
            if (!$amended_report) {
                throw new Exception("Amended report not found");
            }
            $amended_report->superseded_id = $report->report_id;
            $this->dataManager->em->flush($amended_report);
        }

        return $report;
    }

    /**
     * Saves the loan records associated with a C3 or C4 .
     *
     * @param $loans
     * @param $isC4 - true if saving C4 loans, false if saving C3 loans
     * @throws Exception
     */
    protected function saveLoans($loans)
    {
        foreach ($loans as $l) {

            $l->carry_forward = $l->carry_forward ?? false;
            $l->in_kind = $l->in_kind ?? false;

            if ($l->carry_forward) {
                $loan_transaction = $this->generateTransaction('loan', 'carry_forward', $l->amount, $l->date);
            } else {
                $loan_transaction = $this->generateTransaction('loan', 'receipt', $l->amount, $l->date);
            }
            $loan_transaction->inkind = $l->in_kind ?? false;

            if($l->in_kind || $l->carry_forward) {
                $loan_transaction->description = $l->description;
            }

            $this->dataManager->em->persist($loan_transaction);
            $this->dataManager->em->flush($loan_transaction);

            $loan = new Loan();
            $loan->transaction_id = $loan_transaction->transaction_id;

            $due_date = (string)$l->due_date ?? NULL;
            if (!empty ($due_date)) {
                $loan->due_date = (new DateTime($due_date));
            } else {
                $loan->due_date = NULL;
            }
            $loan->interest_rate = $l->interest_rate;
            $loan->payment_schedule = $l->payment_schedule;
            $loan->lender_endorser = 'L';
            $loan->loan_date = !empty($l->date) ? new DateTime($l->date) : NULL;
            $loan->liable_amount = 0.0;
            $this->dataManager->em->persist($loan);
            $this->dataManager->em->flush($loan);

            $contact = $this->contacts[$l->contact_key] ?? NULL;
            if ($contact) {
                $contribution = $this->generateContribution($contact, $l->election);
                $contribution->transaction_id = $loan_transaction->transaction_id;
                $contribution->prim_gen = $l->election;
                $this->dataManager->em->persist($contribution);
                $this->dataManager->em->flush($contribution);

                if (($l->election != 'P' && isset($contact->aggregate_general)) || ($l->election == 'P' && isset($contact->aggregate_primary))) {
                    $sum = new Sum();
                    $sum->transaction_id = $loan_transaction->transaction_id;
                    $sum->aggregate_amount = $l->election == 'P' ? $contact->aggregate_primary : $contact->aggregate_general;
                    $this->dataManager->em->persist($sum);
                    $this->dataManager->em->flush($sum);
                }
            }

            if (!empty($l->endorser)) {
                foreach ($l->endorser as $le) {
                    // Endorsements generate a transaction but the amount is always 0. The liable_amount on the loan
                    // record indicates the endorser's liability.
                    $endorser_trans = $this->generateTransaction('loan', 'endorsement', 0.0, $l->date);
                    $endorser_trans->parent_id = $loan_transaction->transaction_id;
                    $this->dataManager->em->persist($endorser_trans);
                    $this->dataManager->em->flush($endorser_trans);

                    $endorser_loan = new Loan();
                    $endorser_loan->transaction_id = $endorser_trans->transaction_id;
                    $endorser_loan->lender_endorser = 'E';
                    $endorser_loan->liable_amount = $le->liable_amount;
                    $this->dataManager->em->persist($endorser_loan);
                    $this->dataManager->em->flush($endorser_loan);

                    $contact = $this->contacts[$le->contact_key] ?? NULL;
                    if ($contact) {
                        $endorser_contribution = $this->generateContribution($contact, $le->election);
                        $endorser_contribution->transaction_id = $endorser_trans->transaction_id;
                        $this->dataManager->em->persist($endorser_contribution);
                        $this->dataManager->em->flush($endorser_contribution);

                        if (($le->election != 'P' && isset($contact->aggregate_general)) || ($le->election == 'P' && isset($contact->aggregate_primary))) {
                            $sum = new Sum();
                            $sum->transaction_id = $endorser_trans->transaction_id;
                            $sum->aggregate_amount = $le->election == 'P' ? $contact->aggregate_primary : $contact->aggregate_general;
                            $this->dataManager->em->persist($sum);
                            $this->dataManager->em->flush($sum);
                        }
                    }
                }

            }
        }
    }

    /**
     * @param stdClass $report_data
     * @param Fund $fund
     * @return bool
     * @throws Exception
     */
    protected function validateReport(stdClass $report_data, Fund $fund)
    {
        $this->fund = $fund;

        if(empty($report_data->metadata->submission_version) && empty($report_data->version)) {
            $report_data->version = $this->version;
        }
        // Proper and complete report
        if (!isset($report_data->committee->name) || empty($report_data->committee->name)) {
          $this->validator->addError('name.required', "Committee name is required.");
        }
        if (isset($report_data->committee->name) && strlen($report_data->committee->name) > 1000) {
          // Just so we don't get a book
          $this->validator->addError('name.length', "Committee name must be less than 1000 characters.");
        }
        $this->validator->validateRequired($report_data, ['report_type', 'submitted_at', "period_start", "period_end"], "report", "Report");
        $this->validator->validateList($report_data, "report_type", ['c3', 'c4'], "report", "The report submitted");
        $this->validator->validateDate($report_data->submitted_at ?? NULL, "report.submitted_at", "The date the report was submitted must be a valid date. ");
        $this->validator->validateDate($report_data->period_start, "report.period_start", "The starting coverage date for the report must be a valid date. ");
        $this->validator->validateDate($report_data->period_end, "report.period_end", "The ending coverage date for the report must be a valid date. ");

        $this->trimObjectPropertyString($report_data);

        // Don't continue if we don't have a minimal report.
        if (!$this->validator->isValid()) {
            return false;
        }

        // Check to make sure that period_end is within one month of the current date
        $period_end = new DateTime($report_data->period_end);
        $max_period_end = new DateTime();
        $max_period_end->modify('+1 month');
        if ($period_end > $max_period_end) {
          $this->validator->addError("report.period_end", "The ending coverage date for the report must be within one month of the current date.");
        }

        if ($fund->committee_id) {
            $this->committee = $this->dataManager->em->find(Committee::class, $fund->committee_id);

            // Make sure that the committee is verified.
            if (!$this->committee->filer_id) {
                $message = "The committee on the submitted report has not been verified.";
                $this->validator->addError("committee.unverified", $message);
            }

            // Make sure the coverage period is correct for the election year
            if ($this->committee->continuing) {
                $election_year = (int)(new DateTime($report_data->period_start))->format('Y');
                // if an election code exists and the report year is after it throw an error
                if (! empty($this->committee->election_code)) {
                    if (
                        ($election_year <> $report_data->election_year) &&
                        ($election_year > (int)$this->committee->election_code)
                       ) {
                        $this->validator->addError('report.election_year.invalid', "The election year on this report (" . $report_data->election_year . ") conflicts with the start date (" . $report_data->period_start . ").\n");
                    }
                }
                else {
                    if (
                        ($election_year <> $report_data->election_year) &&
                        ($this->committee->pac_type != 'surplus')
                    ){
                        $this->validator->addError('report.election_year.invalid', "The election year on this report (" . $report_data->election_year . ") conflicts with the start date (" . $report_data->period_start . ").\n");
                    }
                }
            }
        }

        // Amendment checking logic.
        if (!empty($report_data->amends) && $report_data->amends != -1) {
            $original_report_id = $report_data->amends;
            /** @var Report $original_report */
            $original_report = $this->dataManager->em->find(Report::class, $report_data->amends);
            if (!$original_report) {
              $this->validator->addError('amendment.missing-report', "The original report cannot be found to amend: $original_report_id");
            }
            else if ($original_report->fund_id <> $fund->fund_id) {
              $this->validator->addError('amendment.wrong-fund', "The submitted report is under a different fund than the original report: $original_report_id");
            }
            else if (!empty($original_report->superseded_id)) {
              $this->validator->addError('amendment.report-already-amended', "An amendment already exists for report: $original_report_id");
            }
        }
        else {
          unset($report_data->amends);
        }

        if (!empty($report_data->contacts)) {
          $this->setGlobalContacts($report_data->contacts);
        }  else {
          $this->contacts = [];
        }

        return $this->validator->isValid();
    }

  /**
   * @param $report_data
   * @param Fund $fund
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   */
    public function submitReport($report_data, Fund $fund)
    {
        $response = new stdClass();
        $response->success = FALSE;
        $response->messages = [];
        $this->fund = $fund;

        $response->success = $this->validator->isValid();
        if ($response->success) {
            $report = $this->saveReport($report_data, $fund);
            $report_period_text = strtoupper($report->report_type) == 'C3' ? 'a deposit made on ' : '';
            $report_period_text .= date_format($report->period_start, 'm-d-Y');
            if (!empty($report->period_end) && $report->period_end <> $report->period_start && strtoupper($report->report_type) <> 'C3') {
              $report_period_text .= ' through ' . date_format($report->period_end, 'm-d-Y');
            }
            $msg = sprintf('%s (election year %d, fund id %d): Your %s for %s was successfully transmitted on %s with confirmation #%d.',
              $report->filer_name, $report->election_year, $report->fund_id, $report->report_type, $report_period_text, date_format($report->submitted_at, 'D M j G:i:s T Y'), $report->report_id);
            $this->validator->addMessage('success', $msg);
            $response->report_id = $report->report_id;
            $response->fund_id = $report->fund_id;
        }
        return $response;
    }

    /**
     * @param $category
     * @param $date
     * @param $amount
     * @param $aggregate_amount
     * @param $legacy_line_item
     * @param $tx_count
     * @throws Exception
     */
    function saveSum($category, $date, $amount, $aggregate_amount = null, $legacy_line_item = null, $tx_count = null)
    {
        $transaction = $this->generateSumTransaction($amount, $date);
        $this->dataManager->em->persist($transaction);
        $this->dataManager->em->flush($transaction);
        $sum = new Sum();
        $sum->transaction_id = $transaction->transaction_id;
        $sum->aggregate_amount = $aggregate_amount;
        $sum->legacy_line_item = $legacy_line_item;
        $sum->tx_count = $tx_count;
        $this->dataManager->em->persist($sum);
        $this->dataManager->em->flush($sum);
    }

    function trimObjectPropertyString (&$data) {
      foreach($data as $key => $value) {
          switch(TRUE) {
          case is_object($value):
            $this->trimObjectPropertyString($value);
            break;
          case is_array($value):
            foreach ($value as $element){
              if (is_object($element)) {
                $this->trimObjectPropertyString($element);
              }
            }
            break;
          case is_string($value):
            $data->$key = $this->trimStringInput($value);
            break;
        }
      }
    }

  protected function saveEmailProperties($fund_id, $properties) {
    $q = 'select private.cf_save_submission_properties(:fund_id, :properties)';
    $params = ['fund_id' => $fund_id, 'properties' => json_encode($properties)];
    $this->dataManager->db->executeQuery($q, $params)->fetchAllAssociative();
  }

    function trimStringInput(String $field_data) {
      $field_data = trim($field_data);
      if (empty($field_data)) {
        $field_data = NULL;
      }
     return $field_data;
    }

  /**
   * @param $input
   * @return string|null
   */
    function replaceSpecialChars($input) {
      if (empty($input)) {
        return null;
      }
      return preg_replace('/[\n\r]+/', ' ', $input);
    }

  /**
   * @return mixed[]
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
    function getNonEmployedOccupations() {
      return $this->dataManager->db
        ->executeQuery('select upper(label) as label from ui_occupation_list where employed = false')
        ->fetchFirstColumn();
    }

    /**
     * @return object
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    function getThresholds(){
        $thresholds = $this->dataManager->db->executeQuery('select property, amount from public.thresholds')->fetchAllAssociative();
        $arr = array();
        foreach ($thresholds as $threshold){
            $arr[$threshold['property']] = $threshold['amount'];
        }
        return (object)$arr;
    }
}
