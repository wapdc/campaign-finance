<?php

namespace WAPDC\CampaignFinance\Reporting;

use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use stdClass;
use WAPDC\CampaignFinance\Model\AuctionItem;
use WAPDC\CampaignFinance\Model\Fund;
use WAPDC\CampaignFinance\Model\Sum;

class C3ReportProcessor extends ReportProcessorBase {

  protected $misc_auction_sum = 0.0;

  protected $misc_auction_count = 0;

  protected $contributions_sum = 0;

  protected $contributions_count = 0;

  protected $personal_funds_sum = 0.0;

  protected $personal_funds_count = 0;

  protected $sums_to_process = ['anonymous' => '1A', 'personal' => '1B', 'loans' => '1C', 'misc_auction' => '1D', 'small' => '1E', 'itemized_contributions' => '2'];

  /**
   * Validates a C3 Report.
   *
   * @param stdClass $report_data
   * @param Fund $fund
   * @return bool
   * @throws Exception
   */
  public function validateReport(stdClass $report_data, Fund $fund) {
    $this->misc_auction_sum = 0.0;
    $this->misc_auction_count = 0;
    $this->loans_count = 0;
    $this->loans_sum = 0.0;
    $this->personal_funds_sum = 0.00;
    $this->personal_funds_count = 0;
    $this->contributions_sum = 0.00;
    $this->contributions_count = 0;
    parent::validateReport($report_data, $fund);

    if ($report_data->period_start != $report_data->period_end) {
      $this->validator->addError("period_end.invalid", "Reporting period start and end must the same (date of the deposit) on a C3 report.");
    }

    // Validate external id
    if(empty($report_data->external_id) && $report_data->version > 1.0) {
        //$this->dataManager->db->fetchOne()
        $this->validator->addError("external_id.required",
            "Error: external_id required.");
    }

    // Validate contributions
    if (!empty($report_data->contributions)) {
      $this->validateContributions($report_data->contributions);
    }
    if (!empty($report_data->auctions)) {
      $this->validateAuctions($report_data->auctions);
    }
    if (!empty($report_data->loans)) {
      $this->validateLoans($report_data->loans);
    }
    if (!empty($report_data->misc)) {
      $this->validateMiscellaneous($report_data->misc);
    }
    $this->validateContributionLimits($fund->fund_id, $this->contacts);
    $this->calculateSums($report_data);
    return $this->validator->isValid();
  }

  /**
   * @param array $contributions
   */
  function validateContributions(array $contributions) {
    foreach ($contributions as $c => $contribution) {
      if($contribution->contact_key && !isset($this->contacts[$contribution->contact_key])) {
        $this->validator->addError("contribution.$c.contact.missing",
          "Error: Contribution for $contribution->amount on $contribution->date does not have a corresponding contact matching key $contribution->contact_key.");
      }
      $name = $this->contacts[$contribution->contact_key]->name ?? "Anonymous ";
      $this->validator->validateRequired($contribution, ['amount'], "contribution.$c", "Error: Contribution for $name");
      if ($this->validator->isValid()) {
        if (!empty($this->contacts[$contribution->contact_key])) {
          $this->validateContact($contribution->contact_key, true, false);
        }
        $this->validator->validateMoney($contribution->amount, "contribution.$c.amount", "Error: Contribution amount for $name");
        $this->validator->validateList($contribution, 'election', static::PRIM_GEN_VALUES, "contribution.$c", "Error: Election for $name");
        $this->checkMinMaxDate($contribution->date, "itemized_contributions");
        $this->contributions_sum += (float)$contribution->amount;
        $this->contributions_count++;
      }
    }
  }

  /**
   * @param $fund_id
   * @param array $contributions
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  function validateContributionLimits($fund_id, array $contacts) {
    $contacts_json = json_encode(array_values($contacts));
    $contributors_json = $this->dataManager->db->executeQuery('select cf_report_validate_contribution_limits(cast(:fund_id as integer), :contacts)', ['fund_id' => $fund_id, 'contacts' => $contacts_json])->fetchOne();
    $contributors = $contributors_json ? json_decode($contributors_json) : null;
    $has_limit = $this->dataManager->db->executeQuery('select cf_validate_campaign_has_limits(cast(:fund_id as integer))', ['fund_id' => $fund_id])->fetchOne();

    if ($contributors && $has_limit) {
      foreach ($contributors as $c => $contributor) {
        if ($contributor->overlimit) {
          $formatted_amount = '$' .  number_format($contributor->amount, 2);
          $this->validator->addMessage("contacts.$c.over_limit", "$contributor->name is over the contribution limit of $formatted_amount");
        }
      }
    }
  }

  /**
   * @param array $auctions
   */
  protected function validateAuctions(array $auctions) {
    foreach($auctions as $a => $auction) {
      $date = $auction->date ?? NULL;
      $this->checkMinMaxDate($date, 'misc_auction');
      $a_msg = "The auction on $date";
      if (empty($auction->items) || count($auction->items) == 0 || empty($auction->date)) {
        $this->validator->addError("auction.$a.incomplete", "$a_msg is incomplete ");
      }
      elseif (!is_array($auction->items)) {
        $this->validator->addError("auction.$a.items.invalid", "An auction items list is invalid for the auction dated $date");
      }
      else {
        foreach($auction->items as $i => $item) {
          $i_msg = $item->description ?? "unknown";
          $this->validator->validateRequired($item, ['description', 'market_value', 'buy', 'donate'], "auction.$a.item.$i","$a_msg, item $i_msg");
          $this->validator->validateRequired($item->buy, ['contact_key', 'amount'], "auction.$a.item.$i.buy", "$a_msg, item $i_msg buy");
          $this->validator->validateRequired($item->donate, ['contact_key', 'amount'], "auction.$a.item.$i.donate", "$a_msg, item $i_msg donate");
          if ($this->validator->isValid()) {
            if (empty($this->contacts[$item->buy->contact_key])) {
              $this->validator->addError("auction.$i.buy.contact_key.invalid", "Contact information not found for auction item $i buy receipt for '$i_msg'");
            }
            else {
              $this->validateContact($item->buy->contact_key, true, true);
            }
            if (empty($this->contacts[$item->donate->contact_key])) {
              $this->validator->addError("auction.$i.donate.contact_key.invalid", "Contact information not found for auction item $i donation receipt for '$i_msg'");
            }
            else {
              $this->validateContact($item->donate->contact_key, true, true);
            }
            $this->validator->validateMoney($item->market_value, "auction.$a.item.$i.market_value", "$a_msg, time $i_msg fair market value");
            $this->validator->validateMoney($item->buy->amount, "auction.$a.item.$i.buy.amount", "$a_msg, time $i_msg fair buy amount must be a number");
            $this->validator->validateMoney($item->donate->amount, "auction.$a.item.$i.donate.amount", "$a_msg, time $i_msg donation amount must be number");
            $this->validator->validateList($item->donate, 'election', static::PRIM_GEN_VALUES, "auction.$a.item.$i.donate", "$a_msg, time $i_msg donation");
            $this->validator->validateList($item->buy, 'election', static::PRIM_GEN_VALUES, "auction.$a.item.$i.buy", "$a_msg, time $i_msg buy");
            $this->misc_auction_sum += (float)$item->buy->amount + (float)$item->donate->amount;
            $this->misc_auction_count++;
          }
        }
      }
    }
  }

  /**
   * @param $misc_records
   */
  protected function validateMiscellaneous($misc_records) {
    foreach($misc_records as $m => $misc) {
      $desc = $misc->description ?? 'Unknown';
      $this->validator->validateRequired($misc, ['contact_key', 'amount', 'date'], "misc.$m","Miscellaneous receipt $desc");
      if ($this->validator->isValid()) {
        if (empty($this->contacts[$misc->contact_key])) {
          $this->validator->addError("misc.$m.contact_key.invalid", "Contact information not found for misc receipt $desc");
        }
        else {
          $this->validateContact($misc->contact_key, true, true);
        }
        $this->validator->validateMoney($misc->amount, "misc.$m.amount", "$desc amount must be a number");
        $this->validator->validateDate($misc->date, "misc.$m.date", "Date for misc receipt $desc must be a valid date");
        if(strlen($desc) > 1000) {
          $this->validator->addError("misc.$m.description.length", "The description for the miscellaneous receipt must be less than 1000 characters");
        }
        $this->checkMinMaxDate($misc->date, 'misc_auction');
        if (!empty($misc->personal_funds)) {
          $this->personal_funds_count++;
          $this->personal_funds_sum += (float) $misc->amount;
        } else {
          $this->misc_auction_sum += (float) $misc->amount;
          $this->misc_auction_count++;
        }
      }
    }
  }

  /**
   *  Calculates all sums and places them on the instance.
   *
   * @param $report_data
   * @throws Exception
   *
   */
  public function calculateSums($report_data)
  {
    $sums = json_decode(json_encode($report_data->sums));

    foreach($this->sums_to_process as $category => $legacy_line_item) {
      if (isset($sums->$category)) {
        $sum = $sums->$category;
      } else {
        $sum = $sums->$category = new stdClass();
        $sum->amount = null;
      }
      $sum->aggregate = !empty($sum->aggregate) ? (float) $sum->aggregate : NULL;
      switch ($category) {
        case 'loans':
          $sum->amount = (float)round($this->loans_sum, 2);
          if($this->loans_min_date != $this->loans_max_date) {
            $sum->date = null;
          } else {
            $sum->date = !empty($this->loans_min_date) ? (new DateTime($this->loans_min_date))->format('Y-m-d') : null;
          }
          $sum->count = (int)$this->loans_count;
          break;
        case 'misc_auction':
          $sum->amount = (float)round($this->misc_auction_sum, 2);
          if ($this->misc_min_auction_date != $this->misc_max_auction_date) {
            $sum->date = null;
          } else {
            $sum->date = !empty($this->misc_min_auction_date) ? (new DateTime($this->misc_min_auction_date))->format('Y-m-d') : null;
          }
          $sum->count = (int)$this->misc_auction_count;
          break;
        case 'anonymous':
          $sum->date = !empty($sum->date) ? (new DateTime($sum->date))->format('Y-m-d') : null;
          break;
        case 'personal':
          if (isset($sum->amount)) {
            $amount = $sum->amount ? $sum->amount : 0.0;
            $sum->amount = $sum->amount >= $this->personal_funds_sum ? round($amount, 2) : round($this->personal_funds_sum, 2);
          }
          $sum->date = !empty($sum->date) ? (new DateTime($sum->date))->format('Y-m-d') : null;
          $sum->aggregate = $sum->aggregate ?? 0.0;
          break;
        case 'small':
          if (isset($sum->amount)) {
            $amount = $sum->amount ? $sum->amount : 0.0;
            $sum->amount = !empty($sum->amount) ? (float)round($amount, 2) : 0;
          }
          $sum->date = !empty($sum->date) ? (new DateTime($sum->date))->format('Y-m-d') : null;
          $sum->count = !empty($sum->count) ? (int)$sum->count : NULL;
          break;
        case 'itemized_contributions':
          $sum->amount = round($this->contributions_sum, 2);
          if ($this->contributions_min_date != $this->contributions_max_date) {
            $sum->date = null;
          } else {
            $sum->date = !empty($this->contributions_min_date) ? (new DateTime($this->contributions_min_date))->format('Y-m-d') : null;
          }
          $sum->count = !empty($sum->count) ? (int)$sum->count : $this->contributions_count;
          break;
      }
    }
    $this->sums = $sums;
  }

  /**
   * Saves the contribution records associated with a C3.
   *
   * Note that because C3 contribution lists can be quite a large number of
   * transactions.  We need to not use the ORM to save them.
   *
   * @param $contributions
   * @throws Exception
   */
  protected function saveContributions($contributions) {
    foreach ($contributions as $contribution) {
      $contact = $this->contacts[$contribution->contact_key] ?? NULL;
      if ($contact) {
        $this->addContributionContactInformation($contribution, $contact, $contribution->election ?? 'N');
      }
    }

    $json = json_encode($contributions);
    $this->dataManager->db->executeStatement('select cf_save_contributions(:fund_id, :report_id, :json)',
      [
        'fund_id' => $this->report->fund_id,
        'report_id' => $this->report->report_id,
        'json' => $json
      ]
    );

  }

  /**
   * @param $auctions
   * @throws Exception
   */
  protected function saveAuctions($auctions) {
    foreach ($auctions as $auction) {

      $auction_date = !empty($auction->date) ? $auction->date : NULL;

      foreach ($auction->items as $item) {
        $donor = $this->contacts[$item->donate->contact_key];
        $buyer = $this->contacts[$item->buy->contact_key];
        $d_tran = $this->generateTransaction('contribution', 'auction donation', $item->donate->amount, $auction_date);
        $b_tran = $this->generateTransaction('contribution', 'auction buy', $item->buy->amount, $auction_date);
        $d_con = $this->generateContribution($donor, $item->donate->election);
        $b_con = $this->generateContribution($buyer, $item->buy->election);
        $b_sum = new Sum();
        $d_sum = new Sum();
        $d_sum->aggregate_amount = $item->donate->election == 'P' ? $donor->aggregate_primary : $donor->aggregate_general;
        $b_sum->aggregate_amount = $item->buy->election == 'P' ? $buyer->aggregate_primary : $buyer->aggregate_general;

        $this->dataManager->em->persist($d_tran);
        $this->dataManager->em->persist($b_tran);
        $this->dataManager->em->flush([$d_tran, $b_tran]);

        $auction_item = new AuctionItem();
        $auction_item->donation_transaction_id = $d_tran->transaction_id;
        $auction_item->buy_transaction_id = $b_tran->transaction_id;
        $auction_item->item_description = $item->description;
        $auction_item->fair_market_value = $item->market_value;
        $auction_item->sale_amount = $item->buy->amount + $item->donate->amount;

        $d_con->transaction_id = $d_tran->transaction_id;
        $d_sum->transaction_id = $d_tran->transaction_id;
        $b_con->transaction_id = $b_tran->transaction_id;
        $b_sum->transaction_id = $b_tran->transaction_id;


        $this->dataManager->em->persist($auction_item);
        $this->dataManager->em->persist($d_con);
        $this->dataManager->em->persist($b_con);
        $this->dataManager->em->persist($d_sum);
        $this->dataManager->em->persist($b_sum);
        $this->dataManager->em->flush([$auction_item, $d_con, $b_con, $b_sum, $d_sum]);
      }

    }
  }

  /**
   * @param $sums
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  function saveSums() {
    foreach($this->sums_to_process as $category => $legacy_line_item) {
      $sum = $this->sums->$category;
      $this->saveSum($category, $sum->date, $sum->amount, $sum->aggregate, $legacy_line_item, $sum->count ?? 0);
    }
  }

  /**
   * @param $misc_records
   * @throws Exception
   */
  protected function saveMiscellaneous($misc_records) {
    foreach ($misc_records as $misc) {
      $transaction = $this->generateTransaction('contribution', 'miscellaneous', $misc->amount, $misc->date);
      $contact = $this->contacts[$misc->contact_key];
      $contribution = $this->generateContribution($contact, null);
      $transaction->description = $misc->description ?? null;

      $this->dataManager->em->persist($transaction);
      $this->dataManager->em->flush($transaction);
      $contribution->transaction_id = $transaction->transaction_id;
      $this->dataManager->em->persist($contribution);
      $this->dataManager->em->flush($contribution);
    }
  }


  /**
   * Submit a C# report.
   *
   * @param $report_data
   * @param Fund $fund
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws ORMException
   * @throws OptimisticLockException
   * @throws Exception
   */
  public function submitReport($json, Fund $fund) {
    $this->validator->clear();
    $response = new stdClass();
    //assign report_data to report portion of $json object / XML submission falls on default
    if(isset($json->report)) {
      $report_data = $json->report;
      $report_data->vendor_name = $json->metadata->vendor_name;
      $report_data->version = $json->metadata->submission_version;
    } else {
      $report_data = $json;
    }
    if ($this->validateReport($report_data, $fund)) {
      $response = parent::submitReport($report_data, $fund);
      //@TODO: C3 specific processing.
      if (!empty($report_data->contributions)) {
        $this->saveContributions($report_data->contributions);
      }
      if (!empty($report_data->auctions)) {
        $this->saveAuctions($report_data->auctions);
      }
      if (!empty($report_data->misc)) {
        $this->saveMiscellaneous($report_data->misc);
      }
      if (!empty($report_data->loans)) {
        $this->saveLoans($report_data->loans);
      }
      if (!empty($report_data->sums)) {
        $this->saveSums();
      }
      if (!empty($report_data->properties)) {
        $this->saveEmailProperties($fund->fund_id, $report_data->properties);
      }
    }
    else {
      $response->success = false;
    }

    $response->messages = $this->validator->getMessages();
    $response->message = $this->validator->getMessageText();

    return $response;
  }

}
