<?php


namespace WAPDC\CampaignFinance\Reporting;

use \DateTime;
use \Exception;
use \stdClass;
use \SimpleXMLElement;

class OrcaReportParser {

  const SCHEMA_PARSER_EXCEPTION_CODE = -987654321;

  const DATE_FORMAT = 'Y-m-d';

  // From BaseXRFConvert.GetContribType in XRFInput
  const CONTRIBUTOR_TYPES = [
    'bus' => 'B',
    'v' => 'B',
    'trb' => 'C',
    'pac' => 'C',
    'fin' => 'F',
    'fi' => 'F',
    'ind' => 'I',
    'cpl' => 'I',
    'cau' => 'L',
    'og' => 'O',
    'sp' => 'P',
    'cp' => 'P',
    'ldp' => 'P',
    'can' => 'S',
    'mp' => 'T',
    'un' => 'U'
    ];

  protected $loan_original_amount = 0.0;
  protected $loan_principle_paid = 0.0;
  protected $loan_principle_repay = 0.0;
  protected $cash_received = 0.0;
  protected $expenditures_amt = 0.0;
  protected $inkind_received = 0.0;
  protected $candidate_cont_id = "";
  protected $personal_funds_sum = 0.0;

  /**
   * @var stdClass
   */
  protected $report;
  protected $source;

  /**
   * Parse XML into json style submission.
   * @param $xml
   * @return stdClass
   * @throws Exception
   */
  public function parse(&$xml, $source = 'ORCA') {
    $report = new stdClass();
    $this->source = $source;
    try {
      $doc = simplexml_load_string($xml);
    } catch (Exception $exception){
      throw new Exception("Submission failed the document is not valid XML.");
    }
    if (empty($doc)) {
      throw new Exception("Submission failed because the resulting XML document was empty.");
    }

    if (!$this->validate($doc)) {
      throw new Exception("Submission failed for an unknown reason during schema validation.");
    }
    $this->parseReport($doc, $report);
    return $report;
  }

    /**
     * Parse vendor-provided XML from the CampaignFinanceReportController into json style submission.
     * @param $xml
     * @throws Exception
     */
    public function vendorParse(&$xml, $source = 'Vendor') {
        $report = new stdClass();
        $this->source = $source;
        try {
            $doc = simplexml_load_string($xml);
        } catch (Exception $exception){
            throw new Exception("Submission failed the document is not valid XML.");
        }
        if (empty($doc)) {
            throw new Exception("Submission failed because the resulting XML document was empty.");
        }

        $this->parseReport($doc, $report);
        return $report;
    }

  protected function parseAddress(SimpleXMLElement $node, stdClass $object) {
    $object->address = (string)$node['str'];
    $object->city = (string)$node['city'];
    $object->state = (string)$node['st'];

    $object->postcode = $this->normalizeAddrZip($node['zip']);
  }

  protected function parseOfficeCode(SimpleXMLElement $node, stdClass $object) {
      $object->office_code = (string)$node['offs1'];
  }

  /**
   * @param \SimpleXMLElement $doc
   * @param \stdClass $report
   * @throws Exception
   */
  protected function parseReport(SimpleXMLElement $doc, stdClass &$report) {
    $this->report = $report;
    $report->report_type = strtolower((string)$doc->getName());

    $cover = $doc->cover;

    $this->report->sums = new stdClass();
    $this->candidate_cont_id = ((string)$cover->contId);
    if ($report->report_type == 'c4') {
      $report->final_report = (string)$cover['final'] === 'Y' ? True : False;
      $period_start = (string)$cover->period['start'];
      $period_end = (string)$cover->period['end'];

      if ($doc->schedA) {
        $this->parseSchedA($doc);
      }

      if($doc->schedB) {
        $this->parseSchedB($doc);
      }

      if ($doc->schedC) {
        $this->parseSchedC($doc);
      }

      if ($doc->schedL) {
        $this->parseSchedL($doc);
      }

      if($doc) {
        $this->parseSumItems($doc);
      }
    }
    elseif ($report->report_type == 'c3') {
      $period_start = $period_end = (string)$cover->depositDate;
    }
    $report->period_start = $period_start ? (new DateTime($period_start))->format('Y-m-d') : NULL;
    $report->period_end = $period_end ? (new DateTime($period_end))->format('Y-m-d') : NULL;

    // Not all xrf files have a dateFiled
    if($cover->dateFiled) {
      $date_filed = (string) $cover->dateFiled;
      $report->submitted_at = $date_filed ? (new DateTime($date_filed))->format('Y-m-d') : NULL;
    }

    $report->committee = new stdClass();
    $report->committee->name = (string)$cover->committee;
    $this->parseAddress($cover->addr, $report->committee);
    $this->parseOfficeCode($cover->officeSought, $report->committee);

    $report->election_code = (string)$cover->electnDate;
    if ($cover->treasurer) {
      $t = $cover->treasurer;
      $report->treasurer = new stdClass();
      if (!empty($t['name'])) {
        $report->treasurer->name = trim((string) $t['name']);
      } else {
        $report->treasurer->name = trim((string)$t['fst']. ' ' . (string)$t['lst']);
      }
      $treasurer_date = (string) $t['date'];
      $report->treasurer->date = $treasurer_date ? (new DateTime($treasurer_date))->format('Y-m-d') : null;
      if (!empty($t['phone'])) {
        $report->treasurer->phone = $this->normalizePhone($t['phone']);
      }
    }
    if($doc->attachedPages) {
      $this->parseAttachedPages($doc);
    }
    if ($doc->cont) {
      $this->parseContacts($doc);
    }
    if ($report->report_type == 'c3') {
      $report->sums = new \StdClass();
      $report->sums->anonymous = new \StdClass();
      $report->sums->anonymous->amount = isset($cover->anon['amt']) ? $this->normalizeMoney($cover->anon['amt']) : null;
      $report->sums->anonymous->aggregate = isset($cover->anon['totalAmt']) ? $this->normalizeMoney($cover->anon['totalAmt']) : null;
      if (!empty($cover->anon['date'])) {
        if (strtolower($cover->anon['date']) == 'various') {
          $report->sums->anonymous->date = null;
        }
        else {
          $report->sums->anonymous->date = $this->normalizeDate($cover->anon['date']);
        }
      }
      else {
        $report->sums->anonymous->date = null;
      }
      $report->sums->anonymous->aggregate = $this->normalizeMoney($cover->anon['totalAmt']);
      $report->sums->small = new \StdClass();
      $report->sums->small->amount = isset($cover->smallAmt['amt']) ? $this->normalizeMoney($cover->smallAmt['amt']) : null;
      $report->sums->small->count = $cover->smallAmt['count'] ?? null;
      if (!empty($cover->smallAmt['date'])) {
        if (strtolower($cover->smallAmt['date']) == 'various') {
          $report->sums->small->date = null;
        }
        else {
          $report->sums->small->date = $this->normalizeDate($cover->smallAmt['date']);
        }
      }
      else {
        $report->sums->small->date = null;
      }
      $report->sums->small->count = (int)$cover->smallAmt['count'] ?? null;

      if ($cover->persFunds && $cover->persFunds->attributes()->count() > 0) {
        $pf = new \StdClass();
        $pf->amount = 0.0;
        $pf->date = NULL;
        $pf->count = 0;

        foreach($cover->persFunds as $fund) {
          $pf_date = $this->normalizeDate($fund['date'] ?? $cover->depositDate ?? null);
          $pf_amount = $this->normalizeMoney($fund['amt'] ?? null);
          $pf_aggregate = $this->normalizeMoney($fund['totalAmt'] ?? null);
          if (!empty($pf_amount) || !empty($pf_aggregate)) {
            if (empty($cover->contId)) {
              throw new Exception('Could not locate a contId in this submission. This field is necessary when submitting reports with personal funds.');
            }
            if (!empty($pf_amount)) {
              $misc = new \StdClass();
              $misc->date = $pf_date;
              $misc->amount = $pf_amount;
              $misc->aggregate = $pf_aggregate;
              $misc->personal_funds = true;
              $misc->contact_key = (string)$cover->contId;
              $report->misc[] = $misc;
            }
            $pf->amount =+ $pf_amount ?? 0;
            $pf->aggregate =+ $pf_aggregate ?? 0;
            $pf->count++;
            if (empty($pf->date)) {
              $pf->date = $pf_date;
            }
          }
        }
        $report->sums->personal = $pf;
      }

      if ($doc->contr) {
        $this->parseContributions($doc);
      }
      if ($doc->auction) {
        $this->parseAuctions($doc->auction);
      }
      if ($doc->miscRcpt) {
        $this->parseMisc($doc);
      }
      if ($doc->loan) {
       $this->parseLoans($doc->loan);
      }
    }
  }

  /**
   *
   * @param SimpleXMLElement $doc
   * @throws Exception
   */
  protected function parseLoans(SimpleXMLElement $doc) {
    $this->report->loans = [];
    foreach ($doc as $xml_loan) {
      $loan = new \StdClass();
      $loan->endorser = [];
      $loan->amount = (float)$xml_loan['amt'];
      $loan->contact_key = (string)$xml_loan['contId'];
      $date =  (string)$xml_loan['date'] ?? NULL;
      $loan->date = $date ? (new DateTime($date))->format('Y-m-d') : NULL;
      $due_date =  (string)$xml_loan['dateDue'] ?? NULL;
      $loan->due_date = $due_date ? (new DateTime($due_date))->format('Y-m-d') : NULL;
      $loan->election = (string)$xml_loan['electn'];
      $loan->interest_rate = (float)$xml_loan['intRate'];
      $loan->payment_schedule = (string)$xml_loan['repaySched'];
      foreach($xml_loan->endorser as $e) {
        $endrs = new stdClass();
        $endrs->liable_amount = (float)$e['amtLiable'];
        $endrs->contact_key = (string)$e['contId'];
        $endrs->election = (string)$e['electn'];
        $loan->endorser[] = $endrs;
      }
      $this->report->loans[] = $loan;
    }
  }

  protected function parseAttachedPages(SimpleXMLElement  $doc)
  {
    $this->report->attachments = [];
    foreach ($doc->attachedPages as $xml_attachment){
      $attachment = new \stdClass();
      if($this->source == "ORCA") {
        //Orca seems to put indent on all lines except for the first one...
        $attachment->content = trim(str_replace("\n    ", "\n", (string)$xml_attachment->page));
      }else{
        $attachment->content = (string)$xml_attachment->page;
      }
      $this->report->attachments[] = $attachment;
    }
  }

  protected function parseContacts(SimpleXMLElement $doc) {
    $this->report->contacts = [];
    foreach ($doc->cont as $xml_cont) {
      $contact = new \StdClass();
      $contact->contact_key = (string)$xml_cont['id'] ?? null;
      $contact->aggregate_general = (float)$xml_cont['gAgg'] ?? null;
      $contact->aggregate_primary = (float)$xml_cont['pAgg'] ?? null;
      $contributor_type = strtolower((string)$xml_cont['type'] ?? "");
      $contact->contributor_type = static::CONTRIBUTOR_TYPES[$contributor_type] ?? NULL;
      if ($contact->contact_key == $this->candidate_cont_id) {
        $contact->contributor_type = 'S';
      }
      if (!empty($xml_cont->name['full'])) {
        $contact->name = (string)($xml_cont->name['full']);
      } else {
        $names = [];
        if (strlen($xml_cont->name['lst'] ?? '')) $names[] = trim($xml_cont->name['lst']);
        if (strlen($xml_cont->name['fst'] ?? '')) $names[] = trim($xml_cont->name['fst']);
        if (strlen($xml_cont->name['mid'] ?? '')) $names[] = substr(trim($xml_cont->name['mid']), 0, 1);
        if (strlen($xml_cont->name['pre'] ?? '')) $names[] = trim($xml_cont->name['pre']);
        if (strlen($xml_cont->name['suf'] ?? '')) $names[] = trim($xml_cont->name['suf']);
        $contact->name = implode(' ', $names);
      }
      $contact->address = (string)$xml_cont->addr['str'] ?? null;
      $contact->city = (string)$xml_cont->addr['city'] ?? null;
      $contact->state = (string)$xml_cont->addr['st'] ?? null;
      $contact->postcode = (string)$xml_cont->addr['zip'] ?? null;
      $contact->employer = new \StdClass();
      $contact->employer->name = trim($xml_cont->employer['name'] ?? '') ?? null;
      $contact->employer->city = (string)$xml_cont->employer['city'] ?? null;
      $contact->employer->state = (string)$xml_cont->employer['st'] ?? null;
      $contact->employer->occupation = (string)$xml_cont->employer['occ'] ?? null;
      $contact->aggregate_general = (float)$xml_cont['gAgg'];
      $contact->aggregate_primary = (float)$xml_cont['pAgg'];
      $this->report->contacts[] = $contact;
    }
  }

  /**
   *
   * @param SimpleXMLElement $doc
   * @throws Exception
   */
  protected function parseContributions(SimpleXMLElement $doc) {
    $this->report->contributions = [];
    foreach ($doc->contr as $xml_contr) {
      $contribution = new \StdClass();
      $contribution->amount = $this->normalizeMoney($xml_contr['amt']) ?? null;
      $contribution->contact_key = (string)$xml_contr['contId'] ?? null;
      $contribution_date = (string)$xml_contr['date'];
      $contribution->date = $contribution_date ? (new DateTime($contribution_date))->format('Y-m-d'): NULL;
      $contribution->election = (string)$xml_contr['electn'] ?? null;
      $this->report->contributions[] = $contribution;
    }
  }

  /**
   * Parse auctions from XML
   * @param SimpleXMLElement $auctions
   * @throws Exception
   */
  public function parseAuctions(SimpleXMLElement $auctions) {
    $this->report->auctions = [];
    /** @var SimpleXMLElement $auction */
    foreach ($auctions as $a_node) {
      $auction = new stdClass();
      $auction_date =  (string)$a_node['auctionDate'] ?? NULL;
      $auction->date = $auction_date ? (new DateTime($auction_date))->format('Y-m-d') : NULL;
      $auction->items = [];
      foreach ($a_node->item as $i_node) {
        $item = new stdClass();
        $item->description = (string)$i_node['descr'];
        $item->market_value = (float)$i_node['marketVal'];
        $item->number = (int)$i_node['number'];
        $item->donate = $donate = new stdClass();
        $donate->contact_key = (string)$i_node->donor['contId'];
        $donate->amount = (float)$i_node->donor['amt'];
        $donate->election = (string)$i_node->donor['electn'];
        $item->buy = $buy = new stdClass();
        $buy->contact_key = (string)$i_node->buyer['contId'];
        $buy->amount = (float)$i_node->buyer['amt'];
        $buy->election = (string)$i_node->buyer['electn'];
        $item->sale_amount = $buy->amount + $donate->amount;
        $auction->items[] = $item;
      }
      $this->report->auctions[] = $auction;
    }
  }

  /**
   * Parse misc receipts.
   * @param $doc
   * @throws Exception
   */
  function parseMisc($doc) {
    // Wanted to pass in $doc->misc but could never get it to come in as an array, it would always come into the function
    // as the SimpleXmlElement attributes of the first miscRcpt array element. Confusing.
    foreach ($doc->miscRcpt as $xml_misc) {
      $misc = new \StdClass();
      $misc->contact_key = (string)$xml_misc['contId'] ?? null;
      $misc->amount = $this->normalizeMoney($xml_misc['amt']);
      $misc->date = $this->normalizeDate($xml_misc['date']);
      $misc->description = (string)$xml_misc['descr'] ?? null;
      $this->report->misc[] = $misc;
    }
  }

  /**
   * Parse Schedule A
   * @param SimpleXMLElement $doc
   * @throws Exception
   */
  function parseSchedA(SimpleXMLElement $doc) {
    foreach ($doc->schedA as $xmlNode) {

      // This is the underfifty which is totalled together
      $this->makeSum($this->report->sums, 'expenses_under_50' ,$this->normalizeMoney($xmlNode->underFifty['amt']));

      $expenses = [];
      foreach ($xmlNode->schedAExpn as $expn) {
        $expense = new stdClass();
        $expense->amount = $this->normalizeMoney($expn['amt']);
        $expense->contact_key = (string)$expn['contId'] ?? null;
        $expense->date = $this->normalizeDate($expn['date']);
        $expense->description = (string)$expn['descr'] ?? null;
        if (!empty($expn['code'])) {
          $expense->category = (string) $expn['code'];
        }
        $expenses[] = $expense;
      }
      $this->report->expenses = $expenses;

      $cash_receipt = [];
      foreach ($xmlNode->cshRcpt as $receipt) {
        $cshRcpt = new stdClass();
        $cshRcpt->date =  $this->normalizeDate($receipt['date']) ?? null;
        $cshRcpt->amount = $this->normalizeMoney($receipt['amt']) ?? null;
        $cash_receipt[] =  $cshRcpt;
      }
      $this->report->misc_receipts = $cash_receipt;
    }
  }

  /**
   * Parse Schedule B
   * @param SimpleXMLElement $doc
   * @throws Exception
   */
  function parseSchedB(SimpleXMLElement $doc) {
    foreach ($doc->schedB as $xmlNode) {

      foreach ($xmlNode->children() as $name => $node) {
        switch ($name) {
          case 'pldgRcvd':
            $pledge = new stdClass();
            $pledge->amount = $this->normalizeMoney($node['fmv']);
            $pledge->contact_key = (string) $node['contId'] ?? NULL;
            $pledge->date = $this->normalizeDate($node['date']);
            $pledge->election = (string) $node['electn'] ?? NULL;
            $pledges[] = $pledge;
            $this->report->pledge = $pledges;
            break;
          case 'ccDebt':
          case 'debt':
            $debt = new stdClass();
            $debt->amount = $this->normalizeMoney($node['amt']);
            $debt->contact_key = (string)$node['contId'] ?? null;
            $debt->date = $this->normalizeDate($node['date']);
            $debt->description = (string)$node['descr'] ?? null;
            $debt->category = (string)$node['code'] ?? null;
            $debt->sub_debt = [];
            $sub_debt_total = 0;
            foreach ($node->subDebt as $sub_item) {
              $sub_debt = new stdClass();
              $sub_debt->amount = $this->normalizeMoney($sub_item['amt']);
              $sub_debt->contact_key = (string)$sub_item['contId'] ?? null;
              $sub_debt->date = $this->normalizeDate($sub_item['date']);
              // Default in greatest date for transaction date, since that's probably the last included on the cc statement.
              if ($sub_debt->date > $debt->date) {
                $debt->date = $sub_debt->date;
              }
              $sub_debt->description = (string)$sub_item['descr'] ?? null;
              $sub_debt->category = (string)$sub_item['code'] ?? null;
              $debt->sub_debt[] = $sub_debt;
              $sub_debt_total += $sub_debt->amount;
            }
            if ($sub_debt_total > 0) {
              $debt->amount = $sub_debt_total;
            }
            $debts[] = $debt;
            $this->report->debt = $debts;
            break;
          case 'ikRcvd':
            $contribution = new stdClass();
            $contribution->in_kind = TRUE;
            $contribution->amount = $this->normalizeMoney($node['fmv']);
            $contribution->contact_key = (string) $node['contId'] ?? NULL;
            $contribution->date = $this->normalizeDate($node['date']);
            $contribution->description = (string) $node['descr'] ?? NULL;
            $contribution->election = (string) $node['electn'] ?? NULL;
            $this->report->contributions_inkind[] = $contribution;
        }
      }
    }
  }


  /**
   * @param SimpleXMLElement $doc
   * @throws Exception
   */
  protected function parseSchedC(SimpleXMLElement $doc) {

    foreach($doc->schedC->children() as $name=>$node) {
      $corr = new stdClass();
      $corr->contact_key = (string)$node['contId'];
      $corr->date = (string)$node['date'] ? (new DateTime((string)$node['date']))->format('Y-m-d') : NULL;

      switch($name) {
        case "contr":
          $corr->reported_amount = is_numeric((string)$node['amtRpt']) ? (float)$node['amtRpt'] : null;
          $corr->corrected_amount = is_numeric((string)$node['crctdAmt']) ? (float)$node['crctdAmt'] : null;
          $corr->difference = is_numeric((string)$node['dif'] ) ? (float)$node['dif'] : null;
          $corr->description = (string)$node['descr'];
          $this->report->contribution_corrections[] = $corr;
          break;
        case "schedCExpn":
          $corr->reported_amount = is_numeric((string)$node['amtRptd']) ? (float)$node['amtRptd'] : null;
          $corr->corrected_amount = is_numeric((string)$node['adjAmt']) ? (float)$node['adjAmt'] : null;
          $corr->difference = is_numeric((string)$node['dif']) ? (float)$node['dif'] : null;
          $corr->description = (string)$node['descr'];
          $this->report->expense_corrections[] = $corr;
          break;
        case "refund":
          $corr->amount  = is_numeric((string)$node['amt']) ? (float)$node['amt'] : null;
          $this->report->expense_refunds[] = $corr;
          break;
      }
    }
  }

  /**
   * Parse schedule L section of XML.
   * @param SimpleXMLElement $doc
   * @throws Exception
   */
  function parseSchedL(SimpleXMLElement $doc) {
    foreach ($doc->schedL as $schedule) {
      $this->report->loan = new stdClass();
      foreach ($schedule->children() as $name => $node) {

        $tran = new stdClass();
        $tran->contact_key = (string)$node['contId'];
        $date = (string) $node['date'];
        $tran->date = $date ? (new DateTime($date))->format('Y-m-d') : NULL;
        switch($name) {
          // In-kind loans
          case "loan":
            $tran->amount = (float)$node['amt'];
            $tran->contact_key = (string)$node['contId'];
            $due_date = (string) $node['dateDue'];
            $tran->due_date = $due_date ? (new DateTime($due_date))->format('Y-m-d') : null;
            $tran->election = (string)$node['electn'];
            $tran->interest_rate = (float)$node['intRate'];
            $tran->payment_schedule = (string)$node['repaySched'];
            $tran->in_kind = ((string)$node['isInKind'] == "true") ? true : false;
            $this->report->loan->receipts[] = $tran;
            break;
          case "pymnt":
            $tran->balance = (float)$node['bal'];
            $tran->interest_paid  = (float)$node['int'];
            $tran->principle_paid = (float)$node['prin'];
            $this->report->loan->payments[] = $tran;
            break;
          case "frgvn":
            $tran->forgiven_amount = (float)$node['frgvn'];
            $tran->original_amount = (float)$node['origAmt'];
            $this->report->loan->payments[] = $tran;
            break;
          case "loansOwed":
            $tran->original_amount = (float)$node['origAmt'];
            $tran->principle_paid = (float)$node['prin'];
            $this->report->loan->balances[]  = $tran;
            break;
        }
      }

    }
  }

  protected function makeSum($sum, $property, $amount, $count = null) {
    $sum->$property = new stdClass();
    $sum->$property->amount = $amount;
    if ($count) {
      $sum->$property->count = $count;
    }
  }

  protected function parseSumItems(SimpleXMLElement $doc)
  {
    $this->makeSum($this->report->sums, 'previous_receipts', $this->normalizeMoney($doc->cover->receipts['prevTotals'])); // 1
    $this->makeSum($this->report->sums, 'previous_expenditures', $this->normalizeMoney($doc->cover->expn['prevTotals'])); // 10
  }

  /**
   * @param \SimpleXMLElement $doc
   * @return bool
   *   indicates whether the schema is valid.
   * @throws Exception
   */
  protected function validate(SimpleXMLElement $doc) {
    $this->normalizePhone($doc->cover->treasurer);
    foreach ($doc as $node) {
      $this->cleanXml($node);
    }
    $type = strtolower((string)$doc->getName());
    $dom = dom_import_simplexml($doc);
    $filename = __DIR__ . "/schema/$type.xsd";

    libxml_use_internal_errors(true);
    libxml_clear_errors();
    if(!$dom->ownerDocument->schemaValidate($filename)) {
      $errors = libxml_get_errors();
      $message = $this->collectXMLSchemaValidationErrors($errors);
      throw new Exception($message, static::SCHEMA_PARSER_EXCEPTION_CODE);
    }
    return true;
  }

  /**
   * @param \LibXMLError[] $errors
   * @returns string
   *
   * Collect up error from schema validation failure into a string.
   */
  public function collectXMLSchemaValidationErrors(array $errors) {
    $messages = array_column($errors, 'message');
    $message = implode($messages);
    return $message;
  }


  /**
   *
   * @param \SimpleXMLElement $node
   *
   * Truncate all middle initials to a single character.
   */
  public function truncateMiddleInitials($mi) {
    return substr($mi, 0, 1);
  }

  /**
   * @param \SimpleXMLElement $node
   *
   *   If a phone number is nnnnnnnnnn or (nnn) nnn-nnnn or nnn.nnn.nnn where
   * the "." is any character, they are normalized to nnn-nnn-nnn. Any that
   * don't match the patterns are removed which is okay (valid) to have missing.
   */
  public function normalizePhone($phone) {
      $phone = (string)$phone;
      $digits = preg_replace("/[^\d]/", "", $phone);
      return preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $digits);
  }

  /**
   * @param string $zip
   *   Zip code to normalize
   *
   *   If a zip code is other than nnnnn or nnnnn-nnnn it will fail to
   * validate but we get lots that are malformed even from ORCA.
   * This will truncate the ones that have at least the first 5 as digits.
   * @throws Exception
   */
  public function normalizeAddrZip($zip) {
      $zipCode = (string)$zip;
      if (strlen($zipCode) > 32) {
        throw new Exception("Post code is too long!");
      }
      return $zipCode;
  }

  /**
   * @param $amount
   * @return float
   */
  public function normalizeMoney($amount) {
    return (float)$amount;
  }

  /**
   * Treats date properly
   * @param $date
   * @return string|null
   * @throws Exception
   */
  public function normalizeDate($date) {
    if (!empty($date)) {
      return (new DateTime($date))->format('Y-m-d');
    }
    return null;
  }

  /**
   * @param \SimpleXMLElement $node
   *
   * cleans \n & control characters characters in node attributes
   */
  private function cleanXml(SimpleXMLElement $node) {
    foreach ($node->attributes() as $key => $attr) {
      $node[$key] = str_replace('\n', "", (string) $attr);
      $node[$key] = strip_tags((string) $attr);
    }
    foreach ($node->children() as $child) {
      $this->cleanXml($child);
    }
  }
}