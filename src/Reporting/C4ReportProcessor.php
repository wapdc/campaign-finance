<?php


namespace WAPDC\CampaignFinance\Reporting;


use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use WAPDC\CampaignFinance\CFDataManager;
use DateTime;
use WAPDC\CampaignFinance\Model\Correction;
use WAPDC\CampaignFinance\Model\Fund;
use WAPDC\CampaignFinance\Model\LoanPayment;
use WAPDC\CampaignFinance\Model\Report;
use WAPDC\CampaignFinance\Model\Expense;
use WAPDC\CampaignFinance\Model\Sum;
use \Exception;
use \stdClass;

class C4ReportProcessor extends ReportProcessorBase {
  protected $cash_expenditures_amount = 0.0;
  protected $cash_expenditures_count = 0;
  protected $credit_expenditures_amount = 0.0;
  protected $credit_expenditures_count = 0;
  protected $loan_principle_payments = 0.0;
  protected $loan_balance_amount = 0.0;
  protected $loan_balance_count = 0;
  protected $in_kind_amount = 0.0;
  protected $in_kind_count = 0;
  protected $debts_amount = 0.0;
  protected $debts_count = 0;
  protected $cash_on_hand = 0;
  protected $pledges_amount = 0.0;
  protected $pledges_count = 0;
  protected $deposit_amount = 0.0;
  protected $deposit_count = 0;
  protected $expense_correction_amount = 0.0;
  protected $expense_correction_count = 0;
  protected $receipt_correction_amount = 0.0;
  protected $receipt_correction_count = 0;
  protected $existing_report;

  public function __construct(CFDataManager $dataManager, $version, $existing_report = false)
  {
    parent::__construct($dataManager, $version);
    $this->existing_report = $existing_report;
  }

  /**
   * @param \stdClass $report_data
   *   Submission data for the report.
   * @param Fund $fund
   *   The fund that the report should be attached to
   * @return bool|void
   * @throws \Exception
   */
  public function validateReport(stdClass $report_data, Fund $fund) {
    $valid =  parent::validateReport($report_data, $fund);

    // Report specific validations
    if ($valid && !$this->existing_report) {
      if (empty($report_data->amends)) {
        // Check to see if a duplicate report exists for the filing period.
        $period_start = new DateTime($report_data->period_start);
        $report = $this->dataManager->em->getRepository(Report::class)
          ->findOneBy(['report_type' => 'C4', 'period_start' => $period_start, 'fund_id' => $fund->fund_id]);
        if ($report) {
          $message = "A report already exists for the reporting period beginning "
            . $period_start->format("m/d/Y")
            . ".\n Perhaps you meant to file an amendment?";
          $this->validator->addError('duplicate-report', $message);
        }
      }
    }

    if (!empty($report_data->misc_receipts)) {
      $this->validateReceipts($report_data->misc_receipts);
    }

    if (!empty($report_data->expenses)) {
      $this->validateExpenses($report_data->expenses);
    }

    if (!empty($report_data->loan)) {
      if (!empty($report_data->loan->receipts)) {
        if (is_array($report_data->loan->receipts)) {
          $this->validateLoans($report_data->loan->receipts, "loan.receipts", true);
        }
        else {
          $this->validator->addError("loan.receipts.invalid", "loan.receipts must be an array");
        }
      }

      if (!empty($report_data->loan->payments)) {
        $this->validateLoanPayments($report_data->loan->payments);
      }

      if (!empty($report_data->loan->balances)) {
        $this->validateLoansOwed($report_data->loan->balances);
      }
    }

    $this->validateCorrections($report_data);

    if (!empty($report_data->pledge)) {
      $this->validatePledges($report_data->pledge);
    }

    if (!empty($report_data->contributions_inkind)) {
      $this->validateInKindContributions($report_data->contributions_inkind);
    }

    if (!empty($report_data->debt)) {
      $this->validateDebt($report_data->debt);
    }

    if (!empty($report_data->sums)) {
      $this->validateSums($report_data);
    }

    if(!empty($report_data->properties)) {
      $this->saveEmailProperties($fund->fund_id, $report_data->properties);
    }

    if (isset($report_data->treasurer->name) && strlen($report_data->treasurer->name) > 250) {
      $this->validator->addError('name.length', "Treasurer name must be less than 250 characters.");
    }
    return $this->validator->isValid();
  }

  /**
   * @param $pledges
   */
  protected function validatePledges($pledges) {
    $this->pledges_amount = 0.0;
    $this->pledges_count = 0;
    foreach ($pledges as $p => $pledge) {
      $this->pledges_amount += (float)($pledge->amount ?? 0.0);
      $this->pledges_count ++;
      $valid = $this->validator->validateRequired($pledge, ['amount'], "pledge.$p", "Error: Missing required pledge amount");
      if ($valid) {
        if(isset($pledge->election)) {
          $this->validator->validateList($pledge, 'election', static::PRIM_GEN_VALUES, "pledge.$p", "Election code for Pledge on date " . $pledge->date . " must be on of P, G or N.");
        } else {
          $this->validator->addError("pledge.$p.election.incomplete", "Election code for Pledge on date " . $pledge->date . " must be on of P, G or N.");
        }
        if (!empty($this->contacts[$pledge->contact_key])) {
          $this->validateContact($pledge->contact_key, true, true);
        }
        $this->validator->validateMoney($pledge->amount, "pledge.$p.amount", "Error: Pledge amount incorrect");
        $this->validator->validateDate($pledge->date, "pledge.$p.date", "The pledge date must be a valid date");
      }
    }
  }

  /**
   * @param $debts
   */
  protected function validateDebt($debts) {
    $this->debts_amount = 0.0;
    $this->debts_count = 0;
    foreach ($debts as $d => $debt) {
      $this->debts_amount += (float)($debt->amount ?? 0.0);
      $this->debts_count ++;
      $valid = $this->validator->validateRequired($debt, ['amount'], "debt.$d", "Error: Missing required debt amount");

      if (isset($debt->description) && strlen($debt->description) > 1000) {
        // Just so we don't get a book
        $this->validator->addError("debt.$d.description.length", "Debt description must be less than 1000 characters.");
      }
      if ($valid) {
        if (!empty($this->contacts[$debt->contact_key])) {
          $this->validateContact($debt->contact_key, true, true);
        }
        $this->validator->validateMoney($debt->amount, "debt.$d.amount", "Error: Debt amount incorrect");
        $this->validator->validateDate($debt->date, "debt.$d.date", "The debt date must be a valid date");
      }
    }
  }

  /**
   * @param $sums
   */
  protected function validateSums($report_data) {
    $this->sums = new stdClass();
    if(!empty($report_data->version) && $report_data->version >= 1.1) {
        if (!empty($report_data->sums->non_itemized_cash_expenses) && !empty($report_data->sums->non_itemized_cash_expenses->amount)) {
            $this->validator->validateMoney($report_data->sums->non_itemized_cash_expenses->amount, "sums.non_itemized_cash_expenses.amount", "Amount for expenses under " . $this->thresholds->itemized_expenses . " must be a number");
            $this->cash_expenditures_amount += (float)$report_data->sums->non_itemized_cash_expenses->amount ?? 0.0;
        }
    } else {
        if (!empty($report_data->sums->expenses_under_50) && !empty($report_data->sums->expenses_under_50->amount)) {
            $this->validator->validateMoney($report_data->sums->expenses_under_50->amount, "sums.expenses_under_50.amount", "Amount for expenses under " . $this->thresholds->itemized_expenses . " must be a number");
            $this->cash_expenditures_amount += (float)$report_data->sums->expenses_under_50->amount ?? 0.0;
        }
    }

      if (!empty($report_data->sums->non_itemized_credit_expenses) && !empty($report_data->sums->non_itemized_credit_expenses->amount)) {
          $this->validator->validateMoney($report_data->sums->non_itemized_credit_expenses->amount, "sums.non_itemized_credit_expenses.amount", "Amount for expenses under " . $this->thresholds->itemized_expenses . " must be a number");
          $this->credit_expenditures_amount += (float)$report_data->sums->non_itemized_credit_expenses->amount ?? 0.0;
      }

    $previous_receipts = (float)$report_data->sums->previous_receipts->amount ?? 0.0;
    $previous_expenditures = (float)$report_data->sums->previous_expenditures->amount ?? 0.0;
    if (!$this->cash_on_hand) {
      $this->cash_on_hand = $previous_receipts + $this->deposit_amount + $this->receipt_correction_amount - $previous_expenditures - $this->cash_expenditures_amount - $this->expense_correction_amount;
    }

    foreach (self::SUMS_LINES as $line_item => $sum_title) {

      $date = (new DateTime($report_data->period_end))->format('Y-m-d');
      $amount = 0.0;
      $count = null;
      if ($line_item < 10) {
        $category = 'receipts';
      } else if ($line_item >= 10 && $line_item < 18) {
        $category = 'expenditures';
      } else {
        $category = 'cash_summary';
      }


      $this->sums->$category = !empty($this->sums->$category) ? $this->sums->$category :  new stdClass();
      $sum = $this->sums->$category->$sum_title = new stdClass();

      // must round amounts due to known json_encode serialization bug: https://wiki.php.net/rfc/precise_float_value
      switch ($sum_title) {
        // #1
        case 'previous_receipts':
          $amount = round($previous_receipts, 2);
          break;
        // #2
        case 'cash_receipts':
          $amount = round($this->deposit_amount, 2);
          $count = $this->deposit_count;
          break;
        // #3, #12
        case 'inkind_expenditures':
        case 'inkind_receipts':
          $amount = round($this->in_kind_amount, 2);
          $count = $this->in_kind_count;
          break;
        // #4
        case 'total_receipts':
          $amount = round($this->in_kind_amount + $this->deposit_amount, 2);
          break;
        // #5,  #14
        case 'loan_principle_paid':
          $amount = round($this->loan_principle_payments, 2);
          break;
        // #6
        case 'receipt_corrections':
          $amount = round($this->receipt_correction_amount, 2);
          $count = $this->receipt_correction_count;
          break;
        // #7
        case 'receipt_adjustments':
          $amount = round($this->receipt_correction_amount - $this->loan_principle_payments, 2);
          break;
        // #8
        case 'campaign_receipts':
          $amount = round($this->in_kind_amount + $this->deposit_amount + $previous_receipts + $this->receipt_correction_amount - $this->loan_principle_payments, 2);
          break;
        // #9
        case 'pledges':
          $amount = round($this->pledges_amount, 2);
          $count = $this->pledges_count;
          break;
        // #10
        case 'previous_expenditures':
          $amount = round($previous_expenditures, 2);
          break;
        // #11
        case 'cash_expenditures':
          $amount = round($this->cash_expenditures_amount, 2);
          $count = $this->cash_expenditures_count;
          break;
        // Not inserted:
        case 'credit_expenditures':
          $amount = round($this->credit_expenditures_amount, 2);
          $count = $this->credit_expenditures_count;
          break;
        // #13
        case 'total_expenditures':
          $amount = round($this->in_kind_amount + $this->cash_expenditures_amount, 2);
          break;
        // #15
        case 'expense_corrections':
          $amount = round($this->expense_correction_amount, 2);
          $count = $this->expense_correction_count;
          break;
        // #16
        case 'expense_adjustments':
          $amount = round($this->expense_correction_amount - $this->loan_principle_payments, 2);
          break;
        // #17
        case 'campaign_expenses':
          $amount = round($previous_expenditures + $this->in_kind_amount + $this->cash_expenditures_amount + $this->expense_correction_amount - $this->loan_principle_payments, 2);
          break;

        // #18
        case 'cash_on_hand':
          $amount = round($this->cash_on_hand, 2);
          if ($amount < 0) {
            $this->validator->addMessage('balance', 'This report shows negative cash on hand (Line 18). This indicates incomplete reporting. Please enter missing receipts before submitting this report.');
          }
          break;
        // #19
        case 'liabilities':
          $amount = round($this->loan_balance_amount + $this->debts_amount, 2);
          break;
        // #20
        case 'balance':
          $amount = round($this->cash_on_hand - ($this->loan_balance_amount + $this->debts_amount), 2);
          break;
      }
      $sum->line_item = $line_item;
      $sum->amount = $amount;
      $sum->date = $date;
      $sum->count = $count;
    }
  }

  /**
   * @param $receipts
   */
  protected function validateReceipts(&$receipts) {
    foreach ($receipts as $r => $receipt) {
      $valid = $this->validator->validateRequired($receipt, ['amount'], "misc_receipts.$r", "Error: Missing required deposit amount");
      if ($valid) {
        $valid = $valid &&  $this->validator->validateMoney($receipt->amount, "misc_receipts.$r.amount", "Error: Deposit amount incorrect");
        $valid = $valid && $this->validator->validateDate($receipt->date, "misc_receipts.$r.date", "The deposit date must be a valid date");
      }
      if ($valid) {
        $this->deposit_count++;
        $this->deposit_amount += $receipt->amount;
      }
    }
  }

  /**
   * @param array $expenses
   */
  protected function validateExpenses(array $expenses) {
    $category_list = [];
    $categories = $this->dataManager->db->executeQuery("SELECT code,label from expense_category")->fetchAllAssociative();
    foreach ($categories as $category) {
        $category_list[$category['code']] = $category['label'];
    }
    $this->cash_expenditures_amount = 0;
    $this->cash_expenditures_count = 0;
    $this->credit_expenditures_count = 0;
    foreach ($expenses as $e => $expense) {
      $date = $expense->date ?? NULL;
      $e_msg = "The expense on $date";
      $desc = $expense->description ?? 'Unknown';
      $valid = $this->validator->validateRequired($expense, ['contact_key', 'amount', 'date'], "expenses.$e", "$e_msg, expense $desc");
      if ($valid) {
        if (!empty($expense->payment_category) && $expense->payment_category !== 'loan' && $expense->payment_category !== 'debt') {
          $this->validator->addError("expense.$e.payment_category.invalid", "Invalid payment category");
        }
        if (empty($this->contacts[$expense->contact_key])) {
          $this->validator->addError("expense.$e.contact_key.invalid", 'Contact information not found for expense amount $' . $expense->amount . ' on ' . $expense->date);
        } else {
          $this->validateContact($expense->contact_key, false, true, 'expenditure');
        }
        $this->validator->validateMoney($expense->amount, "expenses.$e.amount", "$desc amount must be a number");
        $this->validator->validateDate($expense->date, "expenses.$e.date", "Date for $desc must be a valid date");
        if (!empty($expense->creditor_key)) {
          $this->credit_expenditures_amount += (float)($expense->amount ?? 0.0);
          $this->credit_expenditures_count++;
        }
        else {
          $this->cash_expenditures_amount += (float)($expense->amount ?? 0.0);
          $this->cash_expenditures_count++;
        }

        if (empty($expense->category) && $this->version >= 1.1) {
            $this->validator->addError("expenses.$e.category.invalid", "category is required for API version 1.1");
        }

        $this->validator->validateCategory($expense, $category_list, "expenses.$e.category", "Invalid category");
      }
    }
  }

  /**
   * @param $report
   */
  protected function validateCorrections($report) {
    $this->expense_correction_amount = 0.0;
    $this->expense_correction_count = 0;
    $this->receipt_correction_count = 0;
    $this->receipt_correction_amount = 0.0;
    foreach (['expense_correction' => 'expense_corrections', 'contribution_correction' => 'contribution_corrections'] as $prefix => $section) {
      if (!empty($report->$section)) {
        if (!is_array($report->$section)) {
          $this->validator->addError("$prefix.invalid", "$section must be an array");
        }
        else {
          foreach($report->$section as $c => $corr) {
            $correction_type = $section == 'expense_corrections' ? 'Expense' : 'Contribution';
            $valid = $this->validator->validateRequired(
              $corr, ['reported_amount', 'corrected_amount', 'difference', 'date', 'description'], "$prefix.$c", $correction_type." correction");
            if ($valid) {
              $this->validator->validateMoney($corr->reported_amount, "$prefix.$c.reported_amount", "Reported amount");
              $this->validator->validateMoney($corr->corrected_amount, "$prefix.$c.corrected_amount", "Corrected amount");
              $this->validator->validateMoney( $corr->difference, "$prefix.$c.difference", "Difference");
              $this->validator->validateDate($corr->date, "$prefix.$c.date", "Date");
              if ($section == 'expense_corrections') {
                $this->expense_correction_count++;
                $this->expense_correction_amount += (float)($corr->difference ?? 0.0);
              }
              else {
                $this->receipt_correction_count++;
                $this->receipt_correction_amount += (float)($corr->difference ?? 0.0);
              }
            }
          }
        }
      }
    }

    if (!empty($report->expense_refunds)) {
      if (!is_array($report->expense_refunds)) {
        $this->validator->addError("expense_refunds.invalid", "Expense refunds must be an array");
      }
      else {
        foreach($report->expense_refunds as $c => $corr) {
          $valid = $this->validator->validateRequired($corr, ['amount', 'date', 'contact_key'], "expense_refunds.$c", "Expense Refund ");
          if ($valid) {
            if (empty($this->contacts[$corr->contact_key])) {
              $this->validator->addError("corr.$c.contact_key.invalid", 'Contact information not found for correction amount $' . $corr->amount);
            }
            else {
              $this->validateContact($corr->contact_key, false, true);
            }
            $this->expense_correction_amount -= (float)$corr->amount;
            $this->expense_correction_count++;
            $this->receipt_correction_amount -= (float)$corr->amount;
            $this->receipt_correction_count++;
            $this->validator->validateMoney($corr->amount, "expense_refunds.$c.amount", "Expense refund amount");
            $this->validator->validateDate($corr->date, "expense_refunds.$c.date", "Expense refund amount");
          }
        }
      }
    }
  }

  /**
   * @param $payments
   */
  protected function validateLoanPayments($payments) {
    if (!is_array($payments)) {
      $this->validator->addError("loan.payments.invalid", "Loan payments must be an array");
    }
    else {
      foreach ($payments as $p => $payment) {
        $date = $payment->date ?? "unknown";
        $prefix = "loan.payments.$p";
        $message = "Loan payment on $date";
        $valid = $this->validator->validateRequired($payment, ['date', 'contact_key'], $prefix, "Loan on $date is incomplete.");
        if ($valid) {
          if (empty($this->contacts[$payment->contact_key])) {
            $this->validator->addError("loanpayment.$p.contact_key.invalid", "Contact information not found for loan payment on date " . $payment->date);
          }
          else {
            $this->validateContact($payment->contact_key, false, true);
          }
          $this->validator->validateDate($payment->date, "$prefix.date","$message date is invalid");
          if (!empty($payment->interest_paid)) $this->validator->validateMoney($payment->interest_paid, "$prefix.interest_paid", "$message interest paid");
          if (!empty($payment->principle_paid)) {
            $valid = $this->validator->validateMoney($payment->principle_paid, "$prefix.principle_paid", "$message principle paid");
            if ($valid && empty($payment->forgiven_amount)) $this->loan_principle_payments += !empty($payment->principle_paid) ? $payment->principle_paid : 0;
          }
          if (!empty($payment->original_amount)) $this->validator->validateMoney($payment->original_amount, "$prefix.original_amount", "$message original amount");
          if (!empty($payment->forgiven_amount)) $this->validator->validateMoney($payment->forgiven_amount, "$prefix.forgiven_amount", "$message forgiven amount");
        }

      }
    }
  }

  /**
   * @param $loans_owed
   */
  protected function validateLoansOwed($loans_owed) {
    $this->loan_balance_amount = 0.0;
    $this->loan_balance_count = 0;
    if (!is_array($loans_owed)) {
      $this->validator->addError("loan.balances.invalid", "Loans owed must be an array");
    }
    else {
      foreach ($loans_owed as $l => $loan) {
        $date = $loan->date ?? "unknown";
        $prefix = "loan.balances.$l";
        $message = "Loan on $date";
        $valid = $this->validator->validateRequired($loan, ['date', 'contact_key', 'original_amount', 'principle_paid'], $prefix, "Loan on $date is incomplete.");
        if ($valid) {
          if (empty($this->contacts[$loan->contact_key])) {
            $this->validator->addError("loan.$l.contact_key.invalid", "Contact information not found for loan on date " . $date);
          }
          else {
            $this->validateContact($loan->contact_key);
          }
          $this->loan_balance_count++;
          $this->loan_balance_amount += ((float)$loan->original_amount ?? 0.0) - ((float)$loan->principle_paid ?? 0.0);
          $this->validator->validateDate($loan->date, "$prefix.date","$message date is invalid");
          if (!empty($loan->principle_paid)) {
            $this->validator->validateMoney($loan->principle_paid, "$prefix.balance", "$message balance");
          }
          if (!empty($loan->original_amount)) {
            $this->validator->validateMoney($loan->original_amount, "$prefix.original_amount", "$message original amount");
          }
        }
      }
    }

  }

  /**
   * @param $contact
   * @param $category
   * @return Expense
   */
  protected function generateExpense($contact, $category) {
    $expn = new Expense();
    $expn->name = $this->replaceSpecialChars($contact->name ?? null);
    $expn->address = $this->replaceSpecialChars($contact->address ?? null);
    $expn->city = $this->replaceSpecialChars($contact->city ?? null);
    $expn->state = $this->replaceSpecialChars($contact->state ?? null);
    $expn->postcode = $this->replaceSpecialChars($contact->postcode ?? null);
    $expn->contact_key = $this->replaceSpecialChars($contact->contact_key ?? null);
    $expn->category = $category;
    return $expn;
  }

  /**
   * @param $expenses
   * @throws Exception
   */
  protected function saveExpensesGT50($expenses) {
    foreach ($expenses as $expense) {
      $payee =  !empty($expense->payee_key) ? $this->contacts[$expense->payee_key] : NULL;
      $creditor = !empty($expense->creditor_key) ? $this->contacts[$expense->creditor_key] :NULL;
      $contact = $this->contacts[$expense->contact_key] ?? NULL;
      if ($payee){
        $contact->payee = $payee->name;
      }
      if($creditor){
        $contact->creditor = $creditor->name;
      }
      if ($contact) {
        $this->addExpenseContactInformation($expense, $contact);
      }
      if ($this->version < 1.1 && isset($expense->payment_category) && $expense->payment_category === 'loan') {
        unset($expense->payment_category);
      }
    }
    $json = json_encode($expenses);
    $this->dataManager->db->executeStatement('select cf_save_expenses(:fund_id, :report_id, :json)',
      [
        'fund_id' => $this->report->fund_id,
        'report_id' => $this->report->report_id,
        'json' => $json
      ]
    );
  }

  /**
   * @param $sums
   *
   * @throws Exception
   */
  protected function saveExpensesLT50($report_data) {
    if(!empty($report_data->version) && $report_data->version >= 1.1) {
        $transaction = $this->generateTransaction('expense', 'payment', $report_data->sums->non_itemized_cash_expenses->amount ?? 0.0, $report_data->period_start);
    } else {
        $transaction = $this->generateTransaction('expense', 'payment', $report_data->sums->expenses_under_50->amount ?? 0.0, $report_data->period_start);
    }
    $transaction->legacy_form_type = 'A/LE50';
    $transaction->expense_type = 1;
    $this->dataManager->em->persist($transaction);
    $this->dataManager->em->flush($transaction);

    $expn = new Expense();

    $expn->transaction_id = $transaction->transaction_id;
    $expn->anonymous = TRUE;
    $expn->contact_key = NULL;
    $expn->name = 'EXPENSES OF $' . $this->thresholds->itemized_expenses . ' OR LESS';
    $expn->address = NULL;
    $expn->city = NULL;
    $expn->state = NULL;
    $expn->postcode = NULL;
    $expn->category = 'A';
    $this->dataManager->em->persist($expn);
    $this->dataManager->em->flush($expn);
  }

    /**
     * @param $sums
     *
     * @throws Exception
     */
    protected function saveUnderThresholdCredit($report_data) {
        $transaction = $this->generateTransaction('expense', 'payment', $report_data->sums->non_itemized_credit_expenses->amount ?? 0.0, $report_data->period_start);
        $transaction->legacy_form_type = 'A/LE50';
        $transaction->expense_type = 1;
        $this->dataManager->em->persist($transaction);
        $this->dataManager->em->flush($transaction);

        $expn = new Expense();

        $expn->transaction_id = $transaction->transaction_id;
        $expn->anonymous = TRUE;
        $expn->contact_key = NULL;
        $expn->name = 'CREDIT EXPENSES OF $' . $this->thresholds->itemized_expenses . ' OR LESS';
        $expn->address = NULL;
        $expn->city = NULL;
        $expn->state = NULL;
        $expn->postcode = NULL;
        $expn->category = 'A';
        $this->dataManager->em->persist($expn);
        $this->dataManager->em->flush($expn);
    }

  /**
   * save corrections
   * @param $corrections
   * @param $category
   * @throws Exception
   */
  protected function saveCorrections($corrections, $category) {
    foreach($corrections as $corr) {
      if (($corr->reported_amount == $corr->corrected_amount) && (empty($corr->difference))) {
        continue;
      }
      $transaction = $this->generateTransaction($category, 'correction', $corr->difference, $corr->date);

      $transaction->description = $corr->description ?? NULL;
      $transaction->expense_type = ($category === 'expense') ? 1 : 0;
      $this->dataManager->em->persist($transaction);
      $this->dataManager->em->flush($transaction);

      $correction = new Correction();
      $correction->transaction_id = $transaction->transaction_id;
      $correction->corrected_amount = $corr->corrected_amount;
      $correction->original_amount = $corr->reported_amount;
      $this->dataManager->em->persist($correction);
      $this->dataManager->em->flush($correction);

      $contact = !empty($corr->contact_key) ? $this->contacts[$corr->contact_key] : null;
      if ($category == 'expense') {
        $expense = $this->generateExpense($contact, null);
        $expense->transaction_id = $transaction->transaction_id;
        $this->dataManager->em->persist($expense);
        $this->dataManager->em->flush($expense);
      }
      else {
        $contribution = $this->generateContribution($contact, null);
        $contribution->transaction_id = $transaction->transaction_id;
        $this->dataManager->em->persist($contribution);
        $this->dataManager->em->flush($contribution);
      }
    }
  }

  /**
   * Save Loan payment information.
   * @param $payments
   * @param string $act
   * @throws Exception
   */
  protected function saveLoanPayments ($payments, $act='payment') {
    foreach ($payments as $payment) {
      if (property_exists($payment, 'forgiven_amount') && $payment->forgiven_amount > 0) {
          $act = 'forgiven';
      }
      $amount = ($payment->principle_paid ?? 0.0) + ($payment->interest_paid ?? 0.0);
      $transaction = $this->generateTransaction('loan', $act, $amount, $payment->date);
      // Ensure that loan payments are removed from the total expenditures for a campaign
      if ($act === 'payment' && $this->version < 1.1) {
        $transaction->expense_type = -1;
      }
      $transaction->description = strtoupper("loan $act");
      $this->dataManager->em->persist($transaction);
      $this->dataManager->em->flush($transaction);

      $contact = $this->contacts[$payment->contact_key];
      $expense = $this->generateExpense($contact, null);
      $expense->transaction_id = $transaction->transaction_id;
      $this->dataManager->em->persist($expense);
      $this->dataManager->em->flush($expense);

      $lp = new LoanPayment();
      $lp->transaction_id = $transaction->transaction_id;
      $lp->balance_owed = $payment->balance ?? null;
      $lp->forgiven_amount = $payment->forgiven_amount ?? 0.0;
      $lp->interest_paid = $payment->interest_paid ?? 0.0;
      $lp->principle_paid = $payment->principle_paid ?? 0.0;
      $lp->original_amount = $payment->original_amount ?? null;
      $this->dataManager->em->persist($lp);
      $this->dataManager->em->flush($lp);
    }
  }

  /**
   * Save expense refunds.
   * @param $refunds
   * @throws Exception
   */
  protected function saveExpenseRefunds($refunds) {
    foreach ($refunds as $refund) {
      $transaction = $this->generateTransaction('expense', 'refund', -1.0 *  $refund->amount, $refund->date);
      $transaction->expense_type = 1;
      $this->dataManager->em->persist($transaction);
      $this->dataManager->em->flush($transaction);

      $contact = $this->contacts[$refund->contact_key];
      $expense = $this->generateExpense($contact, null);
      $expense->transaction_id = $transaction->transaction_id;
      $this->dataManager->em->persist($expense);
      $this->dataManager->em->flush($expense);
    }
  }

  const SUMS_LINES = [
    '1' => 'previous_receipts',
    '2' => 'cash_receipts',
    '3' => 'inkind_receipts',
    '4' => 'total_receipts' ,
    '5' => 'loan_principle_paid',
    '6' => 'receipt_corrections',
    '7' => 'receipt_adjustments',
    '8' => 'campaign_receipts',
    '9' => 'pledges',
    '10' => 'previous_expenditures',
    '11' => 'cash_expenditures',
    '11.1' => 'credit_expenditures',
    '12' => 'inkind_expenditures',
    '13' => 'total_expenditures',
    '14' => 'loan_principle_paid',
    '15' => 'expense_corrections',
    '16' => 'expense_adjustments',
    '17' => 'campaign_expenses',
    '18' => 'cash_on_hand',
    '19' => 'liabilities',
    '20' => 'balance',
  ];

  /**
   * @param $report
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  protected function saveLineItems()
  {
    $categories = ['receipts', 'expenditures', 'cash_summary'];
    foreach($categories as $category) {
      foreach($this->sums->$category as $s) {
          $trans_category = $s->line_item < 10 ? 'contribution' : 'expense';
          $transaction = $this->generateTransaction($trans_category, 'sum', $s->amount, $s->date);
          $this->dataManager->em->persist($transaction);
          $this->dataManager->em->flush();
          $sum = new Sum();
          $sum->transaction_id = $transaction->transaction_id;
          $sum->legacy_line_item = $s->line_item;
          $sum->tx_count = $s->count;
          $this->dataManager->em->persist($sum);
          $this->dataManager->em->flush($sum);
      }
    }

  }

  /**
   * @param array $contributions
   */
  function validateInKindContributions(array $contributions) {

    $this->in_kind_amount = 0.0;
    $this->in_kind_count = 0;
    foreach($contributions as $c => $contrib) {
      $date = $contrib->date ?? NULL;
      $e_msg = "The inkind contribution on $date";
      $desc = $contrib->description ?? 'Unknown';
      $valid = $this->validator->validateRequired($contrib, [
        'contact_key',
        'amount',
        'date',
      ], "contribution_inkind.$c", "$e_msg, for $desc");

      if ($valid) {
        if(isset($contrib->election)) {
          $this->validator->validateList($contrib, 'election', static::PRIM_GEN_VALUES, "contribution_inkind.$c", "Election code for in-kind contribution on date " . $date . " must be on of P, G or N.");
        } else {
          $this->validator->addError("contribution_inkind.$c.election.incomplete", "Election code for in-kind contribution on date " . $date . " must be on of P, G or N.");
        }

        if (empty($this->contacts[$contrib->contact_key])) {
          $this->validator->addError("loan.$c.contact_key.invalid", "Contact information not found for in-kind contribution on date " . $date);
        }
        else {
          $this->validateContact($contrib->contact_key, true, true);
        }
        $this->validator->validateMoney($contrib->amount, "contribution_inkind.$c.amount", "For $desc amount must be a number");
        $this->validator->validateDate($contrib->date, "contribution_inkind.$c.date", "Date for $desc must be a valid date");
        $this->in_kind_amount += (float)($contrib->amount ?? 0.0);
        $this->in_kind_count++;
      }
      if (empty($this->contacts[$contrib->contact_key ?? NULL])) {
        $this->validator->addError("contribution_inkind.$c.incomplete", "Contact information not provided for $desc");
      }
    }
  }

  /**
   * @param $report_data
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  function saveInKindContributions($report_data) {
    foreach($report_data->contributions_inkind as $contrib) {

      $transaction = $this->generateTransaction('contribution', 'receipt', $contrib->amount, $contrib->date);
      $transaction->inkind = TRUE;
      $transaction->expense_type = 1;
      $transaction->description = $contrib->description ?? NULL;
      $this->dataManager->em->persist($transaction);
      $this->dataManager->em->flush($transaction);

      $contact = $this->contacts[$contrib->contact_key] ?? NULL;
      if ($contact) {
        $contribution_inkind = $this->generateContribution($contact, $contrib->election);
        $contribution_inkind->transaction_id = $transaction->transaction_id;
        $this->dataManager->em->persist($contribution_inkind);
        $this->dataManager->em->flush($contribution_inkind);

        if (isset($contact->aggregate_general) || isset($contact->aggregate_primary)) {
          $sum = new Sum();
          $sum->transaction_id = $transaction->transaction_id;
          $sum->aggregate_amount = $contrib->election == 'P' ? $contact->aggregate_primary : $contact->aggregate_general;
          $this->dataManager->em->persist($sum);
          $this->dataManager->em->flush($sum);
        }
      }
    }

  }

  /**
   * Submit C4 Report.
   * @param $json
   * @param Fund $fund
   * @return \stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws ORMException
   * @throws OptimisticLockException
   * @throws Exception
   */
  public function submitReport($json, Fund $fund) {
    $response = new stdClass();
    $this->loan_principle_payments = 0.0;
    $this->validator->clear();
    //assign report_data to report portion of $json object / XML submission falls on default
      if(isset($json->report)) {
          $report_data = $json->report;
          $report_data->vendor_name = $json->metadata->vendor_name;
          $report_data->version = $json->metadata->submission_version;
      } else {
          $report_data = $json;
      }
    if ($this->validateReport($report_data, $fund)) {
      $response = parent::submitReport($report_data, $fund);

      if (!empty($report_data->misc_receipts)) {
        $this->saveMiscReceipts($report_data->misc_receipts);
      }

      if (!empty($report_data->pledge)) {
        $this->savePledges($report_data->pledge);
      }

      if (!empty($report_data->expenses)) {
        $this->saveExpensesGT50($report_data->expenses);
      }

      if (!empty($report_data->sums->expenses_under_50) || !empty($report_data->sums->non_itemized_cash_expenses)) {
        $this->saveExpensesLT50($report_data);
      }
      if(!empty($report_data->sums->non_itemized_credit_expenses)) {
        $this->saveUnderThresholdCredit($report_data);
      }

      if (!empty($report_data->contribution_corrections)) {
        $this->saveCorrections($report_data->contribution_corrections, 'contribution');
      }

      if (!empty($report_data->expense_corrections)) {
        $this->saveCorrections($report_data->expense_corrections, 'expense');
      }

      if (!empty($report_data->expense_refunds)) {
        $this->saveExpenseRefunds($report_data->expense_refunds);
      }

      if (!empty($report_data->debt)) {
        $this->saveDebt($report_data->debt);
      }

      if (!empty($report_data->loan)) {
        if (!empty($report_data->loan->receipts)) {
          $this->saveLoans($report_data->loan->receipts);
        }
        if (!empty($report_data->loan->payments)) {
          $this->saveLoanPayments($report_data->loan->payments,'payment');
        }
        if (!empty($report_data->loan->balances)) {
          $this->saveLoanPayments($report_data->loan->balances, 'balance');
        }
      }

      if (!empty($report_data->contributions_inkind)) {
        $this->saveInKindContributions($report_data);
      }

      if (!empty($report_data->sums)){
          $this->saveLineItems();
      }
    } else {
      $response->success = false;
    }
    $response->messages = $this->validator->getMessages();
    $response->message = $this->validator->getMessageText();
    return $response;
  }

  /**
   * @param $pledges
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  function savePledges($pledges) {
    foreach($pledges as $pledge) {
      $amount = !empty($pledge->amount) ? (float) $pledge->amount : NULL;
      $date = $pledge->date ?? NULL;
      $transaction = $this->generateTransaction('pledge', 'receipt', $amount, $date, TRUE, FALSE);
      $this->dataManager->em->persist($transaction);
      $this->dataManager->em->flush($transaction);

      if (!empty($this->contacts[$pledge->contact_key])) {
        $contact = $this->contacts[$pledge->contact_key];
        $contribution = $this->generateContribution($contact, $pledge->election);
        $contribution->transaction_id = $transaction->transaction_id;
        $this->dataManager->em->persist($contribution);
        $this->dataManager->em->flush($contribution);
        if (isset($contact->aggregate_general) || isset($contact->aggregate_primary)) {
          $sum = new Sum();
          $sum->transaction_id = $transaction->transaction_id;
          $sum->aggregate_amount = $pledge->election == 'P' ? $contact->aggregate_primary : $contact->aggregate_general;
          $this->dataManager->em->persist($sum);
          $this->dataManager->em->flush($sum);
        }
      }
    }
  }

  function saveDebt($debts) {
    foreach ($debts as $debt) {
      $amount = !empty($debt->amount) ? (float) $debt->amount : NULL;
      $date = $debt->date ?? NULL;
      $transaction = $this->generateTransaction('expense', 'invoice', $amount, $date, TRUE, FALSE);
      $this->dataManager->em->persist($transaction);
      $this->dataManager->em->flush($transaction);

      $contact = $this->contacts[$debt->contact_key];
      if ($contact) {
        $contribution = $this->generateExpense($contact, $debt->category);
        $contribution->transaction_id = $transaction->transaction_id;
        $this->dataManager->em->persist($contribution);
        $this->dataManager->em->flush();
      }
    }
  }

  /**
   * @param $misc_receipts
   *
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Exception
   */
  function saveMiscReceipts($misc_receipts) {
    $this->deposit_count = 0;
    $this->deposit_amount = 0.0;
    foreach($misc_receipts as $receipt) {
      $amount = !empty($receipt->amount) ? (float) $receipt->amount : NULL;
      $date = (strtolower($receipt->date) == 'various') ? NULL : $receipt->date ?? NULL;
      $legacy_line_item = '2.A1.' . $this->deposit_count;
      $transaction = $this->generateTransaction('deposit', 'sum', $amount, $date, FALSE, FALSE);
      $this->dataManager->em->persist($transaction);
      $this->dataManager->em->flush($transaction);
      $sum = new Sum();
      $sum->transaction_id = $transaction->transaction_id;
      $sum->legacy_line_item = $legacy_line_item;
      $this->dataManager->em->persist($sum);
      $this->dataManager->em->flush($sum);
    }

    $this->dataManager->em->persist($sum);
    $this->dataManager->em->flush($sum);
  }
}
