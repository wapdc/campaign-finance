<?php


namespace WAPDC\CampaignFinance;


use Aws\Credentials\CredentialProvider;
use Aws\S3\S3Client;
use WAPDC\Core\AWSFileServiceBase;
use WAPDC\Core\FileServiceInterface;

class CommercialAWSFileService extends AWSFileServiceBase {
  /**
   * The values of the public and private bucket keys contain
   * The name of the environment variable that contains the bucket name.
   */
  const CONFIG = [
    'region' => 'us-west-2',
    'version' => 'latest',
    'buckets' => [],
    'accessModes' => [
      'public' => 'public-read',
      'private' => 'private'
    ],
  ];

   public function __construct()
  {
      $_ENV['AWS_ACCESS_KEY_ID'] = $_ENV['COMMERCIAL_AWS_ACCESS_KEY_ID'];
      $_ENV['AWS_SECRET_ACCESS_KEY'] = $_ENV['COMMERCIAL_AWS_SECRET_ACCESS_KEY'];
      parent::__construct();

  }

  public function assembleS3Url($file, $access_mode = 'private') : String {
    $bucket = $this->buckets[$access_mode];
    $region = static::CONFIG['region'];
    return "https://$bucket.s3.$region.amazonaws.com/" . $file;
  }

  public function setBucket($bucket_name, $access_mode = 'private'){
      $this->buckets = [$access_mode => $bucket_name];
  }
}
