<?php


namespace WAPDC\CampaignFinance;


use WAPDC\Core\DataManager;
use \Exception;
use \DateTime;
use \stdClass;

class Validator {
  /** @var DataManager */
  protected $dataManager;

  protected $messages = [];

  protected $valid = TRUE;

  public function __construct(DataManager $dataManager) {
    $this->dataManager = $dataManager;
  }

  public function addError($code, $message) {
    $this->valid = FALSE;
    $this->addMessage($code, $message);
  }

  public function addMessage($code, $message) {
    $this->messages[$code] = $message;
  }

  public function getMessages() {
      if(count($this->messages))
          return $this->messages;
      else {
          return null;
      }
  }

  /**
   * Returns all error messages one per line.
   * @return string
   */
  public function getMessageText() {
    $text = '';
    foreach($this->messages as $message) {
      $text .= "$message\n";
    }
    return $text;
  }

  public function isValid() {
    return $this->valid;
  }

  public function clear() {
    $this->messages = [];
    $this->valid = TRUE;
  }

  public function validateDate($date, $error_prefix, $message) {
    if ($date) {
      try {
        new DateTime($date);
      }
      catch (Exception $e) {
        $this->addError("$error_prefix.invalid", "$message");
        return false;
      }
    }
    return true;
  }

  /**
   * @param stdClass $object
   *   $data
   * @param array $properties
   *   Array of property names to require.
   * @param string $error_prefix
   *   Error code prefix
   * @param string $message_prefix
   *   Message prefix
   * @return bool
   *   Indicates true if all valid fields are present.
   */
  public function validateRequired($object, $properties, $error_prefix, $message_prefix) {
    $missing_fields = FALSE;
    foreach ($properties as $property) {
      if (!empty($object->$property) && is_string($object->$property)) {
        $object->$property = trim($object->$property);
        if (strlen($object->$property) > 255) {
          $this->addError("$error_prefix.invalid", "$message_prefix exceeds length limit of 255 characters");
        }
      }
      if (empty($object->$property)
        && ($object->$property ?? null) !== FALSE
        && ($object->$property ?? null) !== 0
        && ($object->$property ?? null) !== 0.0
      ) {
        $missing_fields = TRUE;
      }
    }
    if ($missing_fields) {
      $this->addError("$error_prefix.incomplete", "$message_prefix incomplete");
    }
    return !$missing_fields;
  }

  /**
   * If $property of $object is iterable then $values will be searched for
   *   the individual items of $property
   *
   * @param $object
   * @param $property
   * @param $values
   * @param $error_prefix
   * @param $message_prefix
   */
  public function validateList($object, $property, $values, $error_prefix, $message_prefix) {
    if (!empty($object->$property)) {
      if(is_iterable($object->$property)) {
        foreach ($object->$property as $item) {
          if (array_search($item, $values) === FALSE) {
            $this->addError("$error_prefix.$property.invalid", "$message_prefix invalid value for $property");
          }
        }
      } elseif (array_search($object->$property, $values) === FALSE) {
        $this->addError("$error_prefix.$property.invalid", "$message_prefix invalid value for $property");
      }
    }
  }

  public function validateMoney(&$value, $error_prefix, $message_prefix) {
    if ($value && str_starts_with($value, '$')) {
      $value = ltrim($value,'$');
    }
    if ($value && !is_numeric($value)) {
      $this->addError("$error_prefix.invalid", "$message_prefix is not valid" );
      return false;
    }
    return true;
  }

  public function validateCategory($expense, $category_list, $error_prefix, $message_prefix)
  {
    if (!empty($expense->category)) {
      if (empty($category_list[$expense->category])) {
          $this->addError("$error_prefix.invalid", "$message_prefix");
      }
      else {
        if (empty($expense->category_description)) {
          $expense->category_description = $category_list[$expense->category];
        }
      }
    }
  }

  public function validateContactProperties(&$contact) {
    if (isset($contact->name) && strlen($contact->name) > 240) {
      $this->addError("contact.name.length", "Contact name $contact->name, cannot be greater than 240 characters.");
    }
    if (isset($contact->address) && strlen($contact->address) > 120) {
      $this->addError("contact.address.length", "Contact address $contact->address cannot be greater than 120 characters.");
    }
    if (isset($contact->city) && strlen($contact->city) > 120) {
      $this->addError("contact.city.length", "Contact city cannot be greater than 120 characters.");
    }
    if (isset($contact->state) && strlen($contact->state) > 120) {
      $this->addError("contact.state.length", "Contact state cannot be greater than 120 characters.");
    }
    if (isset($contact->postcode) && strlen($contact->postcode) > 40) {
      $this->addError("contact.postcode.length", "contact postal code cannot be longer than 40 characters.");
    }
    if (isset($contact->employer->occupation) && strlen($contact->employer->occupation) > 240) {
      $this->addError("contact.employer.occupation.length", "contact employer occupation cannot be greater than 240 characters.");
    }
    if (isset($contact->employer->name) && strlen($contact->employer->name) > 240) {
      $this->addError("contact.employer.name.length", "contact employer name cannot be longer than 240 characters.");
    }
    if (isset($contact->employer->address) && strlen($contact->employer->address) > 120) {
      $this->addError("contact.employer.address.length", "contact employer address cannot be longer than 120 characters.");
    }
    if (isset($contact->employer->city) && strlen($contact->employer->city) > 120) {
      $this->addError("contact.employer.city.length", "contact employer city cannot be longer than 120 characters.");
    }
    if (isset($contact->employer->state) && strlen($contact->employer->state) > 120) {
      $this->addError("contact.employer.state.length", "contact employer state cannot be longer than 120 characters.");
    }
    if (isset($contact->employer->postcode) && strlen($contact->employer->postcode) > 40) {
      $this->addError("contact.employer.postcode.length", "contact employer postal code cannot be longer than 40 characters.");
    }
  }
}
