<?php

namespace WAPDC\CampaignFinance;

use \stdClass;
use \Exception;
use \DateTime;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\CommitteeLog;
use WAPDC\CampaignFinance\Model\Party;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\CampaignFinance\Model\CommitteeRegistrationDraft;
use WAPDC\CampaignFinance\Model\RegistrationAttachment;
use WAPDC\CampaignFinance\SubmissionDecoder\PaperRegistrationDecoder;
use WAPDC\CampaignFinance\SubmissionDecoder\RegistrationDecoder;
use WAPDC\CampaignFinance\SubmissionDecoder\RegistrationLegacyDecoder;
use WAPDC\CampaignFinance\SubmissionDecoder\SubmissionDecoderInterface;
use WAPDC\Core\EntityProcessor;
use WAPDC\Core\FilerProcessor;
use WAPDC\Core\FileServiceInterface;
use WAPDC\Core\Messenger as WAPDCMessenger;
use WAPDC\Core\Model\Attachment;
use WAPDC\Core\Model\Candidacy;
use WAPDC\Core\Model\Jurisdiction;
use WAPDC\Core\Model\JurisdictionOffice;
use WAPDC\Core\Model\MessengerContact;
use WAPDC\Core\Model\Office;
use WAPDC\Core\Model\Position;
use WAPDC\Core\Model\User;
use WAPDC\Core\SendGrid\MailerInterface;
use WAPDC\Core\Model\Entity;


class RegistrationManager {

  /** @var array */
  private $mailer_recipients = array();

  /** @var MailerInterface */
  private $mailer;

  /** @var WAPDCMessenger */
  private $messenger;

  /**
   * @var CFDataManager
   */
  private $_dataManager;

  /**
   * @var CommitteeManager
   */
  private $_committeeManager;

  /** @var RegistrationDraftManager */
  private $_registrationDraftManager;

  /** @var FilerProcessor */
  private $_filerProcessor;

  private $_fileService;

  /**
   * RegistrationManager constructor.
   *
   * @param CFDataManager $dataManager
   * @param CommitteeManager $committeeManager
   * @param RegistrationDraftManager $registrationDraftManager
   * @param \WAPDC\Core\FileServiceInterface $fileService
   */
  public function __construct(CFDataManager $dataManager, CommitteeManager $committeeManager, RegistrationDraftManager $registrationDraftManager, FilerProcessor $filerProcessor, FileServiceInterface $fileService = null) {
    $this->_dataManager = $dataManager;
    $this->_committeeManager = $committeeManager;
    $this->_registrationDraftManager = $registrationDraftManager;
    $this->_fileService = $fileService;
    $this->_filerProcessor = $filerProcessor;
  }

  private function _getSurplusVerificationOptions($admin_data) {
    $options = [];
    $params = [
      'entity_id' => $admin_data->entity_id ?? null
    ];
    $sql = "
      select
        e.entity_id,
        e.name,
        surplus.committee_id,
        case
          when surplus.committee_id is not null then true
          else false
        end as merge,
        COALESCE(surplus.filer_id, e.filer_id) as filer_id
      from entity e
        left join committee surplus
          on surplus.person_id = e.entity_id
            and surplus.pac_type = 'surplus'
      where e.entity_id = :entity_id";

    $status = $this->_dataManager->db->executeQuery($sql, $params)->fetchAllAssociative()[0] ?? Null;

    $options['needs_2_file'] = false;
    // Don't return a filer id when we need a 2 file because that implies we're going to
    // use that filer id on the candidacy rather than generating a new one.
    $options['filer_id'] =  $status['filer_id'];
    // Make sure filer_id has a "*" in it
    if ($options['filer_id'] && !strpos($options['filer_id'], '*')) {
      $options['filer_id'] = preg_replace("/(.*)(-|\s)/", '\1*', $options['filer_id']);
    }
    $options['mergeable'] = $status['merge'];
    $options['committee_id'] = $status['committee_id'];
    $options['entity_id'] = $status['entity_id'];
    return $options;
  }

  private function _getCandidateVerificationStatus($params)
  {
    $sql = "
      select
        p.entity_id,
        p.name,
        ca.candidacy_id,
        ca.committee_id as committee_id,
        c.reporting_type,
        c.mini_full_permission,     
        case
          when ca.committee_id is not null then true
          else false
        end as merge,
        case
          when ca2.office_code <> :office_code or ca2.jurisdiction_id <> :jurisdiction_id then ca2.candidacy_id
        end as needs_2_file,
        COALESCE(ca.filer_id, p.filer_id) as filer_id      
      from entity p
        left join candidacy ca
          on p.entity_id = ca.person_id
            and ca.office_code = :office_code
            and ca.jurisdiction_id = :jurisdiction_id
            and left(ca.election_code, 4) = left(:election_code, 4)
        left join committee c
          on ca.committee_id = c.committee_id
        left join candidacy ca2
          on ca2.person_id = p.entity_id
            and left(ca2.election_code, 4) = left(:election_code, 4)
            and ca2.filer_id = p.filer_id
      where p.entity_id = :entity_id and p.is_person";

    $result = $this->_dataManager->db->executeQuery($sql, $params)->fetchAllAssociative();
    return $result[0] ?? false;
  }

  /**
   * @param $submission
   * @return mixed|null
   * @throws Exception
   */
  private function _getCandidateVerificationOptions($submission, $admin_data)
  {
    $options = [];
    $params = [
      'election_code' => $submission->committee->election_code ?? null,
      'jurisdiction_id' => $submission->committee->candidacy->jurisdiction_id ?? $submission->committee->candidacy->location_id ?? null,
      'office_code' => $submission->committee->candidacy->office_code ?? $submission->committee->candidacy->offcode ?? null,
      'entity_id' => $admin_data->entity_id ?? null,
    ];

    if (empty($params['entity_id'])) {
      $status = [
        'filer_id' => null,
        'needs_2_file' => false,
        'merge' => false,
        'candidacy_id' => null,
        'entity_id' => null,
        'committee_id' => null,
      ];
    }
    else {
      $status = $this->_getCandidateVerificationStatus($params);
    }

    $options['needs_2_file'] = $status['needs_2_file'];
    // Don't return a filer id when we need a 2 file because that implies we're going to
    // use that filer id on the candidacy rather than generating a new one.
    $options['filer_id'] = !empty($status['needs_2_file']) ? NULL : $status['filer_id'];
    $options['mergeable'] = $status['merge'];
    $options['candidacy_id'] = $status['candidacy_id'];
    $options['committee_id'] = $status['committee_id'];
    $options['entity_id'] = $status['entity_id'];
    if (($status['reporting_type'] ?? null)  == 'mini' && $submission->committee->reporting_type == 'full') {
      $options['error'] = 'Mini to full without prior permission';
    }

    return $options;
  }

  private function _getPacVerificationOptions($registration, $admin_data, $submission) {
    // Default assumption is that we have a new committee.
    $options = ['mergeable' => false, 'filer_id' => null];

    // Load the committee that is the most likely merge candidate.
    if (!empty($admin_data->entity_id)) {

      $committees = $this->_dataManager->db->executeQuery("
        SELECT 
            c.committee_id, c.election_code, c.start_year, c.end_year, c.continuing,
            c.reporting_type, c.mini_full_permission,   
            coalesce(c.filer_id, e.filer_id) as filer_id, 
            case 
                when c2.committee_id is not null then true 
                else false end  as needs_2_file, 
            case when c2.continuing then true end as continuing_end
       FROM entity e join committee cor ON cor.committee_id=:committee_id  
             left join committee c on e.entity_id = c.person_id            
               and c.pac_type=cor.pac_type
               and (
                 (not coalesce(cor.continuing,false) and 
                     cor.election_code = c.election_code)
                 or (cor.continuing and cor.start_year between c.start_year and coalesce(c.end_year, 9999))    
               ) 
               and cor.exempt is not distinct from c.exempt 
             left join committee c2 on c2.person_id = e.entity_id
               and c2.filer_id = e.filer_id
               and cor.start_year between c2.start_year and coalesce(c2.end_year, 9999)
        WHERE e.entity_id = :entity_id
        order by start_year desc
        ",
        [
          'entity_id' => $admin_data->entity_id,
          'committee_id' => $registration['committee_id'],
        ]
      )->fetchAllAssociative() + [null];

      if ($committees) {
        $committee = $committees[0];
        $options['needs_2_file'] = !empty($committee['needs_2_file']);
        if (!$options['needs_2_file'] ) {
          $options['filer_id'] = $committee['filer_id'];
        }
        $options['continuing_end'] = !empty($committee['continuing_end']);

        // Determine if we can merge the committee.
        if ($committee['committee_id']) {

          switch (true) {

            // Single election committee merged to a single election committee
            case (!$registration['continuing'] && !$committee['continuing']):
              $options['mergeable'] = $registration['election_code'] == $committee['election_code'];
              if ($registration['start_year'] == $committee['start_year'] && $registration['election_code'] != $committee['election_code']) {
                $options['needs_2_file'] = true;
              } else {
                $options['filer_id'] = $committee['filer_id'];
              }
              break;


            // A continuing committee can be merged onto a single election committee
            case (!$committee['continuing'] && $registration['continuing']):
              $options['filer_id'] = $committee['filer_id'];
              $options['mergeable'] = $committee['start_year'] == $registration['start_year'];
              $options['continuing'] = true;
              $options['needs_2_file']  = false;
              break;

            // Two continuing committees can always be merged.
            case ($committee['continuing'] && $registration['continuing']):
              $options['mergeable'] = true;
              $options['filer_id'] = $committee['filer_id'];
              $options['continuing'] = true;
              break;
          }
          if (!empty($options['mergeable'])) {
            $options['committee_id'] = $committee['committee_id'];
            if ($committee['reporting_type'] == 'mini' && $submission->committee->reporting_type == 'full') {
              $options['error'] = 'Mini to full without prior permission';
            }
          }
        }
      }
    }
    return $options;
  }


  /**
   * @param $registration_id
   * @param $delete_filer_id
   * @throws Exception
   */
  public function unverifyRegistration($registration_id, $delete_filer_id, $user_name){
    if (empty($registration_id)) {
      throw new Exception('Verifying a committee requires a registration_id.');
    }
    /** @var Registration $registration */
    $registration = $this->_dataManager->em->find(Registration::class, $registration_id);
    if (empty($registration)) {
      throw new Exception("Missing Registration: $registration_id");
    }

    /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $registration->committee_id);
    if (empty($committee)) {
      throw new Exception("Missing Committee for Registration: $registration_id");
    }

    if ($committee->registration_id != $registration_id) {
      throw new Exception("Can only unverify the current registration.");
    }

    if (!empty($registration->admin_data)) {
      $registration->admin_data = null;
    }
    $committee->autoverify = false;
    $registration->verified = false;
    if ($delete_filer_id == "yes") {
      $committee->person_id = null;
      $committee->filer_id = null;
      // delete candidacy if not from declarations
      $sql = 'delete from candidacy where sos_candidate_id is null and committee_id = :committee_id';
      $deleted = $this->_dataManager->db->executeStatement($sql, ['committee_id' => $committee->committee_id]);
      if (!$deleted) {
        // otherwise just remove the committee id from the candidacy
        $sql = 'update candidacy set committee_id = null where committee_id = :committee_id';
        $this->_dataManager->db->executeQuery($sql, ['committee_id' => $committee->committee_id]);
      }
    }

    $this->logRegistrationAction($registration_id,$user_name,'unverify','Successfully unverified registration submitted on '.date_format($registration->submitted_at, 'Y-m-d H:i:s'));

    $this->_dataManager->em->flush($registration);
    $this->_dataManager->em->flush($committee);
  }

  /**
   * @param int $registration_id
   *   The registration id to verify
   * @param boolean $migration
   *   When true assume we are in migration mode, so we're going to premark the c1 records as created
   *   so that we do not update C1 records for verified registration.
   * @param boolean $send_notification
   *   When we verify we want to send a message to the certification email
   * @param $user_name
   * @return Committee
   * @throws Exception
   */
  public function verifyRegistration($registration_id, $migration=FALSE, $send_notification=true, $user_name = null) {
    if (empty($registration_id)) {
      throw new Exception('Verifying a committee requires a registration_id.');
    }
    /** @var Registration $registration */
    $registration = $this->_dataManager->em->find(Registration::class, $registration_id);
    if (empty($registration)) {
      throw new Exception("Missing Registration: $registration_id");
    }

    /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $registration->committee_id);
    if (empty($committee)) {
      throw new Exception("Missing Committee for Registration: $registration_id");
    }

    // If this committee is not the current registration, reload the current registration so that we end up using its data.
    if ($committee->registration_id != $registration_id) {
      $registration_id = $committee->registration_id;
      $registration->verified = true;
      $this->_dataManager->em->flush($registration);
      $registration = $this->_dataManager->em->find(Registration::class, $registration_id);
    }

    if ($registration->reporting_type === 'full' && $committee->reporting_type === 'mini' && $committee->mini_full_permission !== true) {
      throw new Exception('This is a mini to full reporting change without permission, which is highly unexpected. Please contact IT.');
    }

    // Retrieve the registration in a stdCall format.
    $submission = $this->getRegistrationSubmission($registration_id);

    $submission->committee->registration_id = $registration_id;

    $this->_committeeManager->saveCommitteeFromSubmission($submission, $migration);
    // Make sure committee person ID matches one generated if any.
    $committee->person_id = $submission->committee->person_id;


    $query = "select registration_id, committee_id, name, verified from registration where committee_id = :committee_id and verified is null and registration_id <> :registration_id";
    // needs to find all registrations filtered on committee id and not this registration
    $registrations = $this->_dataManager->db->executeQuery( $query, ['committee_id' => $committee->committee_id, 'registration_id' => $registration_id])->fetchAllAssociative();
    foreach($registrations as $value ) {
      if ($value) {
        $reg_id = $value['registration_id'];
        $old_registration = $this->_dataManager->em->find(Registration::class, $reg_id);
        $old_registration->verified = TRUE;
        $this->_dataManager->em->flush($old_registration);
      }
    }

    $ep = new EntityProcessor($this->_dataManager);
    if (!$committee->filer_id) {
      if (!empty($submission->admin_data->filer_id)) {
        $filer_id =  $submission->admin_data->filer_id;
      }
      else {
        if ($committee->pac_type === 'candidate') {
          $candidacy = $this->_dataManager->em->find(Candidacy::class, $submission->committee->candidacy->candidacy_id);
          $filer_id = $candidacy->filer_id;
        } else if ($committee->pac_type === 'surplus') {
          /** @var Entity $person */
          $person = $this->_dataManager->em->find(Entity::class, $submission->committee->person_id);
          // A committee filer_id being set implies that we got it from admin_data.
          if (!$committee->filer_id) {
            if (!$person->filer_id) {
              $person->filer_id = $this->_filerProcessor->generateFilerId($person->name);
              $this->_dataManager->em->flush($person);
            }
            $filer_id = $this->_filerProcessor->assignSurplusFilerId($person->entity_id);
          }
          else {
            $filer_id = $committee->filer_id;
          }
        } else {
          $filer_id = $this->_filerProcessor->assignFilerIdByCommitteeId($committee->committee_id);
        }
      }
      $committee->filer_id = $filer_id;


      // update filer id of entity when not set.
      $entity = $ep->getEntity($committee->person_id);
      if (!$entity->filer_id) {
        $entity->filer_id = $filer_id;
        $ep->saveEntity($entity);
      }
      else {
        // End any pre-existing continuing committees that have not ended.
        if ($committee->committee_type == 'CO' && $entity->filer_id == $committee->filer_id) {
          $end_year= $committee->start_year - 1;
          $this->_dataManager->db->executeStatement("UPDATE committee set end_year=:end_year 
               where person_id = :entity_id and continuing and end_year is null 
                 and filer_id = :filer_id
                 and start_year < :start_year",
          [
            'entity_id' => $entity->entity_id,
            'start_year' => $committee->start_year,
            'end_year' => $end_year,
            'filer_id' => $committee->filer_id
          ]);
        }
      }
    }


    $registration->verified = TRUE;
    $this->_dataManager->em->flush($registration);
    $this->logRegistrationAction($registration_id,$user_name,'committee verification','Successfully verified ' . $user_name . ' registration');

    $ep->grantRoleToEmail($committee->person_id, $registration->username, 'cf_reporter', $this->mailer);

    /** @var Committee $committee */
    $committee->autoverify = true;
    $this->_dataManager->em->flush($committee);
    if ($this->mailer && $send_notification) {
      $this->mailer->setMessageTemplate('d-f976dc73795248f3aabeed0e631e25a2');
      $this->mailer->addRecipient($submission->certification_email, '',
        [
          'committee_name' => $submission->committee->name,
          'reporting_type' => $submission->committee->reporting_type,
          'pac_type' => $submission->committee->pac_type,
        ]);
      $this->mailer->send();
    }
    return $committee;
  }

  /**
   * Given a registration_id, determine if the associated registration is eligible for auto-verification
   *
   * @param $registration_id
   * @return bool
   * @throws Exception
   */
  public function getAutoVerifyEligibility($registration_id){
    if (empty($registration_id)){
      return false;
    }
    //get registration
    $registration = $this->_dataManager->em->find(Registration::class, $registration_id);
    if (empty($registration)){
      throw new Exception("Missing Registration: $registration_id");
    }
    //get committee
    $committee = $this->_dataManager->em->find(Committee::class, $registration->committee_id);
    if (empty($committee)){
      throw new Exception("Missing Committee for Registration: $registration_id");
    }
    // filer_id must be set on committee
    $filer_id = $committee->filer_id;
    if (!$filer_id){
      return false;
    }
    // autoverify flag must be set on committee
    if (empty($committee->autoverify)) {
      return false;
    }
    // get submission
    $submission = $this->getRegistrationSubmission($registration_id);

    // not on mini-to-full amendments
    if ($committee->reporting_type=='mini' && $committee->reporting_type != $submission->committee->reporting_type && empty($committee->mini_full_permission)){
      return false;
    }
    // candidate checks
    if ($committee->pac_type == 'candidate'){
      //make sure there is a candidacy record associated with the committee
      $candidacy = $this->_dataManager->em->getRepository(Candidacy::class)->findBy(['committee_id' => $committee->committee_id]);
      if (!$candidacy){
        return false;
      }
      $candidacy = $candidacy[0];
      //make sure the office exists
      $office = $this->_dataManager->em->find(Office::class, $candidacy->office_code);
      if (!$office){
        return false;
      }
      // jurisdiction must exist
      $jurisdiction = $this->_dataManager->em->find(Jurisdiction::class, $candidacy->jurisdiction_id);
      if (!$jurisdiction){
        return false;
      }
      //if the jurisdiction or office has changed with the amendment, then requires manual verification.
      if($submission->meta_data->candidacy->office_code != $submission->committee->candidacy->offcode || $submission->meta_data->candidacy->jurisdiction_id != $submission->committee->candidacy->jurisdiction_id) {
          $committee->autoverify = false;
          $registration->verified = false;
          return false;
      }
    }

    return true;
  }

  /**
   * Determine what type of decoder to use for a registration
   * @param Registration $reg
   * @return SubmissionDecoderInterface
   */
  public function getSubmissionDecoder(Registration $reg) {

      if(!isset($reg->user_data)) {
       $decoder = new PaperRegistrationDecoder();
       return $decoder;
    }
    $is_xml = strpos($reg->user_data, '<') ===0;
    if ($is_xml) {
      $decoder = new RegistrationLegacyDecoder();
      return $decoder;
    } else {
      $decoder = new RegistrationDecoder();
      return $decoder;
    }
  }

  /**
   * @param $committee_id
   * @param $admin
   *   This is an authorization variable to hide admin specific stuff from the public
   * @return \stdClass
   * @throws Exception
   */
  public function getCurrentRegistration($committee_id, $admin = false) {
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    if ($committee->registration_id) {
      return $this->getRegistrationSubmission($committee->registration_id, $admin);
    } else {
      return $this->_committeeManager->getCommitteeSubmission($committee_id, $admin);
    }

  }

  /**
   * Gets the options available to the user at registration verification time. These options are used to display
   * the outcomes of verification to the admin user in order for them to confirm or reject them.
   * @param \stdClass $admin_data
   *    A standard class object containing
   *       registration_id: id of the registration we are verifying.
   *       person_id: Only present in CA committees. This is the matched person.
   *       committee_id: Present only in CO committees. This is the matched committee.
   *    It is important to recognize that this is *not* the actual admin_data that will be persisted to the registration.
   *    Rather, this is (at this point in time) just an object intended to carry around values required by the function
   *    to derive verification status.
   * @return mixed[]
   *    An assoc array containing
   *       filer_id: If set, this is the filer id assigned to the registration. If null, a new one must be generated.
   *       needs_2_file: If true, 2file filer_id required.
   *       mergeable: If true, this the verified registration committee can be merged with an existing committee.
   *       target_committee: If mergeable is true, then target_committee reflects the committee that this committee's
   *         data, including the registration being verified, will be moved onto.
   * @throws Exception
   */
  public function getVerificationOptions($admin_data)
  {
    [$registration] = $this->_dataManager->db->executeQuery("
        SELECT r.user_data, c.committee_id, c.election_code, c.continuing, c.start_year, c.end_year, c.pac_type
          FROM registration r 
            join committee c on r.committee_id = c.committee_id 
        where r.registration_id = :registration_id",
      ['registration_id' => $admin_data->registration_id])->fetchAllAssociative();

    $submission = json_decode($registration['user_data']);

    if ($submission->committee->committee_type === 'CA') {
      // Old submissions may still have location or location_id which was renamed, so replace them.
      if (isset($submission->committee->candidacy)) {
        $candidacy = $submission->committee->candidacy;
        if (isset($candidacy->location) && !isset($candidacy->jurisdiction)) {
          $candidacy->jurisdiction = $candidacy->location;
          unset($candidacy->location);
        }
        if (isset($candidacy->location_id) && !isset($candidacy->jurisdiction_id)) {
          $candidacy->jurisdiction_id = $candidacy->location_id;
          unset($candidacy->location_id);
        }
      }

      $admin_data->committee_id = !empty($admin_data->committee_id) ? $admin_data->committee_id : $registration['committee_id'];
      if ($submission->committee->pac_type === 'surplus') {
        $options = $this->_getSurplusVerificationOptions($admin_data);
      }
      else {

        $options = $this->_getCandidateVerificationOptions($submission, $admin_data);
      }

    // Determine whether we can merge a committee.
    } elseif ($submission->committee->committee_type === 'CO') {
       $options = $this->_getPacVerificationOptions($registration, $admin_data, $submission);

    } else {
      throw new Exception('Invalid committee type.');
    }

    return $options;
  }

  /**
   * @param $token
   * @return null|\stdClass
   * @throws Exception
   */
  public function getCurrentRegistrationJSONByToken($token) {
    $committee_id = $this->_committeeManager->validateAPIToken($token);
    if (!$committee_id){
      return null;
    }
    return $this->getCurrentRegistration($committee_id);
  }

  /**
   * @param $committee_id
   * @param $registration_id
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  private function _updateCommitteeRegistrationId($committee_id, $registration_id){
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    $committee->registration_id = $registration_id;
    $this->_dataManager->em->persist($committee);
    $this->_dataManager->em->flush();
  }

  /**
   * @param $registration_id
   * @param $admin
   *   This is an authorization variable to hide admin specific stuff from the public
   * @param $amend
   *   Set to true when called by amendRegistration()
   * @return \stdClass
   * @throws Exception
   */
  public function getRegistrationSubmission($registration_id, $admin = false, $amend = false){
    /** @var Registration $registration */
    $registration = $this->_dataManager->em->find(Registration::class, $registration_id);
    if (empty($registration)) {
      throw new Exception('Registration not found');
    }
    $decoder = $this->getSubmissionDecoder($registration);
    $submission = $decoder->generateSubmission($registration);
    $submission->committee->committee_id = $registration->committee_id;
    $submission->registration_id = $registration->registration_id;
    $submission->certification_email = $registration->certification_email;
    $submission->verified = $registration->verified;
    if ($registration->legacy_doc_id) {
      $submission->redirection_url = 'https://web.pdc.wa.gov/rptimg/default.aspx?docid='.$registration->legacy_doc_id;
    }
    $submission->legacy_doc_id = $registration->legacy_doc_id;
    $submission->submitted_at = $registration->submitted_at ? $registration->submitted_at->format('Y-m-d H:i:s') : null;
    $submission->updated_at = $registration->updated_at ? $registration->updated_at->format('Y-m-d H:i:s') : null;
    $submission->type='registration';

    // Load fields from committee that cannot be changed by the registration
    /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $registration->committee_id);
    $submission->committee->pac_type = $committee->pac_type;
    $submission->committee->committee_type = $committee->committee_type;
    $submission->committee->bonafide_type = $committee->bonafide_type;
    $submission->committee->exempt = $committee->exempt;
    $submission->committee->mini_full_permission = $committee->mini_full_permission;
    $submission->committee->person_id = $committee->person_id;
    $submission->committee->election_code = $committee->election_code;
    // reset continuing unless this is a single year pac
    if ($amend === true) {
      $submission->committee->continuing = $committee->continuing ?? false;
    }
    $submission->committee->filer_id = $committee->filer_id;
    $submission->committee->start_year = $committee->start_year;
    $submission->committee->end_year = $committee->end_year;
    $submission->committee->memo = $committee->memo;
    $submission->committee->category = $committee->category;

    // Load admin data
    if ($registration->admin_data) {
      $submission->admin_data = json_decode($registration->admin_data);
    }
    else {
      $submission->admin_data = new stdClass();
    }
    if (!empty($committee->mini_full_permission)) {
      $submission->admin_data->mini_full_permission = $committee->mini_full_permission;
    }
    if (!empty($committee->reporting_type)) {
      $submission->admin_data->reporting_type = $committee->reporting_type;
    }
    // If we are generating an amendment, set candidacy based on current committee information
    if ($amend && $committee->pac_type == 'candidate') {
      /** @var Candidacy $candidacy */
      unset($submission->candidacy->jurisdiction_id);
      $candidacy = $this->_dataManager->em->getRepository(Candidacy::class)
        ->findOneBy(['committee_id' => $committee->committee_id], ['sos_office_code' => 'DESC', 'candidacy_id' => 'ASC']);
      $submission->committee->candidacy->jurisdiction_id = $candidacy->jurisdiction_id;
      $submission->committee->candidacy->offcode = $candidacy->office_code;
      $submission->committee->candidacy->position_id = $candidacy->position_id;
      $submission->committee->candidacy->party_id = $candidacy->party_id;
      if ($candidacy->position_id) {
        /** @var Position $position */
        $position = $this->_dataManager->em->find(Position::class, $candidacy->position_id);
        $submission->committee->candidacy->position = $position->title ?? null;
      }
      else {
        $submission->committee->candidacy->position = null;
      }
      if ($candidacy->party_id) {
        /** @var Party $party */
        $party = $this->_dataManager->em->find(Party::class, $candidacy->party_id);
        $submission->committee->candidacy->party = $party->name ?? null;
      }
      else {
        $submission->committee->candidacy->party = null;
      }
    }

    // get partisan
    if ($submission->committee->pac_type == 'candidate' && !empty($submission->committee->candidacy)) {
      // get jurisdiction_id
      $jid = null;
      // check submitted jurisdiction_id
      $submitted_jid = $submission->committee->candidacy->jurisdiction_id ?? $submission->committee->candidacy->jurisdiction_id ??  null;
      if (!empty($submitted_jid)) {
        /** @var Jurisdiction $j */
        $j = $this->_dataManager->em->find(Jurisdiction::class, $submitted_jid);
        $jid = $j->jurisdiction_id ?? null;
        if ($j) {
          $submission->committee->candidacy->jurisdiction = $j->name;
        }
      }
      // if jurisdiction_id or location_id didn't validate, try jurisdiction
      if (!$jid) {
        /** @var Jurisdiction $j */
        $j = $this->_dataManager->em->getRepository(Jurisdiction::class)->findOneBy(['name' => $submission->committee->candidacy->jurisdiction]);
        $jid = $j->jurisdiction_id ?? null;
      }
      $submission->committee->candidacy->jurisdiction_id = $jid;
      // get offcode
      $offcode = null;
      // check submitted offcode
      if (!empty($submission->committee->candidacy->offcode)) {
        /** @var Office $o */
        $o = $this->_dataManager->em->getRepository(Office::class)->findOneBy(['offcode' => $submission->committee->candidacy->offcode]);
        $offcode = $o->offcode ?? null;
        if ($o) {
          $submission->committee->candidacy->office = $o->title;
        }

      }
      // if offcode didn't validate, try submitted office
      if (!$offcode) {
        /** @var Office $o */
        $o = $this->_dataManager->em->getRepository(Office::class)->findOneBy(['title' => $submission->committee->candidacy->office]);
        $offcode = $o->offcode ?? null;
      }

      $submission->committee->candidacy->offcode = $offcode;

      if ($jid && $offcode) {
        /** @var JurisdictionOffice $jo */
        $jo = $this->_dataManager->em->getRepository(JurisdictionOffice::class)->findOneBy(['jurisdiction_id' => $jid, 'offcode' => $offcode]);
        if (is_object($jo)) {
          $submission->committee->candidacy->partisan = $jo->partisan;
        }
      }
    }

    // Make sure there is a place to put candidacy data on CA committees.
    if ($submission->committee->pac_type == 'candidate' && !isset($submission->admin_data->candidacy)) {
      $submission->admin_data->candidacy = new stdClass();
    }

    // Make sure there is a place to put proposal data on CO committees.
    if ($submission->committee->committee_type == 'CO' && !isset($submission->admin_data->proposals)) {
      $submission->admin_data->proposals = new stdClass();
    }
    //We are calling this to add meta data for extra information
    $submission = $this->_committeeManager->metaData($submission);
    //We want to set memo to null in the case that the user is not an admin
    if (!$admin) {
      $submission->committee->memo = null;
    }

    $attachments = $this->_dataManager->em->getRepository(RegistrationAttachment::class)->findBy(['registration_id' => $registration->registration_id], ['alias' => 'asc'] );
    if(!empty($attachments)){
      foreach ($attachments as $attachment){
        $attachment->file_name = str_replace($attachment->registration_id . '/', '', $attachment->alias);
        $submission->attachments[] = $attachment;
      }
    }
    return $submission;
  }

  public function addPublicCommitteeAttachmentsToSubmission($submission){
      $committee_attachments = $this->_dataManager->em->getRepository(Attachment::class)->findBy(['target_type' => 'committee','target_id' => $submission->committee->committee_id,'access_mode' => 'public']);
      $submission->attachments = array_merge($submission->attachments??[], $committee_attachments??[]);
  }


  /**
   * @param \stdClass $submission
   * @param $certification_email
   * @param $user_name
   *
   * @return int
   * @throws Exception
   */
  public function createRegistrationFromSubmission(stdClass $submission, $certification_email, $user_name){
    //if committee is transitioning from single election to continuing remove the end year.
    if ($submission->committee->committee_type != 'CA' && $submission->committee->continuing){
      $submission->committee->end_year = null;
    }
    $registration = new Registration();
    $committee_id = $submission->committee->committee_id;
    $registration->committee_id = $committee_id;
    $registration->certification_email = $certification_email;
    if (!empty($submission->submitted_at)) {
      try {
        $newDate = new DateTime($submission->submitted_at);
        $registration->submitted_at = $newDate;
      } /** @noinspection PhpUnusedLocalVariableInspection */ catch (Exception $e) {
        throw new Exception('Invalid submission date.');
      }
    } else {
      $registration->submitted_at = new DateTime();
    }
    $registration->updated_at = new DateTime();
    $registration->source = 'campaign registration';
    $registration->version = '1.0';
    $registration->name = $submission->committee->name;
    $registration->username = $user_name;
    $registration->reporting_type = $submission->committee->reporting_type;
    $json  = json_encode($submission);
    $registration->user_data = $json;
    $this->_dataManager->em->persist($registration);
    $this->_dataManager->em->flush($registration);
    $registration_id = $registration->registration_id;
    $this->_updateCommitteeRegistrationId($committee_id, $registration_id);
    $this->sendMessage($submission);
    /** @var CommitteeRegistrationDraft $draft */
    $draft = $this->_dataManager->em->find(CommitteeRegistrationDraft::class, $committee_id);
    $this->_dataManager->em->remove($draft);
    $this->_dataManager->em->flush();
    $this->logRegistrationAction($registration_id, $user_name,'create registration','Registration created from submission for ' . $registration->name);

    if ($this->getAutoVerifyEligibility($registration_id)){
      $this->verifyRegistration($registration_id, null, null, $user_name);
    }
    return $registration_id;
  }

  /**
   * @param $submission
   * @param null $template_id
   * @throws Exception
   */
  protected function sendMessage($submission, $template_id = NULL){
    if ($this->mailer !== NULL) {
      $committee = $submission->committee;
      $this->mailer_recipients[$committee->contact->email] = $committee->contact;
      if (isset($committee->candidacy->email)) {
        $this->mailer_recipients[$committee->candidacy->email] = $committee->candidacy;
      }
      $this->mailer_recipients[$committee->books_contact->email] = $committee->books_contact;
      foreach ($committee->officers as $officer) {
        if (!isset($this->mailer_recipients[$officer->email])) {
          $this->mailer_recipients[$officer->email] = $officer;
        }
      }
      if(isset($submission->certification_email)) {
        $this->mailer_recipients[$submission->certification_email] = $submission;
      }
      /** @var MessengerContact $contact */
      $this->messenger = new WAPDCMessenger($this->_dataManager);
      foreach ($this->mailer_recipients as $key => $value) {
        $contact = $this->messenger->getContact($key);
        if (!$contact) {
          if ($template_id === NULL) {
            $template_id = 'd-37776fb5108e43618bf3af03c8b6edfd';
          }
          $this->messenger->addContact('email', $key);
          $this->messenger->sendValidationMessage($key, $this->mailer, $template_id);
        }
      }
    }
  }

  /**
   * @param $registration_id
   * @param $committee_id
   * @param $user
   * @return mixed
   * @throws Exception
   */
  public function amendRegistration($registration_id, $committee_id, $user) {
    /** @var Registration $registration */
    if ($registration_id) {
      $registration = $this->getRegistrationSubmission($registration_id, false, true);
    } else {
      $registration = $this->getCurrentRegistration($committee_id);
    }
    if (!$registration) {
      throw new Exception('The amendRegistration function could not find a registration.');
    }
    // clear admin data from draft
    if (!empty($registration->admin_data)) {
      unset($registration->admin_data);
    }
    // clear legacy document redirection
    if (!empty($registration->redirection_url)) {
      unset($registration->redirection_url);
    }
    //set party to null if registration has non-partisan office.
    if ($registration->committee->committee_type == 'CA') {
      $candidacy = $registration->committee->candidacy;
      if (!empty($candidacy->office_code) && !empty($candidacy->jurisdiction_id)) {
        $jo = $this->_dataManager->em->getRepository(JurisdictionOffice::class)->
        findOneBy(['jurisdiction_id' => $candidacy->jurisdiction_id, 'offcode' => $candidacy->office_code]);
        if ($jo && !$jo->partisan) {
          $candidacy->party = null;
        }
      }
    }
    $registration->registration_id = null;
    $registration->submitted_at = null;
    $registration->certification = null;
    $registration->certification_email = null;
    /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    if (!$committee) {
      throw new Exception('The amendRegistration function could not find a committee.');
    }
    $registration->committee->reporting_type = $committee->reporting_type;
    $this->_registrationDraftManager->saveCommitteeRegistrationDraft($registration, $user);
    return $this->_registrationDraftManager->getCommitteeRegistrationDraft($registration->committee->committee_id);
  }

  /**
   * @param $registration_id
   *   The id of registration on which to log the entry.
   * @param string $user_name
   *   User performing the action
   * @param $action
   *   The action being performed
   * @param $message
   *   Message related to the action
   * @throws Exception
   */
  public function logRegistrationAction($registration_id, $user_name, $action, $message) {
    $registration = $this->_dataManager->em->find(Registration::class, $registration_id);
    if ($registration) {
      $log = new CommitteeLog();
      $log->committee_id = $registration->committee_id;
      $log->transaction_type = 'registration';
      $log->transaction_id = $registration->registration_id;
      $log->user_name = $user_name;
      $log->action = $action;
      $log->message = $message;
      $log->updated_at = new DateTime();
      $this->_dataManager->em->persist($log);
      $this->_dataManager->em->flush($log);
    }
  }

  /**
   * @param \stdClass $submission
   * @throws Exception
   */
  public function saveAdminData(stdClass $submission) {
    /** @var Registration $registration */
    $registration = NULL;
    if (!empty($submission->registration_id)) {
      $registration = $this->_dataManager->em->find(Registration::class, $submission->registration_id);
    }
    if (!$registration) {
      throw new Exception("Registration Not Found");
    }

    $registration->admin_data = json_encode($submission->admin_data ?? new stdClass());
    $this->_dataManager->em->persist($registration);

  }

  public function setMailer(MailerInterface $m) {
    $this->mailer = $m;
    return $this;
  }

  /**
   * Revert the registration specified in the data
   *
   * @param \stdClass $data
   *   Data containing registration ID.  Potentially this will be extended to include
   *   other data such as reason for rejection.
   *
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function revertRegistration(stdClass $data, User $user) {

    if (!$data->registration_id) {
      $this->logRegistrationAction(-1, $user->user_name, 'Revert Error', 'Revert function run without registration_id.');
      throw new Exception("Registration id required for reversion.");
    }
    elseif ($data->pac_type == 'candidate' && empty($data->election_code)) {
      throw new Exception('Candidate committee requires election code.');
    }

    /** @var Registration $registration */
    $registration = $this->_dataManager->em->find(Registration::class, $data->registration_id);
    if (!$registration) {
      $this->logRegistrationAction($registration->registration_id, $user->user_name, 'Revert Error', 'Revert function found no registration for registration_id ' . $data->registration_id . '.');
      throw new Exception("Registration not found.");
    }

    # fail if registration count > 1
    $reg_count = $this->_dataManager->db->executeQuery("select count(1) from registration r where r.committee_id = :committee_id", ['committee_id' => $registration->committee_id])->fetchOne();
    if ($reg_count > 1) {
      $this->logRegistrationAction($registration->registration_id, $user->user_name, 'Revert Error', 'Revert function found no registration for registration_id ' . $data->registration_id . '.');
      throw new Exception("Multiple registrations found. Revert currently is limited to committees with only one registration.");
    }

    try {
      $this->_dataManager->em->beginTransaction();

      $cid = $registration->committee_id;
      $this->logRegistrationAction($registration->registration_id, $user->user_name, 'Registration Revert', 'Registration revert run by PDC staff member.');
      $this->amendRegistration($data->registration_id, $cid, $user);
      $this->_dataManager->em->remove($registration);
      $this->_dataManager->em->flush($registration);

      /** @var Committee $committee */
      $committee = $this->_dataManager->em->find(Committee::class, $cid);

      $committee->registration_id = null;
      $committee->start_year = date('Y');

      if (!empty($data->pac_type)) switch ($data->pac_type) {
        case 'candidate':
          $committee->pac_type = 'candidate';
          $committee->committee_type = 'CA';
          $committee->exempt = null;
          $committee->bonafide_type = null;
          $committee->continuing = false;
          $committee->party = null;
          if ($data->election_code) {
            $committee->election_code = $data->election_code;
            $committee->start_year = substr($data->election_code, 0, 4);
            $committee->end_year = $committee->start_year;
          }
          else {
            throw new Exception('Candidate committee requires election code.');
          }
          break;
        case 'surplus':
          $committee->pac_type = 'surplus';
          $committee->committee_type = 'CA';
          $committee->exempt = null;
          $committee->bonafide_type = null;
          $committee->continuing = false;
          $committee->election_code = null;
          $committee->end_year = null;
          $committee->party = null;
          break;
        case 'bonafide':
          $committee->pac_type = 'bonafide';
          $committee->committee_type = 'CO';
          $committee->exempt = !empty($data->exempt) ? true : false;
          $committee->continuing = true;
          $committee->election_code = null;
          $committee->end_year = null;
          if (in_array($data->bonafide_type, ['state', 'county', 'district'])) {
            $committee->bonafide_type = $data->bonafide_type;
          }
          else {
            throw new Exception("Expected bonafide type of 'state', 'county', 'district'.");
          }
          break;
        case 'caucus':
          $committee->pac_type = 'caucus';
          $committee->committee_type = 'CO';
          $committee->exempt = !empty($data->exempt) ? true : false;
          $committee->bonafide_type = null;
          $committee->continuing = true;
          $committee->election_code = null;
          $committee->end_year = null;
          break;
        case 'pac':
          $committee->pac_type = 'pac';
          $committee->committee_type = 'CO';
          $committee->exempt = null;
          $committee->bonafide_type = null;
          $committee->party = null;
          if ($data->election_code) {
            $committee->election_code = $data->election_code;
            $committee->start_year = substr($data->election_code, 0, 4);
            $committee->end_year = $committee->start_year;
            // single-year may be continuing (if it amended from single-year)
            $committee->continuing = ($data->continuing) ? true : false;
          }
          else {
            $committee->continuing = true;
            $committee->election_code = null;
            $committee->end_year = null;
          }
          break;
        default:
          throw new Exception('Invalid PAC Type');
      }

      // Make sure the committee changes are reflected on the draft.
      $draft = $this->_registrationDraftManager->getCommitteeRegistrationDraft($committee->committee_id);
      $draft->committee->election_code = $committee->election_code;
      $draft->committee->start_year = $committee->start_year;
      $draft->committee->end_year = $committee->end_year;
      $draft->committee->continuing = $committee->continuing;
      $draft->committee->pac_type = $committee->pac_type;
      $draft->committee->bonafide_type = $committee->bonafide_type;
      $draft->committee->exempt = $committee->exempt;
      $draft->committee->committee_type = $committee->committee_type;
      $draft->committee->party_id = $committee->party;

      // candidate/surplus specific data corrections
      if ($committee->pac_type == 'candidate' or $committee->pac_type == 'surplus') {
        $draft->committee->has_sponsor = '';
        $draft->committee->sponsor = null;
        $draft->committee->affiliations_declared = '';
        unset($draft->committee->proposals);
        unset($draft->committee->affiliations);
        unset($draft->committee->ticket);
        unset($draft->committee->candidates);
        if ($committee->pac_type == 'candidate' && !isset($draft->committee->candidacy)) {
          $draft->committee->candidacy = new stdClass;
        }
      }
      if ($committee->pac_type != 'candidate') {
        unset($draft->committee->candidacy);
      }
      $this->_registrationDraftManager->saveCommitteeRegistrationDraft($draft, $user);

      $this->_dataManager->em->flush($committee);
      $this->_dataManager->em->commit();
    } catch(Exception $e) {
      $this->_dataManager->em->rollback();
      throw $e;

    }
  }

  /**
   * @param $input_data
   *
   * @throws Exception
   */
  public function addRegistrationAttachment(stdClass $input_data){
    $alias = $input_data->registration_id . '/' . trim($input_data->file_name);
    $file_service_url = $input_data->file_service_url;
    $registration_id = $input_data->registration_id;
    $access_mode = $input_data->access_mode ?? 'public';
    $description = $input_data->description ?? '';
    $document_type = $input_data->document_type ?? '';

    if($this->_fileAlreadyExists($input_data->registration_id, $input_data->file_name)){
      throw new Exception("File with the same name has already been uploaded to this registration.");
    }else{
      $attachment = new RegistrationAttachment(
        $alias,
        $file_service_url,
        $registration_id,
        trim($access_mode)
      );

      $attachment->description = trim($description);
      $attachment->document_type = trim($document_type);


      $this->_dataManager->em->persist($attachment);
      $this->_dataManager->em->flush();
    }
  }

  /**
   * @param $input_data
   *
   * @throws Exception
   */
  public function modifyRegistrationAttachment(stdClass $input_data){
    $alias = $input_data->registration_id . '/' . $input_data->file_name;
    /** @var RegistrationAttachment $attachment */
    $attachment = $this->_dataManager->em->find(RegistrationAttachment::class, $alias);

    $attachment->description = $input_data->description;
    $attachment->document_type = $input_data->document_type;

    if($attachment->access_mode !== $input_data->access_mode){
      if($this->_fileService != null){
        $new_url = $this->_fileService->setFileAccess($attachment->file_service_url, $input_data->access_mode);
        $attachment->setAccessMode($input_data->access_mode, $new_url);
      }else{
        throw new Exception("File service not set in registration manager.");
      }
    }

    $this->_dataManager->em->persist($attachment);
    $this->_dataManager->em->flush();
  }

  /**
   * @param $input_data
   *
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function deleteRegistrationAttachment(stdClass $input_data) {
    $alias = $input_data->registration_id . '/' . $input_data->file_name;
    $attachment = $this->_dataManager->em->find(RegistrationAttachment::class, $alias);
    if ($attachment->file_service_url) {
      $this->_fileService->deleteFile($attachment->file_service_url);
      $this->_dataManager->em->remove($attachment);
      $this->_dataManager->em->flush();
    }
  }

  /**
   * Find out if $filename exists for a given registration.
   *
   * @param $registration_id
   * @param $filename
   *
   * @return bool
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  private function _fileAlreadyExists($registration_id, $filename) {
    $alias = $registration_id . '/' . $filename;
    return (bool) $this->_dataManager->em->find(RegistrationAttachment::class, $alias);
  }

  protected function _setExemptStatus($data_exempt) {
    if (!empty($data_exempt)) {
      $exempt = TRUE;
    } else {
      $exempt = FALSE;
    }

    return $exempt;
  }



 }