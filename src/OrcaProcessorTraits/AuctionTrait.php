<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait AuctionTrait {
  /**
   * @param integer $fund_id
   * @param stdClass $auction
   * @return stdClass
   * @throws \Exception
   */
  public function saveAuction(int $fund_id, stdClass $auction): stdClass{
    //validate data
    if (empty($auction->name)) {
      throw new Exception('Auction name required');
    }
    if (empty($auction->date_)) {
      throw new Exception('Auction date required');
    }
    //use contact field validation to verify values that will create contact
    $this->fieldValidationsForContacts($auction, 'HDN');
    $auctionId = $this->_dataManager->db->fetchOne("select private.cf_save_auction(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($auction)]);
    $savedAuction = $this->_dataManager->db->fetchOne("select cast(private.cf_get_fundraiser(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $auctionId]);
    $response = new stdClass();
    $response->success = !empty($savedAuction);
    if ($response->success) {
      $response->auction = json_decode($savedAuction);
    }
    return $response;
  }

  /***
   * @param int $fund_id
   * @param int $auction_id
   * @throws Exception
   */
  public function deleteAuction(int $fund_id, int $auction_id) : void {
    $this->_dataManager->db->executeStatement(
      "
       select private.cf_delete_auction(t.trankeygen_id)
       from private.trankeygen t where t.fund_id = :fund_id and trankeygen_id=:id
      ",
      ['fund_id' => $fund_id, 'id'=> $auction_id]
    );
  }

  /**
   * Retrieve an auction with all auction items.
   * @param int $fund_id
   * @param int $id
   * @return stdClass
   * @throws Exception
   */
  public function getAuctionDetails($fund_id, $id): stdClass {
    $data = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_get_auction_details(:fund_id, :id)",
      ['fund_id' => $fund_id, 'id' => $id]
    );
    $response = new stdClass();
    $response->success = false;
    if ($data) {
      $response->{'auction-details'} = json_decode($data);
      $response->success = true;
    }
    return $response;
  }

  /**
   * @throws \Exception
   */
  public function saveAuctionItem($fund_id, $auctionItem): stdClass {
    $response = new stdClass();
    if(empty($auctionItem->itemnumber)){
      throw new Exception("Missing item number");
    }
    if(empty($auctionItem->itemdescription)){
      throw new Exception("Missing item description");
    }
    $this->validatePositiveAmount($auctionItem->marketval);

    $auctionItemId = $this->_dataManager->db->fetchOne("SELECT private.cf_save_auction_item(:fund_id, CAST(:json AS JSON))", ['fund_id' => $fund_id, 'json' => json_encode($auctionItem)]);

    $data =  $this->_dataManager->db->fetchOne("SELECT private.cf_get_auction_item(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $auctionItemId]);
    if ($data) {
      $response->{'auction-item'} = json_decode($data);
      $response->success = true;
    }

    return $response;
  }

  /**
   * @throws Exception
   */
  public function getAuctionItem($fund_id, $auction_item_id): stdClass{
    $data = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_get_auction_item(:fund_id, :id)",
      ['fund_id' => $fund_id, 'id' => $auction_item_id]
    );
    $response = new stdClass();
    $response->success = false;
    if ($data) {
      $response->{'auction-item'} = json_decode($data);
      $response->success = true;
    }
    return $response;
  }

  /**
   * @throws Exception
   */
  public function deleteAuctionItem($fund_id, $id): stdClass
  {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;
    $this->_dataManager->db->executeStatement('select private.cf_delete_auction_item(:fund_id, :id)', ['fund_id' => $fund_id, 'id' => $id]);

    return $response;
  }
}
