<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait CreditCardTrait {
  /**
   * @throws \Exception
   */
  public function saveCreditCardPayment($fund_id, $payment): stdClass
  {
    //do validations
    $this->validatePositiveAmount($payment->amount);
    if (empty($payment->date_)) {
      throw new Exception('Credit card payment date required');
    }
    if (empty($payment->pid)) {
      throw new Exception("Missing credit card");
    }
    if (empty($payment->did)) {
      throw new Exception("Missing payment account");
    }

    $paymentId = $this->_dataManager->db->fetchOne("SELECT private.cf_save_credit_card_payment(:fund_id, CAST(:json AS JSON))", ['fund_id' => $fund_id, 'json' => json_encode($payment)]);
    $data =  $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $paymentId]);

    $response = new stdClass();
    $response->success = false;
    if ($data) {
      $response->{'credit-card-payment'} = json_decode($data);
      $response->success = true;
    }
    return $response;
  }

  /**
   * Retrieve a credit card
   * @param $fund_id
   * @param $trankeygen_id
   * @return false|stdClass|string
   * @throws \Doctrine\DBAL\Exception
   */
  public function getCreditCard($fund_id, $trankeygen_id) {
    $response = new stdClass();
    $data = $this->_dataManager->db->fetchOne("select private.cf_get_credit_card(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]);
    $response->{'credit-card'} = $data ? json_decode($data) : new stdClass();
    $response->success = !empty($response->{'credit-card'}->trankeygen_id);
    return $response;
  }

  /**
   * Retrieve a credit card payment
   * @param $fund_id
   * @param $trankeygen_id
   * @return false|stdClass|string
   * @throws \Doctrine\DBAL\Exception
   */
  public function getCreditCardPayment($fund_id, $trankeygen_id) {
    $response = new stdClass();
    $data =  $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]);
    $response->{'credit-card-payment'} = $data ? json_decode($data) : new stdClass();
    $response->success = !empty($response->{'credit-card-payment'}->trankeygen_id);
    return $response;
  }

  /**
   * @throws Exception
   */
  public function getCreditCardDetails($fund_id, $account_id):stdClass {
    $data = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_get_credit_card_details(:fund_id, :id)",
      ['fund_id' => $fund_id, 'id' => $account_id]
    );
    $response = new stdClass();
    if ($data) {
      $response = json_decode($data);
      $response->success = true;
    }
    else {
      $response->success = false;
    }
    return $response;
  }
}
