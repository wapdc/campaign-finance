<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait FundraiserTrait {
  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function saveCashFundraiser($fund_id, $fundraiser): stdClass{
    //validate data
    if (empty($fundraiser->name)) {
      throw new Exception('Fundraiser name required');
    }
    //use contact field validation to verify values that will create contact
    $this->fieldValidationsForContacts($fundraiser, 'HDN');
    $savedCashFundraiserId = $this->_dataManager->db->executeQuery("select private.cf_save_cash_fundraiser(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($fundraiser)])->fetchOne();
    $savedCashFundraiser = $this->_dataManager->db->executeQuery("select cast(private.cf_get_fundraiser(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedCashFundraiserId])->fetchOne();
    $response = new stdClass();
    $response->success = !empty($savedCashFundraiser);
    if ($response->success) {
      $response->{'cash-fundraiser'} = json_decode($savedCashFundraiser);
    }
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function saveLowCostFundraiser($fund_id, $fundraiser): stdClass
  {
    //validate data
    if(empty($fundraiser->date_)) {
      throw new Exception('Fundraiser date required');
    }
    if (empty($fundraiser->name)) {
      throw new Exception('Fundraiser name required');
    }
    if (!isset($fundraiser->amount)) {
      throw new Exception('Amount not set');
    }
    else {
      if ($fundraiser->amount < 0) {
        throw new Exception('Amount must not be a negative number');
      }
      if (!is_numeric($fundraiser->amount)) {
        throw new Exception('Amount must be a number');
      }
    }
    //use contact field validation to verify values that will create contact
    $this->fieldValidationsForContacts($fundraiser, 'HDN');
    $savedFundraiserId = $this->_dataManager->db->executeQuery("select private.cf_save_low_cost_fundraiser(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($fundraiser)])->fetchOne();
    $savedFundraiser = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedFundraiserId])->fetchOne();
    $response = new stdClass();
    $response->success = !empty($savedFundraiser);
    if ($response->success) {
      $response->{'low-cost-fundraiser'} = json_decode($savedFundraiser);
    }
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function loadFundraiser($fund_id, $id){
    $response = new stdClass();
    $fundraiser = $this->_dataManager->db->executeQuery("select cast(private.cf_get_fundraiser(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $id])->fetchOne();
    if (!$fundraiser) {
      throw new Exception('Fundraiser not found');
    }
    $response->{'cash-fundraiser'} = json_decode($fundraiser);
    $response->success = TRUE;
    return $response;
  }

  /**
   * Retrieve a fundraiser and all its detail transactions.
   * @param $fund_id
   * @param $trankeygen_id
   * @return false|stdClass|string
   * @throws Exception
   */
  public function getFundraiserDetails($fund_id, $trankeygen_id) {
    $data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_fundraiser_details(:fund_id, :trankeygen_id )", ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]);
    $response = $data ? json_decode($data) : new stdClass();
    $response->success = !empty($response->fundraiser->trankeygen_id);
    return $response;
  }

  /**
   * @throws Exception
   */
  public function deleteCashFundraiser($fund_id, $id): stdClass
  {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    //delete fundraiser in account table
    $this->_dataManager->db->executeStatement('select private.cf_delete_cash_fundraiser(:id)', ['id' => $id]);
    return $response;
  }

  /**
   * Removes a low-cost fundraiser record and the account with it
   * @param $fund_id
   *   The id of the fund so that we ensure we don't delete across funds
   * @param $id
   *   trankeygen_id for the receipt record for the low-cost fundraiser fund
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function deleteLowCostFundraiser($fund_id, $id): stdClass
  {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    $fundraiser = json_decode($this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $id])->fetchOne());
    if(empty($fundraiser)){
      throw new Exception('Fundraiser not found.');
    }
    if($fundraiser->deposit_date){
      throw new Exception('Can not delete fundraiser that has been deposited.');
    }
    //delete fundraiser in receipt table
    $this->_dataManager->db->executeStatement('select private.cf_delete_low_cost_fundraiser(:id)', ['id' => $id]);
    return $response;
  }
}
