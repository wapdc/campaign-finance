<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait LoanTrait {
  /**
   * @throws Exception
   */
  public function saveMonetaryLoan($fund_id, stdClass $loan) {
    $response = new stdClass();
    $response->errors = null;
    if (empty($loan->contid)) {
      throw new Exception('Loan contact required');
    }
    if (empty($loan->date_)) {
      throw new Exception('Date not set');
    }
    if ($loan->carryforward === 1) {
      if (isset($loan->data) === false) {
        throw new Exception('Data is not set while carryforward is 1');
      }
      if (isset($loan->data->committee_name) === false) {
        throw new Exception('Committee name within loan data is not set while carryforward is 1');
      }
      if (isset($loan->data->election_year) === false) {
        throw new Exception('Election year within loan data is not set while carryforward is 1');
      }
    }
    if ($loan->carryforward === 0 && $loan->data !== null) {
      throw new Exception('Data is set while carryforward is 0');
    }
    if (empty($loan->amount)) {
      throw new Exception('Amount not set');
    }
    else {
      if ($loan->amount < 0) {
        throw new Exception('Amount must not be a negative number');
      }
      if (!is_numeric($loan->amount)) {
        throw new Exception('Amount must be a number');
      }
    }

    $trankeygen_id = $this->_dataManager->db->fetchOne("SELECT private.cf_save_monetary_loan(:fund_id, CAST(:json as JSON))", ['fund_id' => $fund_id, 'json' => json_encode($loan)]);

    $loan_data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_loan(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]);
    $response->loan = json_decode($loan_data);

    if(!empty($response->loan)){
      $response->success = true;
    }
    return $response;
  }

  /**
   * @throws Exception
   */
  public function saveInKindLoan($fund_id, stdClass $loan) {
    $response = new stdClass();
    $response->errors = null;
    if (empty($loan->cid)) {
      throw  new Exception('Missing expenditure account');
    }
    if (empty($loan->contid)) {
      throw new Exception('Loan contact required');
    }
    if (empty($loan->date_)) {
      throw new Exception('Date not set');
    }
    if ($loan->carryforward === 1) {
      if (isset($loan->data) === false) {
        throw new Exception('Data is not set while carryforward is 1');
      }
      if (isset($loan->data->committee_name) === false) {
        throw new Exception('Committee name within loan data is not set while carryforward is 1');
      }
      if (isset($loan->data->election_year) === false) {
        throw new Exception('Election year within loan data is not set while carryforward is 1');
      }
    }
    if ($loan->carryforward === 0 && $loan->data !== null) {
      throw new Exception('Data is set while carryforward is 0');
    }
    if (empty($loan->amount)) {
      throw new Exception('Amount not set');
    }
    if (empty($loan->description)) {
      throw new Exception('Description required');
    }
    else {
      if ($loan->amount < 0) {
        throw new Exception('Amount must not be a negative number');
      }
      if (!is_numeric($loan->amount)) {
        throw new Exception('Amount must be a number');
      }
    }

    $trankeygen_id = $this->_dataManager->db->fetchOne("SELECT private.cf_save_in_kind_loan(:fund_id, CAST(:json as JSON))", ['fund_id' => $fund_id, 'json' => json_encode($loan)]);

    $loan_data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_loan(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]);
    $response->loan = json_decode($loan_data);

    if(!empty($response->loan)){
      $response->success = true;
    }
    return $response;
  }

  /**
   * @throws Exception
   */
  public function saveLoanForgiveness($fund_id, stdClass $forgiveness){
    $response = new stdClass();
    $response->success = false;

    if(empty($forgiveness->amount)){
      throw new Exception("Missing amount on loan forgiveness");
    }
    if(empty($forgiveness->date_)){
      throw new Exception("Missing date on loan forgiveness");
    }

    $forgiven_id = $this->_dataManager->db->fetchOne("SELECT private.cf_save_loan_forgiveness(:fund_id, CAST(:json as JSON))", ['fund_id' => $fund_id, 'json' => json_encode($forgiveness)]);
    $data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $forgiven_id]);

    if($data) {
      $response->{'loan-forgiveness'} = json_decode($data);
      $response->success = true;
    }

    return $response;
  }

  /**
   * @throws Exception
   */
  public function saveLoanPayment($fund_id, stdClass $payment) {
    $response = new stdClass();
    $response->success = false;

    if (empty($payment->amount) && empty($payment->interest)) {
      throw new Exception("Missing amount or interest of loan payment");
    }
    if (empty($payment->pid)) {
      throw new Exception("Missing loan transaction");
    }
    if (empty($payment->date_)) {
      throw new Exception("Missing payment date");
    }

    $payment->amount ??= 0;
    $payment->interest ??= 0;

    $payment_id = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_save_loan_payment(:fund_id, CAST(:json AS JSON))", ['fund_id' => $fund_id, 'json' => json_encode($payment)]
    );

    $data = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $payment_id]
    );

    if ($data) {
      $response->{'loan-payment'} = json_decode($data);
      $response->success = true;
    }

    return $response;
  }

  /**
   * @throws Exception
   */
  public function saveLoanEndorser($fund_id, $endorser): stdClass
  {
    $response = new stdClass();
    $response->success = false;

    if (empty($endorser->amount)) {
      throw new Exception("Missing amount for endorsers");
    }
    if (empty($endorser->contid)) {
      throw new Exception("Missing contact for endorser");
    }

    $endorser_trankeygen_id = $this->_dataManager->db->fetchOne("SELECT private.cf_save_loan_endorser(:fund_id, :json)", ['fund_id' => $fund_id, 'json' => json_encode($endorser)]);
    $endorser_results = $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $endorser_trankeygen_id]);

    if ($endorser_results) {
      $response->{'loan-endorser'} = json_decode($endorser_results);
      $response->success = true;
    }
    return $response;
  }

  /**
   * @throws Exception
   */
  public function getLoan($fund_id, $trankeygen_id) {
    $response = new stdClass();
    $json = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_get_loan(:fund_id, :trankeygen_id)",
      ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]
    );
    $response->success = true;
    $response->loan = json_decode($json);
    return $response;
  }

  /**
   * Retrieve a loan and all its detail transactions.
   * @param $fund_id
   * @param $trankeygen_id
   * @return false|stdClass|string
   * @throws Exception
   */
  public function getLoanDetails($fund_id, $trankeygen_id) {
    $data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_loan_details(:fund_id, :trankeygen_id )", ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]);
    $response = $data ? json_decode($data) : new stdClass();
    $response->success = !empty($response->loan->trankeygen_id);
    return $response;
  }

  /**
   * @throws Exception
   */
  public function deleteLoan($fund_id, $loan_id): void {
    $this->_dataManager->db->executeStatement(
      "
       select private.cf_delete_loan(t.trankeygen_id)
       from private.loans l join private.trankeygen t on t.trankeygen_id=l.trankeygen_id
       where fund_id=:fund_id and l.trankeygen_id=:id
      ",
      ['fund_id' => $fund_id, 'id' => $loan_id]
    );
  }
}
