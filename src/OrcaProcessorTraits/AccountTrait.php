<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait AccountTrait {
  /**
   * @param integer $fund_id
   *   The fund in which to save the acccount. Used in update to make sure we cannot save accross accounts
   * @param stdClass $account
   *   Object representation of the combination of an account and a contact.  The contact information is used to store
   *   address information of things like credit cards and bank info.
   *
   * @return stdClass
   *   Returns a response object indicating whether the save is successful, and a representation fo the account as the
   *   account property.
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function saveAccount($fund_id, $account) {
    if (empty($account->acctnum) || ($account->acctnum !== 2600 && $account->acctnum !== 1000)) {
      throw new Exception('Missing or invalid acctnum');
    }
    if (empty($account->name)) {
      throw new Exception('Missing name of account');
    }

    $savedAccountId = $this->_dataManager->db->executeQuery("select private.cf_save_account(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($account)])->fetchOne();
    $savedAccount = $this->_dataManager->db->fetchOne("select private.cf_get_account(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $savedAccountId]);
    $response = new stdClass();
    $response->success = true;
    $response->account = json_decode($savedAccount);
    return $response;
  }

  /**
   * Retrieve an individual account from the database.
   *
   * @param integer $fund_id
   *   Fund in which the account resides, ensures that we don't give out data across funds.
   * @param integer $account_id
   *   ID of the account to retrieve
   * @return stdClass
   *   The representation of the private.vaccounts view of this data.
   * @throws Exception
   */
  public function getAccount($fund_id, $account_id) {

    $account = $this->_dataManager->db->fetchOne("SELECT private.cf_get_account(:fund_id, :account_id)", ['fund_id' => $fund_id, 'account_id' => $account_id]);
    return $account  ? json_decode($account) : null;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function saveAccountTransfer($fund_id, stdClass $transfer) {
    //validate data
    if(!isset($transfer->cid)){
      throw new Exception('Receiving account required');
    }
    if(!isset($transfer->did)){
      throw new Exception('Contributing account required');
    }
    if (!isset($transfer->amount)) {
      throw new Exception('Amount not set');
    }
    if (!isset($transfer->date_)) {
      throw new Exception('Date not set');
    }
    $savedTransferId = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_save_account_transfer(fund_id, CAST(:json_data as JSON)) FROM fund where fund_id=:fund_id",
      ['fund_id' => $fund_id, 'json_data' => json_encode($transfer)]
    );
    $savedTransfer = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedTransferId])->fetchOne();
    $response = new stdClass();
    $response->success = !empty($savedTransfer);
    if ($response->success) {
      $response->{'account-transfer'} = json_decode($savedTransfer);
    }
    return $response;
  }

  /**
   * Retrieve configuration information for an account group.
   *
   * @param integer $acctnum
   *   Account number of the group to retrieve
   * @return stdClass
   *   The returns the representation of the account_groups entry in the database.
   * @throws Exception
   */
  public function getAccountGroup($acctnum) {
    $group = $this->_dataManager->db->fetchOne("SELECT row_to_json(g) from private.account_groups g where acctnum=:acctnum", ['acctnum' => $acctnum]);
    return $group ? json_decode($group) : null;
  }
}
