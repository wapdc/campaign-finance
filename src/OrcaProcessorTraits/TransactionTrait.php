<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait TransactionTrait {
  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function loadTransaction($fund_id, $id, $type){
    $response = new stdClass();
    $transaction = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $id])->fetchOne();
    if(!$transaction && $type == 'low-cost-fundraiser') {
      throw new Exception('Fundraiser not found');
    } elseif(!$transaction && $type == 'loan-forgiveness'){
      throw new Exception('Loan forgiveness not found');
    } elseif(!$transaction && $type == 'vendor-debt-forgiveness'){
      throw new Exception('Vendor debt forgiveness not found');
    } elseif(!$transaction && $type == 'vendor-refund'){
      throw new Exception('Vendor refund not found');
    } elseif(!$transaction && $type == 'pledge-cancel'){
      throw new Exception('Pledge Cancel not found');
    } elseif (!$transaction) {
      throw new Exception('Contribution not found');
    }
    $response->{$type} = json_decode($transaction);
    $response->success = TRUE;
    return $response;
  }

  /**
   * @param $fund_id
   * @return stdClass
   * @throws Exception|\Doctrine\DBAL\Driver\Exception
   */
  public function getDeletedTransactionLogs($fund_id) {
    $response = new stdClass();
    $logs = $this->_dataManager->db->executeQuery("select memo, date, data from private.log where fund_id = :fund_id and memo ilike 'deleted records%' order by date desc", ['fund_id' => $fund_id])->fetchAllAssociative();
    $response->success = true;
    $response->logs = $logs;
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function deleteTransaction($fund_id, $transaction_id){
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    $transaction = json_decode($this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $transaction_id])->fetchOne());
    if(empty($transaction)){
      throw new Exception('Transaction not found.');
    }
    if($transaction->deposit_date){
      throw new Exception('Can not delete transaction that has been deposited.');
    }
    //delete contribution in receipt table
    $this->_dataManager->db->executeStatement('select private.cf_delete_receipt(:id)', ['id' => $transaction_id]);
    return $response;
  }
}
