<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait ContributionTrait {
  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function saveMonetaryContribution($fund_id, $contribution) {
    $this->fieldValidationsForContributions($contribution, 'monetary-contribution');
    $savedContributionId = $this->_dataManager->db->executeQuery("select private.cf_save_contribution(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($contribution)])->fetchOne();
    $savedContribution = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedContributionId])->fetchOne();
    $response = new stdClass();

    $response->success = !empty($savedContribution);
    if($response->success) {
      $response->{'monetary-contribution'} = json_decode($savedContribution);
    }

    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function saveInKindContribution($fund_id, $inkindContribution) {
    $this->fieldValidationsForContributions($inkindContribution, "inkind");
    $savedContributionId = $this->_dataManager->db->executeQuery("select private.cf_save_inkind_contribution(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($inkindContribution)])->fetchOne();
    $savedContribution = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedContributionId])->fetchOne();
    $response = new stdClass();
    $response->success = true;
    $response->{'in-kind-contribution'} = json_decode($savedContribution);
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function saveAnonymousContribution($fund_id, $anonymousContribution) {
    $this->fieldValidationsForContributions($anonymousContribution, "anonymous");
    $savedContributionId = $this->_dataManager->db->executeQuery("select private.cf_save_anonymous_contribution(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($anonymousContribution)])->fetchOne();
    $savedContribution = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedContributionId])->fetchOne();
    $response = new stdClass();
    $response->success = true;
    $response->{'anonymous-contribution'} = json_decode($savedContribution);
    return $response;
  }

  /**
   * @throws Exception
   */
  public function saveFundraiserContribution($fund_id, $contribution):stdClass {
    $response = new stdClass();
    if (empty($contribution->amount)) {
      throw new Exception("Missing amount of contribution");
    }
    if (empty($contribution->pid)) {
      throw new Exception("Missing fundraiser event");
    }
    if (empty($contribution->date_)) {
      throw new Exception("Missing contribution date");
    }
    $contribution_id = $this->_dataManager->db->fetchOne("SELECT private.cf_save_fundraiser_contribution(:fund_id, CAST(:json AS JSON))", ['fund_id' => $fund_id, 'json' => json_encode($contribution)]);
    $data =  $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $contribution_id]);
    if ($data) {
      $response->{'fundraiser-contribution'} = json_decode($data);
      $response->success = true;
    }
    return $response;
  }

  /**
   * @throws Exception
   */
  public function saveMonetaryGroupContribution($fund_id, $payload) {
    $this->_dataManager->db->executeStatement('select private.cf_save_monetary_group_contribution(:fund_id, :json)', [
      'fund_id' => $fund_id,
      'json' => json_encode($payload)
    ]);
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function getMonetaryGroupContribution($fund_id, $id) {
    $response = new stdClass();
    $json = $this->_dataManager->db->executeQuery('select private.cf_get_monetary_group_contribution(:fund_id, :trankeygen_id)', [
      'fund_id' => $fund_id,
      'trankeygen_id' => $id
    ])->fetchOne();

    $response->success = true;
    $response->{'get-monetary-group-contribution'} = json_decode($json);
    return $response;
  }

  /**
   * Import/save contribution records.
   * Records should include both information related to a contact as well as information
   * for the expenditure amount.
   *
   * @param $fund_id
   *   Indicates fund to save contributions to
   * @param array $contributions
   *   Array containing records that represent a contact with its contribution
   * @return stdClass
   *   Data success and/or standard class.
   * @throws Exception
   */
  public function importContributions(int $fund_id, array $contributions): stdClass {
    $return_data = $this->_dataManager->db->fetchOne(
      "select private.cf_save_contributions(:fund_id, cast(:json_data as json))",
      ['fund_id' => $fund_id, 'json_data' => json_encode($contributions)]
    );
    return json_decode($return_data);
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function saveContributionRefund($fund_id, $jsonData) {
    $contributionRefund = json_decode($jsonData);
    $this->fieldValidationsForContributions($contributionRefund, "refund");
    $savedRefundId = $this->_dataManager->db->executeQuery("select private.cf_save_contribution_refund(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => $jsonData])->fetchOne();
    $savedRefundContribution = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedRefundId])->fetchOne();
    $response = new stdClass();
    $response->success = true;
    $response->{'refund-contribution'} = json_decode($savedRefundContribution);
    return $response;
  }

  /**
   * Create a contribution that is split between the primary and the general.
   * @param $fund_id
   * @param $contribution
   * @return stdClass
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function addSplitContribution($fund_id, $contribution):stdClass {
    $this->fieldValidationsForContributions($contribution, 'split-monetary-contribution');
    $savedContributions = $this->_dataManager->db->executeQuery('select private.cf_save_split_contribution(:fund_id, :json_data)', ["fund_id" => $fund_id, 'json_data' => json_encode($contribution)])->fetchOne();
    $response = new stdClass();
    $response->success = !empty($savedContributions);
    if ($response->success) {
      $response->contributions = json_decode($savedContributions);
    }
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function deleteContribution($fund_id, $id){
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    $contribution = json_decode($this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $id])->fetchOne());
    if(empty($contribution)){
      throw new Exception('Contribution not found.');
    }
    if($contribution->deposit_date){
      throw new Exception('Can not delete contribution that has been deposited.');
    }
    //delete contribution in receipt table
    $this->_dataManager->db->executeStatement('select private.cf_delete_receipt(:id)', ['id' => $id]);
    return $response;
  }

  /**
   * @throws Exception
   */
  public function fieldValidationsForContributions($contribution, $type = null) {
    if (!isset($contribution->contid)  && $type !== 'anonymous') {
      throw new Exception('Contributor name required');
    }
    if (!isset($contribution->elekey) && $type !== 'anonymous'
      && $type !== 'split-monetary-contribution'
      && $type !== 'refund'
      && (($type == 'monetary-contribution' || $type == 'in-kind-contribution') && !$contribution->pid)
    ) {
      throw new Exception('Election selection required');
    }
    if (!isset($contribution->date_)) {
      throw new Exception('Contribution date required');
    }

    // Amount validations
    if ($type === 'split-monetary-contribution') {
      if (!isset($contribution->primary_amount)) {
        throw new Exception('Primary amount not set');
      }
      else {
        if ($contribution->primary_amount < 0) {
          throw new Exception('Primary contribution amount must not be a negative number');
        }
        if (!is_numeric($contribution->primary_amount)) {
          throw new Exception('Primary contribution amount must be a number');
        }
      }
      if (!isset($contribution->general_amount)) {
        throw new Exception('General amount not set');
      }
      else {
        if ($contribution->general_amount < 0) {
          throw new Exception('General contribution amount must not be a negative number');
        }
        if (!is_numeric($contribution->general_amount)) {
          throw new Exception('General contribution amount must be a number');
        }
      }
    }
    else {
      if (!isset($contribution->amount)) {
        throw new Exception('Amount not set');
      }
      else {
        if ($contribution->amount < 0) {
          throw new Exception('Contribution amount must not be a negative number');
        }
        if (!is_numeric($contribution->amount)) {
          throw new Exception('Contribution amount must be a number');
        }
      }
    }

    if (!isset($contribution->cid)  && $type === 'inkind') {
      throw new Exception('Account not selected');
    }
    if (!isset($contribution->description)  && $type === 'inkind') {
      throw new Exception('Description not set');
    }
    if (!isset($contribution->pid) && $type === 'refund') {
      throw new Exception('No contribution selected to refund');
    }
    if (strlen($contribution->description ?? "") > 255 && $type === 'inkind'){
      throw new Exception('Description is too long');
    }
    if (isset($contribution->memo) && strlen($contribution->memo) > 1000) {
      throw new Exception('Memo must not exceed 1000 characters');
    }
    if (isset($contribution->checkno) && strlen($contribution->checkno) > 25) {
      throw new Exception('Memo must not exceed 25 characters');
    }
    if (isset($contribution->elekey) && $type !== 'refund' && !in_array($contribution->elekey, range(-1,1))) {
      throw new Exception('Invalid election selected');
    }
  }
}
