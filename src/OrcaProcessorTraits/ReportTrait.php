<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use DateTime;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\ORMException;
use stdClass;
use WAPDC\CampaignFinance\CampaignFinance;

trait ReportTrait {
  /**
   * @throws ConnectionException
   * @throws ORMException
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  protected function submitReport($fund_id, $reportData, $submissionData, $ip_address) {
    $financeReportingProcessor = CampaignFinance::service()->getFinanceReportingProcessor();
    [$committee_id] = $this->_dataManager->db->executeQuery("SELECT committee_id from public.fund where fund_id = :fund_id", ['fund_id' => $fund_id])->fetchFirstColumn();

    // Get notification emails for report submission.
    $emails = [];
    if (!empty($submissionData->committee_notification_email)) {
      $emails[] = $submissionData->committee_notification_email;
    }
    if (!empty($submissionData->candidate_notification_email)) {
      $emails[] = $submissionData->candidate_notification_email;
    }
    $source = 'ORCA';

    $emails = implode(';', $emails);
    $submission_id = $financeReportingProcessor->logSubmission('json', json_encode($reportData), $source, $submissionData->version, $committee_id, $ip_address, 1.1);
    $response = $this->fundProcessor->submitCommitteeReport($committee_id, $reportData, $emails, $source, 1.1);
    $financeReportingProcessor->logProcessResults($submission_id, $response->success, $response->message, $response->report_id ?? NULL, $response->fund_id ?? NULL);
    $this->updatePropertiesAfterReportSubmission($fund_id, $submissionData, $response);
    return $response;
  }

  /**
   * Performs common updates to ORCA based on success or failure of
   *
   * @param $fund_id
   *   The fund for which to update properties
   * @param $submissionData
   *   stdClass object representing the JSON as indicated below.
   *
   * Sample submission:
   *
   * {
   *   "" : ""
   * }
   * @throws Exception
   */
  protected function updatePropertiesAfterReportSubmission($fund_id, $submissionData, $responseData, $report_type = 'C3') {
    // Generate a list of properties to update,
    $properties = [];

    // If the save_email is checked on the response, we need to be able to "save email"
    // The property name references "saving a password" which is a hold-over form the days where
    // email and password saves were both controlled by this property.
    if ($submissionData->save_emails) {
      $properties[] = $this->generateProperty('PDC.UPLOADER.UPLOADMAIN:EMAIL', $submissionData->committee_notification_email);
      $properties[] = $this->generateProperty('PDC.UPLOADER.UPLOADMAIN:EMAIL2', $submissionData->candidate_notification_email);
      $properties[] = $this->generateProperty('PDC.UPLOADER.UPLOADMAIN:SAVEPASSWORDS', "true");
    }
    else {
      $properties[] = $this->generateProperty('PDC.UPLOADER.UPLOADMAIN:EMAIL', null);
      $properties[] = $this->generateProperty('PDC.UPLOADER.UPLOADMAIN:EMAIL2', null);
      $properties[] = $this->generateProperty('PDC.UPLOADER.UPLOADMAIN:SAVEPASSWORDS', "false");
    }

    // If the report type is C4 and it is a final report that has been submitted successfully
    // we need to add the FINALREPORT property.  We're not sure how this is used if at all in ORCA.
    if ($report_type == 'C4' && $submissionData->final_report) {
      $properties[] = $this->generateProperty("FINALREPORT", 'Y');
    }

    // Update the properties by calling a database function to perform the update of all properties based
    // on a JSON representation.
    $this->updateProperties($fund_id, $properties);


    // Add the properties to the response object so that the Java version of ORCA can keep the Derby database in sync.
    $responseData->properties = $properties;
  }

  /**
   * @param $fund_id
   * @param $user_data
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function generateReportPreview($fund_id, $user_data, $version) {
    $reportData = new stdClass();

    [$committee_id] = $this->_dataManager->db->executeQuery("SELECT committee_id from public.fund where fund_id = :fund_id", ['fund_id' => $fund_id])->fetchFirstColumn();

    // Add in committee data.
    $results = $this->_dataManager->db->executeQuery(
      "SELECT * from public.od_campaign_finance_summary_by_fund_id(:fund_id)",
      ['fund_id' => $fund_id]
    )->fetchAllAssociative();
    if (empty($results)) {
      //try again with the surplus call
      $results = $this->_dataManager->db->executeQuery(
        "SELECT * from public.od_surplus_balance where id = :committee_id",
        ['committee_id' => $committee_id]
      )->fetchAllAssociative();
      if (empty($results)) {
        $reportData->committee_data = new stdClass();
      }
    }
    if(!empty($results)) {
      $reportData->committee_data = (object)$results[0];
    }
    $reportData->filer_name = $reportData->committee_data->filer_name;
    $reportData->jurisdiction = $reportData->committee_data->jurisdiction;
    $reportData->office = $reportData->committee_data->office;
    $reportData->origin = $user_data->report_type;

    // Add user data
    $reportData->user_data = $user_data;
    $reportData->receipt_date = $user_data->submitted_at;
    $reportData->report_from = $user_data->period_start;
    $reportData->amends_report = $user_data->amends ?? null;
    $reportData->committee = $user_data->committee;

    // Add metadata
    $results = $this->fundProcessor->validateCommitteeReport($committee_id,  $user_data, $version);
    $reportData->metadata = $results->metadata;
    $reportData->version = $version;
    $reportData->messages = $results->messages;
    $reportData->message = $results->message;

    // category list
    if($user_data->report_type == 'c4'){
      $query = "select code, label from expense_category";
      $categories = $this->_dataManager->db->executeQuery($query)->fetchAllAssociative();

      $reportData->categorylist = $categories;
    }

    return $reportData;
  }

  /**
   * @param $fund_id
   * @throws Exception
   */
  public function insertReportingObligations($fund_id)
  {
    $this->_dataManager->db->executeQuery("select * from private.cf_populate_reporting_obligations(:fund_id)", ["fund_id" => $fund_id]);
  }

  /**
   * @param $fund_id
   * @return stdClass
   * @throws Exception|\Doctrine\DBAL\Driver\Exception
   */
  public function addOneReportingObligation($fund_id): stdClass
  {
    $response = new stdClass();
    $response->success = true;

    [$response->obligation_id] = $this->_dataManager->db->executeQuery("select private.cf_add_one_reporting_obligation(:fund_id)", ["fund_id" => $fund_id])->fetchFirstColumn();

    return $response;
  }

  /**
   * Stretch the reporting period to cover an earlier start date.
   * @param $fund_id
   * @param $obligation_id
   * @param $start_date
   * @return bool
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function combineReportingPeriods($fund_id, $obligation_id, $start_date):bool {
    $start_date = $this->parseDate($start_date);
    if (!$start_date) {
      throw new Exception('Invalid or missing start_date');
    }
    $obligation_id = $this->_dataManager->db->executeQuery(
      "
       SELECT private.cf_combine_reporting_periods(o.obligation_id, :start_date)
       from private.reporting_obligation o
       where o.fund_id=:fund_id and o.obligation_id = :obligation_id
      ",
      ['fund_id' => $fund_id, 'obligation_id' => $obligation_id, 'start_date' => $start_date]
    )->fetchOne();
    return !empty($obligation_id);
  }

  /**
   * Submit a report of deposit.
   * @param int $fund_id
   *   The fund ID identifying the ORCA fund
   * @param int $deposit_id
   *   The deposit id of the deposit.
   * @param stdClass $submissionData
   * @return stdClass
   *   A stdClass object indicating the results of the C3ReportProcessor submission,
   *   but with property modifications added so that ORCA can update maintain the
   *   property changes.
   */
  public function submitC3($fund_id, $deposit_id, $submissionData, $ip_address) {
    // Call the submission generator to generate the deposit report from the data in the private schema
    $reportData = $this->generateC3Submission($fund_id, $deposit_id);
    $reportData->treasurer = $submissionData->treasurer;
    return $this->submitReport($fund_id, $reportData,$submissionData, $ip_address, 1.1);
  }

  /**
   * Receive a summary report (C4) and post it via the submission processor
   * @param int $fund_id
   *   Indicates which fund we are working with
   * @param $obligation_id
   * @param stdClass $submissionData
   *   Parsed json data as described by updatePropertiesAfterReportSubmission
   * @param $ip_address
   * @return stdClass
   *   A stdClass object indicating the results of the C3ReportProcessor submission,
   *   but with property modifications added so that ORCA can update maintain the
   *   property changes.
   * @throws ConnectionException
   * @throws ORMException
   * @throws \Doctrine\DBAL\Driver\Exception|Exception
   */
  public function submitC4($fund_id, $obligation_id, $submissionData, $ip_address) {
    // Call the submission generator to generate the deposit report from the data in the private schema
    $reportData = $this->generateC4Submission($fund_id, $obligation_id);
    $reportData->treasurer = $submissionData->treasurer;
    return $this->submitReport($fund_id, $reportData, $submissionData, $ip_address, 1.1);
  }

  /**
   * @throws ConnectionException
   * @throws Exception
   */
  protected function generateC3Submission($fund_id, $deposit_id) {
    // The begin and end transactions are used to ensure that the temporary table that is created goes away
    $this->_dataManager->beginTransaction();
    // By selecting from the deposit event table we make sure that we have a deposit Id that exists and belongs to the fund we are trying to query.
    $results = $this->_dataManager->db->executeQuery("SELECT private.cf_c3_build_report(:deposit_id) from private.vdepositevents de where de.fund_id=:fund_id and de.trankeygen_id=:deposit_id", ['fund_id' => $fund_id, 'deposit_id' => $deposit_id]);
    $this->_dataManager->endTransaction();
    return json_decode($results->fetchFirstColumn()[0]);
  }

  /**
   * @throws ConnectionException
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  protected function generateC4Submission($fund_id, $obligation_id) {
    // The begin and end transactions are used to ensure that the temporary table that is created goes away
    $this->_dataManager->beginTransaction();
    // By selecting from the deposit event table we make sure that we have a deposit Id that exists and belongs to the fund we are trying to query.
    $results = $this->_dataManager->db->executeQuery(
      "
       select private.cf_c4_build_report(:obligation_id)
       from private.reporting_obligation ro
       where ro.fund_id=:fund_id and ro.obligation_id=:obligation_id
      ",
      ['fund_id' => $fund_id, 'obligation_id' => $obligation_id]
    );
    $this->_dataManager->endTransaction();
    return json_decode($results->fetchFirstColumn()[0]);
  }

  /**
   * @throws ConnectionException
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function generateC3Preview($fund_id, $deposit_id)
  {
    // get user data
    $user_data = $this->generateC3Submission($fund_id, $deposit_id);
    $data = $this->generateReportPreview($fund_id, $user_data, 1.1);

    unset($user_data->submitted_at);
    $data->report_number = '';

    return $data;
  }

  /**
   * @throws ConnectionException
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function generateC4Preview($fund_id, $reporting_obligation_id) {
    // get user data
    $user_data = $this->generateC4Submission($fund_id, $reporting_obligation_id);
    return $this->generateReportPreview($fund_id, $user_data, 1.1);
  }

  /**
   * @param int $fund_id
   *   Fund for which properties are saved
   * @param array $properties
   *   array off stdclass objects in a form equivalent to the following JSON:
   *
   *   {
   *     "name": "PROPERTYNAME",
   *     "value": "PROPERTYVALUE",
   *   }
   * @throws Exception
   */
  public function updateProperties(int $fund_id, array $properties) {
    // build JSON payload from the array.
    $properties_json = json_encode($properties);

    // call stored procedure to execute the save.
    $this->_dataManager->db->executeStatement(
      "
       select private.cf_update_properties(:fund_id, CAST(:properties_json AS JSON))
       FROM fund where fund_id=:fund_id
      ",
      ['fund_id' => $fund_id, 'properties_json' => $properties_json]
    );
  }

  public function parseDate($date) {
    if ($date) {
      try {
        $do = new DateTime($date);
        return $do->format('Y-m-d');
      } /** @noinspection PhpUnusedLocalVariableInspection */ catch (Exception $e) {
        return false;
      }
    }
    return false;
  }

  /**
   * @param $fund_id
   * @param $attachedPageJson
   * @return stdClass
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function saveAttachedPage($fund_id, $attachedPageJson) {
    $response = new stdClass();
    $aData = json_decode($attachedPageJson);
    $attachedPage = $aData->attachedPageData->attachedPage;

    if(!$attachedPage){
      throw new Exception("Invalid attachment data");
    }
    if(!isset($attachedPage->target_id) || !is_numeric($attachedPage->target_id)){
      throw new Exception("Invalid target id");
    }
    if(!isset($attachedPage->target_id) && empty($attachedPage->text_data)){
      throw new Exception("No attachments to save");
    }

    switch($attachedPage->target_type){
      case "reporting_obligation":
      case "deposit":
        break;
      default:
        throw new Exception("Invalid target_type");
    }

    // strip non-ascii characters - https://alvinalexander.com/php/how-to-remove-non-printable-characters-in-string-regex/
    // exclude ascii x0A to allow line break special character.
    $attachedPage->text_data = preg_replace('/[\x00-\x09\x0B-\x1F\x80-\xFF]/', '', $attachedPage->text_data);

    // When saved and no value in text data, delete record
    if(isset($attachedPage->target_id) && isset($attachedPage->target_type) && trim($attachedPage->text_data) === ''){
      $this->_dataManager->db->executeQuery(
        '
           delete from private.attachedtextpages a
           where a.target_id = :target_id and a.fund_id = :fund_id and a.target_type = :target_type
        ',
        [
          'target_id' => $attachedPage->target_id,
          'fund_id' => $fund_id,
          'target_type' => $attachedPage->target_type
        ]
      );

      $response->success = true;

    } else {
      //Create record and update if record already exist.
      $this->_dataManager->db->executeQuery('insert into private.attachedtextpages(fund_id, target_id, target_type, text_data) values (:fund_id, :target_id, :target_type, :text_data) on conflict (fund_id, target_id, target_type)
                                                                                DO UPDATE SET text_data = excluded.text_data', ['fund_id' => $fund_id, 'target_id' => $attachedPage->target_id, 'target_type' => $attachedPage->target_type, 'text_data' => $attachedPage->text_data]);

      $result = $this->_dataManager->db->executeQuery("SELECT private.cf_get_attachment_page_data(:fund_id, :target_id, :target_type)", ['fund_id' => $fund_id, 'target_id' => $attachedPage->target_id, 'target_type' => $attachedPage->target_type])->fetchOne();
      $response->attachedPageData = json_decode($result);
      $response->success = true;

    }
    return $response;
  }
}
