<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait CorrectionTrait {
  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function saveCorrection($fund_id, $correction){
    //validate
    if(empty($correction->reported_id)) {
      if (empty($correction->reported_date)) {
        throw new Exception('Reported date required');
      }
      if (empty($correction->reported_amount)) {
        throw new Exception('Report amount required');
      }
      else {
        if (!is_numeric($correction->reported_amount)) {
          throw new Exception('Amount must be a number');
        }
      }
    }
    if (empty($correction->corrected_amount)) {
      throw new Exception('Corrected amount required');
    }
    else {
      if (!is_numeric($correction->corrected_amount)) {
        throw new Exception('Amount must be a number');
      }
    }
    if(empty($correction->reported_id) && empty($correction->type)){
      throw new Exception('correction type required');
    }
    if(empty($correction->description)){
      throw new Exception('Description required');
    }

    $savedCorrectionId = $this->_dataManager->db->executeQuery("select private.cf_save_correction(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($correction)])->fetchOne();
    $savedCorrection = $this->_dataManager->db->executeQuery("select cast(private.cf_get_correction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedCorrectionId])->fetchOne();
    $response = new stdClass();
    $response->success = !empty($savedCorrection);
    if ($response->success) {
      $response->{'correction'} = json_decode($savedCorrection);
    }
    return $response;
  }

  /**
   * Retrieve a correction record
   * @param $fund_id
   *   Fund that contains the correction
   * @param $correction_id
   *   Id of the correction
   * @return stdClass
   *   Object containing response data.
   * @throws \Doctrine\DBAL\Exception
   */
  public function getCorrection($fund_id, $correction_id) {
    $response = new stdClass();
    $json = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_get_correction(:fund_id, :correction_id)",
      ['fund_id' => $fund_id, 'correction_id' => $correction_id]
    );
    $response->success = true;
    $response->correction = json_decode($json);
    return $response;
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function deleteCorrection($fund_id, $correctionId){
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    $correction = json_decode($this->_dataManager->db->executeQuery("select cast(private.cf_get_correction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $correctionId])->fetchOne());
    if(empty($correction)){
      throw new Exception('Correction not found.');
    }
    //delete contribution in receipt table
    $this->_dataManager->db->executeStatement('select private.cf_delete_correction(:id)', ['id' => $correctionId]);
    return $response;
  }
}
