<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait DebtTrait {
  /**
   * Saves vendor debt forgiveness to database and updates aggregates and account balances
   * Sample data file at tests/data/orca/debt/vendor-debt-forgiveness.yml
   * @param $fund_id - integer
   * @param $forgiven - json data from controller
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function saveVendorDebtForgiveness($fund_id, $forgiven): stdClass {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;
    $this->validatePositiveAmount($forgiven->amount);
    if (!isset($forgiven->date_)) {
      throw new Exception('Date not set');
    }
    $savedForgivenId = $this->_dataManager->db->executeQuery("select private.cf_save_vendor_debt_forgiveness(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($forgiven)])->fetchOne();
    $savedForgiven = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedForgivenId])->fetchOne();

    $response->success = !empty($savedForgiven);
    if ($response->success) {
      $response->{'vendor-debt-forgiveness'} = json_decode($savedForgiven);
    }
    return $response;
  }

  /**
   * Retrieve a vendor debt and all its detail transactions.
   * @param $fund_id
   * @param $account_id
   * @return false|stdClass|string
   * @throws Exception
   */
  public function getVendorDebtDetails($fund_id, $account_id) {
    $data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_debt_details(:fund_id, :id )", ['fund_id' => $fund_id, 'id' => $account_id]);
    $response = new stdClass();
    if ($data) {
      $response = json_decode($data);
      $response->success = true;
    }
    else {
      $response->success = false;
    }
    return $response;
  }

  /**
   * @throws \Exception
   */
  public function saveInvoicedExpensePayment($fund_id, $payment){
    $response = new stdClass();
    if (empty($payment->date_)) {
      throw new Exception("Vendor debt payment requires date");
    }

    $this->validatePositiveAmount($payment->amount);

    $paymentId = $this->_dataManager->db->fetchOne("SELECT private.cf_save_invoiced_expense_payment(:fund_id, CAST(:json AS JSON))", ['fund_id' => $fund_id, 'json' => json_encode($payment)]);
    $data =  $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $paymentId]);

    if ($data) {
      $response->{'invoiced-expense-payment'} = json_decode($data);
      $response->success = true;
    }

    return $response;
  }
}
