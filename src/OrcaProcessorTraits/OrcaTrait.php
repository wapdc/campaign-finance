<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use DateTime;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\TransactionRequiredException;
use stdClass;
use WAPDC\Core\Model\Candidacy;
use WAPDC\Core\Model\Election;

trait OrcaTrait {
  /**
   * @throws \Exception
   */
  private function orcaDate($date) {
    return (new DateTime($date))->format('m/d/Y');
  }

  /**
   * Generate the list of ORCA properties from a committee record.
   *
   * @param stdClass $submission
   * @param $campaign_start_date
   * @param $election_code
   * @param $fund_id
   * @param null $carry_forward
   * @return array
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws ORMException
   * @throws OptimisticLockException
   * @throws TransactionRequiredException
   * @throws \Exception
   */
  private function convertSubmissionToOrcaProperties(stdClass $submission, $campaign_start_date, $election_code, $fund_id, $carry_forward = null): array
  {
    $properties = [];

    $committee = $submission->committee;
    $local_filing = $submission->local_filing ?? NULL;
    $treasurer = $this->findTreasurer($committee);
    $special = false;
    $year = (int)$election_code;
    if (!$committee->continuing) {
      $election_code = $committee->election_code;
      $election = $this->_dataManager->em->find(Election::class, $election_code);
      $election_date = $election->election_date;
      $special = strpos($election_code, "S") ? 'true' : 'false';
      $year = $election_date->format('Y');
    }

    $properties[] = $this->generateProperty('FUND_ID', $fund_id);
    $properties[] = $this->generateProperty('ELECTIONYEAR', $year);
    $properties[] = $this->generateProperty('DATE', $year);
    $properties[] = $this->generateProperty('ISSPECIAL', $special);
    $properties[] = $this->generateProperty("STARTDATE", $committee->start_date?? NULL);
    $properties[] = $this->generateProperty('CAMPAIGNINFO:ISCANDIDATE', $committee->committee_type=='CA' ? "true" : "false");
    $properties[] = $this->generateProperty('C1PC:STATEWIDE', $committee->bonafide_type?? NULL);
    $properties[] = $this->generateProperty("committeeId", $committee->committee_id);

    if ($campaign_start_date) {
      $properties[] = $this->generateProperty("CAMPAIGNINFO:STARTDATE", $this->orcaDate($campaign_start_date));
    }

    if ($carry_forward) {
      $properties[] = $this->generateProperty("carry_forward", $carry_forward);
    }

    $election_codes = $this->_dataManager->db->executeQuery(
      "select string_agg(election_code, ',') from private.election_participation where fund_id = :fund_id",
      ['fund_id' => $fund_id]
    )->fetchOne();
    if ($election_codes) {
      $ballot_info = $this->getBallotTypeFromElectionCodes($election_codes);
      $properties[] = $this->generateProperty($ballot_info->name, $ballot_info->value);
    }

    $properties[] = $this->generateProperty('COMMITTEEINFO:FILERID', $committee->filer_id?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:COUNTY', $committee->county?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:NAME', $committee->name?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:STREET', $committee->contact->address?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:ADDRESS', $committee->contact->address?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:CITY', $committee->contact->city?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:STATE', $this->formatState($committee->contact->state?? NULL));
    $properties[] = $this->generateProperty('COMMITTEEINFO:ZIP', $committee->contact->postcode?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:PHONE1', $committee->contact->phone?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:EMAIL', $committee->contact->email?? NULL);
    $properties[] = $this->generateProperty('COMMITTEEINFO:continuing', $committee->continuing);
    $properties[] = $this->generateProperty('COMMITTEEINFO:pacType', $committee->pac_type);

    $properties[] = $this->generateProperty('TREASURERINFO:FIRSTNAME', $treasurer->name?? $treasurer->person?? NULL);
    $properties[] = $this->generateProperty('TREASURERINFO:LASTNAME', null);
    $properties[] = $this->generateProperty('TREASURERINFO:MIDDLE', null);
    $properties[] = $this->generateProperty('TREASURERINFO:STREET', $treasurer->address?? NULL);
    $properties[] = $this->generateProperty('TREASURERINFO:CITY', $treasurer->city?? NULL);
    $properties[] = $this->generateProperty('TREASURERINFO:STATE', $this->formatState($treasurer->state?? NULL));
    $properties[] = $this->generateProperty('TREASURERINFO:ZIP', $treasurer->postcode?? NULL);
    $properties[] = $this->generateProperty('TREASURERINFO:PHONE', $treasurer->phone?? NULL);
    $properties[] = $this->generateProperty('TREASURERINFO:EMAIL', $treasurer->email?? NULL);
    $properties[] = $this->generateProperty('TREASURERINFO:MINISTERIAL', ($treasurer->ministerial ?? false) ? 'true' : 'false');

    $properties[] = $this->generateProperty('BANKINFO:NAME', $committee->bank->name ?? null);
    $properties[] = $this->generateProperty('BANKINFO:ADDRESS', $committee->bank->address ?? null);
    $properties[] = $this->generateProperty('BANKINFO:CITY', $committee->bank->city ?? null);
    $properties[] = $this->generateProperty('BANKINFO:STATE', $this->formatState($committee->bank->state ?? null));
    $properties[] = $this->generateProperty('BANKINFO:ZIP', $committee->bank->postcode ?? null);

    if ($committee->election_code) {
      $properties[] = $this->generateProperty('CAMPAIGNINFO:ISSPECIAL', strlen($election_code) > 4 ? "true": "false");
    }
    $properties[] = $this->generateProperty('CAMPAIGNINFO:ELECTIONYEAR', substr($election_code, 0,4));

    if ($committee->pac_type=='candidate') {
      // Determine correct office and jurisdiction in order to find office type.
      /** @var Candidacy $candidacy */
      $candidacy = $this->_dataManager->em->getRepository(Candidacy::class)->findOneBy(['committee_id' => $committee->committee_id]);
      if ($candidacy ) {
        $office_code = $candidacy->office_code;
        $jurisdiction_id = $candidacy->jurisdiction_id;
      }
      else {
        $office_code = $committee->candidacy->office_code;
        $jurisdiction_id = $committee->candidacy->jurisdiction_id;
      }

      $office_types = $this->_dataManager->
      db->executeQuery(
        "
         select min(legacy_code)
         from (SELECT jurisdiction_category from foffice
         where offcode = :office_code
         union select category from wapdc.public.jurisdiction where jurisdiction_id = :jurisdiction_id) v
         join jurisdiction_category jc on v.jurisdiction_category = jc.jurisdiction_category_id
        ",
        ['office_code' => $office_code, 'jurisdiction_id' => $jurisdiction_id]
      )->fetchFirstColumn();
      if ($office_types) {
        $properties[] = $this->generateProperty('CANDIDATEINFO:OFFICETYPE', $office_types[0]);
      }
      $properties[] = $this->generateProperty('CANDIDATEINFO:OFFICESOUGHT', $committee->candidacy->office ?? NULL);
      $properties[] = $this->generateProperty('CANDIDATEINFO:JURISDICTION', $committee->candidacy->jurisdiction ?? NULL);
      $properties[] = $this->generateProperty('CANDIDATEINFO:POSITIONNO', $committee->candidacy->position ?? NULL);
      $properties[] = $this->generateProperty('EMAIL', $committee->candidacy->email ?? NULL);
      $properties[] = $this->generateProperty('FIRSTNAME', $committee->candidacy->person ?? NULL);
      $properties[] = $this->generateProperty('LASTNAME',NULL);
      $properties[] = $this->generateProperty('STREET', $committee->contact->address?? NULL);
      $properties[] = $this->generateProperty('CITY', $committee->contact->city?? NULL);
      $properties[] = $this->generateProperty('STATE', $this->formatState($committee->contact->state?? NULL));
      $properties[] = $this->generateProperty('ZIP', $committee->contact->postcode?? NULL);
    }
    if (!empty($local_filing) && !empty($local_filing->seec)) {
      if ($local_filing->seec->required) {
        $properties[] = $this->generateProperty('SEEC_FILER', "true");
      }
    }
    return $properties;
  }

  /**
   * Execute a query form the data/orca directory using dynamic query processing.
   * @param $file
   * @param array $parameters
   * @return array
   * @throws Exception
   */
  public function orcaQuery($file, $parameters = []) {
    $dir = $this->project_directory . '/data/orca';
    if (!strpos($file, '.sql')) {
      $file .= ".sql";
    }
    if (!file_exists("$dir/$file")) {
      throw new Exception("Invalid Query: $dir/$file");
    }
    return $this->_dataManager->executeQueryFromFile("$dir","$file", $parameters);
  }

  /**
   * @param $fund_id
   * @param $payload
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception|Exception
   */
  public function purgeOldOrcaIds($fund_id, $payload): stdClass
  {
    $response = new stdClass();
    $json = json_decode($payload);
    if (!$json->max_id) {
      throw new Exception('Max orca_id required');
    }
    if (!$json->version) {
      throw new Exception('version required');
    }
    $purge = $this->_dataManager->db->executeQuery("select * from private.cf_purge_old_orca_ids(:fund_id, :old_max_id, :version)", ['fund_id' => $fund_id, 'old_max_id' => $json->max_id, 'version' => $json->version])->fetchOne();
    $response->purge = $purge;
    $response->success = true;
    return $response;
  }

  private function findTreasurer($json){
    foreach($json as $property => $value) {
      if ($property == 'officers'){
        foreach ($value as $data){
          foreach ($data as $prop => $val) {
            if ($prop == 'treasurer' && $val == true) {
              return $data;
            }
          }
        }
      }
      if ($property=='candidacy'){
        foreach ($value as $prop => $val){
          if ($prop == 'treasurer' && $val == true) {
            return $value;
          }
        }
      }
    }
    return null;
  }

  /**
   * Format a state properly.
   * @param $state
   * @return string
   */
  private function formatState($state){
    if (empty($state)) return '';
    $fixed_data = strtoupper(trim($state) ?? '');
    if (strlen($fixed_data) > 2) {
      switch ($fixed_data) {
        case 'WASHINGTON':
          $fixed_data = 'WA';
          break;
        case 'WYOMING':
          $fixed_data = 'WY';
          break;
        case 'VIRGINIA':
          $fixed_data = 'VA';
          break;
        case 'OREGON':
          $fixed_data = 'OR';
          break;
        case 'CALIFORNIA':
          $fixed_data = 'CA';
          break;
        case 'NEW YORK':
          $fixed_data = 'NY';
          break;
        case 'KANSAS':
          $fixed_data = 'KS';
          break;
        case 'WASHINGTON DC':
          $fixed_data = 'DC';
          break;
        default:
          $fixed_data = "";
      }
    }
    return $fixed_data;
  }
}
