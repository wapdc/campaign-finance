<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait ContactTrait {
  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function saveContact($fund_id, $jsonData)
  {
    $contact = json_decode($jsonData);
    if (isset($contact->trankeygen_id) && !($this->validateTrankeygen($fund_id, 'private.contacts', $contact->trankeygen_id))){
      throw new Exception('Invalid transaction id');
    }
    $this->fieldValidationsForContacts($contact);
    $savedContactId = $this->_dataManager->db->executeQuery("select private.cf_save_contact(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => $jsonData])->fetchOne();
    $savedContact = $this->_dataManager->db->executeQuery("select cast(private.cf_get_contact(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedContactId])->fetchOne();
    $response = new stdClass();
    $response->success = true;
    $response->contact = json_decode($savedContact);
    return $response;
  }

  /**
   * @param $id
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function loadContact($fund_id, $id): stdClass
  {
    $response = new stdClass();
    $contact =  $this->_dataManager->db->executeQuery("SELECT cast(private.cf_get_contact(:fund_id, :trankeygen_id) as text)", ['fund_id' => $fund_id, 'trankeygen_id' => $id])->fetchOne();
    if (!$contact) {
      throw new Exception('Contact not found');
    }
    $response->contact = json_decode($contact);
    $response->success = TRUE;
    return $response;
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function importContacts($fund_id, $jsonData){
    $response = json_decode($this->_dataManager->db->executeQuery(
      "select * from private.cf_save_contacts(:fund_id, :json_data)",
      [
        "fund_id" => $fund_id,
        "json_data" => json_encode($jsonData),
      ]
    )->fetchOne());
    $response->success = true;
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function mergeContacts($fund_id, $contact_id, $duplicate_id, $sameContact = true){
    $response = new stdClass();
    $response->success = true;
    if($sameContact){
      if ($this->isMergeable($duplicate_id)) {
        $response->contacts = json_decode($this->_dataManager->db->executeQuery("select private.cf_merge_contacts(:fund_id, :contact_id, :duplicate_id)", ["fund_id" => $fund_id, "contact_id" => $contact_id, "duplicate_id" => $duplicate_id])->fetchOne());
      }
      else {
        $response->success = false;
        $response->error = 'Unable to merge contacts. Unmergeable transactions detected.';
      }
    } else {
      $this->_dataManager->db->executeStatement(
        "
         UPDATE private.contact_duplicates set verified_not_duplicate=true
         where contact_id = :contact_id and duplicate_id = :duplicate_id
         and private.contact_duplicates.contact_id in (SELECT trankeygen_id
         FROM private.trankeygen where fund_id = :fund_id)
        ",
        ['fund_id' => $fund_id, 'contact_id' => $contact_id, 'duplicate_id' => $duplicate_id]
      );
    }
    return $response;
  }

  /**
   * @throws Exception
   */
  public function fieldValidationsForContacts($contact, $type=null){
    if (!$type) {
      if (!isset($contact->type)) {
        throw new Exception('Contact type required');
      }
      if (!in_array($contact->type, ['SP', 'LDP', 'IND', 'CPL', 'TRB', 'OG', 'HDN', 'BUS', 'CP', 'MP', 'CAU', 'CAN', 'GRP', 'FI', 'PAC', 'UN'])) {
        throw new Exception('Invalid contact type');
      }
    }
    $contact_type = $contact->type ?? $type;
    if (!isset($contact->name)) {
      throw new Exception('Contact name required');
    }
    if ($contact_type === 'IND' && !isset($contact->firstname)) {
      throw new Exception('Contact first name required');
    }
    if ($contact_type === 'IND' && strlen($contact->prefix ?? '') > 5){
      throw new Exception('Name prefix is too long');
    }
    if ($contact_type === 'IND' && strlen($contact->firstname ?? '') > 50){
      throw new Exception('First name is too long');
    }
    if ($contact_type === 'IND' && strlen($contact->middleinitial ?? '') > 10){
      throw new Exception('Middle initial is too long');
    }
    if ($contact_type === 'IND' && strlen($contact->lastname ?? '') > 50){
      throw new Exception('Last name is too long');
    }
    if ($contact_type === 'IND' &&  strlen($contact->prefix ?? '') > 5){
      throw new Exception('Name suffix is too long');
    }
    if (isset($contact->street) && strlen($contact->street) > 100){
      throw new Exception('Street is too long');
    }
    if (isset($contact->city) && strlen($contact->city) > 100){
      throw new Exception('City is too long');
    }
    if (isset($contact->state) && strlen($contact->state) > 2){
      throw new Exception('State is too long');
    }
    if (isset($contact->zip) && strlen($contact->zip) > 50){
      throw new Exception('Zip is too long');
    }
    if (isset($contact->phone) && strlen($contact->phone) > 50){
      throw new Exception('Phone is too long');
    }
    if (isset($contact->email) && strlen($contact->email) > 50){
      throw new Exception('Email is too long');
    }
    if (isset($contact->memo) && strlen($contact->memo) > 1000){
      throw new Exception('Memo is too long');
    }
    if (isset($contact->employername) && strlen($contact->employername) > 110){
      throw new Exception('Employer name is too long');
    }
    if (isset($contact->employerstreet) && strlen($contact->employerstreet) > 100){
      throw new Exception('Employer street is too long');
    }
    if (isset($contact->employercity) && strlen($contact->employercity) > 100){
      throw new Exception('Employer city is too long');
    }
    if (isset($contact->employerstate) && strlen($contact->employerstate) > 2){
      throw new Exception('Employer state is too long');
    }
    if (isset($contact->employerzip) && strlen($contact->employerzip) > 50) {
      throw new Exception('Employer zip is too long');
    }
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function validateContact($fund_id, $json): stdClass
  {
    $response = new stdClass();
    $response->success = true;
    $potential_duplicates = $this->_dataManager->db->executeQuery("select * from private.cf_contact_find_duplicates(:fund_id, :json_data) d
    join private.vcontacts c on d.contact_id = c.trankeygen_id where c.fund_id = :fund_id limit 5", ["fund_id" => $fund_id, "json_data" => json_encode($json)])->fetchAllAssociative();
    $response->potential_duplicates = $potential_duplicates;
    return $response;
  }

  /**
   * @param $fund_id
   * @param $contact_id
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function deleteContact($fund_id, $contact_id): stdClass
  {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;
    $transactions = $this->_dataManager->db->executeQuery(
      'select coalesce(r.trankeygen_id, l.trankeygen_id, ee.trankeygen_id, d.debtobligation_id)
            as transaction_id, cc.trankeygen_id as couple_id from private.trankeygen t
            left join private.receipts r on r.contid = t.trankeygen_id
            left join private.loans l on l.contid = t.trankeygen_id
            left join private.expenditureevents ee on ee.contid = t.trankeygen_id
            left join private.debtobligation d on d.contid = t.trankeygen_id or d.pid = t.trankeygen_id
            left join private.couplecontacts cc on cc.contact1_id = t.trankeygen_id or cc.contact2_id = t.trankeygen_id
            where t.trankeygen_id = :contact_id and fund_id = :fund_id',
      ['contact_id' => $contact_id, 'fund_id' => $fund_id])->fetchAssociative();
    if ($transactions['transaction_id']) {
      $response->success = false;
      $response->errors = 'Cannot delete contact. Contact has associated transactions';
      throw new Exception('Contact has associated transactions');
    }
    else if ($transactions['couple_id']) {
      $response->success = false;
      $response->errors = 'Cannot delete contact. Contact is part of a couple contact';
      throw new Exception('Contact has associated couple contact record');
    }
    else {
      $this->_dataManager->db->executeQuery('delete from private.trankeygen where trankeygen_id = :contact_id and fund_id = :fund_id', ['contact_id' => $contact_id, 'fund_id' => $fund_id]);
    }
    return $response;
  }

  /**
   * Checks to see if the duplicate contact record can be merged.
   * @param $duplicate_id
   * @return bool
   *   Indicates whether the contact is mergeable.
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function isMergeable ($duplicate_id){
    $unmergeable_records = $this->_dataManager->db->executeQuery(
      '
       select count(1) from private.receipts r
       where contid = :contact_id and r.type not in (1, 4, 7, 8, 11, 18, 27, 28, 6, 16, 35, 40, 41)
      ',
      ['contact_id' => $duplicate_id]
    )->fetchOne();
    return $unmergeable_records == 0;
  }

  /**
   * Add and/or remove members from a group.
   * @param int $fund_id
   * @param stdClass $payload
   *   Properties:
   *     trankeygen_id - the id of the group contact
   *     add - An array of contact trankeygen_ids to add
   *     remove - an array of contact trankeygen_ids to remove.
   * @return void
   * @throws \Doctrine\DBAL\Exception
   */
  public function modifyGroupMembers(int $fund_id, stdClass $payload) {
    $this->_dataManager->db->executeStatement("SELECT private.cf_modify_group_members(:fund_id, :json)", ['fund_id'=> $fund_id, 'json' => json_encode($payload)]);
  }
}
