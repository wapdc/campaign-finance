<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\TransactionRequiredException;
use stdClass;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\Core\Model\User;

trait FundTrait {
  /**
   * Generate a fund record for a new ORCA file.
   * @param $committee_id
   * @param $request_data
   * @param $user_name
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws ORMException
   * @throws OptimisticLockException
   * @throws TransactionRequiredException
   * @throws \Exception
   */
  public function generateOrcaFund($committee_id, $request_data, $user_name): stdClass
  {
    if (!$committee_id) {
      throw new Exception('Invalid committee id');
    }
    /** @var Committee $committee */
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    if (!$committee) {
      throw new Exception('Committee not found');
    }

    if (!$committee->continuing && empty($request_data->campaign_start_date)) {
      throw new Exception('Campaign start date not provided.');
    }

    // Default election code and start date when appropriate
    if ($committee->pac_type == 'surplus') {
      $election_code = $committee->start_year;
      $campaign_start_date = $committee->start_year . "-01-01";
    } else {
      if (!$committee->continuing) {
        $election_code = $committee->election_code ?? $request_data->election_year;
      } else {
        $campaign_start_date = $request_data->election_year . '-01-01';
        $election_code = $request_data->election_year ?? null;
      }
      if (!$election_code) {
        throw new Exception('Missing Election year');
      }
    }

    // always use the campaign start date specified by the user
    if(!empty($request_data->campaign_start_date)) {
      $campaign_start_date = $request_data->campaign_start_date;
    }

    $this->validateCampaignStartDate($election_code, $committee->start_year, $committee->continuing, $committee->election_code, $campaign_start_date, $committee->pac_type == 'surplus');

    $carry_forward = null;
    if (isset($request_data->carry_forward_cash)) {
      $carry_forward = $request_data->carry_forward_cash;
    }

    // Return the response
    $response = new stdClass();
    // Return the correct committee.
    $response->fund = $fundData =  new stdClass();
    $fundData->fund_id = $this->fundProcessor->ensureCommitteeFundAndVendor($committee_id, substr($election_code, 0,4), 'PDC');
    $submission = $this->committeeManager->getCommitteeSubmission($committee_id);
    $token = $this->committeeManager->generateAPIToken($committee_id, "", $user_name);
    $response->fund->committee = $submission->committee ?? null;
    if (isset($request_data->election_codes)) {
      $request_data_json = json_encode($request_data);
      $response->election_codes = $this->_dataManager->db->executeQuery(
        'select * from private.cf_save_election_codes(cast(:fund_id as integer), cast(:input_json as json))',
        ['fund_id' => $fundData->fund_id, 'input_json' => $request_data_json]
      )->fetchOne();
    }
    $response->orcaProperties = $this->convertSubmissionToOrcaProperties($submission, $campaign_start_date ?? null, $election_code, $fundData->fund_id, $carry_forward);
    $this->_dataManager->db->executeQuery('select * from private.cf_populate_limits(:fund_id)', ['fund_id' => $fundData->fund_id]);
    $response->orcaProperties[] = $this->generateProperty('TOKEN', $token);
    return $response;
  }

  /**
   * @param $request_data
   * @param $user_name
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function generateOrcaFundExisting($request_data, $user_name): stdClass
  {
    $response = new stdClass();
    $generate_token = false;
    // First try and load data based on token.
    if (!empty($request_data->token)) {
      $committee_id = $this->committeeManager->validateAPIToken($request_data->token);
    }
    if (empty($committee_id)) {
      // No token means lookup based on filer_id and election year.
      $generate_token = true;

      // Try and find committee based on filer_id
      $committee_ids = $this->_dataManager->db->executeQuery(
        "SELECT committee_id FROM committee where filer_id = :filer_id AND :election_year BETWEEN start_year and coalesce(end_year, :election_year)",
        ['filer_id' => $request_data->filer_id ?? NULL, 'election_year' => (int)($request_data->election_year ?? -1)]
      )->fetchFirstColumn();
      if ($committee_ids) $committee_id = $committee_ids[0];
    }

    if ($committee_id) {
      /** @var Committee $committee */
      $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
      switch (true) {
        case ((!$committee->continuing && $request_data->election_year == substr($committee->election_code,0,4)) || $committee->pac_type == 'surplus'):
          $election_year = $committee->start_year;
          break;
        case ($request_data ->election_year >= $committee->start_year && (!$committee->end_year || $request_data->election_year <= $committee->end_year)):
          $election_year = $request_data->election_year;
          break;
        default:
          throw new Exception('Invalid election year');
      }
      $response->fund_id = $this->fundProcessor->ensureCommitteeFundAndVendor($committee_id, $election_year, 'PDC');
      $response->filer_id = $committee->filer_id;
      if ($generate_token) {
        $response->token = $this->committeeManager->generateAPIToken($committee_id, "ORCA $election_year", $user_name);
      }
      else {
        $response->token = $request_data->token ?? NULL;
      }
    }
    else {
      $response->fund_id = null;
    }

    return $response;
  }

  /**
   * Request access based on the posted TOKEN to
   * @param $request_data
   * @param User $pdc_user
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function requestFundAccess($request_data, User $pdc_user) {
    $response = new stdClass();
    if (!empty($request_data->token) && !empty($request_data->filer_id)) {
      $committee_id = $this->committeeManager->validateAPIToken($request_data->token);
      if ($committee_id) {
        [$filer_id] = $this->_dataManager->db->executeQuery("SELECT filer_id FROM committee where committee_id=:committee_id", ['committee_id' => $committee_id])
          ->fetchFirstColumn();
        if ($filer_id == $request_data->filer_id) {
          $this->committeeManager->grantAccessByCommitteeId($committee_id,$pdc_user, 'owner');
          $response->success = true;
        }
        else {
          $response->success = false;
          $response->error = 'Invalid token/filer_id combination';
        }
      }
      else {
        $response->success = false;
        $response->error = 'Invalid Token';
      }
    }
    else {
      $response->success = false;
      $response->error = "Token and filer id required";
    }
    return $response;
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function getFundData($fund_id) {
    $result = $this->_dataManager->db->executeQuery("SELECT private.cf_get_fund_data(:fund_id)", ['fund_id' => $fund_id]);
    [$json_text] = $result->fetchFirstColumn();
    $decodedResult = json_decode($json_text);
    $decodedResult->registrationProps = $this->getSubmissionSyncPayload($fund_id);
    return $decodedResult;
  }

  /**
   * @throws Exception
   */
  public function getImportFundInfo(int $fund_id) {
      $response = new stdClass();
      $json = $this->_dataManager->db->fetchOne('select * from private.cf_get_import_fund_info(:fund_id)',
          ['fund_id' => $fund_id]);

      $response->success = true;
      $response->{'get-import-fund-info'} = json_decode($json);

      return $json;
  }

  /**
   * @throws Exception|\Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function getSubmissionSyncPayload($fund_id): array
  {
    $fund_data = $this->_dataManager->db->executeQuery('select committee_id, election_code from fund where fund_id = :fund_id', ['fund_id' => $fund_id])->fetchAssociative();
    if (!$fund_data) {
      throw new Exception('The supplied fund_id could not be found.');
    }
    if (!$fund_data['committee_id'] || !$fund_data['election_code']) {
      throw new Exception('The fund record (#' . $fund_id . ') is missing the committee id or election code. Please contact PDC customer service with this error.');
    }
    $submission = $this->committeeManager->getCommitteeSubmission($fund_data['committee_id']);
    return $this->convertSubmissionToOrcaProperties($submission, null, $fund_data['election_code'], $fund_id);
  }
}
