<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use DateTime;
use Doctrine\DBAL\Exception;
use stdClass;

trait ExpenditureTrait {
  /**
   * @param $fund_id
   * @param stdClass $data
   * @return stdClass
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function saveExpenditure($fund_id, stdClass $data) {
    $response = new stdClass();
    $expenditureData = $data->expenditure;

    if (!$data->expenditure->expenditureEvent) {
      throw new Exception("Invalid expenditure data", 001);
    }
    if (empty($expenditureData->expenditureEvent->contid) || !($this->validateTrankeygen($fund_id, 'private.contacts', $expenditureData->expenditureEvent->contid))){
      throw new Exception('Invalid contid', 002);
    }
    if ((($expenditureData->expenditureEvent->acctnum ?? null) != 2000) && empty($expenditureData->expenditureEvent->bnkid)){
      throw new Exception('Missing bnkid' , 003);
    }
    if (!empty($expenditureData->expenditureEvent->checkno) && strlen($expenditureData->expenditureEvent->checkno) > 50){
      throw new Exception('Checkno is too long', 004);
    }
    if (isset($expenditureData->expenditureEvent->memo) && strlen($expenditureData->expenditureEvent->memo) > 1000){
      throw new Exception('Memo is too long', 005);
    }
    if (!isset($expenditureData->expenditureEvent->itemized) || ($expenditureData->expenditureEvent->itemized != 0 && $expenditureData->expenditureEvent->itemized != 1)) {
      throw new Exception( 'Invalid value for itemized', 006);
    }
    if (!isset($expenditureData->expenditureEvent->date_)){
      throw new Exception ('Missing date_', 007);
    }
    //force to throw error if we can't make a datetime from date_
    new DateTime($expenditureData->expenditureEvent->date_);
    //validate expenses
    foreach($expenditureData->expenses as $expense){
      if (!isset($expense->cid)){
        throw new Exception('Missing cid', 101);
      }
      if (!isset($expense->contid)){
        throw new Exception('Missing contid', 102);
      }
      if (!isset($expense->amount) || !is_numeric($expense->amount)) {
        throw new Exception('Invalid or missing amount', 103);
      }
      if (isset($expense->memo) && strlen($expense->memo) > 1000){
        throw new Exception('Memo is too long', 104);
      }
      if (isset($expense->description) && strlen($expense->description) > 1000){
        throw new Exception('Description is too long', 105);
      }
      if (!empty($expense->ad_id) && !$this->validateAd($fund_id, $expense->ad_id)) {
        throw new Exception('Invalid ad_id', 106);
      }
      //force to throw error if we can't make a datetime from date_
    }
    $trankeygen_id = $this->_dataManager->db->executeQuery("SELECT private.cf_save_expenditures(:fund_id, :json)", ['fund_id' => $fund_id, 'json' => json_encode($data)])->fetchOne();
    $result = $this->_dataManager->db->executeQuery("SELECT private.cf_get_expenditure_data(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id])->fetchOne();
    $response->success = true;
    $response->expenditure = json_decode($result);
    return $response;
  }

  /**
   * @param $fund_id
   * @param $id
   * @return stdClass
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function loadExpenditure($fund_id, $id): stdClass
  {
    $response = new stdClass();
    $expenditure =  $this->_dataManager->db->executeQuery("SELECT private.cf_get_expenditure_data(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $id])->fetchOne();
    if (!$expenditure) {
      throw new Exception('Expense not found');
    }
    $response->expenditure = json_decode($expenditure);
    $response->success = TRUE;
    return $response;
  }

  /**
   * Delete expenditure event
   * @param $fund_id
   *  the fund_id of the campaign the expenditure event is being deleted from
   * @param $id
   *  the trankeygen_id of the expenditure to be deleted
   * @return stdClass
   *  response to let us know if deletion succeeded or not
   * @throws \Doctrine\DBAL\Driver\Exception|Exception
   */
  public function deleteExpenditure($fund_id, $id): stdClass
  {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;
    $this->_dataManager->db->executeQuery('select private.cf_delete_expenditure(:fund_id, :id)', ['id' => $id, 'fund_id' => $fund_id])->fetchOne();
    return $response;
  }

  /**
   * @param $fund_id
   * @param $adjustment
   * @return stdClass
   * @throws Exception
   * Unlike vendor payment and forgiveness PID as the expenditure event's trankeygen_id, the PID for adjustment is the trankeygen_id of the expense receipt.
   */
  public function saveInvoicedExpenseAdjustment($fund_id, $adjustment){
    $response = new stdClass();

    if (empty($adjustment->date_)){
      throw new Exception("Invoiced expense adjustment requires date");
    }
    //adjustment amount can be a negative number
    if(empty($adjustment->amount)){
      throw new Exception('Amount not set.');
    }
    else if(!is_numeric($adjustment->amount)){
      throw new Exception('Amount must be a number.');
    }

    $response->success = false;

    $adjustmentId = $this->_dataManager->db->fetchOne("SELECT private.cf_save_invoiced_expense_adjustment(:fund_id, CAST(:json AS JSON))", ["fund_id" => $fund_id, 'json' => json_encode($adjustment)]);
    $data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $adjustmentId]);

    if ($data) {
      $response->{'invoiced-expense-adjustment'} = json_decode($data);
      $response->success = true;
    }

    return $response;
  }

  /**
   * @throws Exception
   */
  public function getInvoicedExpenses($fund_id, $expenditure_event_id){
    $data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_invoiced_expenses(:fund_id, :id)", ['fund_id' => $fund_id, 'id' => $expenditure_event_id]);
    $response = new stdClass();
    if ($data) {
      $response->{'invoiced-expenses'} = json_decode($data);
      $response->success = true;
    }
    else {
      $response->success = false;
    }
    return $response;
  }
}
