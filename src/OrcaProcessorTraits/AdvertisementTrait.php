<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use DateTime;
use Doctrine\DBAL\Exception;
use stdClass;

trait AdvertisementTrait {
  /**
   * @param $fund_id
   * @param $advertisementJson
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function saveAdvertisement($fund_id, $advertisementJson) {
    $response = new stdClass();
    $response->error = null;
    $aData = json_decode($advertisementJson);
    $advertisementData = $aData->advertisement->ad;
    try{
      if (empty($advertisementData->description)){
        throw new Exception('Description required');
      }
      if (isset($advertisementData->description) && strlen($advertisementData->description) > 1000) {
        throw new Exception('Description is too long');
      }
      if (!empty($aData->advertisement->targets) && count($aData->advertisement->targets)) {
        $total = 0;
        $hasCalculatedPercentages = false;
        foreach($aData->advertisement->targets as $target) {
          if (!empty($target->candidacy_id) && !$this->validateId('candidacy', 'candidacy_id', $target->candidacy_id)) {
            throw new Exception('Invalid candidacy id');
          }
          if (!empty($target->jurisdiction_id) && !$this->validateId('jurisdiction', 'jurisdiction_id', $target->jurisdiction_id)) {
            throw new Exception('Invalid jurisdiction id');
          }
          if (!empty($target->percent)) {
            if (!is_numeric($target->percent)) {
              throw new Exception( 'Percentage must be a number');
            }
            if ($target->percent > 100) {
              throw new Exception('Cannot enter percentage greater than 100%');
            }
            if ($target->percent < 0) {
              throw new Exception('Cannot enter percentage less than 0%');
            }
          }
          if (!empty($target->percent)) {
            $total += $target->percent;
          }
          else {
            $hasCalculatedPercentages = true;
          }
        }
        if ($hasCalculatedPercentages && $total >= 100) {
          throw new Exception('Calculated percentages must be greater than 0');
        }
        if ($total >= 101) {
          throw new Exception('Percentages allocated to candidates and proposals must not exceed 101%.');
        }
      }
      //force error if we cannot make date from first run date
      if (!empty($advertisementData->first_run_date)) {
        $valid_start_date = new DateTime($advertisementData->first_run_date);
      }
      if (!empty($advertisementData->last_run_date)) {
        $valid_end_date = new DateTime($advertisementData->last_run_date);
        if (!empty($valid_start_date) && $valid_end_date < $valid_start_date) {
          throw new Exception('Start date is before the end date');
        }
      }

      // Save and return data in response
      $ad_id = $this->_dataManager->db->executeQuery("SELECT private.cf_save_advertisements(:fund_id, :json)", ['fund_id' => $fund_id, 'json' => $advertisementJson])->fetchOne();
      $result = $this->_dataManager->db->executeQuery("SELECT private.cf_get_advertising_data(:fund_id, :ad_id)", ['fund_id' => $fund_id, 'ad_id' => $ad_id])->fetchOne();
      $response->advertisement = json_decode($result);
      $response->success = true;

    } catch (Exception $e) {
      $response->success = false;
      $response->error = $e->getMessage();
    } catch (\Exception $e) {
    }
    return $response;
  }

  /**
   * @param $fund_id
   * @param $id
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function loadAdvertisement($fund_id, $id): stdClass
  {
    $response = new stdClass();
    try {
      $advertisement =  $this->_dataManager->db->executeQuery("SELECT private.cf_get_advertising_data(:fund_id, :ad_id)", ['fund_id' => $fund_id, 'ad_id' => $id])->fetchOne();
      if (!$advertisement) {
        throw new Exception('Advertisement not found');
      }
      $response->advertisement = json_decode($advertisement);
      $response->success = TRUE;
    }
    catch (Exception $e) {
      $response->error = $e->getMessage();
      $response->success = false;
      $response->expense = new stdClass();
    }
    return $response;
  }

  /**
   * @param $fund_id
   * @param $ad_id
   * @return stdClass
   */
  public function deleteAdvertisement($fund_id, $ad_id): stdClass
  {
    $response = new stdClass();
    try {
      $this->_dataManager->db->executeStatement(
        "delete from private.ad where fund_id = :fund_id and ad_id = :ad_id",
        ['fund_id' => $fund_id, 'ad_id' => $ad_id]
      );
      $response->success = true;
    } catch(Exception $e) {
      $response->success = false;
    }
    return $response;
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  private function validateAd($fund_id, $ad_id){
    if (!is_int($ad_id)) {
      return false;
    }
    $record = $this->_dataManager->db->executeQuery(
      'select * from private.ad where fund_id = :fund_id and ad_id = :ad_id',
      ['fund_id' => $fund_id, 'ad_id' => $ad_id])->fetchOne();
    if (!$record) {
      return false;
    }
    return true;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function validateId($table, $idname, $id): bool
  {
    if (!is_int($id)) {
      return false;
    }
    $query = 'select 1 from '.$table.' where '.$idname.' = :id';
    $record = $this->_dataManager->db->executeQuery($query, ['id' => $id])->fetchOne();
    if (!$record) {
      return false;
    }
    return true;
  }
}
