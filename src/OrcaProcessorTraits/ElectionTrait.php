<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait ElectionTrait {
  /**
   * this code is intended to set these orca properties for the purpose of determining whether
   * the ui should allow contributions to be split between primary and general
   *
   * @param $election_codes
   * @return stdClass
   */
  public function getBallotTypeFromElectionCodes($election_codes): stdClass
  {
    $response = new stdClass();
    $response->name = "CAMPAIGNINFO:BALLOTTYPE";

    if (!$election_codes) {
      $response->value = '-1';
    }
    else if (strpos($election_codes, 'S')) {
      if (!strpos($election_codes, ',')) {
        $response->name = "CAMPAIGNINFO:MONTHOFSPECIAL";
        // Orca uses months 0-11
        $response->value = preg_replace('/[^S]*S/', '', $election_codes) - 1;
      } else {
        $response->value = '0';
      }
    } else if (strlen($election_codes) == 4) {
      $response->value = '4';
    } else if (strlen($election_codes) == 5) {
      $response->value = '1';
    } else {
      $response->value = '3';
    }

    return $response;
  }

  /**
   * @param $filer_id
   * @param $election_year
   * @return stdClass
   * @throws Exception|\Doctrine\DBAL\Driver\Exception
   */
  public function getFundIdByFilerIdAndElection($filer_id, $election_year): stdClass
  {
    $response = new stdClass();
    $response->fund_id = $this->_dataManager->db->executeQuery(
      "
       select fund_id from fund
       where filer_id = :filer_id and (election_code = :election_year or filer_id ilike '%*%');
      ",
      ['filer_id' => $filer_id, 'election_year' => $election_year]
    )->fetchOne();
    $response->success = true;
    return $response;
  }
}
