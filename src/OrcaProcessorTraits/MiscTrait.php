<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait MiscTrait {
  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function saveMiscOther($fund_id, $receipt) {
    //validate data
    if (!isset($receipt->name)) {
      throw new Exception('Name required');
    }
    if (!isset($receipt->date_)) {
      throw new Exception('Date not set');
    }
    if (!isset($receipt->amount)) {
      throw new Exception('Amount not set');
    }
    else {
      if ($receipt->amount < 0) {
        throw new Exception('Amount must not be a negative number');
      }
      if (!is_numeric($receipt->amount)) {
        throw new Exception('Amount must be a number');
      }
    }
    $this->fieldValidationsForContacts($receipt, 'HDN');
    //save
    $savedReceiptId = $this->_dataManager->db->executeQuery("select private.cf_save_misc_other(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($receipt)])->fetchOne();
    $savedReceipt = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedReceiptId])->fetchOne();
    $response = new stdClass();
    $response->success = !empty($savedReceipt);
    if ($response->success) {
      $response->{'misc-other'} = json_decode($savedReceipt);
    }
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function saveMiscBankInterest($fund_id, $misc){
    //validate data
    if(!isset($misc->contid)){
      throw new Exception('Bank Account required');
    }
    if(!isset($misc->description))
    {
      throw new Exception('Description required');
    }
    if (!isset($misc->amount)) {
      throw new Exception('Amount not set');
    }
    if (!isset($misc->date_)) {
      throw new Exception('Date not set');
    }
    else {
      if ($misc->amount < 0) {
        throw new Exception('Amount must not be a negative number');
      }
      if (!is_numeric($misc->amount)) {
        throw new Exception('Amount must be a number');
      }
    }
    //use contact field validation to verify values that will create contact
    $savedBankInterestId = $this->_dataManager->db->executeQuery("select private.cf_save_misc_bank_interest(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($misc)])->fetchOne();
    $savedBankInterest = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedBankInterestId])->fetchOne();
    $response = new stdClass();
    $response->success = !empty($savedBankInterest);
    if ($response->success) {
      $response->{'misc-bank-interest'} = json_decode($savedBankInterest);
    }
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function deleteMiscBankInterest($fund_id, $id): stdClass{
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    $miscBankInterest = json_decode($this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $id])->fetchOne());
    if(empty($miscBankInterest)){
      throw new Exception('Bank interest receipt not found.');
    }
    if($miscBankInterest->deposit_date){
      throw new Exception('Can not delete receipt that has been deposited.');
    }
    //delete bank interest receipt in receipt table
    $this->_dataManager->db->executeStatement('select private.cf_delete_bank_interest(:id)', ['id' => $id]);
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function deleteMiscOther($fund_id, $id): stdClass {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    $miscOther = json_decode($this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $id])->fetchOne());
    if(empty($miscOther)){
      throw new Exception('Other miscellaneous receipt not found.');
    }
    if($miscOther->deposit_date){
      throw new Exception('Can not delete receipt that has been deposited.');
    }
    //delete other receipt in receipt table
    $this->_dataManager->db->executeStatement('select private.cf_delete_other_receipt(:id)', ['id' => $id]);
    return $response;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   * @throws \Exception
   */
  public function saveVendorRefund($fund_id, $refund): stdClass {
    $response = new stdClass();
    $response->success = true;
    $response->errors = null;

    $this->validatePositiveAmount($refund->amount);
    if (!isset($refund->date_)) {
      throw new Exception('Date not set');
    }

    $savedRefundId = $this->_dataManager->db->executeQuery("select private.cf_save_vendor_refund(:fund_id, :json_data)", ["fund_id" => $fund_id, "json_data" => json_encode($refund)])->fetchOne();
    $savedRefund = $this->_dataManager->db->executeQuery("select cast(private.cf_get_transaction(:fund_id, :trankeygen_id) as text)", ["fund_id" => $fund_id, "trankeygen_id" => $savedRefundId])->fetchOne();
    $response->success = !empty($savedRefund);
    if ($response->success) {
      $response->{'vendor-refund'} = json_decode($savedRefund);
    }
    $response->success = !empty($savedRefund);
    if ($response->success) {
      $response->{'vendor-refund'} = json_decode($savedRefund);
    }
    return $response;
  }
}
