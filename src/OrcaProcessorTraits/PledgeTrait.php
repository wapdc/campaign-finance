<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait PledgeTrait {
  /**
   * @throws \Exception
   */
  public function savePledge($fund_id, $pledge): stdClass{

    //validation check
    if(empty($pledge->contid)){
      throw new Exception("Contact required for pledge");
    }
    $this->validatePositiveAmount($pledge->amount);

    if(empty($pledge->date_)){
      throw new Exception("Date required for pledge");
    }

    if(empty($pledge->description)){
      throw new Exception("Description required for pledge");
    }

    $pledgeId = $this->_dataManager->db->fetchOne("SELECT private.cf_save_pledge(:fund_id, CAST(:json AS JSON))", ['fund_id' => $fund_id, 'json' => json_encode($pledge)]);
    $data = $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $pledgeId]);

    $response = new stdClass();
    $response->success = false;

    if($data){
      $response->pledge = json_decode($data);
      $response->success = true;
    }
    return $response;
  }

  /**
   * @throws Exception
   */
  public function getPledge($fund_id, $trankeygen_id):stdClass {
    $data = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_get_pledge(:fund_id, :id)",
      ['fund_id' => $fund_id, 'id' => $trankeygen_id]
    );
    $response = new stdClass();
    if ($data) {
      $response->{'pledge'} = json_decode($data);
      $response->success = true;
    }
    else {
      $response->success = false;
    }
    return $response;
  }

  /**
   * @throws \Exception
   */
  public function savePledgeCancel($fund_id, $pledgeCancel):stdClass
  {
    $response = new stdClass();
    if (empty($pledgeCancel->date_)) {
      throw new Exception("Cancelled pledge requires date");
    }

    $this->validatePositiveAmount($pledgeCancel->amount);

    $pledgeCancel_Id = $this->_dataManager->db->fetchOne("SELECT private.cf_save_cancel_pledge(:fund_id, CAST(:json AS JSON))", ['fund_id' => $fund_id, 'json' => json_encode($pledgeCancel)]);
    $data =  $this->_dataManager->db->fetchOne("SELECT private.cf_get_transaction(:fund_id, :trankeygen_id)", ['fund_id' => $fund_id, 'trankeygen_id' => $pledgeCancel_Id]);

    if ($data) {
      $response->{'pledge-cancel'} = json_decode($data);
      $response->success = true;
    }

    return $response;
  }

  /**
   * Removes a pledge record and the accounts with it
   * @param $fund_id
   *   The id of the fund so that we ensure we don't delete across funds
   * @param $receipt_id
   *   trankeygen_id for the receipt record for the pledge fund
   * @return stdClass returns true|false
   *   returns true|false
   * @throws Exception
   */
  public function deletePledge($fund_id, $receipt_id):stdClass
  {
    if (empty($receipt_id)) {
      throw new Exception("Delete pledge requires a receipt_id");
    }

    $this->_dataManager->db->fetchOne("SELECT private.cf_delete_pledge(trankeygen_id) from private.vreceipts where fund_id=:fund_id and trankeygen_id=:pid", ['fund_id' => $fund_id, 'pid' => $receipt_id]);

    $response = new stdClass();
    $response->success = true;
    return $response;
  }
}
