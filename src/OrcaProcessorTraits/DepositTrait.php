<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use Doctrine\DBAL\Exception;
use stdClass;

trait DepositTrait {
  /**
   * @throws Exception
   */
  public function saveDeposit($fund_id, stdClass $deposit) {
    $response = new stdClass();

    $deposit_id = $this->_dataManager->db->fetchOne(
      "SELECT private.cf_save_depositevent(fund_id, CAST(:json_data as JSON)) FROM fund where fund_id=:fund_id",
      ['fund_id' => $fund_id, json_encode($deposit), 'json_data' => json_encode($deposit)]
    );
    $response->depositEvent = $this->_dataManager->db->fetchOne("select cast(private.cf_get_depositevent(:deposit_id) as text)", ['deposit_id' => $deposit_id]);
    if ($response->depositEvent) {
      $response->depositEvent = json_decode($response->depositEvent);
    }
    $response->success = !empty($deposit_id);
    return $response;
  }

  /**
   * Retrieve a deposit and return the event
   * @param $fund_id
   *   Fund to retrieve deposit for
   * @param $deposit_id
   *   ID of deposit to retrieve
   * @return stdClass
   * @throws Exception
   */
  public function getDeposit($fund_id, $deposit_id) {
    $response = new stdClass();
    $response->depositEvent = $this->_dataManager->db->fetchOne("select cast(private.cf_get_depositevent(trankeygen_id) as text) from private.vdepositevents where fund_id = :fund_id and trankeygen_id=:deposit_id", ['fund_id' => $fund_id, 'deposit_id' => $deposit_id]);
    $response->depositEvent = json_decode($response->depositEvent);
    $response->success = true;
    return $response;
  }

  /**
   * @throws Exception
   */
  public function deleteDeposit($fund_id, $deposit_id) {
    $response = new stdClass();
    $this->_dataManager->db->executeStatement(
      "
       select private.cf_delete_depositevent(trankeygen_id)
       from private.vdepositevents de
       where de.fund_id = :fund_id and de.trankeygen_id=:deposit_id
      ",
      ['fund_id' => $fund_id, 'deposit_id' => $deposit_id]
    );
    $response->success = true;
    return $response;
  }
}
