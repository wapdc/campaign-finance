<?php

namespace WAPDC\CampaignFinance\OrcaProcessorTraits;

use DateTime;
use stdClass;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use WAPDC\CampaignFinance\Model\CommitteeLog;
use WAPDC\CampaignFinance\CommercialAWSFileService;

trait CampaignTrait {
  /**
   * @param $committee_id
   * @param $parameters stdClass
   *  election_code (required),
   *  campaignStartDate
   *  carryForwardAmount: carry forward cash for campaign
   *  election_codes: list of election codes campaign expects to participate in
   * @return int
   * @throws \Exception
   */
  public function createOrcaCampaign($committee_id, stdClass $parameters): int
  {
    $election_code = $parameters->election_code;
    $json_parameters = json_encode($parameters);
    $fund_id= $this->fundProcessor->ensureCommitteeFundAndVendor($committee_id, $election_code);
    if (isset($parameters->carryForwardAmount) && !is_numeric($parameters->carryForwardAmount)) {
      throw new Exception('Carry forward amount must be a number.');
    }
    $this->_dataManager->db->executeStatement('select * from private.cf_create_campaign(:fund_id, cast(:json as json))',
      ['fund_id' => $fund_id, 'json' => $json_parameters]);
    return $fund_id;
  }

  /**
   *
   * {
   * "campaign_start_date": "2021-02-21",
   * "itemize_all": 0,
   * "carryforward":
   *  [
   *   { "account_id": 12345, "amount": 10000.00, "memo": "This is a memo" }
   *  ],
   * "election_participation": ['2021', '2021P']
   * }
   *
   * @param $fund_id
   * @param $json
   * @return stdClass
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function saveCampaignSettings($fund_id, $json): stdClass
  {
    $response = new stdClass();
    $response->success = true;
    $response->properties = [];

    $repopulate_reporting_periods = false;
    $json = json_decode($json);
    $fund = (object)$this->_dataManager->db->executeQuery("select f.election_code as election_year, c.start_year, c.continuing, c.election_code, c.pac_type from fund f join committee c on f.committee_id = c.committee_id where fund_id = :fund_id", ["fund_id" => $fund_id])->fetchAssociative();
    if (empty($fund)) {
      $response->success = false;
      $response->error = 'Fund not found for fund_id '. $fund_id .'.';
    } else {
      if (!empty($json->campaign_start_date)) {
        $this->validateCampaignStartDate($fund->election_year, $fund->start_year, $fund->continuing, $fund->election_code, $json->campaign_start_date, $fund->pac_type == 'surplus');
        $csd = new DateTime($json->campaign_start_date);
        $new_prop = $this->generateProperty('CAMPAIGNINFO:STARTDATE', $csd->format('m/d/Y'));
        $this->_dataManager->db->executeQuery("select * from private.cf_save_submission_properties(:fund_id, :json)", ["fund_id" => $fund_id, "json" => json_encode([$new_prop])]);
        $repopulate_reporting_periods = true;
        $response->properties[] = $new_prop;
      }
      if (isset($json->election_codes)) {
        $election_participation = $this->_dataManager->db->executeQuery(
          "select * from private.cf_save_election_codes(:fund_id, :json_data)",
          ["fund_id" => $fund_id, "json_data" => json_encode($json)]
        )->fetchOne();
        $repopulate_reporting_periods = true;
        $response->properties[] = $this->getBallotTypeFromElectionCodes($election_participation);
      }
      if (isset($json->itemize_always)) {
        $new_prop = $this->generateProperty('OPTION:ORCA.UTIL.ERRORLIST.ITEMIZEMISSCONTINFO', json_encode($json->itemize_always));
        $this->_dataManager->db->executeQuery("select * from private.cf_save_submission_properties(:fund_id, :json)", ["fund_id" => $fund_id, "json" => json_encode([$new_prop])]);
        $response->properties[] = $new_prop;
      }
      if ($repopulate_reporting_periods) {
        $this->_dataManager->db->executeStatement("select private.cf_populate_reporting_obligations(:fund_id)", ["fund_id" => $fund_id]);
      }
      if (isset($json->carry_forward)) {
        $response->carry_forward = [];
        foreach ($json->carry_forward as $carryForward) {
          $carryForward->cid = $carryForward->cid??$carryForward->account_trankeygen;
          $carryForward->amount = $carryForward->amount??0;
          array_push($response->carry_forward, $this->updateCarryForward($fund_id, $carryForward));
        }
      }
    }
    return $response;
  }

  /**
   * @throws Exception
   */
  public function updateCampaignVendor($fund_id, $vendor) {
    $this->_dataManager->db->executeQuery("select private.cf_update_campaign_vendor(:p_fund_id, :p_vendor)", ['p_fund_id' => $fund_id, 'p_vendor' => $vendor->vendor]);
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function getCampaignRestore($fund_id) {
    $json = $this->_dataManager->db->executeQuery(
      "SELECT private.cf_get_orca_campaign_restore(:fund_id)::TEXT",
      ['fund_id' => $fund_id]
    )->fetchOne();
    return $json;
  }

  /**
   * @param $election_year
   * @param $start_year
   * @param $continuing
   * @param $election_code
   * @param $campaignStartDate
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function validateCampaignStartDate($election_year, $start_year, $continuing, $election_code, $campaignStartDate, $surplus)
  {
    $fundStartDate = $election_year . '-01-01';
    $fundEndDate = $election_year . '-12-31';
    $committeeStartDate = $start_year. '-01-01';

    if ($surplus) {
      if ($campaignStartDate < $committeeStartDate) {
        throw new Exception( 'The start date must fall during or after the committee start year of '. $start_year);
      }
    }
    else if($continuing) {
      if ($start_year == $election_year) {
        if(!(($campaignStartDate >= $fundStartDate) && ($campaignStartDate <= $fundEndDate))){
          throw new Exception('The start date must be a date that is in ' . $election_year );
        }
      }
      else {
        if(!($campaignStartDate == $fundStartDate)){
          throw new Exception('The start date must be January 1st of '. $election_year);
        }
      }
    } else {
      $election_date = $this->_dataManager->db->executeQuery(
        "select election_date from election
            where election_code = :election_code", ["election_code" => $election_code])->fetchOne();

      if(!($campaignStartDate <= $election_date)){
        throw new Exception('The campaign start date must be prior to the date of the election ('. $election_date . ')' );
      }
    }
  }

  /**
   * Upgrade an orca fund to current version.
   *
   * @param int $fund_id
   * @return void
   * @throws Exception
   */
  public function upgradeCampaign(int $fund_id) {
    $this->_dataManager->db->executeStatement("SELECT private.cf_upgrade_campaign(:fund_id)", ['fund_id' => $fund_id]);
  }

  /**
   * @throws Exception
   */
  public function deleteCampaign($fund_id) {
    $this->_dataManager->db->executeQuery("select private.cf_delete_orca_campaign(:p_fund_id)", ['p_fund_id' => $fund_id]);
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function updateCarryForward($fund_id, $request_data)
  {
    return json_decode($this->_dataManager->db->executeQuery(
      'select * from private.cf_update_carry_forward(:fund_id, :amount, :memo, :bank_trankeygen_id)',
      [
        'fund_id' => $fund_id,
        'amount' => $request_data->amount,
        'memo' => $request_data->memo,
        'bank_trankeygen_id' => $request_data->cid
      ]
    )->fetchOne());
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws Exception
   */
  public function validateSave(int $fund_id, $save_count) {
    $result = $this->_dataManager->db->executeQuery("SELECT private.cf_validate_save(:fund_id, :save_count)",
      ['fund_id' => $fund_id, 'save_count' => $save_count] )->fetchOne();
    return (int) $result;
  }

  /**
   * @throws OptimisticLockException
   * @throws ORMException
   * @throws \Exception
   */
  public function restoreCampaign($fund_id, $payload, $user): stdClass {
    $response = new stdClass();
    $response->success = false;

    $this->createRestoreCommitteeLog($fund_id, $payload, $user);
    $restore_request_id = $this->createRestoreRequestEntry($fund_id, $user);
    $file_name = "restore-$restore_request_id";

    $stage = $_ENV['PANTHEON_ENVIRONMENT'] === 'live' ? 'prod' : 'demo';

      $awsFileService = new CommercialAWSFileService();
      $awsFileService->setBucket("campaign-finance-$stage-restore");

      $results = $awsFileService->getFilePostFormElements($file_name, 'private', 'binary/octet-stream');

    if ($results){
        $response->success = true;
        $response->data = $results;
    }

    return $response;
  }

  /**
   * @throws OptimisticLockException
   * @throws ORMException
   */
  public function createRestoreCommitteeLog($fund_id, $payload, $user): void {
    $log = new CommitteeLog();
    $log->committee_id = $payload->committee_id;
    $log->transaction_type = 'fund';
    $log->transaction_id = $fund_id;
    $log->user_name = $user->user_name;
    $log->action = 'Restore Requested';
    $log->message = "{$user->user_name} requested a restore";
    $log->updated_at = new DateTime();
    $this->_dataManager->em->persist($log);
    $this->_dataManager->em->flush($log);
  }

  /**
   * @throws Exception
   */
  public function createRestoreRequestEntry($fund_id, $user): int {
    $validate = 'false';

    $this->_dataManager->db->executeQuery(
      'insert into private.restore_request(fund_id, email, validate) values (:fund_id, :email, :validate)',
      ['fund_id' => $fund_id, 'email' => $user->user_name, 'validate' => $validate]
    );

    return $this->_dataManager->db->lastInsertId();
  }

  /**
   * @param $contact_id
   * @return void
   * @throws Exception
   */
  public function recalculateContactAggregates($contact_id) {
    $this->_dataManager->db->executeStatement('select * from private.cf_update_aggregates(:contact_id)', ['contact_id' => $contact_id]);
  }
}
