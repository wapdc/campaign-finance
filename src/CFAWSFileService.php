<?php


namespace WAPDC\CampaignFinance;


use WAPDC\Core\AWSFileServiceBase;

class CFAWSFileService extends AWSFileServiceBase {


  /**
   * The values of the public and private bucket keys contain
   * The name of the environment variable that contains the bucket name.
   */
  const CONFIG = [
    'region' => 'us-gov-west-1',
    'version' => 'latest',
    'buckets' => [
      'public' => 'AWS_CF_PUBLIC_BUCKET',
      'private' => 'AWS_CF_PRIVATE_BUCKET'
    ],
    'accessModes' => [
      'public' => 'public-read',
      'private' => 'private'
    ],
  ];
}