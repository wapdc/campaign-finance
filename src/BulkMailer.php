<?php


namespace WAPDC\CampaignFinance;


use WAPDC\Core\SendGrid\MailerInterface;

/**
 * Class BulkMailer
 * Manages sending mail large groups of folks.
 */
class BulkMailer {

  protected $mailer;

  public function __construct(MailerInterface $mailer) {
    $this->mailer = $mailer;
  }

  /**
   * Performs a mail merge based data passed.
   *
   * @param array $data
   *   Array of objects containing mail data.
   * @param string $template_id
   *   The id of the SendGrid template we need.
   * @return array
   *   Emails sent successfully
   * @throws \Exception
   */
  public function mailMerge(array $data, $options) {
    if (empty($options->template_id)) {
      throw new \Exception('Missing Template');
    }

    $this->mailer->setMessageTemplate($options->template_id);

    $email_field = 'email';
    $name_field = 'name';

    if (!empty($options->email_field)) {
      $email_field = $options->email_field;
    }

    if (!empty($options->name_field)) {
      $name_field = $options->name_field;
    }

    $email_sent = [];
    $sends = array_chunk($data, 500);
    foreach ($sends as $send) {
      foreach($send as $row) {
        $email = $row->$email_field;
        $name = $row->$name_field ?? '';
        $replace_data = get_object_vars($row);
        unset($replace_data['email']);
        if ($email) {
          $this->mailer->addRecipient($email, $name, $replace_data);
          $email_sent[] = $email;
        }
      }
      $this->mailer->send();
    }

    return $email_sent;

  }

}