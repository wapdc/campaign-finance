<?php


namespace WAPDC\CampaignFinance;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\ORMException;
use stdClass;


class VendorSubmissionProcessor {

  /** @var \WAPDC\CampaignFinance\CFDataManager $_dataManager */
  private $_dataManager;

  /** @var FundProcessor */
  private $fundProcessor;
  private OrcaProcessor $orcaProcessor;

  private string $project_directory = '.';

  public function __construct(CFDataManager $dataManager) {
    $this->_dataManager = $dataManager;
    $this->fundProcessor = new FundProcessor($dataManager);
    $this->orcaProcessor = new OrcaProcessor($dataManager);
    $this->project_directory = dirname(__DIR__);
  }

  /**
   * Given a draft_id, a committee_id and an ip address this will create a submission record and create a vendor report.
   * If create committee report successfully returns a report_id, then the draft_report record will be deleted.
   *
   * @param $draft_id
   * @param $committee_id
   * @param $ip_address
   * @param $payload
   * @return stdClass
   * @throws Exception
   * @throws DBALException
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws ORMException
   */
  public function submitVendorReport($draft_id, $committee_id, $ip_address, $payload): stdClass
  {
    $draftReport = (object) $this->_dataManager->db->fetchAssociative('select * from private.draft_report where draft_id = :id', ['id' => $draft_id]);
    $reportData = $this->orcaProcessor->generateReportPreview($draftReport->fund_id, json_decode($draftReport->user_data), json_decode($draftReport->metadata)->submission_version);
    $metadata = json_decode($draftReport->metadata);
    $version = $metadata->submission_version;
    //add metadata to store in submission from the vendor
    $reportData->user_data->metadata = $metadata;
    //add new treasurer data
    $reportData->user_data->treasurer = $payload->treasurer;
    $vendor = $metadata->vendor_name;
    $emails = implode(';', $metadata->notification_emails ?? []);
    $financeReportingProcessor = CampaignFinance::service()->getFinanceReportingProcessor();

    $submission_id = $financeReportingProcessor->logSubmission('json', json_encode($reportData->user_data), $vendor, $version, $committee_id, $ip_address, $version);
    $response = $this->fundProcessor->submitCommitteeReport($committee_id, $reportData->user_data, $emails, $vendor, $version);
    if ($response->report_id) {
      $this->_dataManager->db->executeQuery('delete from private.draft_report where draft_id = :draft_id', ['draft_id' => $draft_id]);
    }
    $financeReportingProcessor->logProcessResults($submission_id, $response->success, $response->message, $response->report_id ?? NULL, $response->fund_id ?? NULL);

    return $response;
  }

  /**
   * Provide a JSON aggregated list of draft reports that have
   * been submitted for a committee.
   *
   * @param $committee_id int
   *   The committee id that we would like the drafts for.
   * @return stdClass[]
   *   List of all report drafts in the database.
   */
  public function listDraftReports($committee_id) {
    // The following query is careful to cast the JSON that is being stored in the database
    $result = $this->_dataManager->db->fetchOne("select json_agg(json_build_object(
      'fund_id', dr.fund_id,
      'draft_id', dr.draft_id, 
      'report_type', dr.report_type,
      'period_start', dr.period_start,
      'period_end', dr.period_end,
      'external_id', dr.external_id, 
      'received_date', dr.received_date, 
      'messages', CAST(dr.messages as JSON), 
      'processed', dr.processed,
      'expire_date', dr.expire_date, 
      'view_only', dr.view_only
    ))
      from fund f
        join private.draft_report dr on dr.fund_id = f.fund_id
      where f.committee_id = :committee_id
        and dr.expire_date > now()
        and dr.view_only = false", ['committee_id' => $committee_id]);
    return empty($result) ? [] : json_decode($result);
  }

  // this function does not check for null values and assumes that $value is not null
  protected function checkForTypeError($key, $value, string $type, string $of): string {
    // check if the type matches the expected type
    if (gettype($value) !== $type) {
      return 'Key {' . $key . '} is a(n) ' .
             gettype($value) . ' and not an expected ' .
             $type . ' type';
    }

    // check if within an array type, that the first level matches the secondary $of type
    if ($type === 'array') {
      foreach ($value as $item) {
        if (gettype($item) !== $of) {
          return 'Item in array key {' . $key . '} is a(n) ' .
                 gettype($item) . ' and not an expected ' .
                 $of . ' type';
        }
      }
    }

    return '';
  }

  /**
   * Takes the stdClass representing the vendor report json payload and throws exceptions if it finds unexpected values
   *
   * @param $json stdClass representing vendor submission json
   * @throws \Exception
   */

  public function validateDraftReportStructure(stdClass $json) {

    // Minimally required properties
    if (!$json->metadata)
      throw new \Exception('Invalid JSON: ' . 'Missing metadata property');
    if (!$json->report)
      throw new \Exception('Invalid JSON: ' . 'Missing report property');
    if (!$json->report->report_type)
      throw new \Exception('Invalid JSON: ' . 'Missing report->report_type property');
    if (!$json->report->period_start)
      throw new \Exception('Invalid JSON: ' . 'Missing report->period_start property');
    if (!$json->report->period_end)
      throw new \Exception('Invalid JSON: ' . 'Missing report->period_end property');

    foreach ($json as $key => $value) {
      switch ($key) {
        case 'report':
          foreach ($value as $k => $v) {
            switch ($k) {
              case 'expenses':
              case 'loan':
              case 'attachments':
              case 'auctions':
              case 'contacts':
              case 'contribution_corrections':
              case 'contributions':
              case 'contributions_inkind':
              case 'debt':
              case 'expense_corrections':
              case 'expense_refunds':
              case 'external_id':
              case 'final_report':
              case 'loans':
              case 'misc':
              case 'misc_receipts':
              case 'period_end':
              case 'period_start':
              case 'pledge':
              case 'report_type':
              case 'sums':
                break;
              default:
                throw new \Exception('Unexpected json input for report: ' . $k);
            }
          }
          break;
        case 'metadata':
          foreach ($value as $k => $v) {
            // test for existence
            switch ($k) {
              case 'notification_emails':
              case 'submission_version':
              case 'vendor_contacts':
              case 'vendor_name':
                break;
              default:
                throw new \Exception('Unexpected json input for metadata: ' . $k);
            }
            // test for types
            switch ($k) {
              case 'vendor_contacts':
                $error = $this->checkForTypeError($k, $v, 'array', 'string');
                if ($error !== '') throw new \Exception($error);
              case 'notification_emails':
                $error = $this->checkForTypeError($k, $v, 'array', 'string');
                if ($error !== '') throw new \Exception($error);
            }
          }
          break;
        default:
          throw new \Exception('Unexpected json input: ' . $key);
      }
    }
  }

  /**
   * Save and validate a draft report
   *
   * @param $committee_id
   *  Committee that is submitting draft report
   * @param $json
   *  stdClass that represents the report and the metadata (most likely submitted by vendors)
   * @return stdClass
   *  An object containing the draft_id as well as the report validation results
   *  {
   *    draft_id: int,
   *    validation: {
   *      success: bool,
   *      messages: [array of relevant validation message strings]
   *    }
   *  }
   * @throws Exception
   */
  public function saveDraftReport($committee_id, stdClass $json, $view_only): stdClass
  {
    $response = new stdClass();
    $success = true;
    $messages = [];
    $committee = (object) $this->_dataManager->db->fetchAssociative('select * from committee where committee_id = :id', ['id' => $committee_id]);
    $election_code = null;
    $reportYear = date('Y', strtotime($json->report->period_start));
    if ($committee->continuing) {
      if ($reportYear < $committee->start_year && $committee->election_code) {
        // If report period before continuing start and has an election_dode it started out as single year. Post to fund for election_code.
        $election_code = substr($committee->election_code, 0, 4);
      } else {
        $election_code = $reportYear;
      }
    } else {
      $election_code = substr($committee->election_code, 0, 4);
    }
    try {
      $this->fundProcessor->ensureCommitteeFundAndVendor($committee_id, $election_code, $json->metadata->vendor_name);
    } catch (\Exception $e) {
      $success = false;
      $messages['committee.unverified'] = $e->getMessage();
    }
    $draft_id = $this->_dataManager->db->fetchOne('select private.cf_save_draft_report(:committee_id, :json, :view_only)', ['committee_id' => $committee_id, 'json' => json_encode($json), 'view_only' => $view_only],
      ['committee_id' => \PDO::PARAM_INT, 'json' => \PDO::PARAM_STR, 'view_only' => \PDO::PARAM_BOOL]);
    $response->draft_id = $draft_id;
    $draftReport = (object) $this->_dataManager->db->fetchAssociative('select * from private.draft_report where draft_id = :id', ['id' => $draft_id]);
    if(empty($json->report->external_id) && $json->report->report_type === 'c3') {
        $success = false;
        $messages['external.id.required'] = 'report external id required';
    }
    //validate metadata
    if (empty($json->metadata) || !isset($json->metadata)) {
      $success = false;
      $messages['metadata.required'] = 'report metadata required';
    } else {
      if (empty($json->metadata->vendor_name) || !isset($json->metadata->vendor_name)) {
        $success = false;
        $messages['vendor.name.required'] = 'vendor name required';
      }
      if (empty($json->metadata->vendor_contacts) || !is_array($json->metadata->vendor_contacts)) {
        $success = false;
        $messages['vendor.contacts.required'] = 'vendor contact required';
      }
    }

    $version = $json->metadata->submission_version ?? null;
    $response->validation = $this->fundProcessor->validateCommitteeReport($committee_id, json_decode($draftReport->user_data), $version);
    if ($response->validation->messages) {
      $response->validation->messages += $messages;
    }
    else {
      $response->validation->messages = $messages;
    }
    if (!$success) {
      $response->validation->success = false;
    }
    unset($response->validation->metadata);
    unset($response->validation->message);
    if (empty($response->validation->messages)) {
      $response->validation->messages = null;
      $messageType = \PDO::PARAM_NULL;
    }
    else {
      $messageType = \PDO::PARAM_STR;
    }
    $this->_dataManager->db->executeQuery('update private.draft_report set processed = :processed, messages = :messages where draft_id = :id', ['processed' => $response->validation->success, 'id' => $draft_id, 'messages' => json_encode($response->validation->messages)],
      ['processed' => \PDO::PARAM_BOOL, 'id' => \PDO::PARAM_INT, 'messages' => $messageType]);
    return $response;
  }


  /**
   *  Delete a draft report
   * @return stdClass
   * @throws Exception
   */
  public function deleteDraftReport($draft_id): stdClass {
    $response = new stdClass();
    $response->success = false;
    $result = $this->_dataManager->db->executeQuery('delete from private.draft_report where draft_id = :draft_id', ['draft_id' => $draft_id], ['draft_id' => \PDO::PARAM_INT]);
    if ($result) {
      $response->success = true;
    }
    return $response;
  }

  /**
   * Generates data for a report draft
   *
   * @param $draft_id
   * @return stdClass
   * @throws \Doctrine\DBAL\Driver\Exception|Exception
   */
  public function getDraftReport($draft_id): stdClass
  {
    $draftReport = (object) $this->_dataManager->db->fetchAssociative('select * from private.draft_report where draft_id = :id', ['id' => $draft_id]);

    $response = $this->orcaProcessor->generateReportPreview($draftReport->fund_id, json_decode($draftReport->user_data), json_decode($draftReport->metadata)->submission_version);
    $response->processed = $draftReport->processed;
    $response->view_only = $draftReport->view_only;
    $response->metadata = (object) (array_merge( (array)json_decode($draftReport->metadata), (array)$response->metadata));
    return $response;
  }

  /**
   * @param $committee_id
   * @return object
   * @throws Exception
   */
  public function getVendorCommitteeInfo($committee_id): object
  {
    $response = new stdClass();
    $dir = $this->project_directory . '/data/public/';
    $file = 'get-committee-info.sql';
    if (!file_exists("$dir/$file")) {
      throw new Exception("Invalid Query: $dir/$file");
    }
    $parameters = ['committee_id' => $committee_id];
    $results = $this->_dataManager->executeQueryFromFile($dir, $file, $parameters);

    if (!$results || !$results[0]) {
      throw new Exception('Failed loading committee');
    }
    $info = (object) $results[0];
    $info->candidacy = json_decode($info->candidacy);
    $info->officers = json_decode($info->officers);
    $info->contact = json_decode($info->contact);
    $response->committee = $info;
    return $response;
  }

  /**
   * retrieve a list of contributor types
   *
   * @return array[] array of contributor types
   * @throws Exception
   */
  public function getContributorTypes(): array
  {
    return $this->_dataManager->db->fetchAllAssociative("select * from contributor_type where type_code != 'CPL'");
  }

  /**
   * retrieve a list of expense categories
   *
   * @return array[] array of expense categories
   * @throws Exception
   */
  public function getExpenseCategories(): array
  {
    return $this->_dataManager->db->fetchAllAssociative('
      select code, label, help_text, has_ad, cat_lookup, case when requires_description is false then label end as default_description 
      from expense_category');
  }

  /**
   * retrieve a list of occupations
   *
   * @return array[] array of occupations
   * @throws Exception
   */
  public function getOccupations(): array
  {
    return $this->_dataManager->db->fetchAllAssociative('select * from ui_occupation_list');
  }

  /**
   * @param $draft_id
   * @param $user
   * @return bool
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Exception
   */
  public function draftReportAuthorizationCheck($draft_id, $user): bool
  {
    $q = "select f.committee_id from private.draft_report r join fund f on r.fund_id = f.fund_id where draft_id=?";
    $committee_id = $this->_dataManager->db->executeQuery($q, [$draft_id])->fetchFirstColumn()[0];
    return CampaignFinance::service()->CommitteeAuthorizationCheck($committee_id, $user, 'owner');
  }
}
