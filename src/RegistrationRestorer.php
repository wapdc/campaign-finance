<?php


namespace WAPDC\CampaignFinance;

use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\CommitteeAPIAuthorization;
use WAPDC\CampaignFinance\Model\CommitteeAuthorization;
use WAPDC\CampaignFinance\Model\Registration;

/**
 * Class RegistrationRestorer
 * I have deemed this restorer too difficult to support
 * test coverage since we don't want to maintain a restore
 * instance indefinitely. See the devops project for how
 * to restore database to point in time.
 */
class RegistrationRestorer
{
  protected $dm;
  protected $rm;

  public function __construct(CFDataManager $dataManager, RestoreDataManager $restoreDataManager) {
    $this->dm = $dataManager;
    $this->rm = $restoreDataManager;
  }

  /**
   * Restore committees that are attached to a list of registrations
   * @param array|int $registration_ids
   *   list of ids to try and restore
   *
   * @throws \Exception
   */
  public function restoreCommitteesByRegistrations( $registration_ids) {
    $rm = $this->rm->em;
    $dm = $this->dm->em;
    // cast as array so that we can send just one.
    $registration_ids = (array)$registration_ids;

    foreach($registration_ids as $registration_id) {
      /** @var Registration $registration */
      $registration = $rm->find(Registration::class, $registration_id);
      if (!$registration) {
        echo "Could not find registration id: $registration_id\n";
      }
      else {
        /** @var Committee $committee */
        $committee = $rm->find(Committee::class, $registration->committee_id);
        $name = $committee->name;
        echo "Restoring $name\n";
        $existing_committee = $dm->find(Committee::class, $committee->committee_id);
        if (!$existing_committee) {
          $dm->persist($committee);
          $dm->flush($committee);
        }


        /** @var Registration[] $r_registrations */
        $r_registrations = $rm->getRepository(Registration::class)
          ->findBy(['committee_id' => $committee->committee_id]);
        $count = 0;
        foreach($r_registrations as $r) {
          if (!$dm->find(Registration::class, $r->registration_id)) {
            $count++;
            $dm->persist($r);
            $dm->flush($r);
          }
        }
        echo "--Restored $count Registrations\n";

        /** @var CommitteeAuthorization[] $auths */
        $auths = $rm->getRepository(CommitteeAuthorization::class)
          ->findBy(['committee_id' => $committee->committee_id]);

        $count = 0;
        foreach ($auths as $auth) {
          if (!$dm->getRepository(CommitteeAuthorization::class)
            ->findOneBy(
              ['committee_id' => $auth->committee_id,
               'realm' => $auth->realm,
                'uid' => $auth->uid])
          ) {
            $count++;
            $dm->persist($auth);
            $dm->flush($auth);
          }
        }
        echo "--Restored $count User Authorizations\n";

        /** @var CommitteeAPIAuthorization[] $keys */
        $keys = $rm->getRepository(CommitteeAPIAuthorization::class)
          ->findBy(['committee_id' => $committee->committee_id]);
        $count = 0;
        foreach ($keys as $key) {
          if (!$dm->find(CommitteeAPIAuthorization::class, $key->api_key_id)) {
            $dm->persist($key);
            $dm->flush($key);
            $count++;
          }
        }

        echo "--Restored $count Committee API Keys\n";
      }
    }

  }
}