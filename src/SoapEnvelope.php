<?php


namespace WAPDC\CampaignFinance;


class SoapEnvelope {

  /** @var \SimpleXMLElement */
  private $xml;

  private $method;

  public const SOAP12_NS = 'http://www.w3.org/2003/05/soap-envelope';

  public function __construct($method) {
    $this->method = $method;
    $xmlDoc = '<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <SubmitResponse xmlns="http://tempuri.org/">
    </SubmitResponse>
  </soap12:Body>
</soap12:Envelope>';

    $this->xml = new \SimpleXMLElement($xmlDoc);
    $this->xml->registerXPathNamespace('soap12', SoapEnvelope::SOAP12_NS);
  }

  public function setSubmitResult($result) {

    $nodes = $this->xml->xpath('//soap12:Body');
    $body = $nodes[0];
    $submitResponse = $body->SubmitResponse;
    $name = $this->method . 'Result';
    $submitResponse->$name =  $result;
  }

  public function asXML() {
    return $this->xml->asXML();
  }

  public function setError($message, $class = "ServerError") {
    $time = new \DateTime();
    $xmlDoc = '<?xml version="1.0" encoding="utf-8"?><Exception></Exception>';
    $xml = new \SimpleXMLElement($xmlDoc);
    $xml->Message = $message;
    $xml->Exception = '';
    $xml->Exception['class'] = $class;
    $xml->Exception['time'] = $time->format('YmdHis');
    $this->setSubmitResult($xml->asXML());
  }

  public function setResponse($response) {
    $xmlDoc = '<?xml version="1.0" encoding="utf-8"?><Response></Response>';
    $xml = new \SimpleXMLElement($xmlDoc);
    foreach ($response as $key => $value) {
      $xml->$key = $value;
    }
    $this->setSubmitResult($xml->asXML());
  }

  public function setVersionUpdateResponse($updates) {
    $xmlDoc = '<?xml version="1.0" encoding="utf-8"?><updates></updates>';
    $xml = new \SimpleXMLElement($xmlDoc);

    foreach ($updates as $row) {
      if ($row['mandatory_update'] == TRUE){
        $mandatory_value = 'true';
      } else {
        $mandatory_value = 'false';
      }
      $update = $xml->addChild('update');
      $update->addAttribute('version', $row['version']);
      $update->addAttribute('mandatory', $mandatory_value);
      $lines = explode("\n", $row['fix'] );
      foreach($lines as $line) {
        $update->addChild('fix', $line);
      }
    }
    $this->setSubmitResult($xml->asXML());
  }

  /**
   * @param $xrfZipBase64
   *
   * @return false|string
   */
  public function extractXml($xrfZipBase64) {
    $zip = new \ZipArchive;
    $decodedZip = base64_decode($xrfZipBase64);

    $filename = tempnam(sys_get_temp_dir(), 'ORC');
    $fp = fopen($filename, "w");
    fwrite($fp, $decodedZip);
    fclose($fp);

    $res = $zip->open($filename);

    if ($res === TRUE) {
      $xml_filename = $zip->getNameIndex(0);
      $xml = $zip->getFromName($xml_filename);
      $xml = $this->sanitizeXml($xml);
    }
    else {
      // TODO: RETURN A VALID SOAP ERROR MESSAGE TO ORCA
      $xml = '';
    }
    return $xml;
  }

  function sanitizeXml($xml) {
    // strip non-ascii characters - https://alvinalexander.com/php/how-to-remove-non-printable-characters-in-string-regex/
    // exclude ascii x0A to allow line break special character.
    return preg_replace('/[\x00-\x09\x0B-\x1F\x80-\xFF]/', '', $xml);
  }

}