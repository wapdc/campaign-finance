<?php


namespace WAPDC\CampaignFinance;

/**
 * Class SubmissionDiffTool
 *
 * Deeply compares php stdClass style objects so that  you can determine the differences between
 * two submissions.
 */
class SubmissionDiffTool
{
  /** @var boolean Are objects the same */
  protected $same;

  protected $note_key = 'alteration';

  protected $log = [];

  /**
   * Mark the object with a change indicator.
   * @param $o
   * @param string $mark
   */
  protected function mark(&$o, $prefix, $mark='change') {
    $alt = $this->note_key;
    $this->same = false;
    if (empty($o->$mark)) {
      $o->$alt = $mark;
      $this->log[$prefix] = $mark;
    }
  }

  /**
   * Compare two arrays.
   * @param $a
   * @param $b
   * @param string $prefix
   *   label prefix for change log.
   */
  protected function compareArray(&$a, &$b, $prefix) {
    if (count($a) == 0 && count($b) == 0) {
      return;
    }
    else {
      // compare elements up to the length
      foreach($a as $k => $v) {
        if (is_array($v)) {
          $this->compareArray($a, $b, "$prefix.$k");
        }
        else {
          $this->compareObject($a[$k], $b[$k], "$prefix.$k");
        }

      };
      if (count($a) < count($b)) {
        for ($i=count($a); $i < count($b); $i++) {
          $a[$i] = $b[$i];
          $this->mark($a[$i], "$prefix.$i", 'remove');
        }
      }
    }
  }

  /**
   * Deeply compare two objects.
   *
   * @param mixed $a
   *   The first object to compare.
   * @param mixed $b
   *   The second object to compare
   * @param string $prefix
   *
   */
  protected function compareObject(&$a, &$b, $prefix) {
    // Iterate key value pairs using object _a as a reference.
    $propsScanned = [];

    // Go through properties in a and deep compare using recursion.
    foreach ($a as $key => $value) {
      $propsScanned[$key] = true;
      switch (TRUE) {
        case (is_object($value)):
          $this->compareObject($a->$key, $b->$key, "$prefix.$key");
          break;
        case (is_array($value)):
          $this->compareArray($a->$key, $b->$key, "$prefix.$key");
          break;
        case ($b === NULL):
          $this->mark($a, $prefix, 'add');
          break;
        default:
          if (isset($b->$key)) {
            if ($b->$key !== $a->$key) {
              $this->mark($a, $prefix, 'change');
            }
          }
          else {
            if (isset($a->$key)) {
              $this->mark($a, $prefix, 'change');
            }
          }
      }
    }

    // Go through properties in b that are not in a and deep compare them.
    if ($b) foreach ($b as $key => $value) {
      if (empty($propsScanned[$key])) {
        switch (TRUE) {
          case (is_object($value)):
            $this->compareObject($a->$key, $b->$key, $prefix);
            break;
          case (is_array($value)):
            $this->compareArray($a->$key, $b->$key, $prefix);
            break;
          case ($a === NULL):
            $this->mark($a, $prefix, 'change');
            break;
          default:
            if (isset($a->$key)) {
              if ($b->$key !== $a->$key) {
                $this->mark($a, $prefix,'change');
              }
            }
            else {
              $this->mark($a, $prefix, 'change');
            }
        }
      }
    }
  }

  /**
   * Compare an amendment to an original to see what kind of changes have been made.
   * @param $amendment
   * @param $original
   * @param string $label
   *   Indicates how the change log will be labeled.
   * @return bool
   */
  public function compareSubmissions($amendment, $original, $label='report') {
    $this->log = [];
    $this->same = true;
    $this->compareObject($amendment, $original, $label);
    return $this->same;
  }

  public function changeLog() {
    return $this->log;
  }

  /**
   * Extract the data referenced in a change log from the data.
   * @param $log_key
   * @param $data
   * @return mixed|null
   */
  protected function getData($log_key, &$data) {
    if (strpos($log_key, '.')) {
      list($key, $remaining) = explode('.', $log_key, 2);
      if (is_array($data)) {
        return $this->getData($remaining, $data[$key]);
      }
      elseif (is_object($data)) {
        return $this->getData($remaining, $data->$key);
      }
      else {
        return null;
      }
    } else {
      if (is_array($data)) {
        return $data[$log_key];
      }
      elseif (is_object($data)) {
        return $data->$log_key;
      }
      else {
        return null;
      }
    }
  }

  /**
   * Extract the data corresponding to an object compare for display purposes.
   * @param $log_key
   * @param $data
   * @return mixed|null
   */
  public function changedData($log_key, &$data) {
    // ignore the first key because it's just for show.
    if (strpos($log_key, '.')) {
      list($key, $remaining) = explode('.', $log_key, 2);
      return $this->getData($remaining, $data);
    }
    else {
      return $data;
    }
  }

}