<?php


namespace WAPDC\CampaignFinance;

use DateTime;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\TransactionRequiredException;
use stdClass;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\Fund;
use \Exception;
use WAPDC\CampaignFinance\Model\Report;
use WAPDC\CampaignFinance\Reporting\C3ReportProcessor;
use WAPDC\CampaignFinance\Reporting\C4ReportProcessor;
use WAPDC\CampaignFinance\Reporting\OrcaReportParser;
use WAPDC\Core\SendGrid\MailerInterface;
use WAPDC\Core\SendGrid\Mail;
use WAPDC\Core\SendGrid\NoCMS;
use WAPDC\Core\Model\Election;

/**
 * Class FundProcessor
 *
 */
class FundProcessor {

  /** @var CFDataManager */
  protected $dataManager;

  /** @var Committee */
  protected $committee;

  /** @var Fund */
  protected $fund;

  /** @var MailerInterface */
  private $mailer;

  private $project_directory = '.';

  public function __construct(CFDataManager $dataManager) {
    $this->dataManager = $dataManager;
    $this->project_directory = dirname(__DIR__);
  }

  /**
   * Ensure that a fund exists for the committee.
   *
   * When the fund does not exist, and it is for a year that is valid for the committee,
   * the fund will be automatically created.  If the fund exists, its id is returned. Also sets the vendor when no vendor is set.
   *
   * @param int $committee_id
   *   Id of the committee to check
   * @param string $election_code
   *   Election code of the year for the committee. For continuing committees this is the year of
   *   the expenses and/or contributions.
   * @return int fund_id
   *   Id of fund we have loaded and are working with.
   *
   * @throws Exception
   */
  public function ensureCommitteeFundAndVendor($committee_id, $election_code, $vendor = null) {
    $this->committee = $this->dataManager->em->find(Committee::class, $committee_id);
    if (!$this->committee) {
      throw new Exception("Invalid Committee");
    }
    if (empty($this->committee->filer_id)) {
      throw new Exception("This committee has not yet been verified by PDC staff. This filing will need to wait until the committee is verified.", 901);
    }

    if (empty($this->committee->continuing)) {
      // single year committees should be filing for their election year
      if ((int)$this->committee->election_code <> (int)$election_code) {
        $err_msg = 'Committee is registered for election ' . $this->committee->election_code . ' but this report is for election ' . $election_code;
        throw new Exception($err_msg);
      }
    }

    if (!empty($this->committee->continuing) && $this->committee->pac_type <> 'surplus') {
      // sanity checks on continuing committees
      if ($this->committee->start_year > (int)$election_code) {
        throw new Exception("Continuing committee was started after election: $election_code");
      }
      if (!empty($this->committee->end_year) && ((int)$election_code > $this->committee->end_year)) {
        throw new Exception("Continuing committee has ended before: $election_code ");
      }
      if ((int)$election_code > date('Y')) {
        throw new Exception("Continuing committees is filing for a future year: $election_code ");
      }
    }

    if ($this->committee->pac_type == 'surplus') {
      // surplus committees ignore the election_code
      $funds = $this->dataManager->em->getRepository(Fund::class)
        ->findBy(['committee_id' => $this->committee->committee_id]);
      if (count($funds) > 1) {
        throw new Exception("Surplus committee has multiple funds. Please contact the PDC for data cleanup support.");
      }
      $this->fund = $funds[0] ?? null;
    }
    else {
      $this->fund = $this->dataManager->em->getRepository(Fund::class)
        ->findOneBy(['committee_id' => $this->committee->committee_id, 'election_code' => $election_code]);
    }

    if (empty($this->fund->fund_id)) {
      $election = $this->dataManager->em->find(Election::class, $election_code);
      if (!$election) {
        throw new Exception("Invalid election code: " . $election_code);
      }
      $this->fund = new Fund();
      $this->fund->committee_id = $this->committee->committee_id;
      $this->fund->filer_id = $this->committee->filer_id;
      $this->fund->election_code = $election_code;
      $this->fund->vendor = $vendor;
      $this->dataManager->em->persist($this->fund);
      $this->dataManager->em->flush($this->fund);

    } else if($vendor && !$this->fund->vendor) {
        $this->fund->vendor = $vendor;
        $this->dataManager->em->persist($this->fund);
        $this->dataManager->em->flush($this->fund);
    }

    return $this->fund->fund_id;
  }

    /**
     * Submit a committee report.
     * @param $committee_id
     * @param stdClass $report
     * @return stdClass
     * @throws Exception
     */
  public function validateCommitteeReport($committee_id, stdClass $report, $version)
  {
      $response = new StdClass();
      $response->success = false;
      try {
          $this->ensureCommitteeFundAndVendor($committee_id, $report->election_year);
      }
      catch (Exception $e) {
          $error_type = $e->getCode() == 901 ? 'committee.unverified' : 'fund.error';
          $response->messages = [$error_type =>  $e->getMessage()];
          $response->message = $e->getMessage();
          return $response;
      }
      $this->dataManager->beginTransaction();
      switch (strtolower($report->report_type)) {
          case "c3":
              $processor = new C3ReportProcessor($this->dataManager, $version);
              $validated = $processor->validateReport($report, $this->fund);
              break;
          case "c4":
          default:
              $processor = new C4ReportProcessor($this->dataManager, $version);
              $validated = $processor->validateReport($report, $this->fund);
              break;
      }
      $this->dataManager->endTransaction();
      $response->success = $validated;
      $results = $processor->getValidatorInfo();
      $response->metadata = json_decode($results->metadata);
      $response->messages = $results->messages;
      $response->message = $results->message;
      return $response;
  }

  /**
   * Submit a committee report.
   * @param $committee_id
   * @param stdClass $report
   * @param $notify_emails
   *    This a string of emails separated by a comma (not an array)
   * @return stdClass
   * @throws ConnectionException
   */
  public function submitCommitteeReport($committee_id, stdClass $report, $notify_emails = null, $vendor = null, $submission_version = null): stdClass
  {
    try {
      // if continuing get from report period start, else use election code
      $committee = (object) $this->dataManager->db->fetchAssociative("SELECT * FROM committee WHERE committee_id = :committee_id", ['committee_id' => $committee_id]);
      if ($committee->continuing) {
        $period_start = new DateTime($report->period_start);
        $election_code = $period_start->format('Y');

        if ($committee->election_code && (substr($committee->election_code, 0, 4) > $election_code)) {
          $election_code = substr($committee->election_code, 0, 4);
        }
      }
      else {
        $election_code = substr($committee->election_code, 0, 4);
      }
      $this->ensureCommitteeFundAndVendor($committee_id, $election_code, $vendor);

      //assign vendor to report
      //assign vendor via fund, leveraging ensure vendor function above
      $report->vendor_name = $vendor;

    }
    catch (Exception $e) {
      $response = new StdClass();
      $response->success = false;
      $error_type = $e->getCode() == 901 ? 'committee.unverified' : 'fund.error';
      $response->messages = [$error_type =>  $e->getMessage()];
      $response->message = $e->getMessage();
      return $response;
    }
    $this->dataManager->beginTransaction();
    switch (strtolower($report->report_type)) {
      case "c3":
        $processor = new C3ReportProcessor($this->dataManager, $submission_version);
        $response = $processor->submitReport($report, $this->fund);
        break;
      case "c4":
      default:
        $processor = new C4ReportProcessor($this->dataManager, $submission_version);
        $response = $processor->submitReport($report, $this->fund);
        break;
    }
    $this->dataManager->endTransaction();
    if (!empty($response->success) && !empty($notify_emails)) {
      $report_period = $report->period_start . ($report->period_end != $report->period_start ? " - $report->period_end" : '');
      $email_template_vars = [
        'committee_id' => $committee_id,
        'committee_name' => $report->committee->name ?? '',
        'date_filed' => $report->submitted_at,
        'report_period' => $report_period,
        'report_id' => $response->report_id,
        'report_type' => strtoupper($report->report_type),
        'response_message' => $response->message
      ];
      $email_array = explode(';', $notify_emails);
      $response->email_sent = $this->sendSuccessEmail($email_array, $email_template_vars);
    }
    return $response;
  }

  /**
   * Reprocess a report.
   *
   * Report to be reprocessed should have a fund_id mapped to it in order to be reprocessed.
   *
   * @param integer $submission_id
   *   ID of the submission to reprocess
   * @param integer $amend_report_id
   *   ID of the report to that this report amends.
   *
   * @return \stdClass
   * @throws Exception
   */
  public function reprocessReportSubmission($submission_id, $amend_report_id = NULL) {
    $rows = $this->dataManager->db->fetchAllAssociative("SELECT s.committee_id, c.continuing, c.start_year, c.election_code, c.filer_id, s.payload FROM campaign_finance_submission s JOIN committee c ON c.committee_id = s.committee_id WHERE submission_id=:submission_id", ['submission_id' => $submission_id]);
    if (!$rows) {
      throw new Exception("Report Submission not found: $submission_id");
    }
    $submission = (object) reset($rows);
    $parser = new OrcaReportParser();
    $processor = CampaignFinance::service()->getFundProcessor();
    $xml = $submission->payload;
    $report = $parser->parse($xml);
    if ($submission->continuing) {
      $report->election_year = (int)(new DateTime($report->period_end))->format('Y');
      if ($report->election_year < $submission->start_year) {
        $report->election_year = $submission->start_year;
      }
    }
    else {
      $report->election_year = (int)$submission->start_year;
    }
    $report->filer_id = $submission->filer_id;
    if ($amend_report_id) {
      $report->amends =$amend_report_id;
    }

    $report->submitted_at = (new DateTime())->format('Y-m-d');
    $response = $processor->submitCommitteeReport($submission->committee_id, $report);
    return $response;
  }

  /**
   * Delete a submitted report.
   * @param integer $report_id_to_delete
   * @throws Exception
   */
  public function deleteSubmittedReport($report_id_to_delete) {
    $report_to_delete = $this->dataManager->em->find(Report::class, $report_id_to_delete);
    if (!$report_to_delete) {
      throw new Exception("Report not found: $report_id_to_delete");
    }

    // If the report to be deleted (B) is superseded get the report this amends (A).
    // Set the superseded_id of A to the superseded_id of B, since B will be deleted.
    $amended_report = $this->dataManager->em->getRepository(Report::class)
      ->findBy(['superseded_id' => $report_id_to_delete]);

    if($amended_report) {
      $amended_report[0]->superseded_id = $report_to_delete->superseded_id ? $report_to_delete->superseded_id : NULL;
      $this->dataManager->em->persist($amended_report[0]);
    }

    $this->dataManager->em->remove($report_to_delete);
    $this->dataManager->em->flush();
  }

  public function sendSuccessEmail($email_array, $email_template_variables = []) {
    $sent_emails = [];
    if (!$this->mailer) {
      $this->setMailer(new Mail(new NoCMS));
    }
    $this->mailer->setMessageTemplate('d-491b590768124a12b1bdf9d1bce45a93');
    $this->mailer->setSender('pdc@pdc.wa.gov', 'Washington Public Disclosure Commission');

    foreach($email_array as $email) {
      if (method_exists($this->mailer, 'isLive') && $this->mailer->isLive() == true) {
        $this->mailer->addRecipient($email, '', $email_template_variables);
        $sent_emails[] = $email;
      }
      elseif (preg_match('/@pdc.wa.gov$/', $email)) {
        $this->mailer->setTestModeEmail($email);
        $this->mailer->addRecipient($email, '', $email_template_variables);
        $sent_emails[] = $email;
      }
    }
    $success = $this->mailer->send();
    if ($success) {
      return $sent_emails;
    }
    else {
      return false;
    }
  }

  public function setMailer(MailerInterface $m) {
    $this->mailer = $m;
    return $this;
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function allSubmissionToJson()
  {
    $reports = $this->dataManager->db->executeQuery("select report_id, payload from campaign_finance_submission where processed = true")->fetchAllAssociative();
    foreach($reports as $idx => $report) {
      if ($idx % 100 === 0) echo "\n% " . $idx . " records processed % ";
      $this->submissionToJson($report['report_id']);
    }
  }

  /**
   * @throws ORMException
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws OptimisticLockException
   * @throws TransactionRequiredException
   * @throws \Doctrine\DBAL\Exception
   */
  public function submissionToJson($report_id)
  {
    $parser = new OrcaReportParser();
    [$submission] = $this->dataManager->db->executeQuery("select r.election_year, r.submitted_at,  cfs.fund_id, cfs.payload from campaign_finance_submission cfs join report r on cfs.report_id = r.report_id where cfs.report_id = $report_id and processed = true")->fetchAllAssociative() + [null];
    if (!$submission) {
      echo "\nNo record found for report_id: $report_id";
      return;
    }
    $parsedReport = $parser->parse($submission['payload']);
    /** @var Fund $fund */
    $fund = $this->dataManager->em->find(Fund::class, $submission['fund_id']);

    if(!$fund) {
        [$fund] = $this->dataManager->db->executeQuery("select fund_id from report where report_id = $report_id")->fetchAllAssociative();
        $fund = $this->dataManager->em->find(Fund::class, $fund['fund_id']);
    }
    $parsedReport->submitted_at = $submission['submitted_at'];
    $parsedReport->election_year = $submission['election_year'];

    switch ($parsedReport->report_type) {
      case 'c3':
        $processor = new C3ReportProcessor($this->dataManager, 1.1);
        $processor->validateReport($parsedReport, $fund);
        break;
      case 'c4':
        $processor = new C4ReportProcessor($this->dataManager, 1.1, true);
        $processor->validateReport($parsedReport, $fund);
        break;
      default:
        throw new Exception('Invalid report type');
    }

    $user_data = json_encode($parsedReport);
    $metadata = json_encode(['sums' => $processor->sums]);
    $version = 1.0;
    $q = "UPDATE report SET user_data = ?, metadata = ?, version =  ? where report_id = ?";
    try {
      $this->dataManager->db->executeQuery($q, [$user_data, $metadata, $version, $report_id]);
    } catch (Exception $e) {
      echo "Error occurred processing report_id $report_id";
      echo $e->getMessage();
    }
  }

  public function getCandidacyData($year, $search)
  {
    $dir = $this->project_directory . '/data/public/';
    $file = 'candidacy-search.sql';
    if (!file_exists("$dir/$file")) {
      throw new Exception("Invalid Query: $dir/$file");
    }
    $parameters = ['year' => $year, 'search' => "%" . $search . "%"];
    return $this->dataManager->executeQueryFromFile($dir, $file, $parameters);
  }

  public function SearchJurisdictions($search)
  {
    $q = "select j.jurisdiction_id, j.name from jurisdiction j
      where inactive is null and valid_to is null and category not in (14, 15, 16)
        and name ilike :search";
    $query = $this->dataManager->db->executeQuery($q, ['search' => '%' . $search . '%']);
    return $query->fetchAllAssociative() + [];
  }

  public function getProposalData($jurisdiction_id)
  {
    $q = "select p.*
      from proposal p
      where p.jurisdiction_id = :jurisdiction_id";
    $query = $this->dataManager->db->executeQuery($q, ['jurisdiction_id' => $jurisdiction_id]);
    return $query->fetchAllAssociative() + [];
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function getReportData($repno)
  {
    $surplus = false;
    $q = "select * from od_campaign_finance_reports_by_report_id(?)";
    [$result] = $this->dataManager->db->executeQuery($q, [$repno])->fetchAllAssociative();

    if (!$result) {
        $surplus = true;
        $q = "select * from od_surplus_funds_reports where report_number = ?";
        [$result] = $this->dataManager->db->executeQuery($q, [$repno])->fetchAllAssociative();
    }

    if ($surplus) {
        $sql = "select * from od_surplus_balance where id = ?";
        [$committee_data] = $this->dataManager->db->executeQuery($sql, [$result['committee_id']])->fetchAllAssociative();
        $result['committee_data'] = $committee_data;}
    else {
        $sql = "select * from od_campaign_finance_summary where committee_id = ?";
        [$committee_data] = $this->dataManager->db->executeQuery($sql, [$result['committee_id']])->fetchAllAssociative();
        $result['committee_data'] = $committee_data;
     }

    $result['metadata'] = json_decode($result['metadata']);
    $result['attachments'] = $result['attachments'] ? json_decode($result['attachments']) : [];

    return [$result];
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function getReportUserData($repno)
  {
    $q = "select user_data, metadata, version from report where report_id = ?";
    [$result] = $this->dataManager->db->executeQuery($q, [$repno])->fetchAllAssociative();
    $result['user_data'] = json_decode($result['user_data']);
    $result['metadata'] = json_decode($result['metadata']);

    $query = "select code, label from expense_category";
    $categories = $this->dataManager->db->executeQuery($query)->fetchAllAssociative();

    $result['categorylist'] = [];
    foreach($categories as $row) {
      $result['categorylist'][] = (object)$row;
    }
    return $result;
  }

  /**
   * report_data signature:
   * {"report_id": 100003379, "submitted_at": "2020-12-07", "external_id": 1130911, "amends_id": "110141604", "input_external_id" : 1130911}
   * @param $report_id
   * @param $report_data
   * @return void
   * @throws \Doctrine\DBAL\Exception
   */
  public function editReport($report_id, $report_data)
  {

    $submitted_at = $report_data->submitted_at;
    $amends_id = $report_data->amends_id;
    $external_id = $report_data->external_id;
    $input_external_id = $report_data->input_external_id;

    if(empty($report_id)){
      throw new Exception("Report id is missing.");
    }

    if(empty($submitted_at)){
      throw new Exception("Submitted at date is invalid use this '2022-12-25' format.");
    }

    if($report_id === $amends_id) {
      throw new Exception("Unable to save changes report_id and amends_id are the same report.");
    }

    // handle new external id
    if($input_external_id)
    {
      $external_id = $input_external_id;
    }

    $this->dataManager->db->executeStatement("select * from public.cf_save_amendment_chain(:report_id, :submitted_at, :amended_report_id, :external_id)",
      ["report_id" => $report_id, "submitted_at" => $report_data->submitted_at, "amended_report_id" => $report_data->amends_id, "external_id" => $external_id]);
  }
}
