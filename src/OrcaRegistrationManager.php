<?php

namespace WAPDC\CampaignFinance;

use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\Core\Model\Election;
use \SimpleXMLElement;
use \stdClass;

class OrcaRegistrationManager {

  /**
   * @var CFDataManager
   */
  private $_dataManager;

  /**
   * @var CommitteeManager
   */
  private $_committeeManager;

  /**
   * @var RegistrationManager
   */
  private $_registrationManager;

  /**
   * OrcaRegistrationManager constructor.
   * @param CFDataManager $dataManager
   * @param CommitteeManager $committeeManager
   * @param RegistrationManager $registrationManager
   */
  public function __construct(CFDataManager $dataManager, CommitteeManager $committeeManager, RegistrationManager $registrationManager) {
    $this->_dataManager = $dataManager;
    $this->_committeeManager = $committeeManager;
    $this->_registrationManager = $registrationManager;
  }

  /**
   * @param $committee_id
   * @return bool|int|string
   * @throws \Exception
   */
  public function getCampaignElectionYear($committee_id){
    $committee = $this->_dataManager->em->find(Committee::class, $committee_id);
    if ($committee){
      $year = $committee->election_code && !$committee->continuing ? $committee->election_code : 0;
      if (strlen($year) > 4){
        $year = substr($year, 0, 4);
      }
      return $year;
    }
    return 0;
  }

  /**
   * @param $token
   * @return mixed|null
   * @throws \Exception
   */
  public function getRegistrationXML($token){
    $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>'.'<root></root>');
    $committee_id = $this->_committeeManager->validateAPIToken($token);
    if (!$committee_id){
      return null;
    }

    /** @var Committee $committee */
    $committee = $this->_dataManager->em->Find(Committee::class, $committee_id);
    /** @var Registration $registration */
    $registration = NULL;
    if (!$committee){
      return null;
    }
    if ($committee->registration_id) {
      $registration = $this->_dataManager->em->Find(Registration::class, $committee->registration_id);
    }
    if ($committee->registration_id && $registration->user_data) {
      $submission = $this->_registrationManager->getRegistrationSubmission($committee->registration_id);
      $this->convertSubmissionToXML($xml, $submission);
    }
    else {
      $submission = $this->_committeeManager->getCommitteeSubmission($committee_id);
      $this->convertSubmissiontoXML($xml, $submission);
    }
    return $xml->asXML();
  }

  /**
   * @param $xml
   * @param \stdClass $submission
   * @param $committee_id
   * @param $filer_id
   * @return mixed
   * @throws \Exception
   */
  public function convertSubmissionToXML(SimpleXMLElement $xml, stdClass $submission){
    $committee = $submission->committee;
    $treasurer = $this->findTreasurer($committee);
    $election_code = $committee->election_code?? '';
    $election = $this->_dataManager->em->find(Election::class, $election_code);
    $special = false;
    $year = null;
    if ($election) {
      $election_date = $election->election_date;
      $special = strpos($election_code, "S") ? 'true' : 'false';
      $year = $election_date->format('Y');
    }

    $campaignXml = $xml->addChild('CampaignInfo');
    $campaignXml->addAttribute('ELECTIONYEAR', $year ?? '');
    $campaignXml->addAttribute('ISSPECIAL', $special ?? '');
    $campaignXml->addAttribute("STARTDATE", $committee->start_date ?? '');
    $campaignXml->addAttribute('ISCANDIDATE', $committee->committee_type=='CA' ?? false);
    $campaignXml->addAttribute('STATEWIDE', $committee->bonafide_type ?? '');
    $campaignXml->addAttribute("committeeId", $committee->committee_id);

    $committeeXml =  $xml->addChild('CommitteeInfo');
    $committeeXml->addAttribute('FILERID', $committee->filer_id ?? '');
    $committeeXml->addAttribute('COUNTY', $committee->county ?? '');
    $committeeXml->addAttribute('NAME', $committee->name ?? '');
    $committeeXml->addAttribute('STREET', $committee->contact->address ?? '');
    $committeeXml->addAttribute("ADDRESS", $committee->contact->address ?? '');
    $committeeXml->addAttribute("CITY", $committee->contact->city ?? '');
    $committeeXml->addAttribute("STATE", $this->formatState($committee->contact->state) ?? '');
    $committeeXml->addAttribute("ZIP", $committee->contact->postcode ?? '');
    $committeeXml->addAttribute("PHONE1", $committee->contact->phone ?? '');
    $committeeXml->addAttribute("EMAIL", $committee->contact->email ?? '');
    $committeeXml->addAttribute("continuing", $committee->continuing ?? '');
    $committeeXml->addAttribute("pacType", $committee->pac_type ?? '');

    $treasurerXml = $xml->addChild('TreasurerInfo');
    $treasurerXml->addAttribute("FIRSTNAME", $treasurer->name ?? $treasurer->person?? '');
    $treasurerXml->addAttribute("STREET", $treasurer->address ?? '');
    $treasurerXml->addAttribute("CITY", $treasurer->city ?? '');
    $treasurerXml->addAttribute("STATE", $this->formatState($treasurer->state ?? ''));
    $treasurerXml->addAttribute("ZIP", $treasurer->postcode ?? '');
    $treasurerXml->addAttribute("PHONE", $treasurer->phone ?? '');
    $treasurerXml->addAttribute("EMAIL", $treasurer->email ?? '');
    $treasurerXml->addAttribute("MINISTERIAL", !empty($treasurer->ministerial) ? 'true' : 'false');

    if ($committee->committee_type=='CA') {
      $candidateXml = $xml->addChild('CandidateInfo');
      $candidateXml->addAttribute("OFFICESOUGHT", $committee->candidacy->office ?? '');
      $candidateXml->addAttribute("JURISDICTION", $committee->candidacy->jurisdiction ?? '');
      $candidateXml->addAttribute("POSITIONNO", $committee->candidacy->position ?? '');
      $candidateXml->addAttribute("EMAIL", $committee->candidacy->email ?? '');
      $candidateXml->addAttribute("FIRSTNAME", $committee->candidacy->person ?? '');
      $candidateXml->addAttribute('STREET', $committee->contact->address ?? '');
      $candidateXml->addAttribute("CITY", $committee->contact->city ?? '');
      $candidateXml->addAttribute("STATE", $this->formatState($committee->contact->state ?? ''));
      $candidateXml->addAttribute("ZIP", $committee->contact->postcode ?? '');
    }
    return $xml->asXML();
  }

  public function findTreasurer($json){
    foreach($json as $property => $value) {
      if ($property == 'officers'){
        foreach ($value as $data){
          foreach ($data as $prop => $val) {
            if ($prop == 'treasurer' && $val == true) {
              return $data;
            }
          }
        }
      }
      if ($property=='candidacy'){
        foreach ($value as $prop => $val){
          if ($prop == 'treasurer' && $val == true) {
            return $value;
          }
        }
      }
    }
    return null;
  }

  private function formatState($state){
    $fixed_data = strtoupper(trim($state));
    if (strlen($fixed_data) > 2) {
      switch ($fixed_data) {
        case 'WASHINGTON':
          $fixed_data = 'WA';
          break;
        case 'WYOMING':
          $fixed_data = 'WY';
          break;
        case 'VIRGINIA':
          $fixed_data = 'VA';
          break;
        case 'OREGON':
          $fixed_data = 'OR';
          break;
        case 'CALIFORNIA':
          $fixed_data = 'CA';
          break;
        case 'NEW YORK':
          $fixed_data = 'NY';
          break;
        case 'KANSAS':
          $fixed_data = 'KS';
          break;
        case 'WASHINGTON DC':
          $fixed_data = 'DC';
          break;
        default:
          $fixed_data = "";
      }
    }
    return $fixed_data;
  }
}