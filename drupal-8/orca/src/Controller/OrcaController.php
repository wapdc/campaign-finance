<?php
namespace Drupal\orca\Controller;

use Drupal;
use Drupal\wapdc_core\Controller\CoreControllerBase;
use Exception;
use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WAPDC\CampaignFinance\CampaignFinance;
use WAPDC\CampaignFinance\FinanceReportingProcessor;
use WAPDC\CampaignFinance\OrcaProcessor;

class OrcaController extends CoreControllerBase {

  /**
   * Manage activities related to a fund.
   * @param Request $request
   * @return Response
   * @throws \Exception
   */
  public function fundAPI(Request $request, $fund_id, $object, $id)
  {
      $data = new stdClass();
      $data->errors = NULL;
      $data->success = TRUE;
      $fund_id = (int)$fund_id;
      $ip_address = $request->getClientIp();
      $saveCount = 0;

      /** @var FinanceReportingProcessor $financeReportingProcessor */
      $orcaProcessor = CampaignFinance::service()->getOrcaProcessor();

      $this->prepareRequest($request);
      $method = $request->getMethod();
      $request_data = $this->request_data;
      if ($method == 'POST' || $method == 'PUT') {
          $payload = $this->request_data ? json_encode($this->request_data) : null;
      }

      // Ensure that we check the save count to make sure that
      if (($method == 'POST' || $method == 'PUT')
          && $object != 'save-validations'
          && $object != 'push-changes'
          && $object != 'upgrade'
          && $object != 'purge'
          && $object != 'update-campaign-vendor'
          && $object != 'restore-campaign'
          && $object != 'recalculate-contact-aggregates'
      ) {
        $saveCount =$this->request_data->saveCount ?? 0;
        $saveCount = $orcaProcessor->validateSave($fund_id, $saveCount);
        if (!$saveCount) {
          return  $response = new JsonResponse(null, 409, [], false);
        }
      }

      try {

          //Call correct OrcaProcessor method based on object and request method
          switch (true) {
            case ($method == 'POST' && $object == 'upgrade'):
              $orcaProcessor->upgradeCampaign($fund_id);
              break;
            case ($method == 'GET' && $object == 'sync'):
                $data->sync = $orcaProcessor->getSubmissionSyncPayload($fund_id);
                break;
            case ($method == 'POST' && $object == 'push-changes'):
                // Assume the id parameter is the version for the sync so that we
                // can do different things based on the version
                $version = !empty($id) ? (float)$id : null;

                // Note that we are not pushing parsed changes because
                // we expect the raw json to be passed to the database here.
                // we don't want the string parsing to apply to ORCA syncs yet as it causes problems.
                $payload = file_get_contents('php://input');
                $data->last_sync = $orcaProcessor->pushChanges($fund_id, $payload, $version);
                break;
            case ($method == 'POST' && $object == 'save-validations'):
                $orcaProcessor->saveValidations($fund_id, $payload);
                $data->success = true;
                break;
            case ($method == 'POST' && $object == 'save-campaign-settings'):
                $data = $orcaProcessor->saveCampaignSettings($fund_id, $payload);
                break;
            case ($method == "GET" && $object == 'expenditure' && $id):
                $data = $orcaProcessor->loadExpenditure($fund_id, $id);
                break;
            case ($method == 'POST' && $object == 'expenditure'):
            case ($method == 'PUT' && $object == 'expenditure' && $id):
                $data = $orcaProcessor->saveExpenditure($fund_id, $request_data);
                break;
            case ($method == 'GET' && $object == 'contact' && $id):
                $data = $orcaProcessor->loadContact($fund_id, $id);
                break;
            case ($method == 'POST' && $object == 'contact'):
            case ($method == 'PUT' && $object == 'contact' && $id):
                $data = $orcaProcessor->saveContact($fund_id, $payload);
                break;
            case ($method == 'DELETE' && $object == 'contact' && $id):
                $data = $orcaProcessor->deleteContact($fund_id, $id);
                break;
            case ($method == 'POST' && $object == 'contacts'):
                $data = $orcaProcessor->importContacts($fund_id, $request_data->contacts ?? []);
                break;
            case ($method == 'POST' && $object == 'monetary-contribution'):
            case ($method == 'PUT' && $object == 'monetary-contribution' && $id):
                $data = $orcaProcessor->saveMonetaryContribution($fund_id, $request_data);
                break;
            case ($method == 'GET' && $object == 'monetary-contribution' && $id):
                $data = $orcaProcessor->loadTransaction($fund_id, $id, 'monetary-contribution');
                break;
            case ($method == 'POST' && $object == 'split-monetary-contribution'):
                $data = $orcaProcessor->addSplitContribution($fund_id, $request_data);
                break;
            case ($method == 'POST' && $object == 'in-kind-contribution'):
            case ($method == 'PUT' && $object == 'in-kind-contribution' && $id):
                $data = $orcaProcessor->saveInKindContribution($fund_id, $request_data);
                break;
            case ($method == 'GET' && $object == 'in-kind-contribution' && $id):
                $data = $orcaProcessor->loadTransaction($fund_id, $id, 'in-kind-contribution');
                break;
            case ($method == 'POST' && $object == 'anonymous-contribution'):
            case ($method == 'PUT' && $object == 'anonymous-contribution' && $id):
                $data = $orcaProcessor->saveAnonymousContribution($fund_id, $request_data);
                break;
            case ($method == 'GET' && $object == 'anonymous-contribution' && $id):
                $data = $orcaProcessor->loadTransaction($fund_id, $id, 'anonymous-contribution');
                break;
            case ($method == 'POST' && $object == 'refund-contribution'):
            case ($method == 'PUT' && $object == 'refund-contribution' && $id):
                $data = $orcaProcessor->saveContributionRefund($fund_id, $payload);
                break;
            case ($method == 'GET' && $object == 'refund-contribution' && $id):
                $data = $orcaProcessor->loadTransaction($fund_id, $id, 'refund-contribution');
                break;
            case ($method == 'POST' && $object == 'monetary-contributions'):
                if (!is_array($request_data->contributions ?? null)) {
                  throw new Exception('Invalid or missing contributions');
                }
                $data = $orcaProcessor->importContributions($fund_id, $request_data->contributions);
                $data->success = true;
                break;
            case ($method == 'DELETE' && $object == 'contribution' && $id):
                $data = $orcaProcessor->deleteContribution($fund_id, $id);
                break;
            case ($method == 'GET' && $object == 'advertisement' && $id):
                $data = $orcaProcessor->loadAdvertisement($fund_id, $id);
                break;
            case ($method == 'POST' && $object == 'advertisement'):
            case ($method == 'PUT' && $object == 'advertisement' && $id):
                $data = $orcaProcessor->saveAdvertisement($fund_id, $payload);
                break;
            case ($method == 'POST' && $object == 'derby-updated'):
                $data = $orcaProcessor->setDerbyUpdated($fund_id, $payload);
                break;
            case ($method == 'DELETE' && $object == 'expenditure' && $id):
                $data = $orcaProcessor->deleteExpenditure($fund_id, $id);
                break;
            case ($method == 'GET' && $object == 'logs'):
                $data = $orcaProcessor->getDeletedTransactionLogs($fund_id);
                break;
            case ($method == 'POST' && $object == 'submit-c3-report'):
                $data = $orcaProcessor->submitC3($fund_id, $id, $request_data, $ip_address);
                break;
            case ($method == 'POST' && $object == 'submit-c4-report'):
                $data = $orcaProcessor->submitC4($fund_id, $id, $request_data, $ip_address);
                break;
            case ($method == 'GET' && $object == 'deposits' && $id):
                $data = $orcaProcessor->generateC3Preview($fund_id, $id);
                break;
            case ($method == 'GET' && $object == 'reporting-obligations' && $id):
                $data = $orcaProcessor->generateC4Preview($fund_id, $id);
                break;
            case ($method == 'POST' && $object == 'reporting-obligations'):
                $orcaProcessor->insertReportingObligations($fund_id);
                break;
            case ($method == 'POST' && $object == 'add-reporting-obligation'):
                $data = $orcaProcessor->addOneReportingObligation($fund_id);
                break;
            case ($method == "POST" && $object == 'combine-reporting-periods'):
                $data->success = $orcaProcessor->combineReportingPeriods(
                    $fund_id, $request_data->obligation_id ?? 0, $request_data->start_date ?? null);
                break;
            case ($method == 'GET' && $object == 'c3-attachments' && $id):
                $data = $orcaProcessor->getAttachedPage($fund_id, $id, 'deposit');
                break;
            case ($method == 'POST' && $object == 'c3-attachments'):
            case ($method == 'POST' && $object == 'c4-attachments'):
                $data = $orcaProcessor->saveAttachedPage($fund_id, $payload);
                break;
            case ($method == 'GET' && $object == 'c4-attachments'):
                $data = $orcaProcessor->getAttachedPage($fund_id, $id, 'reporting_obligation');
                break;
            case ($method == 'GET' && $object == 'low-cost-fundraiser' && $id):
                $data = $orcaProcessor->loadTransaction($fund_id, $id, 'low-cost-fundraiser');
                break;
            case ($method == 'POST' && $object == 'low-cost-fundraiser'):
            case ($method == 'PUT' && $object == 'low-cost-fundraiser' && $id):
                $data = $orcaProcessor->saveLowCostFundraiser($fund_id, $request_data);
                break;
            case ($method == 'GET' && $object=='cash-fundraiser' && $id):
            $data = $orcaProcessor->loadFundraiser($fund_id, $id);
            break;
            case ($method == 'DELETE' && $object == 'auction-item' && $id):
              $data = $orcaProcessor->deleteAuctionItem($fund_id, $id);
              break;
          case ($method == 'POST' && $object=='cash-fundraiser'):
          case ($method == 'PUT' && $object=='cash-fundraiser' && $id):
              $data = $orcaProcessor->saveCashFundraiser($fund_id, $request_data);
              break;
          case ($method == 'DELETE' && $object == 'fundraiser' && $id):
                    $data = $orcaProcessor->deleteLowCostFundraiser($fund_id, $id);
                    break;
          case ($method == 'DELETE' && $object == 'cash-fundraiser' && $id):
              $data = $orcaProcessor->deleteCashFundraiser($fund_id, $id);
              break;
          case ($method == 'GET' && $object == 'misc-other' && $id):
              $data = $orcaProcessor->loadTransaction($fund_id, $id, 'misc-other');
              break;
          case ($method == 'POST' && $object == 'misc-other'):
          case ($method == 'PUT' && $object == 'misc-other' && $id):
              $data = $orcaProcessor->saveMiscOther($fund_id, $request_data);
              break;
          case ($method == 'DELETE' && $object == 'misc-other' && $id):
              $data = $orcaProcessor->deleteMiscOther($fund_id, $id);
              break;
          case ($method == 'GET' && $object == 'vendor-debt-details'):
              $data = $orcaProcessor->getVendorDebtDetails($fund_id, $id);
              break;
          case ($method == 'GET' && $object == 'vendor-refund' && $id):
              $data = $orcaProcessor->loadTransaction($fund_id, $id, 'vendor-refund');
              break;
          case ($method == 'POST' && $object == 'vendor-refund'):
          case ($method == 'PUT' && $object == 'vendor-refund' && $id):
              $data = $orcaProcessor->saveVendorRefund($fund_id, $request_data);
              break;
          case ($method == 'POST' && $object == 'vendor-debt-forgiveness'):
          case ($method == 'PUT' && $object == 'vendor-debt-forgiveness' && $id):
              $data = $orcaProcessor->saveVendorDebtForgiveness($fund_id, $request_data);
              break;
          case ($method == 'GET' && $object == 'account' && $id):
              $data->account = $orcaProcessor->getAccount($fund_id, $id);
              break;
          case ($method == 'POST' && $object == 'account'):
          case ($method == 'PUT' && $object == 'account' && $id):
              $data = $orcaProcessor->saveAccount($fund_id, $request_data);
              break;
          case ($method == 'GET' && $object == 'credit-card-payment' && $id):
              $data = $orcaProcessor->getCreditCardPayment($fund_id, $id, 'credit-card-payment');
              break;
          case ($method == 'POST' && $object == 'credit-card-payment'):
          case ($method == 'PUT' && $object == 'credit-card-payment' && $id):
              $data = $orcaProcessor->saveCreditCardPayment($fund_id, $request_data);
              break;
          case ($method == 'DELETE' && $object == 'vendor-refund' && $id):
              $data = $orcaProcessor->deleteVendorRefund($fund_id, $id);
              break;
          case ($method == 'GET' && $object == 'misc-bank-interest' && $id):
              $data = $orcaProcessor->loadTransaction($fund_id, $id, 'misc-bank-interest');
              break;
          case($method == 'POST' && $object == 'misc-bank-interest'):
          case($method == 'PUT' && $object == 'misc-bank-interest' && $id):
              $data = $orcaProcessor->saveMiscBankInterest($fund_id, $request_data);
              break;
          case ($method == 'DELETE' && $object == 'misc-bank-interest' && $id):
              $data = $orcaProcessor->deleteMiscBankInterest($fund_id, $id);
              break;
          case ($method == 'GET' && $object == 'depositevent' && $id):
              $data = $orcaProcessor->getDeposit($fund_id, $id);
              break;
          case ($method == 'POST' && $object == 'depositevent'):
          case ($method == 'PUT' && $object == 'depositevent');
              $data = $orcaProcessor->saveDeposit($fund_id, $request_data);
              break;
          case ($method == 'DELETE' && $object == 'depositevent'):
              $data = $orcaProcessor->deleteDeposit($fund_id, $id);
              break;
          case ($method == 'GET' && $object == 'orca-campaign-restore'):
              $data = $orcaProcessor->getCampaignRestore($fund_id);
              break;
          case ($method == 'GET' && $object == 'transaction' && $id):
              $data = $orcaProcessor->loadTransaction($fund_id, $id, 'transaction');
              break;
          case ($method == 'POST' && $object == 'account-transfer'):
          case ($method == 'PUT' && $object == 'account-transfer' && $id):
              $data = $orcaProcessor->saveAccountTransfer($fund_id, $request_data);
              break;
          case ($method == 'DELETE' && $object == 'transaction' && $id):
              $data = $orcaProcessor->deleteTransaction($fund_id, $id);
              break;
          case ($method == 'POST' && $object == 'merge-contacts'):
              $data = $orcaProcessor->mergeContacts($fund_id, $request_data->contact_id, $request_data->duplicate_id, $request_data->same_contact);
              break;
          case ($method == "DELETE" && $object == 'advertisement'):
              $data = $orcaProcessor->deleteAdvertisement($fund_id, $id);
              break;
          case ($object == 'purge'):
              $data = $orcaProcessor->purgeOldOrcaIds($fund_id, $payload);
              break;
          case ($method == "DELETE" && $object == 'delete-campaign'):
              $orcaProcessor->deleteCampaign($fund_id);
              break;
          case ($method == 'GET' && $object == 'correction' && $id):
              $data = $orcaProcessor->getCorrection($fund_id, $id);
              break;
          case ($method == 'POST' && $object == 'correction'):
          case ($method == 'PUT' && $object == 'correction' && $id):
              $request_data->trankeygen_id = $id;
              $data = $orcaProcessor->saveCorrection($fund_id, $request_data);
              break;
          case ($method == 'GET' && $object == 'loan-details'):
              $data = $orcaProcessor->getLoanDetails($fund_id, $id);
              break;
          case ($method == 'POST' && $object == 'loan-payment'):
          case ($method == 'PUT' && $object == 'loan-payment' && $id):
              $data = $orcaProcessor->saveLoanPayment($fund_id, $request_data);
              break;
          case ($method == 'GET' && $object == 'fundraiser-details'):
              $data = $orcaProcessor->getFundraiserDetails($fund_id, $id);
              break;
          case ($method == 'GET' && $object == 'auction-details'):
              $data = $orcaProcessor->getAuctionDetails($fund_id, $id);
              break;
          case($method == "DELETE" && $object == 'correction'):
              $data = $orcaProcessor->deleteCorrection($fund_id, $id);
              break;
          case ($method == 'GET' && $object == 'loan'):
              $data = $orcaProcessor->getLoan($fund_id, $id);
              break;
          case ($method == 'POST' && $object == 'monetary-loan'):
          case ($method == 'PUT' && $object == 'monetary-loan' && $id):
              $data = $orcaProcessor->saveMonetaryLoan($fund_id, $request_data);
              break;
          case ($method == 'POST' && $object == 'in-kind-loan'):
          case ($method == 'PUT' && $object == 'in-kind-loan' && $id):
              $data = $orcaProcessor->saveInKindLoan($fund_id, $request_data);
              break;
          case ($method == 'DELETE' && $object == 'loan' && $id):
              $orcaProcessor->deleteLoan($fund_id, $id);
              $data->success = true;
              break;
          case($method == 'GET' && $object == 'loan-forgiveness'):
              $data = $orcaProcessor->loadTransaction($fund_id, $id, 'loan-forgiveness');
              break;
          case ($method == 'POST' && $object == 'loan-forgiveness'):
          case ($method == 'PUT' && $object == 'loan-forgiveness' && $id):
              $data = $orcaProcessor->saveLoanForgiveness($fund_id, $request_data);
            break;
          case ($method == 'POST' && $object == 'loan-endorser'):
          case ($method == 'PUT' && $object == 'loan-endorser' && $id):
            $data = $orcaProcessor->saveLoanEndorser($fund_id, $request_data);
                    break;
          case ($method == 'GET' && $object == 'fundraiser-contribution'):
            $data = $orcaProcessor->loadTransaction($fund_id, $id, 'fundraiser-contribution');
            break;
          case ($method == 'POST' && $object == 'fundraiser-contribution'):
          case ($method == 'PUT' && $object == 'fundraiser-contribution' && $id):
            $data = $orcaProcessor->saveFundraiserContribution($fund_id, $request_data);
            break;
          case ($method == 'POST' && $object == 'auction'):
          case ($method == 'PUT' && $object =='auction' && $id):
             $data = $orcaProcessor->saveAuction($fund_id, $request_data);
             break;
          case ($method == 'DELETE' && $object == 'auction' && $id):
             $orcaProcessor->deleteAuction($fund_id, $id);
             $data->success = true;
             break;
          case ($method == 'GET' && $object == 'auction-item' && $id):
            $data = $orcaProcessor->getAuctionItem($fund_id, $id);
            break;
          case ($method == 'POST' && $object == 'auction-item'):
          case ($method == 'PUT' && $object == 'auction-item' && $id):
            $data = $orcaProcessor->saveAuctionItem($fund_id, $request_data);
            break;
          case ($method == 'GET' && $object == 'credit-card-details' && $id):
            $data = $orcaProcessor->getCreditCardDetails($fund_id, $id);
            break;
          case ($method == 'GET' && $object == 'pledge' && $id):
            $data = $orcaProcessor->getPledge($fund_id, $id);
            break;
          case ($method == 'DELETE' && $object == 'pledge' && $id):
            $data = $orcaProcessor->deletePledge($fund_id, $id);
            break;
          case ($method == 'POST' && $object == 'pledge'):
          case ($method == 'PUT' && $object == 'pledge' && $id):
            $data = $orcaProcessor->savePledge($fund_id, $request_data);
            break;
          case ($method == 'GET' && $object == 'pledge-cancel' && $id):
            $data = $orcaProcessor->loadTransaction($fund_id, $id, 'pledge-cancel');
            break;
          case ($method == 'POST' && $object == 'pledge-cancel'):
          case ($method == 'PUT' && $object == 'pledge-cancel' && $id):
            $data = $orcaProcessor->savePledgeCancel($fund_id, $request_data);
            break;
          case ($method == 'GET' && $object == 'invoiced-expenses'):
            $data = $orcaProcessor->getInvoicedExpenses($fund_id, $id);
            break;
          case ($method == 'POST' && $object == 'invoiced-expense-payment'):
          case ($method == 'PUT' && $object == 'invoiced-expense-payment' && $id):
            $data = $orcaProcessor->saveInvoicedExpensePayment($fund_id, $request_data);
            break;
          case ($method == 'POST' && $object == 'invoiced-expense-adjustment'):
          case ($method == 'PUT' && $object == 'invoiced-expense-adjustment' && $id):
              $data = $orcaProcessor->saveInvoicedExpenseAdjustment($fund_id, $request_data);
              break;
          case ($method == 'POST' && $object == 'modify-group-members'):
              $orcaProcessor->modifyGroupMembers($fund_id, $request_data);
              break;
          case ($method === 'POST' && $object === 'save-monetary-group-contribution'):
              $orcaProcessor->saveMonetaryGroupContribution($fund_id, $request_data);
              break;
          case ($method === 'GET' && $object === 'get-monetary-group-contribution'):
              $data = $orcaProcessor->getMonetaryGroupContribution($fund_id, $id);
              break;
          case ($method === 'POST' && $object === 'restore-campaign'):
              $data = $orcaProcessor->restoreCampaign($fund_id, $request_data, $this->getCurrentUser());
              break;
          case ($method === 'GET' && $object === 'get-import-fund-info'):
              $data = $orcaProcessor->getImportFundInfo($fund_id);
              break;
          case ($method == "POST" && $object == 'update-campaign-vendor'):
              $orcaProcessor->updateCampaignVendor($fund_id, $request_data);
              break;
          case ($method == "POST" && $object == 'recalculate-contact-aggregates'):
              $orcaProcessor->recalculateContactAggregates($request_data->contact_id);
              break;
          // Generic case to handle common queries.   This should always be the last one before default.
          case ($method == "GET" && !empty($object)):
            $data = $this->orcaFundQuery($fund_id, $object);
            break;
          default:
              throw new Exception("Invalid API Call ");
          }

          if ($method === 'POST' || $method === 'PUT') {
            $data->saveCount = $saveCount;
          }
      } catch (Exception $e) {
          $data->error = json_encode($e->getMessage());
          $data->success = false;
          Drupal::logger('orca')->error($e->getMessage());
      }

      $response = new JsonResponse($data, 200, [], !is_object($data));
      return $response;
  }

  protected function orcaFundQuery($fund_id, $action) {
    $fund_id = (int)$fund_id;
    /** @var FinanceReportingProcessor $financeReportingProcessor */
    $data = new stdClass();
    $orcaProcessor = CampaignFinance::service()->getOrcaProcessor();
    $parameters = $_GET;
    $parameters['fund_id'] = $fund_id;
    $user = $this->getCurrentUser();
    switch ($action) {
      case 'fund':
        $data->fund = $orcaProcessor->getFundData($fund_id);
        break;
      case 'accounts':
        $data->accounts = $orcaProcessor->orcaQuery('get-accounts-by-fund-id',  ['fund_id' => $fund_id]);
        break;
      case 'contributions':
        $data->contributions = $orcaProcessor->orcaQuery('contribution-search',  $parameters);
        break;
      case 'contact-transactions':
        $data = $orcaProcessor->executeJsonFunction('cf_get_contact_transactions(:fund_id, :contact_id)', $parameters);
        break;
      case 'validate-contact':
        $data = $orcaProcessor->validateContact($fund_id, $parameters);
        break;
      case 'backup-api':
        $data->api = $orcaProcessor->getBackupAPI($fund_id, $user);
        break;
      default:
        $data->$action = $orcaProcessor->orcaQuery($action, $parameters);
        break;
    }
    // if we don't throw an exception, we succeeded
    $data->success = true;
    return $data;
  }

  /**
   * Contains ORCA related committee functions.
   *
   * Used for committee options that are unrelated to a fund id.
   */
  public function committeeAPI(Request $request, $committee_id, $action) {
    $data = new stdClass();
    $data->errors = NULL;
    $data->success = TRUE;

    /** @var OrcaProcessor $orcaProcessor  */
    $orcaProcessor = CampaignFinance::service()->getOrcaProcessor();

    $this->prepareRequest($request);
    $method = $this->method;
    $request_data = $this->request_data;
    $user_name = $this->getCurrentUser()->user_name;

    try {
      //Call correct OrcaProcessor method based on object and request method
      switch (true) {
        case ($action == 'fund' && $method == 'POST' && $committee_id):
          $data = $orcaProcessor->generateOrcaFund($committee_id, $request_data, $user_name);
          $data->success = true;
          break;
        case ($action == 'create-campaign' && $method == 'POST' && $committee_id):
          $data->fund_id = $orcaProcessor->createOrcaCampaign($committee_id, $request_data);
          break;
        case ($action == 'funds' && $method == 'GET'):
          $data->funds = $orcaProcessor->orcaQuery('committee-funds', ['committee_id' => $committee_id]);
          $data->success = true;
          break;
        case ($action == 'committee' && $method == 'GET'):
          $data->committee = $orcaProcessor->orcaQuery('committee', ['committee_id' => $committee_id]);
          $data->success = true;
          break;
        case ($action == 'get-committee-api' && $method == 'GET'):
          $data->api = $orcaProcessor->getCommitteeAPI($committee_id, $this->getCurrentUser());
          $data->success = true;
          break;
        case ($method === 'GET' && $action === 'get-committee-treasurers'):
          $data = $orcaProcessor->getCommitteeTreasurers($committee_id);
          break;
        default:
          throw new Exception("Invalid API Call ");
      }
    } catch (Exception $e) {
      $data->error = json_encode($e->getMessage());
      $data->success = false;
      Drupal::logger('orca')->error($e->getMessage());
    }

    $response = new JsonResponse($data);
    return $response;

  }

  /**
   * Contains ORCA related committee functions.
   *
   * Used for committee options that are unrelated to a fund id.
   */
  public function generalAPI(Request $request, $action) {
    $data = new stdClass();
    $data->errors = NULL;
    $data->success = TRUE;

    /** @var OrcaProcessor $orcaProcessor  */
    $orcaProcessor = CampaignFinance::service()->getOrcaProcessor();

    $method = $request->getMethod();
    $request_data = NULL;

    if ($method== 'POST' || $method == 'PUT') {
      $payload = file_get_contents('php://input');
      $request_data = json_decode($payload, false);
    }
    $params = $_GET;



    try {
      $pdc_user = $this->getCurrentUser();
      CampaignFinance::service()->getCommitteeManager()->setCurrentUser($pdc_user);

      $params['current_uid'] = $pdc_user->uid;
      $params['current_realm'] = $pdc_user->realm;
      //Call correct OrcaProcessor method based on object and request method
      switch (true) {
        case ($action == 'setup-committees-list' && $method='GET'):
          $data->committees = $orcaProcessor->orcaQuery('setup-committees-list', $params);
          $data->success = TRUE;
          break;
        case ($action == 'setup-check-existing-fund' && $method=='GET'):
          $data->fund = $orcaProcessor->orcaQuery('setup-check-existing-fund', $params)[0] ?? null;
          $data->success = TRUE;
          break;
        case ($action == 'get-occupations-list'):
          $data->{'get-occupations-list'} = $orcaProcessor->orcaQuery('get-occupations-list', $params);
          $data->success = true;
          break;
        case ($action == 'fund' && $method == 'POST'):
          $data = $orcaProcessor->generateOrcaFundExisting($request_data, $pdc_user->user_name);
          $data->success = true;
          break;
        case ($action == 'request-fund-access' && $method == 'POST'):
          $data = $orcaProcessor->requestFundAccess($request_data, $pdc_user);
          break;
        case ($action == 'get-fund-id-by-filer-id'):
          $data = $orcaProcessor->getFundIdByFilerIdAndElection($params['filer_id'], $params['election_year']);
          break;
        case ($action  == 'account-group' && $method=='GET'):
          $data->{'account-group'} = $orcaProcessor->getAccountGroup($params['acctnum']?? null);
          $data->success = !empty($data->{'account-group'});
          break;
        default:
          throw new Exception("Invalid API Call ");
      }

    } catch (Exception $e) {
      $data->error = json_encode($e->getMessage());
      $data->success = false;
      Drupal::logger('orca')->error($e->getMessage());
    }

    $response = new JsonResponse($data);
    return $response;

  }

  /**
   * Download campaign backup file from S3.
   *
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function downloadArchive($fund_id) {
    $fund_id = (int)$fund_id;
    $response = new \stdClass();
    $response->success = TRUE;
    $orcaProcessor = CampaignFinance::service()->getOrcaProcessor();
    if ($url = $orcaProcessor->getS3BackupUrl($fund_id)) {
        return new Drupal\Core\Routing\TrustedRedirectResponse($url, 307);
    } else {
        $response->success = FALSE;
        throw new NotFoundHttpException();
    }
  }
}
