<?php

namespace Drupal\orca\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use WAPDC\CampaignFinance\CampaignFinance;
use WAPDC\Core\UserManager;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\Util\Environment;
use WAPDC\Core\Model\User;

/**
 * An implementation of AccessInterface to check that a pdc_user
 * has access to a committee.
 *
 * Class CommitteeAuthorizationCheck
 *
 * @package Drupal\wapdc_registration\Access
 */
class FundAuthorizationCheck implements AccessInterface {

  protected $realm = 'apollo';

  public function access(AccountInterface $account, $fund_id) {
      if($account->hasPermission('access all private data'))
      {
          return AccessResult::allowed();
      }

      $has_access = false;

    // This access check is too early for the regular environment
    // so we need to do it here before doing the access check.
    $dir = \Drupal::service('file_system')->realpath("private://");
    $env = !empty($_ENV['PANTHEON_ENVIRONMENT']) ? $_ENV['PANTHEON_ENVIRONMENT'] : 'ddev';
    $file = "$dir/environment.$env.yml";
    Environment::loadFromYml($file);

    if($account->hasPermission('access wapdc data')){
        $pdc_user = new User();
        $pdc_user->uid = 'PDC';
        $pdc_user->realm = 'PDC';
        if(CampaignFinance::service()->fundAuthorizationCheck($fund_id, $pdc_user)) {
              return AccessResult::allowed();
        }
    }

    if (isset($_SESSION['SAW_USER_GUID'])) {
      $this->realm = 'saml_saw';
      $uid = $_SESSION['SAW_USER_GUID'];
    }
    else {
      $uid = $account->id();
    }
    $userManager = new UserManager(CoreDataService::service()
      ->getDataManager(), $this->realm);
    /** @var \WAPDC\Core\Model\User $user */
    $user = $userManager->getRegisteredUser($uid);

    $has_access = CampaignFinance::service()
      ->FundAuthorizationCheck($fund_id, $user);
    return AccessResult::allowedIf($has_access);
  }
}