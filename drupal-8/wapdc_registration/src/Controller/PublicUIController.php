<?php


namespace Drupal\wapdc_registration\Controller;


use Drupal\wapdc_core\Controller\SPAControllerBase;
use WAPDC\CampaignFinance\CampaignFinance;

class PublicUIController extends SPAControllerBase {

  public function getApplicationDirectory() {
    return CampaignFinance::service()->project_dir . '/ui/public/dist';
  }

}