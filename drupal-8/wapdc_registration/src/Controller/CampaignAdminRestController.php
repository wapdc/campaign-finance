<?php

namespace Drupal\wapdc_registration\Controller;

use Exception;
use Drupal\wapdc_core\Controller\CoreControllerBase;
use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WAPDC\CampaignFinance\BulkMailer;
use WAPDC\CampaignFinance\CampaignFinance;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\Model\User;
use WAPDC\Core\RIPService;
use WAPDC\Core\SendGrid\Drupal8CMS;
use WAPDC\Core\SendGrid\Mail;
use WAPDC\CampaignFinance\IEProcessor;

class CampaignAdminRestController extends CoreControllerBase {

  public function query($sql_base) {
    $result = new \stdClass();
    $result->errors = [];
    $result->success = TRUE;
    $sql_file = $sql_base . '.sql';
    $parms = $_GET;
    $parms['current_user'] = $this->currentUser()->id();
    $parms['current_realm'] = 'apollo';

    try {
      $result->data = CampaignFinance::service()->adminQuery($sql_file, $parms);
    }
    catch (Exception $e) {
      $result->errors[] = $e->getMessage();
      $result->success = FALSE;
    }
    $response = new JsonResponse($result);
    return $response;
  }

  public function IeSponsors(Request $request)
  {
    $data = new stdClass();
    $IEProcessor = CampaignFinance::service()->getIEProcessor();

    try {
      $data->data = $IEProcessor->getReports($_GET['election_year']);
      $data->success = true;
    } catch (Exception $e) {
      $data->data = null;
      $data->error = $e->getMessage();
      $data->success = false;
    }
    return new JsonResponse($data);
  }

  /**
   * @param Request $request
   * @param $repno
   * @return JsonResponse
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function IeReport(Request $request, $repno = null): JsonResponse
  {
    $result = new stdClass();
    $status = 200;
    try {
      switch($request->getMethod()) {
        case 'GET':
          $IEProcessor = CampaignFinance::service()->getIEProcessor();
          $result->data = $IEProcessor->getAllReportData($repno);
          $result->success = !isset($result->data->error);
          break;
        case 'DELETE':
          // Deleting requires connecting to the legacy database. This functionality needs to be implemented differently.
          $result->error = 'This function is not available. This action requires an IT ticket';
          $status = 403;
          $result->success = false;
          break;
      }
    } catch (Exception $e) {
      $result->data = null;
      $result->error = $e->getMessage();
      $result->success = false;
    }
    return new JsonResponse($result, $status);
  }

  public function createCommittee(){
    $data = new \stdClass();
    $data->errors = [];
    $data->success = true;
    $raw_data = file_get_contents('php://input');
    $input_data = json_decode($raw_data);

    try {
      $user = $this->getCurrentUser();
      $cm = CampaignFinance::service()->getCommitteeManager();
      $cm->setCurrentUser($user);
      $data->data = $cm->createCommitteeFromData(get_object_vars($input_data), $user);
    } catch (Exception $e){
      $data->errors[] = $e->getMessage();
      $data->success = false;
    }

    $response = new JsonResponse($data);
    return $response;
  }

  /**
   * @return JsonResponse
   * @throws Exception
   */
  function registration(){
    $result = new \stdClass();
    $result->errors = [];
    $result->success = true;

    if (!empty($_GET['registration_id'])) {
      try {
        $result->data = CampaignFinance::service()
          ->getRegistrationManager()
          ->getRegistrationSubmission($_GET['registration_id']);
      } catch (Exception $exception) {
        $result->errors[] = $exception->getMessage();
        $result->success = false;
      }
    }
    $response = new JsonResponse($result);
    return $response;
  }

  /**
   * @param $action
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws Exception
   */
  function adminProcess($action) {
    $result = new \stdClass();
    $raw_data = file_get_contents('php://input');
    $input_data = json_decode($raw_data);
    $result->errors = [];
    $result->success = true;
    $user = $this->getCurrentUser(FALSE);
    $cm = CampaignFinance::service()->getCommitteeManager();
    $rm = CampaignFinance::service()->getRegistrationManager();
    $cm->setCurrentUser($user);
    $email = $this->currentUser()->getEmail();
    try {
      switch ($action) {
        case 'update-candidacy-status':
          $result->data = $cm->updateCandidacyStatus($input_data);
          break;
        case 'update-admin-memo':
          $result->data = $cm->updateAdminMemo($input_data);
          break;
        case 'mini-full-permission':
          $cm->miniFullPermission($input_data->committee_id, $input_data->mini_full_permission, $user);
          break;
        case 'update-committee':
          $return_object = $cm->updateCommittee($input_data);
          if (!$return_object->success) {
            throw new Exception($return_object->message);
          } else {
            $result->data = $return_object->data;
          }
          break;
        case 'save-admin-data':
          $rm->saveAdminData($input_data);
          break;
        case 'unverify-registration':
          $rm->unverifyRegistration($input_data->registration_id, $input_data->delete_filer_id, $user->user_name);
          break;
        case 'verification-options':
          $result->data = $rm->getVerificationOptions($input_data);
          break;
        case 'verify-committee':
          if ($input_data && $input_data->registration_id) {
           $rm->saveAdminData($input_data);
          }
          $send_notification = !empty($input_data->skip_notification)  ? false : true;
          $committee = $rm->setMailer(new Mail(new Drupal8CMS(), $email))
            ->verifyRegistration($input_data->registration_id, false, $send_notification, $user->user_name);
          $result->data = $rm->getCurrentRegistration($committee->committee_id);
          break;
        case 'authorize-user':
          $email = $input_data->email;
          /** @var \WAPDC\Core\RDSDataManager $dm */
          $dm = CoreDataService::service()->getDataManager();
          $class = User::class;
          $foundUser = NULL;
          $foundUser = $dm->em->createQuery(
            "SELECT u FROM $class u WHERE UPPER(u.user_name) = UPPER(:email)"
          )->setParameter('email', $email)->getResult();

          if (!$foundUser){
            $result->data = "No user found with given email";
          }
          else {
            $cm->setCurrentUser($this->getCurrentUser());
            $cm->grantAccessByCommitteeId(
              $input_data->committee_id,
              $foundUser[0],
              $input_data->role,
              $input_data->reason
            );
            $result->data = true;
          }
          break;
        case 'revoke-user':
          $cm->revokeAllAccessByCommitteeId($input_data->committee_id, $input_data->user);
          break;
        case 'merge-committees':
          $needs_verify = $cm->mergeCommittees($input_data->sourceCommitteeId, $input_data->targetCommitteeId, $this->getCurrentUser()->user_name);
          if ($needs_verify) {
            $send_notification = !empty($input_data->skip_notification)  ? false : true;
            $rm->verifyRegistration($input_data->registration_id, false, $send_notification, $this->getCurrentUser()->user_name);
          }
          $result->data = $rm->getCurrentRegistration($input_data->targetCommitteeId);
          break;
        case 'revert-registration':
          $rm->revertRegistration($input_data, $this->getCurrentUser(false));
          break;
        case 'upload-request':
          $file_path = $input_data->registration_id . '/' . $input_data->file_name;
          $result->data = CampaignFinance::service()->getFileService()->getFilePostFormElements($file_path, 'public', $input_data->content_type);
          break;
        case 'add-attachment':
          $rm->addRegistrationAttachment($input_data);
          break;
        case 'edit-attachment':
          $rm->modifyRegistrationAttachment($input_data);
          break;
        case 'delete-attachment':
          $rm->deleteRegistrationAttachment($input_data);
          break;
        case 'delete-finance-report':
          if ($this->currentUser()->hasPermission('delete wapdc report')) {
            CampaignFinance::service()->getFundProcessor()->deleteSubmittedReport($input_data->report_id);
          } else {
            throw new Exception('Access denied.');
          }
          break;
        case 'mail-merge':
          $mailer = new BulkMailer(new Mail(new Drupal8CMS(), $email));
          if (!is_array($input_data->data)) {
            throw new Exception('Invalid mail data');
          }
          $result->data = $mailer->mailMerge($input_data->data, $input_data->options);
          break;
      }
    }
    catch (Exception $e) {
      $result->errors[] = $e->getMessage();
      $result->success = false;
    }
    $response = new JsonResponse($result);
    return $response;
  }

  /**
   * Move a property from an orm object to the stdClass variant
   * @param $object1
   * @param $object2
   * @param $property
   */
  private function _transferPropertyToOrm($object1, $object2, $property) {
    $lc_property = lcfirst($property);
    if (isset($object1->$property)) {
      if (empty($object1->$property) && $object1->$property!==0) {
        $object2->$lc_property = NULL;
      }
      else {
        $object2->$lc_property = trim($object1->$property);
      }
    }
  }

}
