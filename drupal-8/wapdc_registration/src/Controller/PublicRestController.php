<?php


namespace Drupal\wapdc_registration\Controller;


use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WAPDC\CampaignFinance\CampaignFinance;
use Symfony\Component\HttpFoundation\JsonResponse;


class PublicRestController extends ControllerBase {

  /**
   * @param $action
   * @return JsonResponse
   */
  function publicProcess($action) {
    $data = new \stdClass();
    $data->errors = [];
    $data->success = true;

    try {
      switch ($action) {
        case 'get-registration':
          if (!empty($_GET['registration_id'])) {
            $data->data = CampaignFinance::service()->getRegistrationManager()->getRegistrationSubmission($_GET['registration_id']);
            CampaignFinance::service()->getRegistrationManager()->addPublicCommitteeAttachmentsToSubmission($data->data);
          }
          break;
      }
    } catch (\Exception $exception) {
      $data->errors[] = $exception->getMessage();
      $data->success = false;
    }
    $response = new JsonResponse($data);
    return $response;
  }

  public function getCandidateXml() {
    $xml = '';
    $response = new Response();
    try {
      $days = (int)$_GET['days'] ?? 30;
          /** @var \WAPDC\CampaignFinance $committeeManager */
          $xml_string = CampaignFinance::service()->candidatesXML($days);
          $response = new Response((string) $xml_string);
          $response->headers->set('Content-Type', 'text/xml');
    } catch (\Exception $exception) {
        $response = new Response('invalid request', 500);
    }
    return $response;
  }

}