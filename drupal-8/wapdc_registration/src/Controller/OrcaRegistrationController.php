<?php


namespace Drupal\wapdc_registration\Controller;


use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use WAPDC\CampaignFinance\CampaignFinance;


class ORCARegistrationController extends ControllerBase {

  /**
   * @param $action
   * @return Response
   * @throws \Exception
   */
  function orcaProcess($action) {
    $data = new \stdClass();
    $request = Request::createFromGlobals();
    $token = $request->headers->get('x-api-key');
    try {
        switch ($action){
          case 'get':
            $data = CampaignFinance::service()
              ->getOrcaRegistrationManager()->getRegistrationXML($token);
            if (!$data) {
              return new Response($data, 404);
            }
            $response = new Response($data);
            $response->headers->set('Content-Type', 'xml');
            return $response;
          case 'id':
            $data = CampaignFinance::service()
              ->getCommitteeManager()->validateAPIToken($token);
              break;
          case 'filer':
            $data = CampaignFinance::service()
              ->getCommitteeManager()->getTokenFiler($token);
              break;
          case 'year':
            $committee_id = CampaignFinance::service()
              ->getCommitteeManager()->validateAPIToken($token);
            $data = CampaignFinance::service()->getOrcaRegistrationManager()->getCampaignElectionYear($committee_id);
            break;
        }
    } catch (\Exception $exception) {
        $data->errors[] = $exception->getMessage();
        $data->success = false;
    }
    $response = new Response($data);
    return $response;
  }

  /**
   * @param $action
   * @return JsonResponse
   */
  function getRegistrationByAuthToken() {
    $data = new \stdClass();
    $request = Request::createFromGlobals();
    $token = $request->headers->get('x-api-key');
    $status = Response::HTTP_OK;
    try {
      $data->data = CampaignFinance::service()
        ->getRegistrationManager()->getCurrentRegistrationJSONByToken($token);
      if (!$data) {
        $data->errors = ["No committee matching the provided authentication token"];
        $data->success = false;
        $status = Response::HTTP_NOT_FOUND;
      }
    } catch (\Exception $exception) {
      $data->errors[] = $exception->getMessage();
      $data->success = false;
      $status = Response::HTTP_INTERNAL_SERVER_ERROR;
    }
    $response = new JsonResponse($data, $status);
    return $response;

  }
}