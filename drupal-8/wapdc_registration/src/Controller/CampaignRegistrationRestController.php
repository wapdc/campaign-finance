<?php


namespace Drupal\wapdc_registration\Controller;

use Drupal\wapdc_core\Controller\CoreControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use WAPDC\CampaignFinance\CampaignFinance;
use WAPDC\CampaignFinance\SubmissionErrors;
use WAPDC\Core\SendGrid\Drupal8CMS;
use WAPDC\Core\SendGrid\Mail;
use \Exception;

class CampaignRegistrationRestController extends CoreControllerBase {

  public function query($sql_base) {
    $data = new \stdClass();
    $data->errors = [];
    $data->success = TRUE;
    $sql_file = $sql_base . '.sql';
    $parms = $_GET;
    $current_user = $this->getCurrentUser();
    $parms['current_user'] = $current_user->uid;
    $parms['current_realm'] = $current_user->realm;

    try {
      $data->data = CampaignFinance::service()->campaignRegistrationQuery($sql_file, $parms);
    }
    catch (\Exception $e) {
      $data->errors[] = $e->getMessage();
      $data->success = FALSE;
    }
    $response = new JsonResponse($data);
    return $response;
  }


  public function auth_check(){
    $response = new \stdClass();
    $http_status = 200;
    $response->errors = [];
    $response->success = TRUE;


    try {
      /** @var \Drupal\user\Entity\User $user */
      $user = $this->currentUser();
      $response->authenticated = !empty($user->id());
      $response->permissions = new \stdClass();
      $response->permissions->administer_wapdc_campaign_finance = $user->hasPermission("administer wapdc campaign finance");
      $response->permissions->enter_wapdc_data = $user->hasPermission("enter wapdc data");
      $response->permissions->access_wapdc_data = $user->hasPermission("access wapdc data");
      $response->permissions->delete_wapdc_report = $user->hasPermission("delete wapdc report");
      $response->permissions->orca_online = $user->hasPermission("orca online");
      $response->permissions->access_all_private_data = $user->hasPermission("access all private data");
      $response->permissions->delete_orca_campaign = $user->hasPermission("delete orca campaign");
      $response->permissions->lmc_access = $user->hasPermission("lmc access");
      if(!empty($user->id())){
        $response->email = $user->getEmail();
      }
    }
    catch (\Exception $exception) {
      $response->errors['exception.unknown'] = $exception->getMessage();
      \Drupal::logger('pdc_campaign_registration')->error("An error has occurred: " . $exception->getMessage());
      $response->success = FALSE;
      $http_status = 500;
    }
    try {
      if(!empty($user->id())) {
        /** @var \WAPDC\Core\Model\User $pdc_user */
        $pdc_user = $this->getCurrentUser();
        CampaignFinance::service()->getCommitteeManager()->autoGrantCommitteeAuthorization($pdc_user->uid, $user->getAccountName(), $this->realm);
      }
    }
    catch (\Exception $exception) {
      \Drupal::logger('pdc_campaign_registration')->error("An error has occurred: " . $exception->getMessage());
    }
    return new JsonResponse($response, $http_status);
  }

  public function committeeTokens($committee_id) {
    $data = new \stdClass();
    $data->errors = [];
    $data->success = true;
    $parms = $_GET;
    $current_user = $this->getCurrentUser();
    $parms['current_user'] = $current_user->uid;
    $parms['current_realm'] = $current_user->realm;
    $parms['committee_id'] = $committee_id;

    try {
      $data->data = CampaignFinance::service()->adminQuery('committee_api_keys.sql', $parms);
    }
    catch (\Exception $e) {
      $data->errors[] = $e->getMessage();
      $data->success = FALSE;
    }
    $response = new JsonResponse($data);
    return $response;
  }

  public function getAuthorizedUsers($committee_id) {
    $data = new \stdClass();
    $data->errors = [];
    $data->success = true;
    $parms = $_GET;
    $parms['committee_id'] = $committee_id;

    try {
      $data->data = CampaignFinance::service()->adminQuery('authorized_users.sql', $parms);
    }
    catch (\Exception $e) {
      $data->errors[] = $e->getMessage();
      $data->success = false;
    }
    return new JsonResponse($data);
  }

  function committee($committee_id) {
    $data = new \stdClass();
    $data->errors = [];
    $data->success = true;

    if (empty($committee_id)) {
      $data->errors[] = 'Committee ID is required to get current registration.';
      $data->success = false;
    }
    try {
      $data->data = CampaignFinance::service()->getCommitteeManager()->getCommittee($committee_id);
    } catch (Exception $exception) {
      $data->errors[] = $exception->getMessage();
      $data->success = false;
    }
    $response = new JsonResponse($data);
    return $response;
  }

  function currentRegistration($committee_id) {
    $data = new \stdClass();
    $data->errors = [];
    $data->success = true;

    if (empty($committee_id)) {
      $data->errors[] = 'Committee ID is required to get current registration.';
      $data->success = false;
    }
    try {
      $admin = $this->currentUser()->hasPermission("administer wapdc campaign finance");
      $data->data = CampaignFinance::service()->getRegistrationManager()->getCurrentRegistration($committee_id, $admin);
      $draft = CampaignFinance::service()->getRegistrationDraftManager()->getCommitteeRegistrationDraft($committee_id);
      if ($draft) {
        $data->data->draft = true;
      } else {
        $data->data->draft = false;
      }
    } catch (\Exception $exception) {
      $data->errors[] = $exception->getMessage();
      $data->success = false;
    }
    $response = new JsonResponse($data);
    return $response;
  }

  public function createCommittee(){
    $data = new \stdClass();
    $data->errors = [];
    $data->success = true;
    $raw_data = file_get_contents('php://input');
    $input_data = json_decode($raw_data);

    try {
      $user = $this->getCurrentUser();
      $cm = CampaignFinance::service()->getCommitteeManager();
      $cm->setCurrentUser($user);
      $data->data = $cm->createCommitteeFromData(get_object_vars($input_data), $user);
    } catch (\Exception $e){
      $data->errors[] = $e->getMessage();
      $data->success = false;
    }
    $response = new JsonResponse($data);
    return $response;
  }

  public function committeeDraft($committee_id) {
    $data = new \stdClass();
    $data->errors = [];
    $data->success = true;
    $admin = $this->currentUser()->hasPermission("administer wapdc campaign finance");
    if ($committee_id) {
      try {
        $draft = CampaignFinance::service()
          ->getRegistrationDraftManager()->getCommitteeRegistrationDraft($committee_id);
        if ($draft) {
          $draftErrors = new SubmissionErrors();
          CampaignFinance::service()->getRegistrationDraftManager()->validateDraft($draft, $draftErrors);
          $draft->draftErrors = $draftErrors->getErrors();
          $data->data = $draft;
        } else {
          $registration = CampaignFinance::service()->getRegistrationManager()->getCurrentRegistration($committee_id, $admin);
          $data->data = $registration;
        }
      } catch (\Exception $e) {
        $data->errors = $e->getMessage();
        $data->success = false;
      }
    }
    $response = new JsonResponse($data);
    return $response;
  }


  /**
   * @param $action
   * @return JsonResponse
   */
  public function processSubmission($committee_id, $action) {
    // Response data
    $data = new \stdClass();
    $raw_data = file_get_contents('php://input');
    $input_data = json_decode($raw_data);
    $authUser = $this->currentUser()->hasPermission("administer wapdc campaign finance");;


    $email = $this->currentUser()->getEmail();
    if($authUser) {
      $authEmail = $input_data->certification_email ?? '';
    } else {
      $authEmail = $this->currentUser()->getEmail();
    }

    try {
      $user = $this->getCurrentUser(FALSE);
      if (!empty($input_data->committee->committee_id)) {
        if ($input_data->committee->committee_id != $committee_id) {
          throw new \Exception('Invalid Committee ID');
        }
      }

      switch ($action) {
        case 'log-registration-action':

          CampaignFinance::service()->getRegistrationManager()->logRegistrationAction(
            $input_data->registration_id,
            $email,
            $input_data->action,
            $input_data->message
          );
          $data->errors = '';
          $data->success = true;
          break;
        case 'save-draft':
          $draftErrors = CampaignFinance::service()->getRegistrationDraftManager()->saveCommitteeRegistrationDraft($input_data, $user);
          $data->data = $draftErrors->getErrors();
          $data->errors = [];
          $data->success = true;
          break;
        case 'cancel-edit':
          $data->data = CampaignFinance::service()->getRegistrationDraftManager()->deleteDraft($committee_id, $user->user_name);
          $data->success = true;
          break;
        case 'register':
          if (!$this->currentUser()->hasPermission('administer wapdc campaign finance')) {
            $input_data->submitted_at = new \DateTime();
            $input_data->submitted_at = $input_data->submitted_at->format('Y-m-d H:i:s');
          }
          CampaignFinance::service()->getRegistrationManager()
            ->setMailer(new Mail(new Drupal8CMS(), $email))
            ->createRegistrationFromSubmission($input_data, $authEmail, $user->user_name);
          $data->errors = '';
          $data->success = true;
          break;
        case 'amend':
          $registration_id = $input_data->registration_id ?? null;
          $data->data = CampaignFinance::service()->getRegistrationManager()->amendRegistration($registration_id, $input_data->committee_id, $user);
          $data->errors = '';
          $data->success = true;
          break;
        case 'copy':
          $data->data = CampaignFinance::service()->getRegistrationDraftManager()->copyCommitteeRegistrationDraftToNewCommittee($input_data->registration, $input_data->electionCode, $user);
          $data->errors = '';
          $data->success = true;
          break;
        case 'generate-api-token':
          $data->data = CampaignFinance::service()->getCommitteeManager()->generateAPIToken($committee_id, $input_data->memo, $this->currentUser()->getAccountName());
          $data->errors = '';
          $data->success = true;
          break;
        case 'revoke-api-token':
          CampaignFinance::service()->getCommitteeManager()->revokeAPIToken($input_data->api_key_id, $this->currentUser()->getAccountName());
          $data->errors = '';
          $data->success = true;
          break;
      }
    }
    catch (\Exception $e) {
      $data->errors = $e->getMessage();
      $data->success = false;
    }
    $response = new JsonResponse($data);
    return $response;
  }
}
