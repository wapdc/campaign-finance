<?php

namespace Drupal\wapdc_registration\Access;

use \Exception;
use WAPDC\Core\Model\User;
use WAPDC\Core\UserManager;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\Util\Environment;

class DrupalUserManager {

  protected string $realm = 'apollo';

  /**
   * This access check is too early for the regular environment,
   * so we need to do it here before doing the access check.
   *
   * @throws Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function getDrupalUser($account): ?User
  {
    $dir = \Drupal::service('file_system')->realpath("private://");
    $env = !empty($_ENV['PANTHEON_ENVIRONMENT']) ? $_ENV['PANTHEON_ENVIRONMENT'] : 'ddev';
    $file = "$dir/environment.$env.yml";
    Environment::loadFromYml($file);

    if (isset($_SESSION['SAW_USER_GUID'])) {
      $this->realm = 'saml_saw';
      $uid = $_SESSION['SAW_USER_GUID'];
    } else {
      $uid = $account->id();
    }
    $userManager = new UserManager(CoreDataService::service()
      ->getDataManager(), $this->realm);
    /** @var User $user */
    $user = $userManager->getRegisteredUser($uid);
    return $user;
  }
}