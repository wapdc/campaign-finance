<?php

namespace Drupal\wapdc_registration\Access;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\ORMException;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use WAPDC\CampaignFinance\CampaignFinance;

/**
 * An implementation of AccessInterface to check that a pdc_user
 * has access to a report.
 *
 * Class ReportAuthorizationCheck
 *
 * @package Drupal\wapdc_registration\Access
 */
class ReportAuthorizationCheck implements AccessInterface {

  protected $realm = 'apollo';

  /**
   * @throws Exception
   * @throws DBALException
   * @throws ORMException
   * @throws \Doctrine\DBAL\Exception
   */
  public function access(AccountInterface $account, $report_id) {
    if($account->hasPermission('access wapdc data')){
      return AccessResult::allowed();
    }
    $has_access = false;

    $drupalUserManager = new DrupalUserManager();
    $user = $drupalUserManager->getDrupalUser($account);

    if ($user) {
      $has_access = CampaignFinance::service()
        ->reportAuthorizationCheck($report_id, $user);
    }
    return AccessResult::allowedIf($has_access);
  }
}