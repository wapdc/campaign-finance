<?php

namespace Drupal\wapdc_registration\Access;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\ORMException;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use WAPDC\CampaignFinance\CampaignFinance;

/**
 * An implementation of AccessInterface to check that a pdc_user
 * has access to a draft report.
 *
 * Class DraftReportAuthorizationCheck
 *
 * @package Drupal\wapdc_registration\Access
 */
class DraftReportAuthorizationCheck implements AccessInterface {

  /**
   * @throws Exception
   * @throws DBALException
   * @throws ORMException
   * @throws \Doctrine\DBAL\Exception
   * @throws \Exception
   */
  public function access(AccountInterface $account, $draft_id) {
    if($account->hasPermission('access wapdc data')){
      return AccessResult::allowed();
    }
    $has_access = false;
    $drupalUserManager = new DrupalUserManager();
    $user = $drupalUserManager->getDrupalUser($account);
    if ($user) {
      $has_access = CampaignFinance::service()->getVendorSubmissionProcessor()->draftReportAuthorizationCheck($draft_id, $user);
    }
    return AccessResult::allowedIf($has_access);
  }
}