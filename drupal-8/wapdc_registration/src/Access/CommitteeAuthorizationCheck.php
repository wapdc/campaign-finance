<?php

namespace Drupal\wapdc_registration\Access;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use WAPDC\CampaignFinance\CampaignFinance;

/**
 * An implementation of AccessInterface to check that a pdc_user
 * has access to a committee.
 *
 * Class CommitteeAuthorizationCheck
 *
 * @package Drupal\wapdc_registration\Access
 */
class CommitteeAuthorizationCheck implements AccessInterface {

  protected $realm = 'apollo';

  /**
   * @throws OptimisticLockException
   * @throws Exception
   * @throws ORMException
   * @throws DBALException
   * @throws TransactionRequiredException
   * @throws \Doctrine\DBAL\Exception
   */
  public function access(AccountInterface $account, $committee_id) {
    if($account->hasPermission('access wapdc data')){
      return AccessResult::allowed();
    }
    $has_access = false;

    $drupalUserManager = new DrupalUserManager();
    $user = $drupalUserManager->getDrupalUser($account);

    if ($user) {
      $has_access = CampaignFinance::service()
        ->CommitteeAuthorizationCheck($committee_id, $user);
    }

    return AccessResult::allowedIf($has_access);
  }
}