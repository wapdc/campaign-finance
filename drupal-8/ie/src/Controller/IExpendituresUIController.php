<?php


namespace Drupal\ie\Controller;


use Drupal\wapdc_core\Controller\SPAControllerBase;
use \WAPDC\CampaignFinance\CampaignFinance;

class IExpendituresUIController extends SPAControllerBase {

  public function getApplicationDirectory() {
    return CampaignFinance::service()->project_dir . '/ui/ie/dist';
  }

}