<?php

namespace Drupal\ie\Controller;

use Doctrine\ORM\ORMException;
use Drupal\wapdc_core\Controller\CoreControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WAPDC\CampaignFinance\CampaignFinance;
use stdClass;
use Exception;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\DBAL\Driver\Exception as DriverException;
use WAPDC\CampaignFinance\IEProcessor;
use Drupal\Core\Routing\TrustedRedirectResponse;
use WAPDC\Core\SendGrid\Mail;
use WAPDC\Core\SendGrid\NoCMS;

class IExpendituresController extends CoreControllerBase
{
  protected $response_data;
  protected $response_status;

  /**
   * Get the current drupal user permissions and user data
   *   for the current user
   * @param Request $request
   * @return Response
   */
  public function getIEUser(Request $request): Response
  {
    $this->prepareRequest($request);
    $result = $this->response_data;
    try {
      $drupal_user = $this->currentUser();
      $result->authenticated = !empty($drupal_user->id());
      $result->permissions = new stdClass();
      $result->permissions->administer_wapdc_campaign_finance = $drupal_user->hasPermission("administer wapdc campaign finance");
      $result->permissions->enter_wapdc_data = $drupal_user->hasPermission("enter wapdc data");
      $result->permissions->access_wapdc_data = $drupal_user->hasPermission("access wapdc data");
      $result->permissions->delete_wapdc_report = $drupal_user->hasPermission("delete wapdc report");
      if (!empty($drupal_user->id())) {
        $result->email = $drupal_user->getEmail();
      }
      $result->success = true;
      $this->response_status = 200;
    } catch (Exception $e) {
      $this->setError($e->getMessage());
    }
    return $this->getResponse();
  }

  public function redirectToC6($entity_id, $sponsor_id): TrustedRedirectResponse|Response
  {
    try {
      $IEProcessor = CampaignFinance::service()->getIEProcessor();

      $url = $IEProcessor->redirectToLegacyC6($entity_id,$sponsor_id);
      if ($url) {
        return new TrustedRedirectResponse($url, 301);
      } else {
        return new Response('Not found', 404);
      }
    } catch(Exception $e) {
      return new Response('Error redirecting to C6: '. $e->getMessage(), 500);
    }
  }

  /**
   * @param Request $request
   * @param string $action
   * @return JsonResponse|Response
   * @throws ORMException
   * @throws DBALException
   * @throws Exception
   */
  public function filerAPI(Request $request, string $action = ''): JsonResponse|Response
  {
    $pdc_user = $this->getCurrentUser(true);
    $this->prepareRequest($request);
    $data = new stdClass();
    $parameters = $_GET;
    if ($this->method === 'GET') {
      $parameters['uid'] = $pdc_user->uid;
      $parameters['realm'] = $pdc_user->realm;
    }
    if ($this->request_data) {
      $this->request_data->uid = $pdc_user->uid;
      $this->request_data->realm = $pdc_user->realm;
    }

    $IEProcessor = CampaignFinance::service()->getIEProcessor();
    switch (true) {

      case ($this->method == 'POST' && $action == 'c6-sponsor-request'):
        $data->sponsor_request_id = $IEProcessor->requestAccess($this->request_data);
        $data->success = ($data->sponsor_request_id > 0);
        break;

      case ($this->method === 'GET' && $action == 'my-sponsors'):
        $data->sponsors = $IEProcessor->IEQuery('sponsors-i-file-for', $parameters);
        $data->sponsorRequests = $IEProcessor->IEQuery('sponsor-requests', $parameters);
        break;

      case ($this->method === 'GET'):
        $data->queryResult = $IEProcessor->IEQuery($action, $parameters);
        $data->success = true;
        break;

      default:
        $data->error = 'Invalid API CALL';
        $data->success = false;
    }
    return new JsonResponse($data);
  }

  /**
   * @param $entity_id
   * @param Request $request
   * @param string $action
   * @return JsonResponse
   */
  public function entityAPI($entity_id, Request $request, string $action=''): JsonResponse
  {
    $this->prepareRequest($request);
    if ($this->request_data) {
      $this->request_data->entity_id = (int)$entity_id;
    }
    $data = new stdClass();
    try {
      $IEProcessor = CampaignFinance::service()->getIEProcessor();
      switch (true) {
        case ($this->method == 'POST' && $entity_id && $action == 'enable-c6-for-entity'):
          $sponsor_id = $IEProcessor->enableC6ForEntity($entity_id, $this->request_data->sponsor_type ?? null);
          $data->sponsor_id = $sponsor_id;
          $data->success = !empty($sponsor_id);
          if ($data->success) {
            CampaignFinance::service()->getCommitteeManager()->synchronizeEntityAccess($entity_id);
          }
          break;
        case ($this->method == 'PUT' && $entity_id && empty($action)):
          $sponsor_id = $IEProcessor->updateSponsorInfo($this->request_data);
          $data->sponsor_id = $sponsor_id;
          $data->success =!empty($sponsor_id);
          break;
        default:
          throw new Exception("Unknown API call:" . $action);
      }
    }
    catch (Exception $e) {
      $data->error = $e->getMessage();
      $data->sucess = false;
    }
    return new JsonResponse($data);
  }

  /**
   * @param Request $request
   * @param string $action
   * @return JsonResponse|Response
   * @throws DriverException
   */
  public function adminApi(Request $request, string $action=''): JsonResponse|Response
  {
    $this->prepareRequest($request);
    $data = new stdClass();
    try {
      $IEProcessor = CampaignFinance::service()->getIEProcessor();
      $parameters = $_GET;
      $status = 200;
      switch (true) {
        case ($this->method === 'POST' && $action=='verify-sponsor'):
          $mailer = new Mail(new NoCMS());
          $this->request_data->testEmail = $this->currentUser()->getEmail();
          $sponsor_id = $IEProcessor->verifySponsor($this->request_data->sponsor_request_id, $this->request_data,
            $mailer, $this->request_data->testEmail);

          $data->sponsor_id = $sponsor_id;
          $data->success =!empty($sponsor_id);
          break;
        case ($this->method === 'GET'):
          $data->queryResult = $IEProcessor->IEAdminQuery($action, $parameters);
          $data->success = true;
          break;
        case ($this->method === 'POST' && $action=='deny-sponsor'):
          $IEProcessor->denySponsorRequest($this->request_data->sponsor_request_id);
          $data->success = true;
          break;
        default:
          $data->error = 'Invalid API Call!';
          $data->success = false;
          $status = 500;
      }
    }
    catch (Exception $e) {
      $data->error = $e->getMessage();
      $data->success = false;
      $status = 500;
    }
    return new JsonResponse($data, $status);
  }

}