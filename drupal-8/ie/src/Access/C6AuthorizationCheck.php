<?php

namespace Drupal\ie\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use WAPDC\CampaignFinance\CampaignFinance;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\UserManager;
use WAPDC\Core\Util\Environment;
use Symfony\Component\Routing\Route;

class C6AuthorizationCheck implements AccessInterface {
  protected $realm = 'apollo';

  public function access(AccountInterface $account, Route $route, $entity_id) {
    $role = $route->getRequirement('_c6_role');
    if($account->hasPermission('enter wapdc data')){
      return AccessResult::allowed();
    }
    $has_access = false;

    // This access check is too early for the regular environment
    // so we need to do it here before doing the access check.
    $dir = \Drupal::service('file_system')->realpath("private://");
    $env = !empty($_ENV['PANTHEON_ENVIRONMENT']) ? $_ENV['PANTHEON_ENVIRONMENT'] : 'ddev';

    $file = "$dir/environment.$env.yml";
    Environment::loadFromYml($file);

    if (isset($_SESSION['SAW_USER_GUID'])) {
      $this->realm = 'saml_saw';
      $uid = $_SESSION['SAW_USER_GUID'];
    }
    else {
      $uid = $account->id();
    }
    $userManager = new UserManager(CoreDataService::service()
      ->getDataManager(), $this->realm);
    /** @var \WAPDC\Core\Model\User $user */
    $user = $userManager->getRegisteredUser($uid);

    $has_access = CampaignFinance::service()
      ->getIEProcessor()->checkC6Access($entity_id, $role, $user);
    return AccessResult::allowedIf($has_access);
  }

}