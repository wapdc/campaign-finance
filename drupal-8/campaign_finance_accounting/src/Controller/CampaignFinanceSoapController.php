<?php

namespace Drupal\campaign_finance_accounting\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WAPDC\CampaignFinance\CampaignFinance;
use WAPDC\CampaignFinance\FinanceReportingProcessor;
use WAPDC\CampaignFinance\Reporting\OrcaReportParser;
use WAPDC\CampaignFinance\SoapEnvelope;
use \stdClass;
use \DateTime;
use \Exception;


class CampaignFinanceSoapController extends ControllerBase {

  /**
   * @param Request $request
   * SOAP service payload is zipped base 64 encoded payload
   *   The payload type is for ex: Body->Submit
   *     $xml->Body->OrcaExceptionLog
   *     $xml->Body->OrcaUpdate
   *     $xml->Body->Submit
   *     $xml->Body->PDCSubmit
   *
   * @return Response
   * @throws \Exception
   */
  public function soapAPI(Request $request) {
    $request_data = new stdClass;
    $method = $request->getMethod();
    $user = $this->currentUser();
    if (!$user || !($user->hasPermission('minke soap'))) {
      switch ($method) {
        case 'GET':
        case 'HEAD':
          $response = new Response('');
          $response->headers->set('Content-Type', 'Application/soap+xml');
          break;
        default:
          $envelope = new SoapEnvelope('Submit');
          $envelope->setError("This version of the PDC report submission service is no longer supported.\n");
          $xml = $envelope->asXML();
          $response = new Response($xml);
          $response->headers->set('Content-Type', 'Application/soap+xml');
      }
      return $response;
    }
    else {
      try {
        /** @var FinanceReportingProcessor $financeReportingProcessor */
        $financeReportingProcessor = CampaignFinance::service()
          ->getFinanceReportingProcessor();
        /** @var \WAPDC\CampaignFinance\CommitteeManager $committeeManager */
        $committeeManager = CampaignFinance::service()->getCommitteeManager();

        $envelope = null;
        $soap_method = null;
        if ($request->getMethod() == 'POST') {
          $payload = file_get_contents('php://input');
          $request_data = $this->soapParser($payload);
          $envelope = new SoapEnvelope($request_data->method);
          $soap_method = $request_data->method ?? null;
        }
        else {
          $envelope = new SoapEnvelope('Submit');
        }

        switch (true) {
          case ($request->getMethod() == 'HEAD'):
            $response = new Response('');
            $response->headers->set('Content-Type', 'Application/soap+xml');
            return $response;
            break;
          case isset($_GET['WSDL']):

            $wsdlFile = dirname(__DIR__) . '/Controller/wsdl.xml';

            $location = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $request->getPathInfo();

            $wsdl = simplexml_load_file($wsdlFile);
            $wsdl->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/wsdl/soap/');
            $wsdl->registerXPathNamespace('soap12', 'http://schemas.xmlsoap.org/wsdl/soap12/');

            $loc1 = $wsdl->xpath('//soap:address');
            $loc1[0]['location'] = $location;
            $loc1 = $wsdl->xpath('//soap12:address');
            $loc1[0]['location'] = $location;

            $response = new Response($wsdl->asXML());
            $response->headers->set('Content-Type', 'text/xml');
            return $response;
            break;

          case $soap_method === 'PDCSubmit':
          case $soap_method === 'Submit':
            $xml = $envelope->extractXml($request_data->xrfZipBase64);

            $ip_address= $request->getClientIp();
            $source = strpos(($request_data->version ?? ""), '.') ? 'ORCA' : 'vendor';
            $committee_id = $committeeManager->validateAPIToken($request_data->password1);
            if ($committee_id) {
              $submission_id = $financeReportingProcessor->logSubmission('xml', $xml, $source, $request_data->version, $committee_id, $ip_address);
              // Process the report
              $parser = new OrcaReportParser();
              $processor = CampaignFinance::service()->getFundProcessor();
              $report = $parser->parse($xml, $source);
              $report->election_year = $request_data->year;

              $report->filer_id = $request_data->filerId ?? $request_data->filer_id ?? null;
              if (!empty($request_data->amdno)) {
                $report->amends = $request_data->amdno;
              }
              $report->submitted_at = (new DateTime())->format('Y-m-d');
              $vendor = $request_data->version == '100' ? 'Unknown vendor' : 'PDC';
              $response = $processor->submitCommitteeReport($committee_id, $report, $request_data->email ?? null, $vendor);
              if ($response->success) {
                $envelope->setResponse([
                  'Repno' => $response->report_id,
                  'Message' => $response->message,
                ]);
              } elseif (!empty($response->messages['duplicate-report'])) {
                $envelope->setError($response->message, 'DuplicateReportException');
              } else {
                $envelope->setError($response->message);
              }
              //Log submission success status and message
              $financeReportingProcessor->logProcessResults($submission_id, $response->success, $response->message, $response->report_id ?? NULL, $response->fund_id ?? NULL);
            } else {
              $envelope->setError('Invalid token', 'FilerLoginException');
            }
            break;
          //case 'OrcaExceptionLog':
          //case 'OrcaExceptionLog2':
            //break;
          case $soap_method === 'OrcaUpdate':
          case $soap_method === 'OrcaUpdate2':
            $version = $request_data->version;
            $updates = $financeReportingProcessor->getApplicationUpdates($version, 'orca', 'PDC');
            if ($updates) {
              $envelope->setVersionUpdateResponse($updates);
            }

            break;
          case $soap_method === 'OrcaExceptionLog':
          case $soap_method === 'OrcaExceptionLog2':
            $this->logException($request_data);
            // TODo: send a thank you message
            break;
          default:
            throw new Exception($soap_method . ' not Implemented');
        }
      } catch (Exception $e) {
        $envelope->setError($e->getMessage());
      }

      $xml = $envelope->asXML();
      $response = new Response($xml);
      $response->headers->set('Content-Type', 'Application/soap+xml');
      return $response;
    }
  }

  /**
   * @param $soap_response
   *
   * @return \stdClass
   * @throws \Exception
   */
  public function soapParser($soap_response) {
    $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $soap_response);
    $xml = simplexml_load_string($clean_xml);
    $data = new stdClass();

    foreach ($xml->Body->children() as $method => $parameters) {
      $data->method = (string) $method;
      foreach ($parameters as $key => $value) {
        $data->$key = (string) $value;
      }
    }
    return $data;
  }

  /**
   * This function is a stub and really does nothing but returns
   *   a list of available updates.
   *
   * @param $data
   *
   * @return string
   * @throws \Exception
   */
  public function logException($data){
    return null;
  }

}
