<?php

namespace Drupal\campaign_finance_accounting\Controller;

use Doctrine\DBAL\Exception;
use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\wapdc_core\Controller\CoreControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WAPDC\CampaignFinance\CampaignFinance;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Cache\CacheableJsonResponse;
use WAPDC\Core\TokenProcessor;

class PublicRestController extends CoreControllerBase {

  public function getReportUserData(Request $request, $repno)
  {
    $data = CampaignFinance::service()->getFundProcessor()->getReportUserData($repno);
    return new JsonResponse($data);
  }

  public function getCandidacyData(Request $request)
  {
    if (empty($_GET['search'] || empty($_GET['year']))) {
       throw new Exception('Candidacy search requires year and search parameters');
    }
    $search = $_GET['search'];
    $year = $_GET['year'];
    $data = CampaignFinance::service()->getFundProcessor()->getCandidacyData($year, $search);
    return new JsonResponse($data);
  }

  public function searchJurisdictions(Request $request)
  {
    $search = $_GET['search'];
    $data = CampaignFinance::service()->getFundProcessor()->searchJurisdictions($search);
    return new JsonResponse($data);
  }

  public function getProposalData(Request $request)
  {
    $jurisdiction_id = $_GET['jurisdiction_id'];
    $data = CampaignFinance::service()->getFundProcessor()->getProposalData($jurisdiction_id);
    return new JsonResponse($data);
  }

  /**
   * Use authorization token to save a draft report
   * @param Request $request
   * @return JsonResponse
   *  {
   *    url: string,
   *    success: bool,
   *    messages: [array of relevant validation message strings]
   *  }
   * @throws Exception
   *
   * curl -k -X POST -H "Content-Type: application/json" -H "Authorization: Bearer pdc-test-token-candidate" -d @json_output.json https://apollod8.lndo.site/public/service/campaign-finance/draft-report/send
   */
  public function saveDraftReport(Request $request, ?String $preview): JsonResponse
  {
    $response = new \stdClass();
    $response->success = false;
    $response->messages = new \StdClass;
    $token = $this->getBearerToken();
    $committee_id = CampaignFinance::service()->getCommitteeManager()->validateAPIToken($token);
    if (!$committee_id) {
      $response->messages->error = 'Invalid token';
      return new JsonResponse($response, Response::HTTP_NOT_FOUND);
    }
    $this->prepareRequest($request);
    $view_only = !!$preview;
    try {
      $user = $this->currentUser();
      $request_submission_version = $this->request_data->metadata->submission_version;

      if ($request_submission_version === null) {
        $response->messages->error = 'Submission version not found';
        return new JsonResponse($response, Response::HTTP_BAD_REQUEST);
      }

      $is_version_1_0 = ($request_submission_version === '1.0' || $request_submission_version === '1.0.0');

      if ($is_version_1_0 && ! $user->hasPermission('to vendor API 1.0')) {
        $response->messages->error = 'Version 1.0 is not supported';
        return new JsonResponse($response, Response::HTTP_UNAUTHORIZED);
      }
      CampaignFinance::service()->getVendorSubmissionProcessor()->validateDraftReportStructure($this->request_data);
    } catch (\Exception $e) {
      $response->messages->error = $e->getMessage();
      return new JsonResponse($response, Response::HTTP_BAD_REQUEST);
    }
    //need to return validation and preview url and not validation and draftReport
    $data = CampaignFinance::service()->getVendorSubmissionProcessor()->saveDraftReport($committee_id, $this->request_data, $view_only);
    $claims = ["draft_id" => $data->draft_id];
    $response = $data->validation;
    //only send preview url if validation was successful
    if ($response->success) {
      $success = Response::HTTP_OK;
      $tokenProcessor = new TokenProcessor();
      $token = $tokenProcessor->generateToken($claims, 1209600);
      $origin = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? 'https' : 'http') . '://';
      $origin .= $_SERVER['HTTP_HOST'] ?? '';
      $url = $origin . '/public/registrations/campaign-finance/draft-report/view?jwt=' . $token;
      $response->url = $url;
    }
    else {
      $success = Response::HTTP_BAD_REQUEST;
    }
    return new JsonResponse($response, $success);
  }

  /**
   * @param Request $request
   * @return JsonResponse
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function getDraftReportData(Request $request): JsonResponse
  {
    $token = $this->getBearerToken();
    $tokenProcessor = new TokenProcessor();
    $decoded_token = $tokenProcessor->decodeToken($token);
    $data = CampaignFinance::service()->getVendorSubmissionProcessor()->getDraftReport($decoded_token->draft_id);
    return new JsonResponse($data);
  }

  /**
   * Use Authorization token to get ORCA registration committee info.
   * @return JsonResponse
   * @throws \Exception
   */
  public function getCommitteeInfo(): JsonResponse
  {
    $token = $this->getBearerToken();
    $committee_id = CampaignFinance::service()->getCommitteeManager()->validateAPIToken($token);
    $data = CampaignFinance::service()
      ->getVendorSubmissionProcessor()->getVendorCommitteeInfo($committee_id);
    if (!$data) {
      $data->errors = ["No committee matching the provided authentication token"];
      $data->success = false;
      $status = 404;
    }
    return new JsonResponse($data, $status??200);
  }

  /**
   * Returns contributor types for the public API
   * ************ WARNING ************
   * This function was modified to return a static list and to ignore the table we have because the table disagrees with
   * how we use the codes in ORCA and other places. It seems like a total mess where we've hard coded this information
   * all over the place. To get this to work for vendors without making other changes, this was switched to static json
   * and the original call to get the data from the database has been commented out
   * @param Request $request
   * @return CacheableJsonResponse
   * @throws \Doctrine\DBAL\Exception
   * @todo This needs to be fixed in a consistent way so that we are using codes from a reliable, single source in all
   * the code
   */
  public function getContributorTypes(Request $request): CacheableJsonResponse
  {
    //$data = CampaignFinance::service()->getVendorSubmissionProcessor()->getContributorTypes();
    $json = <<<EOD
[
	{
		"type_code": "BUS",
		"label": "Business",
		"contributor_type": "B"
	},
	{
		"type_code": "CAN",
		"label": "Candidate",
		"contributor_type": "S"
	},
	{
		"type_code": "CAU",
		"label": "Legislative caucus committee",
		"contributor_type": "L"
	},
	{
		"type_code": "CP",
		"label": "County party committee",
		"contributor_type": "P"
	},
	{
		"type_code": "FI",
		"label": "Financial institution",
		"contributor_type": "F"
	},
	{
		"type_code": "IND",
		"label": "Individuals",
		"contributor_type": "I"
	},
	{
		"type_code": "LDP",
		"label": "Legislative district party committee",
		"contributor_type": "P"
	},
	{
		"type_code": "MP",
		"label": "Minor party committee",
		"contributor_type": "T"
	},
	{
		"type_code": "OG",
		"label": "Other organization",
		"contributor_type": "O"
	},
	{
		"type_code": "PAC",
		"label": "Political action committee",
		"contributor_type": "C"
	},
	{
		"type_code": "SP",
		"label": "State party committee",
		"contributor_type": "P"
	},
	{
		"type_code": "TRB",
		"label": "Tribal political committee",
		"contributor_type": "C"
	},
	{
		"type_code": "UN",
		"label": "Union",
		"contributor_type": "U"
	}
]
EOD;

    $data = json_decode($json);
    $json_response = new CacheableJsonResponse($data);
    $json_response->getCacheableMetaData()->setCacheMaxAge(600);
    return $json_response;
  }

  /**
   * @param Request $request
   * @return CacheableJsonResponse
   * @throws \Doctrine\DBAL\Exception
   */
  public function getExpenseCategories(Request $request): CacheableJsonResponse
  {
    $data = CampaignFinance::service()->getVendorSubmissionProcessor()->getExpenseCategories();
    $json_response = new CacheableJsonResponse($data);
    $json_response->getCacheableMetaData()->setCacheMaxAge(600);
    return $json_response;
  }

  /**
   * @param Request $request
   * @return CacheableJsonResponse
   * @throws \Doctrine\DBAL\Exception
   */
  public function getOccupations(Request $request): CacheableJsonResponse
  {
    $data = CampaignFinance::service()->getVendorSubmissionProcessor()->getOccupations();
    $json_response = new CacheableJsonResponse($data);
    $json_response->getCacheableMetaData()->setCacheMaxAge(600);
    return $json_response;
  }
}
