<?php

namespace Drupal\campaign_finance_accounting\Controller;

use Drupal\wapdc_core\Controller\CoreControllerBase;
use Exception;
use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WAPDC\CampaignFinance\CampaignFinance;
use WAPDC\CampaignFinance\Reporting\OrcaReportParser;

class CampaignFinanceReportController extends CoreControllerBase
{
  public function submittedReports($committee_id) {
    $data = CampaignFinance::service()->getFinanceReportingProcessor()->submittedReports($committee_id);
    return new JsonResponse($data);
  }
  public function committeeAPI(Request $request, $committee_id, $action) {
    $this->prepareRequest($request);
    $method  = $this->method;
    try {
      switch (true) {
        case ($action === 'finance-info' && $method === 'GET' ):
          $this->response_data = CampaignFinance::service()->getCommitteeManager()->getCommitteeFinanceInfo($committee_id);
          break;
        case ($action === 'fund' && $method === 'POST' && !empty($this->request_data->election_code)):
          CampaignFinance::service()->getFundProcessor()
            ->ensureCommitteeFundAndVendor($committee_id, $this->request_data->election_code, $this->request_data->vendor ?? NULL);
          $this->response_data->success = TRUE;
          break;
        default:
          throw new Exception("Invalid API Call");
      }
    } catch (Exception $e) {
      $this->response_data->error = $e->getMessage();
      return new JsonResponse($this->response_data, 500);
    }
    return new JsonResponse($this->response_data);
  }

  public function listDraftReports($committee_id) {
    $data = CampaignFinance::service()->getVendorSubmissionProcessor()->listDraftReports($committee_id);
    return new JsonResponse($data);
  }

  public function reportsAPI(Request $request, $report_id)
  {
    $data = CampaignFinance::service()->getFundProcessor()->getReportData($report_id);
    return new JsonResponse($data);
  }

  /**
   * @param $action
   * @return JsonResponse;
   * @throws Exception
   */
  function deleteDraftReport($draft_id) {
      $response = new \stdClass();
      $response->success = false;
      $response->errors = null;
      try {
          CampaignFinance::service()->getVendorSubmissionProcessor()->deleteDraftReport($draft_id);
          $response->success = true;
          return new JsonResponse($response, Response::HTTP_OK);
      } catch (Exception $e) {
          $response->errors['exception.unknown'] = $e->getMessage();
          return new JsonResponse($response, Response::HTTP_BAD_REQUEST);
      }
  }

  public function reportEditor(Request $request, $report_id)
  {
    $this->prepareRequest($request);

    try {
      CampaignFinance::service()->getFundProcessor()->editReport($report_id, $this->request_data);
      $this->response_data->success = true;
      $this->response_status = 200;
    } catch(Exception $e) {
      $this->response_status = 500;
      $this->response_data->success = false;
      $this->response_data->errors[] = $e->getMessage();
    }

    return $this->getResponse();
  }


  /**
   * Get XML from /public/service/campaign-finance-accounting/xml endpoint (public/service/report-translator)
   *
   * This can be tested with the following curl command:
  curl -k -X POST https://apollod8.lndo.site/public/service/report-translator \-H "Content-Type: application/xml" -H "Accept: application/xml" -d '<?xml version="1.0"?>
  <c3>
  <cover>
  <contId>P79</contId>
  <committee>Reluctant Friends of Dave</committee>
  <addr city="Olympia" st="WA" str="711 Capitol Way S #206" zip="98504"/>
  <officeSought offs1="0"/>
  <electnDate>2020</electnDate>
  <depositDate>02/29/2020</depositDate>
  <treasurer fst="Dewey" lst="Decimal" mdl="D." phone="3  605551212"/>
  <anon amt="0.00" totalAmt="10.00"/>
  <persFunds date="02/17/2020" amt="888.88" totalAmt="2888.86"/>
  <smallAmt amt="6401.25" count="2845\n"/>
  </cover>
  <contr amt="126.25" contId="C4300233" date="02/24/2020" electn="N"/>
  <contr amt="700.25" contId="C1234" date="02/25/2020" electn="N"/>
  <cont type="IND" id="C4300233" gAgg="27.00" pAgg="0">
  <name lst="Doe" fst="Jane" pre="Ms." suf="" mid="A"/>
  <addr zip="98498" st="WA" city="TACOMA" str="123 MAIN ST"/>
  <employer st="" city="" name="" occ=""/>
  </cont>
  <cont type="PAC" id="C1234" gAgg="0" pAgg="0">
  <name full="Daves Not Here PAC"/>
  <addr zip="98504" st="WA" city="Olympia" str="PO Box  1234567"/>
  </cont>
  </c3>' -o ./json_output.json
   *
   * Note that it writes this file: ./json_output.json
   */
  public function getVendorXml(Request $request) {
    $report_data = new StdClass();
    $report = new StdClass();

    $payload = file_get_contents('php://input');
    $parser = new OrcaReportParser();
    $report_data = $parser->vendorParse($payload);

    unset($report_data->committee);
    unset($report_data->treasurer);
    unset($report_data->election_year);
    unset($report_data->election_code);
    unset($report_data->submitted_at);

    $report->report = $report_data;
    if (strToLower($report_data->report_type) === 'c3') {
        $report->report->external_id = "Unique identifier for report";
    }
    $report->metadata = new stdClass();
    $report->metadata->vendor_name = "VENDOR NAME";
    $report->metadata->vendor_contacts = ['contact1@noreply.com', 'contact2@noreply.com'];
    $report->metadata->notification_emails = ['contact1@noreply.com'];
    $report->metadata->submission_version = "1.0.0";
    return new JsonResponse($report);
  }

  public function getDraftReport ($committee_id, $draft_id) {
    $data = CampaignFinance::service()->getVendorSubmissionProcessor()->getDraftReport($draft_id);
    return new JsonResponse($data);
  }

  public function submitVendorReport(Request $request, $committee_id, $draft_id) {
    $payload = json_decode(file_get_contents('php://input'));
    $ip = $request->getClientIp();
    $this->prepareRequest($request);
    $data = CampaignFinance::service()->getVendorSubmissionProcessor()->submitVendorReport($draft_id, $committee_id, $ip, $payload);
    return new JsonResponse($data);
  }

}