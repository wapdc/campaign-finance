drop view IF EXISTS private.vaccounts;
CREATE OR REPLACE VIEW private.vaccounts AS
SELECT
       t.fund_id,
       a.*,
       c.name,
       t.orca_id,
       ec.code as exp_code,
       ec.label,
       ec.help_text,
       ec.has_ad,
       ec.cat_lookup,
       coalesce(ec.requires_description, true) as requires_description,
       CASE
           WHEN A.acctnum IN (5020,5040,5050,5080,6030,6000,5100,5120,6020,5180,5230,5280,5140,5250,5260) THEN true
        ELSE false
       END as deprecated
FROM private.trankeygen t join private.accounts a
    ON t.trankeygen_id=a.trankeygen_id
    LEFT JOIN private.contacts c ON a.contid = c.trankeygen_id
    LEFT JOIN public.expense_category ec ON a.code = ec.code

