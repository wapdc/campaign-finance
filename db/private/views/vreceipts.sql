DROP view IF EXISTS private.vreceipts;
CREATE OR REPLACE VIEW private.vreceipts as
SELECT
  t.fund_id,
  r.type,
  rt.code as transaction_type,
  r.trankeygen_id,
  r.amount,
  r.itemized,
  r.description,
  r.user_description,
  c.name,
  c.street,
  c.city,
  c.state,
  c.zip,
  c.type as contact_type,
  r.date_,
  r.data,
  r.elekey,
  r.deptkey,
  r.memo,
  t.orca_id,
  t.rds_updated,
  t.derby_updated,
  greatest(t.rds_updated, t.derby_updated) as updated_at,
  t.trankeygen_id as rds_id,
  r.contid,
  tcontid.orca_id as contid_orca,
  r.pid,
  case when r.type in (20,29,44) and coalesce(cf.version,1.47)<1.490 then dob.orca_id
      else tpid.orca_id end as pid_orca,
  r.did,
  da.acctnum as did_acctnum,
  tdid.orca_id as did_orca,
  r.cid,
  ca.acctnum as cid_acctnum,
  tcid.orca_id as cid_orca,
  ca.code as category_code,
  ec.label as category,
  r.gid,
  tgid.orca_id as gid_orca,
  a.ad_id,
  a.first_run_date as ad_first_run_date,
  a.description as ad_description,
  r.nettype,
  r.aggtype,
  r.carryforward,
  r.checkno,
  de.date_ as deposit_date,
  de.trankeygen_id as deposit_id
FROM private.receipts r
  JOIN private.trankeygen t ON t.trankeygen_id=r.trankeygen_id
  JOIN private.receipt_type rt on rt.type = r.type
  left join private.campaign_fund cf on t.fund_id=cf.fund_id
  left join private.trankeygen tcid ON r.cid = tcid.trankeygen_id
  left join private.trankeygen tdid ON r.did = tdid.trankeygen_id
  left join private.accounts ca on ca.trankeygen_id=r.cid
  left join private.accounts da on da.trankeygen_id=r.did
  left join private.trankeygen tpid ON r.pid = tpid.trankeygen_id
    and (cf.version >= 1.490 or r.type not in (20,29,44))
  left join private.debtobligation dob on r.type in (20,29,44)
      and r.pid = dob.debtobligation_id
  left join private.trankeygen tcontid on r.contid = tcontid.trankeygen_id
  left join private.trankeygen tgid on r.gid = tgid.trankeygen_id
  left join private.contacts c ON R.contid=C.trankeygen_id
  left join private.ad_expenditure ae on ae.expense_id = r.trankeygen_id
  left join private.ad a on a.ad_id = ae.ad_id
  left join expense_category ec on ca.code = ec.code
  left join private.deposititems di on di.rcptid=r.trankeygen_id
  left join private.depositevents de on di.deptid=de.trankeygen_id;
