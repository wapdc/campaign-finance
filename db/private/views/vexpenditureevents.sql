CREATE OR REPLACE VIEW private.vexpenditureevents as
SELECT t.fund_id,
       e.trankeygen_id,
       e.trankeygen_id                          as rds_id,
       t.orca_id                                as orca_id,
       greatest(t.derby_updated, t.rds_updated) as updated_at,
       e.contid,
       tcont.orca_id                            as contid_orca,
       e.bnkid,
       tbank.orca_id                            as bnkid_orca,
       da.name                                  as account,
       e.date_,
       c.name,
       e.amount,
       e.checkno,
       e.itemized,
       e.memo,
       e.uses_surplus_funds,
       a.acctnum
FROM private.expenditureevents e
         JOIN private.trankeygen t ON e.trankeygen_id = t.trankeygen_id
         LEFT JOIN private.contacts c ON e.contid = c.trankeygen_id
         LEFT JOIN private.trankeygen tbank on e.bnkid = tbank.trankeygen_id
         LEFT JOIN private.trankeygen tcont on e.contid = tcont.trankeygen_id
         LEFT JOIN private.contacts da ON e.bnkid = da.trankeygen_id
         LEFT JOIN private.accounts a on e.bnkid=a.trankeygen_id
