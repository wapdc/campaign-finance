DROP view IF EXISTS private.vContacts;
CREATE OR REPLACE VIEW private.vContacts AS
SELECT t.fund_id,
       t.orca_id,
       c.contact_key,
       C.trankeygen_id,
       case when p.value is not null then 'CAN' else c.type end as type,
       c.name,
       ind.prefix,
       ind.firstname,
       ind.middleinitial,
       ind.lastname,
       ind.suffix,
       c.street,
       c.city,
       c.state,
       c.zip,
       c.phone,
       c.email,
       c.occupation,
       c.employerName,
       c.employerStreet,
       c.employerCity,
       c.employerState,
       c.employerZip,
       c.memo,
       greatest(t.rds_updated, t.derby_updated) as updated_at,
       t.rds_updated,
       t.derby_updated,
       c.pagg,
       c.gagg,
       cc.contact1_id,
       cc.contact2_id,
       tk1.orca_id as contact1_orca_id,
       tk2.orca_id as contact2_orca_id
FROM private.Contacts C
         join private.trankeygen t ON t.trankeygen_id = c.trankeygen_id
         left join private.couplecontacts cc on c.trankeygen_id = cc.trankeygen_id
         left join private.individualcontacts ind on ind.trankeygen_id = c.trankeygen_id
         left join private.trankeygen tk1 on tk1.trankeygen_id = cc.contact1_id
         left join private.trankeygen tk2 on tk2.trankeygen_id = cc.contact2_id
         left join private.properties p on p.fund_id=t.fund_id
           and p.name = 'CANDIDATEINFO:CONTACT_ID'
           and p.value = t.orca_id::text
WHERE type != 'HDN';