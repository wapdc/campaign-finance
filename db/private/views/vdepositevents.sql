DROP view IF EXISTS private.vdepositevents;
CREATE OR REPLACE VIEW private.vdepositevents AS
    SELECT d.account_id,
           ta.orca_id as account_orca_id,
           d.amount,
           d.date_,
           d.deptkey,
           d.memo,
           d.trankeygen_id,
           t.orca_id,
           greatest(t.rds_updated, t.derby_updated) as updated_at,
           t.rds_updated,
           t.derby_updated,
           t.fund_id,
           coalesce(c.name, 'Bank Accounts') as account, --TODO:: This coalesce can be removed after the account id is properly populated
        --TODO:: Add the external ID needed to tell if a report has been filed or not
           r.report_id,
           r.submitted_at
FROM private.depositevents d
    INNER JOIN private.trankeygen t on t.trankeygen_id = d.trankeygen_id
    LEFT JOIN private.accounts a on d.account_id = a.trankeygen_id
    LEFT JOIN private.trankeygen ta on ta.trankeygen_id = d.account_id
    LEFT JOIN private.contacts c on d.account_id = c.trankeygen_id
    LEFT JOIN public.report r on t.fund_id = r.fund_id and r.external_id = cast(d.trankeygen_id as varchar) AND r.superseded_id is null;
