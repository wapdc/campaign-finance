DROP view IF EXISTS private.vAuctions;
CREATE OR REPLACE VIEW private.vAuctions AS
SELECT t.fund_id,
       t.orca_id,
       a.trankeygen_id,
       a.itemdescription,
       a.itemnumber,
       a.marketval,
       a.memo,
       c.name,
       greatest(t.rds_updated, t.derby_updated) as updated_at,
       t.rds_updated,
       t.derby_updated
from private.auctionitems a
         join private.trankeygen t on t.trankeygen_id = a.trankeygen_id
         left join private.contacts c on c.trankeygen_id = a.pid