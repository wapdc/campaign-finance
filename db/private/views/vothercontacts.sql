DROP VIEW IF EXISTS private.vothercontacts;
CREATE OR REPLACE VIEW private.vOtherContacts AS
SELECT t.fund_id,
       c.trankeygen_id,
       c.type,
       c.name,
       c.street,
       c.city,
       c.state,
       c.zip,
       c.phone,
       c.email,
       c.memo
FROM private.Contacts C
  JOIN private.trankeygen t on T.trankeygen_id=c.trankeygen_id
WHERE type NOT IN ('ACT', 'CAN', 'IND', 'HDN', 'CPL', 'GRP');