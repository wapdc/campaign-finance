set schema 'private';

drop view if exists private.vcouplecontacts;
create or replace view vcouplecontacts as
select trankeygen_id as couple_contact_id,
       case when n.cnum=1 then contact1_id else contact2_id end as contact_id,
       n.cnum
from private.couplecontacts c1 join (values (1), (2)) n (cnum) on true;