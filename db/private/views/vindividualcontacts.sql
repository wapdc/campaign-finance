CREATE OR REPLACE VIEW private.vIndividualContacts AS
SELECT
       t.fund_id,
       C.trankeygen_id,
       c.name,
       i.prefix,
       i.firstName,
       i.middleInitial,
       i.lastName,
       i.suffix,
       c.street,
       c.city,
       c.state,
       c.zip,
       c.phone,
       c.email,
       c.occupation,
       c.employerName,
       c.employerCity,
       c.employerState,
       c.employerZip,
       c.memo,
       greatest(t.rds_updated, t.derby_updated) as updated_at,
       t.rds_updated,
       t.derby_updated
FROM private.Contacts C
  INNER JOIN private.IndividualContacts I ON C.trankeygen_id = I.trankeygen_id
  INNER JOIN private.trankeygen t ON t.trankeygen_id = i.trankeygen_id;