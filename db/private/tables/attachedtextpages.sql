DROP TABLE IF EXISTS private.ATTACHEDTEXTPAGES;
create table if not exists private.ATTACHEDTEXTPAGES
(
    fund_id         INTEGER not null,
    target_id       INTEGER not null,
    target_type     VARCHAR(50),
    text_data       TEXT,
    primary key (target_id, fund_id, target_type)
);