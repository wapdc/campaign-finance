
set schema 'private';
create table if not exists receipt_type
(
    code text,
    type int primary key not null
);

insert into receipt_type (code, type)
values
           ('Monetary Contribution', 1),
           ('Personal Funds', 2),
           ('Monetary Loan', 3),
           ('Fundraiser Contribution', 4),
           ('Auction Item Donor', 7),
           ('Auction Item Buyer', 8),
           ('Loan Endorsement', 9),
           ('Monetary Couple Contribution (no longer used)', 10),
           ('Monetary Group Contribution', 11),
           ('Pledge', 12),
           ('Debt Forgiven', 44),
           ('Anonymous Contribution', 13),
           ('Other Receipt', 14),
           ('Bank Interest', 15),
           ('Monetary Pledge', 17),
           ('Refund From Vendor', 18),
           ('Correction Receipt', 23),
           ('Correction Receipt Math Error', 24),
           ('Carry Forward Cash', 22),
           ('Pledge Cancelled', 25),
           ('Loan Payment', 26),
           ('Refund Contribution', 27),
           ('Expenditure', 28),
           ('Debt Payment', 29),
           ('Interest Expense', 30),
           ('Credit Card Payment', 31),
           ('Debt Interest', 32),
           ('Correction Expense', 33),
           ('Math Error', 34),
           ('Debt', 19),
           ('Credit Card Debt', 21),
           ('Debt Adjustment', 20),
           ('In Kind Contribution', 6),
           ('In Kind Loan', 5),
           ('In Kind Pledge Payment', 16),
           ('Correction In Kind', 35),
           ('Auction', 36),
           ('Auction Item', 37),
           ('Low Cost Fundraiser', 38),
           ('Loan Forgiven', 39),
           ('Regular Fundraiser', 40),
           ('Monetary Group Contribution Detail', 41),
           ('Account Adjustment', 42),
           ('Expenditure Event', 43);