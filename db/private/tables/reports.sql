create table if not exists private.REPORTS
(
    fund_id         integer,
    reports_id      integer,
    TYPE            SMALLINT                      not null,
    RKEY            VARCHAR(30),
    XRFDATA         text,
    RECEIVERNAME    VARCHAR(50),
    RESPONSEDATA    text,
    RESPONSEID      VARCHAR(50),
    RESPONSEWARNING SMALLINT default 0            not null,
    RESPONSEMESSAGE text,
    RESPONSEDATE    DATE     default CURRENT_DATE not null,
    primary key (fund_id, reports_id)
);