create table if not exists private.DEBTOBLIGATION
(
    debtobligation_id serial,
    PID    INTEGER           not null, --trankeygen for parent receipt
    CONTID INTEGER           not null, --trankeygen for contact
    AMOUNT numeric(16,2   )  default 0 not null,
    DATE_  DATE              not null,
    DESCR  VARCHAR(1000),
    MEMO   VARCHAR(1000),
    primary key (debtobligation_id),
    unique (PID, CONTID)
);