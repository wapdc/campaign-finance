create table if not exists private.TRANKEYGEN_DELETED
(
    ID INTEGER not null,
    DERBY_UPDATED TIMESTAMP default now() not null
);

