DROP TABLE IF EXISTS private.ad_contribution;
create table if not exists private.ad_contribution (
      contribution_id integer,
      ad_id integer,
      primary key (contribution_id, ad_id),
      FOREIGN KEY (ad_id) REFERENCES private.ad(ad_id) ON DELETE CASCADE
);