create table if not exists private.COUPLECONTACTS
(
    trankeygen_id integer not null,
    CONTACT1_ID INTEGER,
    CONTACT2_ID INTEGER,
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_CPLC_FK
        foreign key (trankeygen_id) references private.CONTACTS (trankeygen_id)
            on delete cascade
);

create unique index if not exists idx_couplecontacs_trankeygen_id
    on private.COUPLECONTACTS (trankeygen_id);