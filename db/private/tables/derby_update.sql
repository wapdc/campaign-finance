create table if not exists private.derby_update
(
    update_id      serial primary key,
    fund_id        int,
    run_date timestamptz,
    sql varchar,
    message varchar
);