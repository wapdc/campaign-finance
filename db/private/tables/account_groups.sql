drop table if exists private.account_groups;
create table if not exists private.account_groups
(
    acctnum SMALLINT           not null,
    title text,
    category_code text,
    negate  SMALLINT default 1 not null,
    required boolean default false,
    deprecated boolean default false,
    primary key (acctnum)
);
drop table if exists private.accountgroups;
-- These account groups are from a new campaign setup and just include those created from template.zip
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (0, 'JUNK ACCOUNT');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (1000, 'Bank accounts');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (1600, 'Undeposited items');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (1800, 'Petty cash');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (2000, 'Vendor/agent debt');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (2100, 'Loans');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (2200, 'In-kind loans');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (2600, 'Credit cards');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (3000, 'Surplus/(deficit) account');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4000, 'Monetary contributions');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4096, 'Fundraisers');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4097, 'Low cost fundraisers');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4098, 'Auctions');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4099, 'Anonymous contributions');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4100, 'In-kind contributions');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4200, 'Personal funds');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4300, 'Vendor refunds');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4400, 'Bank interest');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4500, 'Other receipts');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4600, 'Carry forward');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (4900, 'Pledges');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5000, 'Bank charges');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5001, 'Carry forward debt');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5005, 'Broadcast/cable TV advertising');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5010, 'Charity/community organization');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5030, 'Monetary contributions to PAC or candidate');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5060, 'Filing fees');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5070, 'General operation and overhead');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5090, 'Independent Expenditures');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5110, 'Interest Expense');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5130, 'Management and consulting services');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5150, 'Miscellaneous');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5160, 'Newspaper/periodical advertising');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5170, 'Office supplies, furniture, staff food, etc.');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5190, 'Postage, mail permits, stamps, etc.');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5200, 'Printing brochures, literature, postcards');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5210, 'Accounting, legal, regulatory compliance, etc.');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5220, 'Rent, lease, mortgage, PO box rental');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5240, 'Surveys, polling, research');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5270, 'Travel, accommodation, meals');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5290, 'Signature gathering');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5300, 'Wages, salaries, benefits, payroll taxes');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5310, 'Printing signs');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (6010, 'Fundraising');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5034, 'Transfer to political party');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5006, 'Digital advertising');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5275, 'Mileage reimbursement');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5007, 'Radio advertising');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5008, 'Robocalls');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5031, 'Transfer to general fund');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5032, 'Transfer to surplus funds account');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5305, 'Payment for candidate''s lost earnings');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5330, 'Design/graphic art, etc.');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5175, 'Computers, tablets printers, software, phones, etc');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5009, 'Other advertising');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5033, 'Disposal of surplus funds to charity');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5340, 'Campaign merchandise/paraphernalia ');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5035, 'Transfer to new campaign');
INSERT INTO private.account_groups (ACCTNUM, title) VALUES (5306, 'Payment for candidate''s lost earnings - Surplus');

-- These are entries we already have in our database but that so we need to add so they display in the summary.
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5120, 'Legal expense');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5039, 'Other');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5036, 'Ballot issue contributions');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5050, 'Employee services');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5265, 'Website expenses');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15034, 'Transfer to political party or legislative caucus committee');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 6030, 'Entertainment');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5100, 'Insurance');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5080, 'Gifts');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 6020, 'Other consumables');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15035, 'Transfer to new campaign');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5180, 'Payroll taxes');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 6212, 'Proceeds from low cost fundraiser');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5230, 'Subscriptions');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 6246, 'Other');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 6214, 'Other');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5260, 'Telephone');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5038, 'Contributions to other PAC');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 6000, 'Other');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15008, 'Robocalls');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5002, 'Other');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5037, 'Caucus/Party contributions');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5302, 'Website Fees');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, -5555, 'Other');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 6248, 'Other');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5040, 'Electricity');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5020, 'Community organizations');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5320, 'Website');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5345, 'Food and beverage');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15009, 'Other advertising');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15031, 'Transfer to general fund');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15007, 'Radio advertising');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5280, 'Vehicle expense');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15033, 'Disposal of surplus funds to charity');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15032, 'Transfer to surplus funds account');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5225, 'Social media advertising');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15006, 'Digital advertising');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5250, 'SALES TAX');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 5140, 'Maintenance & Repairs');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15330, 'Design/graphic art, etc.');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15175, 'Computers, tablets printers,software, phones, etc');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15340, 'Campaign merchandise/paraphernalia');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15306, 'Payment for candidate''s lost earnings');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15275, 'Mileage reimbursement');
INSERT INTO private.account_groups(deprecated, acctnum, title) VALUES (true, 15305, 'Payment for candidate''s lost earnings');

-- Set negation correctly in new table based on account ranges so the display with the right sign.
update private.account_groups set negate = -1 where acctnum between 2000 and 4899;
update private.account_groups set required = true where acctnum in (1000, 4000, 4100);
