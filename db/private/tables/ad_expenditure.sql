DROP TABLE IF EXISTS private.ad_expenditure;
create table if not exists private.ad_expenditure (
    expense_id integer,
    ad_id integer,
    primary key (expense_id, ad_id),
    FOREIGN KEY (ad_id) REFERENCES private.ad(ad_id) ON DELETE CASCADE
);

