create table if not exists private.DEPOSITEVENTS
(
    trankeygen_id         INTEGER          not null,
    ACCOUNT_ID INTEGER,
    AMOUNT     numeric(16,2) default 0 not null,
    DATE_      DATE             not null,
    DEPTKEY    BIGINT default 0 not null,
    MEMO       VARCHAR(1000),
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_DEPEV_FK
        foreign key (trankeygen_id) references private.TRANKEYGEN (trankeygen_id)
            on delete cascade
);

create index if not exists idx_depositevents_deptkey
    on private.DEPOSITEVENTS (DEPTKEY);

create unique index if not exists idx_depositevents_trankeygen_id
    on private.DEPOSITEVENTS (trankeygen_id);