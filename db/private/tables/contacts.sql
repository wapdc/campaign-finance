create table if not exists private.CONTACTS
(
    trankeygen_id  INTEGER          not null,
    TYPE           VARCHAR(3)       not null,
    NAME           VARCHAR(110),
    STREET         VARCHAR(100),
    CITY           VARCHAR(100),
    STATE          VARCHAR(2),
    ZIP            VARCHAR(50),
    PHONE          VARCHAR(50),
    EMAIL          VARCHAR(50),
    OCCUPATION     VARCHAR(50),
    EMPLOYERNAME   VARCHAR(100),
    EMPLOYERSTREET VARCHAR(100),
    EMPLOYERCITY   VARCHAR(100),
    EMPLOYERSTATE  VARCHAR(2),
    EMPLOYERZIP    VARCHAR(50),
    MEMO           VARCHAR(1000),
    PAGG           numeric(16,2) default 0 not null,
    GAGG           numeric(16,2) default 0 not null,
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_CNT_FK
        foreign key (trankeygen_id) references private.TRANKEYGEN (trankeygen_id)
            on delete cascade
);

create index if not exists idx_contacts_name
    on private.CONTACTS (NAME);

create index if not exists idx_contacts_type
    on private.CONTACTS (TYPE);

create unique index if not exists idx_contacts_trankeygen_id
    on private.CONTACTS (trankeygen_id);