create table if not exists private.restore_request
(
    restore_request_id  serial primary key,
    fund_id             int not null,
    email               text not null,
    validate            bool default true not null
);