create table if not exists private.INDIVIDUALCONTACTS
(
    trankeygen_id integer not null,
    PREFIX        VARCHAR(5),
    FIRSTNAME     VARCHAR(50),
    MIDDLEINITIAL VARCHAR(10),
    LASTNAME      VARCHAR(50),
    SUFFIX        VARCHAR(5),
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_INDC_FK
        foreign key (trankeygen_id) references private.CONTACTS (trankeygen_id)
            on delete cascade
);

create unique index if not exists idx_individualcontacts_trankeygen_id
    on private.INDIVIDUALCONTACTS (trankeygen_id);