create table if not exists private.contact_duplicates
(
    contact_id             INTEGER not null,
    duplicate_id           INTEGER not null,
    verified_not_duplicate BOOLEAN not null default false,
    name_similarity        numeric,
    address_similarity     numeric,
    primary key (contact_id, duplicate_id),
    unique (contact_id, duplicate_id),
    constraint contact_duplicates_contact_FK
        foreign key (contact_id) references private.TRANKEYGEN (trankeygen_id)
            on delete cascade,
    constraint contact_duplicates_dup_FK
        foreign key (duplicate_id) references private.trankeygen (trankeygen_id)
            on delete cascade
);





