DROP TABLE IF EXISTS private.ad_target;
create table if not exists private.ad_target (
    ad_target_id serial primary key,
    ad_id int,
    candidacy_id int,
    ballot_name varchar,
    jurisdiction_id int,
    stance varchar(100),
    amount numeric(16,2),
    percent numeric,
    FOREIGN KEY (ad_id) REFERENCES private.ad(ad_id) ON DELETE CASCADE
);

