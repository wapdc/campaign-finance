--fund_id
create table if not exists private.PROPERTIES
(
    fund_id integer,
    NAME  VARCHAR(255) not null,
    VALUE text,
    primary key (fund_id, NAME)
);