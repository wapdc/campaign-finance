create table if not exists private.ACCOUNTS
(
    trankeygen_id INTEGER not null,
    PID         INTEGER,
    ACCTNUM     SMALLINT,
    CONTID      INTEGER default 0 not null,
    CODE        VARCHAR(3),
    DESCRIPTION VARCHAR(100),
    STYLE       SMALLINT,
    TOTAL       numeric(16,2)  default 0,
    MEMO        VARCHAR(1000),
    DATE_       DATE,
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_ACCT_FK
        foreign key (trankeygen_id) references private.TRANKEYGEN (trankeygen_id)
            on delete cascade
);

create index if not exists IACC_ID_ACCTNUM
    on private.ACCOUNTS (trankeygen_id, ACCTNUM);

create index if not exists IACC_MODTYPE_TOTAL
    on private.ACCOUNTS (STYLE, TOTAL);

create unique index if not exists SQL060614023703751
    on private.ACCOUNTS (trankeygen_id);