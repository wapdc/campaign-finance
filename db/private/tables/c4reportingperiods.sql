create table if not exists private.C4REPORTINGPERIODS
(
    fund_id integer,
    REPORTED  SMALLINT default 0 not null,
    STARTDATE DATE               not null,
    ENDDATE   DATE               not null,
    DUEDATE   DATE               not null,
    primary key (fund_id, STARTDATE, ENDDATE)
);