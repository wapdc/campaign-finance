create table if not exists private.LOANS
(
    trankeygen_id     INTEGER            not null,
    TYPE              SMALLINT           not null,
    CID               INTEGER  default 0 not null,
    DID               INTEGER  default 0 not null,
    CONTID            INTEGER  default 0 not null,
    AMOUNT            numeric(16,2)   default 0 not null,
    DATE_             DATE               not null,
    ELEKEY            SMALLINT           not null,
    INTERESTRATE      numeric(12,6)   default 0,
    DUEDATE           DATE,
    CHECKNO           VARCHAR(25),
    REPAYMENTSCHEDULE VARCHAR(50),
    DESCRIPTION       VARCHAR(1000),
    MEMO              VARCHAR(1000),
    CARRYFORWARD      SMALLINT default 0,
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_LOANS_FK
        foreign key (trankeygen_id) references private.TRANKEYGEN (trankeygen_id)
            on delete cascade
);

create unique index idx_loans_trankeygen_id
    on private.LOANS (trankeygen_id);