--fund_id
create table if not exists private.LIMITS
(
    fund_id     integer,
    TYPE        VARCHAR(3)  not null,
    NAME        VARCHAR(50) not null,
    AMOUNT      numeric(16,2) default 0,
    AGGBYVOTERS SMALLINT,
    primary key (fund_id, TYPE)
);