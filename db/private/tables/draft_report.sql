create table if not exists private.draft_report
(
  draft_id serial primary key,
  fund_id integer,
  report_type varchar,
  external_id varchar,
  period_start date,
  period_end date,
  received_date timestamptz not null default now(),
  user_data varchar,
  metadata varchar,
  processed boolean,
  expire_date timestamptz not null
);