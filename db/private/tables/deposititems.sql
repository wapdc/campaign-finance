create table if not exists private.DEPOSITITEMS
(
    DEPTID INTEGER not null, --trankeygen
    RCPTID INTEGER not null, --trankeygen
    primary key (DEPTID, RCPTID)
);