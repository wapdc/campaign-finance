create table if not exists private.TRANKEYGEN
(
    trankeygen_id serial,
    orca_id  int,
    fund_id int,
    pid INTEGER,
    derby_updated TIMESTAMP,
    rds_updated TIMESTAMP WITH TIME ZONE,
    primary key (trankeygen_id)
);

CREATE INDEX idx_derby_updated
    ON private.TRANKEYGEN (derby_updated);
CREATE INDEX idx_rds_updated
    ON private.TRANKEYGEN (rds_updated);
CREATE INDEX idx_trankeygen_fund ON private.trankeygen (fund_id);