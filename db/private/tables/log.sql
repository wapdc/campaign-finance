create table if not exists private.log
(
    fund_id      INTEGER not null,
    memo         varchar,
    data         varchar,
    date timestamptz default now()
);