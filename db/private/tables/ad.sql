drop table IF EXISTS private.ad CASCADE;
create table if not exists private.ad (
    ad_id serial primary key,
    fund_id integer,
    first_run_date date,
    target_type varchar(100),
    description varchar,
    updated_at timestamptz
);
CREATE INDEX ad_fund_id ON private.ad(fund_id);