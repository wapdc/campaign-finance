create table if not exists private.AUCTIONITEMS
(
    trankeygen_id   INTEGER not null,
    PID             INTEGER not null,
    ITEMNUMBER      VARCHAR(50),
    ITEMDESCRIPTION VARCHAR(100),
    MARKETVAL       numeric(16,2)  not null,
    MEMO            VARCHAR(1000),
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_AUIT_FK
        foreign key (trankeygen_id) references private.TRANKEYGEN (trankeygen_id)
            on delete cascade
);

create unique index if not exists idx_auctionitems_trankeygen_id
    on private.AUCTIONITEMS (trankeygen_id);