create table if not exists private.SERIALIZEDOBJECTS
(
    trankeygen_id     INTEGER not null,
    DATE_  DATE    not null,
    OBJECT bytea, -- byte array
    primary key (trankeygen_id, DATE_)
);