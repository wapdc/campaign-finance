create table if not exists private.reporting_obligation
(
    obligation_id SERIAL,
    fund_id INTEGER not null,
    reporting_period_id INTEGER null,
    startdate DATE not null,
    enddate DATE not null,
    duedate DATE not null,
    unique(fund_id, startdate, enddate),
    primary key (obligation_id)
);
