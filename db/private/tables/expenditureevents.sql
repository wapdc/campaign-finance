create table if not exists private.EXPENDITUREEVENTS
(
    trankeygen_id       INTEGER            not null,
    CONTID   INTEGER,
    BNKID    INTEGER,
    AMOUNT   numeric(16,2)   default 0 not null,
    DATE_    DATE,
    CHECKNO  VARCHAR(50),
    ITEMIZED SMALLINT default 0 not null,
    MEMO     VARCHAR(1000),
    uses_surplus_funds BOOLEAN NOT NULL default false,
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_EXPEV_FK
        foreign key (trankeygen_id) references private.TRANKEYGEN (trankeygen_id)
            on delete cascade
);

create unique index if not exists idx_expenditureevents_trankeygen_id
    on private.EXPENDITUREEVENTS (trankeygen_id);