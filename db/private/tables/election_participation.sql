DROP TABLE IF EXISTS private.election_participation;
create table if not exists private.election_participation (
    fund_id integer,
    election_code varchar,
    primary key (fund_id, election_code)
);