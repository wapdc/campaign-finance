CREATE TABLE IF NOT EXISTS private.campaign_fund (
  fund_id integer,
  version numeric (10,3),
  deposit_number integer,
  primary key (fund_id),
  save_count integer default 0 not null,
  archive_file text,
  archive_expiration date
);