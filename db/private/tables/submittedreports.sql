--fund_id
create table if not exists private.SUBMITTEDREPORTS
(
    submittedreports_id serial,
    fund_id integer,
    orca_id integer,
    REPTTYPE VARCHAR(2),
    DATE_    DATE        not null,
    REPTHASH VARCHAR(50) not null,
    REPTNUMB VARCHAR(50),
    RESPONSE text,
    primary key (fund_id, submittedreports_id)
);