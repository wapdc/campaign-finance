create table if not exists private.RECEIPTS
(
    trankeygen_id   INTEGER          not null,
    TYPE         SMALLINT            not null,
    AGGTYPE      SMALLINT default 0 not null,
    NETTYPE      SMALLINT default 0 not null,
    PID          INTEGER,
    GID          INTEGER,
    CID          INTEGER,
    DID          INTEGER,
    CONTID       INTEGER,
    AMOUNT       numeric(16,2)   default 0  not null,
    DATE_        DATE,
    ELEKEY       SMALLINT default -1 not null,
    ITEMIZED     SMALLINT default 0  not null,
    CHECKNO      VARCHAR(25),
    DESCRIPTION  VARCHAR(1000),
    MEMO         VARCHAR(1000),
    CARRYFORWARD SMALLINT default 0  not null,
    DEPTKEY      BIGINT   default 0  not null,
    primary key (trankeygen_id),
    unique (trankeygen_id),
    constraint IDS_RCPT_FK
        foreign key (trankeygen_id) references private.TRANKEYGEN (trankeygen_id)
            on delete cascade
);

create index if not exists idx_receipts_aggtype
    on private.RECEIPTS (AGGTYPE);

create index if not exists idx_receipts_contid
    on private.RECEIPTS (CONTID);

create index if not exists idx_receipts_date_
    on private.RECEIPTS (DATE_);

create index if not exists idx_receipts_dept_key
    on private.RECEIPTS (DEPTKEY);

create index if not exists idx_receipts_trankeygen_idTYPE
    on private.RECEIPTS (trankeygen_id, TYPE);

create index if not exists idx_receipts_nettype
    on private.RECEIPTS (NETTYPE);

create index if not exists idx_receipts_PIDGID
    on private.RECEIPTS (PID, GID);

create index if not exists idx_receipts_PIDTYPE
    on private.RECEIPTS (PID, TYPE);

create index if not exists idx_receipts_type
    on private.RECEIPTS (TYPE);

create index if not exists idx_receipts_TYPEDATE_
    on private.RECEIPTS (DATE_, TYPE);

create unique index if not exists idx_receipts_trankeygen_id
    on private.RECEIPTS (trankeygen_id);