DROP FUNCTION IF EXISTS private.cf_ensure_trankeygen(p_fund_id INTEGER, p_rds_id INTEGER, p_orca_id INTEGER);
DROP FUNCTION IF EXISTS private.cf_ensure_trankeygen(p_fund_id integer, p_rds_id integer);
CREATE OR REPLACE FUNCTION private.cf_ensure_trankeygen(p_fund_id INTEGER, p_rds_id INTEGER) RETURNS INTEGER AS
$$
  BEGIN
    RETURN private.cf_ensure_trankeygen(p_fund_id,  p_rds_id, cast(NULL as INTEGER));
  END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION private.cf_ensure_trankeygen(p_fund_id INTEGER, p_rds_id INTEGER, p_orca_id INTEGER) RETURNS INTEGER AS
$$
  DECLARE v_rds_id INTEGER;
  BEGIN
    -- IF we are given an rds trankeygen id find it to make sure we update the record.
    IF p_rds_id > 0 THEN
      SELECT trankeygen_id into v_rds_id from private.trankeygen WHERE fund_id=p_fund_id and trankeygen_id=p_rds_id;
      IF v_rds_id IS NOT NULL and p_orca_id >0  THEN
        UPDATE private.trankeygen set orca_id = p_orca_id  WHERE trankeygen_id=p_rds_id;
      END IF;
      IF p_orca_id IS NULL THEN
          UPDATE private.trankeygen set rds_updated = NOW() where trankeygen_id = p_rds_id;
          UPDATE private.campaign_fund set updated = now() where fund_id = p_fund_id;
      end if;
    END IF;

    -- If we have an orca id but not trankeygen id then find a record based on ORCAs id
    IF p_orca_id > 0 AND (v_rds_id IS NULL) THEN
      SELECT trankeygen_id INTO v_rds_id FROM private.trankeygen WHERE fund_id= p_fund_id and orca_id = p_orca_id;
    END IF;

    -- If we still don't have a record after all that then create one
    IF v_rds_id IS NULL and (p_orca_id is null or p_orca_id > 0) THEN
      INSERT INTO private.trankeygen (fund_id,orca_id, rds_updated) VALUES (p_fund_id, case when p_orca_id > 0 then p_orca_id end, case when p_orca_id is null then now() end) RETURNING trankeygen_id into v_rds_id;
      if p_orca_id is null then
          UPDATE private.campaign_fund set updated = now() where fund_id = p_fund_id;
      end if;
    END IF;

    RETURN v_rds_id;
  END;
$$ LANGUAGE plpgsql;

