--DROP function private.cf_purge_old_orca_ids(campaign_fund_id INTEGER, old_id INTEGER, version_number float);
CREATE OR REPLACE FUNCTION private.cf_purge_old_orca_ids(campaign_fund_id INTEGER, old_id INTEGER, version_number float)
    RETURNS json as
$$
DECLARE
    deleted_count integer;
    records       json;
    contacts      json;
    expenditures  json;
    auction_items json;
    accounts      json;
    receipts      json;
BEGIN

    --log records to be deleted
    select json_agg(ex)
    from (select ee.*,
                 (
                     select json_agg(r)
                     from (
                              select *
                              from private.vreceipts vr
                              where vr.pid = ee.trankeygen_id
                          ) r
                 )
          from private.vexpenditureevents ee
          where ee.orca_id > old_id
            and ee.fund_id = campaign_fund_id) ex
    into expenditures;

    select json_agg(re)
    from (select r.*
          from private.vreceipts r
          where r.orca_id > old_id
            and r.fund_id = campaign_fund_id) re
    into receipts;

    select json_agg(ai)
    from (select a.*
          from private.vauctions a
          where a.orca_id > old_id
            and a.fund_id = campaign_fund_id) ai
    into auction_items;

    select json_agg(co)
    from (select c.*
          from private.vcontacts c
          where c.orca_id > old_id
            and c.fund_id = campaign_fund_id) co
    into contacts;

    select json_agg(ac)
    from (select a.*
          from private.vaccounts a
          where a.orca_id > old_id
            and a.fund_id = campaign_fund_id) ac
    into accounts;

    --set orca_id = null for expenditure transactions
    if version_number >= 1.440
    then
        update private.trankeygen
        set orca_id = null
        where exists(select 1
                     from private.trankeygen t
                              join private.receipts r on t.trankeygen_id = r.trankeygen_id
                     where type = 28
                       and t.trankeygen_id = trankeygen.trankeygen_id
                       and t.fund_id = campaign_fund_id
                       and t.orca_id > old_id);

        update private.trankeygen
        set orca_id = null
        where exists(select 1
                     from private.trankeygen t
                              join private.expenditureevents ee on t.trankeygen_id = ee.trankeygen_id
                     where fund_id = campaign_fund_id
                       and t.trankeygen_id = trankeygen.trankeygen_id
                       and orca_id > old_id);
    end if;

    --delete trankeygen records from deposit items
    delete from private.deposititems where rcptid in (select trankeygen_id from private.trankeygen t where t.fund_id = campaign_fund_id and orca_id > old_id);
    delete from private.deposititems where deptid in (select trankeygen_id from private.trankeygen t where t.fund_id = campaign_fund_id and orca_id > old_id);

    --delete trankeygen records
    WITH deleted
             AS (delete from private.trankeygen t where t.fund_id = campaign_fund_id and orca_id > old_id returning *)
    SELECT count(*)
    from deleted
    into deleted_count;

    select row_to_json(s)
    from (select cast(cast(deleted_count as varchar) as json) as deleted_count,
                 expenditures                                 as expenditures,
                 auction_items                                as acution_items,
                 receipts                                     as other_receipts,
                 contacts                                     as contacts,
                 accounts                                     as accounts
         ) s
    into records;

    --create record of deletion
    insert into private.log(fund_id, memo, data) values (campaign_fund_id, 'Deleted records to rectify data sync error', records);

    return records;
END
$$ LANGUAGE plpgsql;