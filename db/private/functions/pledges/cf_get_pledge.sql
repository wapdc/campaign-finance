CREATE OR REPLACE FUNCTION private.cf_get_pledge(p_fund_id INTEGER, p_trankeygen_id INTEGER) returns JSON AS $$
    DECLARE
        v_transaction json;

        BEGIN
        select row_to_json(f) into v_transaction from
           (
               SELECT vr.*, va.total as balance FROM private.vreceipts vr
               JOIN private.vaccounts va on vr.cid = va.trankeygen_id
               WHERE vr.fund_id = p_fund_id
               AND vr.trankeygen_id = p_trankeygen_id
            ) f;

        return v_transaction;

        END;
    $$ language plpgsql;