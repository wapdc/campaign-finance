CREATE OR REPLACE FUNCTION private.cf_delete_pledge(p_pledge_id integer) returns void as $$
    DECLARE
        v_cid INTEGER;
  begin
        select cid into v_cid from private.vreceipts where trankeygen_id = p_pledge_id;
        if v_cid is null then
            raise 'Pledge does not exist to delete: %', p_pledge_id;
        end if;

        perform private.cf_delete_receipt(p_pledge_id);
        delete from private.trankeygen where trankeygen_id = v_cid;
  end;
$$ language plpgsql;