CREATE OR REPLACE FUNCTION private.cf_save_cancel_pledge(p_fund_id INTEGER, p_json JSON) returns INTEGER as $$
    DECLARE
        v_cancel_id INTEGER;
        v_pid INTEGER;
        v_contid INTEGER;
        v_elekey smallint;
        v_itemized smallint;
        v_cid INTEGER;
        v_did INTEGER;
        v_amount numeric(16,2);
        v_old_cid INTEGER;
        v_old_did INTEGER;
        v_old_amount numeric(16,2);
        v_old_contid INTEGER;
        v_deptkey bigint;

    BEGIN

        select vr.trankeygen_id, vr.cid, vr.did, vr.contid, vr.elekey, vr.itemized into v_pid, v_did, v_cid, v_contid, v_elekey, v_itemized FROM private.vreceipts vr
        WHERE vr.fund_id = p_fund_id and vr.trankeygen_id = cast(p_json->>'pid' as integer);

        if v_pid is null then
            raise 'Missing base pledge';
        end if;

        if v_contid is null then
            raise 'Missing contact for pledge';
        end if;

        if v_cid is null then
            raise 'Missing pledge account';
        end if;

        v_cancel_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));
        v_amount = cast(p_json->>'amount' as numeric(16,2));

        SELECT vr.cid, vr.did, vr.amount, vr.contid into v_old_cid, v_old_did, v_old_amount, v_old_contid from private.vreceipts vr where vr.fund_id = p_fund_id and vr.trankeygen_id = v_cancel_id;

        v_deptkey = private.cf_generate_deposit_key(p_fund_id, to_date(p_json->>'date_', 'YYYY-MM-DD'));
        INSERT INTO private.receipts(
                                     trankeygen_id,
                                     type,
                                     aggtype,
                                     nettype,
                                     pid,
                                     cid,
                                     did,
                                     contid,
                                     amount,
                                     date_,
                                     elekey,
                                     itemized,
                                     memo,
                                     deptkey
                                     )
        VALUES (
                v_cancel_id,
                25,
                -1,
                0,
                v_pid,
                v_cid,
                v_did,
                v_contid,
                cast(p_json->>'amount' as numeric(16,2)),
                to_date(p_json->>'date_', 'YYYY-MM-DD'),
                v_elekey,
                v_itemized,
                p_json->>'memo',
                v_deptkey
                )
        ON CONFLICT (trankeygen_id) do update
        set amount = excluded.amount,
            date_ = excluded.date_,
            memo = excluded.memo,
            deptkey = excluded.deptkey;

    PERFORM private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, coalesce(v_old_cid, v_cid), v_amount);
      if (v_old_contid != v_contid) then
        perform private.cf_update_aggregates(v_old_contid);
      end if;
    PERFORM private.cf_update_aggregates(v_contid);

    return v_cancel_id;
    END;
$$ language plpgsql