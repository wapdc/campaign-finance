CREATE OR REPLACE FUNCTION private.cf_save_pledge(p_fund_id INTEGER, p_json json) returns integer
    language plpgsql as $$

    DECLARE
        v_pledge_id integer;
        v_cont_id integer;
        v_cid integer;
        v_did integer;
        v_old_cid integer;
        v_old_did integer;
        v_old_amount numeric(16,2);
        v_old_contid integer;
        v_deptkey bigint;

    begin

        select vc.trankeygen_id into v_cont_id from private.vcontacts vc
        where vc.fund_id = p_fund_id and vc.trankeygen_id = cast(p_json->>'contid' as integer);

        IF v_cont_id IS NULL THEN
            RAISE 'Missing contact';
        end if;

        v_pledge_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));
        select r.cid, r.did, r.amount, r.contid into v_old_cid, v_old_did, v_old_amount, v_old_contid from private.vreceipts r
        where trankeygen_id = v_pledge_id;

        v_cid = private.cf_ensure_trankeygen(p_fund_id, v_old_cid);

        select trankeygen_id into v_did from private.vaccounts va where va.fund_id = p_fund_id and va.acctnum = 0;

        INSERT INTO private.accounts (
                                      trankeygen_id,
                                      pid,
                                      acctnum,
                                      contid,
                                      style,
                                      total
                                     )
        values (
                v_cid,
                v_pledge_id,
                4900,
                cast(p_json->>'contid' as integer),
                3,
                0
               )
        ON CONFLICT (trankeygen_id) do update
        set contid = excluded.contid;

        v_deptkey = private.cf_generate_deposit_key(p_fund_id, to_date(p_json->>'date_', 'YYYY-MM-DD'));

        INSERT INTO private.receipts (
                                      trankeygen_id,
                                      type,
                                      aggtype,
                                      nettype,
                                      cid,
                                      did,
                                      contid,
                                      amount,
                                      date_,
                                      elekey,
                                      itemized,
                                      description,
                                      memo,
                                      deptkey
                                      )
        VALUES (
                v_pledge_id,
                12, -- pledge
                1,
                0,
                v_cid,
                v_did,
                v_cont_id,
                cast(p_json->>'amount' as numeric(16,2)),
                to_date(p_json->>'date_', 'YYYY-MM-DD'),
                cast(p_json->>'elekey' as integer),
                cast(p_json->>'itemized' as integer),
                p_json->>'description',
                p_json->>'memo',
                v_deptkey
               )
        ON CONFLICT (trankeygen_id) do update
        set contid = excluded.contid,
            amount = excluded.amount,
            date_ = excluded.date_,
            elekey = excluded.elekey,
            itemized = excluded.itemized,
            description = excluded.description,
            memo = excluded.memo,
            deptkey = excluded.deptkey;

        perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid,v_old_amount, v_did, v_cid, cast(p_json->> 'amount' as numeric));
        if (v_old_contid != v_cont_id) then
          perform private.cf_update_aggregates(v_old_contid);
        end if;
        perform private.cf_update_aggregates(v_cont_id);

        return v_pledge_id;
    end;

    $$