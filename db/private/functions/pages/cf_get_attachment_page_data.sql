DROP FUNCTION IF EXISTS private.cf_get_attachment_page_data(p_fund_id INTEGER, p_target_id INTEGER, p_target_type varchar(50));
CREATE OR REPLACE FUNCTION private.cf_get_attachment_page_data(p_fund_id INTEGER, p_target_id INTEGER, p_target_type varchar(50)) RETURNS text AS $$
    DECLARE
        j_attachment_page json;
        j_info json;

    BEGIN
        SELECT row_to_json(j)
        INTO j_attachment_page
        FROM
             (
                 select * from private.attachedtextpages a
                 where a.fund_id = p_fund_id
                 and a.target_id = p_target_id
                 and a.target_type = p_target_type
            ) j;

        SELECT row_to_json(j)
        FROM (SELECT j_attachment_page as "attachedPage") j INTO j_info;

        RETURN  j_info;
    END
$$ LANGUAGE plpgsql STABLE
