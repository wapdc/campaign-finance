CREATE OR REPLACE FUNCTION private.cf_get_fundraiser(p_fund_id INTEGER, p_trankeygen_id integer) returns JSON as $$
DECLARE
j_transaction json;

BEGIN
    SELECT row_to_json(f) INTO j_transaction FROM
                (SELECT a.*, c.name, c.street, c.city, c.state, c.zip, c.trankeygen_id
                 FROM private.vaccounts a
                 join private.contacts c on c.trankeygen_id = a.trankeygen_id
                 WHERE a.trankeygen_id = p_trankeygen_id and a.fund_id = p_fund_id
                ) f;

    return j_transaction;

END
  $$ language plpgsql;