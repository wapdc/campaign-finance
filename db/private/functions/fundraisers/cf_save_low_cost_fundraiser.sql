create or replace function private.cf_save_low_cost_fundraiser(p_fund_id integer, json_data json) returns integer
  language plpgsql
as $$
DECLARE
  v_contid int;
  v_fundraiser_id int;
  v_deposit_id int;
  v_cid int;
  v_old_cid int;
  v_old_did int;
  v_old_amount numeric(16,2);

BEGIN
  IF json_data->>'amount' is null or cast(json_data->>'amount' as numeric) = 0 then
    RAISE 'Invalid or missing amount' using column='amount';
  end if;

  IF json_data->>'name' is null THEN
    RAISE 'Missing fundraiser name' using column='name';
  end if;

  select min(trankeygen_id) into v_cid from private.vaccounts
  where acctnum = 1600 and fund_id=p_fund_id;

  IF cast(json_data->>'trankeygen_id' as integer) is not null then
    SELECT trankeygen_id, contid, cid, did, amount, deposit_id into v_fundraiser_id, v_contid, v_old_cid, v_old_did, v_old_amount, v_deposit_id from private.vreceipts where trankeygen_id=cast(json_data->>'trankeygen_id' as integer) and fund_id=p_fund_id;
  else
    v_contid := private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'contid' as integer));
    v_fundraiser_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));
  end if;

  INSERT INTO private.contacts(trankeygen_id, type, name, street, city, state, zip)
  values (v_contid,
          'HDN',
          json_data ->> 'name',
          json_data ->> 'street',
          json_data ->> 'city',
          json_data ->> 'state',
          json_data ->> 'zip') on conflict (trankeygen_id) do update
      set
          name=excluded.name,
          street=excluded.street,
          city=excluded.city,
          state=excluded.state,
          zip=excluded.zip;

  INSERT INTO private.accounts(trankeygen_id, acctnum, contid, style, memo, date_) values
    (v_contid,
     4097,
     v_contid,
     3,
     json_data ->> 'memo',
     to_date(json_data ->> 'date_', 'YYYY-MM-DD')
     )
     on conflict (trankeygen_id) do update
  set memo = excluded.memo,
      date_ = excluded.date_;


  INSERT into private.receipts(trankeygen_id, type, nettype,  contid, elekey, amount, date_, itemized, pid, cid, did, memo)
  values (v_fundraiser_id,
          38,
          1,
          v_contid,
          -1,
          cast(json_data ->> 'amount' as numeric),
          to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
          1,
          v_contid,
          v_cid,
          v_contid,
          json_data ->> 'memo')
  on conflict (trankeygen_id) do update
    set
      amount          = excluded.amount,
      date_           = excluded.date_,
      memo            = excluded.memo;

  perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_contid, v_cid,  cast(json_data ->> 'amount' as numeric) );
  perform private.cf_update_aggregates(cast(json_data ->>'contid' as integer));

  -- update deposit event total
  IF v_deposit_id IS NOT NULL THEN
      PERFORM private.cf_recalculate_deposit_event(p_fund_id, v_deposit_id);
  END IF;

  -- change date updated on this trankeygen and the contact trankeygen
  update private.trankeygen
  set rds_updated = now()
  where trankeygen_id = v_fundraiser_id;

  update private.trankeygen
  set rds_updated = now()
  where trankeygen_id = v_contid;

  RETURN v_fundraiser_id;

END
$$;