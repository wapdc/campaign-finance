CREATE OR REPLACE FUNCTION private.cf_delete_cash_fundraiser(p_id integer) returns void as
$$
declare
    v_cont_id INTEGER;
    v_contributions INTEGER;
begin

    select a.contid into v_cont_id
    FROM private.accounts a JOIN private.trankeygen t on a.trankeygen_id = t.trankeygen_id
    WHERE a.trankeygen_id = p_id;

    if v_cont_id IS null then
        raise exception 'You can not delete a fundraiser that does not exist.';
    end if;

    select count(1) into v_contributions
    FROM private.receipts r
    WHERE r.pid = v_cont_id;

    if v_contributions > 0 then
        raise exception 'You can not delete a fundraiser that has contributions recorded';
    end if;

    delete from private.accounts a where trankeygen_id=p_id;

end
$$ language plpgsql;