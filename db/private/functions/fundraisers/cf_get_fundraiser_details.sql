CREATE OR REPLACE FUNCTION private.cf_get_fundraiser_details(p_fund_id integer, p_trankeygen_id integer) returns json as $$
  declare
    v_json              JSONB;
    v_contributions     json;
  begin

    SELECT json_agg(c order by c.date_ desc) into v_contributions FROM (SELECT r.*
                           FROM private.vreceipts r
                           WHERE fund_id = p_fund_id
                           AND pid = p_trankeygen_id
                           AND r.type = 4
                           ) c;
    SELECT row_to_json(f) into v_json from (SELECT
                          private.cf_get_fundraiser(p_fund_id, p_trankeygen_id) as fundraiser,
                          v_contributions as contributions
                         ) f;

    return  v_json;
  end
  $$ language plpgsql;