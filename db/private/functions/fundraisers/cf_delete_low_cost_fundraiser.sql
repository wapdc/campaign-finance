CREATE OR REPLACE FUNCTION private.cf_delete_low_cost_fundraiser(p_id integer) returns void as
$$
    DECLARE
  v_did Integer;
begin
    select did into v_did from private.vreceipts where trankeygen_id = p_id;

    perform private.cf_delete_receipt(p_id);
    delete from private.trankeygen where trankeygen_id = v_did;
end;
$$ language plpgsql;