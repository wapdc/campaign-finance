create or replace function private.cf_save_cash_fundraiser(p_fund_id integer, json_data json) returns integer
  language plpgsql
as $$

DECLARE
  v_fundraiser_id int;

BEGIN
  IF json_data->>'name' is null THEN
    RAISE 'Missing fundraiser name' using column='name';
  END if;

  -- Set (and return) fundraiser_id to be the contid passed into the function in the json_data
  v_fundraiser_id := private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'contid' as integer));

  INSERT INTO private.contacts(trankeygen_id, type, name, street, city, state, zip)
  values (v_fundraiser_id,
          'HDN',
          json_data ->> 'name',
          json_data ->> 'street',
          json_data ->> 'city',
          json_data ->> 'state',
          json_data ->> 'zip')
  on conflict (trankeygen_id) do update
      set
          name=excluded.name,
          street=excluded.street,
          city=excluded.city,
          state=excluded.state,
          zip=excluded.zip;

  INSERT INTO private.accounts(trankeygen_id, acctnum, contid, style, memo, date_) values
      (v_fundraiser_id,
       4096,
       v_fundraiser_id,
       3,
       json_data ->> 'memo',
       to_date(json_data ->> 'date_', 'YYYY-MM-DD')
      )
  on conflict (trankeygen_id) do update
      set memo = excluded.memo,
          date_ = excluded.date_;

  return v_fundraiser_id;
END
$$;