CREATE OR REPLACE FUNCTION private.cf_save_invoiced_expense_payment(p_fund_id int, p_json json) returns int as $$
declare
  v_payment_id integer;
  v_interest_id integer;
  v_invoiced_expense_id integer;
  v_interest_account integer;
  v_cid integer;
  v_did integer;
  v_acctnum integer;
  v_contid integer;
  v_amount numeric;
  v_interest_amount numeric;
  v_date date;
  v_old_amount numeric(16,2);
  v_old_interest_amount integer;
  v_old_cid integer;
  v_old_did integer;
  v_old_contid integer;
  v_nettype integer := 3;
begin
  select trankeygen_id, bnkid, contid into v_invoiced_expense_id, v_cid, v_contid from private.vexpenditureevents ee
  where ee.fund_id = p_fund_id and ee.trankeygen_id = cast(p_json->>'pid' as integer);

  IF v_invoiced_expense_id is null then
    RAISE 'Invalid or missing vendor debt';
  end if;

  if v_cid is null then
    RAISE 'Invalid or missing account to pay (cid)';
  end if;

  SELECT trankeygen_id, acctnum into v_did, v_acctnum from private.vaccounts where fund_id=p_fund_id and trankeygen_id=cast(p_json->>'did' as integer);

  if v_did is null then
    RAISE 'Invalid or missing account to pay from (did)';
  end if;

  v_payment_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));
  -- Get the interest transaction id
  select trankeygen_id into v_interest_id from private.vreceipts r where r.fund_id = p_fund_id
                                                                     and r.type = 30 and r.pid=v_payment_id;
  select max(trankeygen_id) into v_interest_account from private.vaccounts a
  where a.fund_id = p_fund_id and (a.pid = v_invoiced_expense_id or a.pid is null) and acctnum = 5110;
  v_interest_id = private.cf_ensure_trankeygen(p_fund_id, v_interest_id);

  select cid,did,amount, contid into v_old_cid, v_old_did, v_old_amount, v_old_contid from private.vreceipts r where r.trankeygen_id=v_payment_id;
  select amount into v_old_interest_amount from private.vreceipts r where fund_id=p_fund_id and r.trankeygen_id=v_interest_id;

  v_amount = cast(p_json->>'amount' as numeric(16,2));
  v_interest_amount = coalesce(cast(p_json->>'interest' as numeric(16,2)), 0.0);
  v_date = cast(p_json->>'date_' as date);

  if v_acctnum in (2000,2600) then
      v_nettype = 0;
  end if;
  --insert vendor debt payment
  INSERT into private.receipts(trankeygen_id, type, nettype, pid, cid, did, contid,
                               amount, date_, description, memo)
  values (v_payment_id, 29, v_nettype,  v_invoiced_expense_id, v_cid, v_did, v_contid, v_amount, v_date, p_json->>'description', p_json->>'memo')
  ON CONFLICT (trankeygen_id) DO UPDATE SET
                                          amount = excluded.amount,
                                          description=excluded.description,
                                          date_ = excluded.date_,
                                          memo = excluded.memo,
                                          contid = excluded.contid,
                                          nettype = v_nettype;
  --insert interest
  INSERT INTO private.receipts(trankeygen_id, type, nettype, pid, cid, did, contid, amount, date_, description)
  values (v_interest_id, 30, v_nettype, v_payment_id, v_interest_account, v_did, v_contid, v_interest_amount, v_date,'Interest on debt')
  ON conflict (trankeygen_id) DO UPDATE SET
                                          amount=excluded.amount,
                                          date_ = excluded.date_,
                                          did = excluded.did,
                                          contid = excluded.contid,
                                          nettype = v_nettype;
  --update contact aggregates
  if (v_old_contid != v_contid) then
    perform private.cf_update_aggregates(v_old_contid);
  end if;
  perform private.cf_update_aggregates(v_contid);
  --update account amounts
  perform private.cf_account_adjust(p_fund_id , v_old_did, v_old_cid,  v_old_amount, coalesce(v_old_did, v_did), v_cid, v_amount);
  perform private.cf_account_adjust(p_fund_id, v_old_did, v_interest_account, v_old_interest_amount, v_did, v_interest_account, v_interest_amount);

  return v_payment_id;

end
$$ language plpgsql;