CREATE OR REPLACE FUNCTION private.cf_get_invoiced_expenses(p_fund_id INTEGER, p_trankeygen_id INTEGER) returns json as
$$
declare
    v_expenses JSON;
begin
    select json_agg(r)
    INTO v_expenses
    FROM (select r.*
          from private.vreceipts r
          WHERE r.fund_id = p_fund_id
            and r.type = 28
            and r.pid = p_trankeygen_id) r;

    return v_expenses;
end;
$$ language plpgsql;