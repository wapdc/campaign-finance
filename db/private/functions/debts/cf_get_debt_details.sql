CREATE OR REPLACE FUNCTION private.cf_get_debt_details(p_fund_id integer, p_trankeygen_id integer) returns json as $$
  declare
      v_json JSONB;
      v_vendor json;
      v_payments json;
      v_forgiven json;
      v_adjustment json;
      v_expenses json;
      v_expenditure_event json;
      v_expenditure_event_id int;
  begin


      select  row_to_json(p), p.trankeygen_id into v_expenditure_event, v_expenditure_event_id from (select
         e.*
         FROM
         private.vexpenditureevents e where e.bnkid = p_trankeygen_id and fund_id = p_fund_id) p;

      select json_agg(p order by p.date_ desc, p.trankeygen_id desc) INTO v_payments FROM
          (select vr.*, ri.amount as interest from private.vreceipts vr
              left join private.vreceipts ri on ri.pid=vr.trankeygen_id
              and ri.type=30
           where vr.fund_id = p_fund_id
             and vr.pid = v_expenditure_event_id
             and vr.type = 29
          ) p;

      select json_agg(p order by p.date_ desc, p.trankeygen_id desc) INTO v_forgiven FROM
          (select vr.* from private.vreceipts vr
           where vr.fund_id = p_fund_id
             and vr.pid = v_expenditure_event_id
             and vr.type = 44
          ) p;

      select json_agg(p order by p.date_ desc, p.trankeygen_id desc) INTO v_adjustment FROM (select r.*
          from private.vreceipts r
                   join private.vreceipts r2 on r.pid = r2.trankeygen_id
          where r.fund_id = p_fund_id
            and r.did_acctnum = 2000
            and r2.pid = v_expenditure_event_id
            and r.type = 20) p;

      select row_to_json(v) INTO v_vendor from (
          select -1.0 * a.total as balance, a.pid, a.acctnum, a.contid, a.code,a.description,a.style,a.memo, c.name, c.street, c.city, c.zip from private.accounts a
             join private.contacts c on c.trankeygen_id = a.contid
             where a.trankeygen_id = p_trankeygen_id) v;

       select json_agg(r) INTO v_expenses FROM (select r.name, r.description, r.amount  from private.vreceipts r
         WHERE r.fund_id = p_fund_id and
         r.type = 28 and r.pid = v_expenditure_event_id) r;

      select row_to_json(c) into v_json from ( select
          v_vendor as vendor,
          v_payments AS payments,
          v_adjustment AS adjustment,
          v_forgiven AS forgiven,
          v_expenditure_event as expenditure_event,
          v_expenses as expenses
          ) c;
      return  v_json;

  end
$$ language plpgsql;
