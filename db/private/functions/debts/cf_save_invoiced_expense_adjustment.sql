CREATE OR REPLACE FUNCTION private.cf_save_invoiced_expense_adjustment(p_fund_id int, p_json json) returns int as $$
    declare
        v_adjustment_id integer;
        v_expense_record_id integer;
        v_cid integer;
        v_did integer;
        v_contid integer;
        v_amount numeric(16,2);
        v_date date;
        v_old_amount numeric(16,2);
        v_old_cid integer;
        v_old_did integer;
        v_old_contid integer;
    begin

        select vr.trankeygen_id, vr.cid, vr.did, vr.contid into v_expense_record_id, v_cid, v_did, v_contid from private.vreceipts vr
        where vr.fund_id = p_fund_id and vr.trankeygen_id = cast(p_json->>'pid' as integer);

        if v_expense_record_id is null then
                raise 'Invalid or missing expense record';
        end if;

        if v_contid is null then
            raise 'missing contact';
        end if;


        -- setting values
        v_adjustment_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));

        select cid, did, amount, contid into v_old_cid, v_old_did, v_old_amount, v_old_contid from private.vreceipts r where r.trankeygen_id = v_adjustment_id;

        v_amount = cast(p_json->>'amount' as numeric(16,2));
        v_date = cast(p_json->>'date_' as date);

        -- inserting invoiced expense adjustment
        INSERT INTO private.receipts(trankeygen_id, type, pid, cid, did, contid,
                                     amount, date_, memo)
        values (v_adjustment_id, 20, v_expense_record_id, v_cid, v_did, v_contid, v_amount, v_date, p_json->>'memo')
        on conflict (trankeygen_id) do update set
                                  amount = excluded.amount,
                                  date_ = excluded.date_,
                                  contid = excluded.contid,
                                  memo = excluded.memo;

        --update contact aggregates
        if (v_old_contid != v_contid) then
          perform private.cf_update_aggregates(v_old_contid);
        end if;
        perform private.cf_update_aggregates(v_contid);
        -- update accounting
        perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, coalesce(v_old_did, v_did), v_cid, v_amount);

        return v_adjustment_id;

    end
$$ language plpgsql;