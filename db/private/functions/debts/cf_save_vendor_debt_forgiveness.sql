CREATE OR REPLACE FUNCTION private.cf_save_vendor_debt_forgiveness(p_fund_id int, p_json json) returns int as $$
DECLARE
    v_forgiven_id integer;
    v_expense_id integer;
    v_did integer;
    v_cid integer;
    v_old_cid integer;
    v_old_did integer;
    v_old_amount numeric(16,2);
    v_old_contid integer;
    v_cont_id integer;
    v_amount numeric;
    v_date date;
    v_elekey smallint;
    v_itemized smallint;
    v_description text;
    v_memo text;
    v_deptkey bigint;

BEGIN
    --Get the base expenditure information and account to debit
    select trankeygen_id, bnkid, contid into v_expense_id, v_cid, v_cont_id from private.vexpenditureevents ee
    where ee.fund_id = p_fund_id and ee.trankeygen_id = cast(p_json->>'pid' as integer);

    -- Initial account
    SELECT trankeygen_id into v_did from private.vaccounts where fund_id = p_fund_id and acctnum = 4100;

    IF v_expense_id is null then
        RAISE 'Invalid or missing expense';
    end if;

    --Establish a trankeygen for the forgiven record
    v_forgiven_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));

    --Select existing accounts/amount to use later to update account
    select cid,did,amount, contid into v_old_cid, v_old_did, v_old_amount, v_old_contid from private.vreceipts r where
                                r.trankeygen_id=v_forgiven_id;

    v_amount = cast(p_json->>'amount' as numeric(16,2));
    v_date = cast(p_json->>'date_' as date);
    v_elekey = cast(p_json->>'elekey' as smallint);
    v_itemized = cast(p_json->>'itemized' as smallint);
    v_description = cast(p_json->>'description' as text);
    v_memo = cast(p_json->>'memo' as text);
    v_deptkey = private.cf_generate_deposit_key(p_fund_id, v_date);

    --insert forgiven amount
    INSERT INTO private.receipts(trankeygen_id, type, nettype, aggtype, pid,cid,did,contid, amount,date_,elekey,itemized,description,memo, deptkey)
    values(v_forgiven_id, 44, 1, 1, v_expense_id,v_cid,v_did,v_cont_id,v_amount,v_date,v_elekey,coalesce(v_itemized,0),v_description,v_memo, v_deptkey)
    ON CONFLICT (trankeygen_id) DO UPDATE SET
        amount = excluded.amount,
        contid  =excluded.contid,
        description = excluded.description,
        date_ = excluded.date_,
        elekey = excluded.elekey,
        deptkey = excluded.deptkey;

    --update accounts totals
    perform private.cf_account_adjust(p_fund_id , v_old_did, v_old_cid,  v_old_amount, v_did, v_cid, v_amount);

    --adjust aggregates
    if (v_old_contid != v_cont_id) then
      perform private.cf_update_aggregates(v_old_contid);
    end if;
    perform private.cf_update_aggregates(v_cont_id);
    return v_forgiven_id;
END
$$ language plpgsql;
