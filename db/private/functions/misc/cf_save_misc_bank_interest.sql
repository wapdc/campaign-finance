create or replace function private.cf_save_misc_bank_interest(p_fund_id integer, json_data json) returns integer
 language plpgsql
as $$
DECLARE
    v_contid int;
    v_bank_interest_id int;
    v_deposit_id int;
    v_cid int;
    v_did int;
    v_old_cid int;
    v_old_did int;
    v_old_amount numeric(16,2);
    v_old_contid int;

BEGIN
    IF json_data->>'amount' is null or cast(json_data->>'amount' as numeric) = 0 then
        RAISE 'Invalid or missing amount' using column='amount';
    end if;

    IF json_data->>'date_' is null THEN
        RAISE 'Missing date' using column='date';
    end if;

    IF json_data->>'description' is null THEN
        RAISE 'Missing description' using column='description';
    end if;

    select min(trankeygen_id) into v_cid from private.vaccounts
    where acctnum = 1600 and fund_id=p_fund_id;


    select contid into v_contid from private.vaccounts a where a.trankeygen_id = cast(json_data ->>'contid' as integer)
    and a.fund_id = p_fund_id;

    if v_contid is null then
        raise 'Invalid account_id: %', json_data ->>'contid';
    end if;

    v_bank_interest_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));

    select trankeygen_id into v_did from private.vaccounts a where a.pid = v_bank_interest_id and a.fund_id = p_fund_id and acctnum = 4400;

    v_did = private.cf_ensure_trankeygen(p_fund_id, v_did);

    select r.cid, r.did, r.amount, r.deposit_id, r.contid into v_old_cid, v_old_did, v_old_amount, v_deposit_id, v_old_contid from private.vreceipts r where trankeygen_id = v_bank_interest_id;

    INSERT INTO private.accounts(trankeygen_id, pid, acctnum, contid, style) values
    (v_did,
     v_bank_interest_id,
     4400,
     v_contid,
     3
    )
    on conflict (trankeygen_id) do update
    set date_ = excluded.date_,
        contid = excluded.contid;

    INSERT into private.receipts(trankeygen_id, type, nettype, cid, did, contid, amount, date_, description, memo)
    values(v_bank_interest_id,
           15,
            1,
           v_cid,
           v_did,
           v_contid,
           cast(json_data ->> 'amount' as numeric),
           to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
           json_data ->> 'description',
           json_data ->> 'memo')
    on conflict (trankeygen_id) do update
        set
            amount      = excluded.amount,
            date_       = excluded.date_,
            description = excluded.description,
            memo = excluded.memo,
            contid = excluded.contid;

    perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, coalesce(v_old_cid, v_cid), cast(json_data ->>'amount' as numeric));
    if (v_old_contid != v_contid) then
      perform private.cf_update_aggregates(v_old_contid);
    end if;
    perform private.cf_update_aggregates(cast(json_data ->> 'contid' as integer));

    IF v_deposit_id IS NOT NULL THEN
        PERFORM private.cf_recalculate_deposit_event(p_fund_id, v_deposit_id);
    END IF;

    update private.trankeygen
    set rds_updated = now()
    where trankeygen_id = v_bank_interest_id;

    update private.trankeygen
    set rds_updated = now()
    where trankeygen_id = v_contid;

    update private.trankeygen
    set rds_updated = now()
    where trankeygen_id = v_did;

    return v_bank_interest_id;
END
    $$