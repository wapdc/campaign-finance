create or replace function private.cf_save_misc_other(p_fund_id integer, json_data json) returns integer
  language plpgsql
as $$
DECLARE
  v_contid int;
  v_misc_id int;
  v_deposit_id int;
  v_cid int;
  v_did int;
  v_old_cid int;
  v_old_did int;
  v_old_amount numeric(16,2);

BEGIN
  IF json_data->>'amount' is null or cast(json_data->>'amount' as numeric) = 0 then
    RAISE 'Invalid or missing amount' using column='amount';
  end if;

  IF json_data->>'date_' is null THEN
    RAISE 'Missing date' using column='date';
  end if;

  IF json_data->>'name' is null THEN
    RAISE 'Missing fundraiser name' using column='name';
  end if;

  v_misc_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));

  select r.cid, r.did, r.amount, r.contid, r.deposit_id into v_old_cid, v_old_did, v_old_amount, v_contid, v_deposit_id from private.vreceipts r where trankeygen_id=v_misc_id;

  select min(trankeygen_id) into v_cid from private.vaccounts
  where acctnum = 1600 and fund_id=p_fund_id;

  select min(trankeygen_id) into v_did from private.vaccounts
  where acctnum = 4500 and fund_id=p_fund_id;

  v_contid = private.cf_ensure_trankeygen(p_fund_id, v_contid);

  INSERT INTO private.contacts(trankeygen_id, type, name, street, city, state, zip)
  values (v_contid,
          'HDN',
          json_data ->> 'name',
          json_data ->> 'street',
          json_data ->> 'city',
          json_data ->> 'state',
          json_data ->> 'zip') on conflict (trankeygen_id) do update
    set
      name=excluded.name,
      street=excluded.street,
      city=excluded.city,
      state=excluded.state,
      zip=excluded.zip;

  INSERT into private.receipts(trankeygen_id, type, nettype, contid, amount, date_, pid, cid, did, memo, checkno, description)
  values (v_misc_id,
          14,
          1,
          v_contid,
          cast(json_data ->> 'amount' as numeric),
          to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
          -1,
          v_cid,
          v_did,
          json_data ->> 'memo',
          json_data ->> 'checkno',
          json_data ->> 'description')
  on conflict (trankeygen_id) do update
    set
      amount          = excluded.amount,
      date_           = excluded.date_,
      memo            = excluded.memo,
      checkno         = excluded.checkno,
      description     = excluded.description;

  perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, coalesce(v_old_cid,v_cid), cast(json_data ->> 'amount' as numeric));
  perform private.cf_update_aggregates(cast(json_data ->>'contid' as integer));

  IF v_deposit_id IS NOT NULL THEN
      PERFORM private.cf_recalculate_deposit_event(p_fund_id, v_deposit_id);
  END IF;

  -- change date updated on this trankeygen and the contact trankeygen
  update private.trankeygen
  set rds_updated = now()
  where trankeygen_id = v_misc_id;

  update private.trankeygen
  set rds_updated = now()
  where trankeygen_id = v_contid;

  RETURN v_misc_id;

END
$$;