DROP FUNCTION IF EXISTS private.cf_do_deletions(fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_do_deletions(p_fund_id INTEGER, json_data json)
    RETURNS text as $$
DECLARE
    highest_updated timestamp;
    c record;
BEGIN
    -- Loop through array of transactions to delete
    FOR c IN
        SELECT j.*, o.trankeygen_id
        FROM (select
                  cast(value->>'orca_id' as int) as orca_id,
                  to_timestamp(value->>'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated

              FROM json_array_elements(json_data) d ) j
                  LEFT JOIN private.trankeygen o on o.orca_id = j.orca_id AND o.fund_id = p_fund_id
        LOOP
            IF c.derby_updated > highest_updated OR highest_updated is null THEN highest_updated := c.derby_updated; END IF;
            IF c.trankeygen_id IS NOT NULL THEN
                delete from private.trankeygen where trankeygen_id=c.trankeygen_id;
                delete from private.deposititems di where di.deptid = c.trankeygen_id;
            END IF;
        END LOOP;
    RETURN cast (highest_updated as text);
END
$$ LANGUAGE plpgsql;