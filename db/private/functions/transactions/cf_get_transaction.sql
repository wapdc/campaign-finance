--DROP FUNCTION IF EXISTS private.cf_get_transaction(p_fund_id INTEGER, p_trankeygen_id INTEGER);
CREATE OR REPLACE FUNCTION private.cf_get_transaction(p_fund_id INTEGER, p_trankeygen_id INTEGER) RETURNS json AS $$
DECLARE
    j_transaction json;

BEGIN

SELECT row_to_json(r) INTO j_transaction FROM
        (select rec.*, de.date_ as deposit_date,
                ri.amount as interest, ri.trankeygen_id as interest_rds_id, ri.cid as interest_account_id, ri.cid_orca as interest_account_orca,
             case when rec.type=1 then
              (select sum(amount) from private.vreceipts r2
              where r2.pid = p_trankeygen_id
                and r2.type=27)
          end as total_refunded
         from private.vreceipts rec
   left join private.vdepositevents de on rec.deptkey <> 0 and de.fund_id = p_fund_id and de.deptkey=rec.deptkey
   left join private.vreceipts ri on ri.pid = rec.trankeygen_id and ri.fund_id=p_fund_id and ri.type=30
   WHERE rec.trankeygen_id = p_trankeygen_id and rec.fund_id = p_fund_id) r;

RETURN j_transaction;
END
$$ LANGUAGE plpgsql STABLE
