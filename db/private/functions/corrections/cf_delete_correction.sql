CREATE OR REPLACE FUNCTION private.cf_delete_correction(correction_id integer) returns void as
$$
declare
    v_type INTEGER;
    v_fund_id INTEGER;
    v_cont_id INTEGER;
    v_cid INTEGER;
    v_did INTEGER;
    v_amount NUMERIC(16,2);
    v_reported_type INTEGER;
begin
    -- Determine transaction type
    select r.type, t.fund_id, r.contid, r.cid, r.did, r.amount, r2.type INTO v_type, v_fund_id, v_cont_id, v_cid, v_did, v_amount, v_reported_type
    FROM private.receipts r JOIN private.trankeygen t on r.trankeygen_id = t.trankeygen_id
    LEFT JOIN private.receipts r2 on r.pid = r2.trankeygen_id
    WHERE r.trankeygen_id = correction_id;

    delete from private.trankeygen t where t.trankeygen_id=correction_id;

    IF v_reported_type not in (24, 34) then
        perform private.cf_update_aggregates(v_cont_id);
        -- Back out the transaction.
        perform private.cf_account_transfer_funds(v_fund_id, v_cid, v_cont_id, v_amount);
    end if;
end;
$$ language plpgsql;