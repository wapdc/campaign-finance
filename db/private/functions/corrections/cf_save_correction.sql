create or replace function private.cf_save_correction(p_fund_id integer, json_data json) returns integer
    language plpgsql
as $$
DECLARE
    v_reported_id int;
    v_reported_type int;
    v_reported_date date;
    v_reported_amount int;
    v_corrected_id int;
    v_cid int;
    v_did int;
    v_contid int;
    v_old_cid int;
    v_old_did int;
    v_old_amount numeric(16,2);
    v_old_contid int;
    v_type int;
    v_pid int;

BEGIN
    IF json_data->>'reported_id' is null then
        IF json_data->>'reported_amount' is null or cast(json_data->>'reported_amount' as numeric) = 0 then
            RAISE 'Invalid or missing reported amount' using column='reported_amount';
        else
            v_reported_amount = cast(json_data->>'reported_amount' as numeric);
        end if;

        IF json_data->>'reported_date' is null then
            RAISE 'Invalid or missing reported date' using column='reported_date';
        end if;

        IF json_data->>'type' is null or cast(json_data->>'type' as integer) not in (23, 33) then
            RAISE 'Invalid or missing type' using column='type';
        end if;
    ELSE
        IF json_data->>'trankeygen_id' is not null then
            select contid, cid, did, amount, contid into v_contid, v_old_cid, v_old_did, v_old_amount, v_old_contid from private.vreceipts where trankeygen_id = cast(json_data->>'trankeygen_id' as integer) and fund_id = p_fund_id;
        end if;

        v_reported_id := cast(json_data->>'reported_id' as integer);
        select r.type, r.amount, r.date_, r.cid, r.did, r.contid into v_reported_type, v_reported_amount, v_reported_date, v_cid, v_did, v_contid from private.vreceipts r where r.fund_id = p_fund_id and r.trankeygen_id = v_reported_id;
    end if;

    IF json_data->>'corrected_amount' is null or cast(json_data->>'corrected_amount' as numeric) = 0 then
        RAISE 'Invalid or missing discovered amount' using column='corrected_amount';
    end if;

    IF json_data->>'discovered_date' is null then
        RAISE 'Invalid or missing discovered date' using column='discovered_date';
    end if;

    IF json_data->>'description' is null then
        RAISE 'Missing description' using column='description';
    end if;

    -- math error
    v_type := cast(json_data->>'type' as integer);

    -- Determine receipt or expenditure
    IF v_reported_id is not null AND v_reported_type NOT IN (34, 24) then
        -- assign receipt type base on correction type
        v_type := case
                      when v_reported_type = 6 then 35
                      when v_reported_type in (28, 34) then 33
                      else 23
            end;

        v_pid := v_reported_id;
    else
        -- math error
        -- generate a receipt record with the correct type to restore reported date and amount
        v_reported_type := case
                               when v_type = 33 then 34
                               when v_type = 23 then 24
            end;

        v_reported_amount = cast(json_data->>'reported_amount' as numeric);

        v_reported_id := private.cf_ensure_trankeygen(p_fund_id, v_reported_id);
        INSERT into private.receipts(trankeygen_id, type, pid, amount, date_, description, memo)
        values (v_reported_id,
                v_reported_type,
                v_reported_id,
                cast(json_data ->> 'reported_amount' as numeric),
                to_date(json_data ->> 'reported_date', 'YYYY-MM-DD'),
                json_data ->> 'description',
                json_data ->> 'memo')
        on conflict (trankeygen_id) do update
            set
                type            = excluded.type,
                amount          = excluded.amount,
                date_           = excluded.date_,
                description     = excluded.description,
                memo            = excluded.memo;
    end if;

    -- Correction record
    v_corrected_id := private.cf_ensure_trankeygen(p_fund_id, cast(json_data->>'trankeygen_id' as integer));


    INSERT into private.receipts(trankeygen_id, type, pid, did, cid, contid, amount, date_, description, memo)
    values (v_corrected_id,
            v_type,
            v_reported_id,
            v_did,
            v_cid,
            v_contid,
            cast(json_data->> 'corrected_amount' as numeric) - v_reported_amount,
            to_date(json_data ->> 'discovered_date', 'YYYY-MM-DD'),
            json_data ->> 'description',
            json_data ->> 'memo')
    on conflict (trankeygen_id) do update
        set
            amount          = excluded.amount,
            type            = excluded.type,
            pid             = excluded.pid,
            cid             = excluded.cid,
            did             = excluded.did,
            contid          = excluded.contid,
            date_           = excluded.date_,
            description     = excluded.description,
            memo            = excluded.memo;

    if (v_old_contid != v_contid) then
      perform private.cf_update_aggregates(v_old_contid);
    end if;
    perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount,  v_did, v_cid, cast(json_data->> 'corrected_amount' as numeric) - v_reported_amount );
    perform private.cf_update_aggregates(v_contid);

    -- change date updated on this trankeygen and the contact trankeygen
    update private.trankeygen
    set rds_updated = now()
    where trankeygen_id = v_reported_id;

    update private.trankeygen
    set rds_updated = now()
    where trankeygen_id = v_corrected_id;

    RETURN v_corrected_id;
END
$$;
