DROP FUNCTION IF EXISTS private.cf_get_correction(p_fund_id INTEGER, p_trankeygen_id INTEGER);
CREATE OR REPLACE FUNCTION private.cf_get_correction(p_fund_id INTEGER, p_trankeygen_id INTEGER) RETURNS json AS $$
DECLARE
    c_transaction json;
BEGIN
    SELECT row_to_json(r) INTO c_transaction FROM
        (select r2.date_ as reported_date,
                r2.amount as reported_amount,
                r2.type as reported_type,
                r2.trankeygen_id as reported_id,
                r2.orca_id as reported_orca_id,
                r1.date_ as discovered_date,
                r1.amount + r2.amount as corrected_amount,
                r1.*
         from private.vreceipts r1
                  left join private.vreceipts r2 on r2.trankeygen_id=r1.pid
         where r1.fund_id = p_fund_id
           and r1.trankeygen_id = p_trankeygen_id
           and r1.type in (23, 33, 42, 35)
         order by r1.date_ desc, r1.trankeygen_id desc) r;
    RETURN c_transaction;
END
$$ LANGUAGE plpgsql STABLE;

