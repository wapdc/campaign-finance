
drop function if exists private.cf_contact_find_duplicates(p_fund_id  INTEGER, p_json JSON);
create or replace function private.cf_contact_find_duplicates(p_fund_id  INTEGER, p_json JSON, mode VARCHAR default 'fuzzy')
  returns table(contact_id integer, name_similarity numeric, address_similarity numeric) as $$
  declare
    j_address text;
  begin
    j_address:= coalesce(p_json ->> 'street', '') || ',' || coalesce(p_json ->> 'city', '')
                    || ',' || coalesce(p_json ->> 'state', '') || ',' || coalesce(p_json ->> 'zip', '');

    IF mode = 'exact' THEN
        return query
            select trankeygen_id as contact_id,
                   1.0 as name_similarity,
                   fuzzy_address_match(j_address,coalesce(street,''),coalesce(city,''),coalesce(state,''),coalesce(zip,''))::numeric as address_similarity
            from private.vcontacts
            where fund_id = p_fund_id
              and (p_json ->>'name' = name or
                   (name = p_json->>'firstname' || ' ' || coalesce(p_json->>'middleinitial','') || ' ' ||coalesce(p_json->>'lastname','')))
            order by 2 desc,
                     3 desc,
                     contact_id;
    ELSEIF mode='like' THEN
        return query
            select trankeygen_id as contact_id,
                   fuzzy_name_match(p_json ->> 'name', name)::numeric as name_similarity,
                   fuzzy_address_match(j_address,coalesce(street,''),coalesce(city,''),coalesce(state,''),coalesce(zip,''))::numeric as address_similarity
            from private.vcontacts
            where fund_id = p_fund_id
              and name ilike replace(p_json ->>'name', ' ', '%')
              and (type = p_json ->> 'type' or p_json ->> 'type' not in ('CPL', 'IND'))
            order by 2 desc,
                     3 desc,
                     contact_id;
    ELSE
        return query
            select trankeygen_id as contact_id,
                   fuzzy_name_match(p_json ->> 'name', name)::numeric as name_similarity,
                   fuzzy_address_match(j_address,coalesce(street,''),coalesce(city, '') ,coalesce(state,''), coalesce(zip,''))::numeric as address_similarity
            from private.vcontacts
            where fund_id = p_fund_id
              and fuzzy_name_match(p_json ->> 'name', name) > 0.5
              and (type = p_json ->> 'type' or p_json ->> 'type' not in ('CPL', 'IND'))
            order by 2 desc,
                     3 desc,
                     contact_id;
    end if;

  end
$$ LANGUAGE plpgsql;
