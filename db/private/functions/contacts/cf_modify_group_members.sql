create or replace function private.cf_modify_group_members(p_fund_id integer, p_json JSON) returns void as $$

  declare
      v_group_id integer;
      v_contact_count integer;
  begin
    -- Make sure the group is in the fund we said was
    select trankeygen_id into v_group_id from private.vcontacts c where fund_id=p_fund_id and c.trankeygen_id = cast(p_json->>'trankeygen_id' as integer);

    if v_group_id is null then
        raise 'Invalid or missing group trankeygen_id: %', p_json->>'trankeygen_id';
    end if;

    -- Check contacts on database for adding members
    select count(*) into v_contact_count from json_array_elements(p_json->'add') jc
    join private.vcontacts c on c.trankeygen_id=jc::text::int;

    if v_contact_count < json_array_length(p_json->'add') then
        raise 'One or more invalid contact cannot be added';
    end if;

    -- Check contacts on database for removing members
    select count(*) into v_contact_count from json_array_elements(p_json->'remove') jc
    join private.vcontacts c on c.trankeygen_id=jc::text::int;

    if v_contact_count < json_array_length(p_json->'remove') then
        raise 'One or more invalid contact cannot be removed';
    end if;

    -- insert added members into the database
    insert into private.registeredgroupcontacts(gconid, contid)
      select v_group_id, c.trankeygen_id from json_array_elements(p_json->'add') jc
        join private.vcontacts c on c.trankeygen_id=jc::text::int
    on conflict (gconid,contid) do nothing;

    delete from private.registeredgroupcontacts where gconid=v_group_id
          and contid in (
              select c.trankeygen_id from json_array_elements(p_json->'remove') jc
               join private.vcontacts c on c.trankeygen_id=jc::text::int where fund_id=p_fund_id
          );
  end;
$$ language plpgsql