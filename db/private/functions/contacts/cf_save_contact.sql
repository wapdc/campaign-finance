--DROP function private.cf_save_contact(p_fund_id INTEGER, json_data JSON);
CREATE OR REPLACE FUNCTION private.cf_save_contact(p_fund_id INTEGER, json_data JSON)
  RETURNS int as
$$
DECLARE
  v_contact_id int;
BEGIN

    IF json_data->>'type' IS NULL OR json_data ->>'type' not in ('SP', 'LDP', 'IND', 'CPL', 'TRB', 'OG', 'HDN', 'BUS', 'CP', 'MP', 'CAU', 'CAN', 'GRP', 'FI', 'PAC', 'UN') THEN
        RAISE 'Missing or invalid contact type' using COLUMN = 'type';
    ELSE IF json_data->>'state' is not null AND LENGTH(json_data->>'state') > 2 THEN
        RAISE 'Invalid or misspelled state' using column = 'state';
    ELSE
        IF json_data->>'type' = 'IND' AND json_data->>'firstname' is null THEN
            RAISE 'Individual Missing first name' using COLUMN = 'firstname';
        end if;
    end if;
    end if;

  v_contact_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));

  INSERT into private.contacts(trankeygen_id, type, name, street, city, state, zip, phone, email, memo, occupation, employername, employerstreet, employercity, employerstate, employerzip, contact_key)
  values (v_contact_id,
          json_data ->> 'type',
          json_data ->> 'name',
          json_data ->> 'street',
          json_data ->> 'city',
          json_data ->> 'state',
          json_data ->> 'zip',
          json_data ->> 'phone',
          json_data ->> 'email',
          json_data ->> 'memo',
          json_data ->> 'occupation',
          json_data ->> 'employername',
          json_data ->> 'employerstreet',
          json_data ->> 'employercity',
          json_data ->> 'employerstate',
          json_data ->> 'employerzip',
          coalesce(json_data ->> 'contact_key', v_contact_id::text))
  on conflict (trankeygen_id) do update
    set type            = excluded.type,
        name            = excluded.name,
        street          = excluded.street,
        city            = excluded.city,
        state           = excluded.state,
        zip             = excluded.zip,
        phone           = excluded.phone,
        email           = excluded.email,
        memo            = excluded.memo,
        occupation      = excluded.occupation,
        employername    = excluded.employername,
        employerstreet  = excluded.employerstreet,
        employercity    = excluded.employercity,
        employerstate   = excluded.employerstate,
        employerzip     = excluded.employerzip
  ;

  IF  json_data ->> 'firstname' IS NOT NULL THEN
  INSERT into private.individualcontacts(trankeygen_id, prefix, firstname, middleinitial,lastname,suffix)
  values (v_contact_id,
          json_data ->> 'prefix',
          json_data ->> 'firstname',
          json_data ->> 'middleinitial',
          json_data ->> 'lastname',
          json_data ->> 'suffix')
  on conflict (trankeygen_id) do update
      set prefix           = excluded.prefix,
          firstname        = excluded.firstname,
          middleinitial    = excluded.middleinitial,
          lastname         = excluded.lastname,
          suffix           = excluded.suffix;
  END IF;

  IF (json_data ->> 'type' = 'CPL') then
  INSERT into private.couplecontacts(trankeygen_id, contact1_id, contact2_id)
  values (v_contact_id,
          (json_data ->> 'contact1_id')::int,
          (json_data ->> 'contact2_id')::int)
  on conflict (trankeygen_id) do update
           set contact1_id        = excluded.contact1_id,
               contact2_id        = excluded.contact2_id;
  END IF;

  RETURN v_contact_id;

END
$$ LANGUAGE plpgsql;