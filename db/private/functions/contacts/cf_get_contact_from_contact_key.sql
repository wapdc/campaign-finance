create or replace function private.cf_get_contact_from_contact_key(import_contact_key text, n_fund_id integer) returns integer
    language plpgsql
as
$$

DECLARE
    v_contact_id integer;

BEGIN
    SELECT c.trankeygen_id INTO v_contact_id FROM private.vcontacts c
    WHERE c.contact_key = import_contact_key::text
      AND c.fund_id = n_fund_id;

    RETURN v_contact_id;
END
$$;
