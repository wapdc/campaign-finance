DROP FUNCTION IF EXISTS private.cf_get_contact_transactions(p_fund_id INTEGER, p_trankeygen_id INTEGER);
CREATE OR REPLACE FUNCTION private.cf_get_contact_transactions(p_fund_id INTEGER, p_trankeygen_id INTEGER) RETURNS text AS $$
DECLARE
  j_contact_info json;
  j_members json;
  j_couples json;
  j_groups json;
  j_transactions json;
  j_info json;
  v_contact_type text;

BEGIN

  SELECT row_to_json(r), r.type  INTO j_contact_info, v_contact_type FROM (select * from private.vcontacts c
                                           WHERE trankeygen_id = p_trankeygen_id and fund_id = p_fund_id) r;

  if v_contact_type = 'GRP' then
       SELECT json_agg(j) into j_members from (
           select name, street, city, trankeygen_id, orca_id from private.vcontacts c join private.registeredgroupcontacts gc
               on gc.contid=c.trankeygen_id where gconid = p_trankeygen_id
                                              )j;
  elseif v_contact_type = 'CPL' then
      SELECT json_agg(j) INTO j_members
      FROM (select name, trankeygen_id from private.vcontacts where trankeygen_id in
                                                               (select cc.contact_id from private.vcouplecontacts cc where p_trankeygen_id = cc.couple_contact_id)                                                                and fund_id = p_fund_id) j;
  end if;


  select json_agg(g)
  into j_groups
  from (select * from private.vcontacts c where fund_id = p_fund_id and
                                                trankeygen_id = (select gconid from private.registeredgroupcontacts where contid = p_trankeygen_id)) g;


  SELECT json_agg(c)
  INTO j_couples
  FROM (select c.name, c.type, c.trankeygen_id, c.pagg, c.gagg, c.contact1_id, c.contact2_id, c.street, c.city, c.street, c.zip, c.phone, c.email
        from private.vcontacts c
        where c.fund_id = p_fund_id
          and (c.contact1_id = p_trankeygen_id or c.contact2_id = p_trankeygen_id)
       ) c;

  select json_agg(t)
  into j_transactions
  from (select coalesce(r.trankeygen_id, l.trankeygen_id, ee.trankeygen_id, d.debtobligation_id) as transaction_id
        from private.trankeygen t
               left join private.receipts r on r.contid = t.trankeygen_id
               left join private.expenditureevents ee on ee.contid = t.trankeygen_id
               left join private.loans l on l.contid = t.trankeygen_id
               left join private.debtobligation d on d.contid = t.trankeygen_id or d.pid = t.trankeygen_id
        where t.fund_id = p_fund_id
          and t.trankeygen_id = p_trankeygen_id) t where t.transaction_id is not null;

  select row_to_json( j ) from (SELECT j_contact_info as "contact",
                                       j_members as "members",
                                       j_transactions as "transactions",
                                       j_groups as "groups",
                                       j_couples as "couples") j
  INTO j_info;

  RETURN cast (j_info as text);

END
$$ LANGUAGE plpgsql STABLE