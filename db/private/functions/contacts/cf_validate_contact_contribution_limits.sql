--grab the limit
CREATE OR REPLACE FUNCTION private.cf_validate_contact_contribution_limits(p_fund_id INTEGER, p_contact_id INTEGER)
  RETURNS bool as $$
DECLARE
  c_limits_category_id integer;
  c_voter_count integer;
  overlimit bool;
BEGIN
  --get contribution limits
  c_limits_category_id := (select limit_category
                           from fund f
                                  join candidacy c on f.committee_id = c.committee_id
                                  join jurisdiction_office jo on c.jurisdiction_id = jo.jurisdiction_id and c.office_code = jo.offcode
                           where f.fund_id = p_fund_id);

  --Use the greatest imported year less than election year
  c_voter_count := (select votercount
                    from fund f
                           join candidacy c on f.committee_id = c.committee_id
                           join jurisdiction j on c.jurisdiction_id = j.jurisdiction_id
                           join votercount v
                                on cast(v.year as text) < f.election_code and v.jurisdiction_id = c.jurisdiction_id and
                                   v.county = j.county
                    where fund_id = p_fund_id
                    order by v.year desc
                    limit 1);


  overlimit := (select case
           when c.pagg > c.amount then true
           when c.gagg > c.amount then true
           else false end
  from (select
               aggs.pagg,
               aggs.gagg,
               case
                 when cl.agg_by_voter then
                   cl.amount * c_voter_count
                 else cl.amount end as amount
        from private.contacts co
               join private.trankeygen t on co.trankeygen_id = t.trankeygen_id
               left join contribution_limit cl on co.type = cl.type_code and cl.limit_category = c_limits_category_id
               left join private.cf_report_get_contact_aggregates(co.trankeygen_id, now()) aggs
                         on aggs.contid = t.trankeygen_id
        where t.fund_id = p_fund_id
          and co.type != 'HDN'
          and co.type != 'FI'
          and co.type != 'CAN'
          and co.trankeygen_id = p_contact_id) c);

  return overlimit;

  end;


$$ LANGUAGE plpgsql;