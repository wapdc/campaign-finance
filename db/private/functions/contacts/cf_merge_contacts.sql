DROP FUNCTION IF EXISTS private.cf_merge_contacts(p_fund_id INTEGER, contact_id INTEGER, duplicate_id INTEGER);
CREATE OR REPLACE FUNCTION private.cf_merge_contacts(p_fund_id INTEGER, contact_id INTEGER, duplicate_id INTEGER) RETURNS json AS $$
DECLARE
  j_contact json;
  v_fund_id integer;

BEGIN
  select fund_id into v_fund_id
    from private.trankeygen where trankeygen_id = contact_id;

  if v_fund_id is null or v_fund_id <> p_fund_id THEN
      RAISE 'Invalid transaction to merge!';
  end if;

  select fund_id into v_fund_id
    from private.trankeygen where trankeygen_id = duplicate_id;

  if v_fund_id is null or v_fund_id <> p_fund_id THEN
      RAISE 'Invalid transaction to merge!';

  end if;


  update private.expenditureevents
  set contid = contact_id
  where contid = duplicate_id;

  update private.receipts
  set contid = contact_id
  where contid = duplicate_id;

  update private.couplecontacts
  set contact1_id = contact_id
  where contact1_id = duplicate_id;

  update private.couplecontacts
  set contact2_id = contact_id
  where contact2_id = duplicate_id;

  update private.registeredgroupcontacts rgc
  set contid= contact_id
  where contid = duplicate_id
    and not exists(select 1 from private.registeredgroupcontacts where gconid = rgc.gconid and rgc.contid = duplicate_id);

  delete from private.trankeygen where trankeygen_id = duplicate_id;

  SELECT row_to_json(r) INTO j_contact FROM (select * from private.vcontacts c
                                           WHERE trankeygen_id = contact_id and fund_id = p_fund_id) r;

  RETURN j_contact;
END
$$ LANGUAGE plpgsql