CREATE OR REPLACE FUNCTION private.cf_update_aggregates(p_contact_id integer)
    RETURNS VOID as  $$
DECLARE
    v_contact_id int;
    v_pagg numeric;
    v_gagg numeric;
    v_contact_type text;
    v_contact_1_id int;
    v_contact_2_id int;
BEGIN

    SELECT vc.type, vc.contact1_id, vc.contact2_id INTO v_contact_type, v_contact_1_id, v_contact_2_id FROM private.vcontacts vc WHERE vc.trankeygen_id = p_contact_id;

    SELECT a.contid, coalesce(a.pagg, 0), coalesce(a.gagg,0)
    INTO v_contact_id, v_pagg, v_gagg
    FROM (select * from private.cf_report_get_contact_aggregates(p_contact_id, '2999-12-31')) a;

    update private.contacts set pagg = v_pagg, gagg = v_gagg where trankeygen_id = v_contact_id;

    IF v_contact_type = 'CPL' THEN
        --first person in couple
        SELECT a.contid, coalesce(a.pagg, 0), coalesce(a.gagg,0)
        INTO v_contact_id, v_pagg, v_gagg
        FROM (select * from private.cf_report_get_contact_aggregates(v_contact_1_id, '2999-12-31')) a;

        update private.contacts set pagg = v_pagg, gagg = v_gagg where trankeygen_id = v_contact_id;

        --second person in couple
        SELECT a.contid, coalesce(a.pagg, 0), coalesce(a.gagg,0)
        INTO v_contact_id, v_pagg, v_gagg
        FROM (select * from private.cf_report_get_contact_aggregates(v_contact_2_id, '2999-12-31')) a;

        update private.contacts set pagg = v_pagg, gagg = v_gagg where trankeygen_id = v_contact_id;

    end if;
END
$$ LANGUAGE plpgsql;