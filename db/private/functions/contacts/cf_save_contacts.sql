drop function if exists private.cf_save_contacts(p_fund_id INTEGER, json_data json);
-- this delete is old and can be removed when all devs have updated their core environments.
drop function if exists private.cf_save_contacts(p_fund_id INTEGER, json_data json, mode varchar);
CREATE OR REPLACE FUNCTION private.cf_save_contacts(p_fund_id INTEGER, json_data json)
  RETURNS json as
$$
DECLARE
    c json;
    v_contacts jsonb;
    v_contact_id int;
    v_existing_contact_id int;
    v_address_similarity numeric;
    v_name_similarity numeric;
    v_contact_json jsonb;
    v_get_json jsonb;
    v_insert_duplicate boolean := false;
    v_message_text text;
    v_column text;
    v_errors jsonb;
BEGIN
    v_contacts := jsonb_build_array();
    v_errors = jsonb_build_array();


    -- Loop over array processing each contact
    FOR c in (select * from json_array_elements(json_data))
    LOOP
        v_contact_json = c::jsonb;
        v_insert_duplicate := false;

        -- See if we need to insert contact id
        select trankeygen_id into v_existing_contact_id
        from private.contacts cc
          where cc.trankeygen_id = cast(c->>'duplicate_id' as integer);

        if v_existing_contact_id is not null then
            v_insert_duplicate := true;
        end if;

        begin
        v_message_text := null;
        v_contact_id = private.cf_save_contact(p_fund_id, v_contact_json::json);
        EXCEPTION WHEN OTHERS THEN
            GET STACKED DIAGNOSTICS v_message_text := MESSAGE_TEXT,
               v_column := column_name ;
            v_contact_id := null;
        end;

        IF v_contact_id is not null then
            IF v_insert_duplicate THEN
                INSERT INTO private.contact_duplicates(contact_id, duplicate_id, name_similarity, address_similarity)
                VALUES (v_existing_contact_id, v_contact_id, v_name_similarity, v_address_similarity) ON CONFLICT DO NOTHING;
            end if;
            -- returns an contact id
            v_get_json := private.cf_get_contact(p_fund_id, v_contact_id)::jsonb;
            v_contacts := v_contacts || v_get_json ;

        else
            if v_column is not null then
                v_message_text := 'Missing or invalid data in ' || v_column;
            end if;
            v_contact_json := jsonb_set(v_contact_json, '{error}', to_jsonb(coalesce(v_message_text,'Error saving contact')), true);
            v_contacts = v_contacts || v_contact_json;
        end if;

        -- build an array to put the contacts in
    END LOOP;
        -- return a list of contacts as an json array
    return jsonb_build_object('contacts', v_contacts::json, 'errors', v_errors);
END
$$  LANGUAGE plpgsql volatile;

