DROP FUNCTION IF EXISTS private.cf_get_contact(p_fund_id INTEGER, p_trankeygen_id INTEGER);
CREATE OR REPLACE FUNCTION private.cf_get_contact(p_fund_id INTEGER, p_trankeygen_id INTEGER) RETURNS json AS $$
DECLARE
  j_contact json;

BEGIN

  SELECT row_to_json(r) INTO j_contact FROM (select * from private.vcontacts c
                                           WHERE trankeygen_id = p_trankeygen_id and fund_id = p_fund_id) r;

  RETURN j_contact;
END
$$ LANGUAGE plpgsql STABLE