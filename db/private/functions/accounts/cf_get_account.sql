drop function if exists  private.cf_get_credit_card(p_fund_id integer, p_trankeygen_id integer);
CREATE OR REPLACE FUNCTION private.cf_get_account(p_fund_id INTEGER, p_trankeygen_id integer) returns JSON as
$$
DECLARE
    c_json json;
BEGIN
    SELECT row_to_json(con)
    into c_json
    FROM (
         SELECT
             a.trankeygen_id,
             a.acctnum,
             a.contid,
             a.style,
             t.orca_id,
             c.name,
             c.street,
             c.city,
             c.state,
             c.zip,
             c.memo,
             c.trankeygen_id

         from private.accounts a
         join private.trankeygen t on t.trankeygen_id = a.trankeygen_id
         join private.contacts c on a.contid = c.trankeygen_id
         join private.trankeygen tc on tc.trankeygen_id=c.trankeygen_id
         WHERE a.trankeygen_id = p_trankeygen_id and t.fund_id = p_fund_id
         ) con;

    return c_json;
END
$$ language plpgsql;
