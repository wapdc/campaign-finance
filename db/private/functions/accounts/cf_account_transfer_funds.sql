CREATE OR REPLACE FUNCTION private.cf_account_transfer_funds(p_fund_id integer, p_did integer, p_cid integer, p_amount numeric(16,2))
returns void as
$$
 declare v_invalid_funds integer;
 begin
   SELECT count(1) into v_invalid_funds from private.accounts a left join private.trankeygen t on a.trankeygen_id=t.trankeygen_id
      and t.fund_id=p_fund_id
      where a.trankeygen_id in (p_did, p_cid) and t.trankeygen_id is null;

   if v_invalid_funds > 0 then
      RAISE 'Invalid accounting: did - %, cid - %, % invalid', p_did, p_cid, v_invalid_funds;
   end if;

   UPDATE private.accounts set total = total - p_amount where trankeygen_id = p_did;
   UPDATE private.accounts set total = total + p_amount where trankeygen_id = p_cid;
  end;
$$ language plpgsql;