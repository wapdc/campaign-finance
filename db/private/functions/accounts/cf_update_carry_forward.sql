--drop function private.cf_update_carry_forward(target_fund_id int, p_amount numeric, p_memo varchar, p_bank_trankeygen_id int);
create or replace function private.cf_update_carry_forward(target_fund_id int, p_amount numeric, p_memo varchar, p_bank_trankeygen_id int)
  returns json
  language plpgsql
as $$
declare
  original_carry_forward_amount numeric;
  credit_id int;
  debit_id int;
  v_trankeygen_id int;
begin

  IF target_fund_id is distinct from (select fund_id from private.vaccounts where trankeygen_id = p_bank_trankeygen_id) THEN
    raise  EXCEPTION 'Invalid account for fund';
  end if;

  select r.amount, r.cid, r.did, r.trankeygen_id into original_carry_forward_amount, credit_id, debit_id, v_trankeygen_id
  from private.receipts r
    join private.trankeygen t on r.trankeygen_id = t.trankeygen_id where t.fund_id = target_fund_id and r.type = 22
    and r.carryforward = 1 and cid = p_bank_trankeygen_id;

  if v_trankeygen_id is null then

    debit_id := (select a.trankeygen_id from private.accounts a join private.trankeygen t on a.trankeygen_id = t.trankeygen_id
      where t.fund_id = target_fund_id and a.acctnum = 4600);

    credit_id := (select a.trankeygen_id from private.accounts a join private.trankeygen t on a.trankeygen_id = t.trankeygen_id
      where t.fund_id = target_fund_id and a.trankeygen_id = p_bank_trankeygen_id);

    v_trankeygen_id := private.cf_ensure_trankeygen(target_fund_id, null);

    insert into private.receipts (trankeygen_id, type, nettype, cid, did, amount, date_, carryforward, memo)
    values (v_trankeygen_id, 22, 1, credit_id, debit_id, p_amount, now(), 1, p_memo);

    original_carry_forward_amount := 0;

  else

    update private.receipts rec set amount = p_amount, memo = p_memo
        where rec.trankeygen_id = v_trankeygen_id;

  end if;

  update private.accounts set total = (total - (p_amount - original_carry_forward_amount)) where trankeygen_id = debit_id;

  update private.accounts set total = (total + (p_amount - original_carry_forward_amount)) where trankeygen_id = credit_id;

  return (select row_to_json(r) from private.vreceipts r where r.trankeygen_id = v_trankeygen_id);

end;
$$;