CREATE OR REPLACE FUNCTION private.cf_account_adjust(
  p_fund_id INTEGER,
  p_old_did INTEGER,
  p_old_cid INTEGER,
  p_old_amount NUMERIC,
  p_new_did INTEGER,
  p_new_cid INTEGER,
  p_new_amount NUMERIC) returns void AS
$$
  begin
      -- When changing accounts back out the old transaction and put in the new one.
      if p_old_cid is null or p_old_did is null or p_old_amount is null then
        PERFORM private.cf_account_transfer_funds(p_fund_id, p_new_did, p_new_cid, p_new_amount);
      elseif p_old_cid <> p_new_cid or p_old_did <> p_new_did THEN
        PERFORM private.cf_account_transfer_funds(p_fund_id, p_old_cid, p_old_did, p_old_amount);
        PERFORM private.cf_account_transfer_funds(p_fund_id, p_new_did, p_new_cid, p_new_amount);

      -- Same accounts means that we can just adjust the difference.
      else
        PERFORM private.cf_account_transfer_funds(p_fund_id, p_new_did, p_new_cid, p_new_amount - p_old_amount);
      end if;
  end;
$$ language plpgsql;