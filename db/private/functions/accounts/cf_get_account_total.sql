CREATE OR REPLACE FUNCTION private.cf_get_account_total(p_fund_id integer, p_account_id INTEGER, p_as_of date default null) RETURNS numeric(16,2) as $$
  select cast(COALESCE(sum(amount),0) as numeric(16,2)) as result FROM (
    select
      amount *
          case when r.cid=r.did then cast(0.0 as numeric(16,2)) when r.cid=p_account_id then cast(1.00 as numeric(16,2)) else cast(-1.00 as numeric(16,2)) end
      as amount
      from private.vreceipts r
        join private.vaccounts a on a.fund_id = p_fund_id and a.trankeygen_id = p_account_id
      where r.fund_id = p_fund_id
        and r.did = p_account_id or r.cid = p_account_id
        and (p_as_of is null or r.date_ <= p_as_of)
  ) v;
$$ language sql;