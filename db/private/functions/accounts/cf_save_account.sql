DROP function if exists private.cf_save_credit_card(p_fund_id INTEGER, json_data JSON);
CREATE OR REPLACE FUNCTION private.cf_save_account(p_fund_id INTEGER, json_data JSON)
    RETURNS int as
$$
DECLARE
    v_contact_id int;
    v_ie_account_id int;
    v_acctnum int;
BEGIN

    if json_data ->> 'name' is null then
        RAISE 'Credit card name required';
    end if;
    v_acctnum = cast(json_data ->> 'acctnum' as integer);

    -- IF trankeygen_id is passed this is an update, ELSE an insert
    IF cast(json_data ->> 'trankeygen_id' as integer) is not null then

      select trankeygen_id into v_contact_id from
      private.trankeygen where fund_id = p_fund_id
      and trankeygen_id=cast(json_data->>'trankeygen_id' as integer);

      if v_acctnum = 2600 then
          select trankeygen_id into v_ie_account_id from private.vaccounts where pid = v_contact_id
            and acctnum = 5110;
      end if;

      if v_contact_id is null then
          RAISE 'Invalid fund account';
      end if;

      -- update contacts
      update private.contacts set
          name = json_data ->> 'name',
          street = json_data ->> 'street',
          city = json_data ->> 'city',
          state = json_data ->> 'state',
          zip = json_data ->>'zip',
          memo = json_data ->> 'memo'
          where trankeygen_id = cast(json_data ->> 'trankeygen_id' as integer);
    ELSE
        -- save contact
        v_contact_id := private.cf_ensure_trankeygen(p_fund_id, null);
        insert into private.contacts (trankeygen_id, type, name, street, city, state, zip, memo)
        values (v_contact_id,
                'HDN',
                json_data ->> 'name',
                json_data ->> 'street',
                json_data ->> 'city',
                json_data ->> 'state',
                json_data ->> 'zip',
                json_data ->> 'memo')
        ON CONFLICT (trankeygen_id) DO UPDATE
            set name=excluded.name,
                street=excluded.street,
                city = excluded.city,
                state = excluded.state,
                zip = excluded.zip;


        -- create credit card account entry
        insert into private.accounts (trankeygen_id, acctnum, style, contid)
        values (v_contact_id,
                v_acctnum,
                2,
                v_contact_id)
        on conflict do nothing;

        -- create interest expense account entry
        if v_acctnum = 2600 then
            v_ie_account_id := private.cf_ensure_trankeygen(p_fund_id, v_ie_account_id);
            insert into private.accounts (trankeygen_id, acctnum, pid, style, contid)
            values (v_ie_account_id,
                    5110,
                    v_contact_id,
                    3,
                    v_contact_id)
            on conflict do nothing;

            --set pid of 5110 account
            update private.trankeygen set pid = v_contact_id where trankeygen_id = v_ie_account_id and fund_id = p_fund_id;
        end if;
    END IF;

    return v_contact_id;
END
$$ LANGUAGE plpgsql;