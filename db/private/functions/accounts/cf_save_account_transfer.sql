--DROP function private.cf_save_contribution_refund(p_fund_id INTEGER, json_data JSON);
CREATE OR REPLACE FUNCTION private.cf_save_account_transfer(p_fund_id INTEGER, json_data JSON)
  RETURNS int as
$$
DECLARE
  v_receipt_id int;
  v_did int;
  v_cid int;
  v_elekey int;
  v_contact int;
  v_old_amount numeric(16,2);
  v_old_cid int;
  v_old_did int;
  v_contributing_acct_num smallint;
  v_receiving_acct_num smallint;
BEGIN

  select trankeygen_id, cid, did, elekey, contid into v_receipt_id, v_cid, v_did, v_elekey, v_contact FROM private.vreceipts
  where trankeygen_id = cast(json_data ->> 'trankeygen_id' as integer) and fund_id = p_fund_id;

  IF (v_receipt_id is null)
  THEN
    v_receipt_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));
    select a.trankeygen_id into v_cid from private.accounts a join private.trankeygen t on a.trankeygen_id = t.trankeygen_id where a.trankeygen_id = cast(json_data ->> 'cid' as integer) and fund_id = p_fund_id;
    select a.trankeygen_id into v_did from private.accounts a join private.trankeygen t on a.trankeygen_id = t.trankeygen_id where a.trankeygen_id = cast(json_data ->> 'did' as integer) and fund_id = p_fund_id;
  end if;

  select acctnum into v_contributing_acct_num from private.accounts where trankeygen_id = v_did;
  select acctnum into v_receiving_acct_num from private.accounts where trankeygen_id = v_cid;

  --contributing account should exist and either be a bank account, petty cash, or an account with acctnum between 5000 and 9999
  IF ((v_did is null) or
    (v_contributing_acct_num != 1000 and v_contributing_acct_num != 1800 and v_contributing_acct_num not between 5000 and 9999))
  THEN
    raise  EXCEPTION 'Invalid contributing account';
  end if;

  -- Receiving account should exist. If contributing account is either a bank account or petty cash, then receiving account should also be. Otherwise acctnum should be between 5000 and 9999
  IF (v_cid is null or
    ((v_contributing_acct_num = 1000 or v_contributing_acct_num = 1800) and not (v_receiving_acct_num = 1000 or v_receiving_acct_num = 1800)) or
    (v_contributing_acct_num between 5000 and 9999 and v_receiving_acct_num not between 5000 and 9999))
  THEN
      raise exception 'Invalid receiving account';
  end if;


  SELECT cid, did, amount INTO v_old_cid, v_old_did, v_old_amount FROM private.receipts r where trankeygen_id=v_receipt_id;

  INSERT into private.receipts(trankeygen_id, type, did, cid, amount, date_, memo)
  values (v_receipt_id,
          42,
          cast(json_data->>'did' as integer),
          cast(json_data->>'cid' as integer),
          cast(json_data->>'amount' as numeric),
          to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
          json_data->>'memo'
          )
  on conflict (trankeygen_id) do update
    set
      did          = excluded.did,
      cid          = excluded.cid,
      amount       = excluded.amount,
      date_        = excluded.date_,
      memo         = excluded.memo;

  perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, v_cid,cast(json_data ->> 'amount' as numeric));
  RETURN v_receipt_id;

END
$$ LANGUAGE plpgsql;