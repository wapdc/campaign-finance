DROP FUNCTION IF EXISTS private.cf_get_advertising_data(p_fund_id integer, p_ad_id integer);
CREATE or REPLACE FUNCTION private.cf_get_advertising_data(p_fund_id integer, p_ad_id integer) RETURNS json as $$

DECLARE
j_advertisement json;
j_targets json;
j_contributions json;
j_info json;

BEGIN

-- targets
SELECT json_agg(r) INTO j_targets FROM (
  select coalesce(fo.offtitle, fo2.offtitle) as office_title,
    coalesce(j.name, j2.name) as jurisdiction_name,
    t.*
  from private.ad_target t
    join private.ad on ad.ad_id = t.ad_id
    left join candidacy c on t.candidacy_id = c.candidacy_id
    left join foffice fo on c.office_code = fo.offcode
    left join foffice fo2 on t.office_code = fo2.offcode
    left join jurisdiction j on c.jurisdiction_id = j.jurisdiction_id
    left join jurisdiction j2 on t.jurisdiction_id = j2.jurisdiction_id
  where t.ad_id = p_ad_id and ad.fund_id = p_fund_id) r;

-- contributions
select json_agg(row_to_json(j))
into j_contributions
from (
  select ac.contribution_id, r.amount, r.date_, c.name
  from private.ad_contribution ac
    join private.ad
      on ad.ad_id = ac.ad_id
    left join private.receipts r
      on r.trankeygen_id = ac.contribution_id
    left join private.contacts c
      on c.trankeygen_id = r.contid
  where ac.ad_id = p_ad_id
    and ad.fund_id = p_fund_id
) j;

SELECT row_to_json(j)
INTO j_advertisement
FROM (select * from private.ad
      WHERE ad_id = p_ad_id AND fund_id = p_fund_id) j;

SELECT row_to_json(j)
FROM (SELECT j_advertisement as "ad", j_targets as targets, j_contributions as contributions) j INTO j_info;

RETURN j_info;
END

$$ LANGUAGE plpgsql