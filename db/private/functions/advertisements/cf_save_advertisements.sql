DROP FUNCTION private.cf_save_advertisements(p_fund_id integer, json_data json);
CREATE OR REPLACE FUNCTION private.cf_save_advertisements(p_fund_id integer, json_data json)
    RETURNS integer as
$$
DECLARE
  ret_ad_id integer;
  target json;
  contribution_id_field int;
BEGIN
  IF json_data->'advertisement'->'ad'->'ad_id' IS NOT NULL THEN
    UPDATE private.ad
    SET first_run_date = to_date(json_data->'advertisement'->'ad'->>'first_run_date', 'YYYY-MM-DD'),
      last_run_date = to_date(json_data->'advertisement'->'ad'->>'last_run_date', 'YYYY-MM-DD'),
      description = json_data->'advertisement'->'ad'->>'description',
      outlets = json_data->'advertisement'->'ad'->>'outlets'
    WHERE ad_id = CAST(json_data->'advertisement'->'ad'->>'ad_id' AS integer)
      AND fund_id = p_fund_id;
    ret_ad_id = CAST(json_data->'advertisement'->'ad'->>'ad_id' as integer);
  ELSE
    INSERT INTO private.ad (fund_id, first_run_date, last_run_date, description, outlets)
    values (
      p_fund_id,
      to_date(json_data->'advertisement'->'ad'->>'first_run_date', 'YYYY-MM-DD'),
      to_date(json_data->'advertisement'->'ad'->>'last_run_date', 'YYYY-MM-DD'),
      json_data->'advertisement'->'ad'->>'description',
      json_data->'advertisement'->'ad'->>'outlets'
    )
    RETURNING ad_id INTO ret_ad_id;
  END IF;

  -- targets
  if (json_data->'advertisement'->>'targets' is not null) then
    --remove ad targets that aren't in the json targets array (before doing ad target inserts and updates)
    delete from private.ad_target
    where ad_target_id in (
      select t.ad_target_id
      from private.ad_target t
        left join private.ad a on t.ad_id = a.ad_id
        left join (
          select j ->> 'ad_target_id' as ad_target_id
          from json_array_elements(json_data->'advertisement'->'targets') j
        ) je
          on t.ad_target_id = cast(je.ad_target_id as int)
      where t.ad_id = ret_ad_id
        and a.fund_id = p_fund_id
        and je.ad_target_id is null
    );
    -- loop through targets
    for target in (select j.* from json_array_elements(json_data->'advertisement'->'targets') j)
    loop
      if not exists (
        select 1
        from private.ad_target
        where ad_target_id = cast(target ->> 'ad_target_id' as integer)
      ) then
        insert into private.ad_target (ad_id, candidacy_id, ballot_name, target_type, stance, jurisdiction_id, office_code, proposal_id, percent)
        values (
          ret_ad_id,
          cast(target ->> 'candidacy_id' as integer),
          target ->> 'ballot_name',
          target->>'target_type',
          target ->> 'stance',
          cast(target ->> 'jurisdiction_id' as integer),
          target ->> 'office_code',
          cast(target ->> 'proposal_id' as integer),
          cast(target ->> 'percent' as numeric)
        );
      else
        update private.ad_target
        set candidacy_id=cast(target ->> 'candidacy_id' as integer),
          ballot_name=target ->> 'ballot_name',
          target_type = target ->> 'target_type',
          stance = target ->> 'stance',
          jurisdiction_id=cast(target ->> 'jurisdiction_id' as integer),
          office_code = target ->> 'office_code',
          proposal_id=cast(target ->> 'proposal_id' as integer),
          percent=cast(target ->> 'percent' as numeric)
        where ad_target_id = cast(target ->> 'ad_target_id' as integer)
          and ad_id = ret_ad_id;
      end if;
    end loop;
  else
    --delete all ad targets if targets array is null
    delete from private.ad_target t
    using private.ad a
    where t.ad_id = a.ad_id
      and t.ad_id = ret_ad_id
      and a.fund_id = p_fund_id;
  end if;

  -- contributions
  if (json_data->'advertisement'->>'contributions' is not null) then
    --remove ad contributions that aren't in the json
    delete from private.ad_contribution
    where ad_id = ret_ad_id
      and contribution_id in (
        select ac.contribution_id
        from private.ad_contribution ac
        left join private.ad a
          on ac.ad_id = a.ad_id
        left join (
          select j ->> 'contribution_id' as contribution_id
          from json_array_elements(json_data->'advertisement'->'contributions') j
        ) je
          on ac.contribution_id = cast(je.contribution_id as int)
        where ac.ad_id = ret_ad_id
          and a.fund_id = p_fund_id
          and je.contribution_id is null
      );
    -- loop through contributions
    for contribution_id_field in (
      select cast(j->>'contribution_id' as int)
      from json_array_elements(json_data->'advertisement'->'contributions') j
    )
    loop
      insert into private.ad_contribution (ad_id, contribution_id)
      values (ret_ad_id, contribution_id_field)
      on conflict do nothing;
    end loop;
  else
    --delete all contributions related to this ad
    delete from private.ad_contribution ac
    using private.ad a
    where ac.ad_id = a.ad_id
      and ac.ad_id = ret_ad_id
      and a.fund_id = p_fund_id;
  end if;

  RETURN ret_ad_id;
END
$$ LANGUAGE plpgsql;