
/**
  Function to produce the the json equivalent of the contacts section of the C3 or C4 report for
  inclusion in a submission.

  The function requires that all the contact ids and the parameters that control address/employer information have been
  pre-populated into the temporary table "#temp_contact_table". The function assumes that the temp
  table already exists and has the form:
    create temporary table "#temp_contact_table"
      (contact_key int, force_cont_address boolean, test_emp_address boolean, force_cont_aggs boolean)
    on commit drop;
  @param p_up_to_date the date to use to calculate primary and general aggregate totals. For example, '2021-10-07' will
  aggregate all transactions up to and including that date.
 */

create or replace function private.cf_report_get_contacts(p_up_to_date date)
returns json as
$$
declare
    contacts json;
    address_threshold numeric(16,2);
    employer_threshold numeric(16,2);
begin
    address_threshold := (select amount from thresholds where property = 'itemized_contributions');
    employer_threshold := (select amount from thresholds where property = 'report_occupation');
    select json_agg(v.* order by v.contact_key )
    into contacts from (
      select t.contact_key,
             coalesce(ca.gagg, 0) as aggregate_general,
             coalesce(ca.pagg, 0) as aggregate_primary,
             case
               WHEN c.type IN ('BUS','V') THEN 'B'
               WHEN C.type IN ('TRB','PAC') THEN 'C'
               WHEN c.type IN ('FIN', 'FI') THEN 'F'
               WHEN c.type IN ('IND', 'CPL') THEN 'I'
               WHEN c.type = 'CAU' THEN  'L'
               WHEN c.type = 'OG' THEN 'O'
               WHEN c.type in ('SP', 'CP', 'LDP') then 'P'
               WHEN c.type = 'CAN' THEN 'S'
               WHEN c.type = 'MP' THEN 'T'
               WHEN c.type = 'UN' THEN 'U'
               ELSE 'I'
             end as contributor_type,
             i.prefix as prefix,
             i.firstname as first,
             i.middleinitial as middle,
             i.lastname as last,
             i.suffix as suffix,
             case
                 when (trim(i.firstname) is not null and trim(i.firstname) <> '')
                     and (trim(i.middleinitial) is not null and trim(i.middleinitial) <> '')
                     and (trim(i.lastname) is not null and trim(i.lastname) <> '')
                     and (trim(i.suffix) is not null and trim(i.suffix) <> '')
                     then concat(i.lastname , ' ', i.firstname, ' ', i.middleinitial, ' ', i.suffix)
                 when (trim(i.firstname) is not null and trim(i.firstname) <> '')
                     and (trim(i.middleinitial) is not null and trim(i.middleinitial) <> '')
                     and (trim(i.lastname) is not null and trim(i.lastname) <> '')
                     then concat(i.lastname , ' ', i.firstname, ' ', i.middleinitial)
                 when (trim(i.firstname) is not null and trim(i.firstname) <> '')
                     and (trim(i.lastname) is not null and trim(i.lastname) <> '')
                     then concat(i.lastname , ' ', i.firstname)
                 else c.name
                 end as name,
             case when force_cont_address or total > address_threshold then street end as address,
             case when force_cont_address or total > address_threshold then city end as city,
             case when force_cont_address or total > address_threshold then state end as state,
             case when force_cont_address or total > address_threshold then zip end as postcode,
             case when test_emp_address and total > employer_threshold then
             json_build_object(
                     'name', employername,
                     'city', employercity,
                     'state', employerstate,
                     'occupation', occupation
                 ) end as employer
      from private.contacts c
      join (
          select
                 contact_key,
                 bool_or(force_cont_address) as force_cont_address,
                 bool_or(test_emp_address) as test_emp_address,
                 bool_or(force_cont_aggs) as force_cont_aggs
          from "#temp_contact_table" group by contact_key
          ) t on c.trankeygen_id = cast(t.contact_key as integer)
      left join private.cf_report_get_contact_aggregates(c.trankeygen_id, p_up_to_date) ca on 1 = 1
      left join private.individualcontacts i on i.trankeygen_id = c.trankeygen_id
    ) v;
    return contacts;
end
$$ language plpgsql stable