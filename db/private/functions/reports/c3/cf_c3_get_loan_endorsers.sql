CREATE OR REPLACE FUNCTION private.cf_c3_get_loan_endorsers(p_loan_id INTEGER) RETURNS JSON AS
$$
DECLARE
    loan_endorsers json;

BEGIN
    SELECT json_agg(e ORDER BY e.contact_key)
    INTO loan_endorsers
    FROM (SELECT cast(r.contId as text) as contact_key,
                 case -- elekey is returning as integer, converting to either primary, general or null
                     when r.elekey = 0 then 'P'
                     when r.elekey = 1 then 'G'
                     else 'N'
                     end                as election,
                 r.amount               as liable_amount
          from private.vreceipts r
                   join private.cf_report_add_contact(r.contid, true, true, false) on 1 = 1
          where r.pid = p_loan_id
            and r.transaction_type = 'Loan Endorsement') e;

    RETURN loan_endorsers;
END;
$$
    LANGUAGE plpgsql VOLATILE