/**
  This function produces the both the small contributions cover section and the array if itemized contributions for
  inclusion in a C3 submission. The function was written to handle both so that all contributions are evaluated and
  accounted for as either small or itemized to ensure that no contributions are possibly missed by logic errors

  When evaluating the aggregate totals, if the contribution is from a couple then it needs
  to consider the portion of the contribution that came from each member of the couple and look at the total of
  individual and couple contributions. The portion from the couple contribution is 50% of the total. This is handled by
  joining the receipts to the couple contacts, producing 2 contribution records containing half the amount for each
  member of the couple contact. On the actual report record, there are no couple contributions, they are always reported
  as individuals.

  @param p_depositevent_trankeygen_id the trankeygen_id of the deposit event
  @return json of small contributions
 */

create or replace function private.cf_c3_get_itemized_and_small_contributions(p_depositevent_trankeygen_id int)
    returns json AS
$$

declare
    v_fund_id      int;
    v_deptkey      bigint;
    v_itemize      boolean;
    v_small_limit  numeric(16,2);
    v_small_count  int            := 0;
    v_small_amount decimal(16, 2) := 0;
    v_small_date   date;
    v_date         date;
    v_result       jsonb;
    c_contributions cursor (cv_debt_key bigint, cv_fund_id int) for
        select coalesce(cc.contact_id, r.contid) as contact_key,
               case
                   -- The math here is important that it's (amount - amount * 0.5) for contact 2 because we have amounts
                   -- that are not equally divisible by 2. A couple contact of 49.99 will be 25.00 and 24.99
                   when cc.cnum = 1 then (r.amount * 0.5)::decimal(10, 2)
                   when cc.cnum = 2 then r.amount - (r.amount * 0.5)::decimal(10, 2)
                   else r.amount
                   end as amount,
               case -- elekey is returning as integer, converting to either primary, general or null
                   when r.elekey = 0 then 'P'
                   when r.elekey = 1 then 'G'
                   else 'N'
                   end as election,
               r.itemized,
               r.date_
        from private.vreceipts r
                 join private.receipt_type tc
                      on r.type = tc.type
                          and tc.code in
                              ('Low Cost Fundraiser',
                               'Monetary Contribution',
                               'Monetary Pledge',
                               'Monetary Group Contribution Detail',
                               'Fundraiser Contribution')
                 left join lateral (
            select *
            from private.vcouplecontacts cc
            where cc.couple_contact_id = r.contid
            ) cc on true
        where r.deptkey = cv_debt_key
          and r.fund_id = cv_fund_id
        order by date_, coalesce(cc.contact_id, r.contid); --The contact id in the ordering just helps for testing
begin
    v_small_limit := (select amount from thresholds where property = 'itemized_contributions');
    v_result := jsonb_build_object(
            'itemized', array []::text[],
            'small', jsonb_build_object(
                    'amount', 'null'::jsonb,
                    'count', 'null'::jsonb,
                    'date', 'null'::jsonb
                )
        );

    select deptkey, fund_id, date_
    into v_deptkey, v_fund_id, v_date
    from private.vdepositevents
    where trankeygen_id = p_depositevent_trankeygen_id;

    for v_record in c_contributions (v_deptkey, v_fund_id)
        loop
            v_itemize := true;
            if v_record.amount <= v_small_limit and v_record.itemized = 0 then
                -- Check aggregate totals and determine if this requires itemization. The reason this is not part of the
                -- "if" above (if A and B and C) is that the aggregate totals is expensive and I could not find
                -- documentation stating that postgres would evaluate the conditions in the order that they are written
                -- and skip the expensive function call if one of the preceding statements was false.
                if private.cf_report_get_contact_aggregate_total(
                           v_record.contact_key, v_date) <= v_small_limit then
                    v_small_count := v_small_count + 1;
                    v_small_amount := v_small_amount + v_record.amount;
                    if v_small_count = 1 then
                        v_small_date = v_record.date_;
                    elseif v_small_date <> v_record.date_ then
                        v_small_date := null;
                    end if;
                    v_itemize := false;
                end if;
            end if;
            if v_itemize then
                perform private.cf_report_add_contact(v_record.contact_key,
                                                      true, true, false);
                v_result := jsonb_insert(
                        v_result,
                        '{itemized, -1}',
                        jsonb_build_object(
                                'amount', v_record.amount,
                                'date', v_record.date_,
                                'election', v_record.election,
                                'contact_key', cast(v_record.contact_key as text)
                            ),
                        true);
            end if;
        end loop;

    if v_small_count > 0 then
        v_result := jsonb_set(
                v_result,
                '{small, amount}',
                to_jsonb(v_small_amount),
                true
            );
        v_result := jsonb_set(
                v_result,
                '{small, count}',
                to_jsonb(v_small_count),
                true
            );
        if v_small_date is not null then
            v_result := jsonb_set(
                    v_result,
                    '{small, date}',
                    to_jsonb(v_small_date),
                    true
                );
        end if;
    end if;

    return v_result;
end
$$ language plpgsql volatile