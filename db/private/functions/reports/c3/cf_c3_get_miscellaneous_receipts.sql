
/**
  Function to produce the json equivalent of the miscellaneous receipts section of the C3 deposit report for
  inclusion in a C3 submission, generated from the private database for a fund and deposit key which can be determined
  from the deposit event trankeygen_id.

  It is based on the function getMiscellaneousReceipts in orca-app/src/main/java/orca/reports/C3.java

  It has an additional side effect of populating the temporary table passed as a parameter with the contact id and
  options necessary to build out the contacts that are referenced in the miscellaneous receipt records

  @param p_depositevent_trankeygen_id the trankeygen_id of a specific deposit event
  @return json for example:
        "misc": [
            { "contact_key": "C19930682", "amount": 2396.25, "date": "2021-09-30", "description": "CONTRIBUTIONS" },
            { "contact_key": "C20658530", "amount": 2452.5, "date": "2021-09-30", "description": "CONTRIBUTIONS" }
        ]
 */

create or replace function private.cf_c3_get_miscellaneous_receipts(p_depositevent_trankeygen_id int) returns json as
$$
declare
    j_miscellaneous_receipts json;

begin
    select json_agg(c ORDER BY c.date, c.contact_key)
    into j_miscellaneous_receipts
    from (select cast(r.contId as text) as contact_key, r.amount,  r.date_ as date, r.description,
                 case when c.code='Personal Funds' then true end personal_funds
          from private.depositevents d
                   join private.trankeygen tdep on d.trankeygen_id = tdep.trankeygen_id
                   join private.trankeygen trec on tdep.fund_id = trec.fund_id
                   join private.receipts r on trec.trankeygen_id = r.trankeygen_id and d.deptkey = r.deptkey
                   join private.receipt_type c
                        on r.type = c.type
                            and c.code in
                                ('Refund From Vendor', 'Other Receipt', 'Bank Interest', 'Personal Funds')
    join private.cf_report_add_contact(r.contid, true, true, false) on 1 = 1
          where d.trankeygen_id = p_depositevent_trankeygen_id) c;
    return j_miscellaneous_receipts;
end ;
$$ language plpgsql volatile