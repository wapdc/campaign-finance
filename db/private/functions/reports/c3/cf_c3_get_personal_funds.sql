
/**
  This function produces the json equivalent of the personal funds cover section of the C3 deposit report for
  inclusion in a C3 submission.

  When there are more than one personal funds items, the old behavior would only include the first one and ignore the
  rest because the report only has room for one line. In this implementation, it aggregates them to one total and uses
  the max date for the date.

  It is based on the function line1B() in orca-app/src/main/java/orca/reports/C3.java

  @param p_depositevent_trankeygen_id the trankeygen_id of the deposit event
  @return json of personal funds
 */

create or replace function private.cf_c3_get_personal_funds(p_depositevent_trankeygen_id int) returns json as $$

    declare

        dep_debt bigint;
        dep_fund int;
        dep_date date;

        persFunds_totalAmt      decimal := 0.0;
        persFunds_amt           decimal := 0.0;
        persFunds_date          date;
        result              json;

    begin
        select deptkey, fund_id, date_ into dep_debt, dep_fund, dep_date
        from private.vdepositevents
        where trankeygen_id = p_depositevent_trankeygen_id;

        raise notice '%, %, %', dep_debt, dep_fund, dep_date;

        select sum(amount) into persFunds_totalAmt
        from private.vreceipts r
        where r.deptkey = dep_debt and r.fund_id = dep_fund and r.date_ <= dep_date
          and r.transaction_type = 'Personal Funds';

        select sum(r.amount), max(r.date_) into persFunds_amt,  persFunds_date
        from private.vreceipts r
        where deptkey = dep_debt and fund_id = dep_fund  and r.transaction_type = 'Personal Funds';

        select row_to_json(v) into result from (select persFunds_totalAmt as aggregate, persFunds_amt as amount, persFunds_date as date) v;
        return result;
    end
$$ language plpgsql stable ;