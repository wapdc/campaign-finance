
/**
  This function produces the json equivalent of the auction items section of the C3 deposit report for
  inclusion in a c3 submission, generated from the private database for a fund

  it is based on the function getAuctionsItem in orca-a/src/main/java/orca/reports/C3.java
 */

CREATE OR REPLACE FUNCTION private.cf_c3_get_auction_items(p_auction_trankeygen_id INTEGER, p_deptkey bigint) RETURNS json AS
$$

DECLARE
    j_auction_items json;

BEGIN
    SELECT json_agg(a ORDER BY a.number)
    INTO j_auction_items
    FROM (
             SELECT au.itemnumber         AS number,
                    au.itemdescription    AS description,
                    au.marketval          AS market_value,
                    r1.amount + r2.amount AS sale_amount,
                    json_build_object(
                            'contact_key', cast(r1.contid as text),
                            'amount', r1.amount,
                            'election', case -- elekey is returning as integer, converting to either primary, general or null
                                            when r1.elekey = 0 then 'P'
                                            when r1.elekey = 1 then 'G'
                                            else 'N'
                                end
                        )                 AS donate,
                    json_build_object(
                            'contact_key', cast(r2.contid as text),
                            'amount', r2.amount,
                            'election', case -- elekey is returning as integer, converting to either primary, general or null
                                            when r2.elekey = 0 then 'P'
                                            when r2.elekey = 1 then 'G'
                                            else 'N'
                                end
                        )                 AS buy
             FROM private.auctionitems AS au
                      JOIN private.receipts r1 ON au.trankeygen_id = r1.pid -- donor_contId
                      JOIN private.receipts r2 ON au.trankeygen_id = r2.pid -- buyer_contId
                      JOIN private.receipt_type c1 ON r1.type = c1.type AND c1.code IN ('Auction Item Donor')
                      JOIN private.receipt_type c2 ON r2.type = c2.type AND c2.code IN ('Auction Item Buyer')
                      JOIN private.cf_report_add_contact(r1.contid, true, true, true) rc1 on 1 = 1
                      JOIN private.cf_report_add_contact(r2.contid, true, true, true) rc2 on 1 = 1
             WHERE au.pid = p_auction_trankeygen_id
               AND r1.deptkey = p_deptkey
         ) a;
    RETURN j_auction_items;
END ;

$$
    LANGUAGE plpgsql VOLATILE