/**
  Function to build a c3 submission as json

  @param p_depositevent_trankeygen_id the trankeygen_id of a specific deposit event
  @return json
 */

create or replace function private.cf_c3_build_report(p_depositevent_trankeygen_id int) returns json as
$$
declare
    deposit_date date;
    submitted_at timestamp;
    fund int;
    report jsonb;
    election_code text;
    election_year text;
    filer_id text;
    committee_id int;
    candidate_contact_key text;
    amends text;
    external_id text;

    v_contributions jsonb;

begin


    select de.date_, de.fund_id, de.trankeygen_id into deposit_date, fund, external_id
    from private.vdepositevents de where de.trankeygen_id = p_depositevent_trankeygen_id;

    -- Use the election from the fund table which contains the right value for continuing committees.
    select c.committee_id, c.election_code, cast(substr(f.election_code, 1, 4) as integer), c.filer_id, now(), r.report_id
    into committee_id, election_code, election_year, filer_id, submitted_at, amends
    from public.committee c
    join public.fund f on c.committee_id = f.committee_id
    left join public.report r on r.external_id = cast(p_depositevent_trankeygen_id as varchar) and r.fund_id = fund
      and r.superseded_id is null
    where f.fund_id = fund;


    -- Add Basic submission data ---------------------------------------------------------------------------------------
    report := jsonb_build_object(
        'report_type', 'c3',
        'fund', fund,
        'election_code', election_code,
        'election_year', election_year,
        'filer_id', filer_id,
        'sums', null,
        'period_start', deposit_date,
        'period_end', deposit_date,
        'contacts', array[]::text[],
        'misc', array[]::text[],
        'auctions', array[]::text[],
        'loans', array[]::text[],
        'submitted_at', submitted_at,
        'amends', amends,
        'external_id', external_id
        );

    /**
      The functions used by this function to get all the totals produce a side effect of inserting the contact ids into
      a temporary table that is later used to collect up all the contact information for inclusion in the report. This
      function must define the following temporary table so is is available, even though it's not directly used in this
      function. See cf_report_add_contact.
     */

    -- This drop is helpful for testing scenarios where we are not closing the session or explicitly committing. In
    -- production runs of the code, we can never try to submit more than one report in parallel in the same session.
    drop table if exists "#temp_contact_table";

    create temporary table "#temp_contact_table"
    (contact_key text, force_cont_address boolean, test_emp_address boolean, force_cont_aggs boolean)
    on commit drop;

    -- Include the candidate contact if this is a candidate campaign ---------------------------------------------------
    select cast(c.trankeygen_id as text) into candidate_contact_key from private.trankeygen t join private.vcontacts c on t.fund_id = c.fund_id
    where t.trankeygen_id = p_depositevent_trankeygen_id and c.type = 'CAN';

    if (candidate_contact_key is null) then
        select trankeygen_id::text into candidate_contact_key FROM private.properties p
          left join private.trankeygen t on t.orca_id::text = p.value
            and T.fund_id=p.fund_id
          where p.fund_id = fund and p.name = 'CANDIDATEINFO:CONTACT_ID';
    end if;
    if (candidate_contact_key is not null) then
        perform private.cf_report_add_contact(
            candidate_contact_key, true, false, false);
        report := jsonb_set(report, '{candidate_contact_key}', candidate_contact_key::text::jsonb , true);
    end if;

    /**
      As sections are appended to the jsonb report object, make sure that they coalesce to something if you want to
      include an empty property when there are no items (see misc) or check that the json you are going to insert is not
      null and just don't call jsonb_set if you wnt to exclude it. calling jsonb_set with null returns a null object.
     */

    -- Add committee information ---------------------------------------------------------------------------------------
    report := jsonb_set(report, '{committee}',
         coalesce(private.cf_report_get_committee(fund,committee_id)::jsonb,
              'null'::jsonb), true);


    -- Add treasurer information ---------------------------------------------------------------------------------------
    report := jsonb_set(report, '{treasurer}',
         coalesce(private.cf_report_get_treasurer_info(fund)::jsonb,
                 'null'::jsonb), true);

    -- Create top level for sums where aggregates will list as nested children -----------------------------------------
    report := jsonb_set(report, '{sums}',
        jsonb_build_object());

    -- Add all the line1A Anonymous contribution -----------------------------------------------------------------------
    report := jsonb_set(report, '{sums, anonymous}',
        coalesce(private.cf_c3_get_anonymous_contributions(p_depositevent_trankeygen_id)::jsonb,
                 'null'::jsonb), true);

    -- Add all the line1B Personal contribution ------------------------------------------------------------------------
    report := jsonb_set(report, '{sums, personal}',
        coalesce(private.cf_c3_get_personal_funds(p_depositevent_trankeygen_id)::jsonb,
                 'null'::jsonb), true);

    -- Get all the contributions data ----------------------------------------------------------------------------------
    -- This one is a little weird because the function returns the itemized and small contributions and then we split
    -- out the properties to add them to their respective sections. It was done this way to make sure a single function
    -- accounts for all contributions so that the logic can never skip any of them
    v_contributions := private.cf_c3_get_itemized_and_small_contributions(
        p_depositevent_trankeygen_id);

    -- Add all the line1E small contributions --------------------------------------------------------------------------
    report := jsonb_set(report, '{sums, small}',
                        coalesce(v_contributions->'small',
                                 'null'::jsonb), true);

    -- Add all the itemized contributions ------------------------------------------------------------------------------
    report := jsonb_set(report, '{contributions}',
                        coalesce(v_contributions->'itemized',
                                 'null'::jsonb), true);

    -- Add all the loan receipts ---------------------------------------------------------------------------------------
    report := jsonb_set(report, '{loans}',
        coalesce(private.cf_c3_get_loans(p_depositevent_trankeygen_id)::jsonb,
                                 'null'::jsonb), true);

    -- Add all the auction receipts ------------------------------------------------------------------------------------
    report := jsonb_set(report, '{auctions}',
        coalesce(private.cf_c3_get_auctions(p_depositevent_trankeygen_id)::jsonb,
                                 'null'::jsonb), true);

    -- Add all the miscellaneous receipts ------------------------------------------------------------------------------
    report := jsonb_set(report, '{misc}',
        coalesce(private.cf_c3_get_miscellaneous_receipts(p_depositevent_trankeygen_id)::jsonb,
                 'null'::jsonb), true);

    -- Add all the attached pages ------------------------------------------------------------------------
    report := jsonb_set(report, '{attachments}',
                        coalesce(private.cf_c3_get_attached_text_pages(p_depositevent_trankeygen_id)::jsonb,
                                 'null'::jsonb), true);

    -- Add all the contacts --------------------------------------------------------------------------------------------
    /**
      This should probably be the last step because the contacts temporary table is built up by all the other sections.

      If any refactoring of the code is done, you need to consider the impact on how contacts are aggregated. It is
      important that cf_report_get_contacts() is called after all other parts of the report have been run, or at least
      all the parts that generate contact information. For example, get_contributions adds contacts to the temporary
      table that get_contacts uses. If both were run as part of the same select statement they will run in whatever
      order the database engine decides or they might even run in parallel. This would result in not having all the
      contacts in the table when get_contacts needs them.
     */

    report := jsonb_set(report, '{contacts}',
        coalesce(private.cf_report_get_contacts(deposit_date)::jsonb,
                 'null'::jsonb), true);

    return to_json(report);

end
$$ language plpgsql volatile