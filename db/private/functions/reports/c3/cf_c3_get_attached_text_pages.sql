CREATE OR REPLACE FUNCTION private.cf_c3_get_attached_text_pages(p_depositevent_trankeygen_id INTEGER) RETURNS JSON AS
$$

DECLARE
    result      json;
BEGIN

    select json_agg(json_build_object('content', app.content))
      into result
    FROM private.depositevents de
             join (SELECT ap.target_id, ap.text_data as content
                   FROM private.attachedtextpages ap
                   WHERE ap.target_id = p_depositevent_trankeygen_id
                     AND target_type = 'deposit'
    ) as app on app.target_id = de.trankeygen_id;
   return result;
END
$$ LANGUAGE plpgsql VOLATILE;