/**
  This function produces the json equivalent of the auctions section of the C3 deposit report for
  inclusion in a c3 submission, generated from the private database for a fund

  it is based on the function getAuctions in orca-a/src/main/java/orca/reports/C3.java
 */

CREATE OR REPLACE FUNCTION private.cf_c3_get_auctions(p_depositevent_trankeygen_id INTEGER) RETURNS JSON AS
$$

DECLARE

    auctions json;

BEGIN

    SELECT json_agg(a ORDER BY a.date)
    INTO auctions
    FROM (
             SELECT distinct on (a.trankeygen_id)
                                                  r.date_ AS date,
                                                  x       as items
             FROM private.depositevents d
                      JOIN private.trankeygen tdep ON d.trankeygen_id = tdep.trankeygen_id
                      JOIN private.trankeygen trec ON tdep.fund_id = trec.fund_id
                      JOIN private.receipts r ON trec.trankeygen_id = r.trankeygen_id AND d.deptkey = r.deptkey
                      JOIN private.accounts a ON a.trankeygen_id = r.did
                      JOIN private.receipt_type c
                           ON r.type = c.type
                               AND c.code IN ('Auction Item Donor')
                      JOIN private.cf_c3_get_auction_items(a.trankeygen_id, d.deptkey) x ON 1 = 1

             WHERE d.trankeygen_id = p_depositevent_trankeygen_id) a;

    RETURN auctions;
END
$$ LANGUAGE plpgsql VOLATILE