
/**
  This function produces the json equivalent of the anonymous contributions cover section of the
  C3 deposit report for inclusion in a C3 submission.

  In this implementation, it aggregates them to one total and uses the max date for the data.

  It is based on the function line1A() in orca-app/src/main/java/orca/reports/C3.java

  @param p_deposit_trankeygen_id the trankeygen_id of the deposit event
  @return json of the anonymous contributions

 */


create or replace function private.cf_c3_get_anonymous_contributions(p_depositevent_trankeygen_id int) RETURNS json AS $$
    declare
        dep_debt bigint;
        dep_fund int;
        dep_date date;

        anon_totalAmt     decimal := 0.0;
        anon_amt          decimal := 0.0;
        anon_date         date;
        result            json;

    begin

        select deptkey, fund_id, date_ into dep_debt, dep_fund, dep_date
        from private.vdepositevents
        where trankeygen_id = p_depositevent_trankeygen_id;

        raise notice '%, %, %', dep_debt, dep_fund, dep_date;

        select sum(amount) into anon_totalAmt
        from private.vreceipts r
        where r.deptkey = dep_debt and r.fund_id = dep_fund and r.date_ <= dep_date
          and r.transaction_type = 'Anonymous Contribution';

        select sum(r.amount), case when count(distinct r.date_) =1 then max(r.date_) end  into anon_amt,  anon_date
        from private.vreceipts r
        where deptkey = dep_debt and fund_id = dep_fund  and r.transaction_type = 'Anonymous Contribution';

        select row_to_json(v) into result from (select anon_totalAmt as aggregate, anon_amt as amount, anon_date as date) v;
        return result;
    end
$$ language plpgsql stable;

