/**
  This function produces the json equivalent of the loans section of the C3 deposit report for
  inclusion in a c34 submission, generated from the private database for a fund

  it is based on the function getLoans in orca-a/src/main/java/orca/reports/C3.java
 */

CREATE OR REPLACE FUNCTION private.cf_c3_get_loans(p_depositevent_trankeygen_id INTEGER) RETURNS JSON AS
$$

DECLARE
    loans json;

BEGIN

    select json_agg(l ORDER BY l.date, l.contact_key)
    into loans
    from (select cast(r.contid as text)                            AS contact_key,
                 r.amount                                          AS amount,
                 r.date_                                           AS date,
                 CASE -- elekey is returning as integer, converting to either primary, general or null
                     WHEN r.elekey = 0 THEN 'P'
                     WHEN r.elekey = 1 THEN 'G'
                     ELSE 'N'
                     END                                           AS election,
                 ROUND(l.interestrate, 2)                          AS interest_rate,
                 l.duedate                                         AS due_date,
                 l.repaymentSchedule                               AS payment_schedule,
                 private.cf_c3_get_loan_endorsers(l.trankeygen_id) AS endorser
          FROM private.depositevents d
                   JOIN private.trankeygen tdep ON d.trankeygen_id = tdep.trankeygen_id
                   JOIN private.trankeygen trec ON tdep.fund_id = trec.fund_id
                   JOIN private.receipts r ON trec.trankeygen_id = r.trankeygen_id AND d.deptkey = r.deptkey
                   JOIN private.loans l ON l.trankeygen_id = r.trankeygen_id
                   JOIN private.receipt_type c
                        ON r.type = c.type
                            AND c.code IN ('Monetary Loan')
                   JOIN private.cf_report_add_contact(r.contid, true, true, false) ON 1 = 1
          WHERE d.trankeygen_id = p_depositevent_trankeygen_id) l;


    RETURN loans;
END
$$ LANGUAGE plpgsql VOLATILE