--drop function if exists private.cf_potential_reporting_obligations_by_fund(int);
create or replace function private.cf_potential_reporting_obligations_by_fund(target_fund_id int)
  returns table (fund_id int, reporting_period_id int, start_date date, end_date date, due_date date)
  language plpgsql
as $$
declare
  c_start_date date;
  c_end_date date;
  c_election_codes text[];
  committee_election_code text;
  c_continuing boolean := false;
  c_ballot_type text;
  c_is_special boolean := false;
  c_pac_type text;
  fund_election_year text;
  max_period_end date;
  ro_rec record;
  v_skip_ro boolean = false;
  v_start_date date;
  v_end_date date;
  v_due_date date;
  v_reporting_period_id integer;
begin

  -- election code and continuing status
  select f.election_code, c.continuing, c.election_code::text, c.pac_type
  into fund_election_year, c_continuing, committee_election_code, c_pac_type
    from fund f
      join committee c
        on f.committee_id = c.committee_id
    where f.fund_id = target_fund_id;

  -- error if fund election year is unknown
  if fund_election_year is null then
    raise exception 'Fund election year could not be found for fund %', target_fund_id;
  end if;

  -- ballottype property
  c_ballot_type := p.value
    from private.properties p
    where name = 'CAMPAIGNINFO:BALLOTTYPE'
      and p.fund_id = target_fund_id;

  -- set ballot type from committee election code
  if c_ballot_type is null then
    c_ballot_type =
    case
      -- general and primary election = 3
      when length(committee_election_code) = 4
        then '3'
      -- primary election only = 1
      when length(committee_election_code) = 5
        then '1'
      -- special election = monthly, custom
      when length(committee_election_code) in (6, 7)
        then '0'
      -- special election, unspecified election or unhandled election = monthly, custom
      else '0'
    end;
  end if;

  -- isspecial property
  c_is_special := nullif(p.value, '')::boolean
    from private.properties p
    where name = 'CAMPAIGNINFO:ISSPECIAL'
      and p.fund_id = target_fund_id;

  -- campaign start date property
  c_start_date := date(value)
    from private.properties p
    where name = 'CAMPAIGNINFO:STARTDATE'
      and p.fund_id = target_fund_id;

  -- error if campaign start date is missing
  if c_start_date is null then
    c_start_date := date(fund_election_year || '-01-01');
  end if;

  -- get election codes
  c_election_codes := (select array_agg(election_code) from private.election_participation ep where ep.fund_id = target_fund_id);
    if (c_election_codes is null) then
    -- special february
    if (c_is_special is true and c_ballot_type = '1') then
      c_election_codes := array_append(c_election_codes, fund_election_year  || 'S2');
    -- special april
    elsif (c_is_special is true and c_ballot_type = '3') then
      c_election_codes := array_append(c_election_codes, fund_election_year  || 'S4');
    -- primary only
    elsif (c_is_special is not true and c_ballot_type in ('1', '2')) then
      c_election_codes := array_append(c_election_codes, fund_election_year  || 'P');
    -- primary and general
    elsif (c_is_special is not true and c_ballot_type = '3') then
      c_election_codes := array_append(c_election_codes, fund_election_year  || 'P');
      c_election_codes := array_append(c_election_codes, fund_election_year);
    -- general only
    elsif (c_is_special is not true and c_ballot_type = '4') then
      c_election_codes := array_append(c_election_codes, fund_election_year);
    -- custom periods
    elsif (c_ballot_type in ('-1', '0', '5', '6') and committee_election_code is not null) then
      c_election_codes := array_append(c_election_codes, committee_election_code);
    end if;
  end if;

  -- continuing committee end boundary
  if c_continuing = true then
    if c_pac_type = 'surplus' THEN
        c_end_date := cast(now() + interval '30 days' as date);
    ELSE
        c_end_date := fund_election_year || '-12-31';
    end if;
  else
    -- check for existing reports
    SELECT max(r.period_end) into max_period_end from report r where report_type='C4' and r.fund_id = target_fund_id;
    -- latest election date will determine end boundary for non-continuing
    c_end_date := greatest(max_period_end, max(rp.end_date))
                  from c4_report_period rp
        join election e
          on e.election_date = rp.election_date
                  where e.election_code = any (c_election_codes)
                  group by max_period_end;

    -- include one month after the election for ballottypes 2, 3, 4
    if c_end_date is not null and c_ballot_type in ('2', '3', '4') then
      c_end_date := c_end_date + interval '1' month;
    end if;
  end if;

  -- throw errors if end boundary calculation failed
  if c_end_date is null then
    raise exception 'Campaign end boundary could not be determined for fund %', target_fund_id;
  end if;

  for ro_rec in (
      select * from
          (select
               r1.reporting_period_id,
               r1.start_date,
               r1.end_date,
               r1.due_date,
               r.report_id,
               r.period_start,
               r.period_end,
               first_value( r1.reporting_period_id) over (partition by r.report_id order by r1.start_date) as first_reporting_period_id,
               last_value(r1.reporting_period_id) over (partition by r.report_id order by r1.start_date) as last_reporting_period_id,
               row_number() over (partition by r.report_id order by r1.start_date desc) rn_desc,
               row_number() over (partition by r1.reporting_period_id order by r.period_start desc) as rn_rpt_desc

           from potential_reporting_obligations(c_start_date, c_end_date, c_election_codes) r1
                    left join report r on (r1.start_date, r1.end_date + 1) overlaps (r.period_start, r.period_end + 1)
                        and r.superseded_id is null and r.fund_id=target_fund_id and r.report_type='C4') p
      order by p.start_date, p.period_start) LOOP

          -- If we aren't continuing from the last reporting period then we should initialize the dates
          if v_start_date is null then
              v_start_date = greatest(c_start_date, least(ro_rec.start_date, ro_rec.period_start));
          end if;


          -- If the reporting period overlaps a report, decide what to do
          if ro_rec.report_id is not null then
              if v_reporting_period_id is null then
                  v_reporting_period_id = ro_rec.reporting_period_id;
              end if;
             if ro_rec.rn_desc = 1 then

                 -- If the reporting obligation is wholly contained by the report skip this RO
                 if  ro_rec.start_date >= ro_rec.period_start and ro_rec.end_date < ro_rec.period_end then
                     v_skip_ro = true;
                     v_reporting_period_id = ro_rec.reporting_period_id;
                 end if;

                 -- Use the original reporting period from the report because
                 -- That makes sure we don't duplicate reporting period ids which are used when the reporting
                 -- obligations are saved.
                 if ro_rec.period_end < ro_rec.end_date then
                     select min(r.reporting_period_id) into v_reporting_period_id
                       from c4_report_period r where r.start_date = ro_rec.period_start
                         and r.end_date = ro_rec.period_end;
                     v_reporting_period_id = coalesce(v_reporting_period_id, ro_rec.first_reporting_period_id);
                 end if;
                 v_end_date = least(ro_rec.end_date, ro_rec.period_end);
                 v_due_date = ro_rec.due_date;
              end if;
          else
              v_skip_ro = false;
              v_reporting_period_id = ro_rec.reporting_period_id;
              v_end_date = ro_rec.end_date;
              v_due_date = ro_rec.due_date;
          end if;


          if ro_rec.report_id is null or rO_rec.rn_desc=1  then
              if not v_skip_ro then
                  fund_id = target_fund_id;
                  reporting_period_id = v_reporting_period_id;
                  start_date = v_start_date;
                  end_date = v_end_date;
                  due_date = v_due_date;
                  return next;
                  v_start_date = v_end_date + 1;
                  if v_end_date < ro_rec.end_date and ro_rec.rn_rpt_desc = 1 THEN
                      reporting_period_id = ro_rec.last_reporting_period_id;
                      start_date = v_start_date;
                      end_date = ro_rec.end_date;
                      due_date = ro_rec.due_date;
                      return next;
                      v_start_date = ro_rec.end_date + 1;
                  end if;
                  v_reporting_period_id = null;
              end if;
              v_skip_ro = false;
          end if;
   end loop;
end
$$;