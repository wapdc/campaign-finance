CREATE OR REPLACE function private.cf_get_committee_treasurers(p_committee_id INTEGER) returns json as
$$
    DECLARE
    treasurers json;
  BEGIN
        select json_agg(t.*)
        into treasurers from (select distinct cc.name, cc.phone
                              from public.committee_contact cc
                                       join public.fund f on f.committee_id = cc.committee_id
                              where f.committee_id = p_committee_id
                                and treasurer is true
                                and trim(name) IS NOT NULL
                                and trim(name) <> ''
        ) t;

        RETURN treasurers;
END
$$ LANGUAGE plpgsql STABLE;