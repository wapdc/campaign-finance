
/**
  Function to return the aggregate total of receipts for a given contact up to a specific date.

  Only specific transaction types are included. Developed for generating totals used in report submissions. The reason
  this function wraps cf_report_get_contact_aggregates so that it can be called without the join which might get called
  for every iteration of a join before it gets filtered. For many things that check aggregates, only the total is
  interesting so it seemed simpler to return a simple scalar rather than the whole row of prim, gen, total.

  @param p_cont_id the id of the contact. This is the trankeygen_id in the contacts table which is unique
  @param p_up_to_date aggregate will include all receipts before and up to the date provided
  @return decimal(16, 2)
 */

create or replace function private.cf_report_get_contact_aggregate_total (p_cont_id integer, p_up_to_date date)
returns decimal(16, 2) as $$

    select total from private.cf_report_get_contact_aggregates(p_cont_id, p_up_to_date);

$$ language sql