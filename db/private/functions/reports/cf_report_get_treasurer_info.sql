
/**
  This function produces treasurer information of the C3 or C4 report for
  inclusion in a submission.

  @param p_fund_id the fund id of the fund we need a treasurer for
  @return json of treasurer info
 */
drop function if exists private.cf_report_get_treasurer_info(p_depositevent_trankeygen_id int);
create or replace function private.cf_report_get_treasurer_info(p_fund_id int) returns json as $$

declare

    treasurer_info      record;
    result              json;

begin
    select * from (select *, ROW_NUMBER() OVER (PARTITION by committee_id ORDER BY ministerial desc, contact_id) rownum
        FROM committee_contact
        WHERE treasurer = true) tc
        where tc.committee_id = (select committee_id from fund where fund_id = p_fund_id) and tc.rownum = 1 into treasurer_info;

    select row_to_json(v) into result from (select treasurer_info.name as name, treasurer_info.phone as phone) v;
    return result;
end
$$ language plpgsql stable ;