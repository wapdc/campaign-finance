--drop function private.cf_populate_reporting_obligations(c_fund_id int);
create or replace function private.cf_populate_reporting_obligations(c_fund_id int)
  returns void
  language plpgsql
as $$
begin

-- delete any existing potential obligations that are not in the current set
delete from private.reporting_obligation ro
where ro.fund_id = c_fund_id
  and ro.reporting_period_id not in (select reporting_period_id FROM private.cf_potential_reporting_obligations_by_fund(c_fund_id));

-- Update existing reporting obligations to make sure dates change appropriately when we combine or discover reports
update private.reporting_obligation ro
  set startdate= v.start_date, enddate = v.end_date, duedate = v.due_date
  from (
    select ro2.obligation_id, ro3.start_date, ro3.end_date, ro3.due_date
    from private.reporting_obligation ro2
        join private.cf_potential_reporting_obligations_by_fund(c_fund_id) ro3
          on ro2.reporting_period_id = ro3.reporting_period_id
        where ro2.fund_id = c_fund_id)v
   where v.obligation_id = ro.obligation_id
     AND (v.start_date <> ro.startdate or v.end_date <> ro.enddate)
     and not exists (
      select fund_id
      from private.reporting_obligation reo
      where reo.fund_id = ro.fund_id and reo.startdate = v.start_date and reo.enddate = v.end_date);

-- Insert the records that don't exist.
insert into private.reporting_obligation (fund_id, reporting_period_id, startdate, enddate, duedate)
select fund_id, reporting_period_id, start_date, end_date, due_date
  from private.cf_potential_reporting_obligations_by_fund(c_fund_id) pro
where not exists (
  select reporting_period_id
  from private.reporting_obligation ro
  where ro.fund_id = c_fund_id
    and  (ro.reporting_period_id = pro.reporting_period_id)
  ) on conflict (fund_id, startdate, enddate)
    do update
    set reporting_period_id = excluded.reporting_period_id;

end
$$;