/**
  Function to accumulate contact ids into the temporary table "#temp_contact_table". The function assumes that the temp
  table already exists and has the form:
    create temporary table "#temp_contact_table"
      (contact_key int, force_cont_address boolean, test_emp_address boolean, force_cont_aggs boolean)
    on commit drop;

  The strategy being used is to simply insert all contacts ids and the address inclusion parameters without worrying
  about duplicates. When the table is used to generate the contacts, the duplicates can be removed with a group by,
  and selecting the most inclusive option. Assuming "true" is the most inclusive. Something like:

      select contact_key, bool_or(force_cont_address), bool_or(test_emp_address), bool_or(force_cont_aggs)
      from contact_table group by contact_key

  will produce one row per contact and that output can be joined into the private contacts to get all the actual
  contact values.

  @param p_contact_key the contact id for a single contact that will be added to the temporary table
  @param p_force_cont_address
  @param p_test_emp_address
  @param p_force_cont_agg
  @return table in the form (dummy int) always contains 1 row with the value 1. This is so the function can be used in a
    join with other tables and insert contacts as a side effect.
 */
CREATE OR REPLACE FUNCTION private.cf_report_add_contact(
    p_contact_key text,
    p_force_cont_address boolean,
    p_test_emp_address boolean,
    p_force_cont_agg boolean)
    RETURNS table (dummy int) AS
$$

DECLARE

BEGIN
    insert into "#temp_contact_table" values (p_contact_key, p_force_cont_address, p_test_emp_address, p_force_cont_agg);

    return query select 1;
end
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION private.cf_report_add_contact(
    p_contact_key int,
    p_force_cont_address boolean,
    p_test_emp_address boolean,
    p_force_cont_agg boolean)
    RETURNS table (dummy int) AS
$$
BEGIN
    RETURN query select 1 from private.cf_report_add_contact(
      cast(p_contact_key as text),
      p_force_cont_address,
      p_test_emp_address,
      p_force_cont_agg
    );
  END
$$ LANGUAGE plpgsql VOLATILE;