/*
  save a draft report
  make sure we save to the right fund for the campaign
  test see if we should be updating the draft report before trying to do an insert
 */
create or replace function private.cf_save_draft_report(p_committee_id integer, p_json json, p_view_only bool) returns int as
$$
declare
    v_election_code varchar;
    v_pac_type      varchar;
    v_fund_id       integer;
    v_draft_id      integer;
    v_period_start  date;
    v_period_end    date;
    c4_draft        integer;
    c3_draft        integer;
    v_treasurer     jsonb;
    v_committee     jsonb;
    v_user_data     json;
    v_amends          text;
    expiration_time timestamptz;
    vendor_name text;
    v_metadata json;
begin
    select
        case
            when c.continuing and c.election_code is not null
                and date_part('year', cast(p_json -> 'report' ->> 'period_start' as date))::int < substr(c.election_code, 1, 4)::int then
                substr(c.election_code, 1, 4)
            when c.continuing then
                date_part('year', cast(p_json -> 'report' ->> 'period_start' as date))::varchar
            else
                substr(c.election_code, 1, 4)
            end as election_code,
        pac_type
    into v_election_code, v_pac_type
    from committee c
    where c.committee_id = p_committee_id;

    select
        f.fund_id into v_fund_id
    from fund f
    where f.committee_id = p_committee_id
      and (f.election_code = v_election_code or v_pac_type = 'surplus');

    --fetch relevant treasurer & committee information and stick it on the json
    select json_build_object('name', name,
                             'phone', phone)
    into v_treasurer
    from (select *, ROW_NUMBER() OVER (PARTITION by committee_id ORDER BY ministerial desc, contact_id) rownum
          FROM committee_contact
          WHERE treasurer = true) tc
    where tc.committee_id = p_committee_id and tc.rownum = 1;

    select json_build_object('name', c.name,
                             'email', cc.email,
                             'address', cc.address,
                             'city', cc.city,
                             'zip', cc.postcode,
                             'state', cc.state)
    into v_committee
    from committee c
             join committee_contact cc on c.committee_id = cc.committee_id
    where c.committee_id = p_committee_id and cc.role = 'committee';

    select cast(p_json -> 'report' ->> 'period_start' as date) into v_period_start;
    select cast(p_json -> 'report' ->> 'period_end' as date) into v_period_end;
    expiration_time := now() + interval '2 weeks';

    select max(r.report_id)
    into v_amends
    from report r
    where r.fund_id = v_fund_id
      and r.superseded_id is null
      and (
                    r.report_type = 'C4' and 'C4' = upper(p_json -> 'report' ->> 'report_type')
                and (r.period_start, r.period_end + 1) overlaps (v_period_start, v_period_end + 1)
            or
                    r.external_id = p_json -> 'report' ->> 'external_id');

    select ((p_json -> 'report')::jsonb ||
            (select to_jsonb(j) from (select v_treasurer as "treasurer", v_committee as "committee", v_election_code as "election_year", v_amends as "amends", now() as submitted_at) j))::json
    into v_user_data;

    select ((p_json-> 'metadata')::jsonb ||
            (select to_jsonb(s) from (select p_json->'report'->'sums' as "sums")s))::json
        INTO v_metadata;

    --if reporting periods changed to have more than one draft overlapping delete all drafts for fund that overlap
    if (select count(*)
        from private.draft_report dr
        where (dr.period_start, dr.period_end) overlaps (v_period_start, v_period_end)
          and dr.fund_id = v_fund_id
          and dr.report_type = 'C4') > 1 then
        delete
        from private.draft_report
        where (period_start, period_end) overlaps (v_period_start, v_period_end)
          and fund_id = v_fund_id
          and report_type = 'C4';
    end if;

    --if we have a c4, then we want to get the draft that covers an overlapping reporting period
    select draft_id
    into c4_draft
    from private.draft_report dr
    where (dr.period_start, dr.period_end) overlaps (v_period_start, v_period_end)
      and dr.fund_id = v_fund_id
      and dr.report_type = 'C4';

    --if we have a c3, find the one with a matching external id
    select draft_id
    into c3_draft
    from private.draft_report dr
    where dr.external_id = p_json -> 'report' ->> 'external_id'
      and dr.fund_id = v_fund_id
      and dr.report_type = 'C3';

    if (c4_draft is not null)
    then
        update private.draft_report
        set period_start  = v_period_start,
            period_end    = v_period_end,
            received_date = now(),
            user_data     = v_user_data,
            metadata = v_metadata,
            expire_date   = expiration_time,
            processed = null,
            view_only = p_view_only
        where draft_id = c4_draft;
        v_draft_id := c4_draft;
    elsif (c3_draft is not null)
    then
        update private.draft_report
        set period_start  = v_period_start,
            period_end    = v_period_end,
            received_date = now(),
            user_data     = v_user_data,
            metadata = v_metadata,
            expire_date   = expiration_time,
            processed = null,
            view_only = p_view_only
        where draft_id = c3_draft;
        v_draft_id := c3_draft;
    else
        insert into private.draft_report (fund_id, report_type, external_id, period_start, period_end, user_data, metadata,
                                          expire_date, view_only)
        values (v_fund_id, upper(p_json -> 'report' ->> 'report_type'), p_json -> 'report' ->> 'external_id',
                v_period_start,
                v_period_end, v_user_data, v_metadata, expiration_time, p_view_only)
        returning draft_id into v_draft_id;
    end if;

    --update vendor name in the fund table
    vendor_name = p_json -> 'metadata' ->> 'vendor_name';
    update public.fund
    SET vendor = vendor_name
    WHERE fund_id = v_fund_id AND vendor IS DISTINCT FROM vendor_name;

    return v_draft_id;
end
$$ language plpgsql;