drop function if exists private.cf_add_one_reporting_obligation(integer);
create or replace function private.cf_add_one_reporting_obligation(c_fund_id integer)
returns integer
language plpgsql
as $$
declare
  c_reporting_period_id integer;
  v_obligation_id integer;
  r record;
begin
  c_reporting_period_id := reporting_period_id
    from c4_report_period rp
    where start_date = (
      select (ro.enddate + interval '1 day')::date as start_date
      from private.reporting_obligation ro
      where ro.fund_id = c_fund_id
      order by ro.enddate desc
      limit 1
    )
      and rp.monthly is true;

  if c_reporting_period_id is null then
    -- no reporting obligations is an unexpected state
    raise exception 'Cannot add one more reporting obligations. No reporting obligations found for fund %', c_fund_id;
  end if;

  for r in (
  select c_fund_id, rp.reporting_period_id, rp.start_date, rp.end_date, rp.due_date
    from c4_report_period rp
    where rp.reporting_period_id = c_reporting_period_id) loop
      insert into private.reporting_obligation (fund_id, reporting_period_id, startdate, enddate, duedate)
      values (c_fund_id, r.reporting_period_id, r.start_date, r.end_date, r.due_date)
      returning obligation_id into v_obligation_id;
    end loop;
  return v_obligation_id;

end
$$