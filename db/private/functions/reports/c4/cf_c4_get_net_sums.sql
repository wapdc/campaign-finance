/**
* Calculate the previous receipts and previous expenses for use in reporting the C4.
*   @param p_obligation_id
* Returns the following JSON structure
* {
*   "previous_receipts" : { "amount" : 6054.0 }
*   "previous_expenditures" : { "amount" : 12345.0 }
* }
*
*  Note that deptkey is used in this function to determine the deposit date of the trasaction as
*  the deptkey is the unix timestamp of the date of deposit.
*/
CREATE OR REPLACE FUNCTION private.cf_c4_get_net_sums(p_obligation_id integer) RETURNS json AS $$
    declare
        v_receipts numeric(10,2);
        v_expenses numeric(10,2);
        v_fund_id integer;
  begin
        select fund_id into v_fund_id from private.reporting_obligation where
          obligation_id = p_obligation_id;
        SELECT
            sum(case
                    when r.transaction_type = 'Carry Forward Cash' then r.amount
                    when r.carryforward <> 0 then 0.0
                    when r.deptkey not between 1 and extract(epoch from ro.startdate) * 1000 then 0.0
                    when
                        r.transaction_type in ('Loan Payment', 'Refund Contribution')
                        then -1.00 * amount
                    when r.nettype between 1 and 2 then r.amount
                    else 0.0
                end) receipts,
            sum(case
                    when r.carryforward <> 0 then 0.0
                    when r.transaction_type = 'Refund From Vendor' THEN -1.0 * amount
                    when r.transaction_type = 'Debt Forgiven' then amount
                    when r.nettype between 2 and 3 then amount
                    else 0.0
                end) expenses
        into v_receipts, v_expenses
        FROM private.reporting_obligation ro
            JOIN  private.vreceipts r on r.fund_id=ro.fund_id
         where ro.obligation_id = p_obligation_id
           and r.fund_id = v_fund_id
           and (r.transaction_type = 'Carry Forward Cash' or r.date_ < ro.startdate);

        return json_build_object('previous_receipts', json_build_object('amount', coalesce(v_receipts,0.0)),
            'previous_expenditures', json_build_object('amount', coalesce(v_expenses,0.00)));
  end;
$$ language plpgsql stable