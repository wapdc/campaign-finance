CREATE OR REPLACE FUNCTION private.cf_c4_get_debt(p_obligation_id INTEGER) RETURNS json AS $$
  declare
      j_debts JSONB;
      j_debt JSONB;
      v_debt_date date;
      j_subdebts JSONB;
      j_subdebt JSONB;
      v_fund_id INTEGER;
      v_start_date DATE;
      v_end_date DATE;
      v_max_date DATE;
      v_payments numeric(10,2);
      v_balance numeric(10,2);
      v_debt_balance numeric(10,2);
      acct record; -- Credit card account with totals of all transactions
      debt record; -- An individual credit card debt entry
      debt_ob record; -- RECORD
      debt_ex record; -- RECORD

  begin
     j_debts := json_build_array();
     -- Get base reporting obligation info
     select fund_id, startdate, enddate into v_fund_id, v_start_date, v_end_date
         from private.reporting_obligation where obligation_id = p_obligation_id;

     -- start with credit cards.
     -- Each credit card is tied to an account, so we need to loop through the sum
     -- of all payments to see which items are totally paid off and which ones aren't.
     for acct  in (
         SELECT a.*, total_payments  FROM private.vaccounts a
           LEFT JOIN (
              SELECT cid, sum(amount) as total_payments FROM private.vreceipts r
              WHERE r.fund_id = v_fund_id AND r.date_ <= v_end_date
                AND r.transaction_type = 'Credit Card Payment'
              GROUP BY cid ) vr ON a.trankeygen_id = vr.cid
         WHERE a.fund_id = v_fund_id and a.acctnum = 2600
       ) loop
        -- Initialize total payments to date.
        v_payments := COALESCE(acct.total_payments, 0.0);
        j_subdebts := json_build_array();
        v_balance := 0;
        v_debt_date := null;

        -- Now find all credit card debts in order of expense date we skip
        -- reporting those that have been paid when the payment no longer is
        -- considered applied start including.
        FOR debt in (
           SELECT * from private.vreceipts r
             WHERE r.fund_id = v_fund_id
               and (r.transaction_type = 'Credit Card Debt'
                        or r.transaction_type = 'Expenditure'
                        or r.transaction_type = 'Interest Expense'
                        or r.transaction_type='Debt Payment')
               and r.date_ <= v_end_date
               and r.did = acct.trankeygen_id
            ORDER BY r.date_, r.trankeygen_id
        ) LOOP

          -- Apply all or part of the remaining payment amount to
          -- the debts
          IF v_payments > debt.amount THEN
            v_debt_balance := 0;
            v_payments := v_payments - debt.amount;
          ELSE
            v_debt_balance := debt.amount - v_payments;
            v_payments := 0;
          END IF;

          -- Remaining balance on the debt means that it should be included
          -- as a subdebt entry of the debt.
            IF  v_debt_balance > 0 THEN
              v_debt_date := coalesce(v_debt_date, debt.date_);
              v_balance := v_balance + v_debt_balance;
              IF debt.transaction_type = 'Credit Card Debt' THEN
                j_subdebt := json_build_object(
                  'amount' , v_debt_balance,
                  'contact_key' , debt.contid::text,
                  'date' , debt.date_,
                  'description' , debt.description,
                  'category' , debt.category_code
                  );
                j_subdebts := j_subdebts || j_subdebt;
                -- Testing the employee address feels wrong in credit card transactions but that's the way it is in orca.
                PERFORM private.cf_report_add_contact(debt.contid, true, true, false);
              END IF;
          END IF;
       END LOOP;

       -- IF we have any debt records left then include a new debt entry
       -- into the array of debt entries so far.
       IF v_balance > 0 THEN
         j_debt := jsonb_build_object(
           'amount', v_balance,
           'contact_key', acct.contid::text,
           'date', v_debt_date
         );
         j_debt := jsonb_set(j_debt, '{sub_debt}', j_subdebts, true);
         -- Testing the employee address feels wrong in credit card transactions but that's the way it is in orca.
         PERFORM private.cf_report_add_contact(acct.contid, true, true, false);
         -- adds another debt to the array of debts
         j_debts := j_debts || j_debt;
       END IF;
     END LOOP;

     -- ***** DEBTS AND DEBT PAYMENTS ***************************************************************************
     -- Loop through all the debts that are received before the report end date.
     for debt  in (
         SELECT  vr.trankeygen_id, vr.contid, vr.amount, vr.date_, vr.description, vr.category
           FROM private.vreceipts vr
         where
            vr.fund_id = v_fund_id
            and vr.type = 19
            and transaction_type = 'Debt'
            and vr.date_ <= v_end_date
         ORDER BY vr.date_, vr.trankeygen_id
     ) LOOP
       j_subdebts := json_build_array();
       v_balance = debt.amount;

       -- Each debt entry contains a debt obligation, one of which is the main debt.
        -- Debts with sub-debts contain multiple debtobligation records all of which point
        -- to the base debt entry in the receipt table. The one that matches by contact id
        -- contains the record based on the main debt.
        FOR debt_ob IN  (SELECT dob.contid, dob.amount, dob.date_, dob.descr, COALESCE(pymt.payments,0) as payments FROM private.debtobligation dob
          LEFT JOIN (
              -- THis gets all the payments, forgiveness and adjustments on the debt to
              -- Determine whether it is paid of and how much the balance is.
              SELECT pid, sum(case when transaction_type = 'Debt Adjustment' then amount * -1.0 else amount end) as payments FROM private.vreceipts
                WHERE fund_id = v_fund_id
                  AND date_ <= v_end_date
                  AND transaction_type in ('Debt Payment', 'Debt Forgiven', 'Debt Adjustment')
                  GROUP BY pid
            ) pymt ON pymt.pid = dob.debtobligation_id
          WHERE dob.pid = debt.trankeygen_id
          ORDER BY dob.date_, dob.debtobligation_id) LOOP

          v_debt_balance = debt_ob.amount - debt_ob.payments;
          v_balance = v_balance - debt_ob.payments;

          -- IF we are working with the "main" debt obligation include it's data as the base debt
          -- but the amount field should be the total balance of the debt.
          IF (debt.contid = debt_ob.contid)  THEN
              j_debt := json_build_object(
                      'contact_key' , debt_ob.contid::text,
                      'date' , debt_ob.date_,
                      'description' , COALESCE(debt_ob.descr, debt.description),
                      'category' , debt.category
                  );
              PERFORM private.cf_report_add_contact(debt_ob.contid, true, true, false ) ;
          ELSE
             if v_debt_balance > 0 THEN
               j_subdebt = json_build_object(
                  'amount' , v_debt_balance,
                  'contact_key', debt_ob.contid::text,
                  'date', debt_ob.date_,
                  'description', COALESCE(debt_ob.descr,debt.description),
                  'category', debt.category);
             j_subdebts = j_subdebts || j_subdebt;
             PERFORM private.cf_report_add_contact(debt_ob.contid, true, true, false ) ;
             end if;
          END IF;
         END LOOP;
         IF v_balance > 0 THEN
             j_debt := jsonb_set(j_debt, '{amount}', v_balance::text::jsonb, true);
             -- Add debts
             if jsonb_array_length(j_subdebts) > 0 THEN
                j_debt := jsonb_set(j_debt, '{sub_debt}', j_subdebts);
             end if;
             j_debts := j_debts || j_debt;
             END IF;
    END LOOP;

     -- Get vendor debt
     -- acct 2000 group by cont id
     -- calc balance by balance payment applied

     for debt_ex in (
         select vc.trankeygen_id as contid, sum(ee.amount) as balance, min(ee.date_) as date from private.vaccounts va
            join private.expenditureevents ee on ee.bnkid = va.trankeygen_id
            join private.vcontacts vc on ee.contid = vc.trankeygen_id
         and ee.date_ <= v_end_date
         and va.fund_id = v_fund_id
         and va.acctnum = 2000
         group by vc.trankeygen_id
     ) loop
         -- set balance before payments have been applied
         v_balance = debt_ex.balance;

         select MAX(vr.date_), sum(case when vr.transaction_type = 'Debt Adjustment' then vr.amount * -1.0 else vr.amount end) into v_max_date, v_payments from private.vreceipts vr
             where vr.fund_id = v_fund_id
             and vr.date_ <= v_end_date
             and vr.transaction_type in ('Debt Payment', 'Debt Forgiven', 'Debt Adjustment')
             and vr.contid = debt_ex.contid;

        if v_payments is not null then
            v_balance = v_balance - v_payments;
        end if;

         IF v_balance > 0 or (v_max_date between v_start_date and v_end_date) then
             IF (debt_ex.contid IS NOT NULL)  THEN
                 j_debt := json_build_object(
                    'contact_key' , debt_ex.contid::text,
                    'amount', v_balance,
                    'date', debt_ex.date,
                    'description', '',
                    'category', '');
             end if;

             PERFORM private.cf_report_add_contact(debt_ex.contid, true, true, false );

             j_debt := jsonb_set(j_debt, '{amount}', v_balance::text::jsonb, true);
             j_debts := j_debts || j_debt;
         end if;
         end loop;
    return cast(j_debts as json);
  end
$$ language plpgsql stable;