CREATE OR REPLACE function private.cf_c4_combine_reports_check(p_obligation_id integer) returns json as
$$
  declare
    v_min_period_start date;
    v_max_period_end date;
    v_last_start_date date;
    j json;
    v_count integer;
    v_fund_id integer;
    ro_rec record;
  begin
    v_count := 0;
    j := jsonb_build_object();
    select fund_id, startdate, enddate into v_fund_id,v_last_start_date, v_max_period_end from private.reporting_obligation
      where obligation_id = p_obligation_id;
    for ro_rec in (
        select distinct ro2.startdate, ro2.enddate
          from private.reporting_obligation ro2
          left join report r on r.fund_id=ro2.fund_id
            and r.superseded_id is null
            and (ro2.startdate, ro2.enddate + 1) overlaps (r.period_start, r.period_end + 1)
            and r.report_type = 'C4'
          where ro2.fund_id = v_fund_id
            and ro2.enddate < v_last_start_date
            and r.report_id is null
          order by ro2.startdate desc
      ) loop
      raise notice 'startdate, enddate, last: %, %, %', ro_rec.startdate, ro_rec.enddate, v_last_start_date;
      if ro_rec.enddate + 1 = v_last_start_date then
          v_count := v_count + 1;
          v_min_period_start := least(ro_rec.startdate, coalesce(v_min_period_start,ro_rec.startdate));
          v_last_start_date := ro_rec.startdate;
      else
          raise notice 'skipping startdate, enddate: %, %', ro_rec.startdate, ro_rec.enddate;
      end if;
    end loop;

    if v_count = 0 then
      return null;
    else
     select  row_to_json(ro3) into j from (select v_min_period_start as startdate, v_max_period_end as enddate ) ro3;
    end if;
    return j;
  end
$$ language plpgsql;