CREATE OR REPLACE FUNCTION private.cf_c4_get_deposits(p_obligation_id INTEGER) RETURNS json AS $$
    DECLARE
        j json;
    begin
      SELECT json_agg(d ORDER BY d.date, d.amount )
        INTO j
      FROM (SELECT v.date_ as date, v.amount FROM
        private.reporting_obligation ro
        JOIN private.vdepositevents v
          ON  v.fund_id = ro.fund_id
             AND v.date_ between ro.startdate and ro.enddate
        WHERE
          ro.obligation_id = p_obligation_id
      ) d;
     return j;
    end;
$$ language plpgsql stable;
