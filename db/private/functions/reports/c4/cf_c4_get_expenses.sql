CREATE OR REPLACE FUNCTION private.cf_c4_get_expenses(p_obligation_id INTEGER) RETURNS json AS $$

DECLARE
    c record;
    j_expenses jsonb;
    j_itemized_expenditures jsonb;
    j_itemized_expense jsonb;
    v_total numeric(9,2) := 0.0;
    v_count integer := 0;
    v_date date;
    itemized_expense_threshold numeric(16,2);
    under_threshold_debt_total numeric(9,2) := 0.0;
    under_threshold_debt_count integer := 0;
    under_threshold_debt_date date;
    v_generated_account_code text;
    v_generated_transaction_type_code text;

BEGIN
    itemized_expense_threshold := (select amount from thresholds where property = 'itemized_expenses');
    j_itemized_expenditures := jsonb_build_array();
    FOR c in (
        select
            r.*,
            ro.startdate,
            ro.enddate,
            e.contid as payee_contid,
            coalesce(a.contid,a2.contid) as account_contid,
            e.bnkid,
            sum(ABS(r.amount)) over (partition by r.contid) as contact_amount
        from private.reporting_obligation ro
        join private.vreceipts r
            ON r.fund_id = ro.fund_id and r.date_ between ro.startdate and ro.enddate
        left join private.expenditureevents e on r.pid = e.trankeygen_id
        left join private.accounts a on a.trankeygen_id = e.bnkid
        left join private.accounts a2 on a2.trankeygen_id = r.did

        WHERE ro.obligation_id=p_obligation_id
          AND r.transaction_type in ('Expenditure','Refund Contribution', 'Interest Expense', 'Loan Payment', 'Credit Card Payment', 'Debt Payment' )
          -- This query filter  is redundant but improves performance drastically
          and r.fund_id = (select fund_id from private.reporting_obligation where obligation_id=p_obligation_id)
          and r.amount <> 0
        order by r.date_, r.contid, r.trankeygen_id
    )

      LOOP
        -- General expenses are itemized if contact over the threshold for period, but some are always itemized.
        IF c.contact_amount > itemized_expense_threshold  OR c.itemized <> 0 OR c.transaction_type <> 'Expenditure' THEN

            -- generated based on the transaction_type code
            v_generated_transaction_type_code = case
                when c.transaction_type = 'Loan Payment' then 'L-PYMT'
                when c.transaction_type = 'Debt Payment' or c.transaction_type = 'Credit Card Payment' then 'D-PYMT'
                when c.transaction_type = 'Interest Expense' then 'INT'
                when c.transaction_type = 'Refund Contribution' then 'REF'
            end;

            -- generated based on the account code or the account group category_code
            if v_generated_transaction_type_code is null then
                v_generated_account_code = (
                    select coalesce(a.code, ag.category_code)
                    from private.account_groups ag right join private.accounts a on ag.acctnum=a.acctnum
                    where c.cid=a.trankeygen_id
                );
            end if;

            j_itemized_expense = jsonb_build_object(
                'amount', c.amount,
                'contact_key', cast(c.contid as text),
                'date', c.date_,
                'description', c.description,
                'category', coalesce((select code from expense_category where code = c.category_code), v_generated_transaction_type_code, v_generated_account_code, 'OTHER'),
                'category_description', c.category
            );
            If c.contid <> c.payee_contid then
                j_itemized_expense:= jsonb_set(j_itemized_expense, '{payee_key}', c.payee_contid::text::jsonb,true);
                PERFORM private.cf_report_add_contact(c.payee_contid,true, false, false);
            end if;
            IF c.did_acctnum in (2000, 2600) then
                j_itemized_expense := jsonb_set(j_itemized_expense, '{creditor_key}', c.account_contid::text::jsonb, true);
                PERFORM private.cf_report_add_contact(c.account_contid,true, false, false);
            end if;
            if c.transaction_type in ('Credit Card Payment', 'Debt Payment') THEN
                j_itemized_expense := jsonb_set(j_itemized_expense, '{payment_category}', to_jsonb('debt'::text), true);
            end if;
            if c.transaction_type in ('Loan Payment') THEN
                j_itemized_expense := jsonb_set(j_itemized_expense, '{payment_category}', to_jsonb('loan'::text), true);
            end if;
            PERFORM private.cf_report_add_contact(c.contid,true, false, false);
            j_itemized_expenditures = j_itemized_expenditures || j_itemized_expense;
        ELSEIF c.did_acctnum not in (2000, 2600) THEN
            v_total := v_total + c.amount;
            v_date :=  CASE
                WHEN v_count = 0 THEN c.date_
                WHEN v_date is null THEN NULL
                when v_date = c.date_ THEN v_date
                END;
            v_count := v_count + 1;
        ELSEIF c.did_acctnum in (2000, 2600) THEN
            under_threshold_debt_total := under_threshold_debt_total + c.amount;
            under_threshold_debt_date :=  CASE
                WHEN under_threshold_debt_count = 0 THEN c.date_
                WHEN under_threshold_debt_date is null THEN NULL
                when under_threshold_debt_date = c.date_ THEN under_threshold_debt_date
                END;
            under_threshold_debt_count := under_threshold_debt_count + 1;
        END IF;
        END LOOP;

     j_expenses := json_build_object(
       'itemized_expenses' , cast(j_itemized_expenditures as json),
       'non_itemized_cash_expenses', cast(json_build_object(
           'amount', v_total,
           'count', v_count,
           'date', v_date
           ) as json),
       'non_itemized_credit_expenses', cast(json_build_object(
                     'amount', under_threshold_debt_total,
                     'count', under_threshold_debt_count,
                     'date', under_threshold_debt_date
                 ) as json)
       );
    RETURN j_expenses;
END
$$ LANGUAGE plpgsql STABLE
