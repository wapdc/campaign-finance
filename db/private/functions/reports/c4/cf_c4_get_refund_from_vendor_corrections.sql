CREATE OR REPLACE FUNCTION private.cf_c4_get_refund_from_vendor_corrections(p_obligation_id INTEGER) RETURNS json AS $$
  DECLARE
      j_vendor_corrections  json;
      v_fund_id      INTEGER;
      v_end_date     date;
      v_start_date   date;
  BEGIN
      SELECT fund_id, enddate, startdate
      INTO v_fund_id, v_end_date, v_start_date
      FROM private.reporting_obligation
      WHERE obligation_id = p_obligation_id;

      SELECT json_agg(c)
      INTO j_vendor_corrections
      FROM (SELECT R.amount,
                   R.date_ date,
                   R.contid::text contact_key,
                   0 sumAgg,
                   R.description,
                   A.exp_code
            FROM private.vreceipts R
                     LEFT JOIN (SELECT exp_code, trankeygen_id FROM private.vaccounts) A
                         ON R.did = A.trankeygen_id
            JOIN private.cf_report_add_contact(R.contid, true, false, false) on 1 = 1
            WHERE date_ >= v_start_date
              AND date_<= v_end_date
              AND transaction_type = 'Refund From Vendor'
              AND R.fund_id = v_fund_id
      ) c;
      RETURN j_vendor_corrections;
  END
$$ LANGUAGE plpgsql STABLE;