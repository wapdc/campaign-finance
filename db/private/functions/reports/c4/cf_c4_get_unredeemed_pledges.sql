CREATE OR REPLACE FUNCTION private.cf_c4_get_unredeemed_pledges(p_reporting_obligation_id INTEGER) RETURNS JSON AS
$$
DECLARE
    unredeemed_pledges json;
    v_startdate date;
    v_enddate date;
    v_fund_id integer;

BEGIN
    select fund_id, startdate, enddate into v_fund_id, v_startdate, v_enddate from private.reporting_obligation
      where obligation_id=p_reporting_obligation_id;

    SELECT json_agg(p ORDER BY p.date, p.contact_key)
    INTO unredeemed_pledges
    FROM (SELECT r.type,
                 s.pId,
                 cast(r.contId as text)                                           contact_key,
                 r.amount - CASE WHEN total_amt IS NULL THEN 0 ELSE total_amt END amount,
                 date_                                                            date,
                 CASE -- elekey is returning as integer, converting to either primary, general or null
                     WHEN r.elekey = 0 THEN 'P'
                     WHEN r.elekey = 1 THEN 'G'
                     ELSE 'N'
                     END AS                                                       election
          FROM private.vreceipts r
                   LEFT JOIN (
              SELECT vr.pid, SUM(vr.amount) total_amt
              FROM private.vreceipts vr
              WHERE vr.transaction_type in (
                               'Monetary Pledge',
                               'In Kind Pledge Payment',
                               'Pledge Cancelled'
                  )
                AND vr.fund_id = v_fund_id
                AND vr.date_ <= v_enddate
              GROUP BY vr.pid
          ) s on s.pid = r.rds_id
                   JOIN private.cf_report_add_contact(contid, true, true, false) ON 1 = 1
          WHERE r.fund_id = v_fund_id and  r.transaction_type = 'Pledge'
            -- Don't include future pledges
            AND r.date_ <= v_enddate
            -- Checking to make sure pledge has a non zero balance at the end or the report
            -- Outstanding pledges
            AND (total_amt IS NULL OR total_amt < amount)) p;


    return unredeemed_pledges;
END;
$$ LANGUAGE plpgsql VOLATILE