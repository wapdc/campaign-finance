CREATE OR REPLACE FUNCTION private.cf_c4_get_loans(p_obligation_id INTEGER) RETURNS json AS
$$

DECLARE
    loans        jsonb;
    j_receipts   jsonb;
    j_payments   jsonb;
    j_forgiven   jsonb;
    j_balances   jsonb;
    v_fund_id    integer;
    v_start_date date;
    v_end_date   date;

BEGIN

    --set variables that will be used through function
    select fund_id, startdate, enddate
    into v_fund_id, v_start_date, v_end_date
    from private.reporting_obligation
    where obligation_id = p_obligation_id;

    -- create each individual jsonb array objects
    -- receipts
    SELECT jsonb_agg(l order by l.date, l.contact_key)
    INTO j_receipts
    FROM (
             select l.trankeygen_id,
                    cast(l.contid as text)                            as contact_key,
                    l.amount                                          as amount,
                    --When carry-forward loan grabs the original loan date/ else the receipt date
                    case when l.carryforward = 1
                        then l.date_ else vr.date_ end                as date,
                    l.dueDate                                         as due_date,
                    l.interestrate                                    as interest_rate,
                    CASE -- elekey is returning as integer, converting to either primary, general or null
                        WHEN l.elekey = 0 THEN 'P'
                        WHEN l.elekey = 1 THEN 'G'
                        ELSE 'N'
                        END                                           as election,
                    case when l.type = 5 then true else false end     as in_kind,
                    case when l.carryforward = 1
                        then true else false end                      as carry_forward,
                    --When carry-forward or in-kind we send the description
                    case when l.type = 5 AND l.carryforward = 0
                        then l.description
                        when l.carryforward = 1
                        then
                            concat(case when l.description is not null then concat(l.description, ' / ') end,
                                'carried forward from prior campaign (', vr.data->>'committee_name', ' ', vr.data->>'election_year', ')')
                        end                                           as description,
                    l.repaymentschedule                               as payment_schedule,
                    private.cf_c3_get_loan_endorsers(l.trankeygen_id) AS endorser
             from private.vreceipts vr
                      join private.loans l on vr.trankeygen_id = l.trankeygen_id
                      join private.cf_report_add_contact(l.contid, true, true, false) on 1 = 1
             where vr.fund_id = v_fund_id
               and vr.date_ >= v_start_date
               and vr.date_ <= v_end_date
             ) l;

    -- payments
    SELECT jsonb_agg(l order by l.date, l.contact_key)
    INTO j_payments
    FROM (
             SELECT vr.date_                                                 date,
                    CAST(l.contid AS TEXT) AS                                contact_key,
                    vr.amount                                                principle_paid,
                    ir.amount                                                interest_paid,
                    l.amount - CASE WHEN s.amt IS NULL THEN 0 ELSE s.amt END balance
             FROM private.vreceipts vr
                      JOIN private.vreceipts ir ON vr.trankeygen_id = ir.pid
                      JOIN private.loans l ON vr.pid = l.trankeygen_id
                      LEFT JOIN (
                 SELECT r.pid, SUM(r.amount) amt
                 FROM private.vreceipts r
                 WHERE r.fund_id = v_fund_id
                   AND r.date_ <= v_end_date
                   AND r.transaction_type IN ('Loan Payment', 'Loan Forgiven')
                 GROUP BY r.pid
             ) s ON vr.pid = s.pid
                      JOIN private.cf_report_add_contact(vr.contid, true, false, false) on 1 = 1
             WHERE vr.fund_id = v_fund_id
               AND ir.fund_id = v_fund_id
               AND vr.date_ >= v_start_date
               AND vr.date_ <= v_end_date
               AND vr.transaction_type = 'Loan Payment'
         ) l;
    -- forgiven
    SELECT jsonb_agg(l order by l.date, l.contact_key)
    into j_forgiven
    from (
             select vr.date_                                      date,
                    cast(l.contid as text) as                     contact_key,
                    l.amount                                      original_amount,
                    case when s.amt IS NULL THEN 0 ELSE s.amt END principle_paid,
                    vr.amount                                     forgiven_amount
             FROM private.vreceipts vr
                      JOIN private.loans l on vr.pid = l.trankeygen_id
                      LEFT JOIN (select r.pid, SUM(r.amount) amt
                                 from private.vreceipts r
                                 where r.fund_id = v_fund_id
                                     and (r.date_ <= v_end_date and r.transaction_type = 'Loan Payment')
                                    or (r.date_ < v_start_date and r.transaction_type = 'Loan Forgiven')
                                 group by r.pid) s on vr.pid = s.pid
                      join private.cf_report_add_contact(vr.contid, true, false, false) on 1 = 1
             where vr.fund_id = v_fund_id
               and vr.date_ >= v_start_date
               and vr.date_ <= v_end_date
               and vr.transaction_type = 'Loan Forgiven'
         ) l;

    -- balances
    SELECT jsonb_agg(l order by l.date, l.contact_key)
    into j_balances
    from (
             select l.date_                                         date,
                    cast(l.contId as text)                          contact_key,
                    l.amount                                        original_amount,
                    case when s.prin is null then 0 else s.prin end principle_paid
             from private.loans l
                      join private.trankeygen t on l.trankeygen_id = t.trankeygen_id
                      left join (
                 select r.pid, SUM(r.amount) prin
                 from private.vreceipts r
                 where r.fund_id = v_fund_id
                   and r.transaction_type in ('Loan Payment', 'Loan Forgiven')
                   and r.date_ <= v_end_date
                 group by r.pid
             ) s on l.trankeygen_id = s.pid
                      left join private.cf_report_add_contact(l.contid, true, true, false) on 1 = 1
             where t.fund_id = v_fund_id
               and l.date_ <= v_end_date
               and (prin is null or prin < l.amount)
         ) l;

    -- build loans object w/ respected array of objects
    loans := jsonb_build_object(
            'receipts', coalesce(j_receipts, 'null'::jsonb),
            'payments', coalesce(j_payments, '[]'::jsonb) || coalesce(j_forgiven, '[]'::jsonb),
            'balances', coalesce(j_balances, 'null'::jsonb)
        );
    return loans::json;
END
$$ LANGUAGE plpgsql STABLE