CREATE OR REPLACE FUNCTION private.cf_c4_get_expenditure_corrections(p_obligation_id INTEGER) RETURNS json AS $$
  DECLARE
      j_expense_corrections  json;
      v_fund_id              INTEGER;
      v_end_date             date;
      v_start_date           date;
  BEGIN
      SELECT fund_id, enddate, startdate
      INTO v_fund_id, v_end_date, v_start_date
      FROM private.reporting_obligation
      WHERE obligation_id = p_obligation_id;

      SELECT json_agg(e)
      INTO j_expense_corrections
      FROM (SELECT R.date_ date,
                   R.contId::text contact_key,
                   C.description description,
                   R.amount reported_amount,
                   R.amount + C.amt corrected_amount,
                   C.amt difference
            FROM (
                     SELECT vreceipts.*,
                            CASE
                                WHEN transaction_type = 'Refund Contribution'
                                    THEN amount*-1
                                ELSE amount END amt
                     FROM private.vreceipts
                      where fund_id = v_fund_id
                 ) C
                     JOIN private.receipts R
                                ON C.pid = R.trankeygen_id
                     JOIN private.receipt_type rt ON rt.type=r.type
                     JOIN private.cf_report_add_contact(R.contid, true, false, false) on 1 = 1
            WHERE C.date_ >= v_start_date
              AND C.fund_id = v_fund_id
              AND C.date_ <= v_end_date
              AND C.transaction_type IN (
                                         'Refund Contribution',
                                         'Correction In Kind',
                                         'Correction Expense'
                                        )
              AND rt.code IN (
                                         'Correction Expense Math Error',
                                         'Correction Receipt Math Error',
                                         'Monetary Contribution',
                                         'In Kind Contribution',
                                         'Expenditure',
                                         'Debt Payment',
                                         'Credit Card Payment'
                                        )
          ) e;
      RETURN j_expense_corrections;
  END
$$ LANGUAGE plpgsql STABLE;