CREATE OR REPLACE FUNCTION private.cf_c4_get_inkind_contributions(p_obligation_id INTEGER) RETURNS json AS
$$

DECLARE
    j_contributions_inkind json;
    v_fund_id integer;
BEGIN
    select fund_id into v_fund_id from private.reporting_obligation where obligation_id=p_obligation_id;
    SELECT json_agg(p order by p.date, p.contact_key, p.amount)
    INTO j_contributions_inkind
    FROM (select
              case
                     -- The math here is important that it's (amount - amount * 0.5) for contact 2 because we have amounts
                     -- that are not equally divisible by 2. A couple contact of 49.99 will be 25.00 and 24.99
                     when cc.cnum = 1 then (vr.amount * 0.5)::decimal(10, 2)
                     when cc.cnum = 2 then vr.amount - (vr.amount * 0.5)::decimal(10, 2)
                     else vr.amount
                     end as amount,
                 vr.date_                date,
                 cast(coalesce( cc.contact_id, vr.contid) as text) contact_key,
                 vr.description          description,
                 CASE -- elekey is returning as integer, converting to either primary, general or null
                     WHEN vr.elekey = 0 THEN 'P'
                     WHEN vr.elekey = 1 THEN 'G'
                     ELSE 'N'
                     END AS              election
          from private.reporting_obligation ob
                   join private.vreceipts vr on ob.fund_id = vr.fund_id
                   left join private.vcouplecontacts cc on vr.contid = cc.couple_contact_id
                   left join private.cf_report_add_contact(coalesce(cc.contact_id, vr.contId), true, true, false) on 1 = 1
          where ob.obligation_id = p_obligation_id
            and vr.fund_id = v_fund_id
            and vr.date_ >= ob.startdate
            and vr.date_ <= ob.enddate
            and vr.carryforward = 0
            and vr.transaction_type in (
                            'Debt Forgiven',
                            'In Kind Contribution',
                            'In Kind Loan',
                            'In Kind Pledge Payment'
              )) p;
    RETURN j_contributions_inkind;
END
$$ LANGUAGE plpgsql STABLE