/**
  Function to build a c3 submission as json

  @param p_depositevent_trankeygen_id the trankeygen_id of a specific deposit event
  @return json
 */

create or replace function private.cf_c4_build_report(p_obligation_id int) returns json as
$$
declare
    submitted_at timestamp;
    v_period_start date;
    v_period_end date;
    v_fund_id int;
    report jsonb;
    election_code text;
    election_year text;
    filer_id text;
    committee_id int;
    candidate_contact_key text;
    v_amends text;
    j_expenses json;
    j_combine_check json;

begin


    select ro.startdate, ro.enddate, ro.fund_id into v_period_start, v_period_end, v_fund_id
    from private.reporting_obligation ro where ro.obligation_id = p_obligation_id;

    -- USe the election from the fund table which contains the right value for continuing committees.
    select c.committee_id, c.election_code, cast(substr(f.election_code, 1, 4) as integer), c.filer_id, now()
    into committee_id, election_code, election_year, filer_id, submitted_at
    from public.committee c
             join public.fund f on c.committee_id = f.committee_id
    where f.fund_id = v_fund_id;

    -- Find a report that this could amend
    select max(r.report_id) into v_amends from report r
      where r.fund_id = v_fund_id
      and r.report_type = 'C4'
      and r.superseded_id is null
      and (r.period_start, r.period_end +1) overlaps (v_period_start, v_period_end + 1);


    -- Add Basic submission data ---------------------------------------------------------------------------------------
    report := jsonb_build_object(
            'report_type', 'c4',
            'fund', v_fund_id,
            'election_code', election_code,
            'election_year', election_year,
            'filer_id', filer_id,
            'sums', null,
            'period_start', v_period_start,
            'period_end', v_period_end,
            'submitted_at', submitted_at,
            'amends', v_amends
        );

    j_combine_check := private.cf_c4_combine_reports_check(p_obligation_id);
    if (j_combine_check is not null and v_amends is null) then
      report := jsonb_set(report,'{combine_allowed}',j_combine_check::jsonb, true);
    end if;

    /**
      The functions used by this function to get all the totals produce a side effect of inserting the contact ids into
      a temporary table that is later used to collect up all the contact information for inclusion in the report. This
      function must define the following temporary table so is is available, even though it's not directly used in this
      function. See cf_report_add_contact.
     */

    -- This drop is helpful for testing scenarios where we are not closing the session or explicitly committing. In
    -- production runs of the code, we can never try to submit more than one report in parallel in the same session.
    drop table if exists "#temp_contact_table";

    create temporary table "#temp_contact_table"
    (contact_key text, force_cont_address boolean, test_emp_address boolean, force_cont_aggs boolean)
        on commit drop;

    -- Include the candidate contact if this is a candidate campaign ---------------------------------------------------
    select cast(c.trankeygen_id as text) into candidate_contact_key
       from private.trankeygen t join private.vcontacts c on t.fund_id = c.fund_id
    where t.fund_id = v_fund_id and c.type = 'CAN';

    if (candidate_contact_key is null) then
        select trankeygen_id::text into candidate_contact_key FROM private.properties p
                                                                       left join private.trankeygen t on t.orca_id::text = p.value
            and T.fund_id=p.fund_id
        where p.fund_id = v_fund_id and p.name = 'CANDIDATEINFO:CONTACT_ID';
    end if;

    if (candidate_contact_key is not null) then
        perform private.cf_report_add_contact(
                candidate_contact_key, true, false, false);
        report := jsonb_set(report, '{candidate_contact_key}', candidate_contact_key::text::jsonb , true);
    end if;

    /**
      As sections are appended to the jsonb report object, make sure that they coalesce to something if you want to
      include an empty property when there are no items (see misc) or check that the json you are going to insert is not
      null and just don't call jsonb_set if you wnt to exclude it. calling jsonb_set with null returns a null object.
     */

    -- Add committee information ---------------------------------------------------------------------------------------
    report := jsonb_set(report, '{committee}',
                        coalesce(private.cf_report_get_committee(v_fund_id,committee_id)::jsonb,
                                 'null'::jsonb), true);


    -- Add treasurer information ---------------------------------------------------------------------------------------
    report := jsonb_set(report, '{treasurer}',
                        coalesce(private.cf_report_get_treasurer_info(v_fund_id)::jsonb,
                                 'null'::jsonb), true);

    -- Create top level for sums where aggregates will list as nested children -----------------------------------------
    report := jsonb_set(report, '{sums}',
                        private.cf_c4_get_net_sums(p_obligation_id)::jsonb, true);

    -- Add in-kind contributions
    report := jsonb_set(report, '{contributions_inkind}',
            coalesce(private.cf_c4_get_inkind_contributions(p_obligation_id)::jsonb, 'null'::jsonb), true);

    -- Add deposits
    report := jsonb_set(report, '{misc_receipts}',
            coalesce(private.cf_c4_get_deposits(p_obligation_id)::jsonb, 'null'::jsonb), true);

    -- Add expenses
    j_expenses := private.cf_c4_get_expenses(p_obligation_id);
    if json_array_length(j_expenses->'itemized_expenses') > 0 then
       report := jsonb_set(report, '{expenses}', CAST(j_expenses->'itemized_expenses' AS jsonb), TRUE);
    end if;

    --new refactoring of expenses_expenses_under_50 as threshold has raised
    report := jsonb_set(report, '{sums,non_itemized_cash_expenses}', cast(j_expenses->'non_itemized_cash_expenses' as jsonb), TRUE);

    -- Add credit/vendor debt under threshold
    report := jsonb_set(report, '{sums,non_itemized_credit_expenses}', cast(j_expenses->'non_itemized_credit_expenses' as jsonb), TRUE);

    -- Add Debt
    report := jsonb_set(report, '{debt}',COALESCE(private.cf_c4_get_debt(p_obligation_id)::jsonb, 'null'::jsonb), true);

    -- Add Loans
    report :=jsonb_set(report, '{loan}', COALESCE(private.cf_c4_get_loans(p_obligation_id)::jsonb, 'null'::jsonb), true);

    -- Corrections
    report := jsonb_set(report, '{contribution_corrections}', COALESCE(private.cf_c4_get_contribution_corrections(p_obligation_id)::jsonb, 'null'::jsonb), true);

    report := jsonb_set(report, '{expense_corrections}', COALESCE(private.cf_c4_get_expenditure_corrections(p_obligation_id)::jsonb, 'null'::jsonb), true);

    report := jsonb_set(report, '{expense_refunds}', COALESCE(private.cf_c4_get_refund_from_vendor_corrections(p_obligation_id)::jsonb, 'null'::jsonb), true);

    -- Add unredeemed pledges
    report := jsonb_set(report, '{pledge}',
                        coalesce(private.cf_c4_get_unredeemed_pledges(p_obligation_id)::jsonb,
                                 'null'::jsonb), true);


    -- Add all the attached pages ------------------------------------------------------------------------
    report := jsonb_set(report, '{attachments}',
                        coalesce(private.cf_c4_get_attached_text_pages(p_obligation_id)::jsonb,
                                 'null'::jsonb), true);

    -- Add all the contacts --------------------------------------------------------------------------------------------
    /**
      This should probably be the last step because the contacts temporary table is built up by all the other sections.

      If any refactoring of the code is done, you need to consider the impact on how contacts are aggregated. It is
      important that cf_report_get_contacts() is called after all other parts of the report have been run, or at least
      all the parts that generate contact information. For example, get_contributions adds contacts to the temporary
      table that get_contacts uses. If both were run as part of the same select statement they will run in whatever
      order the database engine decides or they might even run in parallel. This would result in not having all the
      contacts in the table when get_contacts needs them.
     */

    report := jsonb_set(report, '{contacts}',
                        coalesce(private.cf_report_get_contacts(v_period_end)::jsonb,
                                 'null'::jsonb), true);

    return to_json(report);

end
$$ language plpgsql volatile