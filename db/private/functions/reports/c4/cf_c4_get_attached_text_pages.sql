CREATE OR REPLACE FUNCTION private.cf_c4_get_attached_text_pages(p_obligation_id INTEGER) RETURNS JSON AS
$$

DECLARE
    result      json;
BEGIN

    select json_agg(json_build_object('content', app.content))
      into result
    FROM private.reporting_obligation ro
             join (SELECT ap.target_id, ap.text_data as content
                   FROM private.attachedtextpages ap
                   WHERE ap.target_id = p_obligation_id
                     AND target_type = 'reporting_obligation'
    ) as app on app.target_id = ro.obligation_id;
   return result;
END
$$ LANGUAGE plpgsql VOLATILE;