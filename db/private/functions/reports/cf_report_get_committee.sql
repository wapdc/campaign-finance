/**
  This function produces committee information for c3 and c4 reports

  @param fund_id the trankeygen_id of the deposit event
  @return json of committee info
 */

create or replace function private.cf_report_get_committee(p_fund_id int, p_committee_id int) returns json as $$

declare

    result                  json;

begin

     SELECT row_to_json (v)
        into result
        from  (
        select c.name,
               c.committee_id,
               cc.address,
               cc.city,
               cc.state,
               cc.postcode,
               cand.candidacy_id,
               cand.office_code,
               ofv.offtitle AS office
            FROM public.committee c
              join public.fund f on c.committee_id = f.committee_id
              left join public.candidacy cand on cand.committee_id = c.committee_id
              left join public.committee_contact cc on cc.committee_id = c.committee_id
                and cc.role = 'committee'
              left join public.foffice ofv on ofv.offcode=cand.office_code
            where f.fund_id = p_fund_id
            and c.committee_id = p_committee_id
        ) v ;
    return result;
end
$$ language plpgsql stable ;