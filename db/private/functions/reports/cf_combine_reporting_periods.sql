CREATE OR REPLACE function private.cf_combine_reporting_periods(p_obligation_id integer, p_start_date date) returns integer as
$$
  DECLARE
    v_enddate DATE;
    v_startdate DATE;
    v_obligation_id integer;
    v_fund_id INTEGER;
    v_reports INTEGER;
  BEGIN
    -- Make sure the record we are interested in exists.
    select fund_id, obligation_id, enddate INTO v_fund_id, v_obligation_id, v_enddate FROM private.reporting_obligation
      where obligation_id = p_obligation_id;

    if v_obligation_id is null then
        RAISE 'Invalid obligation_id %',p_obligation_id;
    end if;

    select startdate into v_startdate from private.reporting_obligation o where fund_id = v_fund_id
      and o.startdate = p_start_date;


    if v_startdate is null then
        RAISE 'Could not find reporting obligation that matches start fund/date %/%', v_fund_id,p_start_date;
    end if;

    SELECT count(repno) into v_reports from report where fund_id = v_fund_id and (period_start, period_end + 1) overlaps (v_startdate, v_enddate +1);

    if v_reports > 0 then
        RAISE 'Cannot combine reporting periods when they have already been reported against.';
    end if;

    update private.reporting_obligation o set startdate = v_startdate where o.obligation_id = v_obligation_id;

    -- Delete reporting obligations contained by this one but not included.
    delete from private.reporting_obligation o where o.startdate between v_startdate and v_enddate
      and o.enddate between v_startdate and v_enddate
      and o.fund_id = v_fund_id and o.obligation_id <> v_obligation_id;

    return v_obligation_id;
  END
$$ language plpgsql