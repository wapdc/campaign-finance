--DROP function private.cf_save_reporting_periods(p_fund_id INTEGER, j JSON);
CREATE OR REPLACE FUNCTION private.cf_save_reporting_periods(p_fund_id INTEGER, j JSON)
    RETURNS VOID as  $$
BEGIN
    IF j IS NOT NULL THEN
        DELETE FROM private.c4reportingperiods WHERE fund_id = p_fund_id
          AND (startdate,enddate) NOT IN (select cast(value->>'startdate' as date), cast(value->>'enddate' as date) from json_array_elements(j));
        INSERT INTO private.c4reportingperiods(fund_id, reported, startdate, enddate, duedate)
          select fund_id, max(reported) as reported, startdate, enddate, min(duedate) from (
              SELECT p_fund_id as fund_id,
                     cast(value->>'reported' as smallint) as reported,
                     cast(value->>'startdate' as date) as startdate,
                     cast(value->>'enddate' as date) as enddate,
                     cast(value->>'duedate' as date) as duedate
          FROM json_array_elements(J)) r
           group by fund_id, startdate, enddate
        ON CONFLICT (fund_id,startdate, enddate) DO UPDATE set duedate=EXCLUDED.duedate, reported=EXCLUDED.reported;
    END IF;
    RETURN;
END
$$ LANGUAGE plpgsql;