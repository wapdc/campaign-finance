/**
  Function to return the aggregate totals of receipts for a given contact up to a specific date.

  The contact id parameter must be a regular contact, not a couple contact. The function will find all receipts for any
  couple contact that is associated with the individual contact that is passed in.

  For a given contact id, receipts may be associated with the individual contact id but they may also be associated with
  a couple contact id. In those cases when the function receives the contact id, the amounts for receipts on the couple
  contact id need to be included in the aggregate but at 50% of the value.

  Only transactions that affect the aggregates are used, and the aggregate type modifies the amount to determine whether
  it adds or removes data from the transactions.

  @param p_cont_id the id of the contact. This is the trankeygen_id in the contacts table which is unique
  @param p_up_to_date aggregates will include all receipts before and up to the date provided
  @return table as (cont_id int, pagg numeric, gagg numeric, total numeric)
 */

create or replace function private.cf_report_get_contact_aggregates(p_cont_id integer, p_up_to_date date)
    returns table
        (
            contid int,
            pagg   numeric(16, 2),
            gagg   numeric(16, 2),
            total  numeric(16, 2)
        )
as
$$
begin

    return query
    select
        p_cont_id as contid,
        -- The math here is important that it's (amount - amount * 0.5) for contact 2 because we have amounts
        -- that are not equally divisible by 2. A couple contact of 49.99 will be 25.00 and 24.99
        -- The aggtype multiplier in each sum makes sure the transactions appropriately decrement or increment
        -- the aggregate total
        sum(case
                when cc.cnum = 1 then (r.amount * 0.5)::decimal(10, 2)
                when cc.cnum = 2 then r.amount - (r.amount * 0.5)::decimal(10, 2)
                else r.amount
            end * r.aggtype) filter (where r.elekey = 0)         as pagg,
        sum(case
                when cc.cnum = 1 then (r.amount * 0.5)::decimal(10, 2)
                when cc.cnum = 2 then r.amount - (r.amount * 0.5)::decimal(10, 2)
                else r.amount
            end * r.aggtype) filter (where r.elekey != 0) as gagg,
        sum(case
                when cc.cnum = 1 then (r.amount * 0.5)::decimal(10, 2)
                when cc.cnum = 2 then r.amount - (r.amount * 0.5)::decimal(10, 2)
                else r.amount
            end * r.aggtype)                              as total
    from private.receipts r
              join private.trankeygen t on t.trankeygen_id=r.trankeygen_id
              join (
        -- Creates a set with the contid passed in and all couple contact ids where contid is a member of the couple
        select p_cont_id as contid, null as cnum
        union all
        select couple_contact_id as contid, cnum
        from private.vcouplecontacts cc
        where cc.contact_id = p_cont_id
    ) cc on cc.contid=r.contid
    where
       r.date_ <= p_up_to_date
       and r.carryforward = 0
       and r.aggtype <> 0;
end
$$ language plpgsql stable