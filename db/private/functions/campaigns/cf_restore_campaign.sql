DROP FUNCTION IF EXISTS private.cf_restore_campaign(json_data json);
CREATE OR REPLACE FUNCTION private.cf_restore_campaign(json_data json, p_fund_id integer)
  RETURNS void as
$$
DECLARE
  j_trankeygen    json;
  j_attached      json;
  j_account       json;
  j_contact       json;
  j_individual    json;
  j_couple        json;
  j_receipt       json;
  j_auction       json;
  j_campaign_fund json;
  j_deposit       json;
  j_deposit_event json;
  j_election      json;
  j_expenditure   json;
  j_limit         json;
  j_loan          json;
  j_property      json;
  j_group         json;
  j_reporting     json;

BEGIN
  perform private.cf_delete_orca_campaign(p_fund_id);
  if json_data ->> 'campaignFund' is not null then
      for j_campaign_fund in select * from json_array_elements(json_data -> 'campaignFund')
          loop
              if cast(j_campaign_fund ->>'fund_id' as int) is distinct from p_fund_id THEN
                  RAISE 'Fund id mismatch detected in backup';
              end if;
              insert into private.campaign_fund(fund_id, version, deposit_number, save_count, updated)
              values (cast(j_campaign_fund ->> 'fund_id' as int), cast(j_campaign_fund ->> 'version' as numeric(10, 3)),
                      cast(j_campaign_fund ->> 'deposit_number' as int),
                      cast(j_campaign_fund ->> 'save_count' as int), to_timestamp(j_campaign_fund ->> 'updated', 'YYYY-MM-DD"T"HH24:MI:SS'));
          end loop;
  end if;
  for j_trankeygen in select * from json_array_elements(json_data -> 'trankeygen')
    loop
      if cast(j_trankeygen ->>'fund_id' as int) is distinct from p_fund_id THEN
          RAISE 'Fund id mismatch detected in backup';
      end if;
      insert into private.trankeygen(trankeygen_id, orca_id, fund_id, pid, derby_updated, rds_updated)
      values (cast(j_trankeygen ->> 'trankeygen_id' as int), cast(j_trankeygen ->> 'orca_id' as int),
              cast(j_trankeygen ->> 'fund_id' as int), cast(j_trankeygen ->> 'pid' as int),
              to_timestamp(j_trankeygen ->> 'derby_updated', 'YYYY-MM-DD"T"HH24:MI:SS'),
              to_timestamp(j_trankeygen ->> 'rds_updated', 'YYYY-MM-DD"T"HH24:MI:SS'));
      --'YYYY-MM-DD"T"HH24:MI:SS:MS'
    END LOOP;
  if json_data ->> 'accounts' is not null then
    for j_account in select * from json_array_elements(json_data -> 'accounts')
      loop
        insert into private.accounts(trankeygen_id, pid, acctnum, contid, code, description, style, total, memo, date_, orca_total)
        values (cast(j_account ->> 'trankeygen_id' as int), cast(j_account ->> 'pid' as int),
                cast(j_account ->> 'acctnum' as smallint), cast(j_account ->> 'contid' as int),
                j_account ->> 'code', j_account ->> 'description', cast(j_account ->> 'style' as smallint),
                cast(j_account ->> 'total' as numeric(16, 2)),
                j_account ->> 'memo', to_date(j_account ->> 'date_', 'YYYY-MM-DD'),
                cast(j_account ->> 'orca_total' as numeric(16, 2)));
      end loop;
  end if;
  if json_data ->> 'contacts' is not null then
    for j_contact in select * from json_array_elements(json_data -> 'contacts')
      loop
        insert into private.contacts(trankeygen_id, type, name, street, city, state, zip, phone, email, occupation,
                                     employername, employerstreet, employercity, employerstate, employerzip, memo, pagg, gagg)
        values (cast(j_contact ->> 'trankeygen_id' as int), j_contact ->> 'type', j_contact ->> 'name',
                j_contact ->> 'street', j_contact ->> 'city', j_contact ->> 'state', j_contact ->> 'zip',
                j_contact ->> 'phone', j_contact ->> 'email', j_contact ->> 'occupation', j_contact ->> 'employername',
                j_contact ->> 'employerstreet', j_contact ->> 'employercity', j_contact ->> 'employerstate',
                j_contact ->> 'employerzip',
                j_contact ->> 'memo', cast(j_contact ->> 'pagg' as numeric(16, 2)), cast(j_contact ->> 'gagg' as numeric(16, 2)));
      end loop;
  end if;
  if json_data ->> 'individualContacts' is not null then
    for j_individual in select * from json_array_elements(json_data -> 'individualContacts')
      loop
        insert into private.individualcontacts(trankeygen_id, prefix, firstname, middleinitial, lastname, suffix)
        values (cast(j_individual ->> 'trankeygen_id' as int), j_individual ->> 'prefix', j_individual ->> 'firstname',
                j_individual ->> 'middleinitial', j_individual ->> 'lastname', j_individual ->> 'suffix');
      end loop;
  end if;
  if json_data ->> 'coupleContacts' is not null then
    for j_couple in select * from json_array_elements(json_data -> 'coupleContacts')
      loop
        insert into private.couplecontacts(trankeygen_id, contact1_id, contact2_id)
        values (cast(j_couple ->> 'trankeygen_id' as int), cast(j_couple ->> 'contact1_id' as int),
                cast(j_couple ->> 'contact2_id' as int));
      end loop;
  end if;
  if json_data ->> 'registeredGroupContacts' is not null then
    for j_group in select * from json_array_elements(json_data -> 'registeredGroupContacts')
      loop
        insert into private.registeredgroupcontacts(gconid, contid)
        values (cast(j_group ->> 'gconid' as int), cast(j_group ->> 'contid' as int));
      end loop;
  end if;
  if json_data ->> 'receipts' is not null then
    for j_receipt in select * from json_array_elements(json_data -> 'receipts')
      loop
        insert into private.receipts(trankeygen_id, type, aggtype, nettype, pid, gid, cid, did, contid, amount, date_,
                                     elekey, itemized, checkno, description, memo, carryforward, deptkey,
                                     user_description)
        values (cast(j_receipt ->> 'trankeygen_id' as int), cast(j_receipt ->> 'type' as smallint),
                cast(j_receipt ->> 'aggtype' as smallint),
                cast(j_receipt ->> 'nettype' as smallint), cast(j_receipt ->> 'pid' as int),
                cast(j_receipt ->> 'gid' as smallint),
                cast(j_receipt ->> 'cid' as int), cast(j_receipt ->> 'did' as int), cast(j_receipt ->> 'contid' as int),
                cast(j_receipt ->> 'amount' as numeric(16, 2)), to_date(j_receipt ->> 'date_', 'YYYY-MM-DD'),
                cast(j_receipt ->> 'elekey' as smallint), cast(j_receipt ->> 'itemized' as smallint),
                j_receipt ->> 'checkno',
                j_receipt ->> 'description', j_receipt ->> 'memo', cast(j_receipt ->> 'carryforward' as smallint),
                cast(j_receipt ->> 'deptkey' as bigint), j_receipt ->> 'user_description');
      end loop;
  end if;
  if json_data ->> 'auctionItems' is not null then
    for j_auction in select * from json_array_elements(json_data -> 'auctionItems')
      loop
        insert into private.auctionitems(trankeygen_id, pid, itemnumber, itemdescription, marketval, memo)
        values (cast(j_auction ->> 'trankeygen_id' as int), cast(j_auction ->> 'pid' as int),
                j_auction ->> 'itemnumber',
                j_auction ->> 'itemdescription', cast(j_auction ->> 'marketval' as numeric(16, 2)),
                j_auction ->> 'memo');
      end loop;
  end if;
  if json_data ->> 'electionParticipation' is not null then
    for j_election in select * from json_array_elements(json_data -> 'electionParticipation')
      loop
            if cast(j_election ->>'fund_id' as int) is distinct from p_fund_id THEN
                RAISE 'Fund id mismatch detected in backup';
            end if;
        insert into private.election_participation(fund_id, election_code)
        values (cast(j_election ->> 'fund_id' as int), j_election ->> 'election_code');
      end loop;
  end if;
  if json_data ->> 'expenditureEvents' is not null then
    for j_expenditure in select * from json_array_elements(json_data -> 'expenditureEvents')
      loop
        insert into private.expenditureevents(trankeygen_id, contid, bnkid, amount, date_, checkno, itemized, memo,
                                              uses_surplus_funds)
        values (cast(j_expenditure ->> 'trankeygen_id' as int), cast(j_expenditure ->> 'contid' as int),
                cast(j_expenditure ->> 'bnkid' as int),
                cast(j_expenditure ->> 'amount' as numeric(16, 2)), to_date(j_expenditure ->> 'date_', 'YYYY-MM-DD'),
                j_expenditure ->> 'checkno',
                cast(j_expenditure ->> 'itemized' as smallint), j_expenditure ->> 'memo',
                cast(j_expenditure ->> 'uses_surplus_funds' as bool));
      end loop;
  end if;
  if json_data ->> 'limits' is not null then
    for j_limit in select * from json_array_elements(json_data -> 'limits')
      loop
        if cast(j_limit ->>'fund_id' as int) is distinct from p_fund_id THEN
            RAISE 'Fund id mismatch detected in backup';
        end if;
        insert into private.limits(fund_id, type, name, amount, aggbyvoters)
        values (cast(j_limit ->> 'fund_id' as int), j_limit ->> 'type', j_limit ->> 'name',
                cast(j_limit ->> 'amount' as numeric(16, 2)), cast(j_limit ->> 'aggbyvoters' as smallint));
      end loop;
  end if;
  if json_data ->> 'loans' is not null then
    for j_loan in select * from json_array_elements(json_data -> 'loans')
      loop
        insert into private.loans(trankeygen_id, type, cid, did, contid, amount, date_, elekey, interestrate, duedate,
                                  checkno, repaymentschedule, description, memo, carryforward)
        values (cast(j_loan ->> 'trankeygen_id' as int), cast(j_loan ->> 'type' as smallint),
                cast(j_loan ->> 'cid' as int),
                cast(j_loan ->> 'did' as int), cast(j_loan ->> 'contid' as int),
                cast(j_loan ->> 'amount' as numeric(16, 2)), to_date(j_loan ->> 'date_', 'YYYY-MM-DD'),
                cast(j_loan ->> 'elekey' as smallint), cast(j_loan ->> 'interestrate' as numeric(16, 2)),
                to_date(j_loan ->> 'duedate', 'YYYY-MM-DD'),
                j_loan ->> 'checkno', j_loan ->> 'repaymentschedule', j_loan ->> 'description',
                j_loan ->> 'memo', cast(j_loan ->> 'carryforward' as smallint));
      end loop;
  end if;
  if json_data ->> 'reportingObligations' is not null then
    for j_reporting in select * from json_array_elements(json_data -> 'reportingObligations')
      loop
        if cast(j_reporting ->>'fund_id' as int) is distinct from p_fund_id THEN
            RAISE 'Fund id mismatch detected in backup';
        end if;
        insert into private.reporting_obligation(obligation_id, fund_id, reporting_period_id, startdate, enddate,
                                                 duedate)
        values (cast(j_reporting ->> 'obligation_id' as int), cast(j_reporting ->> 'fund_id' as int),
                cast(j_reporting ->> 'reporting_period_id' as int),
                to_date(j_reporting ->> 'startdate', 'YYYY-MM-DD'), to_date(j_reporting ->> 'enddate', 'YYYY-MM-DD'),
                to_date(j_reporting ->> 'duedate', 'YYYY-MM-DD'));
      end loop;
  end if;
  if json_data ->> 'depositEvents' is not null then
    for j_deposit_event in select * from json_array_elements(json_data -> 'depositEvents')
      loop
        insert into private.depositevents(trankeygen_id, account_id, amount, date_, deptkey, memo)
        values (cast(j_deposit_event ->> 'trankeygen_id' as int), cast(j_deposit_event ->> 'account_id' as int),
                cast(j_deposit_event ->> 'amount' as numeric(16, 2)),
                to_date(j_deposit_event ->> 'date_', 'YYYY-MM-DD'),
                cast(j_deposit_event ->> 'deptkey' as bigint), j_deposit_event ->> 'memo');
      end loop;
  end if;
  if json_data ->> 'depositItems' is not null then
    for j_deposit in select * from json_array_elements(json_data -> 'depositItems')
      loop
        insert into private.deposititems(deptid, rcptid)
        values (cast(j_deposit ->> 'deptid' as int), cast(j_deposit ->> 'rcptid' as int));
      end loop;
  end if;
  if json_data ->> 'attachedTextPages' is not null then
    for j_attached in select * from json_array_elements(json_data -> 'attachedTextPages')
      loop
        if cast(j_attached ->>'fund_id' as int) is distinct from p_fund_id THEN
            RAISE 'Fund id mismatch detected in backup';
        end if;
        insert into private.attachedtextpages(fund_id, target_id, target_type, text_data)
        values (cast(j_attached ->> 'fund_id' as int), cast(j_attached ->> 'target_id' as int),
                j_attached ->> 'target_type', j_attached ->> 'text_data');
      end loop;
  end if;
  if json_data ->> 'properties' is not null then
    for j_property in select * from json_array_elements(json_data -> 'properties')
      loop
        if cast(j_property ->>'fund_id' as int) is distinct from p_fund_id THEN
            RAISE 'Fund id mismatch detected in backup';
        end if;
        insert into private.properties(fund_id, name, value)
        values (cast(j_property ->> 'fund_id' as int),
                j_property ->> 'name', j_property ->> 'value');
      end loop;
  end if;
END
$$ LANGUAGE plpgsql;