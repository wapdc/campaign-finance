CREATE OR REPLACE FUNCTION private.cf_upgrade_convert_old_cc_vendor_debt(p_fund_id integer) returns VOID
   language plpgsql as
    $$

    DECLARE v_old_CC_record record;
    DECLARE v_cc_id integer;

    DECLARE v_old_debt_record record;
    DECLARE v_last_tran integer;
    DECLARE v_expenditure_id integer;
    DECLARE v_expenditure_receipt_id integer;

    BEGIN
        -- Gather all credit card debt from fund
        FOR v_old_CC_record IN SELECT * from private.vreceipts vr where vr.fund_id = p_fund_id and vr.type = 21 -- credit card debt
            LOOP
                -- create new expenditureevent record based on the values of the old cc record
                v_cc_id = private.cf_ensure_trankeygen(p_fund_id, null);
                INSERT INTO private.expenditureevents (
                    trankeygen_id,
                    contid,
                    bnkid,
                    amount,
                    date_,
                    checkno,
                    itemized,
                    memo
                )
                values (v_cc_id,
                        v_old_CC_record.contid,
                        v_old_CC_record.did,
                        v_old_CC_record.amount,
                        v_old_CC_record.date_,
                        v_old_CC_record.checkno,
                        v_old_CC_record.itemized,
                        v_old_CC_record.memo
                       );


                -- Update the receipt record to be the new type
                UPDATE private.receipts set pid = v_cc_id, type = 28 -- Expenditure
                WHERE trankeygen_id = v_old_CC_record.trankeygen_id;

                UPDATE private.trankeygen t set pid = v_cc_id, rds_updated = now()
                WHERE t.fund_id = p_fund_id and t.trankeygen_id = v_old_CC_record.trankeygen_id;

                -- We dont need to sync anymore since they are upgrading to 1.500 ORCA
            end loop;

        -- Gather all old debt records from supplied fund_id

        v_last_tran = 0;
        v_expenditure_id = 0;

        FOR v_old_debt_record IN SELECT r.trankeygen_id,
                                        r.contid as expContid,
                                        r.did,
                                        r.cid,
                                        r.date_ as expDate,
                                        r.itemized,
                                        r.elekey,
                                        r.carryforward,
                                        r.deptkey,
                                        r.memo as expMemo,
                                        r.amount as totalAmount,
                                        coalesce(r.description, dob.descr) as description,
                                        dob.debtobligation_id,
                                        dob.contid,
                                        dob.amount,
                                        dob.date_,
                                        dob.descr,
                                        dob.memo,
                                        c.type as contactType
                                 FROM private.vreceipts r
                                          JOIN private.debtobligation dob on dob.pid = r.trankeygen_id
                                          JOIN private.contacts c on dob.contid = c.trankeygen_id
                                 WHERE r.fund_id = p_fund_id and r.type = 19
                                 ORDER BY r.trankeygen_id, dob.debtobligation_id
            LOOP
                if v_old_debt_record.trankeygen_id != v_last_tran THEN
                    if v_last_tran > 0 THEN
                        DELETE FROM private.receipts WHERE trankeygen_id = v_last_tran;
                    end if;

                    v_last_tran = v_old_debt_record.trankeygen_id;
                    v_expenditure_id = private.cf_ensure_trankeygen(p_fund_id, null);

                    -- Insert Expenditure event for debt
                    INSERT INTO private.expenditureevents (
                        trankeygen_id,
                        contid,
                        bnkid,
                        amount,
                        date_,
                        checkno,
                        itemized,
                        memo)
                    VALUES (
                               v_expenditure_id,
                               v_old_debt_record.expContid,
                               v_old_debt_record.did,
                               v_old_debt_record.totalAmount,
                               v_old_debt_record.expDate,
                               null,
                               v_old_debt_record.itemized,
                               v_old_debt_record.expMemo
                           );

                    -- Make sure account are updated correctly with pid
                    UPDATE private.trankeygen t set pid = v_expenditure_id, derby_updated = current_timestamp
                    where t.fund_id = p_fund_id and t.trankeygen_id = v_old_debt_record.did;

                    UPDATE private.accounts a set pid = v_expenditure_id where a.trankeygen_id = v_old_debt_record.did;

                end if;

                --Make receipt of expenditure event
                v_expenditure_receipt_id = private.cf_ensure_trankeygen(p_fund_id, null);
                INSERT INTO private.receipts (
                    trankeygen_id,
                    type,
                    aggtype,
                    nettype,
                    pid,
                    cid,
                    did,
                    contid,
                    amount,
                    date_,
                    elekey,
                    itemized,
                    user_description,
                    description,
                    memo,
                    carryforward,
                    deptkey
                )
                VALUES (
                           v_expenditure_receipt_id,
                           28,
                           0,
                           0,
                           v_expenditure_id,
                           v_old_debt_record.cid,
                           v_old_debt_record.did,
                           v_old_debt_record.contid,
                           v_old_debt_record.amount,
                           v_old_debt_record.date_,
                           v_old_debt_record.elekey,
                           v_old_debt_record.itemized,
                           v_old_debt_record.description,
                           v_old_debt_record.description,
                           v_old_debt_record.memo,
                           v_old_debt_record.carryforward,
                           v_old_debt_record.deptkey
                       );


                -- conditionally update contact type
                IF v_old_debt_record.contactType = 'HDN' THEN
                    UPDATE private.contacts SET type = 'BUS' WHERE trankeygen_id = v_old_debt_record.contid;
                    UPDATE private.trankeygen SET derby_updated = current_timestamp WHERE trankeygen_id = v_old_debt_record.contid;
                end if;

                -- Update trankeygen table to point expenditure id for debt forgiven and payments
                UPDATE private.trankeygen t SET pid = v_expenditure_id, derby_updated = current_timestamp
                WHERE t.fund_id = p_fund_id
                  and t.pid = v_old_debt_record.debtobligation_id
                  and t.trankeygen_id in (select trankeygen_id from private.vreceipts vr where vr.fund_id = p_fund_id and vr.type in (44, 29));

                -- Update receipt table debt forgiven and debt payments pid
                UPDATE private.receipts r set pid = v_expenditure_id, contid = v_old_debt_record.expContid
                where r.pid = v_old_debt_record.debtobligation_id and r.type in (44, 29);

                -- Update receipts debt adjustments
                UPDATE private.receipts r set pid = v_expenditure_receipt_id, contid = v_old_debt_record.expContid where r.pid = v_old_debt_record.debtobligation_id and type = 20;

                -- Update trankeygen pointers for debt adjustments
                UPDATE private.trankeygen t set pid = v_expenditure_receipt_id, derby_updated = current_timestamp
                where t.pid = v_old_debt_record.debtobligation_id
                  and t.trankeygen_id in (select trankeygen_id from private.vreceipts vr where vr.fund_id = p_fund_id and vr.type = 20);

                -- delete debt obligation record
                DELETE from private.debtobligation where debtobligation_id = v_old_debt_record.debtobligation_id;
            end loop;

        -- Delete final entry
        if v_last_tran > 0 THEN
            DELETE from private.receipts where trankeygen_id = v_last_tran;
        end if;
    END;
    $$