CREATE OR REPLACE FUNCTION private.cf_backup_campaign(p_fund_id INTEGER) RETURNS json AS
$$
DECLARE
  j_accounts                  json;
  j_attached_text_pages       json;
  j_auction_items             json;
  j_campaign_fund             json;
  j_contacts                  json;
  j_couple_contacts           json;
  j_deposit_items             json;
  j_deposit_events            json;
  j_election_participation    json;
  j_expenditure_events        json;
  j_individual_contact        json;
  j_limits                    json;
  j_loans                     json;
  j_properties                json;
  j_receipts                  json;
  j_registered_group_contacts json;
  j_reporting_obligation      json;
  j_trankeygen                json;
  j_campaign                  json;

BEGIN
  SELECT json_agg(j order by j.trankeygen_id)
  INTO j_accounts
  FROM (select a.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.accounts a
               join private.trankeygen t on a.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by target_id)
  INTO j_attached_text_pages
  FROM (select a.*
        FROM private.attachedtextpages a
        where a.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_auction_items
  FROM (select a.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.auctionitems a
               join private.trankeygen t on a.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j)
  INTO j_campaign_fund
  FROM (select c.*
        FROM private.campaign_fund c
        where c.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_contacts
  FROM (select c.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.contacts c
               join private.trankeygen t on c.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_couple_contacts
  FROM (select c.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.couplecontacts c
               join private.trankeygen t on c.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by deptid, rcptid)
  INTO j_deposit_items
  FROM (select d.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.deposititems d
               join private.trankeygen t on d.deptid = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_deposit_events
  FROM (select d.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.depositevents d
               join private.trankeygen t on d.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by election_code)
  INTO j_election_participation
  FROM (select e.*
        FROM private.election_participation e
        where e.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_expenditure_events
  FROM (select e.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.expenditureevents e
               join private.trankeygen t on e.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_individual_contact
  FROM (select c.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.individualcontacts c
               join private.trankeygen t on c.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by name)
  INTO j_limits
  FROM (select l.*
        FROM private.limits l
        where l.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_loans
  FROM (select l.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.loans l
               join private.trankeygen t on l.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by name)
  INTO j_properties
  FROM (select p.*
        FROM private.properties p
        where p.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_receipts
  FROM (select r.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.receipts r
               join private.trankeygen t on r.trankeygen_id = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by gconid)
  INTO j_registered_group_contacts
  FROM (select c.*,
               t.orca_id,
               t.pid as trankeygen_pid,
               t.rds_updated
        FROM private.registeredgroupcontacts c
               join private.trankeygen t on c.contid = t.trankeygen_id
        where t.fund_id = p_fund_id) j;

  SELECT json_agg(j order by obligation_id)
  INTO j_reporting_obligation
  FROM (select r.*
        FROM private.reporting_obligation r
        where r.fund_id = p_fund_id) j;

  SELECT json_agg(j order by trankeygen_id)
  INTO j_trankeygen
  FROM (select t.*
        FROM private.trankeygen t
        where t.fund_id = p_fund_id) j;

  select row_to_json(j)
  from (SELECT j_accounts                  as "accounts",
               j_attached_text_pages       as "attachedTextPages",
               j_auction_items             as "auctionItems",
               j_campaign_fund             as "campaignFund",
               j_contacts                  as "contacts",
               j_couple_contacts           as "coupleContacts",
               j_deposit_items             as "depositItems",
               j_deposit_events            as "depositEvents",
               j_election_participation    as "electionParticipation",
               j_expenditure_events        as "expenditureEvents",
               j_individual_contact        as "individualContacts",
               j_limits                    as "limits",
               j_loans                     as "loans",
               j_properties                as "properties",
               j_receipts                  as "receipts",
               j_registered_group_contacts as "registeredGroupContacts",
               j_reporting_obligation      as "reportingObligations",
               j_trankeygen                as "trankeygen") j
  INTO j_campaign;

  RETURN j_campaign;
END
$$ LANGUAGE plpgsql STABLE