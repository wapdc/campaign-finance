CREATE OR REPLACE function private.cf_upgrade_campaign(p_fund_id INTEGER) returns void language plpgsql as $$
  declare
    v_fund_id INTEGER;
    v_count INTEGER;
    v_version NUMERIC(4,3);
  begin
    SELECT f.fund_id, count(COALESCE(p.fund_id, cf.fund_id)), max(coalesce(cf.version, 1.42)) into v_fund_id, v_count, v_version FROM fund f
      left join private.properties p on p.fund_id= f.fund_id
      left join private.campaign_fund cf ON cf.fund_id=f.fund_id
    WHERE f.fund_id=p_fund_id
    GROUP BY f.fund_id;

    IF v_fund_id is null THEN
        raise 'Fund not found: %', p_fund_id;
    end if;
    IF v_count is null THEN
        raise 'No ORCA Campaign found for fund: %', p_fund_id;
    end if;

    -- Upgrade the version on the campaign.
    IF v_version < 1.490 or null THEN
        perform private.cf_upgrade_1400_campaign(p_fund_id, v_version);
    end if;

    INSERT INTO private.campaign_fund(FUND_ID, VERSION) values (p_fund_id, 1.500)
      ON CONFLICT(fund_id) DO UPDATE SET version = excluded.version;

    UPDATE public.fund SET vendor = 'PDC' where fund_id = p_fund_id;

    DELETE FROM private.properties WHERE fund_id=p_fund_id and name='VNUM';
  end
$$;