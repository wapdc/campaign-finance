DROP function if exists private.cf_push_changes(fund_id INTEGER, json_data TEXT);
CREATE OR REPLACE FUNCTION private.cf_push_changes(p_fund_id INTEGER, json_data TEXT, p_version numeric default null)
  RETURNS text as $$
DECLARE
  j json;
  v_last_account text;
  v_last_contact text;
  v_last_expenditure text;
  v_last_receipt text;
  v_last_loan text;
  v_last_au text;
  v_last_debt text;
  v_last_deposit text;
  v_last_deletion text;

BEGIN
    j := cast(json_data as json);
    IF j->'properties' IS NOT NULL THEN
      EXECUTE private.cf_save_properties(p_fund_id, j->'properties');
    END IF;
    IF p_version IS NOT NULL THEN
        INSERT INTO private.properties(fund_id, name, value)
        VALUES (p_fund_id, 'VNUM', trim(to_char(p_version, '9D000')))
        ON CONFLICT(fund_id, name) DO UPDATE set value=EXCLUDED.value;
        insert into private.campaign_fund(fund_id, version) values(p_fund_id, p_version)
          on conflict(fund_id) do update set version = excluded.version;
    end if;
    v_last_account := private.cf_sync_accounts(p_fund_id, j->'accounts');
    v_last_contact := private.cf_sync_contacts(p_fund_id, j->'contacts');
    v_last_expenditure := private.cf_sync_expenditureevents(p_fund_id, j->'expenditureEvents');
    v_last_debt := private.cf_sync_debt_obligations(p_fund_id, j->'debtObligations');
    v_last_au := private.cf_sync_auction_items(p_fund_id, j->'auctionItems');
    v_last_receipt := private.cf_sync_receipts(p_fund_id, j->'receipts', p_version);
    v_last_loan := private.cf_sync_loans(p_fund_id, j->'loans');
    v_last_deposit := private.cf_sync_deposit_events(p_fund_id, j->'depositEvents');
    v_last_deletion := private.cf_do_deletions(p_fund_id, j->'deletions');

    -- update accounts based on version
    update private.accounts set total =
                            case when COALESCE(p_version, 1.420) >= 1.470 then private.cf_get_account_total(p_fund_id, trankeygen_id)
                                 else orca_total end
    where orca_total <> total
      and trankeygen_id in (select trankeygen_id from wapdc.private.vaccounts where fund_id = p_fund_id);
    return greatest(v_last_account, v_last_contact, v_last_expenditure, v_last_receipt, v_last_loan, v_last_au, v_last_debt, v_last_deposit, v_last_deletion);
END
$$ LANGUAGE plpgsql;