CREATE OR REPLACE FUNCTION private.cf_upgrade_1400_campaign(p_fund_id integer, p_version numeric(4,2)) returns VOID
    language plpgsql as
    $$
    DECLARE AccountIds INTEGER[];

    BEGIN

        -- DBU14.sql --
        -- Insert new account types for campaign

        if p_version < 1.430 OR NULL THEN
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5006, 'B-DIGI', 'Digital advertising');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5275, 'MILES', 'Mileage reimbursement');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5007, 'B-RADIO', 'Radio advertising');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5008, 'B-ROBO', 'Robocalls');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5031, 'C-GEN', 'Transfer to general fund');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5032, 'C-SURPLUS', 'Transfer to surplus funds account');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5305, 'LE', 'Payment for candidate''s lost earnings');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5306, 'LE-SURPLUS', 'Payment for candidate''s lost earnings');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5330, 'D-GA', 'Design/graphic art, etc.');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5175, 'OS-E', 'Computers, tablets printers,software, phones, etc');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5009, 'O', 'Other advertising');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5033, 'CHARITY-S', 'Disposal of surplus funds to charity');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5034, 'C-PARTY', 'Transfer to political party or legislative caucus committee');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5035, 'C-NEW', 'Transfer to new campaign');
            insert into private.accounts (trankeygen_id, pid, acctnum, code, description) values (private.cf_ensure_trankeygen(p_fund_id, null), 0, 5340, 'B-MERCH', 'Campaign merchandise/paraphernalia');
        end if;

        -- Add expense for existing accounts
        update private.accounts set CODE = 'G-BANK'     where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Bank Charges & Adjustments') AND CODE IS NULL;
        update private.accounts set CODE = 'B'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Broadcast Advertising') AND CODE IS NULL;
        update private.accounts set CODE = 'F'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Catering, Preparation') AND CODE IS NULL;
        update private.accounts set CODE = 'C-CHARITY'  where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Charity') AND CODE IS NULL;
        update private.accounts set CODE = 'C'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Monetary Contributions') AND CODE IS NULL;
        update private.accounts set CODE = 'G'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Electricity') AND CODE IS NULL;
        update private.accounts set CODE = 'W'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Employee Services') AND CODE IS NULL;
        update private.accounts set CODE = 'F'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Entertainment') AND CODE IS NULL;
        update private.accounts set CODE = 'FEE'        where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Filing Fees') AND CODE IS NULL;
        update private.accounts set CODE = 'G'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='General Operation and Overhead') AND CODE IS NULL;
        update private.accounts set CODE = 'G'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Insurance') AND CODE IS NULL;
        update private.accounts set CODE = 'ALRC'       where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Legal Expense') AND CODE IS NULL;
        update private.accounts set CODE = 'G'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Maintenance & Repairs') AND CODE IS NULL;
        update private.accounts set CODE = 'M'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Management/Consulting Services') AND CODE IS NULL;
        update private.accounts set CODE = 'N'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Newspaper/Periodical Advertise') AND CODE IS NULL;
        update private.accounts set CODE = 'OS'         where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Office Supplies') AND CODE IS NULL;
        update private.accounts set CODE = 'OS'         where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Other Consumables') AND CODE IS NULL;
        update private.accounts set CODE = 'W'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Payroll Taxes') AND CODE IS NULL;
        update private.accounts set CODE = 'P'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Postage/Mailing Permits') AND CODE IS NULL;
        update private.accounts set CODE = 'L'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Printing') AND CODE IS NULL;
        update private.accounts set CODE = 'ALRC'       where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Professional Services') AND CODE IS NULL;
        update private.accounts set CODE = 'G-RENT'     where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Rent') AND CODE IS NULL;
        update private.accounts set CODE = 'G'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Subscriptions') AND CODE IS NULL;
        update private.accounts set CODE = 'S'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Surveys and Polls') AND CODE IS NULL;
        update private.accounts set CODE = 'G'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Taxes') AND CODE IS NULL;
        update private.accounts set CODE = 'G'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Telephone Expense') AND CODE IS NULL;
        update private.accounts set CODE = 'T'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Travel, Accommodations, Meals') AND CODE IS NULL;
        update private.accounts set CODE = 'V'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Voter Signature Gathering') AND CODE IS NULL;
        update private.accounts set CODE = 'W'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Wages, Salaries, Benefits') AND CODE IS NULL;
        update private.accounts set CODE = 'L-SIGNS'    where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Yard Signs, Buttons, etc.') AND CODE IS NULL;
        update private.accounts set CODE = 'C-CHARITY'  where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Community Organizations') AND CODE IS NULL;
        update private.accounts set CODE = 'C'          where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and type='HDN' and NAME='Contrib. to Other Committees') AND CODE IS NULL;


        --rename existing accounts
        select array_agg(va.trankeygen_id) into AccountIds from private.vaccounts va where va.fund_id = p_fund_id;

        update private.contacts c set name = 'Broadcast/cable TV advertising'                 WHERE name = 'Broadcast Advertising'          AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Management and consulting services'             WHERE name = 'Management/Consulting Services' AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Newspaper/periodical advertising'               WHERE name = 'Newspaper/Periodical Advertise' AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Postage, mail permits, stamps, etc.'            WHERE name = 'Postage/Mailing Permits'        AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Travel, accommodation, meals'                   WHERE name = 'Travel, Accommodations, Meals'  AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Office supplies, furniture, staff food, etc.'   WHERE name = 'Office Supplies'                AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Charity/community organization'                 WHERE name = 'Charity'                        AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Rent, lease, mortgage, PO box rental'           WHERE name = 'Rent'                           AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Surveys, polling, research'                     WHERE name = 'Surveys and Polls'              AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Wages, salaries, benefits, payroll taxes'       WHERE name = 'Wages, Salaries, Benefits'      AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Fundraising'                                    WHERE name = 'Catering, Preparation'          AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Monetary contributions to PAC or candidate'     WHERE name = 'Contrib. to Other Committees'   AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Printing brochures, literature, postcards'      WHERE name = 'Printing'                       AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Accounting, legal, regulatory compliance, etc.' WHERE name = 'Professional Services'          AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Printing signs'                                 WHERE name = 'Yard Signs, Buttons, etc.'      AND trankeygen_id = ANY(AccountIds);
        update private.contacts c set name = 'Signature gathering'                            WHERE name = 'Voter Signature Gathering'      AND trankeygen_id = ANY(AccountIds);


        --DBU 15
        update private.accounts a set code = 'D-GA' WHERE a.code = 'D_GA' and a.trankeygen_id = ANY(AccountIds);
--
--         --DBU 16,17,18 are not necessary as the are only forcing re-syncs
--         --DBU 16 update that set current time stamp for derby_updated for all contacts for which pagg and gagg are > 0
--         --DBU 17 Make sure to update all deposit events so that the trankeygen ids for those records get stored.
--         --DBU 18 Force resync of group contacts so that we get the data
--
--         --DBU 19
         update private.accounts set CODE = 'CHARITY-S' where CONTID in (select c.trankeygen_id from private.contacts c join private.trankeygen t on c.trankeygen_id = t.trankeygen_id where t.fund_id = p_fund_id and c.type='HDN' and c.NAME='Disposal of surplus funds to charity') AND CODE = 'C-CHARITY';
         update private.contacts c set name = 'Transfer to new campaign' where name = 'Transfer to New Campaign' and trankeygen_id = ANY(accountIds);
         update private.contacts c set name = 'Mileage reimbursement' where name = 'Milage reimburstment' and trankeygen_id = ANY(AccountIds);

        --DBU 20
        perform private.cf_upgrade_convert_old_cc_vendor_debt(p_fund_id);



    END;
    $$