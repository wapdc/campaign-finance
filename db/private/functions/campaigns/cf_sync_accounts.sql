--DROP function private.cf_sync_accounts(p_fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_sync_accounts(p_fund_id INTEGER, json_data json)
  RETURNS text as $$
DECLARE
  highest_updated timestamp;
  c record;
  v_trankeygen INTEGER;
  v_parent_id INTEGER;
  v_cont_id INTEGER;
BEGIN
  -- Loop through array of accounts
  FOR c IN
    select j.*,
           case when a.trankeygen_id is null and a2.trankeygen_id is null then true end as new_record
    from (select
            cast(value->>'orca_id' as int) as orca_id,
            cast(value->>'pid' as int) as pid,
            cast(value->>'rds_pid' as int) as rds_pid,
            cast(value->>'acctnum' as int) as acctnum,
            cast(value->>'contid' as int) as contid,
            cast(value->>'rds_id' as int) as rds_id,
            value->>'code' as code,
            value->>'description' as description,
            cast(value->>'style' as int) as style,
            cast(value->>'total' as numeric) as total,
            value->>'memo' as memo,
            cast(value->>'date_' as date) as date_,
            to_timestamp(value->>'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated

          from json_array_elements(json_data) d ) j
           left join private.trankeygen o on o.orca_id = j.orca_id and o.fund_id=p_fund_id
           left join private.accounts a ON a.trankeygen_id = o.trankeygen_id
           left join private.trankeygen o2 on o2.fund_id = p_fund_id and o2.trankeygen_id = j.rds_id
           left join private.accounts a2 ON o2.trankeygen_id=a2.trankeygen_id
        ORDER BY J.derby_updated, j.orca_id
    LOOP

      v_trankeygen := private.cf_ensure_trankeygen(p_fund_id, c.rds_id, c.orca_id);
      v_parent_id := private.cf_ensure_trankeygen(p_fund_id, c.rds_pid, c.pid);
      v_cont_id := private.cf_ensure_trankeygen(p_fund_id, NULL, c.contid);
      UPDATE private.trankeygen set derby_updated = c.derby_updated, pid=v_parent_id where trankeygen_id = v_trankeygen;

      IF c.derby_updated > highest_updated OR highest_updated is null THEN highest_updated := c.derby_updated; END IF;
      IF c.new_record THEN
        insert into private.accounts(trankeygen_id,
                                  pid,
                                  acctnum,
                                  contid,
                                  code,
                                  description,
                                  style,
                                  total,
                                  memo,
                                  date_,
                                  orca_total
                                  )
        values(
                v_trankeygen,
                v_parent_id,
                c.acctnum,
                coalesce(v_cont_id, v_trankeygen),
                c.code,
                c.description,
                c.style,
                c.total,
                c.memo,
                c.date_,
                c.total
                );
      ELSE
        UPDATE private.accounts set
                               pid=v_parent_id,
                               acctnum=c.acctnum,
                               contid=coalesce(v_cont_id, v_trankeygen),
                               code=c.code,
                               description=c.description,
                               style=c.style,
                               orca_total=c.total,
                               memo= c.memo,
                               date_ = c.date_
        WHERE trankeygen_id=v_trankeygen;
      END IF;

    END LOOP;
  RETURN cast (highest_updated as text);
END
$$ LANGUAGE plpgsql;
