CREATE OR REPLACE FUNCTION private.cf_create_campaign(c_fund_id integer, json_parameters json) RETURNS void AS
$$
DECLARE
  account_group record;
  account_trankeygen integer;
  campaign_treasurer record;
  committee_record record;
  committee_contact_info record;
  bank_name text;
  is_candidate bool;
  seec_filer text;
BEGIN
insert into private.campaign_fund(fund_id, version) values (c_fund_id, '1.501');
for account_group in (select * from private.account_groups where initial_account order by acctnum)
  loop
    insert into private.trankeygen(fund_id) values (c_fund_id) returning trankeygen_id into account_trankeygen;
    insert into private.contacts(trankeygen_id, type, name) values (account_trankeygen, 'HDN', account_group.title);
    insert into private.accounts(trankeygen_id, contid, acctnum, code, style)
      values (account_trankeygen,
              account_trankeygen,
              account_group.acctnum,
              account_group.category_code,
              case when account_group.acctnum = 0 then 0
                when account_group.acctnum = 4000 then 2
                when account_group.acctnum = 1800 then 1
                when account_group.negate = 1 then 2
                else 1 end);
end loop;

--set properties
select * from committee where committee_id = (select committee_id from fund where fund_id = c_fund_id) into committee_record;
select * from committee_contact where committee_id = committee_record.committee_id and role = 'committee' into committee_contact_info;
select * from (select *, ROW_NUMBER() OVER (PARTITION by committee_id ORDER BY ministerial desc, contact_id) rownum
      FROM committee_contact
      WHERE treasurer = true) tc
where tc.committee_id = committee_record.committee_id and tc.rownum = 1 into campaign_treasurer;
select case when substr(user_data, 1, 1) = '{' THEN cast(user_data as json)->'local_filing'->'seec'->>'required' END from registration where registration_id = committee_record.registration_id into seec_filer;
insert into private.properties(fund_id, name, value) values (c_fund_id, 'TREASURERINFO:FIRSTNAME', campaign_treasurer.name);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'TREASURERINFO:PHONE', campaign_treasurer.phone);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'DATE', (select election_code from fund where fund_id = c_fund_id));
insert into private.properties(fund_id, name, value) values (c_fund_id, 'CAMPAIGNINFO:STARTDATE', case when (json_parameters->'campaignStartDate') is null then '01/01/'||(select election_code::text from fund where fund_id = c_fund_id) else to_char(date(json_parameters->>'campaignStartDate'), 'mm/dd/yyyy') end);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'COMMITTEEINFO:NAME', committee_record.name);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'COMMITTEEINFO:FILERID', committee_record.filer_id);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'COMMITTEEINFO:ADDRESS', committee_contact_info.address);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'COMMITTEEINFO:CITY', committee_contact_info.city);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'COMMITTEEINFO:STATE', committee_contact_info.state);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'COMMITTEEINFO:ZIP', committee_contact_info.postcode);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'COMMITTEEINFO:PHONE1', committee_contact_info.phone);
insert into private.properties(fund_id, name, value) values (c_fund_id, 'COMMITTEEINFO:EMAIL', committee_contact_info.email);
if (cast(seec_filer as bool)) then
    insert into private.properties(fund_id, name, value) values (c_fund_id, 'SEEC_FILER', seec_filer);
end if;


--set up bank (do I need to add account?)
select name from committee_contact WHERE role = 'bank' and committee_id = committee_record.committee_id into bank_name;
if bank_name is not null then
    insert into private.trankeygen(fund_id) values (c_fund_id) returning trankeygen_id into account_trankeygen;
    insert into private.contacts(trankeygen_id, type, name, contact_key) values (account_trankeygen, 'FI', bank_name, account_trankeygen);
end if;

--set candidacy related stuff
select pac_type = 'candidate' from committee c where c.committee_id = committee_record.committee_id into is_candidate;
if is_candidate then
  insert into private.trankeygen(fund_id) values (c_fund_id) returning trankeygen_id into account_trankeygen;
  insert into private.contacts(trankeygen_id, type, name)
  values (account_trankeygen, 'CAN',
          COALESCE((select name from committee_contact c where c.committee_id = committee_record.committee_id and role = 'candidacy'),
            (select p.name from committee c join person p on c.person_id = p.person_id where c.committee_id = committee_record.committee_id)));
end if;

--set election participation if provided
if json_parameters->>'election_codes' is not null and json_parameters->'election_codes'->> 0 is not null then
  perform private.cf_save_election_codes(c_fund_id, json_parameters);
elseif is_candidate then
  insert into private.election_participation (fund_id, election_code) values (c_fund_id, committee_record.election_code);
end if;

perform private.cf_populate_reporting_obligations(c_fund_id);

--add carry forward receipt if carry forward amount provided
if json_parameters->>'carryForwardAmount' is not null then
  perform private.cf_update_carry_forward(c_fund_id, (json_parameters->>'carryForwardAmount')::int::numeric, null, (select trankeygen_id from private.vaccounts where acctnum = 1000 and fund_id = c_fund_id));
end if;

IF json_parameters->>'import' != 'newCampaign' THEN
    IF json_parameters->>'import' = 'importData' THEN PERFORM private.cf_import_prior_campaign_data(c_fund_id, (json_parameters->>'importFund')::int);
    ELSIF json_parameters->>'import' = 'importContacts' THEN PERFORM private.cf_import_prior_campaign_contacts(c_fund_id, (json_parameters->>'importFund')::int);
    END IF;
END IF;

end
$$ LANGUAGE plpgsql;