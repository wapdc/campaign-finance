CREATE OR REPLACE FUNCTION private.cf_import_prior_campaign_contacts(n_fund_id INTEGER, o_fund_id INTEGER)
    RETURNS void as $$
DECLARE
    o_contact record;
    v_contact_id integer;
    v_couple_contact_id integer;
    v_couple_contact_record record;
    v_contact_1 record;
    v_contact_2 record;
    v_group_contact_record record;
    v_group_contact_id integer;
    o_candidate record;
    n_candidate_id integer;
    v_couple_contact_1 INTEGER;
    v_couple_contact_2 INTEGER;
    v_couple_1_contact_key text;
    v_couple_2_contact_key text;
    v_committee_type text;

BEGIN
    --loop over import contacts and create new ones
    --create base contact record here for import loans/expense/contributions etc
    FOR o_contact IN SELECT vc.* FROM private.vcontacts vc WHERE vc.fund_id = o_fund_id AND vc.type NOT IN ('CAN', 'GRP')
        LOOP
            v_contact_id = private.cf_ensure_trankeygen(n_fund_id, null);

            INSERT INTO private.contacts (
                trankeygen_id, type, name, street, city, state, zip, phone, email, occupation, employername, employerstreet, employercity, employerstate, employerzip, memo, contact_key
            )
            VALUES (
                       v_contact_id, o_contact.type, o_contact.name, o_contact.street, o_contact.city, o_contact.state, o_contact.zip, o_contact.phone, o_contact.email, o_contact.occupation, o_contact.employername, o_contact.employerstreet, o_contact.employercity, o_contact.employerstate, o_contact.employerzip, o_contact.memo,
                       COALESCE(o_contact.contact_key, o_contact.trankeygen_id::text)
                   );

            IF o_contact.type = 'IND' THEN

                INSERT INTO private.individualcontacts (trankeygen_id, prefix, firstname, middleinitial, lastname, suffix)
                VALUES (
                           v_contact_id, o_contact.prefix, o_contact.firstname, o_contact.middleinitial, o_contact.lastname, o_contact.suffix
                       );
            END IF;

        END LOOP;

    FOR v_couple_contact_record in SELECT vc.* FROM private.vcontacts vc
                                   where vc.fund_id = o_fund_id and vc.type = 'CPL'
        LOOP
            --edge case where there are empty couple contacts
            IF v_couple_contact_record.contact1_id IS NOT NULL OR v_couple_contact_record.contact2_id IS NOT NULL
            THEN
            --grab the new trankeygen_id
            v_couple_contact_id = private.cf_get_contact_from_contact_key(COALESCE(v_couple_contact_record.contact_key, v_couple_contact_record.trankeygen_id::text), n_fund_id);

            -- check if contacts are correctly in the new campaign
            select vc.contact_key INTO v_couple_1_contact_key FROM private.vcontacts vc WHERE vc.fund_id = o_fund_id AND vc.trankeygen_id = v_couple_contact_record.contact1_id;
            select vc.trankeygen_id, vc.contact_key into v_contact_1 from private.vcontacts vc where vc.fund_id = n_fund_id and vc.contact_key = COALESCE(v_couple_1_contact_key, v_couple_contact_record.contact1_id::text);
            v_couple_contact_1 = private.cf_get_contact_from_contact_key(COALESCE(v_contact_1.contact_key, v_contact_1.trankeygen_id::text), n_fund_id);

            select vc.contact_key INTO v_couple_2_contact_key FROM private.vcontacts vc WHERE vc.fund_id = o_fund_id AND vc.trankeygen_id = v_couple_contact_record.contact2_id;
            select vc.trankeygen_id, vc.contact_key into v_contact_2 from private.vcontacts vc where vc.fund_id = n_fund_id and vc.contact_key = COALESCE(v_couple_2_contact_key, v_couple_contact_record.contact2_id::text);
            v_couple_contact_2 = private.cf_get_contact_from_contact_key(COALESCE(v_contact_2.contact_key, v_contact_2.trankeygen_id::text), n_fund_id);

            -- validate contact
            if v_couple_contact_1 is null then
                RAISE 'Missing contact for couples contact creation 1';
            end if;

            if v_couple_contact_2 is null then
                RAISE 'Missing contact for couples contact creation 2';
            end if;

            -- insert couple contact record
            INSERT INTO private.couplecontacts(trankeygen_id, contact1_id, contact2_id)
            values (
                       v_couple_contact_id,
                       v_couple_contact_1,
                       v_couple_contact_2
                   );
            end if;
        end loop;


    -- importing group contacts
    FOR v_group_contact_record in SELECT * FROM private.vcontacts vc where vc.fund_id = o_fund_id and type = 'GRP'
        loop
            -- create new group contact contact of new campaign
            v_group_contact_id = private.cf_ensure_trankeygen(n_fund_id, null);

            -- double check the correct values needed
            INSERT INTO private.contacts (trankeygen_id, type, name, memo, contact_key)
            values (
                       v_group_contact_id,
                       'GRP',
                       v_group_contact_record.name,
                       v_group_contact_record.memo,
                       coalesce(v_group_contact_record.contact_key, v_group_contact_record.trankeygen_id::text)
                   );

            -- get all contacts associated with this group from new campaign
            -- create a record for each contact in the group contact table
            INSERT INTO private.registeredgroupcontacts (gconid, contid)
            SELECT v_group_contact_id, vc.trankeygen_id from private.vcontacts vc
            where vc.fund_id = n_fund_id
              and vc.contact_key in (select COALESCE(vc3.contact_key, vc3.trankeygen_id::text ) FROM private.vcontacts vc2
                                        JOIN private.registeredgroupcontacts rgc ON rgc.contid = vc2.trankeygen_id AND vc2.fund_id = o_fund_id
                                        JOIN private.vcontacts vc3 ON vc3.contact_key = COALESCE(vc2.contact_key, vc2.trankeygen_id::text)
                                        where rgc.gconid = v_group_contact_record.trankeygen_id);


        end loop;

    --check source/import committee type
    SELECT c.committee_type INTO v_committee_type FROM public.committee c
         JOIN public.fund f ON f.committee_id = c.committee_id
        WHERE fund_id = o_fund_id;

    IF v_committee_type = 'CA' THEN
        SELECT vc.trankeygen_id, vc.contact_key INTO o_candidate FROM private.vcontacts vc
        WHERE vc.fund_id = o_fund_id and vc.type = 'CAN';
        SELECT vc.trankeygen_id INTO n_candidate_id FROM private.vcontacts vc
        WHERE vc.fund_id = n_fund_id and vc.type = 'CAN';
        --Check if old record exists
        IF o_candidate.trankeygen_id IS NOT NULL THEN
            UPDATE private.contacts
            SET contact_key = COALESCE(o_candidate.contact_key, o_candidate.trankeygen_id::TEXT)
            WHERE trankeygen_id = n_candidate_id;
    end if;
    end if;

END
$$ language plpgsql