CREATE OR REPLACE FUNCTION private.cf_get_orca_campaign_restore(p_fund_id INTEGER) returns JSON as $$
  declare
      r_json JSON;
      j_props JSON;
      j_trankeygen JSON;
      j_accounts JSON;
      j_contacts JSON;
      j_receipts JSON;
      j_expenditure_events JSON;
      j_deposit_events JSON;
      j_debt_obligations JSON;
      j_auction_items JSON;
      j_loans JSON;
      v_orca_id INTEGER;
      t_rec RECORD;
      begin
      -- make sure we have no null ORCA ids in trankeygen table.
      SELECT max(orca_id) into v_orca_id from private.trankeygen where fund_id=p_fund_id;

      -- Populate any missing ORCA ids.
      FOR t_rec IN (SELECT * from private.trankeygen t where t.fund_id=p_fund_id and orca_id is null order by trankeygen_id) LOOP
          v_orca_id := v_orca_id + 1;
          UPDATE private.trankeygen t SET orca_id=v_orca_id WHERE trankeygen_id = t_rec.trankeygen_id;
          end loop;


      -- Properties
      SELECT json_agg(p) INTO j_props FROM (select name,value from private.properties
                                            WHERE fund_id = p_fund_id) p;
      -- Trankeygen
      SELECT json_agg(v order by v.orca_id) INTO j_trankeygen
        FROM (
            select
              t.orca_id,
              COALESCE(tp.orca_id,0) as pid,
              t.derby_updated,
              t.trankeygen_id as rds_id
            from private.trankeygen t left join private.trankeygen tp on tp.trankeygen_id=t.pid
              left join private.receipts r on r.trankeygen_id=t.trankeygen_id
            where t.fund_id = p_fund_id
            ) v;

      -- Accounts
      SELECT json_agg(v order by v.orca_id) into j_accounts FROM (
          select
            a.orca_id,
            COALESCE(tp.orca_id,0) as pid,
            a.acctnum,
            COALESCE(tc.orca_id,0) as contid,
            a.code,
            a.style,
            a.total,
            a.description,
            a.memo,
            a.date_
          from private.vaccounts a
              left join private.trankeygen tp on tp.trankeygen_id=a.pid
              left join private.trankeygen tc on tc.trankeygen_id=a.contid
          where a.fund_id=p_fund_id
      )v;

      -- Contacts
      SELECT json_agg(v order by v.orca_id) INTO j_contacts FROM (
        select
          tc.orca_id,
          c.type,
          c.name,
          c.street,
          c.city,
          c.state,
          c.zip,
          c.phone,
          c.email,
          c.occupation,
          c.employername,
          c.employerstreet,
          c.employercity,
          c.employerstate,
          c.employerzip,
          c.memo,
          c.pagg,
          c.gagg,
          i.firstname,
          i.lastname,
          i.middleinitial,
          i.prefix,
          i.suffix,
          c2.contact1_id,
          c2.contact2_id,
          gm.members
        from private.contacts c
          left join private.trankeygen tc on tc.trankeygen_id = c.trankeygen_id
          left join private.individualcontacts i on c.trankeygen_id = i.trankeygen_id
          left join private.couplecontacts c2 on c.trankeygen_id = c2.trankeygen_id
          left join (
            select gc.gconid, array_agg(tc.orca_id) as members from private.registeredgroupcontacts gc
              join private.trankeygen t on t.trankeygen_id = gc.gconid
              join private.trankeygen tc on tc.trankeygen_id=gc.contid
              where t.fund_id = p_fund_id
              group by gc.gconid
          ) gm on c.type = 'GRP' and gm.gconid = c.trankeygen_id
        where tc.fund_id = p_fund_id and (c.type != 'CPL' or c2.contact1_id is not null)
      )v;

      -- Receipts
      SELECT json_agg(v order by v.orca_id) INTO j_receipts FROM (
        select
          r.orca_id,
          r.type,
          r.aggtype,
          r.nettype,
          COALESCE(r.pid_orca,0) as pid,
          COALESCE(r.gid_orca, -1) as gid,
          r.cid_orca as cid,
          r.did_orca as did,
          COALESCE(r.contid_orca, 0) as contid,
          r.amount,
          r.date_,
          r.elekey,
          r.itemized,
          r.checkno,
          r.description,
          r.memo,
          r.carryforward,
          r.deptkey
        from private.vreceipts r
        where r.fund_id=p_fund_id
      ) v;

      --Expenditure events
      select json_agg(v order by v.orca_id) INTO j_expenditure_events from (
        select
          e.orca_id,
          e.contid_orca as contid,
          e.bnkid_orca as bnkid,
          e.amount,
          e.date_,
          e.checkno,
          e.memo,
          e.itemized
        from private.vexpenditureevents e
        where e.fund_id = p_fund_id
      ) v;

      -- Auction Items
      select json_agg(v order by v.orca_id) into j_auction_items from (
        select
          a.orca_id,
          coalesce(tp.orca_id, -1) as pid,
          a.itemnumber,
          a.itemdescription,
          a.marketval,
          a.memo
        from private.vauctions a
          left join private.trankeygen t on t.trankeygen_id=a.trankeygen_id
          left join private.trankeygen tp on tp.trankeygen_id = t.pid
        where a.fund_id = p_fund_id
      ) v;

      -- Deposit events
      select json_agg(v order by v.orca_id) into j_deposit_events from (
        select
          t.orca_id,
          ta.orca_id as account_id,
          d.amount,
          d.date_,
          d.deptkey,
          d.memo,
          dvi.deposit_items as "depositItems"
        from
        private.vdepositevents d
          join private.trankeygen t on t.trankeygen_id=d.trankeygen_id
          join private.trankeygen ta on ta.trankeygen_id=d.account_id
          join ( select di.deptid, array_agg(ti.orca_id) as deposit_items from private.deposititems di
              join private.trankeygen ti on ti.trankeygen_id=di.rcptid
              group by di.deptid
              ) dvi
           on dvi.deptid=d.trankeygen_id
        where t.fund_id = p_fund_id
      ) v;

      -- Debt Obligations
      SELECT json_agg(v order by v.orca_id) INTO j_debt_obligations FROM (
        SELECT
          d.orca_id,
          tp.orca_id as pid,
          tc.orca_id as contid,
          d.amount,
          d.date_,
          d.descr,
          d.memo
          from private.debtobligation d
            join private.trankeygen tp on tp.trankeygen_id=d.pid
            join private.trankeygen tc on tc.trankeygen_id=d.contid
            where d.pid in (select t.trankeygen_id from private.trankeygen t where t.fund_id = p_fund_id)
     ) v;

      -- Loans
      SELECT json_agg(v order by v.orca_id) INTO j_loans FROM (
         SELECT
           t.orca_id,
           l.type,
           tca.orca_id as cid,
           tcd.orca_id as did,
           tc.orca_id as contid,
           l.amount,
           l.date_,
           l.elekey,
           l.interestrate,
           l.duedate,
           l.checkno,
           l.repaymentschedule,
           l.description,
           l.memo,
           l.carryforward
         from
         private.loans l JOIN private.trankeygen t ON t.trankeygen_id=l.trankeygen_id
           join private.trankeygen tca on tca.trankeygen_id = l.cid
           join private.trankeygen tcd on tcd.trankeygen_id = l.did
           join private.trankeygen tc on tc.trankeygen_id=l.contid
         where t.fund_id = p_fund_id
      ) v;

      -- Final result
      SELECT row_to_json(r) into r_json FROM (
        select
          j_props as "orcaProperties",
          j_trankeygen as trankeygens,
          j_accounts as accounts,
          j_contacts as contacts,
          j_receipts as receipts,
          j_expenditure_events as "expenditureEvents",
          j_deposit_events as "depositEvents",
          j_auction_items as "auctionItems",
          j_debt_obligations as "debtObligations",
          j_loans as loans
      ) r;
      return r_json;
  end;
$$ language plpgsql;