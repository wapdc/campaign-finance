CREATE OR REPLACE FUNCTION private.cf_delete_orca_campaign(p_fund_id INTEGER) RETURNS VOID AS
$$
DECLARE
    BEGIN
    RAISE NOTICE 'Deleting fund id %', p_fund_id;
    DELETE from private.debtobligation where pid in (select trankeygen_id from private.trankeygen where fund_id = p_fund_id);
    DELETE FROM private.deposititems WHERE rcptid in (select trankeygen_id from private.trankeygen where fund_id = p_fund_id );
    DELETE FROM private.deposititems WHERE deptid in (select trankeygen_id from private.trankeygen where fund_id = p_fund_id );
    DELETE FROM private.properties WHERE fund_id = p_fund_id;
    DELETE FROM private.campaign_fund WHERE fund_id = p_fund_id;
    DELETE FROM private.trankeygen WHERE fund_id = p_fund_id;
    DELETE FROM private.ad WHERE fund_id = p_fund_id;
    DELETE FROM private.c4reportingperiods WHERE fund_id = p_fund_id;
    DELETE FROM private.limits WHERE fund_id=p_fund_id;
    DELETE FROM private.log WHERE fund_id = p_fund_id;
    DELETE FROM private.reports WHERE fund_id = p_fund_id;
    DELETE FROM private.submittedreports WHERE fund_id = p_fund_id;
    DELETE FROM private.reporting_obligation WHERE fund_id = p_fund_id;
    DELETE FROM private.election_participation WHERE fund_id = p_fund_id;
    DELETE FROM private.attachedtextpages where fund_id = p_fund_id;
    DELETE FROM private.restore_request where fund_id = p_fund_id;
END
$$ LANGUAGE plpgsql;



