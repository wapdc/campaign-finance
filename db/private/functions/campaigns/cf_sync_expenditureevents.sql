--DROP function private.cf_sync_expenditureevents(p_fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_sync_expenditureevents(p_fund_id INTEGER, json_data json)
    RETURNS text as
$$
DECLARE
    highest_updated timestamp;
    c               record;
    v_trankeygen_id INTEGER;
    v_cont_id INTEGER;
    v_parent_id INTEGER;
    v_bnk_id INTEGER;
BEGIN
    -- Loop through array of expenditures
    FOR c IN
        select j.*,
               case when e.trankeygen_id is null then true end as new_record
        from (select cast(value ->> 'orca_id' as int)                                  as orca_id,
                     cast(value->>'rds_id' as int)                                     as rds_id,
                     cast(value->>'pid' as int)                                          as pid,
                     cast(value ->> 'contid' as int)                                     as contid,
                     cast(value ->> 'bnkid' as int)                                      as bnkid,
                     cast(value ->> 'amount' as numeric)                                 as amount,
                     cast(value ->> 'date_' as date)                                     as date_,
                     value ->> 'checkno'                                                 as checkno,
                     cast(value ->> 'itemized' as int)                                   as itemized,
                     value ->> 'memo'                                                    as memo,
                     to_timestamp(value ->> 'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated
              from json_array_elements(json_data) d) j
                 left join private.trankeygen o on (o.orca_id = j.orca_id OR o.trankeygen_id=j.rds_id) and o.fund_id = p_fund_id
                 left join private.expenditureevents e on e.trankeygen_id = o.trankeygen_id
              ORDER BY derby_updated, orca_id

        LOOP
            v_trankeygen_id = private.cf_ensure_trankeygen(p_fund_id, c.rds_id,c.orca_id);
            v_parent_id = private.cf_ensure_trankeygen(p_fund_id, NULL, c.pid);
            v_cont_id = private.cf_ensure_trankeygen(p_fund_id, NULL,  c.contid);
            v_bnk_id = private.cf_ensure_trankeygen(p_fund_id, NULL, c.bnkid);
            UPDATE private.trankeygen set derby_updated = c.derby_updated, pid=v_parent_id where trankeygen_id = v_trankeygen_id;

            IF c.derby_updated > highest_updated OR highest_updated is null THEN
                highest_updated := c.derby_updated;
            END IF;
                insert into private.expenditureevents(trankeygen_id,
                                                   contid,
                                                   bnkid,
                                                   amount,
                                                   date_,
                                                   checkno,
                                                   itemized,
                                                   memo)
                values (v_trankeygen_id,
                        v_cont_id,
                        v_bnk_id,
                        c.amount,
                        c.date_,
                        c.checkno,
                        c.itemized,
                        c.memo)
                          on conflict (trankeygen_id) do update
                set contid   = excluded.contid,
                    bnkid    = excluded.bnkid,
                    amount   = excluded.amount,
                    date_    = excluded.date_,
                    checkno  = excluded.checkno,
                    itemized = excluded.itemized,
                    memo     = excluded.memo;
        END LOOP;
    RETURN cast(highest_updated as text);
END
$$ LANGUAGE plpgsql;

