DROP FUNCTION IF EXISTS private.cf_update_campaign_vendor(p_fund_id INTEGER, p_vendor JSON);
CREATE OR REPLACE FUNCTION private.cf_update_campaign_vendor(p_fund_id INTEGER, p_vendor text) RETURNS VOID AS
$$
DECLARE
BEGIN
    UPDATE fund
    SET vendor = p_vendor
    WHERE fund_id = p_fund_id;

END
$$ LANGUAGE plpgsql;