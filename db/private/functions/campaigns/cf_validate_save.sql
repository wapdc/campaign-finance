CREATE OR REPLACE FUNCTION private.cf_validate_save(p_fund_id INTEGER, p_save_count INTEGER) returns INTEGER AS
$$
  DECLARE
    v_save_count INTEGER;
    v_request_id INTEGER;
  BEGIN
    -- Check to see if we have a restore request.
    SELECT restore_request_id into v_request_id FROM private.restore_request where fund_id=p_fund_id;

    -- Skip save count validation because we are restoring
    if v_request_id is not null then
        return null;
    end if;
    select cf.save_count into v_save_count from private.campaign_fund cf where cf.fund_id=p_fund_id;
    if coalesce(v_save_count,0) = p_save_count then
      v_save_count := coalesce(v_save_count,0) + 1;
      insert into private.campaign_fund (fund_id, save_count) values (p_fund_id, v_save_count)
        on conflict (fund_id) do update set save_count=excluded.save_count, updated=now();
    else
        v_save_count = 0;
    end if;
    return v_save_count;
  END
$$ language plpgsql volatile ;