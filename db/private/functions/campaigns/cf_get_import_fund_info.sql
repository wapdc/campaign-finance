CREATE OR REPLACE FUNCTION private.cf_get_import_fund_info(import_fund_id INTEGER) returns json as $$
DECLARE
    v_json json;
BEGIN

    SELECT json_agg(p) INTO v_json FROM (SELECT va.name, (CASE  WHEN g.acctnum > 1800 THEN g.title END) as title,
       cast((va.total * g.negate) as money) as total
        FROM private.vaccounts va
        JOIN private.account_groups g ON g.acctnum = va.acctnum
        WHERE va.fund_id = import_fund_id
          AND g.acctnum IN (1000, 1800, 2100, 2200)
          AND (va.total != 0 OR g.acctnum <= 1800)
         ORDER BY va.acctnum) p;


RETURN v_json;
END;
$$ language plpgsql;

