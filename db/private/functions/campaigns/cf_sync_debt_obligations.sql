--DROP function private.cf_sync_debt_obligations(p_fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_sync_debt_obligations(p_fund_id INTEGER, json_data json)
  RETURNS text as $$
DECLARE
  highest_updated timestamp;
  c record;
  v_parent_id INTEGER;
  v_cont_id INTEGER;
BEGIN
  -- Loop through array of loans
  FOR c IN
    SELECT j.*,
           case when d2.debtobligation_id is null then true end as new_record,
           d2.debtobligation_id
    FROM (SELECT
            cast(value->>'orca_id' as int) as orca_id,
            cast(value->>'pid' as int) as pid,
            cast(value->>'contid' as int) as contid,
            cast(value->>'amount' as numeric) as amount,
            cast(value->>'date_' as date) as date_,
            value->>'descr' as descr,
            value->>'memo' as memo,
            to_timestamp(value->>'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated
          from json_array_elements(json_data) d ) j
           left join private.trankeygen tp on tp.orca_id = j.pid and tp.fund_id=p_fund_id
           left join private.trankeygen tc on tc.orca_id = j.contid and tc.fund_id = p_fund_id
           left join private.debtobligation d2 on d2.pid = tp.trankeygen_id and tc.trankeygen_id = d2.contid
    ORDER BY derby_updated, pid
    LOOP
      v_parent_id := private.cf_ensure_trankeygen(p_fund_id, NULL, c.pid);
      v_cont_id := private.cf_ensure_trankeygen(p_fund_id, NULL, c.contid);
      IF c.derby_updated > highest_updated OR highest_updated is null THEN highest_updated := c.derby_updated; END IF;
      IF c.new_record THEN
        INSERT INTO private.debtobligation(
                               pid,
                               contid,
                               amount,
                               date_,
                               descr,
                               memo,
                               orca_id
        )
        VALUES (
                v_parent_id,
                v_cont_id,
                c.amount,
                c.date_,
                c.descr,
                c.memo,
                c.orca_id);
      ELSE
        UPDATE private.debtobligation set
                            amount = c.amount,
                            date_ = c.date_,
                            descr = c.descr,
                            memo = c.memo,
                            orca_id = c.orca_id
        WHERE wapdc.private.debtobligation.debtobligation_id=c.debtobligation_id;
      END IF;
    END LOOP;

    DELETE FROM private.debtobligation where debtobligation_id IN (
        SELECT debtobligation_id
        from private.debtobligation d
                 JOIN private.trankeygen tr on d.pid = tr.trankeygen_id
                 JOIN private.trankeygen tc ON d.contid = tc.trankeygen_id
        WHERE tr.fund_id = p_fund_id and tr.fund_id = tc.fund_id
          -- All pids reference in the json...
          AND tr.orca_id IN (SELECT cast(value ->> 'pid' as int) as pid
                             FROM json_array_elements(json_data))
          -- that don't have the same contid
          AND (tr.orca_id, tc.orca_id)
            NOT IN (SELECT cast(value ->> 'pid' as int) as pid, cast(value ->> 'contid' as int) as contid
                    FROM json_array_elements(json_data))
        );

  RETURN cast (highest_updated as text);
END
$$ LANGUAGE plpgsql;