--Import needs, Carry forward Debt/ credit card balances/ Bank Balances (accounts/petty cash)
CREATE OR REPLACE FUNCTION private.cf_import_prior_campaign_data(n_fund_id INTEGER, o_fund_id INTEGER)
RETURNS void as $$

DECLARE
i_loan_rec record;
default_bank_account integer;
i_bank_rec record;
n_bank_id integer;
v_bank_account_id integer;
i_petty_cash_account_balance numeric;
v_petty_cash_account_id integer;
v_carry_forward_account_id integer;
v_carry_forward_debt_account_id integer;
v_loan_contid integer;
v_loan_contact_key text;
v_loan_description text;
v_receipt_id integer;
v_start_date date;
v_committee_name text;
v_election_year text;
v_receipt_data json;

BEGIN

    --Import prior campaign contacts
    PERFORM private.cf_import_prior_campaign_contacts(n_fund_id, o_fund_id);

    --New campaign start date
    SELECT p.value INTO v_start_date FROM private.properties p WHERE p.fund_id = n_fund_id AND p.name = 'CAMPAIGNINFO:STARTDATE';
    IF v_start_date IS NULL THEN
        SELECT CONCAT(p.value, '-01-01') INTO v_start_date FROM private.properties p WHERE p.fund_id = n_fund_id AND p.name = 'DATE';
    end if;

    -- Combine committee name and election year to form data
    select c.name, f.election_code into v_committee_name, v_election_year
    from public.committee c
    join public.fund f on f.committee_id = c.committee_id
    where f.fund_id = o_fund_id;
    v_receipt_data = json_build_object('committee_name', v_committee_name, 'election_year', v_election_year);

    --Assign and insert carry forward bank account balance
    SELECT MIN(cf.trankeygen_id) INTO v_carry_forward_account_id FROM private.vaccounts cf WHERE cf.acctnum = 4600 AND cf.fund_id = n_fund_id;

    --Get the trankeygen of the new bank account of new fund
    SELECT MIN(bnk.trankeygen_id) INTO v_bank_account_id FROM private.vaccounts bnk WHERE bnk.acctnum = 1000 AND bnk.fund_id = n_fund_id;

    --Assuming the lowest trankeygen_id is the default account from previous campaign
    SELECT MIN(va.trankeygen_id) INTO default_bank_account FROM private.vaccounts va WHERE va.fund_id = o_fund_id AND va.acctnum = 1000;

    FOR i_bank_rec IN SELECT va.*, c.contact_key FROM private.vaccounts va
                        JOIN private.contacts c ON c.trankeygen_id = va.trankeygen_id
                      WHERE va.fund_id = o_fund_id AND va.acctnum = 1000 AND va.total > 0
                        ORDER BY va.trankeygen_id
        LOOP
            v_receipt_id = private.cf_ensure_trankeygen(n_fund_id, null);

            --assuming lowest trankeygen is the default bank account; update the default account associated with new campaign
            IF(i_bank_rec.trankeygen_id = default_bank_account) THEN
                UPDATE private.contacts
                SET name = i_bank_rec.name,
                    contact_key = coalesce(i_bank_rec.contact_key, i_bank_rec.trankeygen_id::text)
                WHERE trankeygen_id = v_bank_account_id;

                INSERT INTO private.receipts(trankeygen_id, type, aggtype, nettype, cid, did, contid, amount, date_, elekey, itemized, memo, carryforward, deptkey)
                VALUES (
                           v_receipt_id, 22, 0, 1, v_bank_account_id, v_carry_forward_account_id, v_bank_account_id, i_bank_rec.total, v_start_date, -1, 0, 'Bank balance carry forward', 1, 0
                       );
                PERFORM private.cf_account_transfer_funds(n_fund_id, v_carry_forward_account_id, v_bank_account_id, i_bank_rec.total);
            ELSE
                --Create new accounts/contacts for other bank accounts
                n_bank_id = private.cf_ensure_trankeygen(n_fund_id, null);

                INSERT INTO private.contacts(trankeygen_id, type, name, contact_key, pagg, gagg)
                VALUES (
                           n_bank_id, 'HDN', i_bank_rec.name, coalesce(i_bank_rec.contact_key, n_bank_id::text), 0.00, 0.00
                       );

                INSERT INTO private.accounts (trankeygen_id, acctnum, contid, style)
                VALUES (
                           n_bank_id, i_bank_rec.acctnum, n_bank_id, i_bank_rec.style
                       );

                INSERT INTO private.receipts(trankeygen_id, type, aggtype, nettype, cid, did, contid, amount, date_, elekey, itemized, memo, carryforward, deptkey)
                VALUES (
                           v_receipt_id, 22, 0, 1, n_bank_id, v_carry_forward_account_id, n_bank_id, i_bank_rec.total, v_start_date, -1, 0, 'Bank balance carry forward', 1, 0
                       );
                PERFORM private.cf_account_transfer_funds(n_fund_id, v_carry_forward_account_id, n_bank_id, i_bank_rec.total);
            END IF;
        END LOOP;

    --Assign and insert carry forward petty cash account balance
    SELECT va.total INTO i_petty_cash_account_balance FROM private.vaccounts va WHERE va.fund_id = o_fund_id AND va.total > 0.00 AND va.acctnum = 1800;
    SELECT pc.trankeygen_id INTO v_petty_cash_account_id FROM private.vaccounts pc WHERE pc.acctnum = 1800 AND pc.fund_id = n_fund_id;
    v_receipt_id = private.cf_ensure_trankeygen(n_fund_id, null);

    IF i_petty_cash_account_balance > 0 THEN
        INSERT INTO private.receipts(trankeygen_id, type, aggtype, nettype, cid, did, amount, date_, elekey, itemized, description, carryforward, deptkey)
        VALUES (
                   v_receipt_id, 22, 0, 1, v_petty_cash_account_id, v_carry_forward_account_id, i_petty_cash_account_balance, v_start_date, -1, 0, 'Petty cash balance carry forward', 1, 0
               );
        PERFORM private.cf_account_transfer_funds(n_fund_id, v_carry_forward_account_id, v_petty_cash_account_id, i_petty_cash_account_balance);
    end if;

    --Grab the carry forward debt account id
    SELECT cf.trankeygen_id INTO v_carry_forward_debt_account_id FROM private.vaccounts cf WHERE cf.acctnum = 5001 AND cf.fund_id = n_fund_id;

    --Loop over import type 3, 5 records
    --the total in account is negative, so make it positive (renamed balance)
    FOR i_loan_rec IN SELECT l.*, -va.total as balance FROM private.vaccounts va
                      JOIN private.loans l ON l.trankeygen_id = va.pid
                      WHERE va.fund_id = o_fund_id AND va.total < 0.00 AND va.acctnum IN (2100, 2200)

        LOOP
            --function to retrieve contact
            SELECT vc.contact_key INTO v_loan_contact_key FROM private.vcontacts vc WHERE vc.trankeygen_id = i_loan_rec.contid;
            v_loan_contid = private.cf_get_contact_from_contact_key(COALESCE(v_loan_contact_key, i_loan_rec.contid::text), n_fund_id);

            IF i_loan_rec.type = 3 THEN
                v_loan_description = 'Carry forward monetary loan';
                PERFORM private.cf_save_monetary_loan(n_fund_id,
                      json_build_object(
                      'contid', v_loan_contid,
                      'date_', v_start_date,
                      'original_loan_date', i_loan_rec.date_,
                      'data', v_receipt_data,
                      'amount', i_loan_rec.balance,
                      'elekey', i_loan_rec.elekey,
                      'carryforward', 1,
                      'interestrate', i_loan_rec.interestrate,
                      'duedate', i_loan_rec.duedate,
                      'checkno', i_loan_rec.checkno,
                      'repaymentschedule', i_loan_rec.repaymentschedule,
                      'description', v_loan_description,
                      'memo', i_loan_rec.memo));

            ELSEIF i_loan_rec.type = 5 THEN
                v_loan_description = 'Carry forward in-kind loan';
                PERFORM private.cf_save_in_kind_loan(n_fund_id,
                     json_build_object(
                     'contid', v_loan_contid,
                     'date_', v_start_date,
                     'original_loan_date', i_loan_rec.date_,
                     'data', v_receipt_data,
                     'amount', i_loan_rec.balance,
                     'elekey', i_loan_rec.elekey,
                     'cid', v_carry_forward_debt_account_id,
                     'carryforward', 1,
                     'interestrate', i_loan_rec.interestrate,
                     'duedate', i_loan_rec.duedate,
                     'checkno', i_loan_rec.checkno,
                     'repaymentschedule', i_loan_rec.repaymentschedule,
                     'description', v_loan_description,
                     'memo', i_loan_rec.memo));
            END IF;

        END LOOP;
END
$$ language plpgsql
