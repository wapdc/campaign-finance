DROP function if exists private.cf_sync_receipts(p_fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_sync_receipts(p_fund_id INTEGER, json_data json, p_version numeric )
    RETURNS text as $$
DECLARE
    highest_updated timestamp;
    c               record;
    v_trankeygen    INTEGER;
    v_cont_id INTEGER;
    v_parent_id INTEGER;
    v_cid INTEGER;
    v_did INTEGER;
    v_gid INTEGER;


BEGIN
    -- Loop through array of receipts
    FOR c IN
        select j.*,
               case when COALESCE(r.trankeygen_id, r2.trankeygen_id) is null then true end as new_record,
               case when coalesce(r.type, r2.type)=28 and coalesce( a.acctnum, a2.acctnum) in (2000, 2600) then 0 end as override_nettype
        from (select cast(value ->> 'orca_id' as int)                                    as orca_id,
                     cast(value ->> 'type' as int)                                       as type,
                     cast(value ->> 'aggtype' as int)                                    as aggtype,
                     cast(value ->> 'nettype' as int)                                    as nettype,
                     cast(value ->> 'pid' as int)                                        as pid,
                     cast(value ->> 'gid' as int)                                        as gid,
                     cast(value ->> 'cid' as int)                                        as cid,
                     cast(value ->> 'did' as int)                                        as did,
                     cast(value ->> 'contid' as int)                                     as contid,
                     cast(value ->> 'amount' as numeric)                                 as amount,
                     cast(value ->> 'date_' as date)                                     as date_,
                     cast(value ->> 'elekey' as int)                                     as elekey,
                     cast(value ->> 'itemized' as int)                                   as itemized,
                     cast(value ->> 'rds_id' as INTEGER)                                 as rds_id,
                     value ->> 'checkno'                                                 as checkno,
                     value ->> 'description'                                             as description,
                     value ->> 'memo'                                                    as memo,
                     cast(value ->> 'carryforward' as int)                               as carryforward,
                     cast(value ->> 'deptkey' as bigint)                                 as deptkey,
                     to_timestamp(value ->> 'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated
              from json_array_elements(json_data) d) j
                 left join private.trankeygen o on o.orca_id = j.orca_id AND o.fund_id = p_fund_id
                 left join private.receipts r on r.trankeygen_id = o.trankeygen_id
                 left join private.accounts a on a.trankeygen_id=r.did
                 left join private.trankeygen o2 on o2.fund_id = p_fund_id and o2.trankeygen_id = j.rds_id
                 left join private.receipts r2 on r2.trankeygen_id = o2.trankeygen_id
                 left join private.accounts a2 on a2.trankeygen_id=r2.did
              ORDER BY derby_updated, orca_id

        LOOP
            v_trankeygen := private.cf_ensure_trankeygen(p_fund_id, c.rds_id, c.orca_id);
            v_cont_id := private.cf_ensure_trankeygen(p_fund_id, NULL, c.contid);
            if (c.type in (20,29,44) and COALESCE(p_version, 1.420) < 1.490) THEN
                select d.debtobligation_id into v_parent_id from private.debtobligation d
                  where d.orca_id = c.pid
                and d.pid in (select trankeygen_id from private.trankeygen where fund_id=p_fund_id);
            ELSE
                v_parent_id := private.cf_ensure_trankeygen(p_fund_id, NULL, c.pid);
            end if;
            v_cid := private.cf_ensure_trankeygen(p_fund_id, NULL, c.cid);
            v_did := private.cf_ensure_trankeygen(p_fund_id, NULL, c.did);
            v_gid := private.cf_ensure_trankeygen(p_fund_id, NULL, c.gid);
            UPDATE private.trankeygen
            set derby_updated = c.derby_updated, pid=v_parent_id
            where trankeygen_id = v_trankeygen;
            IF c.derby_updated > highest_updated OR highest_updated is null THEN
                highest_updated := c.derby_updated;
            END IF;
            IF c.new_record THEN
                insert into private.receipts(trankeygen_id,
                                          type,
                                          aggtype,
                                          nettype,
                                          pid,
                                          gid,
                                          cid,
                                          did,
                                          contid,
                                          amount,
                                          date_,
                                          elekey,
                                          itemized,
                                          checkno,
                                          description,
                                          memo,
                                          carryforward,
                                          deptkey,
                                          user_description)
                values (v_trankeygen,
                        c.type,
                        c.aggtype,
                        coalesce(c.override_nettype, c.nettype),
                        v_parent_id,
                        v_gid,
                        v_cid,
                        v_did,
                        v_cont_id,
                        c.amount,
                        c.date_,
                        c.elekey,
                        c.itemized,
                        c.checkno,
                        c.description,
                        c.memo,
                        c.carryforward,
                        c.deptkey,
                        c.description);
            ELSE
                UPDATE private.receipts as ur
                set type=c.type,
                    aggtype = c.aggtype,
                    nettype = coalesce(c.override_nettype, c.nettype),
                    pid = v_parent_id,
                    gid = v_gid,
                    cid = v_cid,
                    did = v_did,
                    contid = v_cont_id,
                    amount = c.amount,
                    date_ = c.date_,
                    elekey = c.elekey,
                    itemized = c.itemized,
                    checkno = c.checkno,
                    description = c.description,
                    memo = c.memo,
                    carryforward = c.carryforward,
                    deptkey = c.deptkey,
                    user_description = coalesce(ur.user_description, c.description)
                WHERE trankeygen_id = v_trankeygen;
            END IF;
        END LOOP;

    -- Update aggregates for all contacts that have transactions with a non-zero
    -- aggtype to ensure that we maintain contact specific aggregations.
    perform private.cf_update_aggregates(trankeygen_id) from (
       select distinct t.trankeygen_id from json_array_elements(json_data) d
         join private.trankeygen t on t.fund_id = p_fund_id and cast(d->>'contid' as int) = t.orca_id
       WHERE cast(d->>'aggtype' as int) <> 0
    )v;

    RETURN cast(highest_updated as text);
END
$$ LANGUAGE plpgsql;