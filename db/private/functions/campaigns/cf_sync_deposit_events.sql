--DROP function private.cf_sync_deposit_events(p_fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_sync_deposit_events(p_fund_id INTEGER, json_data json)
    RETURNS text as
$$
DECLARE
    highest_updated timestamp;
    c                   record;
    v_trankeygen        INTEGER;
    v_account_id        INTEGER;
    v_deposits_count    INTEGER := 0;
    v_reports_count     INTEGER := 0;

BEGIN
    -- Get counts to be used later to determine if we need to set external_ids
    SELECT count(trankeygen_id) into v_deposits_count from private.vdepositevents where fund_id = p_fund_id;
    SELECT count(report_id) into v_reports_count from public.report
        where report_type = 'C3' and fund_id = p_fund_id;

    -- Loop through array of deposit events
    FOR c IN
        select j.*,
               case when o.trankeygen_id is null and od.trankeygen_id is null then true end as new_record
        from (select cast(value ->> 'orca_id' as int)                                    as orca_id,
                     cast(value ->> 'account_id' as int)                                 as account_id,
                     cast(value ->> 'amount' as numeric)                                     as amount,
                     cast(value ->> 'date_' as date)                                     as date_,
                     cast(value ->> 'deptkey' as bigint)                                    as deptkey,
                     cast(value ->> 'rds_id' as int)                                    as rds_id,
                     value ->> 'memo'                                                    as memo,
                     value -> 'depositItems' as deposit_items,
                     value -> 'reports' as reports,
                     to_timestamp(value ->> 'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated

              from json_array_elements(json_data) d) j
                 left join private.trankeygen o on o.orca_id = j.orca_id and o.fund_id = p_fund_id
                 left join private.trankeygen od on od.trankeygen_id = j.rds_id and od.fund_id=p_fund_id

        LOOP
            v_trankeygen = private.cf_ensure_trankeygen(p_fund_id, c.rds_id, c.orca_id);
            v_account_id = private.cf_ensure_trankeygen(p_fund_id, NULL, c.account_id);
            UPDATE private.trankeygen set derby_updated = c.derby_updated where trankeygen_id = v_trankeygen;

            IF c.derby_updated > highest_updated OR highest_updated is null THEN
                highest_updated := c.derby_updated;
            END IF;
            IF c.new_record THEN
                insert into private.depositevents(trankeygen_id,
                                               account_id,
                                               amount,
                                               date_,
                                               deptkey,
                                               memo)
                values (v_trankeygen,
                        v_account_id,
                        c.amount,
                        c.date_,
                        c.deptkey,
                        c.memo);
            ELSE
                UPDATE private.depositevents
                set account_id = v_account_id,
                    amount     = c.amount,
                    date_      = c.date_,
                    deptkey    = c.deptkey,
                    memo= c.memo

                WHERE trankeygen_id = v_trankeygen;
            END IF;
            insert into private.deposititems(deptid, rcptid) select v_trankeygen as trankeygen_id, t.trankeygen_id as rcptid FROM private.trankeygen t
              join private.receipts r on r.trankeygen_id=t.trankeygen_id
              WHERE p_fund_id = t.fund_id AND t.orca_id in (select json_array_elements(c.deposit_items)::text::int)
              ON CONFLICT DO NOTHING;

            update report set external_id = v_trankeygen where fund_id = p_fund_id
              and report_id in (select json_array_elements_text(c.reports)::Integer);

            delete from private.deposititems where (deptid,rcptid) in
              (select deptid,rcptid from private.deposititems di JOIN private.trankeygen t ON di.rcptid=t.trankeygen_id
                WHERE deptid = v_trankeygen and t.fund_id = p_fund_id and  orca_id NOT IN (select json_array_elements(c.deposit_items)::text::int));
        END LOOP;

    IF v_deposits_count = 0 AND v_reports_count > 0 THEN
        UPDATE report ru
        set external_id = trankeygen_id
        from (select r.fund_id, r.report_id, r.period_start, de2.trankeygen_id,
                     count(r.report_id) over (partition by de2.trankeygen_id) as c_deposits,
                     count(de2.trankeygen_id) over (partition by r.report_id ) as c_reports
              from (select r2.fund_id,
                           r2.report_id,
                           max(r2.period_start) as period_start,
                           max(external_id)     as external_id,
                           sum(amount)          as amount
                    from report r2
                             join transaction t on t.report_id = r2.report_id
                             join report_sum rs
                                  on rs.transaction_id = t.transaction_id and rs.legacy_line_item is not null
                    WHERE r2.submitted_at >= '2022-03-15'
                      and r2.superseded_id is null
                      and r2.report_type = 'C3'
                      and r2.fund_id = p_fund_id
                    group by r2.fund_id, r2.report_id
                   ) r
                       left join private.vdepositevents de2 on de2.fund_id = r.fund_id
                  and de2.date_ = r.period_start and de2.amount = r.amount
                       left join private.vdepositevents de
                                 on cast(de.trankeygen_id as varchar) = r.external_id and r.fund_id = de.fund_id
              where de.trankeygen_id is null
                and de2.trankeygen_id is not null
             ) dv
        where dv.report_id = ru.report_id
          and dv.c_deposits = 1
          and dv.c_reports = 1;
    END IF;
    RETURN cast(highest_updated as text);
END
$$ LANGUAGE plpgsql;