--DROP function private.cf_sync_loans(p_fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_sync_loans(p_fund_id INTEGER, json_data json)
    RETURNS text as $$
DECLARE
  highest_updated timestamp;
  c record;
  v_trankeygen INTEGER;
  v_cid INTEGER;
  v_did INTEGER;
  v_cont_id INTEGER;
BEGIN

  -- Loop through array of loans
  FOR c IN
    select j.*,
           case when coalesce(l.trankeygen_id, l2.trankeygen_id) is null then true end as new_record
    from (select
            cast(value->>'orca_id' as int) as orca_id,
            cast(value->>'rds_id' as int) as rds_id,
            cast(value->>'type' as int) as type,
            cast(value->>'cid' as int) as cid,
            cast(value->>'did' as int) as did,
            cast(value->>'contid' as int) as contid,
            cast(value->>'amount' as numeric) as amount,
            cast(value->>'date_' as date) as date_,
            cast(value->>'elekey' as int) as elekey,
            cast(value->>'interestrate' as numeric) as interestrate,
            cast(value->>'duedate' as date) as duedate,
            value->>'checkno' as checkno,
            value->>'repaymentschedule' as repaymentschedule,
            value->>'description' as description,
            value->>'memo' as memo,
            cast(value->>'carryforward' as int) as carryforward,
            to_timestamp(value->>'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated
          from json_array_elements(json_data) d ) j
           left join private.trankeygen o on o.orca_id = j.orca_id and o.fund_id=p_fund_id
           left join private.loans l on o.trankeygen_id = l.trankeygen_id
           left join private.trankeygen o2 on o2.fund_id = p_fund_id and o2.trankeygen_id = j.rds_id
           left join private.loans l2 on l2.trankeygen_id = o2.trankeygen_id
          ORDER BY derby_updated, orca_id
      LOOP
        v_trankeygen := private.cf_ensure_trankeygen(p_fund_id, NULL, c.orca_id);
        v_cont_id := private.cf_ensure_trankeygen(p_fund_id, NULL, c.contid);
        v_cid := private.cf_ensure_trankeygen(p_fund_id, NULL, c.cid);
        v_did := private.cf_ensure_trankeygen(p_fund_id, NULL, c.did);
        UPDATE private.trankeygen set derby_updated = c.derby_updated where trankeygen_id = v_trankeygen;
        IF c.derby_updated > highest_updated OR highest_updated is null THEN highest_updated := c.derby_updated; END IF;
          IF c.new_record THEN

              insert into private.loans(trankeygen_id,
                                        type,
                                     cid,
                                     did,
                                     contid,
                                     amount,
                                     date_,
                                     elekey,
                                     interestrate,
                                     duedate,
                                     checkno,
                                     repaymentschedule,
                                     description,
                                     memo,
                                     carryforward
              )
              values(
                        v_trankeygen,
                        c.type,
                     v_cid,
                     v_did,
                     v_cont_id,
                     c.amount,
                     c.date_,
                     c.elekey,
                     c.interestrate,
                     c.duedate,
                     c.checkno,
                     c.repaymentschedule,
                     c.description,
                     c.memo,
                     c.carryforward);
          ELSE
              UPDATE private.loans set
                                       type = c.type,
                                    cid = v_cid,
                                    did = v_did,
                                    contid = v_cont_id,
                                    amount = c.amount,
                                    date_ = c.date_,
                                    elekey = c.elekey,
                                    interestrate = c.interestrate,
                                    duedate = c.duedate,
                                    checkno = c.checkno,
                                    repaymentschedule = c.repaymentschedule,
                                    description = c.description,
                                    memo = c.memo,
                                    carryforward = c.carryforward

              WHERE trankeygen_id=v_trankeygen;
          END IF;
      END LOOP;
  RETURN cast (highest_updated as text);
END
$$ LANGUAGE plpgsql;