--DROP function private.cf_sync_contacts(p_fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_sync_contacts(p_fund_id INTEGER, json_data json)
  RETURNS text as $$
DECLARE
  highest_updated timestamp;
  c record;
  v_trankeygen_id INTEGER;
  v_parent_id INTEGER;
  v_c1_tran INTEGER;
  v_c2_tran INTEGER;
BEGIN
  -- Loop through array of contacts
  FOR c IN
    select j.*,
           case when oc.trankeygen_id is null and o2.trankeygen_id is null then true end as new_record
    from (select
            cast(value->>'orca_id' as int) as orca_id,
            cast(value->>'pid' as int) as pid,
            cast(value->>'rds_id' as int) as rds_id,
            value->>'name' as name,
            value->>'street' as street,
            value->>'city' as city,
            value->>'state' as state,
            value->>'zip' as zip,
            value->>'phone' as phone,
            value->>'email' as email,
            value->>'occupation' as occupation,
            value->>'employername' as employername,
            value->>'employerstreet' as employerstreet,
            value->>'employercity' as employercity,
            value->>'employerstate' as employerstate,
            value->>'employerzip' as employerzip,
            value->>'memo' as memo,
            value->>'type' as type,
            coalesce(cast(value->>'pagg' as numeric), 0.00) as pagg,
            coalesce(cast(value->>'gagg' as numeric), 0.00) as gagg,
            value->>'prefix' as prefix,
            value->>'firstname' as firstname,
            value->>'lastname' as lastname,
            value->>'middleinitial' as middleinitial,
            value->>'suffix' as suffix,
            cast(value->>'contact1_id' as int) as contact1_id,
            cast(value->>'contact2_id' as int) as contact2_id,
            value -> 'members' as members,
            to_timestamp(value->>'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated

          from json_array_elements(json_data) d ) j
           left join private.trankeygen o on o.orca_id = j.orca_id  and o.fund_id=p_fund_id
           left join private.contacts oc on oc.trankeygen_id=o.trankeygen_id
           left join private.trankeygen o2 on o2.fund_id = p_fund_id and o2.trankeygen_id = j.rds_id
           left join private.contacts c2 on c2.trankeygen_id = o2.trankeygen_id
          ORDER BY derby_updated,j.orca_id
    LOOP
      v_trankeygen_id = private.cf_ensure_trankeygen(p_fund_id, c.rds_id,  c.orca_id);
      v_parent_id = private.cf_ensure_trankeygen(p_fund_id, NULL, c.pid);
      v_c1_tran = private.cf_ensure_trankeygen(p_fund_id, NULL, c.contact1_id);
      v_c2_tran = private.cf_ensure_trankeygen(p_fund_id, NULL, c.contact2_id);
      UPDATE private.trankeygen set derby_updated = c.derby_updated, pid=v_parent_id where trankeygen_id = v_trankeygen_id;
      IF c.derby_updated > highest_updated OR highest_updated is null THEN highest_updated := c.derby_updated; END IF;
      IF c.new_record THEN
        insert into private.contacts(trankeygen_id,
                                  type,
                                  name,
                                  street,
                                  city,
                                  state,
                                  zip,
                                  phone,
                                  email,
                                  occupation,
                                  employername,
                                  employerstreet,
                                  employercity,
                                  employerstate,
                                  employerzip,
                                  pagg,
                                  gagg,
                                  memo)
        values(
                v_trankeygen_id,
                c.type,
                c.name,
                c.street,
                c.city,
                c.state,
                c.zip,
                c.phone,
                c.email,
                c.occupation,
                c.employername,
                c.employerstreet,
                c.employercity,
                c.employerstate,
                c.employerzip,
                c.pagg,
                c.gagg,
                c.memo);
      ELSE

        UPDATE private.contacts set
                               type=c.type,
                               name=c.name,
                               street=c.street,
                               city=c.city,
                               state=c.state,
                               zip=c.zip,
                               phone=c.phone,
                               email=c.email,
                               occupation= c.occupation,
                               employername = c.employername,
                               employerstreet = c.employerstreet,
                               employercity = c.employercity,
                               employerstate = c.employerstate,
                               employerzip = c.employerzip,
                               pagg = c.pagg,
                               gagg = c.gagg,
                               memo = c.memo
        WHERE trankeygen_id=v_trankeygen_id;
      END IF;
      IF coalesce(c.prefix,c.firstname, c.lastname, c.middleinitial, c.suffix) IS NOT NULL THEN
          INSERT INTO private.individualcontacts(trankeygen_id, prefix, firstname, middleinitial, lastname, suffix)
            VALUES(v_trankeygen_id, c.prefix, c.firstname, c.middleinitial, c.lastname, c.suffix )
          ON CONFLICT(trankeygen_id) DO UPDATE
            SET prefix=c.prefix, firstname=c.firstname, middleinitial=c.middleinitial, lastname=c.lastname, suffix=c.suffix;
      ELSE
        DELETE FROM private.individualcontacts WHERE  trankeygen_id=v_trankeygen_id;
      END IF;
      IF c.contact1_id is not null and c.contact2_id IS NOT NULL AND c.contact1_id <> 0 and c.contact2_id<>0 THEN
        SELECT trankeygen_id into v_c1_tran from private.trankeygen WHERE fund_id = p_fund_id and orca_id=c.contact1_id;
        SELECT trankeygen_id into v_c2_tran from private.trankeygen WHERE fund_id = p_fund_id and orca_id=c.contact2_id;

        INSERT INTO private.couplecontacts(trankeygen_id, contact1_id, contact2_id)
          VALUES(v_trankeygen_id, v_c1_tran, v_c2_tran)
        ON CONFLICT(trankeygen_id) DO
          UPDATE
          SET contact1_id=v_c1_tran, contact2_id=v_c2_tran;
      ELSE
        DELETE FROM private.couplecontacts WHERE trankeygen_id=v_trankeygen_id;
      END IF;
      IF c.members is not null THEN
          -- Insert new members.
          insert into private.registeredgroupcontacts (gconid, contid)
            select v_trankeygen_id, t.trankeygen_id FROM private.trankeygen t where t.fund_id = p_fund_id and
                                                   t.orca_id in (select json_array_elements(c.members)::text::int)
          ON CONFLICT DO NOTHING;

          -- Delete members that are not indicated in the json
          delete from private.registeredgroupcontacts where (gconid,contid) in
             (select gconid,contid from private.registeredgroupcontacts rgc JOIN private.trankeygen t ON rgc.contid=t.trankeygen_id
                                                  WHERE rgc.gconid = v_trankeygen_id and t.fund_id = p_fund_id and
                                                        orca_id NOT IN (select json_array_elements(c.members)::text::int));
      end if;
    END LOOP;
  RETURN cast (highest_updated as text);
END
$$ LANGUAGE plpgsql;