--DROP function private.cf_save_submission_properties(p_fund_id INTEGER, j JSON);
CREATE OR REPLACE FUNCTION private.cf_save_submission_properties(p_fund_id INTEGER, j JSON)
  RETURNS VOID as  $$
DECLARE p REFCURSOR;
BEGIN
  IF j IS NOT NULL THEN
    INSERT INTO private.properties(fund_id, name, value) SELECT p_fund_id, value->>'name' as n, value->>'value' v FROM json_array_elements(J)
      ON CONFLICT(fund_id, name) DO UPDATE set value=EXCLUDED.value;
  END IF;
  RETURN;
END
$$ LANGUAGE plpgsql;