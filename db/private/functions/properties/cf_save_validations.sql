--DROP function private.cf_save_validations(p_fund_id INTEGER, json_data TEXT);
CREATE OR REPLACE FUNCTION private.cf_save_validations(p_fund_id INTEGER, json_data TEXT)
  RETURNS VOID
  language plpgsql
  as $$
DECLARE
  j json;
  rds_guid TEXT;
  json_guid TEXT;
BEGIN
  j := cast(json_data as json);
  -- Determine GUID of existing fund;
  SELECT value INTO rds_guid FROM private.properties WHERE fund_id = p_fund_id AND name = 'GUID';

  -- Determine GUID in json
  SELECT value INTO json_guid FROM (SELECT p->>'value' as value FROM json_array_elements(j->'properties') p WHERE p->>'name' = 'GUID') j;

  IF (rds_guid is null OR json_guid = rds_guid) THEN
      IF j->'properties' IS NOT NULL THEN
        EXECUTE private.cf_save_properties(p_fund_id, j->'properties');
      END IF;
      IF j->'limits' IS NOT NULL THEN
          EXECUTE private.cf_save_limits(p_fund_id, j->'limits');
      END IF;
      IF j->'reportingPeriods' IS NOT NULL THEN
          EXECUTE private.cf_save_reporting_periods(p_fund_id, j->'reportingPeriods');
      END IF;
  END IF;

  -- Update reporting obligations
  EXECUTE private.cf_populate_reporting_obligations(p_fund_id);

end
$$;