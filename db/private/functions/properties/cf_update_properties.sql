--DROP function private.cf_update_properties(p_fund_id INTEGER, j JSON);
-- This function sets values for the private.properties table but does NOT remove any existing properties.
-- Use private.cf_save_properties if you intend to completely replace all properties of a fund
CREATE OR REPLACE FUNCTION private.cf_update_properties(p_fund_id INTEGER, j JSON)
    RETURNS VOID as  $$
DECLARE p REFCURSOR;
BEGIN
    IF j IS NOT NULL THEN
        -- Delete values where we are setting the value to null in the JSON, because that means we do not have a value
        DELETE FROM private.properties WHERE fund_id = p_fund_id AND name IN (SELECT value->>'name' from json_array_elements(J) WHERE VALUE is null);
        -- Update any elements that need to be changed.
        INSERT INTO private.properties(fund_id, name, value) SELECT p_fund_id, value->>'name' as n, value->>'value' v FROM json_array_elements(J)
          ON CONFLICT(fund_id, name) DO UPDATE set value=EXCLUDED.value;
    END IF;
    RETURN;
END
$$ LANGUAGE plpgsql;