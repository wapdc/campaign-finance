--DROP function if exists private.cf_save_properties(p_fund_id INTEGER, j JSON);
CREATE OR REPLACE FUNCTION private.cf_save_properties(p_fund_id INTEGER, j JSON)
  RETURNS VOID as  $$
BEGIN
  IF j IS NOT NULL THEN
    DELETE FROM private.properties WHERE fund_id = p_fund_id
        AND name NOT IN (SELECT value->>'name' from json_array_elements(J))
        AND name <> 'VNUM';
    INSERT INTO private.properties(fund_id, name, value) SELECT p_fund_id, value->>'name' as n, value->>'value' v FROM json_array_elements(J)
      ON CONFLICT(fund_id, name) DO UPDATE set value=EXCLUDED.value;
  END IF;
  RETURN;
END
$$ LANGUAGE plpgsql;