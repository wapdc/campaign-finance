CREATE OR REPLACE FUNCTION private.cf_save_depositevent(p_fund_id int, p_json json) returns int as $$
    declare
        v_old_date_ DATE;
        v_deposit_date DATE;
        v_new_cid INTEGER;
        v_deposit_id INTEGER;
        v_deptkey BIGINT;
        v_deposit_amount numeric(16,2);
        deposit_item RECORD;
        v_undeposited_funds_acct INTEGER;
    begin
        v_deposit_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));

        select trankeygen_id into v_new_cid from private.vaccounts a where a.trankeygen_id = cast(p_json->>'account_id' as integer)
          and a.fund_id=p_fund_id;

        if v_new_cid is null then
            raise 'Invalid account_id: %',p_json->>'account_id';
        end if;

        v_deposit_date := cast(p_json->>'date_' as date);

        SELECT min(trankeygen_id) into v_undeposited_funds_acct FROM private.vaccounts WHERE fund_id = p_fund_id and acctnum=1600;


        -- Check to see if the deposit date has changed.
        SELECT date_, deptkey into v_old_date_, v_deptkey from private.vdepositevents where fund_id = p_fund_id and trankeygen_id = v_deposit_id;
        if (CAST(p_json->>'date_' as DATE) is distinct from v_old_date_) THEN
            v_deptkey := private.cf_generate_deposit_key(p_fund_id,v_deposit_date);
        end if;

        insert into private.depositevents (trankeygen_id, account_id, amount, date_, deptkey, memo)
          VALUES (
                  v_deposit_id,
                  v_new_cid,
                  0.0,
                  v_deposit_date,
                  v_deptkey,
                  p_json->>'memo'
                  )
        on conflict (trankeygen_id) DO UPDATE
            set account_id = excluded.account_id,
                date_ = excluded.date_,
                deptkey = excluded.deptkey,
                memo = excluded.memo;

        -- Add deposit items
        v_deposit_amount := 0.0;
        for deposit_item in (
          select r.*, di.deptid from private.vreceipts r
             LEFT JOIN private.deposititems di on di.rcptid=r.trankeygen_id and di.deptid = v_deposit_id
             where r.fund_id=p_fund_id and r.trankeygen_id in (select cast(j->>'trankeygen_id' as integer) from json_array_elements(p_json->'depositItems') j)) LOOP
            if deposit_item.deptid is null then
                if deposit_item.date_ > v_deposit_date then
                    RAISE 'Date of transaction is greater than deposit date';
                end if;
                insert into private.deposititems(deptid, rcptid) values (v_deposit_id, deposit_item.trankeygen_id);
            end if;
            update private.receipts r set deptkey = v_deptkey, cid=v_new_cid where trankeygen_id = deposit_item.trankeygen_id;
            -- Add any group contribution detail records from the deposit.
            if deposit_item.type = 11 then

                insert into private.deposititems(deptid, rcptid) select v_deposit_id, trankeygen_id
                  from private.vreceipts where fund_id=p_fund_id and pid = deposit_item.trankeygen_id
                on conflict (deptid,rcptid) do nothing;
                update private.receipts set deptkey = v_deptkey where pid=deposit_item.trankeygen_id;
            end if;
            perform private.cf_account_adjust(p_fund_id, deposit_item.did, deposit_item.cid, deposit_item.amount, deposit_item.did, v_new_cid, deposit_item.amount);
            v_deposit_amount := v_deposit_amount + deposit_item.amount;
          end loop;

        -- Remove deposit items that are no longer referenced. Exclude group contribution detail records (type=41) as they are removed
        -- when the parent record gets removed.
        for deposit_item in (
            select r.*, di.deptid from private.vreceipts r
                                           JOIN private.deposititems di on di.rcptid=r.trankeygen_id and di.deptid = v_deposit_id
            where r.fund_id=p_fund_id
              and r.type <> 41
              and r.trankeygen_id not in (select cast(j->>'trankeygen_id' as integer) from json_array_elements(p_json->'depositItems') j)
        ) LOOP
            delete from private.deposititems di where di.deptid=v_deposit_id and di.rcptid = deposit_item.trankeygen_id;

            -- Remove child items from group contributions
            if (deposit_item.type = 11) then
                update private.receipts r set deptkey = 0 where pid=deposit_item.trankeygen_id;
                delete from private.deposititems where deptid=v_deposit_id and rcptid in (
                    select trankeygen_id from private.receipts where pid=deposit_item.trankeygen_id
                    );
            end if;
            update private.receipts r
              set deptkey = 0, cid = v_undeposited_funds_acct
            where trankeygen_id = deposit_item.trankeygen_id;
            perform private.cf_account_adjust(p_fund_id, deposit_item.did, deposit_item.cid, deposit_item.amount, deposit_item.did, v_undeposited_funds_acct, deposit_item.amount);
        end loop;

        update private.depositevents set amount = v_deposit_amount where trankeygen_id = v_deposit_id;

        return v_deposit_id;
    end
$$ language plpgsql;
