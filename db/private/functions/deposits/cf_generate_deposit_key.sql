CREATE OR REPLACE FUNCTION private.cf_generate_deposit_key(p_fund_id INTEGER, p_deposit_date DATE) returns BIGINT as $$
  declare
    v_deposit_number integer;
    v_deptkey BIGINT;
  begin
    -- Find out existing deposit number
    SELECT f.deposit_number into v_deposit_number FROM private.campaign_fund f
      where fund_id=p_fund_id;

    if v_deposit_number is null then
        -- IF we have no deposit number, generate it and create it.
        SELECT MAX(t.orca_id) into v_deposit_number from private.trankeygen t join private.depositevents de ON de.trankeygen_id=t.trankeygen_id
        where fund_id = p_fund_id;

        if v_deposit_number is null then
            v_deposit_number := 0;
        end if;
    end if;

    v_deposit_number := v_deposit_number  + 1;
    INSERT into private.campaign_fund(fund_id, deposit_number) VALUES (p_fund_id, v_deposit_number)
      ON CONFLICT(fund_id) DO UPDATE set deposit_number = v_deposit_number;

    -- Determine unix timestamp.
    v_deptkey := (extract(epoch from p_deposit_date) * 1000) + v_deposit_number::BIGINT;

    return v_deptkey;
  end
$$ language plpgsql;
