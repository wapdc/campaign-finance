CREATE OR REPLACE FUNCTION private.cf_delete_depositevent(p_deposit_id integer) returns void as $$
  DECLARE
     v_account_id INTEGER;
     v_fund_id INTEGER;
     v_undeposited_account INTEGER;
     v_report_id INTEGER;
     i_rec RECORD;
  BEGIN

     SELECT de.account_id,de.fund_id, r.report_id  INTO v_account_id, v_fund_id, v_report_id FROM  private.vdepositevents de
       left join report r ON r.fund_id = de.fund_id and r.external_id = cast(de.trankeygen_id as varchar)
         and r.superseded_id is null
     where de.trankeygen_id=p_deposit_id;

     if v_report_id is not null then
         raise 'Cannot delete deposit that has reports';
     end if;

     SELECT MIN(trankeygen_id) INTO v_undeposited_account from private.vaccounts where fund_id=v_fund_id and acctnum=1600;

     FOR i_rec IN (
         select r.trankeygen_id, r.amount from private.deposititems di
           JOIN private.vreceipts r on r.trankeygen_id=di.rcptid
           where di.deptid = p_deposit_id and r.nettype<>0
         ) LOOP
           update private.receipts set cid=v_undeposited_account where trankeygen_id = i_rec.trankeygen_id;
           PERFORM private.cf_account_transfer_funds(v_fund_id, v_account_id, v_undeposited_account, i_rec.amount);
         end loop;

     DELETE FROM private.deposititems WHERE deptid = p_deposit_id;
     DELETE FROM private.trankeygen t WHERE t.trankeygen_id=p_deposit_id;
  end
$$ language plpgsql;
