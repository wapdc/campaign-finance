CREATE OR REPLACE FUNCTION private.cf_recalculate_deposit_event(p_fund_id int, p_deposit_id int) returns int as $$
    declare
        v_deposit_amount numeric(16,2);
    begin

        select sum(vr.amount) into v_deposit_amount from private.vreceipts vr
        inner join private.deposititems di on di.rcptid = vr.trankeygen_id
        where vr.fund_id = p_fund_id
          and di.deptid = p_deposit_id
          and vr.transaction_type != 'Monetary Group Contribution Detail';

        update private.depositevents de set amount = v_deposit_amount
        where de.trankeygen_id = p_deposit_id;

        return v_deposit_amount;
    end
$$ language plpgsql;