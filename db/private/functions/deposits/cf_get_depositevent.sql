CREATE OR REPLACE FUNCTION private.cf_get_depositevent(p_deposit_id INTEGER) returns JSON as $$
    declare
        v_json JSON;
        v_item_json JSON;
        v_orca_ids int[];
        v_fund_id integer;
  BEGIN
    select fund_id into v_fund_id from private.vdepositevents where trankeygen_id=p_deposit_id;

    -- Ran into out of diskspace errors when not filtering based on fund_id.  Which is odd.
    select json_agg(r order by r.trankeygen_id),
           array_agg(r.orca_id order by r.orca_id)
    into v_item_json,
        v_orca_ids
    from (select r2.* from  private.deposititems di join private.vreceipts r2 on r2.trankeygen_id=di.rcptid
    where di.deptid=p_deposit_id and fund_id=v_fund_id and r2.type <> 41) r;

    SELECT row_to_json(v) into v_json from
      (select de.*, v_item_json "depositItems", v_orca_ids "depositItemsOrca" from  private.vdepositevents de
    where trankeygen_id = p_deposit_id) v;
    return v_json;
  end
$$ language plpgsql stable;
