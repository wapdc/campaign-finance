--DROP function private.cf_sync_auction_items(p_fund_id INTEGER, json_data json);
CREATE OR REPLACE FUNCTION private.cf_sync_auction_items(p_fund_id INTEGER, json_data json)
    RETURNS text as $$
DECLARE
  highest_updated timestamp;
  c record;
  v_trankeygen INTEGER;
  v_parent_id INTEGER;
BEGIN
  -- Loop through array of auction items
  FOR c IN
    select j.*,
           case when coalesce(au.trankeygen_id, au2.trankeygen_id) is null then true end as new_record
    from (select
            cast(value->>'orca_id' as int) as orca_id,
            cast(value->>'pid' as int) as pid,
            cast(value->>'rds_id' as int) as rds_id,
            value->>'itemnumber' as itemnumber,
            value->>'itemdescription' as itemdescription,
            cast(value->>'marketval' as numeric) as marketval,
            value->>'memo' as memo,
            to_timestamp(value->>'derby_updated', 'YYYY-MM-DD HH24:MI:SS:MS') as derby_updated
            from json_array_elements(json_data) d ) j
            left join private.trankeygen o on o.orca_id = j.orca_id and o.fund_id=p_fund_id
            left join private.auctionitems au on au.trankeygen_id=o.trankeygen_id
            left join private.trankeygen o2 on o2.trankeygen_id = j.rds_id and o2.fund_id=p_fund_id
            left join private.auctionitems au2 on au2.trankeygen_id=o2.trankeygen_id

      LOOP
          v_trankeygen := private.cf_ensure_trankeygen(p_fund_id, c.rds_id, c.orca_id);
          v_parent_id := private.cf_ensure_trankeygen(p_fund_id, NULL, c.pid);
          UPDATE private.trankeygen set derby_updated = c.derby_updated, pid=v_parent_id where trankeygen_id = v_trankeygen;
          IF c.derby_updated > highest_updated OR highest_updated is null THEN highest_updated := c.derby_updated; END IF;
          IF c.new_record THEN

              insert into private.auctionitems(trankeygen_id,
                                        pid,
                                        itemnumber,
                                        itemdescription,
                                        marketval,
                                        memo
              )
              values(
                        v_trankeygen,
                        v_parent_id,
                        c.itemnumber,
                        c.itemdescription,
                        c.marketval,
                        c.memo);
          ELSE
              UPDATE private.auctionitems set
                                      pid=v_parent_id,
                                      itemnumber = c.itemnumber,
                                      itemdescription = c.itemdescription,
                                      marketval = c.marketval,
                                      memo= c.memo
              WHERE trankeygen_id=v_trankeygen;
          END IF;
      END LOOP;
  RETURN cast (highest_updated as text);
END
$$ LANGUAGE plpgsql;