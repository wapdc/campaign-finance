CREATE OR REPLACE function private.cf_save_auction_item(p_fund_id integer, p_json JSON) returns integer as $$
    DECLARE
        v_auction_item_id INTEGER;
        v_auction_id INTEGER;
        v_donor_rds_id INTEGER;
        v_donor_contid INTEGER;
        v_donor_donation NUMERIC(16,2);
        v_donor_deposit_id INTEGER;
        v_buyer_rds_id INTEGER;
        v_buyer_contid INTEGER;
        v_buyer_donation NUMERIC(16,2);
        v_buyer_deposit_id INTEGER;
        v_sale_price NUMERIC(16,2);
        v_auction_item_date DATE;
        v_cid INTEGER;
        v_marketval NUMERIC(16,2);

        v_donor_old_cid INTEGER;
        v_donor_old_did INTEGER;
        v_donor_old_amount NUMERIC(16,2);

        v_buyer_old_cid INTEGER;
        v_buyer_old_did INTEGER;
        v_buyer_old_amount NUMERIC(16,2);
    begin

        select trankeygen_id, date_ into v_auction_id, v_auction_item_date from private.vaccounts va
        where va.fund_id = p_fund_id and va.trankeygen_id = cast(p_json->>'pid' as integer)
        and va.acctnum = 4098;

        select trankeygen_id into v_donor_contid from private.vcontacts vc
        where vc.fund_id = p_fund_id and vc.trankeygen_id = cast(p_json->>'donor_contid' as integer);

        if v_donor_contid is null then
            RAISE 'Invalid or missing donor contact id';
        end if;

        v_buyer_contid = cast(p_json->>'buyer_contid' as integer);
        if v_buyer_contid is not null then
            select trankeygen_id into v_buyer_contid from private.vcontacts vc
            where vc.fund_id = p_fund_id and vc.trankeygen_id = cast(p_json->>'buyer_contid' as integer);
            if v_buyer_contid is null then
                RAISE 'Invalid buyer contact id';
            end if;
        end if;

        if v_auction_id is null then
            RAISE 'Invalid or missing auction id';
        end if;

        v_sale_price = cast(p_json->>'sale_price' as numeric);
        v_marketval = cast(p_json->>'marketval' as numeric);
        v_auction_item_date = coalesce(cast(p_json->>'date_' as date), v_auction_item_date);

        -- cid
        select trankeygen_id into v_cid from private.vaccounts where fund_id = p_fund_id and acctnum = 1600;

        select
               ai.trankeygen_id,
               rd.trankeygen_id, rd.cid, rd.did, rd.amount, rd.deposit_id,
               rb.trankeygen_id, rb.cid, rb.did, rb.amount, rb.deposit_id
        into
            v_auction_item_id,
            v_donor_rds_id, v_donor_old_cid, v_donor_old_did, v_donor_old_amount, v_donor_deposit_id,
            v_buyer_rds_id, v_buyer_old_cid, v_buyer_old_did, v_buyer_old_amount, v_buyer_deposit_id
        from private.auctionitems ai
        join private.trankeygen t on ai.trankeygen_id = t.trankeygen_id
        join private.vreceipts rd on rd.pid = ai.trankeygen_id and rd.type = 7
        left join private.vreceipts rb on rb.pid = ai.trankeygen_id and rb.type = 8
        where t.fund_id = p_fund_id
        and ai.trankeygen_id = cast(p_json->>'trankeygen_id' as integer);

        v_auction_item_id = private.cf_ensure_trankeygen(p_fund_id, v_auction_item_id);
        v_donor_rds_id = private.cf_ensure_trankeygen(p_fund_id, v_donor_rds_id);

        INSERT INTO private.auctionitems(trankeygen_id, pid, itemnumber, itemdescription, marketval, memo)
        VALUES(v_auction_item_id, v_auction_id, p_json->>'itemnumber', p_json->>'itemdescription',v_marketval, p_json->>'memo')
        ON CONFLICT(trankeygen_id) DO UPDATE SET
            itemnumber = excluded.itemnumber,
            itemdescription = excluded.itemdescription,
            marketval = excluded.marketval,
            memo = excluded.memo;

        -- in the case item hasn't sold
        IF v_sale_price is null then
            v_buyer_donation = 0;
            v_donor_donation = 0;
        ELSEIF v_sale_price >= v_marketval then
            v_buyer_donation = v_sale_price - v_marketval;
            v_donor_donation = v_marketval;
        ELSE
            v_buyer_donation = 0;
            v_donor_donation = v_sale_price;
        end if;

        -- donor receipt insertion
        INSERT INTO private.receipts(trankeygen_id, type, aggtype, nettype, pid, cid, did, contid, amount, date_, elekey, itemized, checkno, deptkey)
        VALUES (v_donor_rds_id, 7, 1, 1, v_auction_item_id, v_cid, v_auction_id, v_donor_contid, v_donor_donation, v_auction_item_date, cast(p_json->>'donor_elekey' as integer), 0, p_json->>'donor_checkno', 0)
        ON CONFLICT (trankeygen_id) DO UPDATE SET
            contid = excluded.contid,
            amount = excluded.amount,
            date_ = excluded.date_,
            elekey = excluded.elekey,
            checkno = excluded.checkno;

        -- adjust accounting
        perform private.cf_account_adjust(p_fund_id, v_donor_old_did, v_donor_old_cid, v_donor_old_amount, v_auction_id, coalesce(v_donor_old_cid, v_cid), v_donor_donation);
        perform private.cf_update_aggregates(v_donor_contid);

        -- buyer receipt insertion
        v_buyer_rds_id = private.cf_ensure_trankeygen(p_fund_id, v_buyer_rds_id);
        INSERT INTO private.receipts(trankeygen_id, type, aggtype, nettype, pid, cid, did, contid, amount, date_, elekey, itemized, checkno, deptkey)
        VALUES (v_buyer_rds_id, 8, 1, 1, v_auction_item_id, v_cid, v_auction_id, v_buyer_contid, v_buyer_donation, v_auction_item_date, cast(p_json->>'buyer_elekey' as integer), 0, p_json->>'buyer_checkno', 0)
        ON CONFLICT (trankeygen_id) DO UPDATE SET
                                                  contid = excluded.contid,
                                                  amount = excluded.amount,
                                                  date_ = excluded.date_,
                                                  elekey = excluded.elekey,
                                                  checkno = excluded.checkno;

        -- adjust accounting
        perform private.cf_account_adjust(p_fund_id, v_buyer_old_did, v_buyer_old_cid, v_buyer_old_amount, v_auction_id, coalesce(v_buyer_old_cid, v_cid), v_buyer_donation);
        perform private.cf_update_aggregates(v_buyer_contid);

        -- update deposit event total
        if v_donor_deposit_id is not null then
            perform private.cf_recalculate_deposit_event(p_fund_id, v_donor_deposit_id);
        end if;

        if v_buyer_deposit_id is not null and v_buyer_deposit_id is distinct from v_donor_deposit_id then
            perform private.cf_recalculate_deposit_event(p_fund_id, v_buyer_deposit_id);
        end if;

        return v_auction_item_id;
    end;
$$ language plpgsql
