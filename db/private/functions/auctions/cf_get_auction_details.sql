CREATE OR REPLACE FUNCTION private.cf_get_auction_details(p_fund_id INTEGER, p_id integer) returns json as $$
  declare
      v_json JSON;
      v_auction_items JSON;
  begin
      SELECT json_agg(i order by i.date_ desc) into v_auction_items from (
        SELECT
            ai.trankeygen_id,
            t.orca_id,
            ai.itemnumber,
            ai.itemdescription,
            ai.marketval,
            greatest(coalesce(rd.deptkey,0), coalesce(rb.deptkey,0)) as deptkey,
            rd.trankeygen_id as donor_rds_id,
            rd.orca_id as donor_orca_id,
            rd.name as donor,
            rd.contid as donor_contid,
            rd.contid_orca as donor_contid_orca,
            rb.name as buyer,
            rb.trankeygen_id as buyer_rds_id,
            rb.orca_id as buyer_orca_id,
            rb.contid as buyer_contid,
            rb.contid_orca as buyer_contid_orca,
            rb.amount as buyer_donation,
            rd.amount as donor_donation,
            case when rd.amount > 0 or rb.amount > 0 then rd.amount + rb.amount end as sale_price,
            rb.date_
          from private.auctionitems ai
          join private.trankeygen t on t.trankeygen_id=ai.trankeygen_id
          join private.vreceipts rd on ai.trankeygen_id=rd.pid and rd.type=7
          left join private.vreceipts rb on rb.pid = ai.trankeygen_id and rb.type=8
          WHERE ai.pid=p_id
            and t.fund_id=p_fund_id
      ) i;

     select row_to_json(au) into v_json
            from (select a.*, c.street, c.city, c.state, c.zip, v_auction_items as items from private.vaccounts a
                left join private.contacts c on c.trankeygen_id=a.contid
                where a.fund_id=p_fund_id and a.trankeygen_id=p_id) au;
     return v_json;
  end
$$ language plpgsql;