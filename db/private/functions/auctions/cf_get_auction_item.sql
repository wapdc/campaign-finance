CREATE OR REPLACE FUNCTION private.cf_get_auction_item(p_fund_id INTEGER, p_trankeygen_id INTEGER) RETURNS json AS $$
    DECLARE
        v_json JSON;
    BEGIN
        SELECT row_to_json(v) INTO v_json
        from (
            select
            ai.trankeygen_id,
            t.orca_id,
            ai.itemnumber,
            ai.itemdescription,
            ai.pid,
            tp.orca_id as pid_orca,
            ai.marketval,
            ai.memo,
            greatest(coalesce(rd.deptkey,0), coalesce(rb.deptkey,0)) as deptkey,
            rd.trankeygen_id as donor_rds_id,
            rd.orca_id as donor_orca_id,
            rd.name as donor,
            rd.contid as donor_contid,
            rd.contid_orca as donor_contid_orca,
            rd.amount as donor_donation,
            rd.elekey as donor_elekey,
            rd.checkno as donor_checkno,
            rb.name as buyer,
            rb.trankeygen_id as buyer_rds_id,
            rb.orca_id as buyer_orca_id,
            rb.contid as buyer_contid,
            rb.contid_orca as buyer_contid_orca,
            rb.amount as buyer_donation,
            rb.elekey as buyer_elekey,
            rb.checkno as buyer_checkno,
            case when rd.amount > 0 or rb.amount > 0 then rd.amount + rb.amount end as sale_price,
            rb.date_
        from private.auctionitems ai
                 join private.trankeygen t on t.trankeygen_id=ai.trankeygen_id
                 join private.trankeygen tp on tp.trankeygen_id = ai.pid
                 join private.vreceipts rd on ai.trankeygen_id=rd.pid and rd.type=7
                 left join private.vreceipts rb on rb.pid = ai.trankeygen_id and rb.type=8
        WHERE ai.trankeygen_id = p_trankeygen_id
          and t.fund_id=p_fund_id) v;
    return v_json;
    END;
$$ language plpgsql;