CREATE OR REPLACE FUNCTION private.cf_save_auction(p_fund_id integer, p_json json) returns integer
  language plpgsql as $$
declare
  v_auction_id integer;
begin
  IF p_json->>'name' is null THEN
    RAISE 'Missing auction name' using column='name';
  end if;

  v_auction_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json ->>'trankeygen_id' as integer));

  INSERT INTO private.contacts(trankeygen_id, type, name, street, city, state, zip, memo)
  VALUES (
          v_auction_id,
          'HDN',
          p_json ->> 'name',
          p_json ->> 'street',
          p_json ->> 'city',
          p_json ->> 'state',
          p_json ->> 'zip',
          p_json ->> 'memo'
         )
  ON CONFLICT(trankeygen_id) do update SET
     name=excluded.name,
       street=excluded.street,
       city=excluded.city,
       state=excluded.state,
       zip=excluded.zip,
       memo=excluded.memo;

  INSERT INTO private.accounts(trankeygen_id, acctnum, contid, style, memo, date_) values
      (v_auction_id,
       4098,
       v_auction_id,
       3,
       p_json ->> 'memo',
       to_date(p_json ->> 'date_', 'YYYY-MM-DD')
      )
  on conflict (trankeygen_id) do update
      set memo = excluded.memo,
          date_ = excluded.date_;
  return v_auction_id;
end
$$;
