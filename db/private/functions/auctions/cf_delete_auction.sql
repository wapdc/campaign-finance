CREATE OR REPLACE FUNCTION private.cf_delete_auction(p_auction_id integer) returns void as $$
    declare
        v_deposit_id integer;
        v_fund_id integer;

        begin

            select max(di.deptid) into v_deposit_id from private.auctionitems ai
                join private.vreceipts rd on rd.pid = ai.trankeygen_id
                join private.deposititems di on di.rcptid = rd.trankeygen_id
            where ai.pid = p_auction_id;

        if v_deposit_id is not null then
            raise 'Cannot delete an auction that has auction items deposited.';
        end if;

            -- delete receipts for auction items
        perform private.cf_delete_receipts_for_pid(trankeygen_id) from private.auctionitems ai
            where ai.pid = p_auction_id;
            -- delete auction item itself
        delete from private.auctionitems ai where ai.pid = p_auction_id;
            -- delete subsequent trankeygen id
        delete from private.trankeygen t where t.trankeygen_id in (select trankeygen_id from private.vaccounts v where v.trankeygen_id = p_auction_id);
    end

    $$ language plpgsql