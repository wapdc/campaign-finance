CREATE OR REPLACE FUNCTION private.cf_delete_auction_item(p_fund_id integer, p_id integer) returns void as
$$
    BEGIN

        perform private.cf_delete_receipts_for_pid(p_id);
        delete from private.trankeygen t where t.fund_id = p_fund_id and t.trankeygen_id = p_id;
    END;
$$ language plpgsql;