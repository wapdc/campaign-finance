CREATE OR REPLACE FUNCTION private.cf_save_credit_card_payment(p_fund_id int, p_json json) returns int as $$
declare
  v_payment_id integer;
  v_interest_id integer;
  v_credit_card_id integer;
  v_interest_account integer;
  v_did integer;
  v_amount numeric;
  v_interest_amount numeric;
  v_date date;
  v_old_amount numeric(16,2);
  v_old_interest_amount numeric(16,2);
  v_old_cid integer;
  v_old_did integer;
begin
  select trankeygen_id into v_credit_card_id from private.vaccounts a
  where a.fund_id = p_fund_id and a.trankeygen_id = cast(p_json->>'pid' as integer);

  IF v_credit_card_id is null then
    RAISE 'Invalid or missing credit card id';
  end if;

  select trankeygen_id into v_interest_account from private.vaccounts a
  where a.fund_id = p_fund_id and a.pid = cast(p_json->>'pid' as integer) and acctnum = 5110;

  IF v_interest_account is null then
    RAISE 'Missing credit card interest account id';
  end if;

  SELECT trankeygen_id into v_did from private.vaccounts where fund_id=p_fund_id and trankeygen_id=cast(p_json->>'did' as integer);

  if v_did is null then
    RAISE 'Invalid or missing account to pay from (did)';
  end if;

  v_payment_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));
  -- Get the interest transaction id
  select trankeygen_id into v_interest_id from private.vreceipts r where r.fund_id = p_fund_id
                                                                     and r.type = 30 and r.pid=v_payment_id;
  v_interest_id = private.cf_ensure_trankeygen(p_fund_id, v_interest_id);

  select cid,did,amount into v_old_cid, v_old_did, v_old_amount from private.vreceipts r where r.trankeygen_id=v_payment_id;
  select amount into v_old_interest_amount from private.vreceipts r where fund_id=p_fund_id and r.trankeygen_id=v_interest_id;


  v_amount = cast(p_json->>'amount' as numeric(16,2));
  v_interest_amount = coalesce(cast(p_json->>'interest' as numeric(16,2)), 0.0);
  v_date = cast(p_json->>'date_' as date);

  --insert credit card payment
  INSERT into private.receipts(trankeygen_id, type, nettype, pid, cid, did, contid,
                               amount, date_, memo)
  values (v_payment_id, 31, 3, v_credit_card_id, v_credit_card_id, v_did, v_credit_card_id, v_amount, v_date, p_json->>'memo')
  ON CONFLICT (trankeygen_id) DO UPDATE SET
                                          amount = excluded.amount,
                                          did=excluded.did,
                                          date_ = excluded.date_,
                                          memo = excluded.memo;
  --insert credit card interest
  INSERT INTO private.receipts(trankeygen_id, type, nettype, pid, cid, did, contid, amount, date_, description)
  values (v_interest_id, 30, 3, v_payment_id, v_interest_account, v_did, v_credit_card_id, v_interest_amount, v_date,'Interest on credit card')
  ON conflict (trankeygen_id) DO UPDATE SET
                                          amount=excluded.amount,
                                          date_ = excluded.date_,
                                          did = excluded.did;

  --update account amounts
  perform private.cf_account_adjust(p_fund_id , v_old_did, v_old_cid,  v_old_amount, v_did, v_credit_card_id, v_amount);
  perform private.cf_account_adjust(p_fund_id, v_old_did, v_interest_account, v_old_interest_amount, v_did, v_interest_account, v_interest_amount);

  return v_payment_id;

end
$$ language plpgsql;