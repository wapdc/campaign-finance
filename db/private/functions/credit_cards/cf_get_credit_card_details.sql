CREATE OR REPLACE FUNCTION private.cf_get_credit_card_details(p_fund_id integer, p_trankeygen_id integer) returns json as $$
    declare
        v_json json;
        v_expenses json;
        v_payments json;
    begin
        select json_agg(p order by p.date_ desc, p.trankeygen_id desc) INTO v_payments FROM
           (SELECT r.*, ri.amount as interest
                FROM private.vreceipts r
                    left join private.vreceipts ri on ri.pid=r.trankeygen_id
                    and ri.type=30
                where r.fund_id = p_fund_id
                  and r.pid = p_trankeygen_id
                  and r.type = 31
               ) p;

        select json_agg(e order by e.date_ desc, e.trankeygen_id desc, amount) INTO v_expenses FROM
           (SELECT date_, trankeygen_id, contid, amount, name, 'Expenditure' as type, uses_surplus_funds
               FROM private.vexpenditureevents ex
               where fund_id = p_fund_id and bnkid = p_trankeygen_id
               union all
               select r.date_, r.trankeygen_id, r.contid, r.amount + coalesce(i.amount,0.00) as amount, r.name, 'Debt payment', null from private.vreceipts r
                  left join private.vreceipts i on i.pid=r.trankeygen_id and i.type=30
                  where r.fund_id = p_fund_id and r.did = p_trankeygen_id
                    and r.type = 29
                ) e;

        select row_to_json(c) into v_json from ( select
            (select row_to_json(a) from private.vaccounts a where a.fund_id = p_fund_id and a.trankeygen_id = p_trankeygen_id) as account,
            v_payments AS payments,
            v_expenses AS expenses
        ) c;

        return v_json;
    end
    $$ language plpgsql;