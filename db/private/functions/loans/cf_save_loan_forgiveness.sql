CREATE OR REPLACE function private.cf_save_loan_forgiveness(p_fund_id integer, p_json JSON)
returns integer as $$
    declare
        v_loan_id                   integer;
        v_forgiven_id               integer;
        v_contid                    integer;
        v_old_cid                   integer;
        v_old_did                   integer;
        v_old_amount                numeric(16,2);
        v_old_contid                integer;
        v_cid                       integer;
        v_did                       integer;
        v_amount                    numeric(16,2);
        v_date                      date;
        BEGIN

        select trankeygen_id, contid into v_loan_id, v_contid from private.vreceipts r
        where r.fund_id = p_fund_id
        and r.trankeygen_id = cast(p_json->>'pid' as integer);

        IF v_loan_id is null then
            RAISE 'Invalid or missing loan id';
        end if;

        -- Get loan forgiven account
        SELECT trankeygen_id into v_did from private.vaccounts where pid=v_loan_id and acctnum in (4000, 4100);

        if v_did is null then
            RAISE 'Missing forgiveness account';
        end if;

        -- Initial loan account
        SELECT trankeygen_id into v_cid from private.vaccounts where pid=v_loan_id and acctnum in (2100, 2200);

        v_forgiven_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));

        select cid,did,amount, contid into v_old_cid, v_old_did, v_old_amount, v_old_contid from private.vreceipts r where r.trankeygen_id=v_forgiven_id;

        v_amount = cast(p_json->>'amount' as numeric(16,2));
        v_date = cast(p_json->>'date_' as date);

        INSERT into private.receipts(trankeygen_id, type, pid, cid, did, contid,
                                     amount, date_, memo)
        values (v_forgiven_id, 39, v_loan_id, v_cid, v_did, v_contid, v_amount, v_date,
                 p_json->>'memo')
        ON CONFLICT (trankeygen_id) DO UPDATE SET
                                                  date_ = excluded.date_,
                                                  amount = excluded.amount,
                                                  contid = excluded.contid,
                                                  memo = excluded.memo;
        --adjust aggregates
        if (v_old_contid != v_contid) then
          perform private.cf_update_aggregates(v_old_contid);
        end if;
        perform private.cf_update_aggregates(v_contid);
        perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, v_cid, v_amount);

        return v_forgiven_id;

        END
    $$ language plpgsql