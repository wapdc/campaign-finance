CREATE OR REPLACE function private.cf_save_loan_payment(p_fund_id integer, p_json JSON) returns integer as $$
  declare
    v_loan_id integer;
    v_payment_id integer;
    v_contid integer;
    v_old_cid integer;
    v_old_did integer;
    v_old_interest_amount numeric(16,2);
    v_interest_account_id integer;
    v_interest_id integer;
    v_old_amount numeric(16,2);
    v_old_contid integer;
    v_interest_amount numeric (16,2);
    v_cid integer;
    v_did integer;
    v_amount numeric(16,2);
    v_date date;
    v_deptkey bigint;
    BEGIN
      select trankeygen_id, contid into v_loan_id, v_contid from private.vreceipts r where r.fund_id = p_fund_id
        and r.trankeygen_id = cast(p_json->>'pid' as integer);

      IF v_loan_id is null then
        RAISE 'Invalid or missing loan id';
      end if;

      -- Validate bank account
      SELECT trankeygen_id into v_did from private.vaccounts where fund_id=p_fund_id and trankeygen_id=cast(p_json->>'did' as integer);

      if v_did is null then
          RAISE 'Invalid or account to pay from (did)';
      end if;

      -- Get initial loan account
      select trankeygen_id into v_cid from private.vaccounts where pid=v_loan_id and acctnum in (2100,2200);

      -- Get loan interest account
      select trankeygen_id into v_interest_account_id from private.vaccounts where pid=v_loan_id and acctnum = 5110;

      if (v_interest_account_id is null or v_cid is null ) then
          raise 'Missing loan account or interest account ';
      end if;


      v_payment_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));

      -- Get the interest transaction id
      select trankeygen_id into v_interest_id from private.vreceipts r where r.fund_id = p_fund_id
        and r.type = 30 and r.pid=v_payment_id;
      v_interest_id = private.cf_ensure_trankeygen(p_fund_id, v_interest_id);

      select cid,did,amount, contid into v_old_cid, v_old_did, v_old_amount, v_old_contid from private.vreceipts r where r.trankeygen_id=v_payment_id;

      select amount into v_old_interest_amount from private.vreceipts r where fund_id=p_fund_id and r.trankeygen_id=v_interest_id;

      v_amount = cast(p_json->>'amount' as numeric(16,2));
      v_interest_amount = coalesce(cast(p_json->>'interest' as numeric(16,2)), 0.0);
      v_date = cast(p_json->>'date_' as date);

      v_deptkey = private.cf_generate_deposit_key(p_fund_id, v_date);

      INSERT into private.receipts(trankeygen_id, type, aggtype, nettype, pid,  cid, did, contid,
                                   amount, date_, checkno, description, memo, deptkey)
        values (v_payment_id, 26, -1, 1, v_loan_id, v_cid, v_did, v_contid, v_amount, v_date ,
            p_json->>'checkno', 'Loan payment', p_json->>'memo', v_deptkey)
      ON CONFLICT (trankeygen_id) DO UPDATE SET
            amount = excluded.amount,
            contid = excluded.contid,
            did=excluded.did,
            checkno = excluded.checkno,
            date_ = excluded.date_,
            memo = excluded.memo,
            deptkey = v_deptkey;

      -- Insert interest transaction
      INSERT INTO private.receipts(trankeygen_id, type, nettype, pid, cid, did, contid, amount, date_, checkno, description)
        values (v_interest_id, 30, 3, v_payment_id, v_interest_account_id, v_did, v_contid,
                v_interest_amount, v_date, p_json->>'checkno','Interest on loan')
      ON conflict (trankeygen_id) DO UPDATE SET
         amount=excluded.amount,
         contid=excluded.contid,
         date_ = excluded.date_,
         did = excluded.did,
         checkno = excluded.checkno;

      --adjust aggregates
      if (v_old_contid != v_contid) then
        perform private.cf_update_aggregates(v_old_contid);
      end if;
      perform private.cf_update_aggregates(v_contid);
      -- Adjust accounting
      perform private.cf_account_adjust(p_fund_id , v_old_did, v_old_cid,  v_old_amount, v_did, v_cid, v_amount);
      perform private.cf_account_adjust(p_fund_id, v_old_did, v_interest_account_id, v_old_interest_amount, v_did, v_interest_account_id, v_interest_amount);
      return v_payment_id;
    END
$$ language plpgsql
