CREATE OR REPLACE FUNCTION private.cf_get_loan(p_fund_id INTEGER, p_trankeygen_id integer) returns JSON as $$
DECLARE
    j_transaction json;

BEGIN
    select row_to_json(r) into j_transaction from
      (select
        rec.*,
        case when rec.carryforward=1 then l.date_ end as original_loan_date,
        l.interestrate,
        l.repaymentschedule,
        l.duedate,
        -1 * la.total as balance,
        de.date_ as deposit_date,
        la.trankeygen_id as loan_account_id,
        la.orca_id as loan_account_orca_id,
        fa.trankeygen_id as forgiveness_account_id,
        fa.orca_id as forgiveness_account_orca_id,
        ia.trankeygen_id as interest_account_id,
        ia.orca_id as interest_account_orca_id
      from private.vreceipts rec
        join private.loans l on l.trankeygen_id=rec.trankeygen_id
        left join private.vdepositevents de on de.fund_id = p_fund_id and de.deptkey=rec.deptkey
        left join private.vaccounts la on la.pid=l.trankeygen_id and la.acctnum in (2100,2200)
        left join private.vaccounts fa on fa.pid=l.trankeygen_id and fa.acctnum in (4000, 4100)
        left join private.vaccounts ia on ia.pid=l.trankeygen_id and ia.acctnum = 5110
      where rec.trankeygen_id = p_trankeygen_id and rec.fund_id = p_fund_id) r;

    RETURN j_transaction;
END
$$ language plpgsql
