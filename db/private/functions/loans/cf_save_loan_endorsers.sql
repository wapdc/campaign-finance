CREATE OR REPLACE function private.cf_save_loan_endorser(p_fund_id integer, p_json JSON)
    returns integer as $$
declare
    v_loan_id                   integer;
    v_endorser_id               integer;
    v_contid                    integer;
    v_elekey                    smallint;
    v_did                       integer;
    v_amount                    numeric(16,2);
    v_date                      date;
BEGIN

    select trankeygen_id, date_ into v_loan_id, v_date from private.vreceipts r
    where r.fund_id = p_fund_id
      and r.trankeygen_id = cast(p_json->>'pid' as integer);

    IF v_loan_id is null then
        RAISE 'Invalid or missing loan id';
    end if;

    -- Get junk account
    SELECT trankeygen_id into v_did from private.vaccounts where acctnum = 0 and fund_id = p_fund_id;

    -- Get contid
    SELECT trankeygen_id into v_contid from private.vcontacts where trankeygen_id = cast(p_json->>'contid' as integer) and fund_id = p_fund_id;

    if v_contid is null then
      RAISE 'Invalid or missing contact';
    end if;

    v_endorser_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));

    v_elekey := cast(p_json->>'elekey' as smallint);
    v_amount := cast(p_json->>'amount' as numeric(16,2));

    INSERT into private.receipts(trankeygen_id, type, pid, cid, did, contid,
                                 amount, date_, memo, elekey)
    values (v_endorser_id, 9, v_loan_id, v_did, v_did, v_contid, v_amount, v_date,
            p_json->>'memo', v_elekey)
    ON CONFLICT (trankeygen_id) DO UPDATE SET
                                              contid = excluded.contid,
                                              amount = excluded.amount,
                                              memo = excluded.memo,
                                              elekey = excluded.elekey;

    return v_endorser_id;

END
$$ language plpgsql