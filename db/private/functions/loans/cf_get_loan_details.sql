CREATE OR REPLACE FUNCTION private.cf_get_loan_details(p_fund_id integer, p_trankeygen_id integer) returns json as $$
  declare
      v_json JSONB;
      v_payments json;
      v_endorsers json;
      v_forgiveness json;
  begin

      SELECT json_agg(p order by p.date_ desc ) into v_payments FROM (SELECT r.*, ri.amount as interest
                             FROM private.vreceipts r
                               left join private.vreceipts ri on ri.pid=r.trankeygen_id
                                 and ri.type=30
                             where r.fund_id = p_fund_id
                               and r.pid = p_trankeygen_id
                               and r.type=26
                             ) p;

      SELECT json_agg(e order by e.name ) into v_endorsers FROM (select r.*
                             FROM private.vreceipts r
                             where r.fund_id = p_fund_id
                               and r.pid = p_trankeygen_id
                               and r.type=9
                             ) e;

      SELECT json_agg(f order by f.date_ desc) into v_forgiveness FROM (SELECT R.*
                             FROM private.vreceipts r
                             where r.fund_id = p_fund_id
                               and r.pid = p_trankeygen_id
                               and r.type=39
                                                                ) f;

      SELECT row_to_json(l) into v_json from (SELECT
                                                  private.cf_get_loan(p_fund_id, p_trankeygen_id) as loan,
                                                  v_endorsers as endorsers,
                                                  v_payments as payments,
                                                  v_forgiveness as forgiveness
                                             ) l;
      return  v_json;

  end
$$ language plpgsql;