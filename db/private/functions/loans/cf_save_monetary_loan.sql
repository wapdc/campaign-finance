CREATE OR REPLACE function private.cf_save_monetary_loan(p_fund_id INTEGER, p_json JSON) RETURNS INTEGER as $$
  declare
    v_loan_id INTEGER;
    v_cid INTEGER;
    v_loan_account_id INTEGER;
    v_loan_interest_account_id INTEGER;
    v_forgiveness_account_id INTEGER;
    v_deposit_id INTEGER;
    v_old_cid INTEGER;
    v_old_did INTEGER;
    v_old_amount NUMERIC(16,2);
    v_old_contid INTEGER;
    v_contid INTEGER;
    v_date DATE;
    v_elekey integer;
    v_amount numeric(16,2);
    v_cid_acctnum integer;
  begin
    v_loan_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));


    -- Validate contact
    SELECT trankeygen_id INTO v_contid  FROM private.vcontacts where fund_id = p_fund_id and trankeygen_id=CAST(p_json->>'contid' as INTEGER);
    IF v_contid IS NULL THEN
        raise 'Invalid or missing contact id';
    end if;

    -- Get current values for accounting
    SELECT cid, did, amount, deposit_id, contid  into v_old_cid, v_old_did, v_old_amount, v_deposit_id, v_old_contid from private.vreceipts WHERE trankeygen_id=v_loan_id;
    v_date = cast(p_json->>'date_' as date);
    v_amount = cast(p_json->>'amount' as numeric(16,2));
    v_elekey = cast(p_json->>'elekey' as integer);
    if (cast(p_json->>'carryforward' as integer) = 1) then
        -- bank account
        v_cid_acctnum = 1000;
    else
        -- undeposited funds
        v_cid_acctnum = 1600;
    end if;

    -- Default
    SELECT MIN(trankeygen_id) INTO v_cid from private.vaccounts where fund_id = p_fund_id and acctnum=v_cid_acctnum;

    -- Ensure base loan account
    SELECT trankeygen_id into v_loan_account_id FROM private.vaccounts where fund_id = p_fund_id and pid=v_loan_id
      and acctnum=2100;
    v_loan_account_id = private.cf_ensure_trankeygen(p_fund_id, v_loan_account_id);
    INSERT INTO private.accounts(trankeygen_id, pid, acctnum, contid, style, total, date_)
      VALUES (v_loan_account_id, v_loan_id,2100 , v_contid, 3, 0.0,  v_date )
    ON CONFLICT(trankeygen_id)  DO UPDATE set date_ = excluded.date_;

    -- Ensure forgiveness account
    SELECT trankeygen_id into v_forgiveness_account_id FROM private.vaccounts where fund_id = p_fund_id and pid=v_loan_id
                                                                         and acctnum=4000;
    v_forgiveness_account_id = private.cf_ensure_trankeygen(p_fund_id, v_forgiveness_account_id);
    INSERT INTO private.accounts(trankeygen_id, pid, acctnum, contid, style, total, date_)
    VALUES (v_forgiveness_account_id, v_loan_id, 4000 , v_contid, 3, 0.0,  v_date )
    ON CONFLICT(trankeygen_id)  DO UPDATE set date_ = excluded.date_;

    -- Set up interest account
    SELECT trankeygen_id into v_loan_interest_account_id FROM private.vaccounts where fund_id = p_fund_id and pid=v_loan_id
                                                                         and acctnum=5110;
    v_loan_interest_account_id = private.cf_ensure_trankeygen(p_fund_id, v_loan_interest_account_id);
    INSERT INTO private.accounts(trankeygen_id, pid, acctnum, contid, style, total, date_)
    VALUES (v_loan_interest_account_id, v_loan_id, 5110 , v_contid, 3, 0.0, v_date )
    ON CONFLICT(trankeygen_id) DO UPDATE set date_ = excluded.date_;

    INSERT INTO private.loans(trankeygen_id, type, cid, did, contid, amount, date_, elekey, interestrate, duedate, checkno, repaymentschedule, description, memo, carryforward)
      VALUES (
        v_loan_id,
        3, v_cid,
        v_loan_account_id,
        v_contid, v_amount,
        case when p_json->>'carryforward' = '1' then cast(p_json->>'original_loan_date' as date) else v_date end,
        v_elekey,
        cast(p_json->>'interestrate' as numeric),
        cast(p_json->>'duedate' as date),
        p_json->>'checkno',
        p_json->>'repaymentschedule',
        p_json->>'description',
        p_json->>'memo',
        cast(p_json->>'carryforward' as integer)
      )
    on conflict(trankeygen_id) do update
        set amount = excluded.amount,
            contid = excluded.contid,
            date_ = excluded.date_,
            interestrate = excluded.interestrate,
            duedate = excluded.duedate,
            checkno = excluded.checkno,
            repaymentschedule = excluded.repaymentschedule,
            description = excluded.description,
            memo = excluded.memo,
            carryforward = excluded.carryforward;

    INSERT INTO private.receipts(
         trankeygen_id,
         type,
         aggtype,
         nettype,
         cid,
         did,
         contid,
         amount,
         date_,
         elekey,
         checkno,
         description,
         memo,
         carryforward,
         deptkey,
         data
    )
    VALUES (
           v_loan_id,
           3,
           1,
           1,
           v_cid,
           v_loan_account_id,
           v_contid,
           v_amount,
           v_date,
           v_elekey,
           p_json->>'checkno',
           p_json->>'description',
           p_json->>'memo',
           cast(p_json->>'carryforward' as int),
           0,
           cast(p_json->>'data' as json)
    )
    on conflict(trankeygen_id)
    do update set
                date_ = excluded.date_,
                amount = excluded.amount,
                contid = excluded.contid,
                elekey = excluded.elekey,
                description=excluded.description,
                memo=excluded.memo,
                carryforward = excluded.carryforward,
                data = excluded.data;

    update private.receipts r set amount = v_amount where pid=v_loan_id and type = 9;

    --adjust aggregates
    if (v_old_contid != v_contid) then
      perform private.cf_update_aggregates(v_old_contid);
    end if;
    perform private.cf_update_aggregates(contid) from private.receipts r where r.pid=v_loan_id and r.type=9;

    perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_loan_account_id, coalesce(v_old_cid, v_cid) , v_amount );

    IF v_deposit_id IS NOT NULL THEN
        PERFORM private.cf_recalculate_deposit_event(p_fund_id, v_deposit_id);
    END IF;

    return v_loan_id;
  end
$$ language plpgsql;
