CREATE OR REPLACE FUNCTION private.cf_delete_loan(p_loan_id integer) returns void as $$
  declare
    v_deposit_id integer;
    v_fund_id integer;
  begin
    -- make sure it is not deposited
    select di.deptid, r.fund_id into v_deposit_id, v_fund_id from private.vreceipts r
      left join private.deposititems di ON di.rcptid=r.trankeygen_id
      where r.trankeygen_id=v_deposit_id;

    if v_deposit_id is not null then
        raise 'Cannot delete a loan that has been deposited.';
    end if;

    perform private.cf_delete_receipts_for_pid(p_loan_id);
    perform private.cf_delete_receipt(p_loan_id);

    delete from private.trankeygen where trankeygen_id in (
        select trankeygen_id from private.vaccounts where pid=p_loan_id
        );

  end
$$ language plpgsql;