DROP FUNCTION IF EXISTS private.cf_get_expenditure_data(p_fund_id INTEGER, p_trankeygen_id INTEGER);
CREATE OR REPLACE FUNCTION private.cf_get_expenditure_data(p_fund_id INTEGER, p_trankeygen_id INTEGER) RETURNS text AS $$
  DECLARE
    j_expenditure_event json;
    j_expenses json;
    j_info json;

  BEGIN

    SELECT json_agg(r order by r.trankeygen_id) INTO j_expenses FROM (select * from private.vreceipts r
      WHERE pid = p_trankeygen_id and fund_id = p_fund_id and r.type=28) r;

    SELECT row_to_json(j) INTO j_expenditure_event
      FROM (select
              e.*,
              a.total * -1 as balance
            FROM
              private.vexpenditureevents e
              left join private.vaccounts a on a.acctnum = 2000 and a.pid = p_trankeygen_id and a.fund_id = p_fund_id
            where e.trankeygen_id = p_trankeygen_id and e.fund_id = p_fund_id) j;

    select row_to_json( j ) from (SELECT j_expenditure_event as "expenditureEvent",
           j_expenses as "expenses") j
           INTO j_info;

    RETURN cast (j_info as text);
    END
$$ LANGUAGE plpgsql STABLE