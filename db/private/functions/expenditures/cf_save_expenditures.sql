CREATE OR REPLACE FUNCTION private.cf_save_expenditures(p_fund_id integer, json_data json)
    RETURNS integer as
$$
DECLARE
    e record;
    expenditureEvent json;
    expenses json;
    v_expenditure_event_id integer;
    v_bank_id integer;
    v_expense_id integer;
    v_itemized smallint;
    v_expenditure_event_amount numeric;
    v_uses_surplus_funds boolean;
    v_acctnum integer;
    v_nettype integer = 3;
    v_date date;
    v_contid integer;
    v_contid_name VARCHAR;

BEGIN

    v_uses_surplus_funds := false;
    v_expenditure_event_amount = 0;
    expenditureEvent = json_data -> 'expenditure' -> 'expenditureEvent';
    expenses = json_data -> 'expenditure' -> 'expenses';
    v_bank_id = cast(expenditureEvent ->> 'bnkid' as integer);
    if(v_bank_id > 0) then
        select acctnum into v_acctnum from private.accounts where trankeygen_id = v_bank_id;
    else
        v_acctnum = cast(expenditureEvent->>'acctnum' as integer);
    end if;
    v_date = to_date(expenditureEvent ->> 'date_', 'YYYY-MM-DD');

    select c.trankeygen_id, c.name into v_contid, v_contid_name from private.contacts c where c.trankeygen_id = cast(expenditureEvent->>'contid' as integer);

    v_itemized = expenditureEvent -> 'itemized';
    v_expenditure_event_id = private.cf_ensure_trankeygen(p_fund_id, cast(expenditureEvent ->> 'rds_id' as integer));

    -- Make sure that debt based expenses get recorded with a nettype of 0
    if v_acctnum in (2000,2600) then
        v_nettype = 0;
    end if;
    if v_acctnum = 2000 then
        select er.bnkid into v_bank_id from private.vexpenditureevents er where trankeygen_id=v_expenditure_event_id;
        if v_bank_id is null then
            v_bank_id = private.cf_ensure_trankeygen(p_fund_id, v_bank_id);
            insert into private.accounts(trankeygen_id, pid, contid, acctnum, style, date_)
            VALUES (v_bank_id, v_expenditure_event_id, v_contid, v_acctnum, 3, v_date);
        else
            update private.accounts set contid=v_contid, date_ = v_date where trankeygen_id=v_bank_id;
        end if;
    else
        select trankeygen_id into v_bank_id from private.vaccounts where fund_id=p_fund_id and trankeygen_id=v_bank_id;
    end if;


    if v_bank_id is null then
        raise 'Invalid debit account' using column='bnk_id';
    end if;


    -- upsert an expenditure event
    insert into private.expenditureevents(trankeygen_id, contid, bnkid, date_, checkno, itemized, memo)
    values (
               v_expenditure_event_id,
               v_contid,
               v_bank_id,
               v_date,
               expenditureEvent ->> 'checkno',
               cast(expenditureEvent ->> 'itemized' as smallint),
               expenditureEvent ->> 'memo'
           )
    on conflict (trankeygen_id) do update
        set bnkid=excluded.bnkid,
            contid=excluded.contid,
            amount=excluded.amount,
            date_=excluded.date_,
            checkno=excluded.checkno,
            itemized=excluded.itemized,
            memo=excluded.memo;

    -- remove unused trankeygen_id records for this expenditure event
    -- also back out transactions while deleting them.
    delete from private.trankeygen
    where trankeygen_id in (
        select v.trankeygen_id
        from ( select t.trankeygen_id, private.cf_account_transfer_funds(t.fund_id,  r.cid, r.did, r.amount) from private.trankeygen t
                                                                                                                      join private.receipts r on r.trankeygen_id=t.trankeygen_id
                                                                                                                      left join (
            select j ->> 'trankeygen_id' as trankeygen_id
            from json_array_elements(expenses) j
        ) je on t.trankeygen_id = cast(je.trankeygen_id as int)
               where t.pid = v_expenditure_event_id
                 and je.trankeygen_id is null
                 and fund_id = p_fund_id) v);

    -- iterate over expenses
    for e in (
        -- grab the data required in the upcoming loop
        select
            a.trankeygen_id as cid,
            c.trankeygen_id as contid,
            cast(j->>'amount' as numeric(16,2)) as amount,
            -- description is calculated
            case
                when c.trankeygen_id <> cast(expenditureEvent ->> 'contid' as integer) then
                    -- subvendor expenses
                    case
                        when ad.ad_id is not null
                            -- include ad info
                            then left(concat('Ad ran on ', ad.outlets, ', first run date ', ad.first_run_date, '. ', j->>'user_description'), 1000)
                        else
                            -- default description
                            left(concat(j->>'user_description'), 1000)
                        end
                else
                    -- main vendor expenses
                    case
                        when ad.ad_id is not null
                            -- include ad info
                            then left(concat('Ad ran on ', ad.outlets, ', first run date ', ad.first_run_date, '. ', j->>'user_description'), 1000)
                        else
                            -- default description
                            left(j->>'user_description', 1000)
                        end
                end as description,
            j->>'rds_id' as rds_id,
            j->>'memo' as memo,
            j->>'user_description' as user_description,
            j->>'ad_id' as ad_id,
            r.cid as old_cid,
            r.did as old_did,
            r.amount as old_amount,
            ec.cat_lookup
        from json_array_elements(expenses) j
                 left join private.trankeygen ta
                           on ta.fund_id = p_fund_id
                               and ta.trankeygen_id = cast(j->>'cid' as integer)
                 left join private.accounts a
                           on a.trankeygen_id = ta.trankeygen_id
                 left join private.receipts r
                           on r.trankeygen_id = cast(j->>'rds_id' as integer)
                 left join private.contacts c
                           on c.trankeygen_id = cast(j ->> 'contid' as integer)
                 left join public.expense_category ec
                           on ec.code = a.code
                 left join private.ad ad
                           on ad.ad_id = cast(j->>'ad_id' as integer)
    )

        loop
            if e.cat_lookup = 'surplus'
            then v_uses_surplus_funds := true;
            end if;
            if e.contid is null then
                raise 'Invalid contid' using column='contid';
            end if;
            if e.cid is null then
                raise 'Invalid cid' using column='cid';
            end if;
            v_expenditure_event_amount = v_expenditure_event_amount + cast(e.amount as numeric);
            v_expense_id = private.cf_ensure_trankeygen(p_fund_id, cast(e.rds_id as integer));
            if exists (
                    select trankeygen_id
                    from private.trankeygen t
                    where trankeygen_id = v_expense_id
                      and pid != v_expenditure_event_id
                )
            then v_expense_id = private.cf_ensure_trankeygen(p_fund_id, null);
            end if;

            --  upsert receipts
            insert into private.receipts as ipr (
                trankeygen_id, type, nettype, pid, cid, did, contid, amount, date_, itemized, description, memo, user_description
            )
            values (
                       v_expense_id,
                       28,
                       v_nettype,
                       v_expenditure_event_id,
                       e.cid,
                       v_bank_id, e.contid,
                       cast(e.amount as numeric),
                       v_date,
                       v_itemized,
                       e.description,
                       e.memo,
                       e.user_description
                   )
            on conflict (trankeygen_id) do update
                set nettype=excluded.nettype,
                    cid=excluded.cid,
                    did=excluded.did,
                    contid=excluded.contid,
                    amount=excluded.amount,
                    date_=excluded.date_,
                    itemized=excluded.itemized,
                    description=excluded.description,
                    memo=excluded.memo,
                    user_description=excluded.user_description
            where ipr.pid = excluded.pid;

            -- change trankeygen to the correct expenditure event
            update private.trankeygen
            set pid = v_expenditure_event_id
            where trankeygen_id = v_expense_id;

            -- Perform adjustments for accounts
            PERFORM private.cf_account_adjust(p_fund_id, e.old_did, e.old_cid, e.old_amount, v_bank_id, e.cid, e.amount);

            -- remove ad expenditures that are no longer needed
            delete from private.ad_expenditure ae
                using private.ad a
            where ae.expense_id = v_expense_id
              and ae.ad_id is distinct from cast(e.ad_id as int)
              and a.fund_id = p_fund_id;

            if (e.ad_id is not null) then
                -- populate ad expenditure if not present
                insert into private.ad_expenditure (expense_id, ad_id)
                values (v_expense_id, cast(e.ad_id as integer))
                on conflict do nothing;
            end if;
        end loop;

    -- change date updated on this trankeygen
    update private.trankeygen
    set rds_updated = now()
    where trankeygen_id = v_expenditure_event_id;

    -- set calculated data on expenditure event
    update private.expenditureevents
    set amount = v_expenditure_event_amount,
        uses_surplus_funds=v_uses_surplus_funds
    where trankeygen_id = v_expenditure_event_id;

    return v_expenditure_event_id;

END
$$ LANGUAGE plpgsql;