/*
  Used to delete expenditure events and vendor debt.
 */
CREATE OR REPLACE FUNCTION private.cf_delete_expenditure(p_fund_id integer, p_expenditure_id integer) returns void as $$
  declare
    expenditure_trankeygen integer;
    v_deposit_id integer;
    v_acctnum integer;
    v_transaction_count integer;
begin

  -- make sure expenditure event is in campaign
  select trankeygen_id,acctnum into expenditure_trankeygen, v_acctnum from private.vexpenditureevents where fund_id = p_fund_id and trankeygen_id = p_expenditure_id;

  if expenditure_trankeygen is null then
    raise 'missing item to delete';
  end if;

  -- make sure it is not deposited
  select di.deptid into v_deposit_id from private.vreceipts r
    left join private.deposititems di on di.rcptid=r.trankeygen_id
  where r.pid = expenditure_trankeygen;

  if v_deposit_id is not null then
    raise 'cannot delete deposited item';
  end if;

  -- If it is a vendor debt make sure there are no payments or forgiveness
  if v_acctnum = 2000 then
      select count(*) into v_transaction_count from private.vreceipts r where r.pid = p_expenditure_id
        and r.type in (29,44);
      if v_transaction_count > 0 then
          raise 'Cannot delete vendor debt when it has payments or forgiveness';
      end if;
      select count(*) into v_transaction_count from private.vreceipts r
           where r.pid in (select trankeygen_id from private.vreceipts r2 where r2.pid = p_expenditure_id and r2.type=28)
              and r.type in (20);
      if v_transaction_count > 0 then
          raise 'Cannot delete vendor debt when it has adjustments';
      end if;
  end if;

  -- do the deletions
  perform private.cf_delete_receipts_for_pid(expenditure_trankeygen);
  delete from private.accounts where pid = expenditure_trankeygen;
  delete from private.trankeygen where trankeygen_id = expenditure_trankeygen and fund_id = p_fund_id;

end
$$ language plpgsql;