CREATE OR REPLACE FUNCTION private.cf_delete_vendor_refund(p_id integer) returns void as
$$
declare
v_fund_id INTEGER;
v_cont_id INTEGER;
v_cid INTEGER;
v_did INTEGER;
v_amount NUMERIC(16,2);
begin
-- Get transaction information
select t.fund_id, r.contid, r.cid, r.did, r.amount INTO  v_fund_id, v_cont_id, v_cid, v_did, v_amount
FROM private.receipts r JOIN private.trankeygen t on r.trankeygen_id = t.trankeygen_id
WHERE r.trankeygen_id = p_id;

delete from private.receipts r where trankeygen_id=p_id;
delete from private.trankeygen t where t.trankeygen_id=p_id;
delete from private.contacts c where trankeygen_id = v_cont_id;

-- Back out the transaction.
perform private.cf_account_transfer_funds(v_fund_id, v_cid, v_did, v_amount);
end;
$$ language plpgsql;