--DROP function private.cf_save_vendor_refund(p_fund_id INTEGER, json_data JSON);
CREATE OR REPLACE FUNCTION private.cf_save_vendor_refund(p_fund_id INTEGER, json_data JSON)
  RETURNS int as
$$
DECLARE
  v_refund_id int;
  v_pid int;
  v_did int;
  v_cid int;
  v_contact int;
  v_old_amount numeric(16,2);
  v_old_cid int;
  v_old_did int;
  v_old_contid int;
BEGIN

  select trankeygen_id, contid into v_pid, v_contact FROM private.vreceipts
  where trankeygen_id = cast(json_data ->> 'pid' as integer) and fund_id = p_fund_id and type = 28;

  if (v_pid is null)
  THEN
    raise EXCEPTION  'Invalid pid';
  end if;

  select min(trankeygen_id) into v_cid from private.vaccounts
  where acctnum = 1600 and fund_id=p_fund_id;

  select min(trankeygen_id) into v_did from private.vaccounts
  where acctnum = 4300 and fund_id=p_fund_id;

  IF (v_contact is null)
  THEN
    raise  EXCEPTION 'Invalid contact for fund' using column = 'contid';
  end if;

  IF json_data->>'date_' is null THEN
    raise 'Missing date' using column = 'date';
  end if;

  v_refund_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));
  SELECT cid, did, amount, contid INTO v_old_cid, v_old_did, v_old_amount, v_old_contid FROM private.receipts r where trankeygen_id=v_refund_id;

  INSERT into private.receipts(trankeygen_id, type, contid, amount, date_, checkno, description, memo, cid, pid, did)
  values (v_refund_id,
          18,
          v_contact,
          cast(json_data ->> 'amount' as numeric),
          to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
          json_data ->> 'checkno',
          json_data ->> 'description',
          json_data ->> 'memo',
          v_cid,
          cast(json_data ->> 'pid' as integer),
          v_did)
  on conflict (trankeygen_id) do update
    set
      contid          = excluded.contid,
      amount          = excluded.amount,
      date_           = excluded.date_,
      checkno         = excluded.checkno,
      description     = excluded.description,
      memo            = excluded.memo;
  perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, coalesce(v_old_cid, v_cid),cast(json_data ->> 'amount' as numeric));
  if (v_old_contid != v_contact) then
    perform private.cf_update_aggregates(v_old_contid);
  end if;
  perform private.cf_update_aggregates(cast(json_data ->>'contid' as integer));
  RETURN v_refund_id;

END
$$ LANGUAGE plpgsql;