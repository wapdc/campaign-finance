\i ./accounts/cf_account_adjust.sql;
\i ./accounts/cf_account_transfer_funds.sql;
\i ./accounts/cf_get_account.sql;
\i ./accounts/cf_get_account_total.sql;
\i ./accounts/cf_save_account.sql;
\i ./accounts/cf_save_account_transfer.sql;

\i ./accounts/cf_update_carry_forward.sql;

\i ./advertisements/cf_get_advertising_data.sql;
\i ./advertisements/cf_save_advertisements.sql;

\i ./auctions/cf_delete_auction.sql;
\i ./auctions/cf_delete_auction_item.sql;
\i ./auctions/cf_get_auction_details.sql;
\i ./auctions/cf_get_auction_item.sql;
\i ./auctions/cf_save_auction.sql;
\i ./auctions/cf_save_auction_item.sql;
\i ./auctions/cf_sync_auction_items.sql;

\i ./campaigns/cf_create_backup.sql;
\i ./campaigns/cf_delete_orca_campaign.sql;
\i ./campaigns/cf_get_orca_campaign_restore.sql
\i ./campaigns/cf_push_changes.sql;
\i ./campaigns/cf_get_fund_data.sql;
\i ./campaigns/cf_get_import_fund_info.sql;
\i ./campaigns/cf_upgrade_campaign.sql;
\i ./campaigns/cf_upgrade_1400_campaign.sql;
\i ./campaigns/cf_upgrade_convert_old_cc_vendor_debt.sql;
\i ./campaigns/cf_sync_accounts.sql;
\i ./campaigns/cf_sync_contacts.sql;
\i ./campaigns/cf_sync_debt_obligations.sql;
\i ./campaigns/cf_sync_deposit_events.sql;
\i ./campaigns/cf_sync_expenditureevents.sql;
\i ./campaigns/cf_sync_loans.sql;
\i ./campaigns/cf_sync_receipts.sql;
\i ./campaigns/cf_validate_save.sql;
\i ./campaigns/cf_create_campaign.sql;
\i ./campaigns/cf_import_prior_campaign_data.sql;
\i ./campaigns/cf_import_prior_campaign_contacts.sql;
\i ./campaigns/cf_restore_campaign.sql;
\i ./campaigns/cf_update_campaign_vendor.sql;

\i ./contacts/cf_contact_find_duplicates.sql;
\i ./contacts/cf_get_contact.sql;
\i ./contacts/cf_get_contact_transactions.sql;
\i ./contacts/cf_merge_contacts.sql;
\i ./contacts/cf_modify_group_members.sql;
\i ./contacts/cf_save_contact.sql;
\i ./contacts/cf_save_contacts.sql;
\i ./contacts/cf_get_contact_from_contact_key.sql;
\i ./contacts/cf_update_aggregates.sql;
\i ./contacts/cf_validate_contact_contribution_limits.sql;

\i ./contributions/cf_get_monetary_group_contribution.sql;
\i ./contributions/cf_save_anonymous_contribution.sql;
\i ./contributions/cf_save_contribution.sql;
\i ./contributions/cf_save_contribution_refund.sql;
\i ./contributions/cf_save_contributions.sql;
\i ./contributions/cf_save_fundraiser_contribution.sql;
\i ./contributions/cf_save_inkind_contribution.sql;
\i ./contributions/cf_save_monetary_group_contribution.sql;
\i ./contributions/cf_save_split_contribution.sql;

\i ./corrections/cf_save_correction.sql;
\i ./corrections/cf_get_correction.sql;
\i ./corrections/cf_delete_correction.sql;

\i ./credit_cards/cf_save_credit_card_payment.sql;
\i ./credit_cards/cf_get_credit_card_details.sql;

\i ./debts/cf_get_debt_details.sql;
\i ./debts/cf_save_vendor_debt_forgiveness.sql;
\i ./debts/cf_get_invoiced_expenses.sql;
\i ./debts/cf_save_invoiced_expense_adjustment.sql;
\i ./debts/cf_save_invoiced_expense_payment.sql;

\i ./deposits/cf_recalculate_deposit_event.sql;
\i ./deposits/cf_generate_deposit_key.sql;
\i ./deposits/cf_save_depositevent.sql;
\i ./deposits/cf_get_depositevent.sql;
\i ./deposits/cf_delete_depositevent.sql;

\i ./elections/cf_save_election_codes.sql;

\i ./expenditures/cf_get_expenditure_data.sql;
\i ./expenditures/cf_delete_expenditure.sql
\i ./expenditures/cf_save_expenditures.sql;
\i ./expenditures/cf_save_vendor_refund.sql;
\i ./expenditures/cf_delete_vendor_refund.sql;

\i ./fundraisers/cf_delete_low_cost_fundraiser.sql;
\i ./fundraisers/cf_save_low_cost_fundraiser.sql;
\i ./fundraisers/cf_get_fundraiser.sql;
\i ./fundraisers/cf_get_fundraiser_details.sql;
\i ./fundraisers/cf_save_cash_fundraiser.sql;
\i ./fundraisers/cf_delete_cash_fundraiser.sql;

\i ./limits/cf_save_limits.sql;
\i ./limits/cf_populate_limits.sql;

\i ./loans/cf_save_monetary_loan.sql;
\i ./loans/cf_save_in_kind_loan.sql;
\i ./loans/cf_get_loan.sql;
\i ./loans/cf_get_loan_details.sql;
\i ./loans/cf_save_loan_forgiveness.sql;
\i ./loans/cf_save_loan_payment.sql;
\i ./loans/cf_delete_loan.sql;
\i ./loans/cf_save_loan_endorsers.sql;

\i ./misc/cf_save_misc_other.sql;
\i ./misc/cf_save_misc_bank_interest.sql;

\i ./orca/cf_purge_old_orca_ids.sql;
\i ./orca/cf_ensure_trankeygen.sql;

\i ./pages/cf_get_attachment_page_data.sql;

\i ./pledges/cf_save_cancel_pledge.sql;
\i ./pledges/cf_save_pledge.sql;
\i ./pledges/cf_get_pledge.sql;
\i ./pledges/cf_delete_pledge.sql;

\i ./properties/cf_save_submission_properties.sql;
\i ./properties/cf_save_properties.sql;
\i ./properties/cf_update_properties.sql;
\i ./properties/cf_save_validations.sql;

\i ./receipts/cf_delete_receipts_for_pid.sql;
\i ./receipts/cf_delete_receipt_parent_when_last_child_is_deleted.sql;
\i ./receipts/cf_delete_receipt.sql;
\i ./receipts/cf_delete_other_receipt.sql;

\i ./reports/cf_report_get_contact_aggregates.sql;
\i ./reports/cf_report_get_contact_aggregate_total.sql;
\i ./reports/cf_report_add_contact.sql
\i ./reports/cf_report_get_treasurer_info.sql;
\i ./reports/cf_report_get_committee.sql;
\i ./reports/cf_potential_reporting_obligations_by_fund.sql;
\i ./reports/cf_populate_reporting_obligations.sql;
\i ./reports/cf_add_one_reporting_obligation.sql;
\i ./reports/cf_report_get_contacts.sql;
\i ./reports/cf_save_reporting_periods.sql;
\i ./reports/cf_save_draft_report.sql;
\i ./reports/cf_combine_reporting_periods.sql;
\i ./reports/cf_get_committee_treasurers.sql;


\i ./reports/c3/cf_c3_get_anonymous_contributions.sql;
\i ./reports/c3/cf_c3_get_itemized_and_small_contributions.sql;
\i ./reports/c3/cf_c3_get_loan_endorsers.sql;
\i ./reports/c3/cf_c3_get_loans.sql;
\i ./reports/c3/cf_c3_get_miscellaneous_receipts.sql
\i ./reports/c3/cf_c3_get_auction_items.sql;
\i ./reports/c3/cf_c3_get_auctions.sql;
\i ./reports/c3/cf_c3_get_personal_funds.sql;
\i ./reports/c3/cf_c3_get_attached_text_pages.sql;
\i ./reports/c3/cf_c3_build_report.sql;

\i ./reports/c4/cf_c4_build_report.sql;
\i ./reports/c4/cf_c4_combine_reports_check.sql;
\i ./reports/c4/cf_c4_get_net_sums.sql;
\i ./reports/c4/cf_c4_get_deposits.sql;
\i ./reports/c4/cf_c4_get_expenses.sql;
\i ./reports/c4/cf_c4_get_debt.sql;
\i ./reports/c4/cf_c4_get_unredeemed_pledges.sql;
\i ./reports/c4/cf_c4_get_inkind_contributions.sql;
\i ./reports/c4/cf_c4_get_loans.sql;
\i ./reports/c4/cf_c4_get_contribution_corrections.sql;
\i ./reports/c4/cf_c4_get_expenditure_corrections.sql;
\i ./reports/c4/cf_c4_get_refund_from_vendor_corrections.sql;
\i ./reports/c4/cf_c4_get_attached_text_pages.sql;

\i ./transactions/cf_get_transaction.sql;
\i ./transactions/cf_do_deletions.sql;
\i ./transactions/cf_delete_bank_interest.sql;

