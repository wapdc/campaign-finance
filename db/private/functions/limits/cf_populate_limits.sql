--DROP FUNCTION IF EXISTS private.cf_populate_limits(p_fund_id integer);
CREATE OR REPLACE FUNCTION private.cf_populate_limits(p_fund_id INTEGER)
    RETURNS int as $$
DECLARE
    c_limits_category_id integer;
    r_count integer;
BEGIN

  c_limits_category_id := (select limit_category
      from fund f
        join candidacy c on f.committee_id = c.committee_id
        join jurisdiction_office jo on c.jurisdiction_id = jo.jurisdiction_id and c.office_code = jo.offcode
      where f.fund_id = p_fund_id);

  if c_limits_category_id is null then
    return 0;
   end if;

    insert into private.limits (fund_id, type, name, aggbyvoters, amount)
      select p_fund_id, cl.type_code, ct.label, cl.agg_by_voter::int, cl.amount
       from contribution_limit cl
        join contributor_type ct on cl.type_code = ct.type_code
       where c_limits_category_id = cl.limit_category
       group by (cl.type_code, ct.label, cl.agg_by_voter, cl.amount)
    on conflict (fund_id, type) do update
      set
        name = excluded.name,
        aggbyvoters = excluded.aggbyvoters,
        amount = excluded.amount;
  get diagnostics r_count = row_count;
  return r_count;
END
$$ LANGUAGE plpgsql;