--DROP function private.cf_save_limits(p_fund_id INTEGER, j JSON);
CREATE OR REPLACE FUNCTION private.cf_save_limits(p_fund_id INTEGER, j JSON)
    RETURNS VOID as  $$
DECLARE p REFCURSOR;
BEGIN
    IF j IS NOT NULL THEN
        DELETE FROM private.limits WHERE fund_id = p_fund_id
          and type  NOT IN (select value->>'type' from json_array_elements(j));
        INSERT INTO private.limits(fund_id, type, name, amount, aggbyvoters)
            SELECT p_fund_id, value->>'type', value->>'name', cast(value->>'amount' as numeric) as amount, cast(value->>'aggbyvoters' as smallint) as aggbyvoters
            FROM json_array_elements(J)
          ON CONFLICT(fund_id,type) DO UPDATE set name=excluded.name, amount=excluded.amount, aggbyvoters=excluded.aggbyvoters;
    END IF;
    RETURN;
END
$$ LANGUAGE plpgsql;