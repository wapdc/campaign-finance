CREATE OR REPLACE FUNCTION private.cf_delete_receipts_for_pid(p_pid INTEGER) RETURNS void AS $$
  SELECT private.cf_delete_receipt(trankeygen_id) from
    private.receipts WHERE pid >0 and pid = p_pid
$$ language sql;