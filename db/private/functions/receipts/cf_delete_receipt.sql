create or replace function private.cf_delete_receipt(p_id integer) returns void as
$$
  declare 
    v_type int;
    v_fund_id int;
    v_cont_id int;
    v_cid int;
    v_did int;
    v_pid int;
    v_amount numeric(16,2);
    v_deposit_id int;
    v_aggtype int;
  begin
    -- check if item has been deposited
    select rd.deposit_id into v_deposit_id from private.vreceipts rd
    where rd.trankeygen_id = p_id;

    if v_deposit_id is not null then
      raise 'Cannot delete a transaction that has been deposited.';
    end if;

    -- Determine transaction type
    select r.type, t.fund_id, r.contid, r.cid, r.did, r.pid, r.amount, r.aggtype into v_type, v_fund_id, v_cont_id, v_cid, v_did, v_pid, v_amount, v_aggtype
    from private.receipts r join private.trankeygen t on r.trankeygen_id = t.trankeygen_id
    where r.trankeygen_id = p_id;

    -- Delete child transactions (e.g. interest)
    perform private.cf_delete_receipts_for_pid(p_id);

    -- Back out the transaction.
    perform private.cf_account_transfer_funds(v_fund_id, v_cid, v_did, v_amount);

    delete from private.trankeygen t where t.trankeygen_id=p_id;

    if v_aggtype <> 0 then
      perform private.cf_update_aggregates(v_cont_id);
    end if;

    -- Delete parent transaction if this is the last child of a monetary group contribution
    if v_type = 41 then
      perform private.cf_delete_receipt_parent_when_last_child_is_deleted(v_fund_id, v_pid);
    end if;
  end;
$$ language plpgsql;
