create or replace function private.cf_delete_receipt_parent_when_last_child_is_deleted(p_fund_id int, p_pid int) returns void as $$
declare
  v_child_count int;
  v_cont_id int;
  v_cid int;
  v_did int;
  v_amount numeric(16,2);
  v_aggtype int;
  v_deposit_id int;
begin
  select count(vr.trankeygen_id)
  into v_child_count
  from private.vreceipts vr
  where vr.fund_id = p_fund_id and vr.pid = p_pid;

  if v_child_count = 0 then

    -- Determine transaction type
    select vr.contid, vr.cid, vr.did, vr.amount, vr.aggtype, vr.deposit_id
    into v_cont_id, v_cid, v_did, v_amount, v_aggtype, v_deposit_id
    from private.vreceipts vr
    where vr.trankeygen_id = p_pid;

    -- check if item has been deposited
    if v_deposit_id is not null then
      raise 'Cannot delete a transaction that has been deposited.';
    end if;

    -- Back out the transaction.
    perform private.cf_account_transfer_funds(p_fund_id, v_cid, v_did, v_amount);

    delete from private.trankeygen t where t.trankeygen_id=p_pid;

    if v_aggtype <> 0 then
      perform private.cf_update_aggregates(v_cont_id);
    end if;

  end if;
end
$$ language plpgsql;
