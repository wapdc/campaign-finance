CREATE OR REPLACE FUNCTION private.cf_delete_other_receipt(p_id integer) returns void as
$$
declare
    v_fund_id INTEGER;
    v_cont_id INTEGER;
    v_cid INTEGER;
    v_did INTEGER;
    v_amount NUMERIC(16,2);
begin
    -- Get transaction information
    select t.fund_id, r.contid, r.cid, r.did, r.amount INTO  v_fund_id, v_cont_id, v_cid, v_did, v_amount
    FROM private.receipts r JOIN private.trankeygen t on r.trankeygen_id = t.trankeygen_id
    WHERE r.trankeygen_id = p_id;

    perform private.cf_delete_receipt(p_id);
    delete from private.trankeygen where trankeygen_id = v_cont_id;
end;
$$ language plpgsql;