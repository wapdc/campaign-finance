create or replace function private.cf_save_contribution(p_fund_id integer, json_data json) returns integer
    language plpgsql
as $$
DECLARE
    v_contribution_id int;
    v_deposit_id int;
    v_contid int;
    v_contact_1_id int;
    v_contact_2_id int;
    v_contact_type varchar;
    v_cid int;
    v_did int;
    v_pid int;
    v_parent_type int;
    v_type int;
    v_aggtype int := 1;
    v_elekey smallint;
    v_old_amount numeric(16,2);
    v_old_cid int;
    v_old_did int;
    v_old_contid int;
BEGIN
    IF json_data->>'amount' is null or cast(json_data->>'amount' as numeric) = 0 then
        RAISE 'Invalid or missing amount' using column='amount';
    end if;

    IF json_data->>'date_' is null THEN
        RAISE 'Missing date' using column='date';
    end if;

    v_elekey = cast(json_data->>'elekey' as smallint);

    v_contid = cast(json_data->>'contid' as integer);

    if v_contid is not null then
        select c.type, c.trankeygen_id, c.contact1_id, c.contact2_id into v_contact_type, v_contid, v_contact_1_id, v_contact_2_id from private.vcontacts c where c.trankeygen_id=v_contid and c.fund_id=p_fund_id;

        if v_contid is null then
            raise 'Invalid contid' using column='contid';
        end if;
    end if;

    IF v_contact_type = 'CAN' THEN v_type := 2;
    ELSE v_type := 1; -- Monetary contribution from IND or BUS
    END IF;

    select min(trankeygen_id) into v_cid from private.vaccounts
    where acctnum = 1600 and fund_id=p_fund_id;

    IF v_type  = 2 THEN -- Personal FUnd
        select min(trankeygen_id) into v_did FROM private.vaccounts
        where acctnum = 4200 and fund_id=p_fund_id;
    ELSE -- Monetary contribution
        select min(trankeygen_id) into v_did FROM private.vaccounts
        where acctnum = 4000 and fund_id=p_fund_id and pid is null;
    end if;

    v_pid = cast(json_data->>'pid' as int);
    if v_pid is not null then
        select r.trankeygen_id, r.type, r.cid, r.elekey into v_pid, v_parent_type, v_did, v_elekey from private.vreceipts r where r.trankeygen_id=cast(json_data->>'pid' as int) and fund_id=p_fund_id;
        if v_pid is null or v_parent_type <> 12 then
            raise 'Invalid pid specified (not a pledge for current fund)' using column='pid';
        else
            v_type = 17;
            v_aggtype = 0;
        end if;
    end if;


    v_contribution_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));

    select cid, did, amount, deposit_id, contid into v_old_cid, v_old_did, v_old_amount, v_deposit_id, v_old_contid from private.vreceipts where trankeygen_id=v_contribution_id;

    INSERT into private.receipts(trankeygen_id, pid, type, contid, elekey, amount, date_, itemized, checkno, memo, cid, did, aggtype, nettype, deptkey)
    values (v_contribution_id,
            v_pid,
            v_type,
            v_contid,
            v_elekey,
            cast(json_data ->> 'amount' as numeric),
            to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
            COALESCE(CAST(json_data ->> 'itemized' as smallint),0),
            json_data ->> 'checkno',
            json_data ->> 'memo',
            v_cid,
            v_did,
            v_aggtype,
            1,
            0)


    on conflict (trankeygen_id) do update
        set
            type            = v_type,
            did             = v_did,
            contid          = excluded.contid,
            elekey          = excluded.elekey,
            amount          = excluded.amount,
            date_           = excluded.date_,
            itemized        = excluded.itemized,
            checkno         = excluded.checkno,
            memo            = excluded.memo;

    if v_pid is not null then
        update private.trankeygen set pid=v_pid where trankeygen_id=v_contribution_id;
    end if;

    if (v_old_contid != v_contid) then
      perform private.cf_update_aggregates(v_old_contid);
    end if;

    perform private.cf_update_aggregates(v_contid);

    perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, coalesce(v_old_cid, v_cid), cast(json_data->>'amount' as numeric));

    -- update deposit event total
    IF v_deposit_id is not null THEN
        PERFORM private.cf_recalculate_deposit_event(p_fund_id, v_deposit_id);
    END IF;

    -- change date updated on this trankeygen
    update private.trankeygen
    set rds_updated = now()
    where trankeygen_id = v_contribution_id;

    RETURN v_contribution_id;

END
$$;