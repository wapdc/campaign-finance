create or replace function private.cf_save_split_contribution(p_fund_id INTEGER, p_contrib JSON) returns json as  $$
    declare
      j_c jsonb;
      j_contributions jsonb;
      v_trankeygen_id integer;
      v_saved_cont jsonb;
      elekey integer;
    begin
      if (p_contrib->>'trankeygen_id')::integer > 0  THEN
        RAISE EXCEPTION 'Cannot split an existing contribution';
      end if;
      j_contributions := jsonb_build_array();
      j_c := p_contrib::jsonb;
      for elekey in (select value as elekey from generate_series(0,1) value) LOOP
          j_c := jsonb_set(j_c, '{elekey}', to_jsonb(elekey), true);
          if elekey = 0 THEN
            j_c = jsonb_set(j_c, '{amount}', to_jsonb((p_contrib->>'primary_amount')::numeric), true);
          ELSE
            j_c = jsonb_set(j_c, '{amount}', to_jsonb((p_contrib->>'general_amount')::numeric), true);
          end if;
          v_trankeygen_id := private.cf_save_contribution(p_fund_id, j_c::Json);
          v_saved_cont := private.cf_get_transaction(p_fund_id, v_trankeygen_id);
          j_contributions := j_contributions || v_saved_cont;
      end loop;
      return j_contributions::json;
    end
$$ language plpgsql;