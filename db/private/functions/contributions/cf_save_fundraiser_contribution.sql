CREATE OR REPLACE function private.cf_save_fundraiser_contribution(p_fund_id integer, p_json JSON) returns integer as $$
    DECLARE
      v_event_id INTEGER;
      v_contribution_id INTEGER;
      v_deposit_id INTEGER;
      v_contid INTEGER;
      v_old_cid INTEGER;
      v_old_did INTEGER;
      v_old_amount numeric(16,2);
      v_old_contid INTEGER;
      v_cid INTEGER;
      v_amount NUMERIC(16,2);
      v_date date;
    begin
      SELECT trankeygen_id into v_event_id from private.vaccounts r where r.fund_id=p_fund_id
        and r.trankeygen_id = cast(p_json->>'pid' as integer);

      IF v_event_id is null then
        RAISE 'Invalid or missing fundraiser event id';
      end if;

      -- validate contact
      SELECT c.trankeygen_id INTO v_contid from private.vcontacts c where c.fund_id=p_fund_id and c.trankeygen_id=cast(p_json->>'contid' as integer);
      IF v_contid is null then
          RAISE 'Invalid or missing contact id';
      end if;

      -- Get undeposited items account
      SELECT trankeygen_id INTO v_cid from private.vaccounts a where a.fund_id=p_fund_id and a.acctnum=1600;

      v_contribution_id = private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as integer));

      select cid, did, amount, deposit_id, contid into v_old_cid, v_old_did, v_old_amount, v_deposit_id, v_old_contid from private.vreceipts r where r.trankeygen_id = v_contribution_id;

      v_amount = cast(p_json->>'amount' as numeric);
      v_date = cast(p_json->>'date_' as date);

      -- Insert/update transaction
      INSERT INTO private.receipts(trankeygen_id, type, aggtype, nettype, pid, cid, did, contid, amount, date_, elekey,
                                   itemized, checkno, memo)
        VALUES(v_contribution_id, 4, 1, 1,v_event_id,  v_cid, v_event_id, v_contid, v_amount, v_date, cast(p_json->>'elekey' as integer),
               cast(p_json->>'itemized' as integer), p_json->>'checkno', p_json->>'memo')
      ON CONFLICT(trankeygen_id) DO UPDATE SET
        contid = excluded.contid,
        amount = excluded.amount,
        date_ = excluded.date_,
        elekey = excluded.elekey,
        checkno = excluded.checkno,
        itemized = excluded.itemized,
        memo = excluded.memo;

      if (v_old_contid != v_contid) then
        perform private.cf_update_aggregates(v_old_contid);
      end if;
      perform private.cf_update_aggregates(v_contid);

      -- Adjust accounting
      perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_event_id, coalesce(v_old_cid, v_cid), v_amount);

      -- update deposit event total
      IF v_deposit_id IS NOT NULL THEN
          PERFORM private.cf_recalculate_deposit_event(p_fund_id, v_deposit_id);
      END IF;
      return v_contribution_id;
    end
$$ language plpgsql;