--DROP function private.cf_save_inkind_contribution(p_fund_id INTEGER, json_data JSON);
CREATE OR REPLACE FUNCTION private.cf_save_inkind_contribution(p_fund_id INTEGER, json_data JSON)
  RETURNS int as
$$
DECLARE
  v_contribution_id int;
  v_did int;
  v_pid int;
  v_parent_type int;
  v_type int;
  v_contid int;
  v_elekey smallint;
  v_account int;
  v_old_cid integer;
  v_old_did integer;
  v_old_amount numeric(16,2);
  v_old_contid integer;
  v_dept_key bigint;
  v_agg_type smallint;
BEGIN

v_elekey = cast(json_data->>'elekey' as smallint);
v_agg_type = 1;
if json_data->>'pid' is not null then
    select r.cid, r.contid, r.trankeygen_id, r.type, r.elekey into v_did, v_contid, v_pid, v_parent_type, v_elekey from private.vreceipts r
    where trankeygen_id = cast(json_data->>'pid' as integer);

    if v_pid is null or v_parent_type <> 12 then
        raise 'Invalid pid specified (not a pledge for current fund)' using column='pid';
    end if;

    v_type = 16;
    v_agg_type = 0;
else
    select trankeygen_id into v_contid FROM private.vcontacts
    where trankeygen_id = cast(json_data ->> 'contid' as integer) and fund_id = p_fund_id;

    IF (v_contid is null)
    THEN
        raise  EXCEPTION 'Invalid contact for fund';
    end if;

    v_type = 6;
    select min(trankeygen_id) into v_did FROM private.vaccounts
    where acctnum = 4100 and fund_id=p_fund_id;
end if;

select trankeygen_id into v_account FROM private.vaccounts
where trankeygen_id = cast(json_data ->> 'cid' as integer) and fund_id = p_fund_id;

IF (v_account is null) THEN
    raise  EXCEPTION 'Invalid account for fund';
end if;

v_contribution_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));

select cid, did, amount, contid into v_old_cid, v_old_did, v_old_amount, v_old_contid from private.receipts
    where trankeygen_id = v_contribution_id;

v_dept_key = private.cf_generate_deposit_key(p_fund_id, to_date(json_data ->> 'date_', 'YYYY-MM-DD'));

INSERT into private.receipts(trankeygen_id, pid, type, contid,elekey, amount, date_, itemized, description, memo, cid, did, aggtype, nettype, deptkey)
values (v_contribution_id,
        v_pid,
        v_type,
        v_contid,
        v_elekey,
        cast(json_data ->> 'amount' as numeric),
        to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
        cast(json_data ->> 'itemized' as smallint),
        json_data ->> 'description',
        json_data ->> 'memo',
        v_account,
        v_did,
        v_agg_type,
        2,
        v_dept_key)
    on conflict (trankeygen_id) do update
        set
        contid          = excluded.contid,
        elekey          = excluded.elekey,
        amount          = excluded.amount,
        cid             = excluded.cid,
        date_           = excluded.date_,
        itemized        = excluded.itemized,
        description     = excluded.description,
        memo            = excluded.memo,
        deptkey         = excluded.deptkey;

if v_pid is not null then
    update private.trankeygen t set pid=v_pid where trankeygen_id=v_contribution_id;
end if;

if (v_old_contid != v_contid) then
  perform private.cf_update_aggregates(v_old_contid);
end if;
perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, v_account, cast(json_data ->> 'amount' as numeric));
perform private.cf_update_aggregates(v_contid);
RETURN v_contribution_id;

END
$$ LANGUAGE plpgsql;