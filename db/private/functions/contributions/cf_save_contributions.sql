CREATE OR REPLACE FUNCTION private.cf_save_contributions(p_fund_id INTEGER, json_data JSON)
    RETURNS json as
$$
DECLARE
    c json;
    v_contacts jsonb;
    v_contact_id int;
    v_existing_contact_id int;
    v_address_similarity numeric;
    v_name_similarity numeric;
    v_contact_json jsonb;
    v_contrib_json jsonb;
    v_get_json jsonb;
    v_insert_duplicate boolean := false;
    v_message_text text;
    v_column text;
    v_receipts jsonb;
    v_receipt_id integer;
    v_errors jsonb;
BEGIN
  v_contacts := jsonb_build_array();
  v_receipts := jsonb_build_array();
  v_errors := jsonb_build_array();

  FOR c IN
    SELECT
      *
    FROM json_array_elements(json_data) j
    LOOP
      v_contact_json := c::jsonb;
      v_contrib_json := c::jsonb;
      v_insert_duplicate := false;


      -- See if we need to insert potential duplicate contact id
      select cc.trankeygen_id into v_existing_contact_id
      from private.contacts cc
        join private.trankeygen t on cc.trankeygen_id = t.trankeygen_id
      where cc.trankeygen_id = cast(c->>'duplicate_id' as integer)
        and t.fund_id=p_fund_id;

      if v_existing_contact_id is not null then
          v_insert_duplicate := true;
      end if;

      -- only update contact record if it is a new one.
      if COALESCE(cast(c->>'contid' as integer), 0) = 0 then
          begin
              v_message_text := null;
              IF c->>'type' ILIKE ANY (array['%CAN%', '%GRP%', '%CPL%']) THEN
                  v_message_text := 'Contact type cannot be CAN, GRP, or CPL';
                  v_column := 'Type';
                  v_contact_id := null;
              ELSE
              v_contact_id = private.cf_save_contact(p_fund_id, v_contact_json::json);
              END IF;
              EXCEPTION WHEN OTHERS THEN
              GET STACKED DIAGNOSTICS v_message_text := MESSAGE_TEXT,
                  v_column := column_name ;
              v_contact_id := null;
          end;

          IF v_contact_id is not null then
              IF v_insert_duplicate THEN
                  INSERT INTO private.contact_duplicates(contact_id, duplicate_id, name_similarity, address_similarity)
                  VALUES (v_existing_contact_id, v_contact_id, v_name_similarity, v_address_similarity) ON CONFLICT DO NOTHING;
              end if;
              -- returns a contact id
              v_get_json := private.cf_get_contact(p_fund_id, v_contact_id)::jsonb;
              v_contacts := v_contacts || v_get_json;
          else
              if v_column is not null then
                  if v_message_text is null then
                      v_message_text := 'Missing or invalid ' || v_column;
                  end if;
              end if;
              v_contact_json := jsonb_set(v_contact_json, '{error}', to_jsonb(coalesce(v_message_text,'Error saving contact')), true);
              v_errors = v_errors || v_contact_json;
          end if;
        else
          select trankeygen_id into v_contact_id from private.vcontacts
            where fund_id=p_fund_id and trankeygen_id=cast(c->>'contid' as integer);
          if v_contact_id is null then
              v_contact_json := jsonb_set(v_contact_json, '{error}', to_jsonb('Invalid contact'));
          end if;
        end if;

      -- Insert the contribution record
      if v_contact_id is not null then
          v_contrib_json := jsonb_set(v_contrib_json, '{contid}', to_jsonb(v_contact_id), true);
          begin
              v_message_text := null;
              v_receipt_id := private.cf_save_contribution(p_fund_id, v_contrib_json::json);
          EXCEPTION WHEN OTHERS THEN
              get stacked diagnostics v_message_text := message_text,
                  v_column := column_name;
              v_receipt_id := NULL;
          end;
          IF v_receipt_id is not null then
              v_get_json := private.cf_get_transaction(p_fund_id, v_receipt_id)::jsonb;
              v_receipts := v_receipts || v_get_json;
          else
              if v_column is not null then
                  v_message_text := 'Missing or invalid ' || v_column;
              end if;
              v_contrib_json := jsonb_set(v_contrib_json, '{error}', to_jsonb(coalesce(v_message_text, 'Error saving contribution')), true);
              v_errors = v_errors || v_contrib_json;
          end if;
      end if;
      end loop;
  return jsonb_build_object('contacts', v_contacts, 'receipts', v_receipts, 'errors', v_errors)::json;
END
$$ LANGUAGE plpgsql;