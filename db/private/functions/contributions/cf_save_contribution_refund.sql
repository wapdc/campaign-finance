--DROP function private.cf_save_contribution_refund(p_fund_id INTEGER, json_data JSON);
CREATE OR REPLACE FUNCTION private.cf_save_contribution_refund(p_fund_id INTEGER, json_data JSON)
  RETURNS int as
$$
DECLARE
  v_contribution_id int;
  v_pid int;
  v_did int;
  v_cid int;
  v_elekey int;
  v_contact int;
  v_account int;
  v_old_amount numeric(16,2);
  v_old_cid int;
  v_old_did int;
  v_deptkey bigint;
  v_old_contid int;
BEGIN

select trankeygen_id, cid, did, elekey, contid into v_pid, v_did, v_cid, v_elekey, v_contact FROM private.vreceipts
where trankeygen_id = cast(json_data ->> 'pid' as integer) and fund_id = p_fund_id;

if (v_pid is null)
THEN
    raise EXCEPTION  'Invalid pid';
end if;

select trankeygen_id into v_account FROM private.vaccounts
where trankeygen_id = v_cid and fund_id = p_fund_id;

IF (v_contact is null)
THEN
    raise  EXCEPTION 'Invalid contact for fund';
end if;

IF (v_account is null)
THEN
    raise  EXCEPTION 'Invalid account for fund';
end if;

v_contribution_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));
SELECT cid, did, amount, contid INTO v_old_cid, v_old_did, v_old_amount, v_old_contid FROM private.receipts r where trankeygen_id=v_contribution_id;

v_deptkey = private.cf_generate_deposit_key(p_fund_id, to_date(json_data ->> 'date_', 'YYYY-MM-DD'));

INSERT into private.receipts(trankeygen_id, type, contid, elekey, amount, date_, checkno, description, memo, cid, pid, did, aggtype, nettype, deptkey)
values (v_contribution_id,
        27,
        v_contact,
        v_elekey,
        cast(json_data ->> 'amount' as numeric),
        to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
        json_data ->> 'checkno',
        json_data ->> 'description',
        json_data ->> 'memo',
        v_cid,
        cast(json_data ->> 'pid' as integer),
        v_did,
        -1,
        1,
        v_deptkey)
    on conflict (trankeygen_id) do update
                                       set
                                       contid          = excluded.contid,
                                       amount          = excluded.amount,
                                       date_           = excluded.date_,
                                       checkno         = excluded.checkno,
                                       description     = excluded.description,
                                       memo            = excluded.memo,
                                       deptkey         = excluded.deptkey;
perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, v_cid,cast(json_data ->> 'amount' as numeric));
if (v_old_contid != v_contact) then
  perform private.cf_update_aggregates(v_old_contid);
end if;
perform private.cf_update_aggregates(cast(json_data ->>'contid' as integer));
RETURN v_contribution_id;

END
$$ LANGUAGE plpgsql;