--DROP function private.cf_save_anonymous_contribution(p_fund_id INTEGER, json_data JSON);
CREATE OR REPLACE FUNCTION private.cf_save_anonymous_contribution(p_fund_id INTEGER, json_data JSON)
  RETURNS int as
$$
DECLARE
v_contribution_id int;
v_deposit_id int;
v_cid int;
v_did int;
v_old_amount numeric(16,2);
v_old_cid int;
v_old_did int;
BEGIN

select min(trankeygen_id) into v_cid from private.vaccounts
where acctnum = 1600 and fund_id=p_fund_id;

select min(trankeygen_id) into v_did FROM private.vaccounts
where acctnum = 4099 and fund_id=p_fund_id;


v_contribution_id = private.cf_ensure_trankeygen(p_fund_id, cast(json_data ->> 'trankeygen_id' as integer));

select cid, did, amount, deposit_id into v_old_cid, v_old_did, v_old_amount, v_deposit_id from private.vreceipts
    where trankeygen_id=v_contribution_id;

INSERT into private.receipts(trankeygen_id, type, amount, date_, memo, cid, did, aggtype, nettype, deptkey)
values (v_contribution_id,
        13,
        cast(json_data ->> 'amount' as numeric),
        to_date(json_data ->> 'date_', 'YYYY-MM-DD'),
        json_data ->> 'memo',
        v_cid,
        v_did,
        0,
        1,
        0)
    on conflict (trankeygen_id) do update
                                       set
                                       amount          = excluded.amount,
                                       date_           = excluded.date_,
                                       memo            = excluded.memo;

perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, coalesce(v_old_cid, v_cid), cast(json_data->>'amount' as numeric));

-- update deposit event total
IF v_deposit_id is not null THEN
    PERFORM private.cf_recalculate_deposit_event(p_fund_id, v_deposit_id);
END IF;

RETURN v_contribution_id;

END
$$ LANGUAGE plpgsql;