create or replace function private.cf_save_monetary_group_contribution(p_fund_id integer, p_json json) returns void as $$
  declare
    v_trankeygen_id int default private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'trankeygen_id' as int));
    v_individual_trankeygen_id int;
    v_contid int default private.cf_ensure_trankeygen(p_fund_id, cast(p_json->>'group_contid' as integer));
    v_itemized smallint default p_json->>'itemized';
    v_date_ date default cast(p_json->>'date_' as date);
    v_memo text default p_json->>'memo';
    v_amount numeric(16,2) default cast(p_json->>'individual_amount' as numeric(16,2));
    v_total_amount numeric(16,2) default 0;
    v_group_contid int default cast(p_json->>'group_contid' as int);
    v_aggtype int default 1;
    v_nettype int default 1;
    v_deptkey bigint default 0;
    v_type int default 11;
    v_elekey smallint default -1;
    v_cid int;
    v_did int;
    v_junk_id int;
    v_old_cid int;
    v_old_did int;
    v_old_amount numeric(16,2);
    v_deposit_id int;
    member record;
  begin
    -- set v_cid
    select min(trankeygen_id) into v_cid from private.vaccounts
    where acctnum=1600 and fund_id=p_fund_id;

    -- set v_did
    select min(trankeygen_id) into v_did from private.vaccounts
    where acctnum=4000 and fund_id=p_fund_id;

    -- set v_junk_id
    select min(trankeygen_id) into v_junk_id from private.vaccounts
    where acctnum=0 and fund_id=p_fund_id;

    -- loop through and delete individual child members that are excluded
    for member in select c.*, r.trankeygen_id as individual_id from private.receipts r
      join private.vcontacts c on r.contid=c.trankeygen_id
      where r.pid=v_trankeygen_id and c.trankeygen_id in (
        select v::text::int from json_array_elements(p_json->'exclude_members')
      v)
    loop
      delete from private.deposititems where rcptid=member.individual_id;
      delete from private.receipts where trankeygen_id=member.individual_id;
      perform private.cf_update_aggregates(member.trankeygen_id);
    end loop;

    -- loop through and create/update individual child members that are included
    for member in select c.*, r.trankeygen_id as individual_id
    from private.registeredgroupcontacts rgc
      join private.vcontacts c on rgc.contid=c.trankeygen_id
      left join private.receipts r on r.contid=c.trankeygen_id and r.pid=v_trankeygen_id
      where gconid=v_group_contid and rgc.contid not in (
        select v::text::int from json_array_elements(p_json->'exclude_members')
      v)
    loop
      v_individual_trankeygen_id = private.cf_ensure_trankeygen(p_fund_id, member.individual_id);

      -- get deposit variables in the case that an individual detail is created
      -- after the group has been deposited
      select deptkey, deposit_id
      into v_deptkey, v_deposit_id
      from private.vreceipts
      where trankeygen_id=v_trankeygen_id;
      if v_deptkey is null then
        v_deptkey = 0;
      end if;

      -- create the receipt for the individual member
      insert into private.receipts(trankeygen_id, aggtype, amount, contid, date_, deptkey, elekey, itemized, nettype, type, cid, did, pid)
      values (v_individual_trankeygen_id, v_aggtype, v_amount, member.trankeygen_id, v_date_, v_deptkey, v_elekey, v_itemized, 0, 41, v_junk_id, v_junk_id, v_trankeygen_id)
      on conflict(trankeygen_id) do nothing;
      perform private.cf_update_aggregates(member.trankeygen_id);
      -- add the item to an already existing deposit
      if v_deposit_id is not null then
        insert into private.deposititems(deptid, rcptid) values(v_deposit_id, v_individual_trankeygen_id)
        on conflict(deptid, rcptid) do nothing;
      end if;
    end loop;

    -- update ghost contacts
    update private.receipts set amount=v_amount, itemized=v_itemized, elekey=v_elekey, date_=v_date_  where pid=v_trankeygen_id;

    -- adjust v_total_amount for 'ghost members' or members that have a receipt but are not part of the group
    select sum(amount)
    into v_total_amount
    from private.vreceipts
    where pid=v_trankeygen_id;

    -- adjust the cid/did accounts
    select cid, did, amount
    into v_old_cid, v_old_did, v_old_amount
    from private.vreceipts
    where trankeygen_id=v_trankeygen_id;
    perform private.cf_account_adjust(p_fund_id, v_old_did, v_old_cid, v_old_amount, v_did, coalesce(v_old_cid, v_cid), v_total_amount);

    -- create the group receipt
    if v_total_amount=0
      then raise exception 'cannot have an amount of $0.00';
    end if;
    insert into private.receipts(trankeygen_id, aggtype, amount, contid, date_, deptkey, elekey, itemized, memo, nettype, type, cid, did)
    values (v_trankeygen_id, v_aggtype, v_total_amount, v_contid, v_date_, v_deptkey, v_elekey, v_itemized, v_memo, v_nettype, v_type, v_cid, v_did)
    on conflict(trankeygen_id) do update set amount=excluded.amount, memo=excluded.memo, date_=excluded.date_, itemized=excluded.itemized, contid=excluded.contid;

    -- update deposit event total
    if v_deposit_id is not null then
      perform private.cf_recalculate_deposit_event(p_fund_id, v_deposit_id);
    end if;

end $$ language plpgsql
