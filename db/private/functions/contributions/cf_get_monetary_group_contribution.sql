create or replace function private.cf_get_monetary_group_contribution(p_fund_id int, p_trankeygen_id int) returns json as $$
declare
  group_receipt json;
  individual_receipts json;
  receipts json;
begin
  -- get group_receipt
  select row_to_json(r), r.contid
  into group_receipt
  from private.vreceipts as r
  where r.fund_id=p_fund_id and
        r.type=11 and
        r.trankeygen_id=p_trankeygen_id;

  -- get individual_receipts
  select json_agg(v), array_agg(v.contid_orca),
    json_agg(json_build_object('orca_id', v.contid_orca, 'rds_id', v.trankeygen_id))
  into individual_receipts
  from private.vreceipts as v
  where v.fund_id=p_fund_id and
        v.type=41 and
        v.pid=p_trankeygen_id;

  -- return as one json object
  select row_to_json(z) into receipts from (select group_receipt, individual_receipts) z;

  return receipts;
end
$$ language plpgsql
