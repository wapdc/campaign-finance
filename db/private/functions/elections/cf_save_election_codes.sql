--drop function private.cf_save_election_codes(p_fund_id integer, input_json json)
create or replace function private.cf_save_election_codes(p_fund_id integer, json_data json)
  returns json as
$$
DECLARE
  V_election_codes text[];
  valid_election_code  varchar;
  valid_election_codes varchar[];
BEGIN

  V_election_codes := (select array_agg(j) from json_array_elements_text(json_data->'election_codes') j);

  select array_agg(election_code::varchar)
  into valid_election_codes
  from election
  where election_code::text = any (V_election_codes);

  delete
  from private.election_participation
  where fund_id = p_fund_id
    and (valid_election_codes is null or not (election_code = any (valid_election_codes)));

  if valid_election_codes is not null then
    foreach valid_election_code in array valid_election_codes
      loop
        insert into private.election_participation (fund_id, election_code)
        values (p_fund_id, valid_election_code)
        on conflict (fund_id, election_code) do nothing;
      end loop;
  else
    update private.properties set value = '-1' where fund_id = p_fund_id and name = 'CAMPAIGNINFO:BALLOTTYPE';
  end if;

  return (select json_agg(election_code) as "election_codes" FROM private.election_participation where fund_id = p_fund_id);
END
$$ LANGUAGE plpgsql;
