
delete from private.derby_update where sql like '%receipts(%';
insert into private.derby_update(fund_id,message, sql)
select fund_id,
       'The PDC has discovered an error in your campaign’s data caused by the most recent ORCA update released July 21.' as message,
       'INSERT INTO TRANKEYGEN(ID, PID, DERBY_UPDATED) VALUES ('|| id || ',' || t_pid || ',CURRENT_TIMESTAMP);'
           || 'INSERT INTO receipts(id,type,aggtype,nettype,pid,gid,cid,did,contid,amount, date_,elekey, itemized, checkno, description, memo, carryforward, deptkey) VALUES ('
           || id || ','
           || type || ','
           || aggtype || ','
           || nettype || ','
           || t_pid || ','
           || orca_gid || ','
           || orca_cid || ','
           || orca_did || ','
           || orca_contid || ','
           || amount || ','
           || quote_nullable(date_) || ','
           || elekey || ','
           || itemized || ','
           || quote_nullable(checkno) || ','
           || quote_nullable(description) || ','
           || quote_nullable(memo) || ','
           || carryforward || ','
           || deptkey || ');'
       || case when loan_id is not null then
           'INSERT INTO loans(id,type,cid,did,contid,amount,date_,elekey,interestrate,duedate,checkno,repaymentschedule,description,memo,carryforward) VALUES('
           || id || ','
           || type || ','
           || orca_cid || ','
           || orca_did || ','
           || orca_contid || ','
           || amount || ','
           || quote_nullable(date_) ||','
           || elekey || ','
           || interestrate || ','
           || quote_nullable(duedate) || ','
           || quote_nullable(checkno) || ','
           || quote_nullable(repaymentschedule) || ','
           || quote_nullable(description) || ','
           || quote_nullable(memo) ||','
           || carryforward || ');'


       else '' end

                                                                      as sql

from
    (select t.fund_id,  t.orca_id as id, COALESCE(t2.orca_id,0) as t_pid, r.*,
            l.trankeygen_id as loan_id,
            l.interestrate,
            l.duedate,
            l.repaymentschedule,
            coalesce(tcont.orca_id,0) as orca_contid,
            coalesce(tcid.orca_id,0) as orca_cid,
            coalesce(tdid.orca_id,0) as orca_did,
            coalesce(tgid.orca_id,0) as orca_gid
     from private.trankeygen t
              join private.missing_59 m on t.fund_id = m.fund_id
              join private.receipts r on r.trankeygen_id = t.trankeygen_id
              left join private.trankeygen t2 ON t2.trankeygen_id = t.pid
              left join private.trankeygen tcont ON tcont.trankeygen_id = r.contid
              left join private.trankeygen tcid ON tcid.trankeygen_id = r.cid
              left join private.trankeygen tdid ON tdid.trankeygen_id = r.did
              left join private.trankeygen tgid ON tgid.trankeygen_id = r.gid
              left join private.loans l on l.trankeygen_id = t.trankeygen_id
     where t.orca_id = 59
     order by l.trankeygen_id, t.fund_id
    ) as statements;

