delete from private.derby_update where sql like '%depositevents(%' ;
insert into private.derby_update (fund_id, message, sql)
select fund_id,
       'The PDC has discovered an error in your campaign’s deposit data caused by the most recent ORCA update released July 21.' as message,
      'INSERT INTO TRANKEYGEN(ID, PID, DERBY_UPDATED) VALUES ('|| id || ',' || pid || ',CURRENT_TIMESTAMP);'
     || 'INSERT INTO depositevents(id,account_id, amount, date_, deptkey, memo) values ('
     || id || ','
     || account_id || ','
     || amount || ','
     || quote_nullable(date_) || ','
     || deptkey || ','
     || quote_nullable(memo) || ');'
  as sql
from
    (select t.fund_id,  t.orca_id as id, COALESCE(t2.orca_id,0) as pid, d.*
     from private.trankeygen t
              join private.missing_59 m on t.fund_id = m.fund_id
              join private.depositevents d on d.trankeygen_id=t.trankeygen_id
              left join private.trankeygen t2 ON t2.trankeygen_id = t.pid
     where t.orca_id = 59
    ) as statements;