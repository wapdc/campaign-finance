update private.receipts
set elekey = -1
where trankeygen_id in (select count(*), r.trankeygen_id
                        from private.receipts r
                                 join private.trankeygen t on t.trankeygen_id = r.trankeygen_id
                        where t.rds_updated is not null
                          and r.type != 28
                          and fund_id in (select a.fund_id
                                          from (select f.fund_id,
                                                       -- indicate whether candidate can target primary
                                                       case
                                                           when c.pac_type <> 'candidate' then false
                                                           -- exclude tacoma and seattle
                                                           when jc1.legacy_code is null then false
                                                           when jc1.legacy_code = '' then false
                                                           when j.jurisdiction_id = 4048 then false
                                                           when ca.office_code = '50' then false
                                                           when ca.office_code >= '35' and jc1.legacy_code = 'LC'
                                                               then false
                                                           -- exclude offices that don't allow primary
                                                           when jc1.legacy_code = 'PC' then false
                                                           else true
                                                           end as allow_primary_limits
                                                FROM fund f
                                                         join committee c ON f.committee_id = c.committee_id
                                                         left join candidacy ca ON c.committee_id = ca.committee_id
                                                         left join jurisdiction j on j.jurisdiction_id = ca.jurisdiction_id
                                                         left join foffice o on o.offcode = ca.office_code
                                                         left join jurisdiction_category jc1 on j.category = jc1.jurisdiction_category_id) as a
                                          where allow_primary_limits = false));
