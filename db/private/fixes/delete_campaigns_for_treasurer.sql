do $$
    declare
    campaign record;
begin
    for campaign in (
        select p.fund_id, cc.name, p.value as synced, c.name as committee_name, f.election_code
        from private.properties p
          join fund f on f.fund_id=p.fund_id
          join committee_contact cc on cc.committee_id=f.committee_id
          join committee c on c.committee_id = f.committee_id
        where p.name='DERBY_TO_RDS'
          and cc.name ilike :treasure_name
        ) LOOP
            perform private.cf_delete_orca_campaign(campaign.fund_id);
    end loop;
end
$$ language plpgsql
