do $$

 declare
   rec record;
   e_rec record;
   v_index integer;
   p_fund_id integer := :fund_id;
 begin
   -- Loop through all of the orphaned type 28 receipt records to find expenses with no expenditureevent
   for rec in (
       select r.fund_id,r.trankeygen_id, r.pid, r.orca_id, r.pid_orca, r.date_, r.amount, r.contid from private.vreceipts r
       left join private.expenditureevents e on r.pid=e.trankeygen_id
             where r.type=28
               and e.trankeygen_id is null
               and r.fund_id = p_fund_id
       order by r.fund_id, r.orca_id) loop
       v_index := 0;

       -- Search for a matching record in the fund based on date, amount and contid
       -- Make sure to exclude records that have expenditure event amounts matching the detail record amounts.
       for e_rec in (
         select e.trankeygen_id as pid,
                count(1) as records,
                max(e.amount) as amount,
                sum(d.amount) as detail_amount
         from private.expenditureevents e join private.trankeygen t ON t.trankeygen_id=e.trankeygen_id
           left join private.receipts d on d.pid = e.trankeygen_id
         where t.fund_id = rec.fund_id and e.date_ = rec.date_ and e.contid = rec.contid and e.amount >= rec.amount
         group by e.trankeygen_id
         having sum(e.amount) is distinct from sum(d.amount)

       ) loop

           v_index := v_index + 1;
           if v_index > 1 then
               raise 'Too Many records for expense item: %',rec.pid;
           end if;

           raise notice 'found expenditure event record for fund_id,orca_id %, %: %, %, %',rec.fund_id, rec.orca_id, e_rec.records, e_rec.amount, e_rec.detail_amount;

           update private.trankeygen set pid= e_rec.pid where trankeygen_id=rec.trankeygen_id;
           update private.receipts set pid = e_rec.pid where trankeygen_id=rec.trankeygen_id;
           end loop;
       if v_index = 0 then
           raise notice 'Could not find expense transaction for fund,orca_id,trankeygen_id %, %, %', rec.fund_id, rec.orca_id, rec.trankeygen_id;
       end if;
       end loop;
 end
$$ language plpgsql;