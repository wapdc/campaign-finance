select distinct t.fund_id, com.name, cc.email,
       CASE
            when l.trankeygen_id is not null then 'loan'
            when r.trankeygen_id is not null then 'receipt'
            when a.trankeygen_id is not null then 'account'
            when c.trankeygen_id is not null then 'contact'
            when e.trankeygen_id is not null then 'expenditureevent'
            when au.trankeygen_id is not null then 'auction item'
            when dep.trankeygen_id is not null then 'deposit'
           end as type
from private.trankeygen t
         join private.missing_59 m on t.fund_id = m.fund_id
         join fund f on f.fund_id = t.fund_id
         join committee com on com.committee_id = f.committee_id
         left join wapdc.public.committee_contact cc on com.committee_id = cc.committee_id and cc.treasurer = true
         left join private.contacts c on c.trankeygen_id = t.trankeygen_id
         left join private.accounts a on a.trankeygen_id = t.trankeygen_id
         left join private.receipts r on r.trankeygen_id = t.trankeygen_id
         left join private.contacts c2 on c2.trankeygen_id = r.contid
         left join private.expenditureevents e on e.trankeygen_id = t.trankeygen_id
         left join private.auctionitems au on au.trankeygen_id = t.trankeygen_id
         left join private.depositevents dep on dep.trankeygen_id = t.trankeygen_id
         left join private.loans l on l.trankeygen_id = t.trankeygen_id
where t.orca_id = 59
order by 2