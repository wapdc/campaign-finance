insert into private.derby_update(fund_id, message, sql)
select fund_id,
    'The PDC has discovered an error in your campaign’s chart of accounts data caused by the most recent ORCA update released July 21.' message,
    'INSERT INTO TRANKEYGEN(ID, PID, DERBY_UPDATED) VALUES ('
        || id || ',' || pid || ', CURRENT_TIMESTAMP);INSERT INTO accounts(id, pid, acctnum, contid, style,total) ' ||
    'VALUES (' || id || ',' || pid || ',' || acctnum || ',' || contid || ','|| style || ',' || total || ');'
    || CASE WHEN contact_id is not  null then
       'INSERT INTO CONTACTS(id,type,name,street,city,state,zip) VALUES('
        || id || ','
        || quote_nullable(type) || ','
        || quote_nullable(name) || ','
        || quote_nullable(street) || ','
        || quote_nullable(city) || ','
        || quote_nullable(state) || ','
        || quote_nullable(zip) || '); '
        else '' end    as sql from
 (select
       t.fund_id,
       t.orca_id as id,
       COALESCE(t2.orca_id, 0) as pid,
       a.acctnum,
       a.contid,
       a.style,
       a.total,
       c.trankeygen_id as contact_id,
       c.type,
       c.name,
       c.street,
       c.city,
       c.state,
       c.zip
from private.trankeygen t
         join private.missing_59 m on t.fund_id = m.fund_id
         join private.accounts a on a.trankeygen_id = t.trankeygen_id
         left join private.trankeygen t2 ON t2.trankeygen_id = t.pid
         left join private.contacts c on c.trankeygen_id = t.trankeygen_id
where t.orca_id = 59 order by t.fund_id) as accounts
