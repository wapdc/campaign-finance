do $$
    declare c record;
    begin
        FOR c in select row_number()  over (partition by t2.fund_id, t2.orca_id order by trankeygen_id desc) r,
               max(t2.trankeygen_id) over (partition by t2.fund_id, t2.orca_id) as corrected_id,
               t2.trankeygen_id from private.trankeygen t2
        where (t2.fund_id, t2.orca_id) in (select t.fund_id, t.orca_id
                                           from private.trankeygen t
                                           group by fund_id, orca_id
                                           having count(1) > 1)
        loop
            if c.r > 1 THEN
                -- trankeygen pointer records
                UPDATE private.trankeygen set pid = c.corrected_id where pid=c.trankeygen_id;

                -- Contacts
                UPDATE private.receipts set contid=c.corrected_id where contid = c.trankeygen_id;
                UPDATE private.accounts set contid=c.corrected_id where contid = c.trankeygen_id
                  and contid <> accounts.trankeygen_id;
                update private.couplecontacts set contact1_id = c.corrected_id where contact1_id = c.trankeygen_id;
                update private.couplecontacts set contact2_id = c.corrected_id where contact2_id = c.trankeygen_id;

                -- accounts on receipts
                update private.receipts set cid = c.corrected_id where cid=c.trankeygen_id;
                update private.receipts set did = c.corrected_id where did=c.trankeygen_id;

                -- deposit events
                update private.depositevents set account_id= c.corrected_id where account_id = c.trankeygen_id;

                update private.deposititems set rcptid = c.corrected_id where rcptid = c.trankeygen_id
                  and deptid not in  (select deptid from private.deposititems where rcptid = c.trankeygen_id);

                -- expenditure
                update private.expenditureevents set bnkid = c.corrected_id where bnkid = c.trankeygen_id;
                update private.expenditureevents set contid = c.corrected_id where contid = c.trankeygen_id;

                -- auction items
                update private.auctionitems set pid=c.corrected_id where pid=c.trankeygen_id;

                -- loans
                update private.loans set cid = c.corrected_id where cid=c.trankeygen_id;
                update private.loans set did = c.corrected_id where did=c.trankeygen_id;
                update private.loans set contid = c.corrected_id where contid = c.trankeygen_id;


                -- debt obligations
                UPDATE private.debtobligation set contid=c.corrected_id where contid = c.trankeygen_id;
                UPDATE private.debtobligation set pid = c.corrected_id where pid = c.trankeygen_id;

                -- receipt pid
                update private.receipts set pid = c.corrected_id where pid = c.trankeygen_id;

                -- group contacts
                update private.registeredgroupcontacts set contid = c.corrected_id where contid = c.trankeygen_id;

                -- finally delete the trankeygen record
                delete from private.trankeygen where trankeygen_id = c.trankeygen_id;

            end if;
        end loop;
    end;

$$ language plpgsql;