update private.receipts r
set deptkey = d.deptkey
from (
       select e.deptkey, i.rcptid
       from private.trankeygen t
              join private.depositevents e on t.trankeygen_id = e.trankeygen_id
              left join private.deposititems i on e.trankeygen_id = i.deptid
       where fund_id = :fund_id) d
where d.rcptid = r.trankeygen_id and r.deptkey = 0