select c.name, f.election_code, f.fund_id, row_number()  over (partition by t2.fund_id, t2.orca_id order by trankeygen_id desc) r,
       max(t2.trankeygen_id) over (partition by t2.fund_id, t2.orca_id) as corrected_id,
       t2.trankeygen_id from private.trankeygen t2
       join fund f on t2.fund_id= f.fund_id
       join committee c on f.committee_id = c.committee_id
where (t2.fund_id, t2.orca_id) in (select t.fund_id, t.orca_id
                                   from private.trankeygen t
                                   group by fund_id, orca_id
                                   having count(1) > 1);