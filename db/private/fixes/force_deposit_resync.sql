INSERT INTO private.derby_update (fund_id, sql, message)
select f.fund_id, 'UPDATE TRANKEYGEN SET DERBY_UPDATED = CURRENT_TIMESTAMP WHERE ID IN (SELECT ID FROM DEPOSITEVENTS);' as sql,
        'Fix for deposits not showing as reported.  Close and reopen campaign after running.' as message
from fund f
   join private.properties p  on p.fund_id = f.fund_id and p.name='GUID'
where f.committee_id = :committee_id;


