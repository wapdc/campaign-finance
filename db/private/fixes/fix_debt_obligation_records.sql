 INSERT INTO private.derby_update (fund_id, sql, message)
          select fund_id, 'UPDATE TRANKEYGEN SET DERBY_UPDATED = CURRENT_TIMESTAMP WHERE ID IN (SELECT ID FROM receipts where type in (19,20,29,32,44));',
                'Force resync of all debt to fix bug prior to version 1.441.  Please, close and reopen campaign after running.'
        from (select distinct vr.fund_id from private.vreceipts vr left join private.derby_update du on du.fund_id=vr.fund_id
        where
          vr.type in (20,32,29,44)) v;
