-- This update statement is used to make sure we've matched up the C3 records for reports filed after the 15th
-- external ID sets will have been broken because new trankeygen records will have been created.
UPDATE report ru set external_id = trankeygen_id from (select r.fund_id, r.report_id, r.period_start,
       count(r.report_id) over (partition by de2.trankeygen_id) as c_deposits,
       count(de2.trankeygen_id) over (partition by r.report_id ) as c_reports,
       de2.trankeygen_id
from (select r2.fund_id, r2.report_id, max(r2.period_start) as period_start, max(external_id) as external_id, sum(amount) as amount from report r2
  join transaction t on t.report_id =r2.report_id
  join report_sum rs on rs.transaction_id=t.transaction_id and rs.legacy_line_item is not null
    WHERE r2.submitted_at >= '2022-03-15'
    and r2.superseded_id is null
    and r2.report_type = 'C3'
    group by r2.fund_id, r2.report_id
    ) r
  left join private.vdepositevents de2 on de2.fund_id=r.fund_id
    and de2.date_ = r.period_start and de2.amount = r.amount
  left join private.vdepositevents de on de.trankeygen_id = r.external_id and r.fund_id = de.fund_id
  where
    de.trankeygen_id is null
    and de2.trankeygen_id is not null
    ) dv
where dv.report_id = ru.report_id
  and dv.c_deposits =1 and dv.c_reports = 1;



