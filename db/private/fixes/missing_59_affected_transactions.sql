select  t.fund_id, CASE WHEN r.trankeygen_id is not null then 'receipt' end as receipt,
                   case when a.trankeygen_id is not null then 'account' end as account,
                   case when c.trankeygen_id is not null then 'contact' end as contact,
                   case when e.trankeygen_id is not null then 'expenditureevent' end as expenditureevent,
                   case when au.trankeygen_id is not null then 'auction item' end as auction,
                   case when dep.trankeygen_id is not null then 'deposit' end as deposit,
                   case when l.trankeygen_id is not null then 'loan'
                    end as loan
from private.trankeygen t
         join private.missing_59 m on t.fund_id = m.fund_id
         join fund f on f.fund_id = t.fund_id
         join committee com on com.committee_id = f.committee_id
         left join wapdc.public.committee_contact cc on com.committee_id = cc.committee_id and cc.treasurer = true
         left join private.contacts c on c.trankeygen_id = t.trankeygen_id
         left join private.accounts a on a.trankeygen_id = t.trankeygen_id
         left join private.receipts r on r.trankeygen_id = t.trankeygen_id
         left join private.contacts c2 on c2.trankeygen_id = r.contid
         left join private.expenditureevents e on e.trankeygen_id = t.trankeygen_id
         left join private.auctionitems au on au.trankeygen_id = t.trankeygen_id
         left join private.depositevents dep on dep.trankeygen_id = t.trankeygen_id
         left join private.loans l on l.trankeygen_id = t.trankeygen_id
WHERE t.orca_id = 59
order by 2,3,4,5,6,7,8,1