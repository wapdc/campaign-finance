insert into private.derby_update(fund_id, message, sql)
select fund_id,
       'The PDC has discovered an error in your campaign’s data caused by the most recent ORCA update released July 21.' as message,
    'INSERT INTO TRANKEYGEN(ID, PID, DERBY_UPDATED) VALUES ('|| id || ',' || pid || ',CURRENT_TIMESTAMP);'
    || 'INSERT INTO expenditureevents(id,contid,bnkid,amount,date_,checkno,itemized,memo) VALUES('
    || id || ','
    || orca_contid || ','
    || orca_bnkid || ','
    || amount || ','
    || quote_nullable(date_) || ','
    || quote_nullable(checkno) || ','
    || itemized || ','
    || quote_nullable(memo) || ');'
    as sql

from
    (select t.fund_id,  t.orca_id as id, COALESCE(t2.orca_id,0) as pid, e.*,
            coalesce(tcont.orca_id,0) as orca_bnkid, coalesce(tcont.orca_id,0) as orca_contid
     from private.trankeygen t
              join private.missing_59 m on t.fund_id = m.fund_id
              join private.expenditureevents e on t.trankeygen_id = e.trankeygen_id
              left join private.trankeygen t2 ON t2.trankeygen_id = t.pid
              left join private.trankeygen tcont ON e.contid = tcont.trankeygen_id
              left join private.trankeygen tbnk ON e.bnkid = tbnk.trankeygen_id
     where t.orca_id = 59 order by t.fund_id
    ) as statements