insert into private.derby_update(fund_id, message, sql)
select fund_id,
       'The PDC has discovered an error in your campaign’s contact data caused by the most recent ORCA update released July 21.',
       'INSERT INTO TRANKEYGEN(ID, PID, DERBY_UPDATED) VALUES ('|| id || ',' || pid || ', current_timestamp);'
 || 'INSERT INTO CONTACTS(id,type,name,street,city,state,zip,phone,email,occupation,employername, employerstreet, employercity, employerstate, employerzip, memo, pagg,gagg)'
 || 'VALUES('
     || id || ','
     || quote_nullable(type) || ','
     || quote_nullable(name) || ','
     || quote_nullable(street) || ','
     || quote_nullable(city) || ','
     || quote_nullable(state) || ','
     || quote_nullable(zip) || ','
     || quote_nullable(phone) || ','
     || quote_nullable(email) || ','
     || quote_nullable(occupation) || ','
     || quote_nullable(employername) || ','
     || quote_nullable(employerstreet) || ','
     || quote_nullable(employercity) || ','
     || quote_nullable(employerstate) || ','
     || quote_nullable(employerzip) || ','
     || quote_nullable(memo) || ','
     || pagg || ','
     || gagg || ');'
     || CASE WHEN type IN ('IND', 'CAN') then
         'INSERT INTO individualcontacts(id,prefix,firstname,middleinitial,lastname,suffix) VALUES('
         || id || ','
         || quote_nullable(prefix) || ','
         || quote_nullable(firstname) || ','
         || quote_nullable(middleinitial) || ','
         || quote_nullable(lastname) || ','
         || quote_nullable(suffix) || ');'
         else '' END as sql
from
    (select t.fund_id,  t.orca_id as id, COALESCE(t2.orca_id,0) as pid,
     c.*, ci.prefix, ci.firstname, ci.middleinitial, ci.lastname, ci.suffix
     from private.trankeygen t
              join private.missing_59 m on t.fund_id = m.fund_id
              join private.contacts c on c.trankeygen_id = t.trankeygen_id
              left join private.accounts a on a.trankeygen_id = t.trankeygen_id
              left join private.individualcontacts ci on ci.trankeygen_id = t.trankeygen_id
              left join private.trankeygen t2 ON t2.trankeygen_id = t.pid

     where t.orca_id = 59
        and a.trankeygen_id is null
     order by t.fund_id
    ) as statements