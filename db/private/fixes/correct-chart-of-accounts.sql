-- This script is used to make the totals in the chart of accounts be accurate
-- by recalculating the chart based on what it is in the receipts table.
update private.accounts t set total = v.corrected_amount
  from (
       select a.fund_id, a.trankeygen_id, a.acctnum, a.name, a.total, coalesce( dyn.amount,0.0) as corrected_amount from private.vaccounts a
         left join (select idyn.fund_id, idyn.account_id, sum(idyn.amount) as amount
            from (select fund_id, account_id, amount
               from (select fund_id, cid as account_id, sum(amount) as amount
                     from private.vreceipts r
                     where r.did <> r.cid
                     group by fund_id, cid) credits
               union all
               (select fund_id, did, sum(-1 * amount) as amount
                from private.vreceipts r2
                where r2.did <> r2.cid
            group by fund_id, did)) idyn group by idyn.fund_id, idyn.account_id) dyn on dyn.account_id=a.trankeygen_id
       where
         a.total <> coalesce(dyn.amount,0.0)
   ) v
where t.trankeygen_id = v.trankeygen_id;