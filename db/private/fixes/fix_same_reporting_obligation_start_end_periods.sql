do $$
    declare r record; vcount integer := 0;
    begin
        for r in select distinct fund_id from private.reporting_obligation ob where ob.startdate = ob.enddate loop
            delete from private.reporting_obligation ob where ob.fund_id = r.fund_id and ob.startdate = ob.enddate;

            perform private.cf_populate_reporting_obligations(r.fund_id);
            vcount:= vcount + 1;
            end loop;

        raise notice 'Modified % fund', vcount;
    end;

$$ language plpgsql;