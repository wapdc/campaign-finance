do $$
    declare p_fund_id integer;
    begin
        p_fund_id  := cast(:fund_id as integer);
        perform private.cf_delete_orca_campaign(p_fund_id);
    end;

    $$ language plpgsql;