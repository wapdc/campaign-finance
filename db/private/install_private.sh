#!/usr/bin/env bash

set -e

# Run scripts out of postgres directory
cd db/private/tables

echo "Creating orca tables..."
  $PSQL_CMD wapdc -f install.sql
