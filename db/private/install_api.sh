#!/usr/bin/env bash

set -e

# Run scripts out of db/private directory
cd db/private/functions

echo "Creating orca functions..."
  $PSQL_CMD wapdc -f install.sql

cd ../views
echo "Creating orca views..."
  $PSQL_CMD wapdc -f install.sql
