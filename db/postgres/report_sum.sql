drop table if exists report_sum cascade;
create table report_sum (
                          transaction_id int primary key,
                          aggregate_amount numeric (10,2),
                          legacy_line_item text,
                          foreign key (transaction_id) references transaction(transaction_id) on delete cascade
);
