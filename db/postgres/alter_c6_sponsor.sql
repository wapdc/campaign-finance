BEGIN TRANSACTION;

ALTER TABLE wapdc.public.c6_sponsor ADD CONSTRAINT c6_sponsor_pk PRIMARY KEY (sponsor_id);
CREATE SEQUENCE IF NOT EXISTS c6_sponsor_sponsor_id_seq MINVALUE 2000;
ALTER TABLE c6_sponsor ALTER sponsor_id SET DEFAULT nextval('c6_sponsor_sponsor_id_seq');
DROP TABLE IF EXISTS c6_sponsor_request;
CREATE TABLE c6_sponsor_request
(
    sponsor_request_id  serial primary key,
    uid                 varchar(255) not null,
    realm               varchar(255) not null,
    name                varchar,
    submitted_at        timestamp(0) with time zone default now(),
    sponsor_type        varchar,
    email               varchar(255),
    sponsor_id          integer,
    address             text,
    premise             text,
    city                text,
    state               text,
    postcode            text,
    phone               text
);

END TRANSACTION;
