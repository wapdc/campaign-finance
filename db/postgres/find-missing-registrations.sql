select registration_id - prior_registration_id -1 record_count, least(submitted_at, last_submission) as restore_date, v.* from
    (select
         registration_id,
         lag(registration_id) over (partition by 1 order by registration_id) as prior_registration_id,
         submitted_at,
         lag(submitted_at) over (PARTITION BY 1 ORDER BY registration_id desc) as last_submission
     from registration r where submitted_at > '2019-05-10' and r.source like 'campaign%') v
where registration_id - prior_registration_id >1
order by restore_date