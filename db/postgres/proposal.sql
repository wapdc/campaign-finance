CREATE TABLE proposal (
  proposal_id SERIAL PRIMARY KEY,
  proposal_type VARCHAR(32),
  number varchar(255),
  title VARCHAR(255),
  jurisdiction_id INT,
  election_code VARCHAR(16),
  verified BOOLEAN DEFAULT FALSE
);
