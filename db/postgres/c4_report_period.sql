CREATE OR REPLACE view c4_report_period AS
SELECT
    reporting_period_id,
    reporting_period_type,
    election_date,
    start_date,
    end_date,
    due_date,
    -- Deriving boolean values based on reporting_period_type
    CASE
        WHEN reporting_period.reporting_period_type::text = 'Monthly' THEN TRUE
        ELSE FALSE
        END AS monthly,
    CASE
        WHEN reporting_period_type IN ('SevenDayPrePrimary', 'SevenDayPreGeneral', 'SevenDayPreElection') THEN TRUE
        ELSE FALSE
        END AS pre7,
    CASE
        WHEN reporting_period_type IN ('TwentyOneDayPrePrimary', 'TwentyOneDayPreGeneral', 'TwentyOneDayPreElection') THEN TRUE
        ELSE FALSE
        END AS pre21,
    CASE
        WHEN reporting_period_type IN ('PostPrimary', 'PostGeneral', 'PostElection') THEN TRUE
        ELSE FALSE
        END AS post
FROM
    reporting_period where report_type='C4'