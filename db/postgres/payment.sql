drop table if exists payment cascade;
create table payment (
  transaction_id int primary key,
  principle_paid numeric(10,2),
  forgiven_amount numeric(10,2),
  interest_paid numeric(10,2),
  balance_owed numeric(10,2),
  original_amount numeric(10,2),
  foreign key (transaction_id) references transaction(transaction_id) on delete cascade
);
