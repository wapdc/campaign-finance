
INSERT INTO api_consumer (application, vendor, vendor_email, vendor_phone, vendor_contact_name) VALUES ('orca', 'PDC','ittech@pdc.wa.gov', '360-753-1111','James Gutholm');
INSERT INTO api_consumer_version (consumer_id, release_date, version, mandatory_update, fix) VALUES
(1,'2017-02-15','1.112','TRUE','Download www.pdc.wa.gov/learn/file-online/orca.' || chr(10) || 'Updated filing periods.' || chr(10) || 'Updated jurisdictions.'),
(1,'2017-07-06','1.113','FALSE','Download www.pdc.wa.gov/learn/file-online/orca.' || chr(10) || 'Corrects limits for union contributions.' || chr(10) || 'Minor interface enhancements.'),
(1,'2019-05-10','1.200','FALSE','Adds compatability with the PDC online registration system.' || chr(10) || 'Until you upgrade, you will not be able add new campaigns.' || chr(10) || 'See: https://www.pdc.wa.gov/learn/campaign-registration-changes.'),
(1,'2019-05-20','1.201','FALSE','Updates compatability with the PDC online registration system.' || chr(10) || 'Corrects an error while verifying tokens.' || chr(10) || 'Before updating see: https://www.pdc.wa.gov/learn/campaign-registration-changes.'),
(1,'2019-05-31','1.202','FALSE','Corrects duplicate candidate contact records.' || chr(10) || 'Before updating see: https://www.pdc.wa.gov/learn/campaign-registration-changes.'),
(1,'2019-06-03','1.203','FALSE','Fixes to improve C3 submissions for candidate fund submissions. Show correct contact edit screen for candidate contacts with validation errors, and only update candidate contact record if name contains NULL.' || chr(10) || 'Before updating see: https://www.pdc.wa.gov/learn/campaign-registration-changes.'),
(1,'2019-06-06','1.204','FALSE','Fixes to improve C3 submissions for candidate fund submissions. Show correct contact edit screen for candidate contacts with validation errors, and only update candidate contact record if name contains NULL.' || chr(10) || 'Before updating see: https://www.pdc.wa.gov/learn/campaign-registration-changes.'),
(1,'2019-06-17','1.206','FALSE','Contact fixes and middle name fix.' || chr(10) || 'Before updating see: https://www.pdc.wa.gov/learn/campaign-registration-changes.');
