CREATE TABLE registration (
  registration_id SERIAL PRIMARY KEY,
  committee_id INTEGER,
  name VARCHAR(255),
  valid_from DATE,
  valid_to DATE,
  user_data VARCHAR,
  admin_data VARCHAR,
  html VARCHAR,
  submitted_at TIMESTAMP(0) WITH TIME ZONE,
  updated_at TIMESTAMP(0) WITH TIME ZONE,
  username VARCHAR(255),
  source VARCHAR(32),
  version VARCHAR(32),
  memo VARCHAR,
  certification VARCHAR(255),
  certification_email VARCHAR(255),
  attachment_reference VARCHAR(255),
  verified BOOLEAN,
  FOREIGN KEY (committee_id) REFERENCES committee(committee_id) ON DELETE CASCADE
);
CREATE INDEX idx_registration_committee ON registration(committee_id);
CREATE INDEX idx_registration_submitted ON registration(submitted_at);

