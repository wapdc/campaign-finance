CREATE TABLE api_consumer
(
    consumer_id         SERIAL PRIMARY KEY,
    application         VARCHAR,
    vendor              VARCHAR,
    vendor_email        VARCHAR,
    vendor_phone        VARCHAR,
    vendor_contact_name VARCHAR,
    api_key             VARCHAR
)