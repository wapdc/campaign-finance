#!/usr/bin/env bash
set -e

# Make no op update to all reports for surplus funds tp update od record
$PSQL_CMD wapdc <<EOF

update report r
set fund_id = fund_id
where fund_id in (select fund_id
                  from fund f
                         join committee c on f.committee_id = c.committee_id
                  where c.pac_type = 'surplus')

EOF