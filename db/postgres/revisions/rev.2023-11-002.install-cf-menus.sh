#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc -f db/postgres/registration/cf_count_unverified_registrations.sql
$PSQL_CMD wapdc <<EOF
insert into menu_links(category, menu, title, abstract, url, permission, count_function)
  values (
          'Campaign finance',
          'staff',
          'Unverified registrations',
          'Verify candidate and committee registrations (C1) matched to candidates or political committees',
          '/campaigns/admin/unverified-committees',
          'enter wapdc data',
          'cf_count_unverified_registrations'
         );
insert into menu_links(category, menu, title, abstract, url, permission)
  values (
          'Campaign finance',
          'staff',
          'Contact search',
          'Search for registered committees (C1, C5) based on which officers or contacts are associated with the latest registration',
          '/campaigns/contact-search',
          'access wapdc data'
         );
insert into menu_links(category, menu, title, abstract, url, permission)
  values (
          'Campaign finance',
          'staff',
          'Unattached paper registrations',
          'Show paper-field registrations for committees that do not have an attached image document',
          '/campaigns/admin/paper-registrations',
          'enter wapdc data'
         );
insert into menu_links(category, menu, title, abstract, url, permission)
  values (
          'Campaign finance',
          'staff',
          'Independent expenditures reports (C6)',
          'Search the reporting history of independent expenditure reports (C6)',
          '/campaigns/admin/c6-reports',
          'access wapdc data'
         );
delete from menu_links where url='/campaigns/admin';
EOF

