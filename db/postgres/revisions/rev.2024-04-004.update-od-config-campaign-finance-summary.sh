#!/usr/bin/env bash
set -e

php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_campaign_finance_summary.json
