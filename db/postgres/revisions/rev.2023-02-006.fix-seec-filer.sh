$PSQL_CMD wapdc <<EOF
  insert into private.properties(fund_id, name, value)
  select fund_id, 'SEEC_FILER', seec from (
  select cf.fund_id, cast(user_data as json) -> 'local_filing' -> 'seec' ->> 'required' as seec
  from private.campaign_fund cf
  join fund f on f.fund_id = cf.fund_id and version > 1.50
  join committee c on c.committee_id = f.committee_id
  join registration r on c.registration_id = r.registration_id) x where x.seec = 'true'
  on conflict (fund_id, name) do nothing;
EOF