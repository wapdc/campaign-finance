#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
alter table private.campaign_fund
    add archive_file text;

alter table private.campaign_fund
    add archive_expiration date;
EOF
