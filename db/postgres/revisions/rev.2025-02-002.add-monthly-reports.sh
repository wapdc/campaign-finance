#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc -f db/postgres/c4_report_period.sql
$PSQL_CMD wapdc -f db/private/functions/reports/cf_populate_reporting_obligations.sql;

$PSQL_CMD wapdc <<EOF
  insert into reporting_period (reporting_period_id, reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES (default, 'Monthly', null, '2025-03-01', '2025-03-31', '2025-04-10', 'C4', null, null);
  INSERT INTO public.reporting_period (reporting_period_id, reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES (464, 'Monthly', null, '2028-02-01', '2028-02-29', '2028-03-10', 'C4', null, null);

  select private.cf_populate_reporting_obligations(fund_id)
  from private.reporting_obligation
  where '2025-02-01' < duedate and '2025-05-31' > duedate group by fund_id
EOF
