#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
ALTER TABLE reporting_period
    ADD COLUMN report_type VARCHAR(32) DEFAULT 'C4',
    ADD COLUMN reminder_date DATE,
    ADD COLUMN reminder_sent_date DATE,
    DROP COLUMN monthly,
    DROP COLUMN pre7,
    DROP COLUMN pre21,
    DROP COLUMN post;

alter table reporting_period
    drop constraint unique_start_end;

alter table reporting_period
    add constraint unique_start_end
        unique (start_date, end_date, report_type);
EOF