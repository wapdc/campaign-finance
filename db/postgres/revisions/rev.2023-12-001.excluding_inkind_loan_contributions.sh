#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc -f db/postgres/functions/fund_summary.sql
$PSQL_CMD wapdc -f db/postgres/open_data/od_contributions.sql