#!/usr/bin/env bash
set -e

echo "Updating config..."
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_campaign_finance_summary.json

echo "Dropping artifacts..."

$PSQL_CMD wapdc <<EOF
drop trigger if exists od_c6_identified_entity_event_trigger on c6_identified_entity;
drop trigger if exists od_c6_reports_event_trigger on c6_reports;
drop function if exists c6_identified_entity_change_trigger_function();
drop function if exists c6_reports_change_trigger_function();
drop table if exists c6_candidacy;
EOF

echo "Done dropping artifacts"
