#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF
drop view od_campaign_and_committee_registrations;
EOF

$PSQL_CMD wapdc <<EOF
delete from open_data_config where job_name = 'cf_registrations';
EOF

# LMC dependency dropped - https://gitlab.com/wapdc/lmc/-/merge_requests/123
$PSQL_CMD wapdc <<EOF
drop function proposal_stance(varchar, varchar) cascade;
EOF

pushd db/postgres/functions
$PSQL_CMD wapdc -f install.sql
popd

pushd db/postgres/open_data
$PSQL_CMD wapdc -f install.sql
popd




