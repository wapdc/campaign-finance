#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF

alter table public.report drop column submission_version cascade;

EOF
