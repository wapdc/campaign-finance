#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF

alter table private.receipts add column if not exists data JSON;

EOF
