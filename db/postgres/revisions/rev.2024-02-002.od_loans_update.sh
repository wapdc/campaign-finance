#!/usr/bin/env bash
set -e

pushd db/postgres/open_data
$PSQL_CMD wapdc -f od_loans.sql
$PSQL_CMD wapdc -f od_loans_by_id.sql
$PSQL_CMD wapdc -f od_loans_by_report_id.sql
