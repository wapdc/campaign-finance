#!/usr/bin/env bash
set -e

# Adds function to public schema
$PSQL_CMD wapdc -f db/postgres/open_data/od_candidates_and_committees.sql
