#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc -f db/postgres/fixes/fix_loan_forgiven.sql
. db/postgres/revisions/install-od-config-and-views.sh
