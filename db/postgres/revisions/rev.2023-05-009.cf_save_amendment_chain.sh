#!/usr/bin/env bash
set -e

# Adds function to public schema
$PSQL_CMD wapdc -f db/postgres/functions/reports/cf_save_amendment_chain.sql

# Remove this function from the private schema
$PSQL_CMD wapdc <<EOF
DROP function private.cf_save_amendment_chain(p_report_id INTEGER, p_submitted_at date, p_amended_report_id INTEGER);
EOF


