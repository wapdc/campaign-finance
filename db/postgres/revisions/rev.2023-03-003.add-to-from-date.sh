#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF
alter table public.committee_authorization add column if not exists granted_date DATE;
alter table public.committee_authorization add column if not exists expiration_date DATE;
insert into public.pdc_user values ('PDC', 'PDC', 'pdc@pdc.wa.gov', '2023-03-01 07:53:07 +00:00');
EOF
