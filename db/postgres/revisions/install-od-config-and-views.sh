#!/usr/bin/env bash
set -e

echo "Installing configuration files..."

php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_campaign_finance_reports.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_contributions.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_debts.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_expenditures.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_loans.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_pledges.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_surplus_funds_expenditures.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_surplus_funds_latest_reports.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_surplus_funds_reports.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_campaign_finance_summary.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_campaign_and_committee_registrations.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_imaged_documents_and_reports.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_imaged_documents_and_reports_ax.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_imaged_documents_and_reports_registrations.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_imaged_documents_and_reports_attachments.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_independent_expenditures.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_out_of_state_contributions.json
php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_out_of_state_expenditures.json

$PSQL_CMD wapdc <<EOF
delete from open_data_config where job_name = 'cf_reports';
EOF

cd db/postgres/functions
$PSQL_CMD wapdc -f install.sql
cd ../open_data
$PSQL_CMD wapdc -f install.sql

echo "All configuration installed successfully!"
