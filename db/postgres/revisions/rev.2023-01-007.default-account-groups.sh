#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF
alter table private.account_groups add column initial_account boolean default false;
update private.account_groups set initial_account = true where deprecated is false;
update private.account_groups set initial_account = false where title = 'Auctions';
update private.account_groups set initial_account = false where title = 'Bank interest';
update private.account_groups set initial_account = false where title = 'In-kind loans';
update private.account_groups set initial_account = false where title = 'Loans';
update private.account_groups set initial_account = false where title = 'Pledges';
update private.account_groups set initial_account = false where title = 'Vendor/agent debt';
update private.account_groups set initial_account = false where title = 'Fundraisers';
update private.account_groups set initial_account = false where title = 'Credit cards';
update private.account_groups set initial_account = false where title = 'Low cost fundraisers';
--update the account_groups to have category_codes
update private.account_groups set category_code = 'W' where title = 'Wages, salaries, benefits, payroll taxes';
update private.account_groups set category_code = 'V' where title = 'Signature gathering';
update private.account_groups set category_code = 'T' where title = 'Travel, accommodation, meals';
update private.account_groups set category_code = 'S' where title = 'Surveys, polling, research';
update private.account_groups set category_code = 'P' where title = 'Postage, mail permits, stamps, etc.';
update private.account_groups set category_code = 'OS-E' where title = 'Computers, tablets printers, software, phones, etc';
update private.account_groups set category_code = 'OS' where title = 'Office supplies, furniture, staff food, etc.';
update private.account_groups set category_code = 'O' where title = 'Other advertising';
update private.account_groups set category_code = 'N' where title = 'Newspaper/periodical advertising';
update private.account_groups set category_code = 'MILES' where title = 'Mileage reimbursement';
update private.account_groups set category_code = 'M' where title = 'Management and consulting services';
update private.account_groups set category_code = 'LE-SURPLUS' where title = 'Payment for candidate''s lost earnings';
update private.account_groups set category_code = 'LE' where title = 'Payment for candidate''s lost earnings';
update private.account_groups set category_code = 'L-SIGNS' where title = 'Printing signs';
update private.account_groups set category_code = 'L' where title = 'Printing brochures, literature, postcards';
update private.account_groups set category_code = 'G-RENT' where title = 'Rent, lease, mortgage, PO box rental';
update private.account_groups set category_code = 'G-BANK' where title = 'Bank charges';
update private.account_groups set category_code = 'G' where title = 'General operation and overhead';
update private.account_groups set category_code = 'FEE' where title = 'Filing fees';
update private.account_groups set category_code = 'F' where title = 'Fundraising';
update private.account_groups set category_code = 'D-GA' where title = 'Design/graphic art, etc.';
update private.account_groups set category_code = 'CHARITY-S' where title = 'Disposal of surplus funds to charity';
update private.account_groups set category_code = 'C-SURPLUS' where title = 'Transfer to surplus funds account';
update private.account_groups set category_code = 'C-PARTY' where title = 'Transfer to political party or legislative caucus committee';
update private.account_groups set category_code = 'C-NEW' where title = 'Transfer to new campaign';
update private.account_groups set category_code = 'C-GEN' where title = 'Transfer to general fund';
update private.account_groups set category_code = 'C-CHARITY' where title = 'Charity/community organization';
update private.account_groups set category_code = 'C' where title = 'Monetary contributions to PAC or candidate';
update private.account_groups set category_code = 'B-ROBO' where title = 'Robocalls';
update private.account_groups set category_code = 'B-RADIO' where title = 'Radio advertising';
update private.account_groups set category_code = 'B-MERCH' where title = 'Campaign merchandise/paraphernalia';
update private.account_groups set category_code = 'B-DIGI' where title = 'Digital advertising';
update private.account_groups set category_code = 'B' where title = 'Broadcast/cable TV advertising';
update private.account_groups set category_code = 'ALRC' where title = 'Accounting, legal, regulatory compliance, etc.';

alter table private.campaign_fund add column updated TIMESTAMP(0) WITH TIME ZONE default now();
EOF