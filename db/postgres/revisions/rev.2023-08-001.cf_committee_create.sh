#!/usr/bin/env bash
set -e

# Recompile cf_create_campaign
$PSQL_CMD wapdc -f db/private/functions/campaigns/cf_create_campaign.sql
