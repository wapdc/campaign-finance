#!/usr/bin/env bash
set -e

# Fix report version that was incorrectly sumbbted
$PSQL_CMD wapdc <<EOF
update report set version='1.1' where version not in ('1','1.0', '1.1');
EOF
