#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc -f db/private/functions/reports/cf_add_one_reporting_obligation.sql
$PSQL_CMD wapdc -f db/private/functions/reports/cf_potential_reporting_obligations_by_fund.sql
$PSQL_CMD wapdc -f db/postgres/functions/determine_special_reporting_periods_by_election_date.sql
$PSQL_CMD wapdc -f db/postgres/functions/potential_reporting_obligations.sql