#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF

alter table public.report add column if not exists submission_version varchar(30);
alter table public.report add column if not exists source varchar(16);
alter table public.campaign_finance_submission add column if not exists submission_version varchar(30);

EOF
