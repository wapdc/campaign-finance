#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc -f db/private/functions/debts/cf_save_invoiced_expense_payment.sql
$PSQL_CMD wapdc -f db/private/functions/reports/c4/cf_c4_get_debt.sql
$PSQL_CMD <<EOF
update private.receipts set nettype = 0 where trankeygen_id in (select vreceipts.trankeygen_id from private.vreceipts where did_acctnum =  2600 )
  and nettype =3;
EOF
