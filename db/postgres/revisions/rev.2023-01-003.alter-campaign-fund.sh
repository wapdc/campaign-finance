#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF
alter table private.campaign_fund add column save_count integer not null default 0;
EOF
