#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF
delete from menu_links where url = '/campaigns/admin/candidates-missing-filer-ids';
EOF