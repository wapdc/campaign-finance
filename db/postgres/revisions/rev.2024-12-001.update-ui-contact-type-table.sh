#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
    UPDATE public.ui_contact_type set label = 'County party committee' where label = 'Co Party Committee';
    UPDATE public.ui_contact_type set label = 'Financial institution' where label = 'Financial Institution';
    UPDATE public.ui_contact_type set label = 'Legislative caucus committee' where label = 'Leg Caucus Committee';
    UPDATE public.ui_contact_type set label = 'Legislative district party committee' where label = 'Leg Dist Party Committee';
    UPDATE public.ui_contact_type set label = 'Minor party committee' where label = 'Minor Party Committee';
    UPDATE public.ui_contact_type set label = 'Other organization' where label = 'Other Organization';
    UPDATE public.ui_contact_type set label = 'Political action committee' where label = 'Political Action Committee';
    UPDATE public.ui_contact_type set label = 'State party committee' where label = 'St Party Committee';
    UPDATE public.ui_contact_type set label = 'Tribal political committee' where label = 'Tribal Political Committee';
EOF
