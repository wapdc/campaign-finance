#!/usr/bin/env bash
set -e

# install trigger and related proc
$PSQL_CMD wapdc -f db/postgres/triggers/update_entity_id_when_committee_id_changes.sql

# add entity_id column to fund table and populate it initially
$PSQL_CMD wapdc <<EOF

ALTER TABLE public.fund ADD COLUMN IF NOT EXISTS entity_id INTEGER;

CREATE INDEX idx_fund_entity_id ON public.fund (entity_id);

UPDATE public.fund AS f SET entity_id = c.person_id
FROM public.committee AS c
  WHERE c.person_id IS NOT NULL
  AND f.entity_id IS NULL
  AND f.committee_id = c.committee_id;


EOF