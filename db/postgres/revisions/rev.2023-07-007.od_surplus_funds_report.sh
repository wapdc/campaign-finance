#!/usr/bin/env bash
set -e

# Adds function to public schema
$PSQL_CMD wapdc -f db/postgres/open_data/od_surplus_funds_reports.sql
