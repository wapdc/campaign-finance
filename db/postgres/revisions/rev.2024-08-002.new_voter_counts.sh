#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF
--delete previous 2023 votercount records to make sure we don't include counts from any counties that are no longer part of jurisdiction
delete from votercount where jurisdiction_id in (select jurisdiction_id from jurisdiction where category = 4) and year = 2023;

--populate the votercount for all legislative districts
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4789, '32', 103846, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4787, '17', 106558, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4781, '27', 58815, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4781, '34', 43661, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4795, '01', 5570, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4795, '02', 14543, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4795, '07', 2865, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4795, '12', 1639, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4795, '22', 8379, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4795, '32', 42550, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4795, '38', 22648, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4791, '04', 917, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4809, '03', 31867, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4809, '11', 23608, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4809, '13', 246, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4791, '09', 22193, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4791, '10', 5143, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4791, '24', 25614, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4791, '26', 10792, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4791, '32', 7218, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4791, '33', 34389, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4783, '32', 100576, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4785, '32', 110979, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4793, '03', 83481, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4793, '11', 12166, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4779, '17', 60703, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4779, '31', 39328, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4809, '36', 36884, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4815, '08', 48209, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4815, '14', 15832, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4815, '21', 14848, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4815, '25', 16816, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4815, '34', 3258, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4815, '35', 3475, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4819, '31', 92513, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4799, '17', 87024, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4797, '15', 61617, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4797, '29', 13709, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4797, '31', 36452, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4811, '06', 88121, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4811, '20', 10795, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4811, '30', 9109, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4807, '03', 6718, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4807, '39', 85630, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4805, '03', 4625, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4805, '11', 8422, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4805, '20', 5018, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4805, '39', 42455, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4813, '06', 105970, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4801, '04', 50903, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4801, '17', 26480, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4801, '31', 28276, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4803, '01', 2495, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4803, '09', 4161, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4803, '13', 47815, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4803, '19', 29696, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4817, '06', 46941, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4817, '08', 24427, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4817, '21', 40280, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4817, '34', 1305, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4827, '27', 96322, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4831, '27', 93147, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4821, '23', 1, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4821, '34', 105341, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4872, '17', 89720, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4870, '17', 101583, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4854, '31', 91972, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4825, '05', 57267, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4825, '14', 32767, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4825, '16', 27588, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4856, '29', 26563, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4856, '31', 81078, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4847, '18', 23479, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4847, '23', 44513, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4847, '34', 44637, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4841, '17', 51591, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4841, '31', 48177, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4845, '17', 104851, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4860, '17', 98432, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4868, '17', 97044, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4839, '17', 12813, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4839, '27', 91361, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4862, '37', 111232, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4829, '18', 57286, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4829, '27', 53309, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4833, '27', 79415, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4866, '31', 95764, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4876, '06', 92049, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4852, '17', 99820, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (3558, '18', 108168, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4858, '28', 14649, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4858, '29', 45484, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4858, '37', 48877, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4835, '27', 80838, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4850, '17', 108345, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4864, '17', 92370, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4843, '17', 81324, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4837, '17', 80355, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4874, '17', 77777, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4735, '32', 103846, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4734, '17', 106558, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4731, '27', 58815, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4731, '34', 43661, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4738, '01', 5570, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4738, '02', 14543, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4738, '07', 2865, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4738, '12', 1639, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4738, '22', 8379, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4738, '32', 42550, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4738, '38', 22648, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4736, '04', 917, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4745, '03', 31867, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4745, '11', 23608, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4745, '13', 246, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4736, '09', 22193, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4736, '10', 5143, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4736, '24', 25614, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4736, '26', 10792, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4736, '32', 7218, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4736, '33', 34389, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4732, '32', 100576, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4733, '32', 110979, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4737, '03', 83481, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4737, '11', 12166, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4730, '17', 60703, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4730, '31', 39328, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4745, '36', 36884, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4748, '08', 48209, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4748, '14', 15832, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4748, '21', 14848, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4748, '25', 16816, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4748, '34', 3258, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4748, '35', 3475, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4750, '31', 92513, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4740, '17', 87024, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4739, '15', 61617, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4739, '29', 13709, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4739, '31', 36452, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4746, '06', 88121, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4746, '20', 10795, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4746, '30', 9109, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4744, '03', 6718, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4744, '39', 85630, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4743, '03', 4625, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4743, '11', 8422, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4743, '20', 5018, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4743, '39', 42455, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4747, '06', 105970, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4741, '04', 50903, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4741, '17', 26480, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4741, '31', 28276, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4742, '01', 2495, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4742, '09', 4161, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4742, '13', 47815, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4742, '19', 29696, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4749, '06', 46941, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4749, '08', 24427, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4749, '21', 40280, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4749, '34', 1305, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4754, '27', 96322, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4756, '27', 93147, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4751, '23', 1, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4751, '34', 105341, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4776, '17', 89720, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4775, '17', 101583, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4767, '31', 91972, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4753, '05', 57267, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4753, '14', 32767, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4753, '16', 27588, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4768, '29', 26563, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4768, '31', 81078, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4764, '18', 23479, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4764, '23', 44513, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4764, '34', 44637, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4761, '17', 51591, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4761, '31', 48177, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4763, '17', 104851, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4770, '17', 98432, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4774, '17', 97044, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4760, '17', 12813, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4760, '27', 91361, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4771, '37', 111232, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4755, '18', 57286, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4755, '27', 53309, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4757, '27', 79415, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4773, '31', 95764, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4778, '06', 92049, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4766, '17', 99820, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4752, '18', 108168, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4769, '28', 14649, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4769, '29', 45484, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4769, '37', 48877, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4758, '27', 80838, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4765, '17', 108345, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4772, '17', 92370, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4762, '17', 81324, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4759, '17', 80355, 2023, '2024-05-06');
INSERT INTO votercount (jurisdiction_id, county, votercount, year, date) VALUES (4777, '17', 77777, 2023, '2024-05-06');

EOF
