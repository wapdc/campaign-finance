#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF

drop table if exists public.committee_proposal;
alter table transaction
    add if not exists expense_type integer default 0 not null;
comment on column transaction.expense_type is 'indicates how transaction amount is applied to expense';
-- Update transactions to new expense_type
update transaction t set expense_type = case when category = 'loan' and act = 'payment' then -1 else 1 end
from (select t_exp.transaction_id from transaction t_exp
              where (t_exp.category = 'expense' and (t_exp.act = 'payment' or t_exp.act = 'correction' or t_exp.act = 'refund')
                or ((t_exp.category = 'contribution' and t_exp.act = 'receipt' and t_exp.inkind = true) or
                    (t_exp.category = 'loan' and t_exp.act = 'payment')))
    ) v
where v.transaction_id = t.transaction_id;
-- Update already submitted payments that cause duplicates.
update transaction set expense_type=0 where transaction_id in (select t.transaction_id from private.campaign_fund cf join report r on r.fund_id=cf.fund_id join  transaction t on t.report_id=r.report_id
  where t.description ilike 'Debt payment' and r.submitted_at>='2022-11-18');
EOF
# Recompile expense saving procedure.
$PSQL_CMD wapdc -f db/postgres/functions/cf_save_expenses.sql
$PSQL_CMD wapdc -f db/postgres/functions/fund_summary.sql