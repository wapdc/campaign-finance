#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF
alter table public.fund add column vendor text;
update fund f set vendor = q.vendor from (select fund_id, case when version = '100' then 'Unknown external vendor' else 'PDC' end as vendor, r from (select fund_id, version,  row_number() over (partition by cf.fund_id order by submitted_at desc) as r from campaign_finance_submission cf) v
    where v.r = 1) q
  where q.fund_id=f.fund_id
    and f.vendor is null;
update public.fund set vendor='PDC' where fund_id in (select fund_id from fund f where f.fund_id in (select fund_id from private.properties)
  and f.vendor is null);
EOF
