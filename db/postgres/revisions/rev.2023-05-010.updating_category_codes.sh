#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF

-- update category codes for account groups
update private.account_groups set category_code='DEBT' where acctnum=5001;
update private.account_groups set category_code='OTHER' where acctnum=5002;
update private.account_groups set category_code='C-CHARITY' where acctnum=5020;
update private.account_groups set category_code='C-CHARITY' where acctnum=5033;
update private.account_groups set category_code='C-PARTY' where acctnum=5034;
update private.account_groups set category_code='C' where acctnum=5036;
update private.account_groups set category_code='C' where acctnum=5037;
update private.account_groups set category_code='C' where acctnum=5038;
update private.account_groups set category_code='OTHER' where acctnum=5039;
update private.account_groups set category_code='G' where acctnum=5040;
update private.account_groups set category_code='W' where acctnum=5050;
update private.account_groups set category_code='OTHER' where acctnum=5080;
update private.account_groups set category_code='I' where acctnum=5090;
update private.account_groups set category_code='G' where acctnum=5100;
update private.account_groups set category_code='INT' where acctnum=5110;
update private.account_groups set category_code='ALRC' where acctnum=5120;
update private.account_groups set category_code='G' where acctnum=5140;
update private.account_groups set category_code='OTHER' where acctnum=5150;
update private.account_groups set category_code='W' where acctnum=5180;
update private.account_groups set category_code='B-DIGI' where acctnum=5225;
update private.account_groups set category_code='G' where acctnum=5230;
update private.account_groups set category_code='G' where acctnum=5250;
update private.account_groups set category_code='G' where acctnum=5260;
update private.account_groups set category_code='B-DIGI' where acctnum=5265;
update private.account_groups set category_code='G' where acctnum=5280;
update private.account_groups set category_code='B-DIGI' where acctnum=5302;
update private.account_groups set category_code='LE' where acctnum=5306;
update private.account_groups set category_code='INET' where acctnum=5320;
update private.account_groups set category_code='B-MERCH' where acctnum=5340;
update private.account_groups set category_code='F' where acctnum=5345;
update private.account_groups set category_code='OTHER' where acctnum=6000;
update private.account_groups set category_code='OS' where acctnum=6020;
update private.account_groups set category_code='F' where acctnum=6030;
update private.account_groups set category_code='OTHER' where acctnum=6214;
update private.account_groups set category_code='OTHER' where acctnum=6246;
update private.account_groups set category_code='OTHER' where acctnum=6248;
update private.account_groups set category_code='OS' where acctnum=15175;

-- update deprecated for account groups
update private.account_groups set deprecated=TRUE where acctnum=5090;

-- update title for "miscellaneous" to say "other"
update private.account_groups set title='Other' where acctnum=5150;

-- for expense categories
insert into public.expense_category values ('L-PYMT', 'Loan payments', null, FALSE, 'e', TRUE);
insert into public.expense_category values ('D-PYMT', 'Debt payments', null, FALSE, 'e', TRUE);
insert into public.expense_category values ('INT', 'Interest', null, FALSE, 'e', TRUE);
insert into public.expense_category values ('REF', 'Contribution Refunds', null, FALSE, 'e', TRUE);

EOF