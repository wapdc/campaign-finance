#!/usr/bin/env bash
set -e

echo "Reinstalling open data views..."
./db/postgres/revisions/install-od-config-and-views.sh
echo "Reinstall of open data views complete..."
