#!/usr/bin/env bash
set -e

$PSQL_CMD wapdc <<EOF
-- Update
alter table expense
    add if not exists payee text,
    add if not exists creditor text;
EOF
#recompile expense saving procedure
$PSQL_CMD wapdc -f db/postgres/functions/cf_save_expenses.sql
