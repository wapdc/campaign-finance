#!/usr/bin/env bash
$PSQL_CMD wapdc -f db/postgres/open_data/od_imaged_documents_and_reports.sql
# Perform a no-oop update on all LMC's that were filed by clients or firms to force publication
$PSQL_CMD wapdc <<EOF
update report r set report_type=r.report_type where report_id
in (select r2.report_id from report r2 join fund f on f.fund_id=r2.fund_id where r2.report_type='LMC' and
    f.target_type in ('lobbyist_firm', 'lobbyist_client'));
EOF
