#!/usr/bin/env bash
set -e

echo "Installing configuration..."

php vendor/wapdc/core/bin/wapdc_core.php load-od-config db/postgres/open_data/od_surplus_funds_reports.json
