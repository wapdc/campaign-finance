#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc -f db/private/functions/credit_cards/cf_get_credit_card_details.sql
$PSQL_CMD wapdc -f db/private/functions/reports/c4/cf_c4_get_debt.sql
$PSQL_CMD wapdc -f db/private/functions/reports/c4/cf_c4_get_expenses.sql
