#!/usr/bin/env bash
set -e
pushd db/postgres/functions
$PSQL_CMD wapdc -f install.sql
popd
pushd db/postgres/open_data
$PSQL_CMD wapdc -f install.sql
popd
. db/postgres/revisions/install-od-config-and-views.sh
