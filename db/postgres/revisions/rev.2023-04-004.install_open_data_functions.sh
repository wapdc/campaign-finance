#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc -f db/postgres/functions/fund_summary.sql

cd db/postgres/open_data
$PSQL_CMD wapdc -f install.sql
