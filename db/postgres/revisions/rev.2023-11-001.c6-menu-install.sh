#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc -f db/postgres/c6/c6_count_sponsor_requests.sql
$PSQL_CMD wapdc -f db/postgres/c6/c6_count_unmatched_reports.sql
$PSQL_CMD wapdc <<EOF
insert into menu_links(category, menu, title, abstract, url, permission, count_function)
  values (
          'Campaign finance',
          'staff',
          'Unverified sponsor requests',
          'Verify requests to file independent expenditure reports (C6) for committees, candidates and other C6 sponsors ',
          '/iexpenditures/admin',
          'enter wapdc data',
          'c6_count_sponsor_requests'
         );
insert into menu_links(category, menu, title, abstract, url, permission)
values (
           'Campaign finance',
           'staff',
           'C6 Candidates missing filer ids',
           'Match C6 independent expenditure reports to known candidates so that IE data can show up on our web site',
           '/campaigns/admin/candidates-missing-filer-ids',
           'enter wapdc data'
       );
EOF