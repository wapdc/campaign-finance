#!/usr/bin/env bash
set -e

# create the columns and populate the rows with existing committee information.
$PSQL_CMD wapdc <<EOF

alter table public.fund drop column if exists fund_type;
alter table public.fund add column if not exists target_type varchar(255) default 'committee' NOT NULL;
alter table public.fund add column if not exists target_id integer;

update public.fund as f set target_id = f2.committee_id
FROM public.fund AS f2
  WHERE f.fund_id = f2.fund_id
  AND f.target_type = 'committee'
  AND f.target_id IS NULL;

alter table public.fund alter column target_id set not null;

EOF

# install the trigger and related function to handle the changes
$PSQL_CMD wapdc -f db/postgres/triggers/fund_target_committee_trigger.sql