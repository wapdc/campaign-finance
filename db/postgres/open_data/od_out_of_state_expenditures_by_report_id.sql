DROP FUNCTION IF EXISTS od_out_of_state_expenditures_by_report_id(int);
CREATE OR REPLACE FUNCTION od_out_of_state_expenditures_by_report_id(int)
    RETURNS setof od_out_of_state_expenditures
    language plpgsql
AS
$$
declare
    committeeId  int;
begin
    select f.committee_id into committeeId
        from report r
            JOIN fund f on r.fund_id = f.fund_id where r.report_id = $1;
    return query
        select * from od_out_of_state_expenditures
        where report_id = $1 and committee_id = committeeId;
end
$$;