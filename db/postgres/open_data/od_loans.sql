/*
    This query provides information on loans to candidates and political committees
*/

--DROP VIEW IF EXISTS od_loans CASCADE;
CREATE OR REPLACE VIEW od_loans
AS

select
t.transaction_id AS id,
t.report_id AS report_number,
transaction_origin(r.form_type, t.category, t.act, t.inkind, t.itemized) AS origin,
cc.committee_id AS committee_id,
f.filer_id AS filer_id,
cc.filer_type AS type,
cc.filer_name AS filer_name,
cc.office AS office,
cc.legislative_district AS legislative_district,
cc.position AS position,
cc.party AS party,
cc.jurisdiction AS jurisdiction,
cc.jurisdiction_county AS jurisdiction_county,
cc.jurisdiction_type AS jurisdiction_type,
cc.election_year AS election_year,
cash_or_in_kind(t.inkind) AS cash_or_in_kind,
case when t.act = 'carry_forward' then 'Yes' else 'No' end AS carry_forward_loan,
case when t.act = 'carry_forward' OR t.inkind = true
     then t.description end as description,
open_data_date_format(t.transaction_date) AS receipt_date,
l.payment_schedule AS repayment_schedule,
open_data_date_format(l.due_date) AS loan_due_date,
lender_or_endorser(l.lender_endorser) AS lender_or_endorser,
case
    when vi.tran_type = 'pmt' then 'Payment'
    when vi.tran_type = 'int' then 'Interest'
    when t.act = 'forgiven' then 'Forgiven'
    else 'Received' end as transaction_type,
case
    when vi.tran_type = 'pmt' then (-1 * t.amount)::numeric(16,2)
    when vi.tran_type = 'int' then (p.interest_paid)::numeric(16,2)
    when t.act = 'forgiven' then (-1 * p.forgiven_amount)::numeric(16,2)
    else t.amount::numeric(16,2) end as amount,
l.liable_amount AS endorser_liable_amount,
primary_or_general(c.prim_gen) AS primary_general,
c.name AS lenders_name,
c.address AS lenders_address,
c.city AS lenders_city,
c.state AS lenders_state,
c.postcode AS lenders_zip,
c.occupation AS lenders_occupation,
c.employer AS lenders_employer,
c.employer_city AS employers_city,
c.employer_state AS employers_state,
pdc_report_url(r.user_data, r.report_id, false) as url

from od_candidates_and_committees cc
     join fund f on f.committee_id = cc.committee_id and left(f.election_code, 4)::int = cc.election_year
     join report r on r.fund_id = f.fund_id
     join transaction t on t.report_id = r.report_id
     left join contribution c on c.transaction_id = t.transaction_id
     left join loan l on l.transaction_id = t.transaction_id
     left join payment p on p.transaction_id = t.transaction_id
     left join ( select * from  (VALUES ('int'), ('pmt')) AS t (tran_type)) vi
               on t.act = 'payment' and ((p.principle_paid > 0 AND vi.tran_type = 'pmt') OR (p.interest_paid > 0 and vi.tran_type = 'int'))
where t.category='loan' and t.act is distinct from 'sum' and t.act is distinct from 'balance' and t.act is distinct from 'endorsement'
  and not (t.act = 'receipt' AND t.inkind = false AND r.report_type = 'C4') -- exclude cash loans reported on the c4
and r.superseded_id IS NULL
and r.report_type in ('C3', 'C4');
