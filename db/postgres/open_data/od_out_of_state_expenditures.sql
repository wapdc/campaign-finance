DROP VIEW IF EXISTS od_out_of_state_expenditures CASCADE;
CREATE OR REPLACE VIEW od_out_of_state_expenditures
AS
SELECT
    e.c5_expense_id                                                                 AS id,
    r.report_id,
    cc.filer_name                                                                   AS filer_name,
    cc.committee_id                                                                 AS committee_id,
    cc.committee_address                                                            AS committee_address,
    cc.committee_city                                                               AS committee_city,
    cc.committee_state                                                              AS committee_state,
    cc.committee_zip                                                                AS committee_zip,
    cc.committee_email                                                              AS committee_email,
    cc.person_id                                                                    AS entity_id,
    cc.election_year                                                                AS election_year,
    e.amount                                                                        AS amount,
    open_data_date_format(e.expense_date)                                      AS expense_date,
    CASE WHEN    e.type  = 'CA' then 'Candidate'
         else 'Committee'
        END                                                                         AS recipient_type,
    CASE WHEN e.target_type = 'committee' THEN e.target_id END                      AS recipient_committee_id,
    CASE WHEN e.target_type = 'candidate' THEN e.target_id END                      AS recipient_candidacy_id,
    coalesce(c.name,
             case when ca.ballot_name <> ent.name
                      then concat(ent.name, ' (', ca.ballot_name, ')')
                  else ent.name end,
             e.name)                                                                AS recipient_name,
    coalesce(fo.offtitle, lfo.offtitle)                                             AS recipient_office,
    j.name                                                                          AS recipient_jurisdiction,
    coalesce(p.name, lp.name)                                                       AS recipient_party,
    e.description                                                                   AS inkind_description,
    e.purpose                                                                       AS purpose,
    e.ballot_number                                                                 AS ballot_number,
    e.for_or_against                                                                AS for_or_against,
    e.inkind                                                                        AS inkind,
    e.vendor_name                                                                   AS vendor_name,
    e.vendor_city                                                                   AS vendor_city,
    e.vendor_state                                                                  AS vendor_state,
    e.vendor_postcode                                                               AS vendor_postcode,
    e.vendor_address                                                                AS vendor_address,
    cc.url

FROM od_candidates_and_committees cc
         JOIN report r on r.fund_id = cc.fund_id
         JOIN fund f on f.fund_id = r.fund_id
         JOIN c5_expense e on e.report_id = r.report_id
         LEFT JOIN candidacy ca on ca.candidacy_id = e.target_id and e.target_type = 'candidate'
         LEFT JOIN party p on p.party_id =  ca.party_id
         LEFT JOIN party lp on lp.party_id = e.party
         LEFT JOIN foffice fo on fo.offcode = ca.office_code
         LEFT JOIN foffice lfo on lfo.offcode = e.office
         LEFT JOIN jurisdiction j on j.jurisdiction_id = ca.jurisdiction_id
         LEFT JOIN committee c on c.committee_id = e.target_id and e.target_type = 'committee'
         LEFT JOIN entity ent on ent.entity_id = ca.person_id
where e.amount is not null;




