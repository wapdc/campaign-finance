DROP VIEW IF EXISTS od_out_of_state_contributions;
CREATE OR REPLACE VIEW od_out_of_state_contributions AS
SELECT
    cont.c5_contribution_id                                                         AS id,
    r.report_id,
    cc.filer_name                                                                   AS filer_name,
    cc.election_year                                                                AS election_year,
    cc.committee_id                                                                 AS committee_id,
    cc.committee_address                                                            AS committee_address,
    cc.committee_city                                                               AS committee_city,
    cc.committee_state                                                              AS committee_state,
    cc.committee_zip                                                                AS committee_zip,
    cc.committee_email                                                              AS committee_email,
    cc.person_id                                                                    AS entity_id,
    open_data_date_format(cont.contribution_date)                              AS contribution_date,
    cont.aggregate_total                                                            AS aggregate_total,
    cont.amount                                                                     AS amount,
    cont.resident                                                                   AS resident,
    cont.name                                                                       AS name,
    cont.address                                                                    AS address,
    cont.city                                                                       AS city,
    cont.state                                                                      AS state,
    cont.postcode                                                                   AS postcode,
    cont.employer                                                                   AS employer,
    cont.employer_address                                                           AS employer_address,
    cont.employer_city                                                              AS employer_city,
    cont.employer_state                                                             AS employer_state,
    cc.url
FROM od_candidates_and_committees cc
JOIN committee c on c.committee_id = cc.committee_id
JOIN fund f on f.fund_id = cc.fund_id and left(f.election_code, 4)::int = cc.election_year
JOIN report r on r.fund_id = f.fund_id
JOIN c5_contribution cont on cont.report_id = r.report_id
WHERE
   cont.amount is not null;
