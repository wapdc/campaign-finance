--DROP VIEW IF EXISTS od_campaign_finance_summary cascade ;

CREATE OR REPLACE VIEW od_campaign_finance_summary AS

SELECT cc.id                                                                              AS id,
       cc.fund_id,
       cc.candidacy_id,
       cc.committee_id,
       cc.person_id,
       cc.filer_id,
       cc.committee_type                                                                  as filer_type,
       cc.registered,
       cc.declared,
       cc.withdrew,
       cc.discontinued,
       amounts.fund_filing_type                                                           as filing_type,
       cc.pac_type,
       cc.continuing,
       cc.receipt_date,
       cc.election_year,
       cc.candidate_committee_status,
       cc.legislative_district,
       cc.filer_name,
       cc.committee_acronym,
       cc.committee_address,
       cc.committee_city,
       cc.committee_county,
       cc.committee_state,
       cc.committee_zip,
       cc.committee_email,
       cc.candidate_email,
       cc.candidate_committee_phone,
       cc.political_committee_type,
       cc.committee_category,
       case
           when cc.pac_type = 'bonafide' then 'X'
           else null end as bonafide_committee,
       case
           when cc.pac_type = 'bonafide' and upper(cc.committee_category) = 'MINOR PARTY' then 'Minor Party'
           when cc.pac_type = 'bonafide' then bonafide_type
           when upper(cc.committee_category) in ('DEMOCRATIC OTHER', 'REPUBLICAN OTHER') then 'Associated'
           else null end as bonafide_type,
       cc.office,
       cc.office_code,
       cc.jurisdiction,
       cc.jurisdiction_code,
       cc.jurisdiction_county,
       cc.jurisdiction_type,
       cc.jurisdiction_voters,
       cc.jurisdiction_reporting_code,
       cc.jurisdiction_reporting_requirement,
       coalesce(cast(cc.position_number as text), cc.position) as position,
       cc.party_code,
       cc.party,
       cc.election_date,
       cc.reporting_option,
       cc.active_candidate,
       cc.on_primary_election_ballot,
       cc.on_general_election_ballot,
       cc.primary_election_status,
       cc.general_election_status,
       cc.election_status,
       cc.treasurer_name,
       cc.treasurer_address,
       cc.treasurer_city,
       cc.treasurer_state,
       cc.treasurer_zip,
       cc.treasurer_phone,
       exempt_nonexempt,
       cc.ballot_committee,
       cc.ballot_proposal_count,
       cc.ballot_number,
       cc.ballot_title,
       cc.for_or_against,
       cc.ballot_proposal_detail,
       CASE
           when upper(cc.political_committee_type) = 'OTHER' then 'X'
           else '' end                                                                    as other_pac,
       CASE when cc.origin = 'C5' then
           c5_amounts.total_contributions
           else
           amounts.total_contributions end                                                 as contributions_amount,
       CASE when cc.origin = 'C5' then
           c5_amounts.total_expenditures
           else
           amounts.total_expenditures end                                                  as expenditures_amount,
       amounts.total_loans                                                                as loans_amount,
       last_report.total_pledges                                                          as pledges_amount,
       last_report.total_debt                                                             as debts_amount,
       ie.for_amount                                                                      as independent_expenditures_for_amount,
       ie.against_amount                                                                  as independent_expenditures_against_amount,
       amounts.total_carryforward                                                         as carryforward_amount,
       cc.url,
       to_char((select greatest(
                               cc.updated_at, amounts.updated_at,
                                last_report.updated_at,
                               ie.updated_at))::timestamp without time zone, 'yyyy-mm-dd"T"HH24:MI:SS'::text) as updated_at
from od_candidates_and_committees cc
         --left join lateral fund_summary_independent_expenditures(cc.filer_id, cc.election_year) ie ON true
         left join lateral fund_summary(cc.fund_id) amounts on cc.committee_type != 'OS'
         left join lateral fund_summary_c5(cc.fund_id) c5_amounts on cc.committee_type = 'OS'
         left join lateral fund_sum_debts_and_pledges(cc.fund_id) last_report  on cc.committee_type != 'OS'


         left join (select ie.filer_id,
                           r.election_year,
                           sum(case when ie.spt_opp = 'SPT' then portion_of_amt else 0 end) as for_amount,
                           sum(case when ie.spt_opp = 'OPP' then portion_of_amt else 0 end) as against_amount,
                           max(r.date_filed) as updated_at
                    from c6_reports r
                             join c6_identified_entity ie on r.repno = ie.repno
                    where r.superseded is null
                    group by ie.filer_id, r.election_year) ie
                   on (cc.filer_id = ie.filer_id and cc.election_year = ie.election_year::int)
                       or cc.candidacy_id::text = ie.filer_id;