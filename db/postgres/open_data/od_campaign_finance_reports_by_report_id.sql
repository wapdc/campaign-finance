--DROP FUNCTION IF EXISTS od_campaign_finance_reports_by_report_id(int);
CREATE OR REPLACE FUNCTION od_campaign_finance_reports_by_report_id(int)
    RETURNS setof od_campaign_finance_reports
    language plpgsql
AS
$$
declare
    committeeId  int;
    fundId       int;
begin
    select r.fund_id into fundId
        from report r where r.report_id = $1;
    select f.committee_id into committeeId
        from fund f where f.fund_id = fundId;
    return query
        select * from od_campaign_finance_reports r
        where r.report_number = $1 and r.committee_id = committeeId;
end
$$;