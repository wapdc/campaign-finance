--DROP VIEW IF EXISTS od_pledges CASCADE;

CREATE OR REPLACE VIEW od_pledges AS
SELECT
    t.transaction_id                                                                  AS id,
    r.report_id                                                                       AS report_number,
    'B.2'                                                                             AS origin,
    cc.committee_id                                                                   AS committee_id,
    cc.filer_id                                                                       AS filer_id,
    cc.filer_type                                                                     AS type,
    cc.filer_name                                                                     AS filer_name,
    cc.office                                                                         AS office,
    cc.legislative_district                                                           AS legislative_district,
    cc.position                                                                       AS position,
    cc.party                                                                          AS party,
    cc.ballot_number                                                                  AS ballot_number,
    cc.for_or_against                                                                 AS for_or_against,
    cc.jurisdiction                                                                   AS jurisdiction,
    cc.jurisdiction_county                                                            AS jurisdiction_county,
    cc.jurisdiction_type                                                              AS jurisdiction_type,
    r.election_year                                                                   AS election_year,
    t.amount                                                                          AS amount,
    t.transaction_date                                                                AS receipt_date,
    prim_gen_formatted(c.prim_gen)                                                    AS primary_general,
    coalesce(code_by_contributor_type(c.contributor_type), 'Other')                   AS code,
    c.name                                                                            AS contributor_name,
    c.address                                                                         AS contributor_address,
    c.city                                                                            AS contributor_city,
    c.state                                                                           AS contributor_state,
    left(c.postcode, 5)                                                               AS contributor_zip,
    c.occupation                                                                      AS contributor_occupation,
    c.employer                                                                        AS contributor_employer_name,
    c.employer_city                                                                   AS contributor_employer_city,
    c.employer_state                                                                  AS contributor_employer_state,
    pdc_report_url(r.user_data, r.report_id, false) as url
from od_candidates_and_committees cc
         join fund f on f.committee_id = cc.committee_id and left(f.election_code, 4)::int = cc.election_year
         join report r on r.fund_id = f.fund_id
         join transaction t on t.report_id = r.report_id
        left join contribution c on c.transaction_id = t.transaction_id
where amount > 0 and r.superseded_id is null
  and t.category = 'pledge'
and r.report_type in ('C3', 'C4');