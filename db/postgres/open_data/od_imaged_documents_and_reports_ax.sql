create or replace view od_imaged_documents_and_reports_ax as
select
    docid::text||'.image' as id,
    batch_number as report_number,
    d.form_type as origin,
    f.description as document_description,
    filer_id,
    batch_type as type,
    filer_name,
    office_sought as office,
    legislative_district,
    description_party as party,
    election_year,
    open_data_date_format(date_received) as receipt_date,
    open_data_date_format(batch_date) as processed_date,
    case when append = 'EFILE' then 'Electronic' else 'Other' end as filing_method,
    open_data_date_format(report_from) as report_from,
    open_data_date_format(report_to) as report_to,
    case
        when
            d.form_type not ilike 'f1%'
            then legacy_pdc_url() || '/rptimg/default.aspx?docid=' || docid::text
        else
            'https://web.pdc.wa.gov/browse/request-agency-records'
        end as url
from ax_document d
left join ax_document_form_type f on d.form_type = f.form_type
where (election_year >= date_part('year', CURRENT_DATE)::int - 17
or date_part('year', date_received)::int >= date_part('year', CURRENT_DATE)::int - 17)
and d.form_type != 'BRIEF'
order by id;