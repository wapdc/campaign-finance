DROP VIEW IF EXISTS od_surplus_balance CASCADE;
CREATE OR REPLACE VIEW od_surplus_balance
AS
select balance.committee_id                                                        as id,
       balance.report_id                                                     as report_number,
       'C4'                                                                  as origin,
       od.filer_id                                                           as filer_id,
       od.person_id                                                          as person_id,
       od.account_name                                                       as filer_name,
       open_data_date_format(balance.period_start)                           as from_date,
       open_data_date_format(balance.period_end)                             as thru_date,
       date_part('year', balance.period_end)                                 as year,
       coalesce(balance.raised, 0)                                           as raised,
       coalesce(balance.spent, 0)                                            as spent,
       coalesce(balance.balance, 0)                                          as balance,
       pdc_report_url(balance.user_data, balance.report_id, true) as url
from od_surplus_fund_account od
         join (select c.committee_id,
                      latest_report.report_id,
                      latest_report.period_start,
                      latest_report.period_end,
                      max(latest_report.user_data) as user_data,
                      max(t.amount) filter (where legacy_line_item = '8')  as raised,
                      max(t.amount) filter (where legacy_line_item = '17') as spent,
                      max(t.amount) filter (where legacy_line_item = '20') as balance
               from committee c

                        join fund f on f.committee_id = c.committee_id

                        join (select r.*, ROW_NUMBER() OVER (PARTITION by fund_id ORDER BY period_end desc) rownum
                              FROM report r
                              where date_part('year', r.period_end) >= greatest(date_part('year', CURRENT_DATE)::int - 17, 2007)
                                and r.superseded_id is null
                                and r.report_type = 'C4') latest_report
                             on latest_report.fund_id = f.fund_id and latest_report.rownum = 1

                        join transaction t on t.report_id = latest_report.report_id

                        join report_sum rs
                             on rs.transaction_id = t.transaction_id and rs.legacy_line_item in ('8', '17', '20')
               where c.pac_type = 'surplus'

               group by c.committee_id, latest_report.period_start, latest_report.period_end,
                        latest_report.report_id) balance
              on balance.committee_id = od.committee_id

