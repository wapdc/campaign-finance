--DROP VIEW IF EXISTS od_surplus_funds_reports CASCADE;
CREATE OR REPLACE VIEW od_surplus_funds_reports
AS
SELECT r.report_id                                                     as id,
       r.report_id                                                     as report_number,
       rep.report_id as amends_report,
       r.superseded_id as amended_by_report,
       r.report_type                                                   as origin,
       od.filer_id                                                     as filer_id,
       od.person_id                                                    as person_id,
       od.account_name                                                 as filer_name,
       open_data_date_format(r.period_start)                                     as from_date,
       open_data_date_format(r.period_end)                                       as thru_date,
       od.start_year                                                   as year,
       coalesce(t_raised.amount, 0)                                    as raised,
       coalesce(t_spent.amount, 0)                                     as spent,
       coalesce(t_balance.amount, 0)                                   as balance,
       od.committee_id                                                 as committee_id,
       r.metadata                                                      as metadata,
       r.version                                                       as version,
       pdc_report_url(r.user_data, r.report_id, true)   as url,
       cf_get_report_attachments(r.report_id)                 as attachments
FROM od_surplus_fund_account od
         join fund f on f.committee_id = od.committee_id
         join report r on r.fund_id = f.fund_id
         join committee c on c.committee_id = f.committee_id
         left join lateral (
    select amount
    from report_sum rs join transaction t on t.transaction_id = rs.transaction_id
    where rs.legacy_line_item = '8' and t.report_id = r.report_id) t_raised on true
         left join lateral (
    select amount
    from report_sum rs join transaction t on t.transaction_id = rs.transaction_id
    where rs.legacy_line_item = '17' and t.report_id = r.report_id) t_spent on true
         left join lateral (
    select amount
    from report_sum rs join transaction t on t.transaction_id = rs.transaction_id
    where rs.legacy_line_item = '20' and t.report_id = r.report_id) t_balance on true
         left join report rep on rep.superseded_id = r.report_id
where c.pac_type = 'surplus'
  and date_part('year', r.period_end) >= greatest(date_part('year', CURRENT_DATE)::int - 17, 2007)
  and r.report_type in ('C3', 'C4');
