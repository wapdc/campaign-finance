--DROP FUNCTION IF EXISTS od_campaign_finance_summary_by_candidacy_id(int);
CREATE OR REPLACE FUNCTION od_campaign_finance_summary_by_candidacy_id(int)
    RETURNS setof od_campaign_finance_summary
    language plpgsql
AS
$$
declare
    candidacyId int := $1;
    committeeId  int;
    fundId int;
begin
    select c.committee_id into committeeId from candidacy c where c.candidacy_id = candidacyId;

    if committeeId is not null then
        select f.fund_id into fundId from fund f
        where f.committee_id = committeeId;
    end if;

    if committeeId is null then
        -- Only candidates can have a record without a committee id
        return query
            select * from od_campaign_finance_summary s
            where s.candidacy_id = candidacyId and s.committee_id is null and s.fund_id is null;
    elseif committeeId is not null and fundId is null then
        return query
            -- The is null on fund dramatically speeds things up when there is no committee/fund.
            select * from od_campaign_finance_summary s
            where s.committee_id = committeeId and s.fund_id is null;
    else
        return query
            select * from od_campaign_finance_summary s
            where s.committee_id = committeeId and s.fund_id = fundId;
    end if;
end;
$$;