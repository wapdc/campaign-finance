DROP VIEW IF EXISTS od_surplus_funds_expenditures CASCADE;
CREATE OR REPLACE VIEW od_surplus_funds_expenditures
AS
select t.transaction_id                                                         as id,
       r.report_id                                                              as report_number,
       transaction_origin(r.form_type, t.category, t.act, t.inkind, t.itemized) as origin,
       od.filer_id                                                              as filer_id,
       od.person_id                                                             as person_id,
       r.filer_name                                                             as filer_name,
       r.election_year                                                          as election_year,
       t.amount                                                                 as amount,
       'Candidate'                                                              as type,
       case
           when t.itemized = true then 'Itemized'
           when t.itemized = false then 'Non-itemized'
           end
                                                                                as itemized_or_non_itemized,
       t.transaction_date                                                       as expenditure_date,
       t.description                                                            as description,
       ec.label                                                                 as code,
       ex.name                                                                  as recipient_name,
       ex.address                                                               as recipient_address,
       ex.city                                                                  as recipient_city,
       ex.state                                                                 as recipient_state,
       ex.postcode                                                              as recipient_zip,
       pdc_report_url(r.user_data, r.report_id, true)                         AS url,
       point_as_wkt(ga.location)                                                as recipient_location
from od_surplus_fund_account od
         join fund f on f.committee_id = od.committee_id
         join report r on r.fund_id = f.fund_id
         join transaction t on t.report_id = r.report_id
         left join expense ex on ex.transaction_id = t.transaction_id
         left join expense_category ec on ec.code = ex.category
         left join geocoded_address ga on ga.id = ex.address_hash
where r.superseded_id is null
  and t.category = 'expense'
  and (t.act = 'correction' or t.act = 'payment' or t.act = 'refund' or t.act = 'invoice')
  and date_part('year', r.period_end) >= greatest(date_part('year', CURRENT_DATE)::int - 17, 2007)
and r.report_type in ('C3', 'C4');
