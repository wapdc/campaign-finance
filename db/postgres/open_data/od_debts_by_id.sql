DROP FUNCTION IF EXISTS od_debts_by_id(int);
CREATE OR REPLACE FUNCTION od_debts_by_id(int)
    RETURNS setof od_debts
    language plpgsql
AS
$$
declare
    committeeId  int;
    fundId       int;
begin
    select t.fund_id into fundId
        from transaction t where t.transaction_id = $1;
    select f.committee_id into committeeId
        from fund f where f.fund_id = fundId;
    return query
        select * from od_debts
        where id = $1 and committee_id = committeeId;
end
$$;