/*
    This query extracts base data for surplus fund accounts.
    The existing surplus funds data on Open Data is inaccurate and most of the related
    views will be completely restructured to use this base data and join
    on whichever applicable tables: expenditures, etc.
*/

DROP VIEW IF EXISTS od_surplus_fund_account CASCADE;
CREATE OR REPLACE VIEW od_surplus_fund_account
AS
select
       c.committee_id as committee_id,
       r.registration_id as registration_id,
       c.filer_id   as filer_id,
       c.person_id  as person_id,
       c.name       as account_name,
       c.start_year as start_year,
       c.reporting_type as reporting_type,
       cc.name       as contact_name,
       cc.title      as contact_title,
       cc.address    as caddress,
       cc.city       as city,
       cc.state      as state,
       cc.postcode   as postcode,
       cc.email      as email,
       cc.phone      as phone,
       c.c1_sync
from committee c
       left join committee_contact cc on cc.contact_id = c.committee_id and cc.role = 'committee'
       left join registration r on c.registration_id = r.registration_id
where c.pac_type = 'surplus'
and c.registered_at is not null;
