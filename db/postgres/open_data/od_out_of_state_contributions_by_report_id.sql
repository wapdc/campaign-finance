DROP FUNCTION IF EXISTS od_out_of_state_contributions_by_report_id(int);
CREATE OR REPLACE FUNCTION od_out_of_state_contributions_by_report_id(int)
    RETURNS setof od_out_of_state_contributions
    language plpgsql
AS
$$
declare
    declare
    committeeId  int;
begin
    select f.committee_id into committeeId
    from report r
             JOIN fund f on r.fund_id = f.fund_id where r.report_id = $1;
    return query
        select * from od_out_of_state_contributions
        where report_id = $1 and committee_id = committeeId;
end
$$;
