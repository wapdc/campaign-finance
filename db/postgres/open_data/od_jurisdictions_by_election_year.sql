DROP VIEW IF EXISTS od_jurisdictions_by_election_year CASCADE;

CREATE OR REPLACE VIEW od_jurisdictions_by_election_year AS
select
                    c.election_year || '.' || j.jurisdiction_id || '-' || o.offcode as id,
                    c.election_year,
                    jurisdiction_class_title(j.category) as class_title,
                    jurisdiction_class_code(j.category) as class_code,
                    '00000' || jurisdiction_class_code(j.category) as class_sort,
                    jurisdiction_office_category_title(j.name, j.category, o.offcode, co.coname) as category_title,
                    jurisdiction_office_category_code(j.jurisdiction_id, j.category, o.offcode, j.county) as category_code,
                    jurisdiction_office_category_sortcode(j.name, j.category, o.offcode, j.county) as category_sort,
                    case when j.jurisdiction_id = 1000 then o.offtitle
                         else o.offtitle || ', ' || regexp_replace(j.name, '(LEG | - HOUSE| - SENATE)', '', 'g')
                        end as juris_office_title,
                    j.jurisdiction_id || '-' || o.offcode as juris_office_code,
                    case when o.offcode = '12' or o.offcode = '13' then RIGHT('000000' || regexp_replace(j.name, '\D', '', 'g'), 6)
                         else RIGHT('000000' || o.offcode, 6)
                        end AS juris_office_sort,
                    j.name as jurisdiction,
                    j.jurisdiction_id as jurisdiction_code,
                    coalesce(co.coname, '') as jurisdiction_county,
                    o.offcode as office_code
from
    jurisdiction j
        join
    (select jurisdiction_id, office_code, extract(YEAR from e.election_date) as election_year
     from candidacy c join election e on c.election_code = e.election_code
         group by (jurisdiction_id, office_code, e.election_code)) c on c.jurisdiction_id = j.jurisdiction_id
        join foffice o on c.office_code = o.offcode
        left join fcounty co on conum = j.county
