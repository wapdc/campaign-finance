--DROP VIEW IF EXISTS od_expenditures CASCADE;
CREATE OR REPLACE VIEW od_expenditures
AS
SELECT t.transaction_id                                                                   AS id,
       r.report_id                                                                        AS report_number,
       transaction_origin(r.form_type, t.category, t.act, t.inkind, t.itemized)           AS origin,
       cc.committee_id                                                                    AS committee_id,
       f.filer_id                                                                         AS filer_id,
       cc.filer_type                                                                      AS type,
       cc.filer_name                                                                      AS filer_name,
       cc.office                                                                          AS office,
       cc.legislative_district                                                            AS legislative_district,
       cc.position                                                                        AS position,
       cc.party                                                                           AS party,
       cc.ballot_number                                                                   AS ballot_number,
       cc.for_or_against                                                                  AS for_or_against,
       cc.jurisdiction                                                                    AS jurisdiction,
       cc.jurisdiction_county                                                             AS jurisdiction_county,
       cc.jurisdiction_type                                                               AS jurisdiction_type,
       left(f.election_code, 4)                                                           AS election_year,
       ex.payee                                                                           AS payee,
       ex.creditor                                                                        AS creditor,

       case
           when t.category = 'loan' then (select pmt.principle_paid * -1)
           else t.amount
           end
                                                                                          AS amount,
       case
           when t.itemized = true then 'Itemized'
           when t.itemized = false then 'Non-itemized'
           end
                                                                                          AS itemized_or_non_itemized,
       open_data_date_format(t.transaction_date)                                     as expenditure_date,
       t.description                                                                      AS description,
       case when t.inkind then 'Inkind contribution' else ec.label end AS code,
       case when t.inkind then c.name else ex.name end AS recipient_name,
       case when t.inkind then c.address else ex.address end AS recipient_address,
       case when t.inkind then c.city else ex.city end AS recipient_city,
       case when t.inkind then c.state else ex.state end AS recipient_state,
       case when t.inkind then c.postcode else ex.postcode end AS recipient_zip,
       pdc_report_url(r.user_data, r.report_id, false) as url,
       point_as_wkt(case when t.category = 'contribution' then gac.location else ga.location end) AS recipient_location
FROM od_candidates_and_committees cc
         join fund f on f.committee_id = cc.committee_id and left(f.election_code, 4)::int = cc.election_year
         join report r on r.fund_id = f.fund_id
         join transaction t on t.report_id = r.report_id
         left join expense ex on ex.transaction_id = t.transaction_id
         left join contribution c on t.transaction_id = c.transaction_id
         left join expense_category ec on ex.category = ec.code
         left join payment pmt on pmt.transaction_id = t.transaction_id
         Left join geocoded_address ga on ga.id = ex.address_hash
         Left join geocoded_address gac on gac.id = c.address_hash

where r.superseded_repno is null
  and r.superseded_id is null
  and (t.category = 'expense'
           and (
             (t.act = 'payment' and t.expense_type != 0 and t.amount != 0)
             or t.act = 'correction'
             or t.act = 'refund'
           )
    or (
        (t.category = 'contribution' and t.act = 'receipt' and t.inkind = true) or
        (t.category = 'loan' and t.act = 'payment')
        )
    )
and r.report_type in ('C3', 'C4');
