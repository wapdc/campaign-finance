-- DROP VIEW IF EXISTS od_contributions CASCADE;
CREATE OR REPLACE VIEW od_contributions AS
select
       t.transaction_id                                                                           as id,
       t.report_id                                                                                as report_number,
       transaction_origin(r.form_type, t.category, t.act, t.inkind, t.itemized)                   as origin,
       cc.committee_id                                                                            as committee_id,
       f.filer_id                                                                                 as filer_id,
       cc.filer_type                                                                              as type,
       cc.filer_name                                                                              as filer_name,
       cc.office                                                                                  as office,
       cc.legislative_district                                                                    as legislative_district,
       cc.position                                                                                as position,
       cc.party                                                                                   as party,
       cc.ballot_number                                                                           as ballot_number,
       cc.for_or_against                                                                          as for_or_against,
       cc.jurisdiction                                                                            as jurisdiction,
       cc.jurisdiction_county                                                                     as jurisdiction_county,
       cc.jurisdiction_type                                                                       as jurisdiction_type,
       r.election_year                                                                            as election_year,
       t.amount                                                                                   as amount,
       CASE
           WHEN t.inkind = true THEN
               'In-kind'
           ELSE 'Cash'
           END                                                                                    as cash_or_in_kind,
       open_data_date_format(t.transaction_date)                                                  as receipt_date,
       contribution_description(t.category, t.act, rs.legacy_line_item, rs.tx_count,
                                cor.original_amount, cor.corrected_amount, a.fair_market_value,
           a.sale_amount, coalesce(a.item_description, t.description), cont.contributor_type)                            as description,
       case
         when t.act = 'auction donation' THEN 'Item Donor'
         when t.act = 'auction buy' THEN 'Item Buyer'
       end                                                                                        as memo,
       prim_gen_formatted(cont.prim_gen)                                                           as primary_general,
       code_by_contributor_type(cont.contributor_type)                                             as code,
       case when cont.contributor_type in ('B', 'C', 'F', 'L', 'O', 'P', 'M', 'U')
           then 'Organization'
               else 'Individual' end                                                               as contributor_category,
       case
         when t.act = 'correction' then coalesce(cont.name, 'CORRECTION TO CONTRIBUTIONS')
         when t.act = 'sum' then
           case
             when rs.legacy_line_item = '1A' then 'ANONYMOUS CONTRIBUTIONS'
             when rs.legacy_line_item = '1E' then 'SMALL CONTRIBUTIONS'
           end
         when (t.category = 'expense' and t.act = 'refund' and t.amount < 0) then e.name
         when t.act = 'miscellaneous' and cont.contributor_type = 'S' then cc.filer_name
         when t.act = 'miscellaneous' then 'MISCELLANEOUS RECEIPTS'
         else coalesce(nullif(cont.name,''),'NOT REPORTED')
       end                                                                                         as contributor_name,
       case
         when t.act = 'miscellaneous' and cont.contributor_type = 'S' then cc.committee_address
         else cont.address
       end                                                                                         as contributor_address,
       case
         when t.act = 'miscellaneous' and cont.contributor_type = 'S' then cc.committee_city
         else cont.city
       end                                                                                         as contributor_city,
       case
         when t.act = 'miscellaneous' and cont.contributor_type = 'S' then cc.committee_state
         else cont.state
       end                                                                                         as contributor_state,
       case
         when t.act = 'miscellaneous' and cont.contributor_type = 'S' then left(cc.committee_zip, 5)
         else left(cont.postcode, 5)
       end                                                                                         as contributor_zip,
       cont.occupation                                                                             as contributor_occupation,
       cont.employer                                                                               as contributor_employer_name,
       cont.employer_city                                                                          as contributor_employer_city,
       cont.employer_state                                                                         as contributor_employer_state,
       pdc_report_url(
               case when user_data is null then null else '{}' end,
               r.report_id,
               false)                                                                              as url,
       point_as_wkt(ga.location)                                                                   as contributor_location
from od_candidates_and_committees cc
  join fund f on f.committee_id = cc.committee_id and left(f.election_code, 4)::int = cc.election_year
  join report r on f.fund_id = r.fund_id
  join transaction t on t.report_id = r.report_id
  left join report_sum rs on t.transaction_id = rs.transaction_id
  left join contribution cont on t.transaction_id = cont.transaction_id
  left join expense e on t.transaction_id = e.transaction_id
  left join correction cor on t.transaction_id = cor.transaction_id
  left join auction_item a on a.buy_transaction_id = t.transaction_id
  left join geocoded_address ga on cont.address_hash = ga.id
where t.amount != 0
  and (
    (t.category = 'contribution' and (t.act = 'correction' or t.amount > 0))
      or (t.category = 'expense' and t.act = 'refund' and t.amount < 0)
  )
  -- exclude the contribution for in-kind loans (changed act to be 'loan' instead of 'receipt')
  and not (t.category = 'contribution' and t.act='loan' and t.inkind = true)

  and (rs.legacy_line_item in ('1A', '1E') or rs.legacy_line_item is null)
  and r.superseded_id is null
and r.report_type in ('C3', 'C4');
