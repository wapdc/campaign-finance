DROP VIEW IF EXISTS od_imaged_documents_and_reports_registrations CASCADE;

CREATE OR REPLACE VIEW od_imaged_documents_and_reports_registrations AS

    /*
     Using .registration on the id ensures no conflicts with the actual imaged reports. All reports in AX are handled by
     the imaged_documents sync on zeus (MSSQL)
     */
select r.registration_id::text || '.registration'                                               as id,
       ''                                              as report_number,
       case
           when mr.amendment = false then
               case
                   when co.pac_type = 'surplus' then 'SURPLUS ACCT C1'
                   when co.pac_type = 'candidate' then 'C1'
                   else 'C1PC'
                   end
           else
               case
                   when co.pac_type = 'surplus' then 'SURPLUS ACCT C1 AMENDED'
                   when co.pac_type = 'candidate' then 'C1 AMENDED'
                   else 'C1PC AMENDED'
                   end
           end                                                                                  as origin,
       case
           when mr.amendment = false then
               case
                   when co.pac_type = 'surplus' then 'Form C1, surplus account registration'
                   when co.pac_type = 'candidate' then 'Form C1, candidate registration'
                   else 'Form C1PC, committee registration'
                   end
           else
               case
                   when co.pac_type = 'surplus' then 'Amended Form C1, surplus account registration'
                   when co.pac_type = 'candidate' then 'Amended form C1, candidate registration'
                   else 'Amended form C1PC, committee registration'
                   end
           end                                                                                  as document_description,
       co.filer_id                                                                              as filer_id,
       legacy_ax_committee_type(jc.jurisdiction_category_id)                                    as type,
       r.name                                                                                   as filer_name,
       o.offtitle                                                                               as office,
       ''                                                                                       as legislative_district,
       COALESCE(paco.name, pa.name)                                                             as party,
       substring(co.election_code, 1, 4)                                                        as election_year,
       open_data_date_format(r.submitted_at)                                               as receipt_date,
       open_data_date_format(r.submitted_at)                                               as processed_date,
       'Electronic'                                                                             as filing_method,
       ''                                                                                       as report_from,
       ''                                                                                       as report_to,
       concat(apollo_url(), '/public/registrations/registration?registration_id=', r.registration_id) as url
from registration r
    join committee co on r.committee_id = co.committee_id
    left join (
     select false as amendment, registration_id,
            ROW_NUMBER() OVER (PARTITION by committee_id ORDER BY submitted_at, registration_id) rownum
     FROM registration
     WHERE verified = true
    ) mr on r.registration_id = mr.registration_id and rownum = 1
    left join candidacy ca on co.committee_id = ca.committee_id
    left join foffice o on ca.office_code = o.offcode
    left join jurisdiction_category jc on o.jurisdiction_category = jc.jurisdiction_category_id
    left join party pa on ca.party_id = pa.party_id
    left join party paco on co.party = paco.party_id
where r.verified = true
  and r.source = 'campaign registration'
