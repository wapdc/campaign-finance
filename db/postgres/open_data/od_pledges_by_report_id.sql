DROP FUNCTION IF EXISTS od_pledges_by_report_id(int);
CREATE OR REPLACE FUNCTION od_pledges_by_report_id(int)
    RETURNS setof od_pledges
    language plpgsql
AS
$$
declare
    committeeId  int;
    fundId       int;
begin
    select r.fund_id into fundId
        from report r where r.report_id = $1;
    select f.committee_id into committeeId
        from fund f where f.fund_id = fundId;
    return query
        select * from od_pledges
        where report_number = $1 and committee_id = committeeId;
end
$$;