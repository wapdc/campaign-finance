/**
  This view is intended to eventually encompass all types of attachments that might be consolidated into a single
  dataset. This may be driven by the larger effort to replace AX. The initial definition only handles reporting
  modifications so that we can stop putting new reporting modification orders in AX.

  We should probably modify the dataset to have an entity_id column and start adding that field so that this dataset is
  more useful for linking information.
 */
create or replace view od_imaged_documents_and_reports_attachments as
select
    a.attachment_id::text || '.attachment'       as id,
    m.reporting_mod_id::text || '.reporting_mod' as report_number,
    'ORDER'                                      as origin,
    'Commission orders'                          as document_description,
    e.filer_id                                   as filer_id,
    'MISCELLANEOUS/ORDERS'                       as type,
    e.name                                       as filer_name,
    ''                                           as office,
    ''                                           as legislative_district,
    ''                                           as party,
    null                                         as election_year,
    open_data_date_format(m.decision_date)       as receipt_date,
    open_data_date_format(m.decision_date)       as processed_date,
    'Other'                                      as filing_method,
    open_data_date_format(m.start_date)          as report_from,
    open_data_date_format(m.end_date)            as report_to,
    a.file_service_url                           as url
from reporting_modification m
    join attachment a on m.reporting_mod_id = a.target_id and a.target_type = 'reporting_modification'
    join entity e on m.target_type = 'person' and m.target_id = e.entity_id
where access_mode = 'public';