--DROP FUNCTION IF EXISTS od_campaign_finance_summary_by_fund_id(int);
CREATE OR REPLACE FUNCTION od_campaign_finance_summary_by_fund_id(int)
    RETURNS setof od_campaign_finance_summary
    language plpgsql
AS
$$
declare
    committeeId  int;
begin
    select f.committee_id into committeeId
        from fund f where f.fund_id = $1;
    return query
        select * from od_campaign_finance_summary s where s.fund_id = $1 and s.committee_id = committeeId;
end
$$;