--DROP VIEW IF EXISTS od_debts CASCADE;

CREATE OR REPLACE VIEW od_debts as
select
    t.transaction_id                                        AS id,
    r.report_id                                             AS report_number,
    'B.3'                                                   AS origin,
    cc.committee_id                                         AS committee_id,
    f.filer_id                                              AS filer_id,
    cc.committee_type                                       AS filer_type,
    cc.filer_name                                           AS filer_name,
    cc.office                                               AS office,
    cc.legislative_district                                 AS legislative_district,
    cc.position                                             AS position,
    cc.party                                                AS party,
    cc.jurisdiction                                         AS jurisdiction,
    cc.jurisdiction_county                                  AS jurisdiction_county,
    cc.jurisdiction_type                                    AS jurisdiction_type,
    r.election_year                                         AS election_year,
    t.amount                                                AS amount,
    'DEBT'                                                  AS record_type,
    r.period_start                                          AS from_date,
    r.period_end                                            AS thru_date,
    t.transaction_date                                      AS debt_date,
    ec.label                                                AS code,
    t.description                                           AS description,
    e.name                                                  AS vendor_name,
    e.address                                               AS vendor_address,
    e.city                                                  AS vendor_city,
    e.state                                                 AS vendor_state,
    e.postcode                                              AS vendor_zip,
    pdc_report_url(r.user_data, r.report_id, false) as url
from od_candidates_and_committees cc
         join fund f on f.committee_id = cc.committee_id and left(f.election_code, 4)::int = cc.election_year
         join report r on f.fund_id = r.fund_id
         join transaction t on t.report_id = r.report_id
         left join expense e on e.transaction_id = t.transaction_id
         left join expense_category ec on e.category = ec.code
where t.category = 'expense' and t.act = 'invoice'
  and r.superseded_id is null and r.superseded_repno is null
and r.report_type in ('C3', 'C4');