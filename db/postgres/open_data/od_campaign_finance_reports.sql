--DROP VIEW IF EXISTS od_campaign_finance_reports CASCADE;
CREATE OR REPLACE VIEW od_campaign_finance_reports AS
select
r.report_id as report_number,
rep.report_id as amends_report,
r.superseded_id as amended_by_report,
r.report_type as origin,
od.committee_id as committee_id,
od.filer_id as filer_id,
od.election_year as election_year,
od.filer_type as type,
od.filer_name as filer_name,
od.office as office,
od.legislative_district as legislative_district,
od.position as position,
od.party as party,
od.ballot_number as ballot_number,
od.for_or_against as for_or_against,
od.jurisdiction as jurisdiction,
od.jurisdiction_county as jurisdiction_county,
od.jurisdiction_type as jurisdiction_type,
open_data_date_format (r.submitted_at) as receipt_date,
CASE WHEN r.filing_method = 'P' then 'Paper'
  else 'Electronic'
  end as filing_method,
open_data_date_format(r.period_start) as report_from,
open_data_date_format(r.period_end) as report_through,
pdc_report_url(r.user_data, r.report_id, false, r.report_type) as url,
CASE
    WHEN r.user_data is not null THEN apollo_url() || '/public/service/reports/'::text || r.report_id::text || '/userdata'
END as report_data,
r.metadata as metadata,
r.version as version,
cf_get_report_attachments(r.report_id) as attachments
from od_candidates_and_committees od
join fund f on od.committee_id = f.committee_id and od.election_year = left(f.election_code, 4)::int
join report r on f.fund_id = r.fund_id
left join report rep on rep.fund_id = r.fund_id and rep.superseded_id = r.report_id
where r.report_type in ('C3', 'C4', 'C5');


