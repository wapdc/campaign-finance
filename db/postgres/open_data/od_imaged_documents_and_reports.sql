CREATE OR REPLACE VIEW od_imaged_documents_and_reports AS
select r.report_id::text || '.image'                         as id,
       -- Using .image, the 110000000 series report_id's are high enough to never overlap with AX docid
       cast(r.report_id as text)                            as report_number,
       case
           when c.pac_type = 'surplus' and amd.report_id > 0 then concat('SURPLUS ACCT ', r.report_type, ' AMENDED')
           when c.pac_type = 'surplus' then concat('SURPLUS ACCT ', r.report_type)
           when amd.report_id > 0 then concat(r.report_type, ' AMENDED')
           else r.report_type
           end                                              as origin,
       case
           when coalesce(amd.report_id, cast(0 as int)) = 0 and r.report_type = 'C3'
               then 'Form '::text || r.report_type || ', campaign cash receipts'::text
           when coalesce(amd.report_id, cast(0 as int)) = 0 and r.report_type = 'C4'
               then 'Form '::text || r.report_type || ', campaign summary report'::text
           when coalesce(amd.report_id, cast(0 as int)) = 0 and r.report_type = 'LMC'
               then 'Form '::text || r.report_type || ', last minute contribution'::text
           when coalesce(amd.report_id, cast(0 as int)) = 0 and r.report_type = 'C5'
               then 'Form '::text || r.report_type || ', out-of-state committee contributions'::text
           when r.report_type = 'C3'
               then 'Amended Form '::text || r.report_type || ', campaign cash receipts'::text
           when r.report_type = 'C4'
               then 'Amended Form '::text || r.report_type || ', campaign summary report'::text
           when r.report_type = 'C5'
               then 'Amended Form '::text || r.report_type || ', out-of-state committee contributions'::text
           when r.report_type = 'LMC'
               then 'Amended Form '::text || r.report_type || ', last minute contribution'::text
           else '' end                                      as document_description,
       case when f.target_type = 'lobbyist_firm' then 'LF-' || f.target_id
           when f.target_type = 'lobbyist_client' then 'LC-' || f.target_id
           else c.filer_id end::text                             as filer_id,
          case
              when f.target_type = 'lobbyist_client' then 'LOBBYIST CLIENT/EMPLOYER'
              when f.target_type = 'lobbyit_firm' then 'LOBBYIST FIRM'
              when r.report_type = 'C5'
                  then 'OUT OF STATE COMMITTEE'
              else
                legacy_ax_committee_type(jc.jurisdiction_category_id)
             end                                               as type,
       cast(coalesce(per.name ||
                     case
                         when (trim(ca.ballot_name) <> per.name) then ' ('::text || trim(ca.ballot_name) || ')'::text
                         else ''
                         end, COALESCE(c.name, r.filer_name)) as text)              as filer_name,
       coalesce(cast(fo.offtitle as text), '')              as office,
       case
           when jc.jurisdiction_category_id = 4 then substring(j.short_name from ' [0-9]+')
           else ''
           end                                              as legislative_district,
       cast(coalesce(ca_pty.name, co_pty.name, '') as text) as party,
       r.election_year                                      as election_year,
       to_char(r.submitted_at, 'YYYY-MM-DD')                as receipt_date,
       cast(to_char(r.submitted_at, 'YYYY-MM-DD') as text)  as processed_date,
       'Electronic'::text                                   as filing_method,
       cast(to_char(r.period_start, 'YYYY-MM-DD') as text)  as report_from,
       cast(to_char(r.period_end, 'YYYY-MM-DD') as text)    as report_to,
       -- Having this case statement let's us deploy early and keep the current behavior where the URLs point directly
       -- to the images unless they are json only. It's a bit different that the Apollo behavior but the AX records are
       -- a more specific 1:1 mapping of the index to the docid.
       case
         when c.pac_type = 'surplus' then pdc_report_url(r.user_data, r.report_id, true)
         else pdc_report_url(r.user_data, r.report_id, false, r.report_type) end as url
from report r
         join fund f on r.fund_id = f.fund_id
         left join committee c on f.committee_id = c.committee_id
         left join candidacy ca on c.committee_id = ca.committee_id
         left join foffice fo on ca.office_code = fo.offcode
         left join party ca_pty on ca_pty.party_id = ca.party_id
         left join party co_pty on co_pty.party_id = c.party
         left join person per on per.person_id = ca.person_id
         left join report amd on r.report_id = amd.superseded_id
         left join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
         left join jurisdiction_category jc on j.category = jc.jurisdiction_category_id
         left join (select cr.committee_id,
                           extract(YEAR from e.election_date) as election_year,
                           true                               as proposal
                    from committee_relation cr
                             join proposal p on cr.target_id = p.proposal_id
                             join election e on p.election_code = e.election_code
                    group by cr.committee_id, extract(YEAR from e.election_date)) p
                   on c.committee_id = p.committee_id and r.election_year = cast(p.election_year as text)
-- Reports where user_data is null are reports imaged in AX
where r.user_data is not null;
