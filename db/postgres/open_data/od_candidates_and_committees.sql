/*
    This query extracts data for the OpenData Candidates and Committees dataset and replaces the following existing
    MSSQL queries:
        candidates_and_committees
        candidates_and_committees_formatted
*/

DROP VIEW IF EXISTS od_candidates_and_committees CASCADE;
CREATE OR REPLACE VIEW od_candidates_and_committees AS

-- Out-of-state committees section
-- all out-of-state committees are in the committee table with committee_type = 'OS'
-- the fund table has a committee_id and fund_id for each committee
-- the report table has a fund_id and election_year for each report
-- the registration table has a report_id as a property called "report" in the json in the admin data field that matches the report_id in the report table
-- get the registration_id for each out-of-state committee of the most recent registration for each election year based on the report election_year
with years as (select generate_series(greatest(date_part('year', CURRENT_DATE)::int - 17, 2007),
                                      date_part('year', CURRENT_DATE)::int) as election_year),
  annual_registrations as (WITH latest_registrations AS (
  SELECT c.committee_id,
               f.fund_id,
               substring(f.election_code, 1, 4)::int as election_year,
               r.filer_name,
               reg.registration_id,
               r.report_id,
               r.ax_legacy_docid,
               ROW_NUMBER()
               OVER (PARTITION BY c.committee_id, f.fund_id ORDER BY r.submitted_at DESC, r.report_id DESC) AS rn
        FROM committee c
                 JOIN fund f ON c.committee_id = f.committee_id
                 JOIN committee_contact cc
                      on cc.committee_id = c.committee_id and role = 'committee'
                 JOIN report r ON f.fund_id = r.fund_id
                 JOIN registration reg ON c.committee_id = reg.committee_id
        WHERE c.committee_type = 'OS'
     AND (r.user_data IS NOT NULL OR reg.admin_data::json->>'report' IS NOT NULL ))
  SELECT committee_id,
         fund_id,
         report_id,
         filer_name,
         election_year,
         registration_id,
         ax_legacy_docid
  FROM latest_registrations
  WHERE rn = 1
  ORDER BY committee_id,
           election_year)
select 'co-' || ar.election_year::text || '-' || c.committee_id::text as id,
       'CF-' || r.registration_id                                                                 as report_number,
       c.committee_id                                                                             as committee_id,
       pac_type                                                                                   as pac_type,
       null::integer                                                                              as candidacy_id,
       person_id                                                                                  as person_id,
       ar.fund_id                                                                                 as fund_id,
       'OS'                                                                                       as committee_type,
       'C5'                                                                                       as origin,
       c.filer_id                                                                                 as filer_id,
       'Political Committee'                                                                      as filer_type,
       null                                                                                       as registered,
       null                                                                                       as declared,
       null                                                                                       as withdrew,
       null                                                                                       as discontinued,
       open_data_date_format(c.registered_at)                                                     as receipt_date,
       ar.election_year                                                                           as election_year,
       null                                                                                       as candidate_committee_status,
       c.name                                                                                     as filer_name,
       c.acronym                                                                                  as committee_acronym,
       cc.address                                                                                 as committee_address,
       cc.city                                                                                    as committee_city,
       null                                                                                       as committee_county,
       cc.state                                                                                   as committee_state,
       cc.postcode                                                                                as committee_zip,
       cc.email                                                                                   as committee_email,
       null                                                                                       as candidate_email,
       cc.phone                                                                                   as candidate_committee_phone,
       null                                                                                       as office,
       null                                                                                       as office_code,
       null                                                                                       as jurisdiction,
       null                                                                                       as legislative_district,
       null                                                                                       as jurisdiction_code,
       null                                                                                       as jurisdiction_county,
       null                                                                                       as jurisdiction_type,
       null::integer                                                                              as jurisdiction_voters,
       null                                                                                       as jurisdiction_reporting_code,
       null                                                                                       as jurisdiction_reporting_requirement,
       null                                                                                       as position,
       null::integer                                                                              as position_number,
       null                                                                                       as party_code,
       null                                                                                       as party,
       null                                                                                       as election_date,
       null                                                                                       as reporting_option,
       null::boolean                                                                              as active_candidate,
       null::boolean                                                                              as on_primary_election_ballot,
       null                                                                                       as on_general_election_ballot,
       null                                                                                       as primary_election_status,
       null                                                                                       as general_election_status,
       null                                                                                       as election_status,
       'Out-of-state political committee'                                                         as committee_category,
       'Out-of-State'                                                                             as political_committee_type,
       null                                                                                       as party_committee,
       null                                                                                       as bonafide_type,
       null                                                                                       as exempt_nonexempt,
       null                                                                                       as party_account_type,
       null                                                                                       as ballot_committee,
       null                                                                                       as ballot_proposal_count,
       null                                                                                       as initiative_type,
       null                                                                                       as ballot_number,
       null                                                                                       as ballot_title,
       null                                                                                       as for_or_against,
       null                                                                                       as ballot_proposal_detail,
       continuing                                                                                 as continuing,
       tc.name                                                                                    as treasurer_name,
       tc.address                                                                                 as treasurer_address,
       tc.city                                                                                    as treasurer_city,
       tc.state                                                                                   as treasurer_state,
       tc.postcode                                                                                as treasurer_zip,
       tc.phone                                                                                   as treasurer_phone,
       case WHEN r.user_data is not null then
            concat(apollo_url(), '/c5/#/public/report?report_id=', ar.report_id)
         WHEN ar.ax_legacy_docid is not null then
            concat(legacy_pdc_url(), '/rptimg/default.aspx?docid=', ar.ax_legacy_docid)
       else
            regexp_replace('https://www.pdc.wa.gov/political-disclosure-reporting-data/browse-search-data/reports?filer_name='
            || replace(replace(replace(ar.filer_name,' ','%20'), '(','%28'), ')', '%29') || '&origin=C5%7CC5+AMENDED', '\s+', '', 'g')            end as url,
       null::point                                                                                as cand_comm_location,
       c.updated_at                                                                               as updated_at

from annual_registrations ar
         join registration r on ar.registration_id = r.registration_id
         join committee c on ar.committee_id = c.committee_id
         join committee_contact cc on cc.committee_id = c.committee_id and cc.role = 'committee'
         left join
     (select *, ROW_NUMBER() OVER (PARTITION by committee_id ORDER BY ministerial desc, contact_id) rownum
      FROM committee_contact
      WHERE treasurer = true) tc
     on tc.committee_id = c.committee_id and tc.rownum = 1

union all



-- In-state committees section
select 'co-' || coalesce(y.election_year, com.start_year)::text || '-' || com.committee_id as id,
       'CF-' || coalesce(reg.registration_id, vreg.registration_id)               as report_number,
       com.committee_id                                                           as committee_id,
       com.pac_type                                                               as pac_type,
       null::integer                                                              as candidacy_id,
       com.person_id                                                              as person_id,
       f.fund_id                                                                  as fund_id,
       'CO'                                                                       as committee_type,
       'C1PC'                                                                     as origin,
       com.filer_id                                                               as filer_id,
       'Political Committee'                                                      as filer_type,
       null                                                                       as registered,
       null                                                                       as declared,
       null                                                                       as withdrew,
       null                                                                       as discontinued,
       -- receipt_date is poorly named. It is intended to be first registered date, at least in how the data is used on
       -- the website. For a continuing committee, this should still be the date they very first registered in any year.
       open_data_date_format(com.registered_at)                                   as receipt_date,
       coalesce(y.election_year, com.start_year)::int                             as election_year,
       null                                                                       as candidate_committee_status,
       case
           when com.acronym is not null then concat(com.name, ' (', com.acronym, ')')
           else com.name
           end                                                                    as filer_name,
       com.acronym                                                                as committee_acronym,
       cc.address                                                                 as committee_address,
       cc.city                                                                    as committee_city,
       pi.jurisdiction_county                                                     as committee_county,
       cc.state                                                                   as committee_state,
       cc.postcode                                                                as committee_zip,
       cc.email                                                                   as committee_email,
       null                                                                       as candidate_email,
       cc.phone                                                                   as candidate_committee_phone,
       null                                                                       as office,
       null                                                                       as office_code,
       pi.jurisdiction                                                            as jurisdiction,
       legislative_district_by_jurisdiction(pi.jurisdiction)                      as legislative_district,
       pi.jurisdiction_code                                                       as jurisdiction_code,
       pi.jurisdiction_county                                                     as jurisdiction_county,
       pi.jurisdiction_type                                                       as jurisdiction_type,
       null::integer                                                              as jurisdiction_voters,
       null                                                                       as jurisdiction_reporting_code,
       null                                                                       as jurisdiction_reporting_requirement,
       null                                                                       as position,
       null::integer                                                              as position_number,
       null                                                                       as party_code,
       pty.name                                                                   as party,
       open_data_date_format(el.election_date)                                    as election_date,
       case
           when coalesce(com.continuing, false) <> true then com.reporting_type
           else
               case
                   when extract(YEAR from vreg.submitted_at) = vreg.election_year then vreg.reporting_type
                   else 'full'
               end
       end                                                                        as reporting_option,
       null                                                                       as active_candidate,
       null::boolean                                                              as on_primary_election_ballot,
       null::boolean                                                              as on_general_election_ballot,
       null                                                                       as primary_election_status,
       null                                                                       as general_election_status,
       null                                                                       as election_status,
       com.category                                                               as committee_category,
       political_committee_type(com.category, com.pac_type, pty.code, com.continuing)
                                                                                  as political_committee_type,
       party_committee(com.pac_type)                                              as party_committee,
       bonafide_type(bonafide_type)                                               as bonafide_type,
       CASE
           WHEN com.exempt = true then 'E'
           WHEN com.pac_type = 'bonafide' then 'N'
           end                                                                    as exempt_nonexempt,
       party_exempt(com.exempt)                                                   as party_account_type,
       case
           when pi.proposals is not null or com.category ilike '%ballot%'
               then 'X'
           end                                                                    as ballot_committee,
       pi.proposal_count                                                          as ballot_proposal_count,
       pi.proposal_type                                                           as initiative_type,
       pi.ballot_numbers                                                          as ballot_number,
       pi.title                                                                   as ballot_title,
       pi.stance                                                                  as for_or_against,
       pi.proposals::text                                                         as ballot_proposal_detail,
       coalesce(com.continuing, false)                                            as continuing,
       treas_contact.name                                                         as treasurer_name,
       treas_contact.address                                                      as treasurer_address,
       treas_contact.city                                                         as treasurer_city,
       treas_contact.state                                                        as treasurer_state,
       treas_contact.postcode                                                     as treasurer_zip,
       treas_contact.phone                                                        as treasurer_phone,
       case
         -- Prefer registrations to display in the following order
         --   Registration having legacy_doc_id in the exact year (both paper and e-filed) - link to image by docid
         --   Registration having user_data as json for exact year - link to html version.
         --   Registration having user_data as xml for exact year - link to image by filer_id and election year.
         --   Registration having legacy_doc_id in a different year (both paper and e-filed) - link to image by docid
         --   Registration (having user_data) for a different year - link to html version.
         --   Else, link to a search by filer_id and election_year
           when reg.legacy_doc_id is not null then concat(legacy_pdc_url(), '/rptimg/default.aspx?docid=', reg.legacy_doc_id)
           when reg.user_data like '{%' then concat(apollo_url(), '/public/registrations/registration?registration_id=',reg.registration_id)
           when vreg.user_data like '{%' then concat(apollo_url(), '/public/registrations/registration?registration_id=',vreg.registration_id)
           when reg.user_data like '<%' then concat(legacy_pdc_url(), '/rptimg/default.aspx?filerid_election_year=',
                                                    replace(com.filer_id, ' ', '%20'), '%3A%7C%3A', coalesce(y.election_year, com.start_year)::text)
           when vreg.legacy_doc_id is not null then concat(legacy_pdc_url(), '/rptimg/default.aspx?docid=', vreg.legacy_doc_id)
           else concat(legacy_pdc_url(), '/rptimg/default.aspx?filerid_election_year=',
           replace(com.filer_id, ' ', '%20'), '%3A%7C%3A', coalesce(y.election_year, com.start_year)::text)
       end                                                                    as url,
       null::point                                                                as cand_comm_location,
       com.updated_at                                                             as updated_at
from committee com
  left join years y
    on y.election_year between com.start_year and coalesce(com.end_year, date_part('year', CURRENT_DATE))
  left join lateral proposal_info(com.committee_id, coalesce(y.election_year, com.start_year)::text) pi on true
  left join committee_contact cc on cc.committee_id = com.committee_id and role = 'committee'
  left join party pty on pty.party_id = com.party
  left join election el on el.election_code = com.election_code
  left join
     (
         select *, ROW_NUMBER() OVER (PARTITION by committee_id ORDER BY ministerial desc, contact_id) rownum
         FROM committee_contact
         WHERE treasurer = true
     ) treas_contact
     on treas_contact.committee_id = com.committee_id and treas_contact.rownum = 1


  -- Registration directly from committee only for single year, otherwise, we want the latest registration that was
  -- filed on or before the election year we are producing.
  left join registration reg on com.registration_id = reg.registration_id and coalesce(com.continuing, false) = false
  -- Prefer the latest reg submitted in the current year or any earlier year (by date and docid if present). If
  -- there are no registrations before, prefer the oldest registration (closest to the current year) for example,
  -- given year 2010 and registrations 2008-01-01, 2009-01-01, 2009-05-01, 2011..., prefer 2009-05-01.
  -- given year 2010 and registrations 2011-03-01, 2011-08-01, 2012..., prefer 2011-03-01, closest to 2010.
  left join (
    select r.*, y.election_year, row_number() over (
      partition by r.committee_id, y.election_year
      order by
        case
          when extract(year from r.submitted_at) <= y.election_year
            then extract(epoch from r.submitted_at)
          else extract(epoch from r.submitted_at) * -1
        end desc, --All the higher years are sorted lower and in reverse
        r.legacy_doc_id desc, -- Possible to have multiples with same date and time. This is just so it's not random.
        r.registration_id desc
      ) as rn
    from years y
      cross join registration r
  ) vreg
    on vreg.committee_id = com.committee_id
      and vreg.election_year = y.election_year
      and vreg.rn = 1
  left join fund f
    on com.committee_id = f.committee_id
      and left(f.election_code, 4)::int = coalesce(y.election_year, left(com.election_code, 4)::int)
where com.filer_id is not null
  and com.registered_at is not null
  and com.committee_type = 'CO'



union all

-- Candidates section
select 'ca-' || left(cand.election_code, 4) || '-' || cand.candidacy_id as id,
       'CF-' || reg.registration_id                                     as report_number,
       com.committee_id                                                 as committee_id,
       com.pac_type                                                     as pac_type,
       cand.candidacy_id                                                as candidacy_id,
       p.person_id                                                      as person_id,
       f.fund_id                                                        as fund_id,
       'CA'                                                             as committee_type,
       'C1'                                                             as origin,
       cand.filer_id                                                    as filer_id,
       'Candidate'                                                      as filer_type,
       open_data_date_format(com.registered_at)                         as registered,
       open_data_date_format(cand.declared_date)                        as declared,
       case when exit_reason = 'withdrew'
           then open_data_date_format(exit_date) end                    as withdrew,
       case when exit_reason = 'discontinued'
           then open_data_date_format(exit_date) end                    as discontinued,
       -- receipt_date is poorly named. It is intended to be first registered date, at least in how the data is used on
       -- the website
       open_data_date_format(com.registered_at)                         as receipt_date,
       left(cand.election_code, 4)::int                                 as election_year,
       CASE
           WHEN cand.exit_reason = 'withdrew'
               THEN 'Candidate withdrew'
           WHEN cand.exit_reason = 'deceased'
               THEN 'Candidate deceased'
           WHEN cand.exit_reason = 'discontinued'
               THEN 'Candidate discontinued campaign'
           WHEN cand.declared_date is not null
             THEN 'Candidate declared'
           WHEN cand.committee_id is not null
             THEN 'Candidate registered'
           END                                                          as candidate_committee_status,
       case
         when cand.ballot_name <> p.name
           then concat(p.name, ' (', cand.ballot_name, ')')
         else p.name
       end                                                              as filer_name,
       com.acronym                                                      as committee_acronym,
       cc.address                                                       as committee_address,
       cc.city                                                          as committee_city,
       fc.coname                                                        as committee_county,
       cc.state                                                         as committee_state,
       cc.postcode                                                      as committee_zip,
       cc.email                                                         as committee_email,
       cand.declared_email                                              as candidate_email,
       cc.phone                                                         as candidate_committee_phone,
       fo.offtitle                                                      as office,
       cand.office_code                                                 as office_code,
       j.name                                                           as jurisdiction,
       legislative_district_by_jurisdiction(j.name)                     as legislative_district,
       cand.jurisdiction_id                                             as jurisdiction_code,
       fc.coname                                                        as jurisdiction_county,
       jurisdiction_type(j.category)                                    as jurisdiction_type,
       jurisdiction_voter_count(
               cand.jurisdiction_id,
               left(cand.election_code, 4)::int)                        as jurisdiction_voters,
       jurisdiction_reporting_status(cand.jurisdiction_id
                                    , left(cand.election_code, 4)::int) as jurisdiction_reporting_code,
       case jurisdiction_reporting_status(cand.jurisdiction_id, left(cand.election_code, 4)::int)
           when 'N' then 'No reporting required'
           when 'F' then 'Financial affairs only'
           else 'Registration and financial affairs'
           end                                                          as jurisdiction_reporting_requirement,
       pos.title                                                        as position,
       pos.position_number                                              as position_number,
       fparty_type(cand.party_id)                                       as party_code,
       pty.name                                                         as party,
       open_data_date_format(el.election_date)                          as election_date,
       com.reporting_type                                               as reporting_option,
       -- For active_candidate, in_primary and in_general are null, indeterminate before we get ballot information from
       --   SOS and the candidate is considered "active" up to that point. Basically, only false on both make them
       --   inactive.
       case
           when (cand.in_primary is distinct from false or cand.in_general is distinct from false) and
                cand.exit_date is null
               then true
           else false
           end                                                          as active_candidate,
       cand.in_primary                                                  as on_primary_election_ballot,
       cand.in_general                                                  as on_general_election_ballot,
       primary_election_status(cand.primary_result)                     as primary_election_status,
       general_election_status(cand.general_result)                     as general_election_status,
       cand.incumbency                                                  as election_status,
       com.category                                                     as committee_category,
       'Candidate'                                                      as political_committee_type,
       null                                                             as party_committee,
       null                                                             as bonafide_type,
       null                                                             as exempt_nonexempt,
       null                                                             as party_account_type,
       null                                                             as ballot_committee,
       null                                                             as ballot_proposal_count,
       null                                                             as initiative_type,
       null                                                             as ballot_number,
       null                                                             as ballot_title,
       null                                                             as for_or_against,
       null                                                             as ballot_proposal_detail,
       null                                                             as continuing,
       treas_contact.name                                               as treasurer_last_name,
       treas_contact.address                                            as treasurer_address,
       treas_contact.city                                               as treasurer_city,
       treas_contact.state                                              as treasurer_state,
       treas_contact.postcode                                           as treasurer_zip,
       treas_contact.phone                                              as treasurer_phone,
       case
           -- Prefer registrations to display in the following order
           --   Registration having legacy_doc_id (both paper and e-filed) - link to image by docid
           --   Registration having user_data as json  - link to html version.
           --   Else, link to a search by filer_id and election_year
           when reg.legacy_doc_id is not null then concat(legacy_pdc_url(), '/rptimg/default.aspx?docid=', reg.legacy_doc_id)
           when reg.user_data like '{%' then concat(apollo_url(), '/public/registrations/registration?registration_id=',reg.registration_id)
           else concat(legacy_pdc_url(), '/rptimg/default.aspx?filerid_election_year=',
                       replace(cand.filer_id, ' ', '%20'), '%3A%7C%3A', left(cand.election_code, 4))
           end                                                                    as url,
       null::point                                                      as cand_comm_location,
       (select greatest(cand.updated_at,com.updated_at))                as updated_at
from candidacy cand
         left join committee com on com.committee_id = cand.committee_id
         left join person p on cand.person_id = p.person_id
         left join registration reg on com.registration_id = reg.registration_id
         left join committee_contact cc on cc.committee_id = com.committee_id and cc.role = 'committee'
         left join jurisdiction j on j.jurisdiction_id = cand.jurisdiction_id
         left join foffice fo on fo.offcode = cand.office_code
         left join fcounty fc on fc.conum = j.county
         left join position pos on pos.position_id = cand.position_id
         left join party pty on pty.party_id = cand.party_id
         left join election el on el.election_code = com.election_code
         left join jurisdiction_category jc on jc.jurisdiction_category_id = j.category
         left join fund f on com.committee_id = f.committee_id and com.election_code = f.election_code
         left join (select *, ROW_NUMBER() OVER (PARTITION by committee_id ORDER BY ministerial desc, contact_id) rownum
                    FROM committee_contact
                    WHERE treasurer = true) treas_contact
                   on treas_contact.committee_id = com.committee_id and treas_contact.rownum = 1
where left(cand.election_code, 4)::int >= greatest(date_part('year', CURRENT_DATE)::int - 17, 2007)
  and cand.filer_id is not null AND (cand.exit_reason IS DISTINCT FROM 'withdrew' OR cand.committee_id is not null OR cand.fa_statement_id is not null)



