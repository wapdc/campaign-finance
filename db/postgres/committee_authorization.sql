CREATE TABLE committee_authorization (
  committee_id INTEGER NOT NULL,
  realm VARCHAR(255) NOT NULL,
  role VARCHAR(255) NOT NULL,
  uid VARCHAR(255) NOT NULL,
  approved BOOLEAN NOT NULL DEFAULT FALSE,
  granted_date DATE,
  expiration_date DATE,
  PRIMARY KEY (committee_id, realm, uid),
  FOREIGN KEY (committee_id) references committee(committee_id) ON DELETE CASCADE
);
CREATE INDEX idx_committee_auth_uid ON committee_authorization(uid);