select 'UPDATE committee SET start_year = ' || start_year || ' WHERE committee_id = ' || committee_id || ';'from (
    select extract(year from min(r.period_start)) as start_year, c.committee_id from committee c
       join fund f on f.committee_id = c.committee_id
       join report r on f.fund_id = r.fund_id
    where c.pac_type = 'surplus'
    group by c.committee_id) v