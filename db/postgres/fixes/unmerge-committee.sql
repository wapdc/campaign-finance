 do language plpgsql $$
/**
  This script is used to unmerge a committee and performs the following actions:

  - Creates a new committee from the old committee provide.
  - Moves a list of registration that you provide from the old current registration from the old committee to the new committee to the new committee and unverifies it.
  - Resets the current registration on the current committee to the remaining one that has the highest ID.
  - Resets the jurisdiction_id, office_code and position_id to the value from the old registration, assuming it was a json one.

  After running this script, registrations must be verified for the new committee and the old committee.
  You should then check the committee authorization records on the old committee to make sure they are accurate.

  The script was created for the case where a person accidentally was allowed to change the jurisdiction and office
  for the new committee.
 */
   DECLARE
     v_new_committee_id integer;
     v_old_committee_id integer;
     v_new_current_registration integer;
     v_old_current_registration integer;
     v_json JSON;
 BEGIN
   v_old_committee_id := :committee_id;

   -- Create a new committee from the old one
   INSERT INTO committee (name, updated_at, committee_type, pac_type, election_code, start_year, end_year, bonafide_type, exempt)
       select name, now(), committee_type, pac_type, election_code, start_year, end_year, bonafide_type, exempt
         from committee c2 where c2.committee_id=v_old_committee_id returning committee_id into v_new_committee_id;

   -- Log creation of new committees
   INSERT INTO committee_log (committee_id, action, message, user_name, updated_at)
       values (v_new_committee_id, 'Manual Unmerge', 'Committee created by IT unmerge', 'ITTech@pdc.wa.gov', now());

   -- Move the registrations from the old committee to the new committee
   UPDATE registration set committee_id=v_new_committee_id where committee_id=v_old_committee_id
     AND registration_id in (:registration_ids);

   -- Determine current registration on new committee and un-verify it.
   SELECT registration_id
     INTO v_new_current_registration
   FROM registration
   WHERE committee_id = v_new_committee_id
   ORDER BY registration_id DESC
   LIMIT 1;
   update registration set verified = false where registration_id = v_new_current_registration;
   update committee set registration_id = v_new_current_registration where committee_id = v_new_committee_id;

   -- Determine the current registration on the old committee and un-verify it.
   SELECT registration_id, case when user_data ilike '{%' then user_data::json end
   INTO v_old_current_registration, v_json
   FROM registration
   WHERE committee_id = v_old_committee_id
   ORDER BY registration_id DESC
   LIMIT 1;
   update registration set verified = false where registration_id = v_old_current_registration;
   update committee set registration_id = v_old_current_registration where committee_id = v_old_committee_id;

   if v_json is not null then
       update candidacy set office_code = cast(v_json->'committee'->'candidacy'->>'offcode' as text),
                            jurisdiction_id = cast(v_json->'committee'->'candidacy'->>'jurisdiction_id' as integer),
                            position_id = cast(v_json->'committee'->'candidacy'->>'position_id' as integer)
       where committee_id = v_old_committee_id;
   end if;

   -- Log the change to the old committee
   INSERT INTO committee_log (committee_id, transaction_type, action, message, user_name, updated_at)
   values (v_new_committee_id, 'registration', 'Unmerge Committee', 'Un-verify due to unmerge', 'ITTech@pdc.wa.gov', now());
   end
 $$

