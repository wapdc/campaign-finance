-- if phone is empty, use the registration->user_data->committee->contact->phone
update committee_contact cc
set phone=subquery.phone
from (select user_data::json->'committee'->'contact'->>'phone' as phone, committee_id from registration  where source = 'campaign registration') as subquery
where (cc.phone is null or cc.phone = '') and subquery.committee_id = cc.committee_id and role = 'candidacy';

-- if name is empty, use the candidacy ballot_name
update committee_contact cc
set name=subquery.ballot_name
from (select ballot_name, committee_id from candidacy) as subquery
where (cc.name is null or cc.name = '') and subquery.committee_id = cc.committee_id and role = 'candidacy';

-- if name is still empty, use the entity name
update committee_contact cc
set name=subquery.name
from (select e.name, c.committee_id from entity e join committee c on e.entity_id = c.person_id) as subquery
where (cc.name is null or cc.name = '') and subquery.committee_id = cc.committee_id and role = 'candidacy';
