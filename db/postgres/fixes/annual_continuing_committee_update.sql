
/**
  The original intent of this script was to annually update continuing committees from mini to full. It was modified to
  do an update on all committees so that we publish a new open data summary record for the new year.

  The script needs to:
  -- Exclude committees that are not pacs
  -- Exclude committees that have submitted a registration in the current calendar year because if someone files a new
     registration as mini on Jan 2 and we run the script on Jan 4, we don't want to set them back to full. It will
     already have been updated in open data.
  -- Only change verified committees.
  -- Handle older committees that don't have a registration.
 */

with committees_to_update as (
    select
        c.committee_id
    from committee c left join registration r on c.registration_id = r.registration_id
    where continuing and end_year is null and pac_type in ('pac', 'bonafide', 'caucus')
      and
        (
                (
                    -- If they have a registration, only verified and not submitted this year
                            extract('Year' from r.submitted_at) < extract('Year' from current_date)
                        and r.verified = true
                    )
                or (
                    -- Include all the old ones that don't have a registration. Needed because we still want the no-op update.
                    r.registration_id is null
                    )
            )
)
update committee set reporting_type = 'full'
from committees_to_update
where committee.committee_id = committees_to_update.committee_id;