-- If we don't have a postc1 date then this was a placeholder record.
delete from committee where committee_id in (select c.committee_id
                                             from committee c
                                                      join c1 on c1.election_year=c.election_code and c1.filer_id=c.filer_id
                                                      left join candidacy ca on ca.committee_id=c.committee_id
                                             where c.pac_type = 'candidate'
                                               and c.registration_id is null
                                               and c.registered_at is null
                                               and c1.postc1 is null
                                             order by c.election_code);
-- Clean up orphaned committee records.
update candidacy set committee_id = null where candidacy_id in (select ca.candidacy_id from candidacy ca left join committee c on ca.committee_id = c.committee_id
                                                                where c.committee_id is null);