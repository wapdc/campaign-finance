/**
  This query was developed to move reports all reports that were filed in a committee registered for the wrong year
  in a candidate committee. A registration was created in error for 2019 when it should have been 2020. This query should
  move all reports filed under the 2019 committee to the 2020 one, along with any registrations.
 */
do $$
    declare
    v_src_committee_id integer;
    v_dest_committee_id integer;
    v_year integer;
    v_continuing boolean;
    v_src_fund_id INTEGER;
    v_dest_fund_id INTEGER;
begin
   select c.committee_id, f.fund_id into v_src_committee_id, v_src_fund_id from committee c join fund f on f.committee_id=c.committee_id where c.committee_id = :src_committee_id;

   select c.committee_id, c.continuing, c.start_year, f.fund_id into v_dest_committee_id, v_continuing, v_year, v_dest_fund_id from committee c join fund f on f.committee_id=c.committee_id where c.committee_id = :target_committee_id;

   if v_src_committee_id is null or v_dest_committee_id is null then
       raise 'Invalid committee!';
   end if;

   if v_continuing then
       raise 'Cannot merge onto continuing committee';
   end if;

   -- Move reports;
   update transaction set fund_id = v_dest_fund_id where fund_id=v_src_fund_id;
   update report set fund_id = v_dest_fund_id, election_year=v_year where fund_id=v_src_fund_id;

   -- Move registrations
   update registration set committee_id = v_dest_committee_id where committee_id=v_src_committee_id;
   -- Insert missing auth records
   update committee_authorization set committee_id=v_dest_committee_id where committee_id = v_src_committee_id
       and (realm, uid) not in (select realm, uid from committee_authorization where committee_id=v_dest_committee_id);
   update campaign_finance_submission set committee_id=v_dest_committee_id where committee_id=v_src_committee_id;
   -- Move api keys.
   update committee_api_key set committee_id=v_dest_committee_id where committee_id = v_src_committee_id;

   -- Remove committee/fund
   delete from fund where fund_id=v_src_fund_id;
   delete from committee where committee_id = v_src_committee_id;
end
$$ language plpgsql;