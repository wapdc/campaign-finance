-- Identify candidacy records missing email address update with committee_contact email.
UPDATE candidacy
SET
    declared_email = v.email
FROM (
    select  candidacy_id, coalesce(cc2.email, cc.email) as email
    from committee_contact cc
             join candidacy ca on cc.committee_id = ca.committee_id
             left join committee_contact cc2 on ca.committee_id = cc2.committee_id
        and cc2.role = 'candidacy'
    where (ca.declared_email is null or ca.declared_email = '')
      and (cc.email is not null or cc.email <> '')
      and cc.role = 'committee'
     ) v
WHERE candidacy.candidacy_id = v.candidacy_id