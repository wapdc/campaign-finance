do $$
    /*
      This script was created for the case where a filer amended a registration and changed their candidacy. This will
      split the committee and preserve the original candidacy while creating a new committee and candidacy for what should
      have been.

      What you need to know: The committee_id in question that needs to split, the registration_id of the oldest registration
      that should be moved to the new committee, and the fund year of the the funds to be moved. You can specify null, if you do
      not want any funds moved.

      After the script has run, you need to go to the registration verification screen on Apollo and verify the original
      committee first, then the second committee. The order of operations is critical as you do not want to overwrite the
      original candidacy record.

      After the new committee is created and assigned a filer_id put it on the funds are transferred to the new committee, if any.
     */
    declare
        --The original committee that needs to be reverted to its prior state
        v_original_committee int := :original_committee_id;
        --The registration where the new committee needs to be applied to, i.e. the amendment that caused the need for the split
        v_starting_registration_id int := :beginning_registration_id;
        --The fund related to the new committee that needs to be reassigned
        v_starting_fund_year text := :starting_fund_year;
        --The latest registration that applies to the original committee
        v_original_committee_registration int;
        v_new_committee_id int;

    begin
        --create new committee based on the latest information of the current committee
        insert into committee(name, updated_at, committee_type, pac_type, limits_type, sponsor, registered_at, election_code, start_year, end_year, bonafide_type, exempt, jurisdiction_id, reporting_type, bank_name, bank_branch, bank_city, pc_stance_candidate, pc_stance_party, pc_donation_ack, county, url, acronym, memo, registration_id, continuing, party, c1_sync, autoverify, mini_full_permission, category, legacy_diss1, legacy_jurisdiction)
        select name, now(), committee_type, pac_type, limits_type, sponsor, registered_at, election_code, start_year, end_year, bonafide_type, exempt, jurisdiction_id, reporting_type, bank_name, bank_branch, bank_city, pc_stance_candidate, pc_stance_party, pc_donation_ack, county, url, acronym, memo, registration_id, continuing, party, c1_sync, false, mini_full_permission, category, legacy_diss1, legacy_jurisdiction
        from committee where committee_id = v_original_committee returning committee_id into v_new_committee_id;

        --pass all the same authorizations to the new committee
        insert into committee_authorization(committee_id, realm, role, uid, approved, granted_date, expiration_date)
        select v_new_committee_id, realm, role, uid, approved, granted_date, expiration_date from committee_authorization where committee_id = v_original_committee;

        --update all the registrations that pertain to the new committee, i.e. the amendment that may have created the need for a new committee
        update registration
        set committee_id = v_new_committee_id
        where committee_id = v_original_committee and registration_id >= v_starting_registration_id;

        --after the separation, the new latest registration for the original committee
        select max(r.registration_id) into v_original_committee_registration from registration r where r.committee_id = v_original_committee;
        update committee
        set registration_id = v_original_committee_registration
        where committee_id = v_original_committee;

        --unverify both registrations so that the staff can then verify the information is correct
        update registration
        set verified = false
        where registration_id = v_original_committee_registration;

        update registration
        set verified = false
        where registration_id = (select max(r.registration_id) from registration r where r.committee_id = v_new_committee_id);

        --transfer all the existing funds to the new committee within a specified range
        update fund
        set committee_id = v_new_committee_id
        where committee_id = v_original_committee and election_code >= v_starting_fund_year;

        --transfer all the submissions to the new committee that relate to the funds
        update campaign_finance_submission
        set committee_id = v_new_committee_id
        where fund_id in (select f.fund_id from fund f where f.committee_id = v_new_committee_id);

        --log the actions for both committees
        insert into committee_log(committee_id, transaction_type, action, message, user_name)
        values(v_new_committee_id, 'committee', 'IT committee split', 'new committee from ' || v_original_committee, 'IT staff');

        insert into committee_log(committee_id, transaction_type, action, message, user_name)
        values(v_original_committee, 'committee', 'IT committee split', 'moved registrations and unverify', 'IT staff');

        --force update to opendata for changes on the website.
        update report
        set report_type = report_type
        where fund_id in (select f.fund_id from fund f where f.committee_id = v_new_committee_id);
end
$$ language plpgsql;
