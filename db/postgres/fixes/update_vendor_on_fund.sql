update fund
set vendor = u.vendor
from (select fund_id,
             vendor
      from (select f.fund_id,
                   row_number() over (partition by f.fund_id order by r.submitted_at desc) as r,
                   case
                       when o.version = '100' OR o.version = '10' then
                           'Unknown external vendor'
                       else
                           'PDC'
                       end
                   as vendor
            from orca_xrfinput o
                     join report r on r.report_id = o.repno
                     join fund f on r.fund_id = f.fund_id
            where f.vendor is null
            order by fund_id) v
      where r = 1) u
where u.fund_id = fund.fund_id;
