UPDATE candidacy SET committee_id = v.committee_id FROM
  (select CA.candidacy_id, co.committee_id
  from candidacy ca
         inner join committee co
                    on co.filer_id = ca.filer_id
                      and ca.committee_id is null
                      and co.election_code = ca.election_code
  where co.pac_type = 'candidate') v
WHERE candidacy.candidacy_id = v.candidacy_id

