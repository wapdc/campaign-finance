-- There are some duplicate candidacy contact records (7) so lets get rid of them.
DELETE FROM committee_contact WHERE contact_id in (
  select max(contact_id) FROM committee_contact WHERE role='candidacy' GROUP BY committee_id HAVING count(1) >1
);

-- First update all the data so that the source record data is on the target.
UPDATE committee_contact
SET
  address = v.address,
  city = v.city,
  state = v.state,
  postcode = v.postcode,
  email = v.email,
  phone = v.phone
FROM (select
  cct.contact_id,
  COALESCE(cct.address, ccs.address) AS address,
  CASE WHEN cct.address IS NOT NULL THEN cct.city ELSE ccs.city END city,
  CASE WHEN cct.address IS NOT NULL THEN cct.state ELSE ccs.state END state,
  CASE WHEN cct.address IS NOT NULL THEN cct.postcode ELSE ccs.postcode END postcode,
  COALESCE(cct.email, ccs.email) AS email,
  COALESCE(cct.phone, ccs.phone) AS phone,
  COALESCE(cct.treasurer, ccs.treasurer) as treasurer
  FROM committee_contact ccs
  JOIN committee_contact cct ON ccs.committee_id = cct.committee_id and ccs.role='candidate' and cct.role='candidacy') v
where committee_contact.contact_id = v.contact_id;
-- Now delete the 'candidate' records that have both 'candidate' and 'candidacy' roles
DELETE FROM committee_contact WHERE contact_id in (
  select
     ccs.contact_id
   FROM committee_contact ccs
          JOIN committee_contact cct ON ccs.committee_id = cct.committee_id and ccs.role='candidate' and cct.role='candidacy'
  );
-- Finally rename all of the remaining 'candidate' records to 'candidacy' for consistency.
UPDATE committee_contact set role = 'candidacy' WHERE role = 'candidate';