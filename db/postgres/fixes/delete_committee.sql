/**
  This script will totally eliminate a committee and all artifacts of the committee, its funds and ORCA campaigns
  leaving only a single log entry referenced by the committee name. It is extremely destructive.

  If the committee has a candidacy, delete the candidacy or remove the committee_id from the candidacy. The script will
  halt if it finds a candidacy for the committee.
*/

do $$
    declare
        v_committee_id integer;
        v_report_attachments integer[];

    begin
        select :committee_id into v_committee_id;

        -- Check for a candidacy and halt if there is one.
        if exists(select * from candidacy where committee_id = v_committee_id) then
            raise 'Committee has a candidacy. Either delete the candidacy first or remove the committee id from it if you want to keep the record.';
        end if;

        -- If there are any committee attachments, halt the script so they can be removed
        if exists(select * from attachment where target_type = 'committee' and target_id = v_committee_id) then
             raise 'Committee has one or more attachments. Delete them through the UI first so they will be removed from the file store';
         end if;

        -- If there are any registration attachments, halt the script so they can be removed
        if exists(select * from registration r join registration_attachment a
            on r.registration_id = a.registration_id where r.committee_id = v_committee_id) then
            raise 'Committee has one or more registration attachments. Delete them through the UI first so they will be removed from the file store';
        end if;

        -- If there are any report attachments, halt the script so they can be removed
        select array_agg(distinct r.report_id) into v_report_attachments from fund f join report r
            on f.fund_id = r.fund_id join attachment a on r.report_id = a.target_id and a.target_type = 'report'
        where f.committee_id = v_committee_id;

        if (v_report_attachments is not null) then
            raise 'Committee has the following report with attachments: %. The attachments must be deleted through the UI first.', v_report_attachments::text;
        end if;

        -- Check for compliance cases and halt if any reference the committee. Remove the reference from the case or point it at the entity. Cascade delete of the
        -- committee would delete the case. Not what we want.
        -- If there are any registration attachments, halt the script so they can be removed
        if exists(select * from compliance_case where committee_id = v_committee_id) then
            raise 'Committee is referenced by a compliance case. You must remove the reference or change it to reference the entity.';
        end if;

        -- If there are ORCA campaigns, remove them
        perform private.cf_delete_orca_campaign(fund_id) from fund where committee_id = :committee_id;

        -- If there are any filed reports, delete the reports, removing all the report data. Delete cascade should work.
        delete from report cascade where fund_id in (select fund_id from fund where committee_id = v_committee_id);
        delete from campaign_finance_submission where fund_id in (select fund_id from fund where committee_id = v_committee_id);
        delete from fund where committee_id = v_committee_id;

        -- The proposal table does not have FK to committee relation so proposals need to be deleted.
        delete from proposal p
            using committee_relation r
        where r.target_id = p.proposal_id and relation_type = 'proposal' and committee_id = v_committee_id;

        -- User authorization does not have a FK for cascade.
        delete from pdc_user_authorization where target_type = 'committee' and target_id = v_committee_id;

        -- Registration does not have on delete cascade on committee_id
        delete from registration where committee_id = v_committee_id;

        -- Cascade on the committee will handle committee_authorization, committee_api_key, committee_contact,
        -- committee_relation, registration
        delete from committee cascade where committee_id = v_committee_id;

        insert into committee_log
            (committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
        values
        (v_committee_id,'committee', null, 'delete', 'Committee deleted by manually run script', 'script', now());

    end
$$ language plpgsql;