do $$
  declare
  rp record;
  rj jsonb;
  jm json;
  j_meta jsonb;

  v_overcount int := 0;
  v_oversum numeric(6,2) := 0.0;
  v_contact_key text;
  v_final_count int;
  m_index integer;

  v_sum_id integer;
  v_sum_amount numeric(6,2);
begin

    -- Analyse all records that have been submitted that show personal funds reported.
    for rp in (select r.*, rs.aggregate_amount, cfs.version as orca_version from report r join transaction t on r.report_id = t.report_id
      join report_sum rs  on t.transaction_id = rs.transaction_id and rs.legacy_line_item='1B'
      left join campaign_finance_submission cfs on cfs.fund_id = r.fund_id and cfs.report_id=r.report_id
      where r.user_data is not null and t.amount > 0
        and cfs.version <> '100' and cfs.version>'1.440'
    ) loop

      raise notice 'Analyzing report % personal_funds % version %', rp.report_id, rp.aggregate_amount, rp.orca_version;
      rj := rp.user_data::jsonb;

      select j->>'contact_key' into v_contact_key from json_array_elements(cast (rj as json)->'contacts') j  where j->>'contributor_type' = 'S';

      v_overcount := 0;
      v_oversum := 0.0;
      if rj->>'misc' is not null then
          m_index := 0;
          for jm in (select j from json_array_elements(cast(rj as json)->'misc') j) loop
            -- Determine if this is a submission that was sent missing personal funds property that caused it to
            -- be included in the misc total
            if jm->>'contact_key' = v_contact_key and jm->>'personal_funds' is null then
                rj := jsonb_set(rj, array['misc', m_index::text, 'personal_funds'], to_jsonb(true), true);
                v_oversum := v_oversum + (jm->>'amount')::numeric(6,2);
                v_overcount := v_overcount + 1;
            end if;
            m_index := m_index + 1;
            end loop;

          -- If we corrected any transactions then we must add the property and adjust the metadata
          -- to ensure that the transactions are reported correctly.
          if v_overcount > 0 then
              j_meta := rp.metadata::jsonb;
              v_final_count := (j_meta->'sums'->'misc_auction'->>'count')::integer - v_overcount;
              v_sum_amount := (j_meta->'sums'->'misc_auction'->>'amount')::numeric(6,2) - v_oversum;
              raise notice '%', j_meta->'sums'->'misc_auction';
              if v_final_count > 0 then
                  j_meta := jsonb_set(j_meta, '{sums,misc_auction,count}', to_jsonb(v_final_count));
                  j_meta := jsonb_set(j_meta, '{sums,misc_auction,amount}', to_jsonb(v_sum_amount));

              else
                  j_meta := jsonb_set(j_meta, '{sums,misc_auction,count}', 'null');
                  j_meta := jsonb_set(j_meta, '{sums,misc_auction,amount}', 'null');
              end if;

              raise notice 'Correcting incorrectly reported misc entries of % ', v_oversum;
              raise notice '%', (rj->'misc')::text;
              raise notice '%', j_meta::text;
              update report set user_data=rj::text, metadata=j_meta::text where report_id = rp.report_id;
              select t.transaction_id into v_sum_id from transaction t join report_sum s on t.transaction_id = s.transaction_id where report_id=rp.report_id
                and s.legacy_line_item='1D';
              update transaction set amount = v_sum_amount where transaction_id = v_sum_id;
              update report_sum set tx_count = v_final_count where transaction_id = v_sum_id;
          end if;
      else
          -- These indicate a set of reports that had no misc entries but personal funds
          -- and will be corrected when the reports are resubmitted.
          raise notice 'No misc';
      end if;
    end loop;
end
$$ language plpgsql;