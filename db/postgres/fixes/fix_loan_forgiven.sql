with pmts as (
    select t.transaction_id
    from payment p join transaction t on p.transaction_id = t.transaction_id
    where t.category = 'loan' and t.act = 'payment' and coalesce(p.forgiven_amount, 0) > 0
)
update transaction set act = 'forgiven', description = 'LOAN FORGIVEN'
    from pmts where transaction.transaction_id = pmts.transaction_id;

