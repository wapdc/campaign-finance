-- Determine how many duplicate/overlapping committees exist
SELECT
  c2.committee_id,
  c.committee_id,
  c.filer_id,
  c.start_year,
  c.end_year,
  c.election_code,
  c.pac_type,
  c.exempt,
  c.name,  c.memo, c.registration_id,
       c.registered_at, c2.committee_id, c2.start_year, c2.end_year, c2.name, c2.pac_type, c2.exempt, c2.memo, c2.registration_id, c2.registered_at from committee c
  JOIN committee c2 on c2.filer_id = c.filer_id
   and (c.start_year between c2.start_year and coalesce(c2.end_year, 2999)
     OR c2.start_year between c.start_year and coalesce(c.end_year, 2999))
   and c2.committee_id>c.committee_id
order by filer_id;


-- generate a query to fix bonafides which should all be continuing.
-- also fix candidates which appear to be duplicates which should be merged.
SELECT
  c2.committee_id,
  c.filer_id,
  c.start_year,
  c.end_year,
  c.election_code,
  c2.pac_type,
  c.committee_id as src_committee_id,
  c2.name,
  null as election_year,
  c2.start_year as old_start_year
from committee c                                                                                                                                        JOIN committee c2 on c2.filer_id = c.filer_id
  and (c.start_year between c2.start_year and coalesce(c2.end_year, 2999)
    OR c2.start_year between c.start_year and coalesce(c.end_year, 2999))
  and c2.committee_id>c.committee_id
  and (c.pac_type='bonafide' or c.pac_type='candidate')
order by pac_type,filer_id;


-- Fix the continuing pacs by filer_id in case there are some that
-- might be transitioning committees
SELECT
  c2.committee_id,
  c.filer_id,
  c.start_year,
  c.end_year,
  c.election_code,
  c2.pac_type,
  c.committee_id as src_committee_id,
  c2.name,
  null as election_year,
  c2.start_year as old_start_year
from committee c                                                                                                                                        JOIN committee c2 on c2.filer_id = c.filer_id
  and (c.start_year between c2.start_year and coalesce(c2.end_year, 2999)
    OR c2.start_year between c.start_year and coalesce(c.end_year, 2999))
  and c2.committee_id>c.committee_id
order by filer_id;


-- Determine how many (non-surplus funds) funds in the migrated database do not have matching committees.
select * from fund f
  where committee_id is null and filer_id not like '%*%'


-- funds with no committee
select *
from fund f
  left join committee c
    on c.committee_id = f.committee_id
where c.committee_id is null

