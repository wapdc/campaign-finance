
select json_build_object(
               'events', json_agg(json_build_object(
                'job_name', 'cf_contributions_to_candidates_and_political_committees',
                'event', 'replaceContributionsData',
                'data', row_to_json(e)))
           )se from (  select report_id from report r join fund f on r.fund_id=f.fund_id
  WHERE committee_id = :committee_id and f.election_code = :election_year and report_type='C3') e;

select json_agg(json_build_object(
               'events', json_build_object(
                'job_name', 'cf_contributions_to_candidates_and_political_committees',
                'event', 'replaceContributionsData',
                'data', row_to_json(e)))
           )se from (  select report_id from report r join fund f on r.fund_id=f.fund_id
                       WHERE committee_id = :committee_id and f.election_code = :election_year and report_type='C3') e;

select json_agg(json_build_object(
               'events', json_build_object(
                'job_name', 'cf_contributions_to_candidates_and_political_committees',
                'event', 'replaceContributionsData',
                'data', row_to_json(e)))
           )se from (  select distinct report_number as report_id from od_contributions
                       WHERE committee_id = :committee_id and election_year = :election_year) e
