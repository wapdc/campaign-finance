insert into election (election_code, title, is_primary, is_special, election_date)
values ('2021S12', '2021 December Special', false, true, '2021-12-07');

insert into reporting_period (reporting_period_type, election_date, start_date, end_date, due_date)
values ('TwentyOneDayPreElection', '2021-12-07', '2021-10-01', '2021-11-15', '2021-11-16');
insert into reporting_period (reporting_period_type, election_date, start_date, end_date, due_date)
values ('SevenDayPreElection', '2021-12-07', '2021-11-16', '2021-11-29', '2021-11-30');
insert into reporting_period (reporting_period_type, election_date, start_date, end_date, due_date)
values ('PostElection', '2021-12-07', '2021-11-30', '2021-12-31', '2022-01-10');