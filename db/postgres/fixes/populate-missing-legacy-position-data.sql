update candidacy c set position_id = v.position_id
--select v.candidacy_id, count(v.candidacy_id), min(v.position_id), max(v.position_id), min(v.jurisdiction_office_id)
from (
  select
    p.position_id,
    c.candidacy_id,
    c1.c1_id,
    jo.jurisdiction_office_id
    --p.position_number,
    --c1.poss1,
    --c.*
    -- count(*)
  from candidacy c
    left join c1
      on c1.diss1 = c.jurisdiction_id::text
        and c1.offs1 = c.office_code
        and c1.election_year::text = substr(c.election_code, 1, 4)::text
        and c1.filer_id = c.filer_id
    join jurisdiction j
      on j.jurisdiction_id = c.jurisdiction_id
    join foffice f
      on f.offcode = c.office_code
    join jurisdiction_office jo
      on jo.jurisdiction_id = j.jurisdiction_id
        and jo.offcode = f.offcode
        and (
          -- match the jurisdiction office that was temporally valid at the time of the candidacy
          (jo.valid_from is null and jo.valid_to is null)
            -- prefer the declaration date over the campaign start date
            or (coalesce(c.sos_filing_date, c.campaign_start_date) between coalesce(jo.valid_from, date('1972-11-07')) and coalesce(jo.valid_to, now()::date))
        )
    join position p
      on p.jurisdiction_office_id = jo.jurisdiction_office_id
        and p.position_number = nullif(regexp_replace(c1.poss1, '^.*[^0-9]+.*$', ''), '')::int
        and (
          -- match the position that was temporally valid at the time of the candidacy
          (p.valid_from is null and p.valid_to is null)
          -- prefer the declaration date over the campaign start date
            or (coalesce(c.sos_filing_date, c.campaign_start_date) between coalesce(p.valid_from, date('1972-11-07')) and coalesce(p.valid_to, now()::date))
        )
  where c.position_id is null
) v
-- group by v.candidacy_id having count(v.candidacy_id) > 1
where v.candidacy_id = c.candidacy_id
