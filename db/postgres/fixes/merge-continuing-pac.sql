/**
  This query was developed to merge two continuing committees.
  Currently it only sets the dates of the committees correctly and puts current committee information on the old committee.
  IT does not merge reports from funds.
*/
do $$
    declare
        v_src_committee_id integer;
        v_dest_committee_id integer;
        v_continuing boolean;
        v_src_continuing boolean;
        v_src_pac_type TEXT;
        V_dest_pac_type text;
        v_src_end_year integer;
        v_dest_end_year integer;

    begin
        select c.committee_id, continuing, end_year into v_src_committee_id, v_src_continuing, v_src_end_year from committee c where c.committee_id = :src_committee_id;

        select c.committee_id, c.continuing, end_year into v_dest_committee_id, v_continuing, v_dest_end_year from committee c join fund f on f.committee_id=c.committee_id where c.committee_id = :target_committee_id;

        if v_src_committee_id is null or v_dest_committee_id is null then
            raise 'Invalid committee!';
        end if;

        if v_continuing is distinct from true or v_src_continuing is distinct from true then
            raise 'Both committees must be continuing';
        end if;

        if v_src_pac_type <> v_dest_pac_type THEN
            raise 'Both committees must have the same pac type';
        end if;

        -- Move registrations
        update registration set committee_id = v_dest_committee_id where committee_id=v_src_committee_id;
        -- Insert missing auth records
        update committee_authorization set committee_id=v_dest_committee_id where committee_id = v_src_committee_id
                                                                              and (realm, uid) not in (select realm, uid from committee_authorization where committee_id=v_dest_committee_id);
        update campaign_finance_submission set committee_id=v_dest_committee_id where committee_id=v_src_committee_id;
        -- Move api keys.
        update committee_api_key set committee_id=v_dest_committee_id where committee_id = v_src_committee_id;

        -- Move contacts
        delete from committee_contact where committee_id=v_src_committee_id;
        update committee_contact set committee_id=v_dest_committee_id where committee_id=v_src_committee_id;

        -- Move relations
        update committee_relation set committee_id=v_dest_committee_id where committee_id=v_src_committee_id;

        -- Move logs
        update committee_log set committee_id=v_dest_committee_id where committee_id=v_src_committee_id;
        insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
        values (v_dest_committee_id, 'merge', null, 'merge', 'IT Merge of from committee ' || v_src_committee_id, 'IT' , now());

        update committee set end_year = v_src_end_year,
                             registration_id = (select max(registration_id) from registration where committee_id = v_dest_committee_id
                                                                                                and submitted_at = (select max(submitted_at) from registration where committee_id=v_dest_committee_id))
                         where committee_id=v_dest_committee_id;



        -- Move any funds.
        update fund set target_id = v_dest_committee_id where target_type='committee' and target_id=v_src_committee_id;

        -- Remove committee
        delete from committee where committee_id = v_src_committee_id;
    end
$$ language plpgsql;