-- json registrations
--update registration set reporting_type = null where true;
update registration r
set reporting_type = lower(v.reporting_type)
from (
  select r.registration_id, r.user_data::json -> 'committee' ->> 'reporting_type' as reporting_type
  from registration r
  where r.reporting_type is null
    and r.user_data like '{%'
  ) v
where r.registration_id = v.registration_id
;

-- xml registrations
update registration r
set reporting_type = v.reporting_type
from (
  select r.registration_id, (xpath('//reporting/@type', r.user_data::xml))[1] as reporting_type
  from registration r
  where r.reporting_type is null
    and r.user_data like '<?xml%'
) v
where r.registration_id = v.registration_id
;

-- c1 matches where c1.ro is not null
update registration r
set reporting_type = lower(v.ro)
from (
  select r.registration_id, c1.ro
  from registration r
    left join committee c
      on c.committee_id = r.committee_id
    left join c1
      on c1.filer_id = c.filer_id
        and c1.election_year = date_part('YEAR', r.submitted_at)::text
  where r.reporting_type is null
    and r.user_data is null
    and nullif(c1.ro,'') is not null
  ) v
where v.registration_id = r.registration_id

-- remainder solution?
select reporting_type, count(*)
from registration
group by reporting_type

select c.reporting_type, r.legacy_doc_id, *
from registration r
  left join committee c
    on c.committee_id = r.committee_id
  left join c1
    on c1.filer_id = c.filer_id
      and c1.election_year = date_part('YEAR', r.submitted_at)::text
where r.reporting_type is null
  and c1.c1_id is not null

select count(*) from registration where reporting_type is null and user_data is not null