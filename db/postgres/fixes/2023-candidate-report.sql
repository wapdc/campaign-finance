select report_information.name,
       report_information.candidate_name,
       report_information.fund_id,
       'https://apollo.pdc.wa.gov/campaigns/committee?committee_id=' ||
       report_information.committee_id                                                       as committee_url,
       report_information.twenty_one_day_reported,
       report_information.twenty_one_day_pre_general_submitted_at,
       case
         when twenty_one_day_reported then
           greatest(0, date_part('day', twenty_one_day_pre_general_submitted_at -
                                        '2023-10-18')) end                                   as number_of_days_past_due_twenty_one_pre_general_reported,
       twenty_one_day_pre_general_expenditure_activity,
       seven_day_reported,
       seven_day_pre_general_submitted_at,
       case

         when seven_day_reported then
           greatest(0,
                    date_part('day', seven_day_pre_general_submitted_at - '2023-10-31')) end as number_of_days_past_due_seven_pre_general_reported,
       seven_day_pre_general_expenditure_activity,
       post_reported,
       post_general_submitted_at,
       case
         when post_reported then
           greatest(0,
                    date_part('day', post_general_submitted_at - '2023-12-11')) end          as number_of_days_past_due_post_general_reported,
       post_general_expenditure_activity,
       total_contributions_in_latest_report,
       total_expenditures_in_latest_report,
       latest_report_id,
       twenty_one_day_pre_general_covered_in_single_report,
       twenty_one_day_pre_general_complete_report_id,
       twenty_one_day_pre_general_starting_report_id,
       twenty_one_day_pre_general_ending_report_id,
       twenty_one_day_pre_general_report_one_period_start,
       twenty_one_day_pre_general_report_one_period_end,
       twenty_one_day_pre_general_report_one_expenditure_activity,
       twenty_one_day_pre_general_report_two_period_start,
       twenty_one_day_pre_general_report_two_period_end,
       twenty_one_day_pre_general_report_two_expenditure_activity,
       seven_day_pre_general_covered_in_single_report,
       seven_day_pre_general_complete_report_id,
       seven_day_pre_general_starting_report_id,
       seven_day_pre_general_ending_report_id,
       seven_day_pre_general_report_one_period_start,
       seven_day_pre_general_report_one_period_end,
       seven_day_pre_general_report_one_expenditure_activity,
       seven_day_pre_general_report_two_period_start,
       seven_day_pre_general_report_two_period_end,
       seven_day_pre_general_report_two_expenditure_activity,
       post_general_covered_in_single_report,
       post_general_complete_report_id,
       post_general_starting_report_id,
       post_general_ending_report_id,
       post_general_report_one_period_start,
       post_general_report_one_period_end,
       post_general_report_one_expenditure_activity,
       post_general_report_two_period_start,
       post_general_report_two_period_end,
       post_general_report_two_expenditure_activity
from (select label                                                                      as name,
             report_ids.fund_id,
             e.name as candidate_name,
             report_ids.committee_id,
             --get values for twenty one day pre-general
             case
               when twenty_one_day_pre_general_complete_report_id is not null then true
               else false end                                                           as twenty_one_day_pre_general_covered_in_single_report,
             twenty_one_day_pre_general_complete_report_id                              as twenty_one_day_pre_general_complete_report_id,
             twenty_one_day_pre_general_starting_report_id                              as twenty_one_day_pre_general_starting_report_id,
             twenty_one_day_pre_general_ending_report_id                                as twenty_one_day_pre_general_ending_report_id,
             coalesce(twenty_one_day_pre_general_complete_report.period_start,
                      twenty_one_day_pre_general_starting_report.period_start)          as twenty_one_day_pre_general_report_one_period_start,
             coalesce(twenty_one_day_pre_general_complete_report.period_end,
                      twenty_one_day_pre_general_starting_report.period_end)            as twenty_one_day_pre_general_report_one_period_end,
             twenty_one_day_pre_general_ending_report.period_start                      as twenty_one_day_pre_general_report_two_period_start,
             twenty_one_day_pre_general_ending_report.period_end                        as twenty_one_day_pre_general_report_two_period_end,
             case
               when twenty_one_day_pre_general_complete_report_id is not null
                 then twenty_one_day_pre_general_complete_report.submitted_at
               else greatest(twenty_one_day_pre_general_starting_report.submitted_at,
                             twenty_one_day_pre_general_ending_report.submitted_at) end as twenty_one_day_pre_general_submitted_at,
             case
               when twenty_one_day_pre_general_complete_report_id is not null
                 or (twenty_one_day_pre_general_starting_report_id is not null and
                     twenty_one_day_pre_general_ending_report_id is not null) then true
               else false end                                                           as twenty_one_day_reported,
             case
               when twenty_one_day_pre_general_complete_report_id is not null then
                 (twenty_one_day_pre_general_complete_report.metadata::json -> 'sums' -> 'expenditures' ->
                  'total_expenditures' ->>
                  'amount')::decimal
               else (twenty_one_day_pre_general_starting_report.metadata::json -> 'sums' -> 'expenditures' ->
                     'total_expenditures' ->>
                     'amount')::decimal +
                    (twenty_one_day_pre_general_ending_report.metadata::json -> 'sums' -> 'expenditures' ->
                     'total_expenditures' ->>
                     'amount')::decimal
               end                                                                      as twenty_one_day_pre_general_expenditure_activity,
             case
               when twenty_one_day_pre_general_complete_report_id is null then
                         twenty_one_day_pre_general_starting_report.metadata::json -> 'sums' -> 'expenditures' ->
                         'total_expenditures' ->>
                         'amount' end                                                   as twenty_one_day_pre_general_report_one_expenditure_activity,
             case
               when twenty_one_day_pre_general_complete_report_id is null then
                         twenty_one_day_pre_general_ending_report.metadata::json -> 'sums' -> 'expenditures' ->
                         'total_expenditures' ->>
                         'amount' end                                                   as twenty_one_day_pre_general_report_two_expenditure_activity,
             --get values for seven day pre-general
             case
               when seven_day_pre_general_complete_report_id is not null then true
               else false end                                                           as seven_day_pre_general_covered_in_single_report,
             seven_day_pre_general_complete_report_id                                   as seven_day_pre_general_complete_report_id,
             seven_day_pre_general_starting_report_id                                   as seven_day_pre_general_starting_report_id,
             seven_day_pre_general_ending_report_id                                     as seven_day_pre_general_ending_report_id,
             coalesce(seven_day_pre_general_complete_report.period_start,
                      seven_day_pre_general_starting_report.period_start)               as seven_day_pre_general_report_one_period_start,
             coalesce(seven_day_pre_general_complete_report.period_end,
                      seven_day_pre_general_starting_report.period_end)                 as seven_day_pre_general_report_one_period_end,
             seven_day_pre_general_ending_report.period_start                           as seven_day_pre_general_report_two_period_start,
             seven_day_pre_general_ending_report.period_end                             as seven_day_pre_general_report_two_period_end,
             case
               when twenty_one_day_pre_general_complete_report_id is not null
                 then seven_day_pre_general_complete_report.submitted_at
               else greatest(seven_day_pre_general_starting_report.submitted_at,
                             seven_day_pre_general_ending_report.submitted_at) end      as seven_day_pre_general_submitted_at,
             case
               when seven_day_pre_general_complete_report_id is not null
                 or (seven_day_pre_general_starting_report_id is not null and
                     seven_day_pre_general_ending_report_id is not null) then true
               else false end                                                           as seven_day_reported,
             case
               when seven_day_pre_general_complete_report_id is not null then
                 (seven_day_pre_general_complete_report.metadata::json -> 'sums' -> 'expenditures' ->
                  'total_expenditures' ->>
                  'amount')::decimal
               else (seven_day_pre_general_starting_report.metadata::json -> 'sums' -> 'expenditures' ->
                     'total_expenditures' ->>
                     'amount')::decimal +
                    (seven_day_pre_general_ending_report.metadata::json -> 'sums' -> 'expenditures' ->
                     'total_expenditures' ->>
                     'amount')::decimal end                                             as seven_day_pre_general_expenditure_activity,
             case
               when seven_day_pre_general_complete_report_id is null then
                         seven_day_pre_general_starting_report.metadata::json -> 'sums' -> 'expenditures' ->
                         'total_expenditures' ->>
                         'amount' end                                                   as seven_day_pre_general_report_one_expenditure_activity,
             case
               when seven_day_pre_general_complete_report_id is null then
                         seven_day_pre_general_ending_report.metadata::json -> 'sums' -> 'expenditures' ->
                         'total_expenditures' ->>
                         'amount' end                                                   as seven_day_pre_general_report_two_expenditure_activity,
             --get values for post-general
             post_general_starting_report_id                                            as post_general_starting_report_id,
             post_general_ending_report_id                                              as post_general_ending_report_id,
             post_general_complete_report_id                                            as post_general_complete_report_id,
             coalesce(post_general_complete_report.period_start,
                      post_general_starting_report.period_start)                        as post_general_report_one_period_start,
             coalesce(post_general_complete_report.period_end,
                      post_general_starting_report.period_end)                          as post_general_report_one_period_end,
             post_general_ending_report.period_start                                    as post_general_report_two_period_start,
             post_general_ending_report.period_end                                      as post_general_report_two_period_end,
             case
               when post_general_complete_report_id is not null
                 then post_general_complete_report.submitted_at
               else greatest(post_general_starting_report.submitted_at,
                             post_general_ending_report.submitted_at) end               as post_general_submitted_at,
             case
               when post_general_complete_report_id is not null
                 or (post_general_starting_report_id is not null and
                     post_general_ending_report_id is not null) then true
               else false end                                                           as post_reported,
             case
               when post_general_complete_report_id is not null then true
               else false end                                                           as post_general_covered_in_single_report,
             case
               when post_general_complete_report_id is not null then
                 (post_general_complete_report.metadata::json -> 'sums' -> 'expenditures' ->
                  'total_expenditures' ->>
                  'amount')::decimal
               else (post_general_starting_report.metadata::json -> 'sums' -> 'expenditures' ->
                     'total_expenditures' ->>
                     'amount')::decimal +
                    (post_general_ending_report.metadata::json -> 'sums' -> 'expenditures' ->
                     'total_expenditures' ->>
                     'amount')::decimal end                                             as post_general_expenditure_activity,
             case
               when post_general_complete_report_id is null then
                         post_general_starting_report.metadata::json -> 'sums' -> 'expenditures' ->
                         'total_expenditures' ->>
                         'amount' end                                                   as post_general_report_one_expenditure_activity,
             case
               when post_general_complete_report_id is null then
                         post_general_ending_report.metadata::json -> 'sums' -> 'expenditures' ->
                         'total_expenditures' ->>
                         'amount' end                                                   as post_general_report_two_expenditure_activity,
             --get latest report data
             latest_report.metadata::json -> 'sums' -> 'receipts' -> 'campaign_receipts' ->>
             'amount'                                                                   as total_contributions_in_latest_report,
             latest_report.metadata::json -> 'sums' -> 'expenditures' -> 'campaign_expenses' ->>
             'amount'                                                                   as total_expenditures_in_latest_report,
             latest_report_id
      from (select label,
                   cm.target_id as fund_id,
                   committee_id,
                   --get report(s) that cover the twenty one day pre-general reporting period
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start <= '2023-09-01'
                      and period_end > '2023-09-01'
                    order by period_start, submitted_at
                    limit 1)    as twenty_one_day_pre_general_starting_report_id,
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start > '2023-09-01'
                      and period_end >= '2023-10-16'
                    order by period_start, submitted_at
                    limit 1)    as twenty_one_day_pre_general_ending_report_id,
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start <= '2023-09-01'
                      and period_end >= '2023-10-16'
                    order by period_start, submitted_at
                    limit 1)       twenty_one_day_pre_general_complete_report_id,
                   --get report(s) that cover the seven day pre-general reporting period
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start <= '2023-10-17'
                      and period_end > '2023-10-17'
                    order by period_start, submitted_at
                    limit 1)    as seven_day_pre_general_starting_report_id,
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start < '2023-10-30'
                      and period_end >= '2023-10-30'
                    order by period_start, submitted_at
                    limit 1)    as seven_day_pre_general_ending_report_id,
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start <= '2023-10-17'
                      and period_end >= '2023-10-30'
                    order by period_start, submitted_at
                    limit 1)       seven_day_pre_general_complete_report_id,
                   --get report(s) that cover the seven day post-general reporting period
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start <= '2023-10-31'
                      and period_end > '2023-10-31'
                    order by period_start, submitted_at
                    limit 1)    as post_general_starting_report_id,
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start < '2023-11-30'
                      and period_end >= '2023-11-30'
                    order by period_start, submitted_at
                    limit 1)    as post_general_ending_report_id,
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                      and period_start <= '2023-10-31'
                      and period_end >= '2023-11-30'
                    order by period_start, submitted_at
                    limit 1)       post_general_complete_report_id,
                   (select report_id
                    from report
                    where fund_id = cm.target_id
                    order by period_start desc, submitted_at desc
                    limit 1)       latest_report_id
            from collection_member cm
                   left join fund f on cm.target_id = f.fund_id
            where cm.collection_id = :collection_id) report_ids
             left join committee c on report_ids.committee_id=c.committee_id
             left join entity e on e.entity_id=c.person_id
             left join report twenty_one_day_pre_general_starting_report
                       on twenty_one_day_pre_general_starting_report.report_id =
                          report_ids.twenty_one_day_pre_general_starting_report_id
             left join report twenty_one_day_pre_general_ending_report
                       on twenty_one_day_pre_general_ending_report.report_id =
                          report_ids.twenty_one_day_pre_general_ending_report_id
             left join report twenty_one_day_pre_general_complete_report
                       on twenty_one_day_pre_general_complete_report.report_id =
                          report_ids.twenty_one_day_pre_general_complete_report_id
             left join report seven_day_pre_general_starting_report on seven_day_pre_general_starting_report.report_id =
                                                                       report_ids.seven_day_pre_general_starting_report_id
             left join report seven_day_pre_general_ending_report on seven_day_pre_general_ending_report.report_id =
                                                                     report_ids.seven_day_pre_general_ending_report_id
             left join report seven_day_pre_general_complete_report on seven_day_pre_general_complete_report.report_id =
                                                                       report_ids.seven_day_pre_general_complete_report_id
             left join report post_general_starting_report
                       on post_general_starting_report.report_id = report_ids.post_general_starting_report_id
             left join report post_general_ending_report
                       on post_general_ending_report.report_id = report_ids.post_general_ending_report_id
             left join report post_general_complete_report
                       on post_general_complete_report.report_id = report_ids.post_general_complete_report_id
             left join report latest_report
                       on latest_report.report_id = latest_report_id) report_information
order by candidate_name