INSERT INTO collection (collection_group, collection_key, title, updated_at, target, category, definition, metadata) VALUES ('it-collection', null, 'IT Collection for 146495 respondents', now(), 'fund', 'it', null, null);

insert into collection_member(collection_id, target_id, label)
  (select :collection_id, fund_id, c.name
   from fund f
          join committee c on f.committee_id = c.committee_id
   where f.committee_id in (
31427,
31833,
33104,
32599,
32144,
33423,
34345,
32869,
33366,
33863,
32021,
32297,
32597,
33690,
34467,
33203,
33825,
32262,
32623,
29841,
33515,
32146,
33740,
34333,
31933,
22884,
33359,
33524
     ));