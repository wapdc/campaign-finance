do $$
    -- This script updating a numbers of tables as it relates to the c6_sponsor (IE) application.
    DECLARE
        e_rec record;
        v_entity_id integer;
        v_count integer;
        v_total integer;
        begin
          -- Point all candidate committees to entity records.
          select count(*) into v_count from wapdc.public.committee where pac_type = 'candidate' and person_id is null;
          raise notice '% candidate committees with null person_ids', v_count;

          update committee c set person_id = cv.person_id FROM
             (SELECT c.committee_id, ca.person_id FROM candidacy ca join committee c on ca.committee_id=c.committee_id) CV
          WHERE c.committee_id = cv.committee_id;

          select count(*) into v_count from wapdc.public.committee where pac_type = 'candidate' and person_id is null;
          raise notice '% candidate committees with null person_ids after conversion', v_count;

          -- Group by filer id and create entities for all existing committees.
          for e_rec in (
              SELECT * FROM (SELECT
                c.filer_id,
                c.name,
                cc.address,
                cc.premise,
                cc.city,
                cc.state,
                cc.postcode,
                cc.email,
                row_number() over (partition by c.filer_id order by c.start_year desc) rn
              FROM committee c
                left join committee_contact cc on c.committee_id = cc.committee_id
                   and role = 'committee'
              WHERE filer_id IS NOT NULL
                AND person_id IS NULL
                AND committee_type = 'CO'
                AND coalesce(c.end_year,9999) >= 2016
              ) cv
              where rn=1) LOOP

            -- insert a new entity, assign the entity_id to the variable
            insert into entity (is_person, name, filer_id, address, premise, city, state, postcode, email)
                values (false, e_rec.name, e_rec.filer_id, e_rec.address, e_rec.premise, e_rec.city, e_rec.state, e_rec.postcode, e_rec.email)
                returning entity_id into v_entity_id;

            -- update the committee record with the entity_id
            update committee c set person_id = v_entity_id
             where c.filer_id = e_rec.filer_id;

          end loop;

          -- Make sure we have marked all entities from the committee records.
          update c6_sponsor s set entity_id = v.entity_id from (
              select c.person_id as entity_id,
                     t.sponsor_id
              from committee c
                  join tmp_committee_c6_sponsors t ON t.commitee_id=c.committee_id
                  left join c6_sponsor c6s2 on c6s2.sponsor_id = t.sponsor_id
              where t.sponsor_id is not null and t.commitee_id is not null
                and c6s2.entity_id is null
              ) v
              where v.sponsor_id=s.sponsor_id;

          select count(entity_id), count(sponsor_id) into v_count, v_total from c6_sponsor;
          raise notice '%/% records converted', v_count, v_total;

          -- Create entity records for all types setting is person to true for A/B sponsor records.
          FOR e_rec in (SELECT
            sponsor_id,
            CASE WHEN c.sponsor_desc in ('A', 'B') then true else false end as is_person,
            TRIM(coalesce(fname, '') || ' ' || coalesce(name, '')) as name,
            c.addr as address,
            c.city as city,
            c.state as state,
            c.zip as postcode,
            c.email
            FROM c6_sponsor c where
              c.entity_id is null
              and sponsor_id in (select sponsor_id from c6_reports where election_year >= '2016')) LOOP

              insert into entity(is_person, name, address, city, state, postcode, email)
                values(e_rec.is_person, e_rec.name, e_rec.address, e_rec.city, e_rec.state, e_rec.postcode, e_rec.email)
                returning entity_id into v_entity_id;

              update c6_sponsor set entity_id = v_entity_id where sponsor_id=e_rec.sponsor_id;

          end loop;
          -- Make sure all committees have authorization records for filing c6 reports (but not F1).
          insert into entity_authorization(entity_id, realm, role, uid, approved)
          select distinct c2.person_id, realm, 'cf_reporter', uid, approved from committee_authorization ca
             join committee c2 on ca.committee_id = c2.committee_id
             join entity e on e.entity_id=c2.person_id
             on conflict do nothing;

          -- Check where we are on conversion.
          select count(t.sponsor_id), count(e.entity_id) into v_total,v_count from tmp_committee_c6_sponsors t
                                                                                       left join c6_sponsor c6s on t.sponsor_id = c6s.sponsor_id
                                                                                       left join entity e on c6s.entity_id = e.entity_id;
          raise notice '%/% mapped records converted', v_count, v_total;

          select count(entity_id), count(sponsor_id) into v_count, v_total from c6_sponsor;
          raise notice '%/% records converted', v_count, v_total;

          raise notice '----------------- Summary ----------------------------';
          for e_rec in (select c.sponsor_desc, count(e.entity_id) entities, count(c.sponsor_id) total from c6_sponsor c left join entity e on c.entity_id = e.entity_id
                        group by c.sponsor_desc order by 1) loop
              raise notice '%: %/%',e_rec.sponsor_desc, e_rec.entities, e_rec.total;
              end loop;
        end
    $$ language plpgsql;

