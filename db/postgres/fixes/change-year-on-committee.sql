-- This script updates the committee, fund, report, and related records to reflect the new election year.

-- Parameters:
-- :committee_id - The ID of the committee to update
-- :election_year - The new election year
-- :election_code - The new election code (optional, defaults to the election year if not provided)

-- In addition to simply changing the year on a committee (and all affiliated records) you can also use  this when
-- changing from a special election to a regular election or vice versa. Also, if you need to change from continuing
-- to single year, you can run this then change the continuing flag on committee to false and add the end year. From
-- there, the filer can submit a registration amendment which will update the user_data field on the registration
-- to include ALL the updated data - election year, single  year status, etc. A real time saver!--

do $$

declare
    p_committee_id integer;
    v_committee_id integer;
    v_fund_id integer;
    v_election_year integer;
    v_end_year integer;
    v_election_code text;
    v_old_election_code text;
    v_old_election_year integer;
    v_pac_type text;
    v_continuing boolean;
    v_report record;
    v_user_data jsonb;
    v_sos_candidate text;
    v_month_of_special integer;
    v_orca_id integer;

begin
    p_committee_id := :committee_id;
    v_election_year := :election_year;
    v_election_code := :election_code;


    -- Check for a valid committee
    SELECT committee_id, election_code, start_year, v_end_year, continuing, pac_type INTO v_committee_id, v_old_election_code,v_old_election_year, v_end_year, v_continuing, v_pac_type
      from committee c
      where committee_id=p_committee_id;


    if v_committee_id is null then
        RAISE 'Cannot find committee for this committee_id';
        end if;

    if v_pac_type = 'candidate' then
        select max(coalesce(sos_candidate_code, sos_candidate_id::text)) into v_sos_candidate from candidacy where committee_id=v_committee_id;
        if v_sos_candidate is not null then
            raise 'Cannot change election year on a candidacy that was registered with the secretary of state';
        end if;
    end if;

    if v_election_code is not null and substr(v_election_code, 1, 4) <> cast(v_election_year as text) then
        raise 'Cannot change committee start year to a different year';
    end if;

    -- Update the committee record.
    if v_continuing is distinct from true then
       raise notice 'Updating single election committee election code with type: %', v_pac_type ;
       update committee set
         start_year = v_election_year,
         end_year = v_election_year,
         election_code = v_election_code::text
        where committee_id= v_committee_id;

       if v_pac_type = 'candidate' then
           -- Modify the candidacy record.
           raise notice 'Changing election year on candidacy record';
           update candidacy set election_code=v_election_year::text
             where committee_id = v_committee_id;
       end if;
    else
       if v_end_year is not null  then
           raise 'Too dangerous to try and update a committee that has ended';
       end if;
       raise notice 'Updating continuting committee election code with type: %', v_pac_type ;
       update committee set
                            start_year = v_election_year,
                            election_code = v_election_code::text
       where committee_id= v_committee_id;
    end if;

    select fund_id into v_fund_id from fund where committee_id = v_committee_id
      and (election_code = v_old_election_year::text);

    if v_fund_id is not null then

      raise notice 'Found fund to change year on';
      -- Update the fund table
      update fund set election_code = v_election_year::text where fund_id=v_fund_id;

      update report set election_year = v_election_year::text where fund_id=v_fund_id;

      for v_report in select report_id, user_data from report where user_data is not null and fund_id = v_fund_id LOOP
        raise notice 'altering report json for report: %', v_report.report_id;
        v_user_data = v_report.user_data::jsonb;
        v_user_data := jsonb_set(v_user_data, '{election_year}', to_jsonb(v_election_year));
        update report
        set user_data=v_user_data::text
        where report_id=v_report.report_id;

        raise notice 'User data changed: %', v_user_data::text;

      end loop;

      select fund_id into v_orca_id from private.campaign_fund where fund_id=v_fund_id;

      if v_orca_id is not null then
      -- Change the year on the orca file.
          update private.properties set value = v_election_year::text where
            value = v_old_election_year::text and fund_id=v_fund_id;

          -- If the new election code is a special election, we need to set orca properties correctly.
          IF (length(v_election_code)) > 4 then
            if (substr(v_election_code,5,1) = 'P') then
                v_month_of_special := 7;
            else
                v_month_of_special := cast(substr(v_election_code, 6, 1) as integer) -1;
            end if;
            insert into private.properties(fund_id, name, value)
                values (v_fund_id, 'CAMPAIGNINFO:ISSPECIAL', 'true')
            on conflict (fund_id, name) do nothing;
            insert into private.properties(fund_id, name, value)
                values (v_fund_id, 'CAMPAIGNINFO:MONTHOFSPECIAL', v_month_of_special)
            on conflict (fund_id, name) do update set value=excluded.value;
          else
            delete from private.properties where fund_id=v_fund_id and name in ('CAMPAIGNINFO:MONTHOFSPECIAL', 'CAMPAIGNINFO:ISSPECIAL');
          end if;

          delete from private.election_participation where fund_id = v_fund_id;
          insert into private.election_participation(fund_id, election_code) VALUES (v_fund_id, v_election_code);
          perform private.cf_populate_reporting_obligations(v_fund_id);
      end if;
    end if;

    -- Uncomment for debugging
    -- rollback;
end
$$ language plpgsql
