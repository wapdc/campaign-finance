alter table contribution_limit drop column if exists valid_to;
alter table contribution_limit drop column if exists valid_from;

update contribution_limit set amount = 4800.00 where amount = 4000.00;
update contribution_limit set amount = 2400.00 where amount = 2000.00;
update contribution_limit set amount = 1200.00 where amount = 1000.00;
update contribution_limit set amount = 1.20 where amount = 1.00;
update contribution_limit set amount = 0.60 where amount = 0.50;

update thresholds set amount = 250.00 where amount = 100.00;
update thresholds set amount = 200.00 where amount = 50.00;
update thresholds set amount = 100.00 where amount = 25.00;
update thresholds set amount = 150.00 where property = 'report_auctions';
update thresholds set amount = 500.00 where property = 'report_anonymous';
update thresholds set amount = 7000.00 where property = 'mini_reporting';

select private.cf_populate_limits(f.fund_id) from fund f
where fund_id in (select distinct fund_id from private.limits)
