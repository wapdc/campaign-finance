INSERT INTO election (election_code,title,is_primary,is_special,election_date) VALUES ('2027','2027 General',false,false,'2027-11-02');

INSERT INTO election (election_code,title,is_primary,is_special,election_date) VALUES ('2027P','2027 Primary',true,false,'2027-08-03');

INSERT INTO election (election_code,title,is_primary,is_special,election_date) VALUES ('2027S2','2027 February Special',false,true,'2027-02-09');

INSERT INTO election (election_code,title,is_primary,is_special,election_date) VALUES ('2027S4','2027 April Special',false,true,'2027-04-27');

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('TwentyOneDayPrePrimary',
     '2027-08-03',
     '2027-06-01',
     '2027-07-12',
     '2027-07-13',
     false,
     true,
     false);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('TwentyOneDayPreGeneral',
     '2027-11-02',
     '2027-09-01',
     '2027-10-11',
     '2027-10-12',
     false,
     true,
     false);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('PostPrimary',
     '2027-08-03',
     '2027-07-27',
     '2027-08-31',
     '2027-09-10',
     false,
     false,
     true);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('PostGeneral',
     '2027-11-02',
     '2027-10-26',
     '2027-11-30',
     '2027-12-10',
     false,
     false,
     true);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('PostElection',
     '2027-02-09',
     '2027-02-02',
     '2027-02-28',
     '2027-03-10',
     false,
     false,
     true);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('PostElection',
     '2027-04-27',
     '2027-04-20',
     '2027-05-31',
     '2027-06-10',
     false,
     false,
     true);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('SevenDayPrePrimary',
     '2027-08-03',
     '2027-07-13',
     '2027-07-26',
     '2027-07-27',
     true,
     false,
     false);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('SevenDayPreGeneral',
     '2027-11-02',
     '2027-10-12',
     '2027-10-25',
     '2027-10-26',
     true,
     false,
     false);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('SevenDayPreElection',
     '2027-02-09',
     '2027-01-19',
     '2027-02-01',
     '2027-02-02',
     true,
     false,
     false);

INSERT INTO reporting_period(reporting_period_type,election_date,start_date,end_date,due_date,pre7,pre21,post) VALUES
    ('SevenDayPreElection',
     '2027-04-27',
     '2027-04-06',
     '2027-04-19',
     '2027-04-20',
     true,
     false,
     false);
