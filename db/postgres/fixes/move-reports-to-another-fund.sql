-- Move reports from one fund to another
-- :fund_id = 16921
-- :report_ids = 110009242,110009248,110009257,110009258,110009259
UPDATE report set fund_id=:fund_id
  where report_id in (:report_ids);

UPDATE transaction set fund_id=:fund_id
  where report_id in (:report_ids);


