CREATE TABLE committee_registration_draft (
  committee_id INTEGER PRIMARY KEY,
  registration_data VARCHAR,
  updated_at TIMESTAMP(0) WITH TIME ZONE,
  username VARCHAR(255),
  attachment_reference VARCHAR(255),
  verified BOOLEAN,
  FOREIGN KEY (committee_id) REFERENCES committee(committee_id)
);

