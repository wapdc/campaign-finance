INSERT INTO lookup (category, field, value, label) VALUES
    ('report', 'document_type', 'other', 'Other'),
    ('report', 'document_type', 'pdc correspondence', 'PDC correspondence'),
    ('report', 'document_type', 'compliance response', 'Compliance related'),
    ('report', 'document_type', 'reporting modification', 'Reporting modification'),
    ('report', 'document_type', 'investment of funds', 'Investment of funds'),
    ('report', 'document_type', 'in kind contributions', 'In kind contributions')