CREATE TABLE registration_change (
  registration_id INT,
  change_type VARCHAR(128),
  PRIMARY KEY (registration_id, change_type)
);
