drop table if exists campaign_finance_submission cascade;
CREATE TABLE campaign_finance_submission (
  submission_id SERIAL PRIMARY KEY,
  version VARCHAR(30),
  source VARCHAR(255),
  type VARCHAR(255),
  payload varchar,
  submitted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NOW(),
  submitted BOOLEAN NOT NULL DEFAULT FALSE,
  processed BOOLEAN NOT NULL DEFAULT FALSE,
  has_warnings BOOLEAN DEFAULT FALSE,
  updated_at TIMESTAMP(0) WITH TIME ZONE,
  fund_id INT,
  committee_id INT,
  message varchar
)