-- Update to endorsement and set amount 0
with cte as (select transaction_id from loan where lender_endorser = 'E')
update transaction set amount = 0, act = 'endorsement' from cte where transaction.transaction_id = cte.transaction_id;

-- Set parent IDs
update transaction set parent_id = 11201273 where transaction_id = 11201274;
update transaction set parent_id = 6073520 where transaction_id = 6073521;
update transaction set parent_id = 13001354 where transaction_id = 13001355;
update transaction set parent_id = 13001348 where transaction_id = 13001349;
update transaction set parent_id = 11266346 where transaction_id = 11266347;
update transaction set parent_id = 12633800 where transaction_id = 12633801;
update transaction set parent_id = 12633798 where transaction_id = 12633799;
update transaction set parent_id = 12389785 where transaction_id = 12389786;
update transaction set parent_id = 11201271 where transaction_id = 11201272;
update transaction set parent_id = 11201275 where transaction_id = 11201276;
update transaction set parent_id = 11201269 where transaction_id = 11201270;
update transaction set parent_id = 14514743 where transaction_id in (14514744, 14514745);
update transaction set parent_id = 12594938 where transaction_id in (12594939, 12594940);
update transaction set parent_id = 13001350 where transaction_id = 13001351;
update transaction set parent_id = 13001352 where transaction_id = 13001353;
update transaction set parent_id = 14192011 where transaction_id = 14192012;
update transaction set parent_id = 14192013 where transaction_id = 14192014;
update transaction set parent_id = 14754650 where transaction_id = 14754651;
update transaction set parent_id = 14760799 where transaction_id = 14760800;