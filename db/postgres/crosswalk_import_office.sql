INSERT INTO crosswalk (field_name, external_value, internal_value) VALUES
  ('office_code', 'AD-AS-1', '21'), --ASSESSOR
  ('office_code', 'AS-1', '21'), --ASSESSOR
  ('office_code', 'ASSE', '21'), --ASSESSOR
  ('office_code', 'ASSESSOR', '21'), --ASSESSOR
  ('office_code', 'BC-A', '21'), --ASSESSOR
  ('office_code', 'CAS', '21'), --ASSESSOR
  ('office_code', 'CASS', '21'), --ASSESSOR
  ('office_code', 'CH29', '21'), --ASSESSOR
  ('office_code', 'CTYW', '21'), --ASSESSOR
  ('office_code', 'CU12', '21'), --ASSESSOR
  ('office_code', 'FR1540', '21'), --ASSESSOR
  ('office_code', 'JE-900', '21'), --ASSESSOR
  ('office_code', 'KPASSESS', '21'), --ASSESSOR
  ('office_code', 'KS-AS', '21'), --ASSESSOR
  ('office_code', 'LE-AS', '21'), --ASSESSOR
  ('office_code', 'OKAS', '21'), --ASSESSOR
  ('office_code', 'PA50', '21'), --ASSESSOR
  ('office_code', 'SCA1', '21'), --ASSESSOR
  ('office_code', 'SCASR', '21'), --ASSESSOR
  ('office_code', 'SJ-ASR', '21'), --ASSESSOR
  ('office_code', 'SKASS', '21'), --ASSESSOR
  ('office_code', 'SP256', '21'), --ASSESSOR
  ('office_code', 'TCAS', '21'), --ASSESSOR
  ('office_code', 'WL-AS', '21'), --ASSESSOR
  ('office_code', 'YAASSESS', '21'), --ASSESSOR
  ('office_code', 'WM1740', '23'), --AT-LARGE POSITION B
  ('office_code', 'AD-AU-1', '20'), --AUDITOR
  ('office_code', 'AU-1', '20'), --AUDITOR
  ('office_code', 'AUD', '20'), --AUDITOR
  ('office_code', 'AUDI', '20'), --AUDITOR
  ('office_code', 'AUDITOR', '20'), --AUDITOR
  ('office_code', 'BC-AUD', '20'), --AUDITOR
  ('office_code', 'CAUD', '20'), --AUDITOR
  ('office_code', 'CH30', '20'), --AUDITOR
  ('office_code', 'CTYW-2', '20'), --AUDITOR
  ('office_code', 'CU13', '20'), --AUDITOR
  ('office_code', 'FR1550', '20'), --AUDITOR
  ('office_code', 'JE-950', '20'), --AUDITOR
  ('office_code', 'KPAUDITOR', '20'), --AUDITOR
  ('office_code', 'KS-AU', '20'), --AUDITOR
  ('office_code', 'LE-AU', '20'), --AUDITOR
  ('office_code', 'OKAU', '20'), --AUDITOR
  ('office_code', 'PA51', '20'), --AUDITOR
  ('office_code', 'PI-AUD', '20'), --AUDITOR
  ('office_code', 'SCA2', '20'), --AUDITOR
  ('office_code', 'SCAUD', '20'), --AUDITOR
  ('office_code', 'SJ-AUD', '20'), --AUDITOR
  ('office_code', 'SKAUD', '20'), --AUDITOR
  ('office_code', 'SP255', '20'), --AUDITOR
  ('office_code', 'TCAU', '20'), --AUDITOR
  ('office_code', 'WL-AU', '20'), --AUDITOR
  ('office_code', 'YAAUD', '20'), --AUDITOR
  ('office_code', 'AU-CL-1', '22'), --CLERK
  ('office_code', 'BC-CLK', '22'), --CLERK
  ('office_code', 'CCLE', '22'), --CLERK
  ('office_code', 'CH31', '22'), --CLERK
  ('office_code', 'CL-1', '22'), --CLERK
  ('office_code', 'CLER', '22'), --CLERK
  ('office_code', 'CLK', '22'), --CLERK
  ('office_code', 'CLK001', '22'), --CLERK
  ('office_code', 'CU14', '22'), --CLERK
  ('office_code', 'FR1560', '22'), --CLERK
  ('office_code', 'JE-1000', '22'), --CLERK
  ('office_code', 'KPCLERK', '22'), --CLERK
  ('office_code', 'KS-CL', '22'), --CLERK
  ('office_code', 'LE-CL', '22'), --CLERK
  ('office_code', 'OKCL', '22'), --CLERK
  ('office_code', 'PA52', '22'), --CLERK
  ('office_code', 'SCC1', '22'), --CLERK
  ('office_code', 'SCCLK', '22'), --CLERK
  ('office_code', 'SJ-CLERK', '22'), --CLERK
  ('office_code', 'SKCLE', '22'), --CLERK
  ('office_code', 'SP258', '22'), --CLERK
  ('office_code', 'TCCL', '22'), --CLERK
  ('office_code', 'WL-CL', '22'), --CLERK
  ('office_code', 'YACLERK', '22'), --CLERK
  ('office_code', 'LE-CC-3', '23'), --COMM DIST
  ('office_code', 'LE-PUD-2', '48'), --COMM DIST
  ('office_code', 'BC-COM2', '23'), --COMMISSIONER
  ('office_code', 'BC-PUD3', '48'), --COMMISSIONER
  ('office_code', 'CCD3', '23'), --COMMISSIONER
  ('office_code', 'CCM3', '23'), --COMMISSIONER
  ('office_code', 'CH34', '23'), --COMMISSIONER
  ('office_code', 'COM3', '23'), --COMMISSIONER
  ('office_code', 'COMM3', '23'), --COMMISSIONER
  ('office_code', 'CPUD2', '48'), --COMMISSIONER
  ('office_code', 'CU17', '23'), --COMMISSIONER
  ('office_code', 'DG146', '48'), --COMMISSIONER
  ('office_code', 'FR1530', '23'), --COMMISSIONER
  ('office_code', 'FR6930', '48'), --COMMISSIONER
  ('office_code', 'KS-CM3', '23'), --COMMISSIONER
  ('office_code', 'KS-PUD2', '48'), --COMMISSIONER
  ('office_code', 'OKC3', '23'), --COMMISSIONER
  ('office_code', 'PA55', '23'), --COMMISSIONER
  ('office_code', 'PUD1-D1', '48'), --COMMISSIONER
  ('office_code', 'PUD12', '48'), --COMMISSIONER
  ('office_code', 'SKCCM3', '23'), --COMMISSIONER
  ('office_code', 'SKPUD-2', '48'), --COMMISSIONER
  ('office_code', 'SPUD3', '48'), --COMMISSIONER
  ('office_code', 'STCD2', '23'), --COMMISSIONER
  ('office_code', 'WL-CM3', '23'), --COMMISSIONER
  ('office_code', 'OKPUDC2', '48'), --COMMISSIONER DIST.
  ('office_code', 'GR179', '48'), --COMMISSIONER DISTICT
  ('office_code', 'GR183', '48'), --COMMISSIONER DISTICT B
  ('office_code', 'CCCD-3', '23'), --COMMISSIONER DISTRICT
  ('office_code', 'KPCOMM3', '23'), --COMMISSIONER DISTRICT
  ('office_code', 'KPPUDCOMM3', '48'), --COMMISSIONER DISTRICT
  ('office_code', 'PUD1-3', '48'), --COMMISSIONER DISTRICT
  ('office_code', 'PUD101', '48'), --COMMISSIONER DISTRICT
  ('office_code', 'PUD102', '48'), --COMMISSIONER DISTRICT
  ('office_code', 'PUD3-3', '48'), --COMMISSIONER DISTRICT
  ('office_code', 'WM1422', '48'), --COMMISSIONER DISTRICT
  ('office_code', 'SP253', '23'), --COMMISSIONER DISTRICT 2
  ('office_code', 'SP254', '23'), --COMMISSIONER DISTRICT 3
  ('office_code', 'PUD3', '48'), --COMMISSIONER DISTRICT NO.
  ('office_code', 'JE-1070', '23'), --COMMISSIONER, DISTRICT
  ('office_code', 'JE-2130', '48'), --COMMISSIONER, DISTRICT
  ('office_code', 'CR-CPUD-1', '48'), --COMMISSIONER, DISTRICT NO.
  ('office_code', 'TPUD-1', '48'), --COMMISSIONER, DISTRICT NO.
  ('office_code', 'BC-COR', '24'), --CORONER
  ('office_code', 'CCO', '24'), --CORONER
  ('office_code', 'CH32', '24'), --CORONER
  ('office_code', 'CO-1', '24'), --CORONER
  ('office_code', 'CORONER', '24'), --CORONER
  ('office_code', 'CRN', '24'), --CORONER
  ('office_code', 'FR1570', '24'), --CORONER
  ('office_code', 'KPCORONER', '24'), --CORONER
  ('office_code', 'KS-CO', '24'), --CORONER
  ('office_code', 'LE-CO', '24'), --CORONER
  ('office_code', 'OKCN', '24'), --CORONER
  ('office_code', 'SKCOR', '24'), --CORONER
  ('office_code', 'STCOR', '24'), --CORONER
  ('office_code', 'TCCO', '24'), --CORONER
  ('office_code', 'WL-COR', '24'), --CORONER
  ('office_code', 'YACORONER', '24'), --CORONER
  ('office_code', 'CR-VAN1', '32'), --COUNCIL - POSITION NO.
  ('office_code', 'EV4', '32'), --COUNCIL POSITION
  ('office_code', 'SJ-CR-D3', '25'), --COUNCIL RESIDENCY DISTRICT
  ('office_code', 'CR-CCC', '25'), --COUNCILOR, CHAIR (AT-LARGE)
  ('office_code', 'CR-CCD1', '25'), --COUNCILOR, DISTRICT NO.
  ('office_code', 'CR-CCD2', '25'), --COUNCILOR, DISTRICT NO.
  ('office_code', 'AS1', '21'), --COUNTY ASSESSOR
  ('office_code', 'CRASSESSOR', '21'), --COUNTY ASSESSOR
  ('office_code', 'DG58', '21'), --COUNTY ASSESSOR
  ('office_code', 'FE1', '21'), --COUNTY ASSESSOR
  ('office_code', 'GA7', '21'), --COUNTY ASSESSOR
  ('office_code', 'GR108', '21'), --COUNTY ASSESSOR
  ('office_code', 'IS-AS-1', '21'), --COUNTY ASSESSOR
  ('office_code', 'LI80', '21'), --COUNTY ASSESSOR
  ('office_code', 'PE35', '21'), --COUNTY ASSESSOR
  ('office_code', 'AS2', '20'), --COUNTY AUDITOR
  ('office_code', 'CRAUDITOR', '20'), --COUNTY AUDITOR
  ('office_code', 'DG59', '20'), --COUNTY AUDITOR
  ('office_code', 'FE2', '20'), --COUNTY AUDITOR
  ('office_code', 'GA8', '20'), --COUNTY AUDITOR
  ('office_code', 'GR109', '20'), --COUNTY AUDITOR
  ('office_code', 'IS-AU-1', '20'), --COUNTY AUDITOR
  ('office_code', 'LI81', '20'), --COUNTY AUDITOR
  ('office_code', 'PE36', '20'), --COUNTY AUDITOR
  ('office_code', 'AS3', '22'), --COUNTY CLERK
  ('office_code', 'CRCLERK', '22'), --COUNTY CLERK
  ('office_code', 'DG60', '22'), --COUNTY CLERK
  ('office_code', 'FE3', '22'), --COUNTY CLERK
  ('office_code', 'GA9', '22'), --COUNTY CLERK
  ('office_code', 'GR110', '22'), --COUNTY CLERK
  ('office_code', 'IS-CL-1', '22'), --COUNTY CLERK
  ('office_code', 'LI82', '22'), --COUNTY CLERK
  ('office_code', 'PE37', '22'), --COUNTY CLERK
  ('office_code', 'AS6', '23'), --COUNTY COMMISSIONER
  ('office_code', 'DG54', '23'), --COUNTY COMMISSIONER
  ('office_code', 'FE5', '23'), --COUNTY COMMISSIONER
  ('office_code', 'GA12', '23'), --COUNTY COMMISSIONER
  ('office_code', 'GR113', '23'), --COUNTY COMMISSIONER
  ('office_code', 'LI79', '23'), --COUNTY COMMISSIONER
  ('office_code', 'PE39', '23'), --COUNTY COMMISSIONER
  ('office_code', 'TCCC-3', '23'), --COUNTY COMMISSIONER
  ('office_code', 'CCM-3', '23'), --COUNTY COMMISSIONER DIST. NO.
  ('office_code', 'AD-CC-3', '23'), --COUNTY COMMISSIONER DISTRICT
  ('office_code', 'CC-3', '23'), --COUNTY COMMISSIONER DISTRICT
  ('office_code', 'IS-CC-3', '23'), --COUNTY COMMISSIONER, DISTRICT
  ('office_code', 'YACOMMD3', '23'), --COUNTY COMMISSIONER; DISTRICT 3
  ('office_code', 'GR114', '24'), --COUNTY CORONER
  ('office_code', 'IS-CR-1', '24'), --COUNTY CORONER
  ('office_code', 'PI-CC1', '25'), --COUNTY COUNCIL NO.
  ('office_code', 'PI-CC5', '25'), --COUNTY COUNCIL NO.
  ('office_code', 'PI-CC7', '25'), --COUNTY COUNCIL NO.
  ('office_code', 'CRPROSECUT', '26'), --COUNTY PROSECUTING ATTORNEY
  ('office_code', 'LI83', '26'), --COUNTY PROSECUTING ATTORNEY
  ('office_code', 'AS7', '26'), --COUNTY PROSECUTOR
  ('office_code', 'GA13', '26'), --COUNTY PROSECUTOR
  ('office_code', 'GR115', '26'), --COUNTY PROSECUTOR
  ('office_code', 'IS-PA-1', '26'), --COUNTY PROSECUTOR
  ('office_code', 'PE41', '26'), --COUNTY PROSECUTOR
  ('office_code', 'AS8', '27'), --COUNTY SHERIFF
  ('office_code', 'CRSHERIFF', '27'), --COUNTY SHERIFF
  ('office_code', 'FE8', '27'), --COUNTY SHERIFF
  ('office_code', 'GA14', '27'), --COUNTY SHERIFF
  ('office_code', 'GR116', '27'), --COUNTY SHERIFF
  ('office_code', 'IS-SH-1', '27'), --COUNTY SHERIFF
  ('office_code', 'LI84', '27'), --COUNTY SHERIFF
  ('office_code', 'PE42', '27'), --COUNTY SHERIFF
  ('office_code', 'AS9', '28'), --COUNTY TREASURER
  ('office_code', 'CRTREASURE', '28'), --COUNTY TREASURER
  ('office_code', 'DG53', '28'), --COUNTY TREASURER
  ('office_code', 'FE9', '28'), --COUNTY TREASURER
  ('office_code', 'GA15', '28'), --COUNTY TREASURER
  ('office_code', 'GR117', '28'), --COUNTY TREASURER
  ('office_code', 'IS-TR-1', '28'), --COUNTY TREASURER
  ('office_code', 'LI85', '28'), --COUNTY TREASURER
  ('office_code', 'PE43', '28'), --COUNTY TREASURER
  ('office_code', 'CTYW-6', ''), --DIRECTOR OF COMMUNITY DEVELOPMENT
  ('office_code', 'CDC1', '17'), --DISTRICT COURT
  ('office_code', 'CDC2', '17'), --DISTRICT COURT
  ('office_code', 'AS10', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'BC-JDG1', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'BC-JDG2', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'BC-JDG3', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'BC-JDG4', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'BC-JDG5', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'CCDC-1', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'CCDC-2', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'CCDC-3', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'CH38', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'CH39', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'CU21', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'DCJ', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'DCJ-1', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'DG57', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'FE10', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'FR2100', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'GA16', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'GR118', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'GR119', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'IS-DC-J1', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'KS-LDC', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'KS-UDC', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'LI87', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'PA59', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'PA60', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'PE44', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SC-DCJ', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SCDJUDGE', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SJ-DCJ', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SP243', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SP244', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SP245', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SP246', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SP247', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SP248', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SP249', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'SP250', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'TCDCJ-1', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'TCDCJ-2', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'TCDCJ-3', '17'), --DISTRICT COURT JUDGE
  ('office_code', 'WL-DCFT', '17'), --DISTRICT COURT JUDGE - FULL TIME
  ('office_code', 'WL-DCPT', '17'), --DISTRICT COURT JUDGE - PART TIME
  ('office_code', 'GR1200', '17'), --DISTRICT COURT JUDGE 3
  ('office_code', 'KPDC1', '17'), --DISTRICT COURT JUDGE DEPARTMENT
  ('office_code', 'KPDC2', '17'), --DISTRICT COURT JUDGE DEPARTMENT
  ('office_code', 'KPDC3', '17'), --DISTRICT COURT JUDGE DEPARTMENT
  ('office_code', 'KPDC4', '17'), --DISTRICT COURT JUDGE DEPARTMENT
  ('office_code', 'SKDCJ1', '17'), --DISTRICT COURT JUDGE POSITION
  ('office_code', 'SKDCJ2', '17'), --DISTRICT COURT JUDGE POSITION
  ('office_code', 'SKDCJ3', '17'), --DISTRICT COURT JUDGE POSITION
  ('office_code', 'WM2021', '17'), --DISTRICT COURT JUDGE POSITION
  ('office_code', 'WM2022', '17'), --DISTRICT COURT JUDGE POSITION
  ('office_code', 'AD-DCT-1', '17'), --DISTRICT COURT JUDGE POSITION 1
  ('office_code', 'AD-DCT-2', '17'), --DISTRICT COURT JUDGE POSITION 2
  ('office_code', 'JUDGE', '17'), --DISTRICT COURT JUDGE POSTION 1
  ('office_code', 'CR-DCJ-1', '17'), --DISTRICT COURT JUDGE, DEPARTMENT NO.
  ('office_code', 'CR-DCJ-2', '17'), --DISTRICT COURT JUDGE, DEPARTMENT NO.
  ('office_code', 'CR-DCJ-3', '17'), --DISTRICT COURT JUDGE, DEPARTMENT NO.
  ('office_code', 'CR-DCJ-4', '17'), --DISTRICT COURT JUDGE, DEPARTMENT NO.
  ('office_code', 'CR-DCJ-5', '17'), --DISTRICT COURT JUDGE, DEPARTMENT NO.
  ('office_code', 'CR-DCJ-6', '17'), --DISTRICT COURT JUDGE, DEPARTMENT NO.
  ('office_code', 'PI-DC1', '17'), --DISTRICT COURT NO.
  ('office_code', 'PI-DC2', '17'), --DISTRICT COURT NO.
  ('office_code', 'PI-DC3', '17'), --DISTRICT COURT NO.
  ('office_code', 'PI-DC4', '17'), --DISTRICT COURT NO.
  ('office_code', 'PI-DC5', '17'), --DISTRICT COURT NO.
  ('office_code', 'PI-DC6', '17'), --DISTRICT COURT NO.
  ('office_code', 'PI-DC7', '17'), --DISTRICT COURT NO.
  ('office_code', 'PI-DC8', '17'), --DISTRICT COURT NO.
  ('office_code', 'LE-DC-1', '17'), --DISTRICT CT JUDGE DEPT
  ('office_code', 'LE-DC-2', '17'), --DISTRICT CT JUDGE DEPT
  ('office_code', 'LE-CC1-1', '30'), --FREEHOLDER DIST 1 POSITION
  ('office_code', 'LE-CC1-2', '30'), --FREEHOLDER DIST 1 POSITION
  ('office_code', 'LE-CC1-3', '30'), --FREEHOLDER DIST 1 POSITION
  ('office_code', 'LE-CC1-4', '30'), --FREEHOLDER DIST 1 POSITION
  ('office_code', 'LE-CC1-5', '30'), --FREEHOLDER DIST 1 POSITION
  ('office_code', 'LE-CC2-1', '30'), --FREEHOLDER DIST 2 POSITION
  ('office_code', 'LE-CC2-2', '30'), --FREEHOLDER DIST 2 POSITION
  ('office_code', 'LE-CC2-3', '30'), --FREEHOLDER DIST 2 POSITION
  ('office_code', 'LE-CC2-4', '30'), --FREEHOLDER DIST 2 POSITION
  ('office_code', 'LE-CC2-5', '30'), --FREEHOLDER DIST 2 POSITION
  ('office_code', 'LE-CC3-1', '30'), --FREEHOLDER DIST 3 POSITION
  ('office_code', 'LE-CC3-2', '30'), --FREEHOLDER DIST 3 POSITION
  ('office_code', 'LE-CC3-3', '30'), --FREEHOLDER DIST 3 POSITION
  ('office_code', 'LE-CC3-4', '30'), --FREEHOLDER DIST 3 POSITION
  ('office_code', 'LE-CC3-5', '30'), --FREEHOLDER DIST 3 POSITION
  ('office_code', 'OKDCJ1', '17'), --JUDGE
  ('office_code', 'OKDCJ2', '17'), --JUDGE
  ('office_code', 'CRT-D1', '17'), --JUDGE - DISTRICT COURT
  ('office_code', 'CRT-D2', '17'), --JUDGE - DISTRICT COURT
  ('office_code', '5114', '15'), --JUDGE POSITION
  ('office_code', '5117', '15'), --JUDGE POSITION
  ('office_code', '5131', '15'), --JUDGE POSITION
  ('office_code', '5213', '15'), --JUDGE POSITION
  ('office_code', '5222', '15'), --JUDGE POSITION
  ('office_code', '5231', '15'), --JUDGE POSITION
  ('office_code', '5311', '15'), --JUDGE POSITION
  ('office_code', '5332', '15'), --JUDGE POSITION
  ('office_code', '6035', '16'), --JUDGE POSITION
  ('office_code', '7042', '16'), --JUDGE POSITION
  ('office_code', '7043', '16'), --JUDGE POSITION
  ('office_code', '7085', '16'), --JUDGE POSITION
  ('office_code', '7143', '16'), --JUDGE POSITION
  ('office_code', '7232', '16'), --JUDGE POSITION
  ('office_code', '7292', '16'), --JUDGE POSITION
  ('office_code', '7409', '16'), --JUDGE POSITION
  ('office_code', '7422', '16'), --JUDGE POSITION
  ('office_code', '7438', '16'), --JUDGE POSITION
  ('office_code', '7453', '16'), --JUDGE POSITION
  ('office_code', '7631', '16'), --JUDGE POSITION
  ('office_code', '7810', '16'), --JUDGE POSITION
  ('office_code', 'CASDC1', '17'), --JUDGE POSITION
  ('office_code', 'EVEDC1', '17'), --JUDGE POSITION
  ('office_code', 'EVEDC2', '17'), --JUDGE POSITION
  ('office_code', 'EVRDC1', '17'), --JUDGE POSITION
  ('office_code', 'EVRDC2', '17'), --JUDGE POSITION
  ('office_code', 'NEJC1', '17'), --JUDGE POSITION
  ('office_code', 'NEJC2', '17'), --JUDGE POSITION
  ('office_code', 'NEJC3', '17'), --JUDGE POSITION
  ('office_code', 'NEJC4', '17'), --JUDGE POSITION
  ('office_code', 'NEJC5', '17'), --JUDGE POSITION
  ('office_code', 'NEJC6', '17'), --JUDGE POSITION
  ('office_code', 'NEJC7', '17'), --JUDGE POSITION
  ('office_code', 'SODC1', '17'), --JUDGE POSITION
  ('office_code', 'SODC2', '17'), --JUDGE POSITION
  ('office_code', 'SODC3', '17'), --JUDGE POSITION
  ('office_code', 'JE-1900', '17'), --JUDGE POSITION NO. 1
  ('office_code', 'KI7090', '17'), --JUDGE POSITION NO. 1
  ('office_code', 'KI7110', '17'), --JUDGE POSITION NO. 1
  ('office_code', 'KI7170', '17'), --JUDGE POSITION NO. 1
  ('office_code', 'KI7220', '17'), --JUDGE POSITION NO. 1
  ('office_code', 'KI7100', '17'), --JUDGE POSITION NO. 2
  ('office_code', 'KI7120', '17'), --JUDGE POSITION NO. 2
  ('office_code', 'KI7180', '17'), --JUDGE POSITION NO. 2
  ('office_code', 'KI7230', '17'), --JUDGE POSITION NO. 2
  ('office_code', 'KI7130', '17'), --JUDGE POSITION NO. 3
  ('office_code', 'KI7190', '17'), --JUDGE POSITION NO. 3
  ('office_code', 'KI7240', '17'), --JUDGE POSITION NO. 3
  ('office_code', 'KI7140', '17'), --JUDGE POSITION NO. 4
  ('office_code', 'KI7200', '17'), --JUDGE POSITION NO. 4
  ('office_code', 'KI7250', '17'), --JUDGE POSITION NO. 4
  ('office_code', 'KI7150', '17'), --JUDGE POSITION NO. 5
  ('office_code', 'KI7210', '17'), --JUDGE POSITION NO. 5
  ('office_code', 'KI7260', '17'), --JUDGE POSITION NO. 5
  ('office_code', 'KI7160', '17'), --JUDGE POSITION NO. 6
  ('office_code', '4002', '14'), --JUSTICE POSITION
  ('office_code', '4008', '14'), --JUSTICE POSITION
  ('office_code', '4009', '14'), --JUSTICE POSITION
  ('office_code', 'KT-ASSESS', '21'), --KLICKITAT COUNTY ASSESSOR
  ('office_code', 'KT-AUDITOR', '20'), --KLICKITAT COUNTY AUDITOR
  ('office_code', 'KT-CLERK', '22'), --KLICKITAT COUNTY CLERK
  ('office_code', 'KT-CC-2', '23'), --KLICKITAT COUNTY COMMISSIONER
  ('office_code', 'KT-EDCJ', '17'), --KLICKITAT COUNTY EAST DISTRICT COURT JUDGE
  ('office_code', 'KT-PROSEC', '26'), --KLICKITAT COUNTY PROSECUTING ATTORNEY
  ('office_code', 'KT-SHERIFF', '27'), --KLICKITAT COUNTY SHERIFF
  ('office_code', 'KT-TREAS', '28'), --KLICKITAT COUNTY TREASURER
  ('office_code', 'KT-WDCJ', '17'), --KLICKITAT COUNTY WEST DISTRICT COURT JUDGE
  ('office_code', 'KI11350', '18'), --MUNICIPAL COURT JUDGE POSITION NO. 1
  ('office_code', 'KI11360', '18'), --MUNICIPAL COURT JUDGE POSITION NO. 2
  ('office_code', 'KI11370', '18'), --MUNICIPAL COURT JUDGE POSITION NO. 3
  ('office_code', 'KI11380', '18'), --MUNICIPAL COURT JUDGE POSITION NO. 4
  ('office_code', 'KI11390', '18'), --MUNICIPAL COURT JUDGE POSITION NO. 5
  ('office_code', 'KI11400', '18'), --MUNICIPAL COURT JUDGE POSITION NO. 6
  ('office_code', 'KI11410', '18'), --MUNICIPAL COURT JUDGE POSITION NO. 7
  ('office_code', 'BC-PA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'CH36', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'CPA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'CTYW-3', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'DG55', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'FE7', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'FR1600', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'KI3010', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'KPPA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'KS-PA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'LE-PR', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'OKPA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'PA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'PA56', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'PI-PA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'PROS', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'PROS ATT', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'SC-ATTY', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'SCPATTNY', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'SJ-PA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'SKPRO', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'SP260', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'TCPA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'WL-PA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'WM1410', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'YAPA', '26'), --PROSECUTING ATTORNEY
  ('office_code', 'AD-PA-1', '26'), --PROSECUTOR
  ('office_code', 'CU18', '26'), --PROSECUTOR
  ('office_code', 'PR-1', '26'), --PROSECUTOR
  ('office_code', 'PRO', '26'), --PROSECUTOR
  ('office_code', 'AS15', '48'), --PUBLIC UTILITY COMMISSIONER
  ('office_code', 'AS16', '48'), --PUBLIC UTILITY COMMISSIONER
  ('office_code', 'FE22', '48'), --PUBLIC UTILITY COMMISSIONER
  ('office_code', 'LI105', '48'), --PUBLIC UTILITY COMMISSIONER
  ('office_code', 'PA96', '48'), --PUBLIC UTILITY COMMISSIONER
  ('office_code', 'PE77', '48'), --PUBLIC UTILITY COMMISSIONER
  ('office_code', 'CH159', '48'), --PUBLIC UTILITY DIST COMMISSIONER
  ('office_code', 'CH162', '48'), --PUBLIC UTILITY DIST COMMISSIONER B
  ('office_code', 'PUD01', '48'), --PUD COMM (1)
  ('office_code', 'AD-SH-1', '27'), --SHERIFF
  ('office_code', 'BC-SHF', '27'), --SHERIFF
  ('office_code', 'CH37', '27'), --SHERIFF
  ('office_code', 'CSH', '27'), --SHERIFF
  ('office_code', 'CTYW-4', '27'), --SHERIFF
  ('office_code', 'CU19', '27'), --SHERIFF
  ('office_code', 'DG56', '27'), --SHERIFF
  ('office_code', 'FR1580', '27'), --SHERIFF
  ('office_code', 'JE-1150', '27'), --SHERIFF
  ('office_code', 'KPSHERIFF', '27'), --SHERIFF
  ('office_code', 'KS-SH', '27'), --SHERIFF
  ('office_code', 'LE-SH', '27'), --SHERIFF
  ('office_code', 'OKSH', '27'), --SHERIFF
  ('office_code', 'PA57', '27'), --SHERIFF
  ('office_code', 'SH-1', '27'), --SHERIFF
  ('office_code', 'SHER', '27'), --SHERIFF
  ('office_code', 'SHER001', '27'), --SHERIFF
  ('office_code', 'SHF', '27'), --SHERIFF
  ('office_code', 'SHRF1', '27'), --SHERIFF
  ('office_code', 'SJ-SHER', '27'), --SHERIFF
  ('office_code', 'SKSHE', '27'), --SHERIFF
  ('office_code', 'SP259', '27'), --SHERIFF
  ('office_code', 'STSHRF', '27'), --SHERIFF
  ('office_code', 'TCSH', '27'), --SHERIFF
  ('office_code', 'WL-SH', '27'), --SHERIFF
  ('office_code', 'YASHERIFF', '27'), --SHERIFF
  ('office_code', '8011', '13'), --STATE REPRESENTATIVE
  ('office_code', '8012', '13'), --STATE REPRESENTATIVE
  ('office_code', '8021', '13'), --STATE REPRESENTATIVE
  ('office_code', '8022', '13'), --STATE REPRESENTATIVE
  ('office_code', '8031', '13'), --STATE REPRESENTATIVE
  ('office_code', '8032', '13'), --STATE REPRESENTATIVE
  ('office_code', '8041', '13'), --STATE REPRESENTATIVE
  ('office_code', '8042', '13'), --STATE REPRESENTATIVE
  ('office_code', '8051', '13'), --STATE REPRESENTATIVE
  ('office_code', '8052', '13'), --STATE REPRESENTATIVE
  ('office_code', '8061', '13'), --STATE REPRESENTATIVE
  ('office_code', '8062', '13'), --STATE REPRESENTATIVE
  ('office_code', '8071', '13'), --STATE REPRESENTATIVE
  ('office_code', '8072', '13'), --STATE REPRESENTATIVE
  ('office_code', '8081', '13'), --STATE REPRESENTATIVE
  ('office_code', '8082', '13'), --STATE REPRESENTATIVE
  ('office_code', '8091', '13'), --STATE REPRESENTATIVE
  ('office_code', '8092', '13'), --STATE REPRESENTATIVE
  ('office_code', '8101', '13'), --STATE REPRESENTATIVE
  ('office_code', '8102', '13'), --STATE REPRESENTATIVE
  ('office_code', '8111', '13'), --STATE REPRESENTATIVE
  ('office_code', '8112', '13'), --STATE REPRESENTATIVE
  ('office_code', '8121', '13'), --STATE REPRESENTATIVE
  ('office_code', '8122', '13'), --STATE REPRESENTATIVE
  ('office_code', '8131', '13'), --STATE REPRESENTATIVE
  ('office_code', '8132', '13'), --STATE REPRESENTATIVE
  ('office_code', '8141', '13'), --STATE REPRESENTATIVE
  ('office_code', '8142', '13'), --STATE REPRESENTATIVE
  ('office_code', '8151', '13'), --STATE REPRESENTATIVE
  ('office_code', '8152', '13'), --STATE REPRESENTATIVE
  ('office_code', '8161', '13'), --STATE REPRESENTATIVE
  ('office_code', '8162', '13'), --STATE REPRESENTATIVE
  ('office_code', '8171', '13'), --STATE REPRESENTATIVE
  ('office_code', '8172', '13'), --STATE REPRESENTATIVE
  ('office_code', '8181', '13'), --STATE REPRESENTATIVE
  ('office_code', '8182', '13'), --STATE REPRESENTATIVE
  ('office_code', '8191', '13'), --STATE REPRESENTATIVE
  ('office_code', '8192', '13'), --STATE REPRESENTATIVE
  ('office_code', '8201', '13'), --STATE REPRESENTATIVE
  ('office_code', '8202', '13'), --STATE REPRESENTATIVE
  ('office_code', '8211', '13'), --STATE REPRESENTATIVE
  ('office_code', '8212', '13'), --STATE REPRESENTATIVE
  ('office_code', '8221', '13'), --STATE REPRESENTATIVE
  ('office_code', '8222', '13'), --STATE REPRESENTATIVE
  ('office_code', '8231', '13'), --STATE REPRESENTATIVE
  ('office_code', '8232', '13'), --STATE REPRESENTATIVE
  ('office_code', '8241', '13'), --STATE REPRESENTATIVE
  ('office_code', '8242', '13'), --STATE REPRESENTATIVE
  ('office_code', '8251', '13'), --STATE REPRESENTATIVE
  ('office_code', '8252', '13'), --STATE REPRESENTATIVE
  ('office_code', '8261', '13'), --STATE REPRESENTATIVE
  ('office_code', '8262', '13'), --STATE REPRESENTATIVE
  ('office_code', '8271', '13'), --STATE REPRESENTATIVE
  ('office_code', '8272', '13'), --STATE REPRESENTATIVE
  ('office_code', '8281', '13'), --STATE REPRESENTATIVE
  ('office_code', '8282', '13'), --STATE REPRESENTATIVE
  ('office_code', '8291', '13'), --STATE REPRESENTATIVE
  ('office_code', '8292', '13'), --STATE REPRESENTATIVE
  ('office_code', '8301', '13'), --STATE REPRESENTATIVE
  ('office_code', '8302', '13'), --STATE REPRESENTATIVE
  ('office_code', '8311', '13'), --STATE REPRESENTATIVE
  ('office_code', '8312', '13'), --STATE REPRESENTATIVE
  ('office_code', '8321', '13'), --STATE REPRESENTATIVE
  ('office_code', '8322', '13'), --STATE REPRESENTATIVE
  ('office_code', '8331', '13'), --STATE REPRESENTATIVE
  ('office_code', '8332', '13'), --STATE REPRESENTATIVE
  ('office_code', '8341', '13'), --STATE REPRESENTATIVE
  ('office_code', '8342', '13'), --STATE REPRESENTATIVE
  ('office_code', '8351', '13'), --STATE REPRESENTATIVE
  ('office_code', '8352', '13'), --STATE REPRESENTATIVE
  ('office_code', '8361', '13'), --STATE REPRESENTATIVE
  ('office_code', '8362', '13'), --STATE REPRESENTATIVE
  ('office_code', '8371', '13'), --STATE REPRESENTATIVE
  ('office_code', '8372', '13'), --STATE REPRESENTATIVE
  ('office_code', '8381', '13'), --STATE REPRESENTATIVE
  ('office_code', '8382', '13'), --STATE REPRESENTATIVE
  ('office_code', '8391', '13'), --STATE REPRESENTATIVE
  ('office_code', '8392', '13'), --STATE REPRESENTATIVE
  ('office_code', '8401', '13'), --STATE REPRESENTATIVE
  ('office_code', '8402', '13'), --STATE REPRESENTATIVE
  ('office_code', '8411', '13'), --STATE REPRESENTATIVE
  ('office_code', '8412', '13'), --STATE REPRESENTATIVE
  ('office_code', '8421', '13'), --STATE REPRESENTATIVE
  ('office_code', '8422', '13'), --STATE REPRESENTATIVE
  ('office_code', '8431', '13'), --STATE REPRESENTATIVE
  ('office_code', '8432', '13'), --STATE REPRESENTATIVE
  ('office_code', '8441', '13'), --STATE REPRESENTATIVE
  ('office_code', '8442', '13'), --STATE REPRESENTATIVE
  ('office_code', '8451', '13'), --STATE REPRESENTATIVE
  ('office_code', '8452', '13'), --STATE REPRESENTATIVE
  ('office_code', '8461', '13'), --STATE REPRESENTATIVE
  ('office_code', '8462', '13'), --STATE REPRESENTATIVE
  ('office_code', '8471', '13'), --STATE REPRESENTATIVE
  ('office_code', '8472', '13'), --STATE REPRESENTATIVE
  ('office_code', '8481', '13'), --STATE REPRESENTATIVE
  ('office_code', '8482', '13'), --STATE REPRESENTATIVE
  ('office_code', '8491', '13'), --STATE REPRESENTATIVE
  ('office_code', '8492', '13'), --STATE REPRESENTATIVE
  ('office_code', '8060', '12'), --STATE SENATOR
  ('office_code', '8070', '12'), --STATE SENATOR
  ('office_code', '8080', '12'), --STATE SENATOR
  ('office_code', '8130', '12'), --STATE SENATOR
  ('office_code', '8150', '12'), --STATE SENATOR
  ('office_code', '8210', '12'), --STATE SENATOR
  ('office_code', '8260', '12'), --STATE SENATOR
  ('office_code', '8290', '12'), --STATE SENATOR
  ('office_code', '8300', '12'), --STATE SENATOR
  ('office_code', '8310', '12'), --STATE SENATOR
  ('office_code', '8320', '12'), --STATE SENATOR
  ('office_code', '8330', '12'), --STATE SENATOR
  ('office_code', '8340', '12'), --STATE SENATOR
  ('office_code', '8350', '12'), --STATE SENATOR
  ('office_code', '8360', '12'), --STATE SENATOR
  ('office_code', '8370', '12'), --STATE SENATOR
  ('office_code', '8380', '12'), --STATE SENATOR
  ('office_code', '8390', '12'), --STATE SENATOR
  ('office_code', '8420', '12'), --STATE SENATOR
  ('office_code', '8430', '12'), --STATE SENATOR
  ('office_code', '8440', '12'), --STATE SENATOR
  ('office_code', '8450', '12'), --STATE SENATOR
  ('office_code', '8460', '12'), --STATE SENATOR
  ('office_code', '8470', '12'), --STATE SENATOR
  ('office_code', '8480', '12'), --STATE SENATOR
  ('office_code', 'PI-TACMUN1', '18'), --TACOMA MUNICIPAL COURT POS.
  ('office_code', 'PI-TACMUN2', '18'), --TACOMA MUNICIPAL COURT POS.
  ('office_code', 'PI-TACMUN3', '18'), --TACOMA MUNICIPAL COURT POS.
  ('office_code', 'AD-TR-1', '28'), --TREASURER
  ('office_code', 'BC-TREAS', '28'), --TREASURER
  ('office_code', 'CH40', '28'), --TREASURER
  ('office_code', 'CTR', '28'), --TREASURER
  ('office_code', 'CTYW-5', '28'), --TREASURER
  ('office_code', 'CU20', '28'), --TREASURER
  ('office_code', 'FR1590', '28'), --TREASURER
  ('office_code', 'JE-1200', '28'), --TREASURER
  ('office_code', 'KPTREAS', '28'), --TREASURER
  ('office_code', 'KS-TR', '28'), --TREASURER
  ('office_code', 'LE-TR', '28'), --TREASURER
  ('office_code', 'OKTR', '28'), --TREASURER
  ('office_code', 'PA58', '28'), --TREASURER
  ('office_code', 'SCTREA', '28'), --TREASURER
  ('office_code', 'SJ-TREA', '28'), --TREASURER
  ('office_code', 'SKTRE', '28'), --TREASURER
  ('office_code', 'SP257', '28'), --TREASURER
  ('office_code', 'TCTR', '28'), --TREASURER
  ('office_code', 'TR-1', '28'), --TREASURER
  ('office_code', 'TREA', '28'), --TREASURER
  ('office_code', 'TREAS1', '28'), --TREASURER
  ('office_code', 'TREASURER', '28'), --TREASURER
  ('office_code', 'TRS', '28'), --TREASURER
  ('office_code', 'WL-TR', '28'), --TREASURER
  ('office_code', 'YATREAS', '28'), --TREASURER
  ('office_code', 'PUD-1', '48'), --UTILITY COMMISSIONER
  ('office_code', 'DC1', '17'), --YAKIMA COUNTY DISTRICT COURT POSITION 1
  ('office_code', 'DC2', '17'), --YAKIMA COUNTY DISTRICT COURT POSITION 2
  ('office_code', 'DC3', '17'), --YAKIMA COUNTY DISTRICT COURT POSITION 3
  ('office_code', 'DC4', '17'); --YAKIMA COUNTY DISTRICT COURT POSITION 4
