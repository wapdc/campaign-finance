CREATE TABLE crosswalk (
  crosswalk_id SERIAL PRIMARY KEY,
  field_name VARCHAR(50),
  internal_value VARCHAR(255),
  external_value VARCHAR(255)
);