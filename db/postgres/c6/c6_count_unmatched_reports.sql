CREATE OR REPLACE FUNCTION c6_count_unmatched_reports() returns int language sql as $$
select reports from
   (select count(1) as reports   from c6_sponsor s
  join c6_reports r on s.sponsor_id = r.sponsor_id
  join c6_identified_entity ie on r.repno = ie.repno
where r.imaging_status = 1
  and r.superseded is null
  and s.email is not null
  and ie.filer_id is null
  and ie.ballot_name is null
  and cast(r.election_year as int) > (extract(year  from now()) - 2)) as result
$$;
