CREATE OR REPLACE FUNCTION c6_count_sponsor_requests() returns int language sql as $$
select count(1) from c6_sponsor_request as result
$$;

