create table c6_candidacy
(
    candidacy_id integer primary key not null
);
comment on table c6_candidacy is
    'This table is just to track changes to data on C6 reports and identified entity tables that will result in a need to update the campaign_finance_summary dataset for those candidacies';

alter table c6_reports add constraint pk_repno_pk primary key (repno);
alter table c6_identified_entity add constraint pk_ident primary key (ident);
