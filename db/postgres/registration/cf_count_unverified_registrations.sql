CREATE OR REPLACE FUNCTION cf_count_unverified_registrations() returns int language sql as $$
select registrations from (select count(1) as registrations from registration r
 inner join committee c on r.committee_id = c.committee_id and c.registration_id = r.registration_id
where (r.verified = false OR r.verified IS NULL)) as result
$$;
