/*
  The alias is the application specific unique key to the file and specifically
  must start with the registration id which allows a single registration to never have a
  duplicate alias but the same alias (sans the registration id) can exist in many registrations.

  The alias is used for all references to the file that are made by the
  application and interpreting and handling the file_service_url is application
  specific behavior.

  The file_service_url needs to begin with a proper uri scheme as defined in
  RFC 3986. see: https://tools.ietf.org/html/rfc3986#section-3.1
  Must start with an alpha followed by zero or more alpha, digit, +, -, or .,
  followed by :// and at least one non slash character.
 */
CREATE TABLE registration_attachment(
    alias VARCHAR(200) PRIMARY KEY NOT NULL,
    file_service_url VARCHAR NOT NULL,
    registration_id INT NOT NULL,
    description VARCHAR,
    access_mode VARCHAR NOT NULL,
    document_type VARCHAR,
    CHECK (alias LIKE concat(cast(registration_id as VARCHAR), '/_%')),
    CHECK (file_service_url ~ '^[a-zA-Z][a-zA-Z0-9+-.]{0,20}://[^/].*'),
    FOREIGN KEY (registration_id) REFERENCES registration(registration_id) ON DELETE CASCADE
);