delete
from committee c
where c.start_year not in
      (
          select *
          from
              (
                  select MAX(start_year)
                  from
                      committee
                  where c.pac_type = 'surplus'
                    and filer_id = c.filer_id
                  group by c.filer_id
              ) temp
      ) and c1_sync IS NOT NULL AND pac_type = 'surplus';