update committee b set category = v.category from (select c.committee_id, c1.type,
  case
    when c1.type = 'S' then 'Single Election'
    when c1.type = 'B' then 'Business'
    when c1.type = 'D' then 'Democratic Bona Fide'
    when c1.type = 'DC' then 'Democratic Caucus'
    when c1.type = 'DO' then 'Democratic Other'
    when c1.type = 'E' then 'Education'
    when c1.type = 'P' then 'Initiative'
    when c1.type = 'I' then 'Issue'
    when c1.type = 'L' then 'Local Issue'
    when c1.type = 'M' then 'Minor Party'
    when c1.type = 'O' then 'Other'
    when c1.type = 'R' then 'Republican Bona Fide'
    when c1.type = 'RC' then 'Republican Caucus'
    when c1.type = 'RO' then 'Republican Other'
    when c1.type = 'U' then 'Union'
    when c1.type = 'LB' then 'Libertarian Bona Fide'
  end category
from c1 join committee c ON c1.filer_id = c.filer_id
  and cast(c1.election_year as integer) between c.start_year and COALESCE(c.end_year, 2020)
  and c1.type is not null) v
where b.committee_id = v.committee_id;

-- default in party committees
update committee c set category = v.case_category from (select committee_id,
  case
    WHEN pac_type = 'bonafide' AND party = 3 THEN 'Democratic Bona Fide'
    WHEN pac_type = 'bonafide' AND party = 15 THEN 'Republican Bona Fide'
    WHEN pac_type = 'bonafide' AND party NOT IN ( 3, 15 ) THEN 'Minor Party'
    WHEN pac_type = 'caucus' AND party = 3 THEN 'Democratic Caucus'
    WHEN pac_type = 'caucus' AND party = 15 THEN 'Republican Caucus'
    WHEN pac_type = 'pac' AND party = 3 THEN 'Democratic Other'
    WHEN pac_type = 'pac' AND party = 15 THEN 'Republican Other'
  end case_category
from committee where category is null and party is not null and pac_type in ('pac','bonafide', 'caucus')) v
where c.committee_id = v.committee_id;


-- Default in proposal committees.
update committee a set category = b.case_category from (select v.committee_id, p.proposal_type, j.category,
  case
    WHEN j.category = 7 AND COALESCE(v.continuing, false) = false THEN 'Education'
    WHEN j.category = 1 AND COALESCE(v.continuing, false) = false THEN 'Initiative'
    WHEN j.category != 1 AND COALESCE(v.continuing, false) = false THEN 'Local Issue'
    WHEN p.proposal_type = 'local' THEN 'Local Issue'
    WHEN p.proposal_type = 'statewide' THEN 'Initiative'
  end case_category
from (select c.committee_id, c.continuing, max(cr.relation_id) as relation_id from committee c join committee_relation cr ON c.committee_id = cr.committee_id
   where relation_type = 'proposal'
   group by c.committee_id having count(1) = 1) v
    join proposal p on v.relation_id=p.proposal_id
    left join jurisdiction j ON p.jurisdiction_id = j.jurisdiction_id and j.category is not null) b
  where a.committee_id = b.committee_id and (b.proposal_type in ('local', 'statewide') or b.case_category is not null);
