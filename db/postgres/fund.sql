drop table if exists fund cascade;
create table fund (
  fund_id serial primary key,
  fund_type text,
  committee_id int,
  filer_id text,
  election_code text,
  updated_at timestamp(0) with time zone default now()
);
create index idx_fund_fund_type on fund(fund_type);
create index idx_fund_committee_id on fund(committee_id);
create index idx_fund_filer_id on fund(filer_id);
create index idx_fund_election_code on fund(election_code);
