update candidacy set committee_id = v.committee_id
from (select ca.candidacy_id, c.committee_id
  from committee c
    join candidacy ca on c.filer_id = ca.filer_id
      and c.election_code = ca.election_code
      and c.committee_type = 'CA') v
where candidacy.candidacy_id = v.candidacy_id;