create table public.thresholds
(
    property TEXT NOT NULL PRIMARY KEY,
    amount NUMERIC(16,2),
    description TEXT
);

INSERT INTO public.thresholds (property, amount, description)
VALUES ('itemized_expenses', 50, 'Threshold for being required to itemize expenditures.'),
       ('itemized_contributions', 25, 'Threshold for reporting address of contributors.'),
       ('report_occupation', 100, 'Threshold for reporting the employer and occupation of contributors. '),
       ('report_auctions', 50, 'Threshold for auctions and low cost fundraiser reporting.'),
       ('report_anonymous', 300, 'Annual limit for anonymous contributions '),
       ('mini_reporting', 5000, 'Registration threshold to fall below for mini reporting ');
