--Surplus committee created in error. Committee authorization and api records moved in previous merge request
delete from committee where filer_id = 'GRIFDA 524' AND pac_type = 'surplus';

--Surplus committee created in error. Committee authorization and api records moved in previous merge request
--Candidate filer_id mistakenly set to surplus - this fixes that.
delete from committee where filer_id = 'DISTA-*225' AND pac_type = 'surplus';
UPDATE committee SET filer_id = 'DISTA--225' where filer_id = 'DISTA-*225' and pac_type = 'candidate';