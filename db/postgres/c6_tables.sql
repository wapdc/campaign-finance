create table if not exists c6
(
    repno               integer not null,
    sponsor_id          integer,
    rpt_date            timestamp(0) with time zone,
    rpt_type            varchar(10),
    sponsor_desc        varchar(100),
    election_year       varchar(4),
    sponsor_name        varchar(100),
    address             varchar(100),
    city                varchar(100),
    st                  varchar(2),
    zip                 varchar(10),
    total_unitemized    numeric(12, 2),
    total_cycle         numeric(12, 2),
    memo                varchar,
    total_this_report   numeric(12, 2),
    sponsor_location_id varchar(256)
);

create table if not exists c6_reports
(
    repno          int,
    superseded     int,
    sponsor_id     int,
    election_year  varchar(4),
    date_filed     timestamp(0) with time zone,
    rpt_type       varchar(50),
    how_filed      varchar(10),
    imaging_status int
);

create table if not exists c6_expenditure
(
    ident                   int,
    repno                   int,
    date_made               timestamp(0) with time zone,
    date_presented          timestamp(0) with time zone,
    vendor_name             varchar(75),
    addr                    varchar(50),
    city                    varchar(40),
    state                   varchar(2),
    zip                     varchar(10),
    expn_desc               varchar(84),
    amount                  decimal(12, 2),
    expenditure_location_id varchar(256)
);

create table if not exists c6_sponsor(
    sponsor_id int,
    name varchar(75),
    fname varchar(75),
    mi varchar(2),
    suffix varchar(50),
    addr varchar(77),
    city varchar(136),
    state varchar(2),
    zip varchar(10),
    email varchar(40),
    phone varchar(19),
    sponsor_desc varchar(1)
);

create table if not exists c6_identified_entity(
    ident int,
    repno int,
    cand_lname varchar(38),
    cand_fname varchar(38),
    office varchar(37),
    juris varchar(50),
    ballot_name varchar(100),
    ballot_num varchar(6),
    ballot_type varchar(10),
    spt_opp varchar(10),
    portion_of_amt decimal(12, 2),
    total_cycle decimal(12, 2),
    party varchar(50),
    filer_id varchar(12),
    offs1 varchar(2)
);

create table if not exists c6_source(
    ident int,
    repno int,
    date_rcvd timestamp(0) with time zone,
    amount decimal(12, 2),
    name varchar(75),
    fname varchar(29),
    mi varchar(2),
    addr varchar(50),
    city varchar(44),
    state varchar(2),
    zip varchar(10),
    occupation varchar(29),
    employer varchar(29),
    emp_city varchar(25),
    emp_state varchar(2),
    source_location_id varchar(256)
);

