drop table if exists loan cascade;
create table loan (
  transaction_id int primary key,
  loan_date date,
  due_date date,
  interest_rate numeric(6,2),
  payment_schedule text,
  liable_amount numeric (10,2),
  lender_endorser text,
  foreign key (transaction_id) references transaction(transaction_id) on delete cascade
);
