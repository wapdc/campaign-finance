drop table if exists report cascade;
create table report (
  report_id serial primary key,
  repno int,
  superseded_id int,
  superseded_repno int,
  report_type text,
  submitted_at timestamp(0) with time zone,
  fund_id int,
  period_start date,
  period_end date,
  record_type text,
  form_type text,
  election_year text,
  entity_code text,
  filer_name text,
  address1 text,
  address2 text,
  city text,
  state text,
  postcode text,
  office_code text,
  treasurer_name text,
  treasurer_date date,
  treasurer_phone text,
  final_report boolean,
  primary_election text,
  general_election text,
  candidate_name text,
  ind_exp boolean
);
create index idx_report_report_type on report(report_type);
create index idx_report_fund_id on report(fund_id);
create index idx_report_period_start on report(period_start);
