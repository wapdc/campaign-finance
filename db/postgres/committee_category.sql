drop function if exists committee_category(varchar, boolean);

create table committee_category (
  committee_category_id serial primary key,
  name text not null,
  pac_type varchar(16),
  bonafide_type varchar(64),
  party_id int, -- Value of -1 represents "any other" party_id not specified in another committee_category row
  jurisdiction_category int, -- Value of -1 represents "any other" jurisdiction category not specified
  continuing boolean,
  staff_only boolean not null default false
);

insert into committee_category (pac_type, bonafide_type, party_id, jurisdiction_category, continuing, staff_only, name)
values ('candidate', null, null, null, false, false, 'Candidate'),
  ('surplus', null, null, null, true, false, 'Surplus'),
  ('caucus', null, 3, null, true, false, 'Democratic Caucus'),
  ('caucus', null, 15, null, true, false, 'Republican Caucus'),
  ('bonafide', 'county', 3, null, true, false, 'Democratic County Party'),
  ('bonafide', 'district', 3, null, true, false, 'Democratic Leg District Party'),
  ('bonafide', 'state', 3, null, true, false, 'Democratic State Party'),
  ('bonafide', 'county', 15, null, true, false, 'Republican County Party'),
  ('bonafide', 'district', 15, null, true, false, 'Republican Leg District Party'),
  ('bonafide', 'state', 15, null, true, false, 'Republican State Party'),
  ('bonafide', null, -1, null, true, false, 'Minor Party'), -- any other non-null party_id
  ('pac', null, null, 1, false, false, 'Statewide Ballot Measure'),
  ('pac', null, null, 7, false, false, 'Education'),
  ('pac', null, null, -1, false, false, 'Local Ballot Measure'), -- any other non-null jurisdiction_category
  ('pac', null, null, null, true, false, 'Continuing Committee'),
  ('pac', null, null, null, false, false, 'Single Election Committee'),
  (null, null, null, null, null, false, 'Other'),
  ('pac', null, 3, null, true, true, 'Democratic Caucus Affiliated'), -- not reachable programmatically, staff selection
  ('pac', null, 15, null, true, true, 'Republican Caucus Affiliated'), -- not reachable programmatically, staff selection
  ('pac', null, null, null, true, true, 'Continuing Committee (Business)'), -- not reachable programmatically, staff selection
  ('pac', null, null, null, true, true, 'Continuing Committee (Union)'), -- not reachable programmatically, staff selection
  ('pac', null, null, null, null, true, 'Local Ballot Measure'), -- staff selection option in addition to programmatic versions
  ('pac', null, null, null, null, true, 'Statewide Ballot Measure'); -- staff selection option in addition to programmatic versions

update committee set category = 'Single Election Committee' where category = 'Single Election';
update committee set category = 'Statewide Ballot Measure' where category = 'Initiative'; -- statewide measure? what about referenda?
update committee set category = 'Local Ballot Measure' where category = 'Local Issue';
update committee set category = 'Continuing Committee (Business)' where category = 'Business';
update committee set category = 'Continuing Committee (Union)' where category = 'Union';
update committee set category = 'Democratic State Party' where pac_type = 'bonafide' AND bonafide_type = 'state' and party = 3;
update committee set category = 'Republican State Party' where pac_type = 'bonafide' AND bonafide_type = 'state' and party = 15;
update committee set category = 'Democratic County Party' where pac_type = 'bonafide' AND bonafide_type = 'county' and party = 3;
update committee set category = 'Republican County Party' where pac_type = 'bonafide' AND bonafide_type = 'county' and party = 15;
update committee set category = 'Democratic Leg District Party' where pac_type = 'bonafide' AND bonafide_type = 'district' and party = 3;
update committee set party = 15 where committee_id in (3, 144) and pac_type = 'bonafide' and party is null;
update committee set category = 'Republican Leg District Party' where pac_type = 'bonafide' AND bonafide_type = 'district' and party = 15;
update committee set category = 'Democratic Caucus Affiliated' where committee_id in (8085, 10220, 16657); -- truman fund, kennedy fund, roosevelt fund (closed)
update committee set category = 'Republican Caucus Affiliated' where committee_id in (16019, 11224); -- reagan fund, the leadership council
update committee set category = 'Candidate' where pac_type = 'candidate';
update committee set category = 'Surplus' where pac_type = 'surplus';
