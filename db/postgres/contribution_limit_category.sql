create table contribution_limit_category (
  category_id serial not null primary key ,
  code varchar not null,
  name varchar not null
);

insert into contribution_limit_category (code, name)
values ('judicial', 'Judicial Candidates'),
       ('default', 'Default Candidates'),
       ('state-port', 'State & Port Candidates');