drop table if exists contribution cascade;
create table contribution (
  transaction_id int primary key,
  anonymous boolean NOT NULL DEFAULT TRUE,
  contact_key text,
  name text,
  address text,
  city text,
  state text,
  postcode text,
  occupation text,
  employer text,
  employer_address text,
  employer_city text,
  employer_state text,
  employer_postcode text,
  prim_gen text,
  contributor_type text,
  geocode_hash_key text,
  foreign key (transaction_id) references transaction(transaction_id) on delete cascade
);
create index idx_contribution_contributor_type on contribution(contributor_type);
create index idx_contribution_prim_gen on contribution(prim_gen);
