/**
 * This script will be used to create tables associated with the new project.
 */

\i party.sql
\i committee_authorization.sql
\i committee_contact.sql
\i committee_relation.sql
\i registration_change.sql
\i registration.sql
\i proposal.sql
\i committee_registration_draft.sql
\i committee_log.sql
\i committee_api_key.sql
