UPDATE jurisdiction_office SET partisan = false;
UPDATE jurisdiction_office SET partisan = true
WHERE offcode IN ('03', '04', '05', '06', '07', '08', '09', '11', '12', '13',
  '19', '20', '21', '22', '23', '24', '25', '26', '27', '28');
--CLALLAM EXCEPTIONS
UPDATE jurisdiction_office SET partisan = false
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id
           from jurisdiction_office jo
                  join jurisdiction j
                  on j.jurisdiction_id = jo.jurisdiction_id
           where county = '05' and
                 (jo.title = 'COUNTY ASSESSOR' OR jo.title = 'COUNTY AUDITOR' OR jo.title = 'COUNTY TREASURER'
                    OR jo.title = 'COUNTY SHERIFF' OR jo.title = 'COUNTY DIR OF COMM DEV'));
UPDATE jurisdiction_office SET partisan = true
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id
           from jurisdiction_office jo
                  join jurisdiction j
                  on j.jurisdiction_id = jo.jurisdiction_id
           where county = '05' and
                 (jo.title = 'COUNTY COMMISSIONER' OR jo.title = 'COUNTY PROSECUTOR' OR jo.title = 'COUNTY CORONER'));
--KING EXCEPTIONS
UPDATE jurisdiction_office SET partisan = false
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id
           from jurisdiction_office jo
                  join jurisdiction j
                  on j.jurisdiction_id = jo.jurisdiction_id
           where county = '17' and
                 (jo.title = 'COUNTY EXECUTIVE' OR jo.title = 'COUNTY COUNCIL MEMBER' OR jo.title = 'COUNTY ASSESSOR'
                    OR jo.title = 'COUNTY SHERIFF' OR jo.title = 'COUNTY DIR OF ELECTIONS'));
UPDATE jurisdiction_office SET partisan = true
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id
           from jurisdiction_office jo
                  join jurisdiction j
                  on j.jurisdiction_id = jo.jurisdiction_id
           where county = '17' and
                 (jo.title = 'COUNTY PROSECUTOR'));
--PIERCE EXCEPTIONS
UPDATE jurisdiction_office SET partisan = false
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id from jurisdiction_office jo join jurisdiction j on j.jurisdiction_id = jo.jurisdiction_id
           where county = '27' and
                 (jo.title = 'COUNTY ASSESSOR' OR jo.title = 'COUNTY AUDITOR' OR jo.title = 'COUNTY SHERIFF'
                    OR jo.title = 'TREASURER'));
UPDATE jurisdiction_office SET partisan = true
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id
           from jurisdiction_office jo
                  join jurisdiction j
                  on j.jurisdiction_id = jo.jurisdiction_id
           where county = '27' and
                 (jo.title = 'COUNTY EXECUTIVE' OR jo.title = 'COUNTY COUNCIL MEMBER' OR jo.title = 'COUNTY COMMISSIONER'));
--SAN JUAN EXCEPTIONS
UPDATE jurisdiction_office SET partisan = false
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id from jurisdiction_office jo join jurisdiction j on j.jurisdiction_id = jo.jurisdiction_id
           where county = '28' and
                 (jo.title = 'COUNTY COUNCIL MEMBER' OR jo.title = 'COUNTY COMMISSIONER' OR jo.title = 'COUNTY ASSESSOR' OR jo.title = 'COUNTY AUDITOR' OR jo.title = 'COUNTY CLERK'
                    OR jo.title = 'COUNTY TREASURER' OR jo.title = 'COUNTY SHERIFF'));
UPDATE jurisdiction_office SET partisan = true
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id
           from jurisdiction_office jo
                  join jurisdiction j
                  on j.jurisdiction_id = jo.jurisdiction_id
           where county = '28' and
                 (jo.title = 'COUNTY PROSECUTOR'));
--SNOHOMISH EXCEPTIONS
UPDATE jurisdiction_office SET partisan = false
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id from jurisdiction_office jo join jurisdiction j on j.jurisdiction_id = jo.jurisdiction_id
           where county = '31' and
                 (jo.title = 'COUNTY ASSESSOR' OR jo.title = 'COUNTY AUDITOR' OR jo.title = 'COUNTY CLERK'
                    OR jo.title = 'COUNTY TREASURER' OR jo.title = 'COUNTY SHERIFF'));
UPDATE jurisdiction_office SET partisan = true
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id
           from jurisdiction_office jo
                  join jurisdiction j
                  on j.jurisdiction_id = jo.jurisdiction_id
           where county = '31' and
                 (jo.title = 'COUNTY EXECUTIVE' OR jo.title = 'COUNTY COUNCIL MEMBER'));
--WHATCOM EXCEPTIONS
UPDATE jurisdiction_office SET partisan = false
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id from jurisdiction_office jo join jurisdiction j on j.jurisdiction_id = jo.jurisdiction_id
           where county = '37' and
                 (jo.title = 'COUNTY EXECUTIVE' OR jo.title = 'COUNTY COUNCIL MEMBER' OR jo.title = 'COUNTY ASSESSOR' OR
                  jo.title = 'COUNTY AUDITOR' OR jo.title = 'COUNTY TREASURER' OR jo.title = 'COUNTY SHERIFF'));
UPDATE jurisdiction_office SET partisan = true
WHERE jurisdiction_office_id
        IN (
           SELECT jo.jurisdiction_office_id
           from jurisdiction_office jo
                  join jurisdiction j
                  on j.jurisdiction_id = jo.jurisdiction_id
           where county = '37' and
                 (jo.title = 'COUNTY PROSECUTOR'));