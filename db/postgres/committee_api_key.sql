CREATE TABLE committee_api_key
(
  api_key_id     SERIAL PRIMARY KEY,
  committee_id   INTEGER NOT NULL,
  hash_value     VARCHAR(1000) UNIQUE,
  memo           VARCHAR(1000),
  date_generated TIMESTAMP WITH TIME ZONE,
  FOREIGN KEY (committee_id) REFERENCES committee(committee_id) ON DELETE CASCADE
);