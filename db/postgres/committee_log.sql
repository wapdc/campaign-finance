CREATE TABLE committee_log (
  log_id SERIAL PRIMARY KEY,
  committee_id INTEGER NOT NULL,
  transaction_type VARCHAR(128),
  transaction_id INTEGER,
  action VARCHAR(255),
  message VARCHAR,
  user_name VARCHAR,
  updated_at TIMESTAMP WITH TIME ZONE
);
CREATE INDEX committee_log_committee_idx ON committee_log(committee_id);
CREATE INDEX committee_log_transaction_idx ON committee_log(transaction_id);
CREATE INDEX committee_log_updated_idx ON committee_log(updated_at);