CREATE TABLE api_consumer_version
(
    version_id       SERIAL PRIMARY KEY,
    consumer_id      INT,
    release_date     DATE,
    version          VARCHAR,
    mandatory_update BOOLEAN,
    fix              VARCHAR
)