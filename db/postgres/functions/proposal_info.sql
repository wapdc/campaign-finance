-- drop function proposal_info(integer,text) cascade;
/**
  Function is to act as a ballot proposal lookup service, taking in a committee ID and election code as filters and
  returning a summarized, single row table of aggregated details on the associated ballot proposals.
  Ballot numbers and proposal type are concatenated, comma seperated strings of all values. The `proposals` column is
  the json representation of all proposals for the committee/year. Refer to the conditional handling in the function for
  all other columns.
  @param p_committee_id int The committee ID to filter on.
  @param p_election_code text The election code to filter on. The election code will be truncated to 4 chars to get the
  election year.
  @return table A single row table of aggregated details on the associated ballot proposals.
 */
create or replace function proposal_info(p_committee_id int, p_election_code text)
    returns table
            (
                proposal_count      int,
                ballot_numbers      text,
                stance              text,
                title               text,
                jurisdiction        text,
                jurisdiction_code   int,
                jurisdiction_county text,
                jurisdiction_type   text,
                proposal_type       text,
                proposals           json
            )
as
$$
declare
    proposal_count               int;
    proposal_stance              text;
    proposal_title               text;
    proposal_jurisdiction        text;
    proposal_jurisdiction_code   int;
    proposal_jurisdiction_county text;
    proposal_jurisdiction_type   text;
    rec                          record;

begin
    select count(*)::int                                                                as c_proposal_count,
           string_agg(distinct p.number, ', ' order by p.number)                        as c_numbers,
           array_agg(distinct cr.stance order by cr.stance)                             as c_stances,
           array_agg(distinct p.title order by p.title)                                 as c_titles,
           array_agg(distinct j.name order by j.name)                                   as c_jurisdictions,
           array_agg(distinct p.jurisdiction_id::text order by p.jurisdiction_id::text) as c_jurisdiction_ids,
           array_agg(distinct fc.coname order by fc.coname)                             as c_jurisdiction_countys,
           array_agg(distinct jc.name order by jc.name)                                 as c_jurisdiction_types,
           initcap(string_agg(distinct p.proposal_type, ', ' order by p.proposal_type)) as c_types,
           json_agg(
                   json_build_object('proposal_id', p.proposal_id, 'proposal_type', p.proposal_type, 'number', p.number,
                                      'title', p.title, 'stance', cr.stance, 'jurisdiction', j.name,
                                      'jurisdiction_type', jc.name, 'jurisdiction_county', fc.coname)
           )                                                                            as c_proposals
    into rec
    from committee c
             join committee_relation cr on c.committee_id = cr.committee_id
             join proposal p on cr.relation_type = 'proposal' and cr.target_id = p.proposal_id
             left join jurisdiction j
                       on p.jurisdiction_id = j.jurisdiction_id -- left join because of missing jurisdictions
             left join fcounty fc on j.county = fc.conum -- left join because statewide proposals don't have a county.
             left join jurisdiction_category jc on j.category = jc.jurisdiction_category_id -- left join because of missing jurisdictions
    where c.committee_id = p_committee_id
      and left(p.election_code, 4) = left(p_election_code, 4);

    if rec.c_proposals is not null then

        -- Count of proposals, zero is handled by the "else" case.
        proposal_count := rec.c_proposal_count;

        -- ballot numbers is output as the raw list in all instances. No need for special handling

        -- Ballot stance, only show when all are the same or "for and against" when there are multiple.
        if array_length(rec.c_stances, 1) = 1 then
            proposal_stance := rec.c_stances[1];
        elseif array_length(rec.c_stances, 1) > 1 then
            proposal_stance := 'both';
        end if;

        -- Ballot title, only show when one title or "Various" when there are multiple.
        if array_length(rec.c_titles, 1) < 1 or array_length(rec.c_titles, 1) is null then
            proposal_title := 'Not provided';
        elseif array_length(rec.c_titles, 1) = 1 then
            proposal_title := rec.c_titles[1];
        elseif array_length(rec.c_titles, 1) > 1 then
            proposal_title := 'Various';
        end if;

        -- Jurisdiction ID, only show id when one jurisdiction or leave null.
        -- Jurisdiction name, only show when one jurisdiction or "Various" when there are multiple.
        -- Even with multiple proposals, they might be in the same jurisdiction and we will still show them because the
        --   query uses distinct.
        if array_length(rec.c_jurisdiction_ids, 1) = 1 then
            proposal_jurisdiction := rec.c_jurisdictions[1];
            proposal_jurisdiction_code := rec.c_jurisdiction_ids[1];
        elseif array_length(rec.c_jurisdiction_ids, 1) > 1 then
            proposal_jurisdiction := 'Various';
        end if;

        -- Jurisdiction county, only show when one county or or "Various" when there are multiple. Even with multiple
        -- jurisdictions, they might all be in the same county and we will show the county.
        if array_length(rec.c_jurisdiction_countys, 1) = 1 then
            proposal_jurisdiction_county := rec.c_jurisdiction_countys[1];
        elseif array_length(rec.c_jurisdiction_countys, 1) > 1 then
            proposal_jurisdiction_county := 'Various';
        end if;

        -- Jurisdiction type, only show when one jurisdiction type or "Various". Even with multiple jurisdictions, they
        -- might all have the same type such as "City" and we will show it.
        if array_length(rec.c_jurisdiction_types, 1) = 1 then
            proposal_jurisdiction_type := rec.c_jurisdiction_types[1];
        elseif array_length(rec.c_jurisdiction_types, 1) > 1 then
            proposal_jurisdiction_type := 'Various';
        end if;
    else
        proposal_count := 0;
    end if;

    return query select proposal_count,
                        rec.c_numbers,
                        proposal_stance,
                        proposal_title,
                        proposal_jurisdiction,
                        proposal_jurisdiction_code,
                        proposal_jurisdiction_county,
                        proposal_jurisdiction_type,
                        rec.c_types,
                        rec.c_proposals;
end

$$ language plpgsql volatile
                    strict;

alter function proposal_info(int, text) owner to wapdc;
