CREATE OR REPLACE FUNCTION primary_election_status(status_id varchar)
RETURNS varchar  AS $$
    BEGIN
        CASE status_id
            WHEN 'N' THEN RETURN 'Not in primary';
            WHEN 'W' THEN RETURN 'Qualified for general';
            WHEN 'L' THEN RETURN 'Lost in primary';
            WHEN 'U' THEN RETURN 'Unopposed in primary';
            WHEN 'C' THEN  RETURN 'Judge certified before primary';
            ELSE RETURN null;
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function primary_election_status(varchar) owner to wapdc;