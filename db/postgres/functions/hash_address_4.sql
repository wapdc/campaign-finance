CREATE OR REPLACE FUNCTION hash_address_4
(
    address  VARCHAR,
    city     VARCHAR,
    state    VARCHAR,
    zip4     VARCHAR
)

    RETURNS VARCHAR(40)
AS $$
BEGIN
RETURN (SELECT encode(digest(trim(coalesce(address, ''))
    || trim(coalesce(city, ''))
    || trim(coalesce(state, ''))
    || trim(coalesce(zip4,'')), 'SHA1'), 'hex'));
END
    $$ LANGUAGE plpgsql IMMUTABLE;