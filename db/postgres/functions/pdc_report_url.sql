/**
  This function generates the correct report URL for a given report number.

  NOTE: There are two versions of this function, one with three arguments (C3, C4, LMC, etc) and this one with four.
        Others may use the fourth parameter at some point in the future

  For C5 reports with user_data (filer submitted) use modern HTML rendering.
  For C5 reports without user_data (electronically filed/data entered) use the report number.
  For reports with user_data, use modern html rendering. Surplus has a slightly different path.
  For reports without user_data, if the report number is less than 10 digits, it was electronically filed so reference
  the report in AX by report number.
  Else, it's an old paper file. Redirect to the website search.
 */
-- drop function if exists pdc_report_url(user_data varchar, v_report_id integer, is_surplus boolean, varchar) cascade;
--drop function if exists pdc_surplus_report_url cascade;

create or replace function pdc_report_url(user_data varchar, v_report_id integer, is_surplus boolean, report_type varchar)
    returns varchar as
$$
select
    case
        when report_type = 'C5' and user_data is not null
            --added in the public/-/ to bypass hash mode, hyphen there to give generic uniformity across applications
            then apollo_url() || '/c5/public/-/#/public/report/'::text || v_report_id
        when coalesce(user_data,'') != '' and lower(cast(user_data as JSON)->>'report_type') = 'lmc'
            then apollo_url() || '/lmc/public/report/'::text || v_report_id
        when coalesce(user_data, '') != ''
            then apollo_url() || '/public/registrations/campaign-finance-' ||
                 case when is_surplus then 'surplus-' else '' end || 'report/'::text || v_report_id
        when v_report_id < 1000000000
            then legacy_pdc_url() || '/rptimg/default.aspx?repno='::text || v_report_id
        else wapdc_setting('pdc_url') || (
            select
                '/political-disclosure-reporting-data/browse-search-data/reports?origin=' ||
                case when is_surplus then replace('SURPLUS ACCT C3|SURPLUS ACCT C3 AMENDED|SURPLUS ACCT C4|SURPLUS ACCT C4 AMENDED', ' ', '+')
                     else replace('C5|C3|C3 AMENDED|C4|C4 AMENDED', ' ', '+') end ||
                '&filer_id=' || c.filer_id ||
                case when is_surplus then ''
                     else ('&election_year=' || r.election_year) end
            from report r
                     join fund f on r.fund_id = f.fund_id
                     join committee c on f.committee_id = c.committee_id
            where report_id = v_report_id
        )
        end as url
$$ language sql stable;

create or replace function pdc_report_url(user_data varchar, v_report_id integer, is_surplus boolean)
    returns varchar as
$$
select pdc_report_url(user_data, v_report_id, is_surplus, null::varchar);
$$ language sql stable;

alter function pdc_report_url(varchar, integer, boolean, varchar) owner to wapdc;
