CREATE OR REPLACE FUNCTION jurisdiction_class_code(jurisdiction_category int)
    RETURNS varchar  AS $$

BEGIN
    case
        when jurisdiction_category = 1 or jurisdiction_category = 4 then return '1';
        when jurisdiction_category = 2 then return '2';
        when jurisdiction_category = 5 then return '3';
        when jurisdiction_category = 3 then return '4';
        else return '5';
        end case;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function jurisdiction_class_code(int) owner to wapdc;