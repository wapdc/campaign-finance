CREATE OR REPLACE FUNCTION primary_or_general(prim_gen varchar)
    RETURNS varchar  AS $$

BEGIN
    CASE prim_gen
        WHEN 'P' THEN RETURN 'Primary';
        WHEN 'G' THEN RETURN 'General';
        WHEN 'N' THEN RETURN 'Full election cycle';
        ELSE RETURN null;
        END CASE;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function primary_or_general(varchar) owner to wapdc;