DROP FUNCTION IF EXISTS fund_summary_c5(integer) CASCADE;

create or replace function fund_summary_c5(integer) returns TABLE(total_expenditures numeric, total_contributions numeric)
    language sql
as $$


select
    COALESCE(SUM(e.amount), 0.0) AS total_expenditures,
    COALESCE(SUM(cont.amount), 0.0) AS total_contributions
FROM
    fund f
        JOIN
    report r ON r.fund_id = f.fund_id
        LEFT JOIN
    c5_expense e ON e.report_id = r.report_id
        LEFT JOIN
    c5_contribution cont ON cont.report_id = r.report_id
WHERE
    r.report_type = 'C5' and f.fund_id = $1;

$$;

alter function fund_summary_c5(integer) owner to wapdc;

