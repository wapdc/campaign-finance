CREATE OR REPLACE FUNCTION cf_save_contributions(fund_id integer, report_id integer, json_data TEXT)
  RETURNS void as $$
  DECLARE
    t_id integer;
    c record;
  BEGIN
    -- Loop through array of contributions
    FOR c IN
      select
        cast(value->>'amount' as numeric) as amount,
        'contribution' as category,
        'receipt' as act,
        to_date(value->>'date', 'YYYY-MM-DD') as transaction_date,
        value->>'contact_key' as contact_key,
        value->>'contributor_type'  as contributor_type,
        value->>'name' as name,
        value->>'address' as address,
        value->>'city' as city,
        value->>'state' as state,
        value->>'postcode' as postcode,
        value->'employer'->>'name' as employer,
        value->'employer'->>'address' as emp_address,
        value->'employer'->>'city' as emp_city,
        value->'employer'->>'state' as emp_state,
        value->'employer'->>'postcode' as emp_postcode,
        value->>'occupation' as occupation,
        value->>'election' as prim_gen,
        case when (value->>'election' = 'P') THEN CAST(value->>'aggregate_primary' as numeric)
          else CAST(value->>'aggregate_general' as numeric) end as aggregate_amount

      from json_array_elements(cast(json_data as json)) d
      -- insert into transaction
    LOOP
      insert into transaction (fund_id, report_id, category, act, amount, transaction_date)
         values(fund_id, report_id, c.category, c.act, c.amount, c.transaction_date) RETURNING transaction_id INTO t_id;
      insert into contribution (transaction_id, name, contributor_type, contact_key, prim_gen, address, city, state, postcode,employer, employer_address, employer_city, employer_state, employer_postcode, occupation)
        values (t_id, c.name, c.contributor_type, c.contact_key, c.prim_gen, c.address, c.city, c.state, c.postcode, c.employer, c.emp_address, c.emp_city, c.emp_state, c.emp_postcode, c.occupation );
      insert into report_sum(transaction_id, aggregate_amount)  VALUES (t_id, c.aggregate_amount);
    END LOOP;
  END
$$ LANGUAGE plpgsql;