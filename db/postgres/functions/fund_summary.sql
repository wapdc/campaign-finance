--DROP FUNCTION IF EXISTS fund_summary(integer) CASCADE;
create or replace function fund_summary(integer)
    returns TABLE(fund_id integer, total_expenditures numeric, total_contributions numeric, total_loans numeric, total_carryforward numeric, fund_filing_type text, updated_at timestamptz)
as $$
select
    f.fund_id,
    coalesce(s.total_expenditures, 0.0) as total_expenditures,
    COALESCE(s.total_contributions,0.0) as total_contributions,
    COALESCE(s.total_loans,0.0) as total_loans,
    COALESCE(s.total_carryforward,0.0) as total_carryforward,
    CASE
        WHEN min_method != max_method THEN 'Mixed'
        WHEN min_method = 'p' THEN 'Paper'
        ELSE 'Electronic' END AS fund_filing_type,
    s.updated_at as updated_at
from
    fund f join
    (
        select
            f.fund_id,
            sum(
                    case
                        when t_exp.category = 'loan' then pmt.principle_paid * -1
                         else t_exp.amount * t_exp.expense_type
                        end
                )  as total_expenditures,
            sum(t_cont.amount) as total_contributions,
            sum(case when t_loan.act = 'forgiven' then p.forgiven_amount * -1
                     when t_loan.act = 'payment' then p.principle_paid * -1
                     when t_loan.act = 'receipt' AND t_loan.inkind = false AND r.report_type = 'C4' then 0
                     else t_loan.amount
                     end)
                     as total_loans,
            sum(t_carry.amount) as total_carryforward,
            min(r.filing_method) as min_method,
            max(r.filing_method) as max_method,
            max(t.updated_at) as updated_at

        from fund f
                 join report r on r.fund_id = f.fund_id
                 join transaction t on t.report_id = r.report_id
                 left join report_sum rs on t.transaction_id = rs.transaction_id

                 left join transaction t_exp on t_exp.transaction_id = t.transaction_id
            and (t_exp.category = 'expense' and (t_exp.act = 'payment' or t_exp.act = 'correction' or t_exp.act = 'refund')
                or ((t_exp.category = 'contribution' and t_exp.act = 'receipt' and t_exp.inkind = true) or
                    (t_exp.category = 'loan' and t_exp.act = 'payment')))
                 left join payment pmt on pmt.transaction_id = t_exp.transaction_id

                 left join transaction t_loan on t_loan.transaction_id = t.transaction_id and t_loan.category = 'loan' and t_loan.act <> 'sum' and t_loan.act <> 'balance'
                 left join payment p on p.transaction_id = t_loan.transaction_id

                 left join transaction t_cont on t_cont.transaction_id = t.transaction_id
            and ((t_cont.category = 'contribution'
                and (t_cont.act = 'correction' or t_cont.amount > 0))
                or (t_cont.category = 'expense' and t_cont.act = 'refund' and t_cont.amount < 0))
            and (rs.legacy_line_item in ('1A', '1E') or rs.legacy_line_item is NULL)

            -- exclude the contribution for in-kind loans (changed act to be 'loan' instead of 'receipt')
            and (t_cont.act <> 'loan')

            -- Don't include records from t_cont where the legacy_line_item has a value that is not in '1A', '1B', '1E', '1D'
            left join
                (select fund_id, report_id, row_number() over (partition by fund_id order by period_end) r
                    from report r
                    where r.superseded_id is null
                    and r.fund_id = $1
                    and r.report_type = 'C4') lr
                        on lr.r = 1 and lr.report_id = r.report_id
            left join transaction t_carry on lr.r=1 and t_carry.transaction_id = t.transaction_id and rs.legacy_line_item = '1'

        where
            r.superseded_id is null
          and f.fund_id = $1
        group by f.fund_id) s on f.fund_id=s.fund_id
where f.fund_id=$1;
$$ LANGUAGE SQL;
