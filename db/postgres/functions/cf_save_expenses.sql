create or replace function cf_save_expenses(fund_id integer, report_id integer, json_data text, legacy_form_type text default 'A/GT50')
  returns void as $$
  declare
    t_id integer;
    e record;
  begin
    for e in
      select
        'expense' as category,
        case when coalesce(cast(value->>'credit' as boolean),false) then 'charge' else 'payment' end as act,
        cast(value->>'amount' as numeric) as amount,
        to_date(value->>'date', 'YYYY-MM-DD') as transaction_date,
        value->>'description' as description,
        value->>'category' as expense_category,
        value->>'contact_key' as contact_key,
        value->>'name' as name,
        value->>'address' as address,
        value->>'city' as city,
        value->>'state' as state,
        value->>'postcode' as postcode,
        value->>'payment_category' as payment_category,
        value->>'payee' as payee,
        value->>'creditor' as creditor
      from json_array_elements(cast(json_data as json)) d
    loop
      insert into transaction (fund_id, report_id, category, act, amount, transaction_date, description, legacy_form_type, expense_type)
        values (fund_id, report_id, e.category, e.act, e.amount, e.transaction_date, e.description, legacy_form_type, case when e.payment_category='debt' then 0 else 1 end)
        returning transaction_id into t_id;
      insert into expense (transaction_id, contact_key, name, address, city, state, postcode, category, payee, creditor)
        values (t_id, e.contact_key, e.name, e.address, e.city, e.state, e.postcode, e.expense_category, e.payee, e.creditor);
    end loop;
  end
$$ language plpgsql;