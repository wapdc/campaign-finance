CREATE OR REPLACE FUNCTION legislative_district_by_jurisdiction(j varchar)
    RETURNS varchar AS $$

BEGIN
    CASE
        WHEN j like 'LEG DISTRICT %' THEN RETURN substring(j, 14, 2);
        ELSE RETURN null;
        END CASE;
END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function legislative_district_by_jurisdiction(varchar) owner to wapdc;
