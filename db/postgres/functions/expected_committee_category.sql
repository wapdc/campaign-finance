drop function if exists committee_category(category varchar, continuing boolean);
drop function if exists expected_committee_category(varchar, varchar, int, int, boolean);
create or replace function expected_committee_category (x_pac_type varchar, x_bonafide_type varchar, x_party_id int, x_jurisdiction_id int, x_continuing boolean)
returns varchar
language plpgsql
as $$
declare
  x_jurisdiction_category_id int;
begin
  if (x_jurisdiction_id is not null) then
    x_jurisdiction_category_id := category
      from jurisdiction
      where jurisdiction_id = x_jurisdiction_id;
  end if;

  return (select cc.name
  from committee_category cc
  where (
      cc.pac_type = x_pac_type
        or cc.pac_type is null
    )
    and (
      cc.bonafide_type = x_bonafide_type
        or cc.bonafide_type is null
    )
    and (
      cc.continuing = x_continuing
        or cc.continuing is null
    )
    and (
      cc.party_id = x_party_id
        or cc.party_id is null
        or (
          -- party id and jurisdiction category also allow -1 as a value, which means any non-null value
          -- these should be ordered below more specific values, meaning party id 3 should take precedence over -1
          cc.party_id = -1
            and x_party_id is not null
        )
    )
    and (
      cc.jurisdiction_category = x_jurisdiction_category_id
        or cc.jurisdiction_category is null
        or (
          cc.jurisdiction_category = -1
            and x_jurisdiction_category_id is not null
        )
    )
    and cc.staff_only = false
  order by pac_type nulls last, bonafide_type nulls last, party_id desc nulls last, cc.jurisdiction_category desc nulls last, cc.continuing
  limit 1
  );
end
$$;

--select expected_committee_category('bonafide', 'district', 15, null, true); -- Republican Leg District Party
--select expected_committee_category('bonafide', 'district', 'REPUBLICAN', null, false); -- Other
--select expected_committee_category('bonafide', 'district', 14, null, true); -- Minor Party
--select expected_committee_category('candidate', 'district', 14, null, false); -- Candidate
--select expected_committee_category('candidate', 'district', 14, null, true); -- Other
--select expected_committee_category('candidate', null, null, null, false); -- Candidate
--select expected_committee_category('pac', null, null, 1000, false); -- Statewide Ballot Measure
--select expected_committee_category('pac', 'district', 14, 1000, false); -- Statewide Ballot Measure
--select expected_committee_category('pac', 'district', 14, 3000, false); -- Local Ballot Measure
--select expected_committee_category('pac', 'district', 14, 3027, false); -- Education
--select expected_committee_category('pac', 'district', 14, 3027, true); -- Continuing Committee
--select expected_committee_category('pac', null, null, null, true); -- Continuing Committee
--select expected_committee_category('pac', 'district', 12, null, false); -- Single Election Committee
--select expected_committee_category('pac', null, null, null, false); -- Single Election Committee