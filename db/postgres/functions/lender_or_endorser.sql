CREATE OR REPLACE FUNCTION lender_or_endorser(lender_endorser varchar)
    RETURNS varchar  AS $$

BEGIN
    CASE lender_endorser
        WHEN 'L' THEN RETURN 'Lender';
        WHEN 'E' THEN RETURN 'Endorser';
        ELSE RETURN null;
        END CASE;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function lender_or_endorser(varchar) owner to wapdc;