CREATE OR REPLACE FUNCTION code_by_contributor_type(ct varchar)
    RETURNS varchar AS $$

BEGIN
    CASE
        WHEN ct = 'A'
            THEN RETURN 'Anonymous';
        WHEN ct = 'B'
            THEN RETURN 'Business';
        WHEN ct = 'C'
            THEN RETURN 'Political Action Committee';
        WHEN ct = 'F'
            THEN RETURN 'Business';
        WHEN ct = 'I'
            THEN RETURN 'Individual';
        WHEN ct = 'L'
            THEN RETURN 'Caucus';
        WHEN ct = 'O'
            THEN RETURN 'Other';
        WHEN ct = 'P'
            THEN RETURN 'Party';
        WHEN ct = 'S'
            THEN RETURN 'Self';
        WHEN ct = 'M'
            THEN RETURN 'Minor Party';
        WHEN ct = 'U'
            THEN RETURN 'Union';
        ELSE RETURN 'Other';
        END CASE;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function code_by_contributor_type(varchar) owner to wapdc;