drop function if exists cf_get_contribution_limits(p_fund_id INTEGER);
CREATE OR REPLACE FUNCTION cf_get_contribution_limits(p_fund_id INTEGER)
  RETURNS json as $$
DECLARE
  c_limits_category_id integer;
  c_voter_count integer;
  contribution_limits json;
BEGIN
  --get contribution limits
  c_limits_category_id := (select limit_category
                           from fund f
                                  join candidacy c on f.committee_id = c.committee_id
                                  join jurisdiction_office jo on c.jurisdiction_id = jo.jurisdiction_id and c.office_code = jo.offcode
                           where f.fund_id = p_fund_id);

  --Use the greatest imported year less than election year
  c_voter_count := (select votercount
                    from fund f
                           join candidacy c on f.committee_id = c.committee_id
                           join jurisdiction j on c.jurisdiction_id = j.jurisdiction_id
                           join (select
                                     v1.jurisdiction_id, v1.year, sum(v1.votercount) as votercount
                                 from votercount v1 group by v1.jurisdiction_id, v1.year
                                ) v
                                on cast(v.year as text) < f.election_code and
                                   v.jurisdiction_id = c.jurisdiction_id
                    where fund_id = p_fund_id order by v.year desc
                    limit 1);

  select json_agg(lim) into contribution_limits from (select
         case when agg_by_voter is true
           then amount * c_voter_count
           else amount end as amount,
           cl.type_code,
           contributor_type,
           label
  from contribution_limit cl left join contributor_type ct on cl.type_code = ct.type_code where limit_category = c_limits_category_id) lim;

  return contribution_limits;

end;


$$ LANGUAGE plpgsql;