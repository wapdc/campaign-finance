-- drop function determine_special_reporting_periods_by_election_date(date);
create or replace function determine_special_reporting_periods_by_election_date(x_election_date date)
  -- note: this code assumes a Tuesday election in its coverage of specific holidays
  returns table (
    reporting_period_id int,
    reporting_period_type text,
    election_date date,
    start_date date,
    end_date date,
    due_date date
  )
  language plpgsql immutable
  as $$
declare
  election_record election%rowtype;
  election_year int;
  pre_seven reporting_period%rowtype;
  pre_twenty_one reporting_period%rowtype;
  post reporting_period%rowtype;
  obligation_type_suffix text;
  holidays text[];
  x_start_date date;
  x_end_date date;
  x_due_date date;
BEGIN
  -- get election record
  select e.*
  from election e
  where e.election_date = x_election_date
  into election_record;

  election_year := left(election_record.election_code, 4);
  raise notice 'Election record: %', election_record;
  raise notice 'Election year: %', election_year;

  obligation_type_suffix := case
    when (election_record.is_special is true)
      then 'Special'
    when (election_record.is_primary is true)
      then 'Primary'
    else
      'General'
  end;

  -- holidays that might fall 7 or 21 days before a Tuesday election: Jan 1, July 4, Nov 10, Dec 25
  holidays := array[ '1-1', '7-4', '11-10', '12-25'];

  -- 21-day pre ends 22 days prior to the election, and it negates any reporting obligation in that month,
  -- which means the 21-day reporting obligation will include the previous calendar month
  x_start_date := date_trunc('month', x_election_date - interval '1 month 22 days');
  x_end_date := x_election_date - 22;
  x_due_date := x_election_date - 21;
  -- check month and day against holiday list
  if (date_part('month', x_due_date) || '-' || date_part('day', x_due_date) = any(holidays))
    then x_due_date := x_due_date + 1;
  end if;
  pre_twenty_one := (null, 'TwentyOneDayPre' || obligation_type_suffix, x_election_date, x_start_date, x_end_date, x_due_date)
    from election e
    where e.election_date = x_election_date;

  -- seven day pre
  x_start_date := x_end_date + 1;
  x_end_date := x_election_date - 8;
  x_due_date := x_election_date - 7;
  -- check month and day against holiday list
  if (date_part('month', x_due_date) || '-' || date_part('day', x_due_date) = any(holidays))
    then x_due_date := x_due_date + 1;
  end if;
  pre_seven := (null, 'SevenDayPre' || obligation_type_suffix, x_election_date, x_start_date, x_end_date, x_due_date)
    from election e
    where e.election_date = x_election_date;

  -- post
  x_start_date := x_end_date + 1;
  -- end date and due date can be obtained from the existing monthly reporting period this overlaps
  select rp.end_date, rp.due_date
  from c4_report_period rp
  where rp.election_date is null
   and date_trunc('month', x_election_date) = rp.start_date
  into x_end_date, x_due_date;
  post := (null, 'Post' || obligation_type_suffix, election_record.election_date, x_start_date, x_end_date, x_due_date)
    from election e
    where e.election_date = x_election_date;

  -- output the three periods
  return query with special_periods as (
    select null::int, post.reporting_period_type::text, post.election_date, post.start_date, post.end_date, post.due_date
    union
    select null::int, pre_seven.reporting_period_type::text, pre_seven.election_date, pre_seven.start_date, pre_seven.end_date, pre_seven.due_date
    union
    select null::int, pre_twenty_one.reporting_period_type::text, pre_twenty_one.election_date, pre_twenty_one.start_date, pre_twenty_one.end_date, pre_twenty_one.due_date
  )
  select * from special_periods sp
  order by sp.reporting_period_type desc;

END
$$

-- examples
-- select * from determine_special_reporting_periods_by_election_date('2021-12-07')
-- select * from determine_special_reporting_periods_by_election_date('2021-11-02')
-- select * from determine_special_reporting_periods_by_election_date('2023-11-07')
-- holiday potential: must insert 2023-07-25 election, then the 21-day due date will be 7/5 not 7/4
-- select * from determine_special_reporting_periods_by_election_date('2023-07-25')
