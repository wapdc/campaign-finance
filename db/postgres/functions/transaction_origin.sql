CREATE OR REPLACE FUNCTION transaction_origin(report_type text, category text, act text, inkind boolean, itemized boolean)
    RETURNS varchar  AS $$

BEGIN
    CASE
        WHEN category = 'contribution' and act = 'receipt' and inkind = true THEN RETURN 'B.1';
        WHEN category = 'contribution' and act = 'receipt' THEN RETURN 'C3';
        WHEN category = 'contribution' and act = 'auction buy' THEN RETURN 'AU';
        WHEN category = 'contribution' and act = 'auction donation' THEN RETURN 'AU';
        WHEN category = 'contribution' and act = 'sum' THEN RETURN 'C3';
        WHEN category = 'contribution' and act = 'correction' THEN RETURN 'C.1';
        WHEN category = 'contribution' and act = 'miscellaneous' THEN RETURN 'C3';
        WHEN category = 'deposit' and act = 'sum' THEN RETURN 'C4';
        WHEN category = 'expense' and act = 'correction' THEN RETURN 'C.2';
        WHEN category = 'expense' and act = 'sum' THEN RETURN 'C4';
        WHEN category = 'expense' and act = 'payment' and itemized = true THEN RETURN 'A/GT50';
        WHEN category = 'expense' and act = 'payment' and itemized = false THEN RETURN 'A/LE50';
        WHEN category = 'expense' and act = 'refund' THEN RETURN 'C.3';
        WHEN category = 'expense' and act = 'invoice' THEN RETURN 'B.3';
        WHEN category = 'fund' and act = 'sum' THEN RETURN 'C4';
        WHEN category = 'loan' and act = 'forgiven' THEN RETURN 'L.3';
        WHEN category = 'loan' and act = 'payment' THEN RETURN 'L.2';
        WHEN category = 'loan' and act = 'balance' THEN RETURN 'L.4';
        WHEN category = 'loan' and act = 'receipt' THEN RETURN 'L.1';
        WHEN category = 'loan' and act = 'sum' THEN RETURN 'C3';
        WHEN category = 'pledge' and act = 'receipt' THEN RETURN 'C3';
        ELSE RETURN null;
        END CASE;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function transaction_origin(report_type text, category text, act text, inkind boolean, itemized boolean) owner to wapdc;