CREATE OR REPLACE FUNCTION jurisdiction_office_category_sortcode(jurisdiction_name varchar, jurisdiction_category int, office_code varchar, county_code varchar)
    RETURNS varchar  AS $$

BEGIN
    case when jurisdiction_category = 1 then return '000001';
        when office_code = '12' then return '000002';
        when office_code = '13' then return '000003';
        when jurisdiction_category = 2 then return RIGHT('000000' || county_code, 6);
        when office_code = '14' then return '000001';
        when office_code = '15' then return '000002';
        when office_code = '16' then return '000003';
        when office_code = '17' then return '000004';
        when office_code = '18' then return '000005';
        when jurisdiction_category = 3 then return regexp_replace(jurisdiction_name, 'CITY OF |TOWN OF ', '');
        else return RIGHT('000000' || county_code, 6);
        end case;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function jurisdiction_office_category_sortcode(varchar, int, varchar, varchar) owner to wapdc;