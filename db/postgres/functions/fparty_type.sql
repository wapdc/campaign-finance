CREATE OR REPLACE FUNCTION fparty_type(party_id INT)
RETURNS varchar  AS $$

    BEGIN
        CASE party_id
            WHEN 20 THEN RETURN 'NO';
            WHEN 21 THEN RETURN 'O';
            WHEN 24 THEN RETURN 'SL';
            WHEN 25 THEN RETURN 'J';
            WHEN 3 THEN RETURN 'D';
            WHEN 5 THEN RETURN 'G';
            WHEN 6 THEN RETURN 'I';
            WHEN 7 THEN RETURN 'L';
            WHEN 11 THEN RETURN 'N';
            WHEN 15 THEN RETURN 'R';
            WHEN 16 THEN RETURN 'S';
            WHEN 18 THEN RETURN 'CP';
            ELSE RETURN NULL;
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function fparty_type(INT) owner to wapdc;
