CREATE OR REPLACE FUNCTION bonafide_type(bonafide_type varchar)
RETURNS varchar  AS $$

    BEGIN
        CASE bonafide_type
            WHEN 'county' THEN RETURN 'County Party';
            WHEN 'district' THEN RETURN 'Leg Dist Party';
            WHEN 'state' THEN RETURN 'State Party';
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function bonafide_type(varchar) owner to wapdc;
