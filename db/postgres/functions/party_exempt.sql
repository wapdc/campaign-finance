CREATE OR REPLACE FUNCTION party_exempt(exempt boolean)
RETURNS varchar  AS $$

    BEGIN
        CASE exempt
            WHEN true THEN RETURN 'Exempt account';
            WHEN false THEN RETURN 'Non-Exempt account';
            ELSE RETURN null;
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function party_exempt(boolean) owner to wapdc;
