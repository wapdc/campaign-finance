CREATE OR REPLACE FUNCTION initiative_type(jurisdiction_category_id integer)
    RETURNS varchar  AS $$

BEGIN
    CASE
        WHEN jurisdiction_category_id = 1 THEN RETURN 'Statewide ballot initiative';
        WHEN jurisdiction_category_id != 1 THEN RETURN 'Local ballot initiative';
        ELSE RETURN null;
        END CASE;
END
$$ LANGUAGE plpgsql IMMUTABLE
                    STRICT;

alter function initiative_type(integer) owner to wapdc;