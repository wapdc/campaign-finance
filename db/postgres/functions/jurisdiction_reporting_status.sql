-- drop function jurisdiction_reporting_status(p_jurisdiction_id int, p_election_year int);
/**
  Gets the jurisdiction reporting status for a given year as "N", "F", or "C". The jurisdiction status table is driven
  by date, not year. To get the status for an election year, Oct 31 was chosen as a lookup date as being something very
  close to, but never after the general election as the best point of lookup.
    @name jurisdiction_voter_count_year
    @param p_jurisdiction_id
    @param p_election_year
    @return text
 */
create or replace function jurisdiction_reporting_status(p_jurisdiction_id int, p_election_year int)
    RETURNS text AS $$
DECLARE
    v_jurisdiction_status text;

BEGIN
    select status into v_jurisdiction_status
    from jurisdiction_status
    where to_date(p_election_year::text || '-10-31', 'YYYY-MM-DD')
        between valid_from and coalesce(valid_to, '2099-12-31')
    and jurisdiction_id = p_jurisdiction_id
    limit 1;

    return v_jurisdiction_status;
END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function jurisdiction_reporting_status(p_jurisdiction_id int, p_year int) owner to wapdc;