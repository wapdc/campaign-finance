drop function if exists political_committee_type cascade;
create or replace function political_committee_type(category varchar, pac_type varchar, party varchar, continuing boolean) returns varchar
as
$$
BEGIN
  CASE
    when category is not null then return category;
    when lower(pac_type) = 'bonafide' and upper(party) = 'D' then return 'Democratic Bona Fide';
    when lower(pac_type) = 'bonafide' and upper(party) = 'R' then return 'Republican Bona Fide';
    when lower(pac_type) = 'bonafide' and upper(party) not in ( 'D', 'R' ) then return 'Minor Party';
    when lower(pac_type) = 'caucus' and upper(party) = 'D' then return 'Democratic Caucus';
    when lower(pac_type) = 'caucus' and upper(party) = 'R' then return 'Republican Caucus';
    when lower(pac_type) = 'pac' and upper(party) = 'D' then return 'Democratic Other';
    when lower(pac_type) = 'pac' and upper(party) = 'R' then return 'Republican Other';
    when lower(pac_type) = 'pac' and coalesce(continuing, false) = false then return 'Single Election';
    when lower(pac_type) = 'candidate' then return 'Candidate';
    when lower(pac_type) = 'surplus' then return 'Surplus';
    when lower(pac_type) = 'out-of-state' then return 'Out-of-State';
    else return 'Other';
    end case;
end
$$ language plpgsql immutable;

alter function political_committee_type(varchar, varchar, varchar, boolean) owner to wapdc;
