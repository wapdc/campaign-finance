create or replace function cf_reporting_status(p_committee_id integer, p_date date)
    returns varchar as $$
 declare
    v_reporting_status varchar(16);
 begin
     select coalesce(
                     (select reporting_type from
                         (select reporting_type,  row_number() over (partition by committee_id order by submitted_at desc, registration_id desc) as r
                          from registration where committee_id = p_committee_id and submitted_at < p_date) v
                      where r= 1),
                    ( select reporting_type
                     from committee
                     where committee_id = p_committee_id )
            ) into v_reporting_status;
     return v_reporting_status;
 end
$$ language plpgsql;

