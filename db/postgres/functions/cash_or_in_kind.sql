CREATE OR REPLACE FUNCTION cash_or_in_kind(inkind boolean)
    RETURNS varchar  AS $$

BEGIN
    CASE inkind
        WHEN true THEN RETURN 'In-kind';
        WHEN false THEN RETURN 'Cash';
        ELSE RETURN null;
        END CASE;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function cash_or_in_kind(boolean) owner to wapdc;