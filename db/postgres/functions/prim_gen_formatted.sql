CREATE OR REPLACE FUNCTION prim_gen_formatted(pg varchar)
    RETURNS varchar AS $$

BEGIN
    CASE
        WHEN pg = 'P' THEN RETURN 'Primary';
        WHEN pg = 'G' THEN RETURN 'General';
        WHEN pg = 'N' THEN RETURN 'Full Election';
        END CASE;
END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function prim_gen_formatted(varchar) owner to wapdc;
