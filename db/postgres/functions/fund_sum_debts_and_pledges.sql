DROP FUNCTION IF EXISTS fund_sum_debts_and_pledges(integer);
CREATE OR REPLACE FUNCTION fund_sum_debts_and_pledges(integer)
  RETURNS table(fund_id integer, total_pledges numeric, total_debt numeric, updated_at timestamptz)  AS $$
  select f.fund_id,
    coalesce(s.total_pledges, 0.0) as total_pledges,
    COALESCE(s.total_debt,0.0) as total_debt,
    s.updated_at
  from
    fund f left join
    (SELECT fund_id, report_id, row_number() OVER (PARTITION BY fund_id ORDER BY period_end DESC) r
          FROM report r
          WHERE r.superseded_id IS NULL
            AND r.fund_id = $1
            AND r.report_type = 'C4') lr
    ON lr.fund_id = f.fund_id and lr.r=1
    LEFT JOIN (
      select t.report_id,
             sum(case when t.category='expense' and t.act='invoice' then t.amount end) as total_debt,
             sum(case when t.category='pledge' then t.amount end) as total_pledges,
             max(t.updated_at) as updated_at
        from transaction t
        where t.fund_id = $1
          and ((t.category= 'expense' and t.act='invoice') OR (t.category = 'pledge'))
        group by t.report_id
      ) s ON s.report_id = lr.report_id
  where f.fund_id=$1;
$$ LANGUAGE SQL;
