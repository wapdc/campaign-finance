CREATE OR REPLACE FUNCTION legacy_ax_committee_type(jurisdiction_category_id int)
RETURNS varchar  AS $$

    BEGIN
        CASE
            when jurisdiction_category_id = 1 then return 'STATE EXECUTIVE';
            when jurisdiction_category_id = 4 then return 'STATE LEGISLATIVE';
            when jurisdiction_category_id = 5 then return 'JUDICIAL';
            when jurisdiction_category_id is not null and jurisdiction_category_id not in (14, 15, 16)
                then return 'LOCAL CANDIDATES';
            else return 'COMMITTEES';
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function legacy_ax_committee_type(int) owner to wapdc;
