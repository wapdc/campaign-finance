-- drop function jurisdiction_voter_count(p_jurisdiction_id int, p_election_year int);
/**
  Gets the effective voter count for an election year. The voter counts for the prior year are used for the current year
  election. For example, a 2023 campaign needs to use the 2022 counts when they are calculating things like limits or
  jurisdiction reporting requirements. This function does not try to account for that. To get the effective count for
  a 2023 campaign, pass 2022 as the election year.
    @name jurisdiction_voter_count_year
    @param p_jurisdiction_id
    @param p_election_year
    @return int
 */
create or replace function jurisdiction_voter_count(p_jurisdiction_id int, p_election_year int)
    RETURNS int AS $$
DECLARE
    v_voter_count int;

BEGIN
    select
        sum(v.votercount) into v_voter_count
    from votercount v
    where v.jurisdiction_id = p_jurisdiction_id and year < p_election_year
    group by jurisdiction_id, year
    order by v.year desc limit 1;

    return v_voter_count;
END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function jurisdiction_voter_count(p_jurisdiction_id int, p_year int) owner to wapdc;