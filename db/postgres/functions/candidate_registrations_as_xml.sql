DROP FUNCTION IF EXISTS candidate_registrations_as_xml(integer);
CREATE OR REPLACE FUNCTION candidate_registrations_as_xml(num_days integer)
    RETURNS xml
AS
$$

select xmlelement(name committee,
    xmlagg(xmlelement(name candidate_committee,
        xmlattributes (
          p.name || ', ' || f.offtitle || ' - ' || e.title as title,
          candidacy.candidacy_id as id,
          candidacy.filer_id as filer_id,
          extract(year from e.election_date) as election_year
        ),
        xmlelement( name body, 'party:' || coalesce(p2.name, 'NONE') || ' jurisdiction:' || j.name  )
        ))) as xml
from candidacy
         join person p on candidacy.person_id = p.person_id
         join foffice f on candidacy.office_code = f.offcode
         join election e on e.election_code = candidacy.election_code
         join jurisdiction j on candidacy.jurisdiction_id = j.jurisdiction_id
         left join party p2 on candidacy.party_id = p2.party_id
where candidacy.updated_at >= now() - num_days * interval '1 day';

$$ LANGUAGE SQL;
alter function candidate_registrations_as_xml(integer) owner to wapdc;