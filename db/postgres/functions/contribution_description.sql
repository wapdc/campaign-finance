CREATE OR REPLACE FUNCTION contribution_description(category varchar, act varchar, line_item varchar, person_count integer,
original_amount numeric, corrected_amount numeric, fm_value numeric, sale_value numeric, description varchar, contributor_type varchar)
    RETURNS varchar  AS $$

BEGIN
    CASE WHEN act = 'correction'
        THEN RETURN
                'CORRECTION TO CONTRIBUTIONS ' ||
                '(Reported amount: ' || original_amount::text ||
                '; Corrected amount: ' || corrected_amount::text || ') ' ||
                'Description: ' || COALESCE(description, 'Not provided.');
        WHEN category = 'expense'
        THEN RETURN
                'CORRECTION - REFUND FROM VENDOR' || '; '
                || 'Description: ' || COALESCE(description, 'Not provided.');
        WHEN act = 'auction donation' or act = 'auction buy'
        THEN RETURN     '(Fair Market Value: '
            || fm_value::text
            || '; Sale Price: '
            || sale_value::text
            || 'Item description: '
            || COALESCE(description, 'Not Provided.');
        WHEN line_item = '1A'
                THEN RETURN 'Anonymous - Cash';
        WHEN act = 'miscellaneous' and contributor_type = 'S'
                THEN RETURN 'Candidates Personal Funds - Does not include candidate loans';
            WHEN act = 'miscellaneous'
                THEN RETURN 'Miscellaneous Receipts';
            WHEN line_item = '1E'
                THEN RETURN 'Small contributions' || COALESCE(' (' || person_count::text || ' persons giving)', '');
        ELSE RETURN COALESCE(description, '');
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function contribution_description(category varchar, act varchar, line_item varchar, count integer,
original_amount numeric, corrected_amount numeric, fm_value numeric, sale_value numeric, description varchar, contributor_type varchar) owner to wapdc;