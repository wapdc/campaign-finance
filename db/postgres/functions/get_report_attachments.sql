CREATE OR REPLACE FUNCTION cf_get_report_attachments(x_report_id int)
    RETURNS text as $$
DECLARE
    retvar text;
BEGIN
    select json_agg(v) into retvar from
        (select * from attachment where target_id = x_report_id and target_type = 'report' and access_mode = 'public') v;
    return retvar;
END
$$ LANGUAGE plpgsql;