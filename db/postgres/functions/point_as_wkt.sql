CREATE OR REPLACE FUNCTION point_as_wkt(location point)
    RETURNS varchar  AS $$

BEGIN
    RETURN format('POINT (%s %s)', location[0], location[1]);
END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function point_as_wkt(point) owner to wapdc;