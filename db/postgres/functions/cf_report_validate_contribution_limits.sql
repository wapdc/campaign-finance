drop function if exists cf_report_validate_contribution_limits(p_fund_id INTEGER, p_contacts_text text);
CREATE OR REPLACE FUNCTION cf_report_validate_contribution_limits(p_fund_id INTEGER, p_contacts_text text)
  RETURNS json as $$
DECLARE
  p_contacts json;
  limits json;
  contact json;
  cl_contacts json;
BEGIN

  p_contacts := cast(p_contacts_text as json);
  limits := (select * from cf_get_contribution_limits(p_fund_id));

  select json_agg(cont)
  into cl_contacts
  from (
         select case when pagg > amount then true when gagg > amount then true else false end as overlimit,
                name,
                contact_key,
                pagg,
                gagg,
                amount
         from (select c ->> 'contact_key'                        as contact_key,
                      c ->> 'name'                               as name,
                      cast(c ->> 'aggregate_primary' as numeric) as pagg,
                      cast(c ->> 'aggregate_general' as numeric) as gagg,
                      cast(l ->> 'amount' as numeric)            as amount
               from json_array_elements(p_contacts) c
                      left join json_array_elements(limits) l on c ->> 'contributor_type' = l ->> 'contributor_type'
              ) amounts) cont;

  return cl_contacts;

  end
$$ LANGUAGE plpgsql;