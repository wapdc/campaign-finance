create or replace function ie_sponsor_description(sponsor_type_code text, report_type_code text) returns varchar AS
$$
begin
    case
        when sponsor_type_code = 'A' then return 'An individual using only personal funds';
        when sponsor_type_code = 'B'
            then return 'An individual using personal funds and/or funds received from others.';
        when sponsor_type_code = 'C'
            then return 'A business, union, group, association, organization, or other person using only general treasury funds.';
        when sponsor_type_code = 'D'
            then return 'A business, union, group, association, organization, or other person using general treasury funds and/or funds received from others.';
        when sponsor_type_code = 'E'
            then return 'A political committee filing C-3 and C-4 reports. (RCW 42.17A.205 - .240)';
        when sponsor_type_code = 'F' then return 'A political committee filing C-5 reports. (RCW 42.17A.250)';
        when sponsor_type_code = 'G' then return 'Other';
        else return 'Not Required for an ' || case
                when report_type_code = 'IE' then 'Independent Expenditure'
                when report_type_code = 'IEA' then 'Independent Expenditure Ad'
            end;
        end case;
end
$$ language plpgsql immutable;

alter function ie_sponsor_description(sponsor_type_code text, report_type_code text) owner to wapdc;
