CREATE OR REPLACE FUNCTION jurisdiction_class_title(jurisdiction_category int)
    RETURNS varchar  AS $$

BEGIN
    case
        when jurisdiction_category = 1 or jurisdiction_category = 4 then return 'STATE OFFICES';
        when jurisdiction_category = 2 then return 'COUNTY';
        when jurisdiction_category = 5 then return 'JUDICIAL';
        when jurisdiction_category = 3 then return 'MUNICIPAL';
        else return 'SPECIAL';
        end case;
    END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function jurisdiction_class_title(int) owner to wapdc;