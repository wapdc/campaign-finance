
CREATE OR REPLACE FUNCTION date_format(date timestamptz)
RETURNS varchar  AS $$
    BEGIN
        RETURN TO_CHAR(date :: DATE, 'mm/dd/yyyy');
    END;
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function date_format(timestamptz) owner to wapdc;