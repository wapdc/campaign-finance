DROP FUNCTION IF EXISTS fund_summary_independent_expenditures(integer);
DROP FUNCTION IF EXISTS fund_summary_independent_expenditures(filer_id text, election_year text);
CREATE OR REPLACE FUNCTION fund_summary_independent_expenditures(filer_id text, election_year text)
    RETURNS table
            (
                filer_id       text,
                election_year  text,
                for_amount     numeric,
                against_amount numeric
            )
AS $$
select ie.filer_id,
       r.election_year,
       sum(case when ie.spt_opp = 'SPT' then portion_of_amt else 0 end) as for_amount,
       sum(case when ie.spt_opp = 'OPP' then portion_of_amt else 0 end) as against_amount
from c6_reports r
         join c6_identified_entity ie on r.repno = ie.repno
where r.superseded is null
  and ie.filer_id = fund_summary_independent_expenditures.filer_id
  and r.election_year = fund_summary_independent_expenditures.election_year
group by ie.filer_id, r.election_year

$$ LANGUAGE SQL;
