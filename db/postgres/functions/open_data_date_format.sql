CREATE OR REPLACE FUNCTION open_data_date_format(date timestamptz)
    RETURNS varchar  AS $$
BEGIN
    RETURN TO_CHAR(date :: DATE, 'yyyy-mm-dd');
END;
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function open_data_date_format(timestamptz) owner to wapdc;