CREATE OR REPLACE FUNCTION party_committee(pac_type varchar)
RETURNS varchar  AS $$

    BEGIN
        CASE pac_type
            WHEN 'bonafide' THEN RETURN 'Political party committee';
            ELSE RETURN null;
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function party_committee(varchar) owner to wapdc;