CREATE OR REPLACE FUNCTION jurisdiction_office_category_code(jurisdiction_id int, jurisdiction_category int, office_code varchar, county_code varchar)
    RETURNS varchar  AS $$

BEGIN
    case when jurisdiction_category = 1 then return '1';
        when office_code = '12' then return '2';
        when office_code = '13' then return '3';
        when jurisdiction_category = 2 then return regexp_replace(county_code, '(^)0*', '');
        when office_code = '14' then return '1';
        when office_code = '15' then return '2';
        when office_code = '16' then return '3';
        when office_code = '17' then return '4';
        when office_code = '18' then return '5';
        when jurisdiction_category = 3 then return jurisdiction_id::text;
        else return county_code;
        end case;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function jurisdiction_office_category_code(int, int, varchar, varchar) owner to wapdc;