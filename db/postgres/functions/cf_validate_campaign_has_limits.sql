DROP FUNCTION IF EXISTS cf_validate_campaign_has_limits(p_fund_id INTEGER);
CREATE OR REPLACE FUNCTION cf_validate_campaign_has_limits(p_fund_id INTEGER)
RETURNS bool as $$
    DECLARE
        has_limits bool;
    BEGIN
        has_limits := (select
            case
                when c.pac_type <> 'candidate' then false
                when jc1.legacy_code is null then false
                when jc1.legacy_code='' then false
                when ca.office_code = '50' then false
                when ca.office_code >= '35' and jc1.legacy_code in ('CL', 'LC') then false
                -- exclude offices that don't allow primary
                when jc1.legacy_code  = 'PC' then false
                else true
                end as has_limits
        FROM
            fund f join committee c ON f.committee_id = c.committee_id
                   left join candidacy ca ON c.committee_id=ca.committee_id
                   left join jurisdiction j on j.jurisdiction_id=ca.jurisdiction_id
                   left join foffice o on o.offcode = ca.office_code
                   left join jurisdiction_category jc1 on j.category=jc1.jurisdiction_category_id
        where fund_id = p_fund_id limit 1);

        RETURN has_limits;
    END
$$ LANGUAGE plpgsql STABLE

