create or replace function report_description(report_type_code text)
returns varchar as $$

    begin
        case lower(report_type_code)
            when 'ie' then return 'Independent Expenditure';
            when 'ec' then return 'Electioneering Communication';
            when 'iea' then return 'Independent Expenditure Ad';
        end case;
    end
$$ language plpgsql immutable strict ;

alter function report_description(report_type_code text) owner to wapdc;
