CREATE OR REPLACE FUNCTION general_election_status(status_id varchar)
RETURNS varchar  AS $$

    BEGIN
        CASE status_id
            WHEN 'W' THEN RETURN 'Won in general';
            WHEN 'L' THEN RETURN 'Lost in general';
            WHEN 'U' THEN RETURN 'Unopposed in general';
            WHEN 'C' THEN  RETURN 'Judge certified before general';
            ELSE RETURN null;
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function general_election_status(varchar) owner to wapdc;