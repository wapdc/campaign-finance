-- this function pulls from the public reporting_period table a list of reporting periods given the following:
-- start boundary text
-- end_boundary text
-- election dates text array
drop function potential_reporting_obligations(date, date, varchar[]);
create or replace function potential_reporting_obligations(start_boundary date, end_boundary date, election_codes varchar[] default null)
  returns table (reporting_period_id int, reporting_period_type varchar(32), election_date date, start_date date, end_date date, due_date date)
  language plpgsql
  as $$
  declare
    latest_election varchar := null;
    election_dates date[];
  begin

    if election_codes is not null then
      -- election dates
      election_dates := array_agg(e.election_date) from election e where e.election_code = any(election_codes);
      -- latest election code
      latest_election := e.election_code from election e where e.election_code = any(election_codes) order by e.election_date desc limit 1;
    end if;

    return query
      select rp.reporting_period_id, rp.reporting_period_type, rp.election_date, rp.start_date, rp.end_date, rp.due_date
      from c4_report_period rp
        -- join any monthly obligations against overlapping special obligations in order to exclude them
        left join c4_report_period rp2
          on rp.reporting_period_id <> rp2.reporting_period_id
            -- not monthly obligations
            and rp2.monthly is false
            -- only special obligations that match one of the election dates
            and rp2.election_date = ANY (election_dates)
            -- overlaps uses a "half-open" interval meaning it checks <= on one value and < on the other
            -- because of that we need to add one day to one of the intervals (except in the case where
            -- the start and end date are exactly the same, which should never be the case for a special
            -- reporting obligation)
            and (rp.start_date, rp.end_date + interval '1' day) overlaps (rp2.start_date, rp2.end_date + interval '1' day)
      -- all periods that start
      where (rp.start_date, rp.end_date) overlaps (start_boundary, end_boundary)
        and (rp.monthly or rp.election_date = ANY (election_dates))
        and rp2.reporting_period_id is null
      order by rp.start_date;

  end;
$$;

-- example queries should all work
-- select * from potential_reporting_obligations('2021-01-01', '2021-11-30', null); --no elections
-- select * from potential_reporting_obligations('2021-01-01', '2021-11-30', array['2021P', '2021']); --multiple elections
-- select * from potential_reporting_obligations('2021-01-01', '2021-11-30', array['2021', '2021P']); --change order of election codes, same result
-- select * from potential_reporting_obligations('abc', '2021-11-30', array['2021', '2021P']); --bad data, should error
-- select * from potential_reporting_obligations('2021-01-01', '2021-11-30', array['2021', '2021P', 'abc']); --bad data, should error
-- select * from potential_reporting_obligations('2021-01-01', '2021-12-30', array['2021P', '2021']); --should get last period through 12/31, despite the 12/30 end
-- select * from potential_reporting_obligations('2021-10-05', '2021-11-30', array['2021']); --should include special obligation for twenty-one day
