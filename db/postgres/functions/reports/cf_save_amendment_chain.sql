/**
  Submitted reports can get out of sequence. For whatever reason, this function
  serves to fix the chain of reports for either:
  -  an amendment
  -  or external id
 */
CREATE OR REPLACE FUNCTION public.cf_save_amendment_chain(p_report_id INTEGER, p_submitted_at date,
                                                          p_amended_report_id INTEGER, p_external_id TEXT)
    RETURNS VOID as
$$
DECLARE
    v_original_superseded_id INTEGER;
    v_current_amends_id      INTEGER;
    v_current_submitted_at   DATE;
    v_current_fund_id        INTEGER;
    v_amended_fund_id        INTEGER;
    v_external_id            TEXT;
BEGIN


    -- Assign: v_current_submitted_at, v_current_amends_id, v_current_fund_id, v_external_id
    select r.submitted_at, ra.report_id, r.fund_id, r.external_id
    into v_current_submitted_at, v_current_amends_id, v_current_fund_id, v_external_id
    from report r
             left join report ra on ra.superseded_id = r.report_id
    where r.report_id = p_report_id;

    -- Assign: v_amended_report_id
    if p_amended_report_id is not null then
        select fund_id
        into v_amended_fund_id
        from report
        where p_amended_report_id = report_id;

        if v_current_fund_id <> v_amended_fund_id then
            raise exception 'Reports must be within the same fund to amend each other.';
        end if;

        if p_report_id = p_amended_report_id then
            raise exception 'A report cannot amend itself.';
        end if;
    end if;

    -- trap: fund_id/report_id check
    -- do not go further --
    if v_current_fund_id is null or
       (v_amended_fund_id is null and
        p_amended_report_id is not null) then
        raise exception 'Invalid report id.';
    end if;


    -- Update: submitted at date
    if p_submitted_at is distinct from v_current_submitted_at and p_submitted_at is not null then
        update report set submitted_at = p_submitted_at where report_id = p_report_id;
    end if;

    -- Assign: overwrite the v_external_id if params value exists
    if p_external_id is not null then
        v_external_id = p_external_id;
    end if;

    -- report amendment chain fix
    if p_amended_report_id is distinct from v_current_amends_id then

        -- Assign: v_original_supersede_id
        select superseded_id into v_original_superseded_id from report where report_id = p_amended_report_id;

        -- Amendment chain
        update report
        set superseded_id = v_original_superseded_id,
            external_id   = v_external_id
        where report_id = p_report_id;

        update report
        set superseded_id = null,
            external_id   = v_external_id
        where superseded_id = v_original_superseded_id
          and report_id <> p_report_id;

        update report
        set superseded_id = p_report_id,
            external_id   = v_external_id
        where report_id = p_amended_report_id;

        update report
        set superseded_id = null,
            external_id   = v_external_id
        where superseded_id = p_report_id
          and report_id is distinct from p_amended_report_id;
    else
        -- for a single case
        update report
        set external_id = v_external_id
        where report_id = p_report_id;
    end if;
END
$$ LANGUAGE plpgsql;