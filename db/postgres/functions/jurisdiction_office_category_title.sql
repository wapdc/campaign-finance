CREATE OR REPLACE FUNCTION jurisdiction_office_category_title(jurisdiction_name varchar, jurisdiction_category int, office_code varchar, county_name varchar)
    RETURNS varchar  AS $$

BEGIN
    case when jurisdiction_category = 1 then return 'EXECUTIVE';
        when office_code = '12' then return 'STATE SENATE';
        when office_code = '13' then return 'STATE HOUSE OF REPRESENTATIVES';
        when jurisdiction_category = 2 then return jurisdiction_name;
        when office_code = '14' then return 'SUPREME COURT';
        when office_code = '15' then return 'APPEALS COURT';
        when office_code = '16' then return 'SUPERIOR COURT';
        when office_code = '17' then return 'DISTRICT COURT';
        when office_code = '18' then return 'MUNICIPAL COURT';
        when jurisdiction_category = 3 then return jurisdiction_name;
        else return county_name || ' CO';
        end case;
END
$$ LANGUAGE plpgsql IMMUTABLE;

alter function jurisdiction_office_category_title(varchar, int, varchar, varchar) owner to wapdc;