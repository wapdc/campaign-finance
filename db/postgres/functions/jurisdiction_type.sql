CREATE OR REPLACE FUNCTION jurisdiction_type(category integer)
RETURNS varchar  AS $$

    BEGIN
        CASE category
            WHEN 1 THEN RETURN 'Statewide';
            WHEN 4 THEN RETURN 'Legislative';
            WHEN 5 THEN RETURN 'Judicial';
            WHEN 14 THEN RETURN 'Appointed';
            WHEN 15 THEN RETURN 'Appointed';
            WHEN 16 THEN RETURN 'Appointed';
            ELSE RETURN 'Local';
        END CASE;
    END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

alter function jurisdiction_type(integer) owner to wapdc;