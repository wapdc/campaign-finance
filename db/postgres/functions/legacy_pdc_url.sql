CREATE OR REPLACE FUNCTION legacy_pdc_url()
    RETURNS VARCHAR AS
$func$
SELECT n as na
FROM (
      select url.value as n from wapdc_settings url
                            join wapdc_settings stage on url.stage = stage.value
      where stage.property = 'stage' and url.property = 'legacy_pdc_url'
     ) q
$func$ LANGUAGE sql IMMUTABLE;