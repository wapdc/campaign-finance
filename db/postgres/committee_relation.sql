CREATE TABLE committee_relation (
  relation_id SERIAL,
  committee_id INT,
  registration_id INT,
  relation_type VARCHAR(128),
  target_id INT,
  target_name VARCHAR(255),
  stance VARCHAR(128),
  updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NOW(),
  FOREIGN KEY (committee_id) references committee(committee_id) ON DELETE CASCADE,
  PRIMARY KEY (relation_id)
)
