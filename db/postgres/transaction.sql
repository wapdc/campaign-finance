drop table if exists transaction cascade;
create table transaction (
  transaction_id serial primary key,
  fund_id int not null,
  report_id int,
  origin text,
  category text NOT NULL,
  act text NOT NULL,
  amount numeric (10,2) not null,
  inkind boolean not null DEFAULT FALSE,
  itemized boolean not null DEFAULT TRUE,
  transaction_date date,
  created_at timestamp(0) with time zone default now(),
  updated_at timestamp(0) with time zone default now(),
  parent_id int,
  external_guid text,
  external_type text,
  legacy_id int,
  legacy_table text,
  legacy_form_type text,
  description text,
  foreign key (fund_id) references fund(fund_id),
  FOREIGN KEY (report_id) references report(report_id) ON DELETE CASCADE
);
create index idx_transaction_fund_id on transaction(fund_id);
create index idx_transaction_report_id on transaction(report_id);
create index idx_transaction_category on transaction(category);
create index idx_transaction_legacy_id on transaction(legacy_id);
