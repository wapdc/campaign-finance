-- Fixes to surplus accounts from Colin
--
-- Was reverted by Kim, orphaned record. No record to move in committee_authorization or committee_api_key
delete from committee where filer_id = 'SALTD--202' AND pac_type = 'surplus';

-- Change mini surplus to candidate committee. Was filed in error. Filer will need to amend.
update committee set election_code = 2019 where filer_id = 'CHARJ  277' and pac_type = 'surplus';
update committee set end_year = 2019  where filer_id = 'CHARJ  277' and pac_type = 'surplus';
update committee set continuing = false  where filer_id = 'CHARJ  277' and pac_type = 'surplus';
update committee set pac_type = 'candidate' where filer_id = 'CHARJ  277' and pac_type = 'surplus';

-- Change mini surplus to candidate committee. Was filed in error. Filer will need to amend.
update committee set election_code = 2019 where filer_id = 'MCDOS  823' and pac_type = 'surplus';
update committee set end_year = 2019  where filer_id = 'MCDOS  823' and pac_type = 'surplus';
update committee set continuing = false  where filer_id = 'MCDOS  823' and pac_type = 'surplus';
update committee set pac_type = 'candidate' where filer_id = 'MCDOS  823' and pac_type = 'surplus';

-- Change mini surplus to candidate committee. Was filed in error. Filer will need to amend.
update committee set election_code = 2019 where filer_id = 'SILVD  272' and pac_type = 'surplus';
update committee set end_year = 2019  where filer_id = 'SILVD  272' and pac_type = 'surplus';
update committee set continuing = false  where filer_id = 'SILVD  272' and pac_type = 'surplus';
update committee set pac_type = 'candidate' where filer_id = 'SILVD  272' and pac_type = 'surplus';

-- Surplus committee filed in error.
update committee_api_key set committee_id = 22419 where committee_id = 22888;
update registration set committee_id = 22419 where committee_id = 22888;
delete from committee where filer_id = 'SALTD--202' AND pac_type = 'surplus';

-- Surplus committee filed in error.
update committee_authorization set committee_id = 23206 where committee_id = 22231;
update committee_api_key set committee_id = 23206 where committee_id = 22231;
update registration set committee_id = 23206 where committee_id = 22231;
delete from committee where filer_id = 'SALTD--202' AND pac_type = 'surplus';

-- Delete extraneous surplus acount. No record to move in committee_authorization or committee_api_key
update registration set committee_id = 4904 where committee_id = 23285;
delete from committee where filer_id = 'DELVJ  338' AND pac_type = 'surplus';
