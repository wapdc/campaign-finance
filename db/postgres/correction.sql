drop table if exists correction cascade;
create table correction (
                            transaction_id int primary key,
                            original_amount numeric (10,2),
                            corrected_amount numeric (10,2),
                            foreign key (transaction_id) references transaction(transaction_id) on delete cascade
);
