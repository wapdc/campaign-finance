alter table jurisdiction_office
add column limit_category int;
alter table jurisdiction_office add constraint fk_limit_category_id foreign key (limit_category) references contribution_limit_category (category_id);

update jurisdiction_office as jo
set limit_category = (select category_id from contribution_limit_category where code = 'judicial')
from jurisdiction j
where jo.jurisdiction_id = j.jurisdiction_id and j.category = 5;

update jurisdiction_office as jo
set limit_category = (select category_id from contribution_limit_category where code = 'state-port')
from jurisdiction j
where jo.jurisdiction_id = j.jurisdiction_id and j.category in (1,8);

update jurisdiction_office as jo
set limit_category = (select category_id from contribution_limit_category where code = 'default')
from jurisdiction j
where jo.jurisdiction_id = j.jurisdiction_id and (j.category in (2,3,4,6,7,10,11,12,13) or j.jurisdiction_id in (3504,3505,4197));