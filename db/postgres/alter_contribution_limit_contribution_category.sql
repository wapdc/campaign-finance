alter table contribution_limit
add column limit_category int;
alter table contribution_limit add constraint fk_limit_category_id foreign key (limit_category) references contribution_limit_category(category_id);

update contribution_limit
set limit_category = (select category_id from contribution_limit_category where code = 'judicial')
where office_type = 'JD';

update contribution_limit
set limit_category = (select category_id from contribution_limit_category where code = 'state-port')
where office_type in ('SE');

update contribution_limit
set limit_category = (select category_id from contribution_limit_category where code = 'default')
where office_type in ('CL');