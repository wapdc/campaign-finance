drop table if exists expense cascade;
create table expense (
  transaction_id int primary key,
  anonymous boolean NOT NULL DEFAULT TRUE,
  contact_guid text,
  name text,
  address text,
  city text,
  state text,
  postcode text,
  category text,
  geocoded_address text,
  foreign key (transaction_id) references transaction(transaction_id) on delete cascade
);
create index idx_expense_category on expense(category);
