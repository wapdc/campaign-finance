CREATE OR REPLACE FUNCTION target_on_fund_function()
    RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.target_id IS NOT NULL AND NEW.target_id IS DISTINCT FROM OLD.target_id)
    THEN
        IF(NEW.target_type = 'committee')
            THEN
                NEW.committee_id := NEW.target_id;
            ELSE
                --change for future lobbyist functionality
                NEW.committee_id := null;
            END IF;
    ELSE
        IF(NEW.committee_id IS DISTINCT FROM OLD.committee_id AND NEW.committee_id IS NOT NULL)
        THEN
            NEW.target_id := NEW.committee_id;
            NEW.target_type := 'committee';
        END IF;
    END IF;

    RETURN NEW;
END;

$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS target_on_fund_trigger ON fund cascade;

CREATE TRIGGER target_on_fund_trigger
    BEFORE INSERT OR UPDATE ON fund
    FOR EACH ROW EXECUTE FUNCTION target_on_fund_function();

