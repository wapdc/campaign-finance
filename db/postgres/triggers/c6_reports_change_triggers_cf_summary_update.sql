/**
  The purpose of this trigger is to cause an event that will result in an open data update to the
  campaign_finance_summary dataset whenever a C6 report record is updated and superseded is not null. The goal is to
  force recomputing the for/against totals.

  To account for the possibility that a filer_id appeared on a previous report, but not the new report during amendment,
  the trigger gathers up all filer_ids that appear on this report and any previous report in the amendment chain. This
  approach is more efficient than triggering on the reports that have been amended because the list of filer_ids are
  mostly redundant across amendments.

  The only thing not accounted for is when an existing identified_entity record is manually manipulated. There is a
  trigger on c6_identified_entity specifically for that purpose. It would generally happen when staff manually assign
  a filer_id.

  The trigger does nothing when a superseded report record is inserted or updated.

  Ultimately, this trigger works by upserting a row to the c6_candidacy table and that table is watched by the
  data distributor.
 */

create or replace function c6_reports_change_trigger_function()
    returns trigger as
$$
declare
    report int;
    superseded_report int;
begin
    if (tg_op = 'INSERT' or tg_op = 'UPDATE') then
        report = new.repno;
        superseded_report = new.superseded;
    else
        report = old.repno;
        superseded_report = old.superseded;
    end if;

    if (superseded_report is null and (tg_op = 'INSERT' or tg_op = 'UPDATE' or tg_op = 'DELETE')) then
        -- Get every candidacy_id from every filer_id/election_year that appears on this report or any other report in
        -- the amendment chain so that we can get all the filer_ids that appear on any of them. The reason that we need
        -- to do the amended reports is because a candidate might be listed on the original report but not on the new
        -- report and we need to update their campaign_finance_summary record. There's a bit of a weird situation where
        -- the election year of this report might differ with the election year of some amended report so when we lookup
        -- the candidacy_id, we use the election year on the particular report. Upsert the values to the c6_candidacy
        -- table, just performing an x = x noop update when they exist so it will trigger a change record for open data.
        insert into c6_candidacy (candidacy_id)
        with recursive t(parent_id) as (
            select r.repno, c6ie.filer_id, r.election_year
            from c6_reports r
                     join c6_identified_entity c6ie on r.repno = c6ie.repno
            where r.repno = report
            union
            select r.repno, c6ie.filer_id, r.election_year
            from c6_reports r
                     join c6_identified_entity c6ie on r.repno = c6ie.repno
                     join t on r.superseded = t.parent_id
        )
        select distinct c.candidacy_id
        from t
                 join candidacy c on t.filer_id = c.filer_id and t.election_year = c.election_code
        on conflict on constraint c6_candidacy_pkey
            do update set candidacy_id = c6_candidacy.candidacy_id;
    end if;

    case tg_op
        when 'INSERT', 'UPDATE' then
            return new;
        when 'DELETE' THEN
            return old;
        else
            -- Really isn't needed but I'm putting this here in case someone alters the code
            -- without realizing not returning the row will cancel the db operation.
            return new;
        end case;
end;
$$
    language 'plpgsql';

drop trigger if exists od_c6_reports_event_trigger on c6_reports;
create trigger od_c6_reports_event_trigger
    before insert or update or delete on c6_reports for each row
execute procedure c6_reports_change_trigger_function();
