create trigger committee_contact_address_clean_and_geocode_trigger before insert or update
    on committee_contact for each row execute procedure address_clean_and_geocode_trigger_function();
