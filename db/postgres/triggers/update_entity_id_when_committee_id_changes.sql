CREATE OR REPLACE FUNCTION update_entity_id()
    RETURNS TRIGGER AS $$
BEGIN
    IF ((TG_OP = 'INSERT' OR TG_OP = 'UPDATE')
        AND NEW.committee_id IS NOT NULL
        AND (OLD IS NULL OR NEW.committee_id IS DISTINCT FROM OLD.committee_id))
    THEN
        NEW.entity_id := (SELECT person_id FROM committee WHERE committee_id = NEW.committee_id);
    END IF;
    RETURN NEW;
END;

$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_entity_id_trigger ON fund;

CREATE TRIGGER update_entity_id_trigger
    BEFORE INSERT OR UPDATE OF committee_id ON fund
    FOR EACH ROW EXECUTE FUNCTION update_entity_id();


