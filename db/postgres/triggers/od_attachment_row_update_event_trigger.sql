/**
  The purpose of this trigger is to cause an event that will result in an open data update when an attachment is added,
  update or deleted and the attachment update cannot produce a sufficient update event. For example,
  od_campaign_finance_reports contains a single column with the json representation of all attachments for that report.
  When an attachment is deleted, the od_campaign_finance_reports dataset requires an update event for the row based on
  the attachment target_id (report.report_id), but target_id is unavailable because delete events only pass the primary
  key.

  If this trigger causes a noop event on the report table before the delete, we can use the pre-row to update the
  correct report table row which will fire the open data update.

  If adding to this trigger for actions on other tables, be sure to do the noop update on a column that is not indexed
  if possible so there is no index update that would hurt performance. 'set somefield = somefield' is sufficient to fire
  the update event.
 */

create or replace function od_attachment_row_update_event_trigger_function()
    returns trigger as
$$
begin
    if tg_op = 'DELETE'
    then
        -- Update the report table so that od_campaign_finance_reports will be updated to reflect the attachment delete.
        if OLD.target_type = 'report'
        then
            update report set report_type = report_type where report_id = OLD.target_id;
        end if;
        return OLD;
    else
        -- Really isn't needed because the trigger is 'before delete' but I'm putting this hear in case someone added
        -- insert/update without realizing not returning the row will cancel the db operation.
        return NEW;
    end if;
end
$$
    language 'plpgsql';

drop trigger if exists od_attachment_row_update_event_trigger on attachment;
create trigger od_attachment_row_update_event_trigger
    before delete on attachment
    for each row execute procedure od_attachment_row_update_event_trigger_function();