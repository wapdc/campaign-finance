create trigger official_address_clean_and_geocode_trigger before insert or update
    on official for each row execute procedure address_clean_and_geocode_trigger_function();
