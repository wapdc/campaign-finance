create trigger expense_address_clean_and_geocode_trigger before insert or update
    on expense for each row execute procedure address_clean_and_geocode_trigger_function();
