create trigger candidacy_address_clean_and_geocode_trigger before insert or update
    on candidacy for each row execute procedure address_clean_and_geocode_trigger_function();
