/**
  The purpose of this trigger is to cause an event that will result in an open data update to the
  campaign_finance_summary dataset whenever a c6_identified_entity filer_id is directly changed. Most changes are
  tracked when a c6_reports record changes but we know that there are direct updates to the filer_id. We need to make
  sure that campaign_finance_summary is updated for both the old and the new value. We want to ignore row inserts and
  any update where the filer_id does not change. Inserts happen when a report is filed and there is another trigger on
  the c6_reports table for that purpose. We should handle deletes in case there are manual actions but they really
  should not happen.

  Ultimately, this trigger works by upserting a row to the c6_candidacy table and that table is watched by the
  data distributor.
 */

create or replace function c6_identified_entity_change_trigger_function()
    returns trigger as
$$
begin
    if (tg_op = 'UPDATE' and new.filer_id is distinct from old.filer_id) then
        insert into c6_candidacy (candidacy_id)
        select c.candidacy_id from candidacy c join c6_reports r on c.election_code = r.election_year
            and ((c.filer_id = old.filer_id and r.repno = old.repno) or c.filer_id = new.filer_id and r.repno = new.repno)
        on conflict on constraint c6_candidacy_pkey
            do update set candidacy_id = c6_candidacy.candidacy_id;
        return new;
    elseif (tg_op = 'DELETE') then
        insert into c6_candidacy (candidacy_id)
        select c.candidacy_id from candidacy c join c6_reports r on c.election_code = r.election_year
            and c.filer_id = old.filer_id and r.repno = old.repno
        on conflict on constraint c6_candidacy_pkey
            do update set candidacy_id = c6_candidacy.candidacy_id;
        return old;
    else
        -- Really isn't needed but I'm putting this here in case someone alters the code
        -- without realizing not returning the row will cancel the db operation.
        return new;
    end if;
end;
$$
    language 'plpgsql';

drop trigger if exists od_c6_identified_entity_event_trigger on c6_identified_entity;
create trigger od_c6_identified_entity_event_trigger
    before update or delete on c6_identified_entity for each row
execute procedure c6_identified_entity_change_trigger_function();
