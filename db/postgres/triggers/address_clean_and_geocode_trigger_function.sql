create function address_clean_and_geocode_trigger_function()
    returns trigger as
$$
BEGIN
    IF tg_op = 'UPDATE'
        THEN
        if OLD.address = NEW.address and OLD.city = NEW.city and OLD.state = NEW.state and OLD.postcode = NEW.postcode
            then
            return NEW;
        end if;
    END IF;

    NEW.address = trim(NEW.address);
    NEW.city = trim(NEW.city);
    NEW.state = trim(NEW.state);
    NEW.postcode = trim(NEW.postcode);
    NEW.address_hash = hash_address_4(NEW.address, NEW.city, NEW.state,
                                                      NEW.postcode);

    IF NEW.address is not null or NEW.city is not null or NEW.state is not null or NEW.postcode is not null
        THEN
        INSERT INTO geocoded_address_queue
        SELECT
            hash_address_4(NEW.address, NEW.city, NEW.state, NEW.postcode),
            NEW.address,
            NEW.city,
            NEW.state,
            NEW.postcode,
            0,
            current_timestamp,
            'new'
        ON CONFLICT (id) do nothing;
    END IF;

    RETURN NEW;
END
$$
    language 'plpgsql';