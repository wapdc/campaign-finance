create trigger contribution_address_clean_and_geocode_trigger before insert or update
    on contribution for each row execute procedure address_clean_and_geocode_trigger_function();
