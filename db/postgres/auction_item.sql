drop table if exists auction_item cascade;
create table auction_item (
                          donation_transaction_id int primary key,
                          buy_transaction_id int unique NOT NULL,
                          item_description text,
                          fair_market_value numeric (10,2),
                          sale_amount numeric (10,2),
                          foreign key (donation_transaction_id) references transaction(transaction_id) on delete cascade,
                          foreign key (buy_transaction_id) references transaction(transaction_id)
);
