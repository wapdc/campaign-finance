update registration
set committee_id = 5026
from committee c
WHERE c.committee_id = registration.committee_id
  AND c.filer_id = 'DOGLB *507'
  AND c.start_year = 2016;

update registration
set committee_id = 6500
from committee c
WHERE c.committee_id = registration.committee_id
  AND c.filer_id = 'FRAMN *111'
  AND c.start_year = 2016;

update registration
set committee_id = 7155
from committee c
WHERE c.committee_id = registration.committee_id
  AND c.filer_id = 'GOODR *033'
  AND c.start_year = 2016;

update registration
set committee_id = 22795
from committee c
WHERE c.committee_id = registration.committee_id
  AND c.filer_id = 'JENKW *350'
  AND c.start_year = 2018;

update registration
set committee_id = 10520
from committee c
WHERE c.committee_id = registration.committee_id
  AND c.filer_id = 'KLOBS *033'
  AND c.start_year = 2016;

update registration
set committee_id = 12334
from committee c
WHERE c.committee_id = registration.committee_id
  AND c.filer_id = 'MCBRJ *033'
  AND c.start_year = 2016;

update registration
set committee_id = 23324
from committee c
WHERE c.committee_id = registration.committee_id
  AND c.filer_id = 'STEEM *816'
  AND c.start_year = 2017;

update registration
set committee_id = 21988
from committee c
WHERE c.committee_id = registration.committee_id
  AND c.filer_id = 'ZEIGH *371'
  AND c.start_year = 2011;
