# Campaign Finance

This application facilitates the disclosure of who gave, how much and what for in elections.

The project is currently under development. For additional information please see the [ Project Wiki](https://gitlab.com/wapdc/campaign-finance/wikis/home)

## What code goes in this project
All PHP web code that is about Campaign Finance, including campaign registration(C1) and campaign expenditure tracking (C3-6). 
New code for these applications should be included in this project.  Before committing code to this project you should 
ask: 

* Is the code also relevant to financial affairs (F1), Compliance Case Tracking or Lobbying? If so use the wapdc/core project. 
* Is the code used to display information to voters or the public?  If so consider one of the OpenData or Info Search projects.

## API Documentation

The Washington State Public Disclosure Commission provides a REST API for interacting with campaign finance reporting.  See 
the [ Rest API Documentation ](doc/vendorAPI/README.md) for additional information.

## ORCA Data model
As the ORCA java product transitions to a web application most tables are being created and/or stored in the RDS.  
Additional documentation can be found in the [ ORCA Data Model ](doc/OrcaDataModel.md) design document.

## Political Advertising and Electioneering Communications

As C6 is replaced with an application that facilitates the filing of C6's the ORCA schema is being extended to handle 
C6 reporting requirements. Additional documentation can be found in the [ Political Ads and Electioneering Communication  ](doc/IEDataModel.md) design document.

## Developer Documentation 

**[Glossary of Terms](doc/Glossary.md)** - Defines the terms we use in the data model. 

**[Access Control](doc/Access-Model.md)** - Defines how access to filing data is controlled
  in the new system. 
  
**[Report Model](doc/ReportModel.md)** - Defines the structure and validations for finance reports, (e.g. C3/C4)

**[Error Messges](doc/ErrorMessages.md)** - Lists the error messages received by ORCA when posting reports.

**[ORCA Web Service](doc/ORCAWebService.md)** - Defines the strategy for processing submissions 