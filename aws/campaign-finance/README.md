# campaign-finance

This project contains the AWS resources required to create and restore campaign backups, and will be the future location
of AWS lambda functions required for other AWS method calls. 

## Creating a backup file

The campaigns vue ui will request a JWT token and a rest URL that includes the basic information about the backup.  It will then make a
call to the CampaignFinanceBackupApi to execute the requestCampaignBackup.  Because the lambda may need to change as we delete and redeploy 
stacks the URL for this request will be stored in the wapdc_settings table on the RDS.  

In order to allow the generation of the backup file to last for more than the 30 seconds processing time imposed by the lambda the requestCampaignBackup lambda 
will simply use the AWS sdk to invoke a generateRestoreFile. 

The generateRestoreFile lambda is responsible for:

- Calling database functions to get the JSON that is required. 
- Digitally signing the backup. 
- Compressing the data using gzip
- Placing the data on an S3 bucket
- Calling the sendgrid api to send a message to the person indicated in the request. 


## Restoring a backup from the file

The campaign vue application requests a special key for facilitating the upload of the restore file requested to the database 
after having inserted relevant information for the restore request into the database in the private.restore_request table.  
Writing a file to the RestoreRequest bucket triggers the generateRestoreFile lambda function that is responsible for:

- Lookup the restore request metadata from the RDS 
- Unzipping the backup file
- Validating the signature (unless the request metadata says to skip this step)
- Calling the RDS restore campaign function with the json from the backup file to recreate the data.

## Registering pertinent endpoint information

As described above, since the Rest API associated with this end point may move, relavent locations of the items are stored 
in the wapdc_settings.  As part of the deployment and/or cloning processes we will invoke the registerEndpoints  lambda to register the
urls and configurations for the REST apis in the wapdc_settings table in the RDS. 


## Development prerequisites

The AWS SAM CLI is an extension of the AWS CLI that adds functionality for building and testing Lambda applications. It uses Docker to run your functions in an Amazon Linux environment that matches Lambda. It can also emulate your application's build environment and API.

To use the AWS SAM CLI, you need the following tools:

* AWS SAM CLI - [Install the AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html).
* Node.js - [Install Node.js 18](https://nodejs.org/en/), including the npm package management tool.
* Docker - [Install Docker community edition](https://hub.docker.com/search/?type=edition&offering=community).

After having these products installed, build install and build your application by using the following commands.  Unless 
noted otherwise, all of the commands described below should be run from inside the aws/campaign-finance directory 
in the campaign-finance project. 

```bash
cd aws/campaign-finance
npm install
sam build
```

## Testing the requestRestore ui

Lambda UI functions can be started locally in your docker container by using the following commands.  

```bash
sam local start-api --env-vars local/env.json --warm-containers EAGER
```
In order for your local apollo environment to understand where this container is you will need to register the URL in your 
local RDS database by executing a command similar to the following: 

```bash
sam local invoke RegisterBackupUrlFunction --event events/registerUrl.json --env-vars local/env.json
```
The url in the sample events/registerUrl.json file can be used as an example.

### Testing JWT authorization locally

The sam local environment does not invoke the authorizers that are defined in the SAM template, so if you want to test
the JWT decoding process you need to do that by manually invoking the lambda.  Use events/authorize.json as event as an example,
but create a local copy and modify the JWT authorizer token in the payload.

```bash
sam local invoke JwtAuthorizeFunction --event local/authorize.json --env-vars local/env.json
```

### Testing S3 related functions locally

The sam local environment does not support S3, but you can run through the logic for determining 
backup contents by manually invoking the lambda. 

```bash
sam local invoke CampaignRestoreFunction --event events/restoreUploaded.json --env-vars local/env.json
```

@TODO: We need to decide whether we'll have a local way of reading files that does not rely on 
the lambda get object function. 

## Manually deploying an application to demo

```bash
my-application$ sam deploy
```

Open the [**Applications**](https://console.aws.amazon.com/lambda/home#/applications) page of the Lambda console, and choose your application. When the deployment completes, view the application resources on the **Overview** tab to see the new resource. Then, choose the function to see the updated configuration that specifies the dead-letter queue.

## Fetch, tail, and filter Lambda function logs

To simplify troubleshooting, the AWS SAM CLI has a command called `sam logs`. `sam logs` lets you fetch logs that are generated by your Lambda function from the command line. In addition to printing the logs on the terminal, this command has several nifty features to help you quickly find the bug.

**NOTE:** This command works for all Lambda functions, not just the ones you deploy using AWS SAM.

```bash
my-application$ sam logs -n helloFromLambdaFunction --stack-name sam-app --tail
```

**NOTE:** This uses the logical name of the function within the stack. This is the correct name to use when searching logs inside an AWS Lambda function within a CloudFormation stack, even if the deployed function name varies due to CloudFormation's unique resource name generation.

You can find more information and examples about filtering Lambda function logs in the [AWS SAM CLI documentation](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-logging.html).

## Unit tests

Tests are defined in the `__tests__` folder in this project. Use `npm` to install the [Jest test framework](https://jestjs.io/) and run unit tests.

```bash
my-application$ npm install
my-application$ npm run test
```

