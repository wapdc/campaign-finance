
// Configuration for unit tests comes from .env in project root.
import env from 'dotenv';
env.config();

import {mockClient} from "aws-sdk-client-mock";
import {S3Client, PutObjectCommand, GetObjectCommand, DeleteObjectCommand} from "@aws-sdk/client-s3";
import {jest, beforeEach, expect, test,describe} from '@jest/globals';
import {wapdcDb} from "../../src/WapdcDb.js";
import mailer from "../../src/Mailer.js"
import {readFile} from 'fs/promises';
import pako from 'pako';
import candidateCampaign from "../data/sample-campaign.json" assert {type: "json"};
import * as signer from '../../src/Signer'

const mockS3 = mockClient(S3Client)
import {backupProcessor} from "../../src/BackupProcessor.js";



const lastMailData = {
  templateId: null,
  name: null,
  email: null,
  parameters: null
}

const wapdcSettings = {
  apollo_url: "https://apollo-d8.lndo.site",
}

beforeEach(() => {
  jest.resetAllMocks();
  mockS3.reset();
  jest.mock('../../src/WapdcDb.js');
  jest.mock('../../src/Mailer.js');
  wapdcDb.getCampaignArchive = jest.fn().mockReturnValue(candidateCampaign);
  wapdcDb.getWapdcSetting = jest.fn().mockImplementation((property) => wapdcSettings[property]);
  mailer.sendTemplatedMessage = jest.fn().mockImplementation((templateId, email, name, data) => {
    lastMailData.templateId = templateId;
    lastMailData.email = email;
    lastMailData.name = name;
    lastMailData.data = data;
  })
  wapdcDb.recordCampaignArchive = jest.fn();
})


describe('Backup and Restore data',  () => {

  test('Error handling when restoring a campaign', async () => {
    wapdcDb.restoreCampaign = jest.fn().mockImplementation(async () => {
      throw new Error('Error restoring file');
    });
    wapdcDb.getRestoreRequest = jest.fn();
    wapdcDb.deleteRestoreRequest = jest.fn();

    const restoreRequest = {request_id: 12345, fund_id: 23456, email: 'test@noreply.com', validate: true, committee_name: "Test Committee"};
    wapdcDb.getRestoreRequest.mockResolvedValue(restoreRequest);

    const fileData = {
      object: {
        key: "restore_12345",
        size: "1024"
      }
    };

    // Mock the return call to the S3 so that it returns the sample-restore-file from the data directory.
    const backupFileContents = await readFile('__tests__/data/sample-campaign.gz');

    const s3Response = {
      Body: {
        transformToByteArray: () => {
          return backupFileContents;
        }
      }
    }
    mockS3.on(GetObjectCommand).resolves(s3Response);

    // Call the backup processor
    await backupProcessor.restoreCampaign(fileData);

    expect(mailer.sendTemplatedMessage).toHaveBeenCalledTimes(1);
    expect(lastMailData.templateId).toBe('d-742e882455494204b0673d17a0571f09');
    expect(lastMailData.email).toBe(restoreRequest.email);
    expect(lastMailData.data.committee_name).toBe(restoreRequest.committee_name);
    expect(lastMailData.data.message).toBe('Error restoring campaign: Error restoring file');

  });

  test('Create a backup file', async () => {

    const event = {
      fund_id: 12345,
      email: "test@noreply.com",
      committee_name: "Test Committee"
    }

    await backupProcessor.generateBackup(event);

    const calls = mockS3.commandCalls(PutObjectCommand);
    expect(calls).toHaveLength(1);
    const input = calls[0].args[0].input;

    expect(input.Bucket).toBe(process.env.BACKUP_BUCKET);
    expect(input.Key).toBe('backup_' + event.fund_id + '.gz');
    const backupFileContents = await readFile('__tests__/data/sample-campaign.jwt', 'ascii');
    const unzippedExpectedData = pako.ungzip(input.Body, {to: "string"});

    expect(signer.decode(unzippedExpectedData)).toMatchObject(signer.decode(backupFileContents));

    expect(wapdcDb.recordCampaignArchive).toHaveBeenLastCalledWith(event.fund_id, input.Key);
    expect(mailer.sendTemplatedMessage).toHaveBeenCalledTimes(1);
    expect(lastMailData.templateId).toBe('d-6e7c6a0fb56e4100a210c937d4cdaf3d');
    expect(lastMailData.email).toBe(event.email);
    expect(lastMailData.data.committee).toBe(event.committee_name);
    expect(lastMailData.data.url).toBe(wapdcSettings.apollo_url + "/campaigns/ORCA/campaign/12345/campaign-info#archive");
  });


  test('Restore a campaign', async() => {

    wapdcDb.restoreCampaign = jest.fn();
    wapdcDb.getRestoreRequest = jest.fn();
    wapdcDb.deleteRestoreRequest = jest.fn();
    const restoreRequest = {request_id: 12345, fund_id: 23456, email: 'test@noreply.com', validate: true, committee_name: "Test Committee"};
    // noinspection JSCheckFunctionSignatures
    wapdcDb.getRestoreRequest.mockResolvedValue(restoreRequest)

    const fileData = {
      object: {
        key: "restore_12345",
        size: "1024"
      }
    }

    const fundId = 23456;
    // Mock the database call so that we can determine the data for the fund id

    // Mock the return call to the S3 so that it returns the sample-restore-file from the data directory.
    const backupFileContents = await readFile('__tests__/data/sample-campaign.gz');
    const jsonData = await readFile('__tests__/data/sample-campaign.json', 'ascii');

    const s3Response = {
      Body: {
        transformToByteArray: () => {
          return backupFileContents;
        }
      }
    }
    mockS3.on(GetObjectCommand).resolves(s3Response);

    // Call the backup processor
    await backupProcessor.restoreCampaign(fileData);

    // Ensure that we've called the getRestoreRequest function for the correct request id
    expect(wapdcDb.getRestoreRequest).toHaveBeenLastCalledWith(12345);

    // Ensure that we loaded the correct file.
    {
      const calls = mockS3.commandCalls(GetObjectCommand);
      expect(calls).toHaveLength(1);
      const input = calls[0].args[0].input;
      expect(input.Bucket).toBe(process.env.RESTORE_BUCKET);
      expect(input.Key).toBe(fileData.object.key);
    }

    // ensure that we've called the backup function from the database.
    const expectedCampaign = JSON.parse(jsonData);
    expectedCampaign.iat = 1680822132;
    expectedCampaign.iss = "pdc.wa.gov";
    expect(wapdcDb.restoreCampaign).toHaveBeenLastCalledWith(fundId, JSON.stringify(expectedCampaign));

    // Ensure that we've deleted the restore request
    expect(wapdcDb.deleteRestoreRequest).toHaveBeenLastCalledWith(12345);

    // ensure that we've sent the email indicating the restore request has been complete.
    expect(mailer.sendTemplatedMessage).toHaveBeenCalledTimes(1);
    expect(lastMailData.templateId).toBe('d-e4d5998a5b2b4ea39b7dfa37bc893577');
    expect(lastMailData.email).toBe(restoreRequest.email);
    expect(lastMailData.data.committee).toBe(restoreRequest.committee_name);
    expect(lastMailData.data.url).toBe(wapdcSettings.apollo_url + "/campaigns/ORCA/campaign/23456");

    // Ensure that we've deleted the file from the s3 bucket
    {
      const calls = mockS3.commandCalls(DeleteObjectCommand);
      expect(calls).toHaveLength(1);
      const input = calls[0].args[0].input;
      expect(input.Bucket).toBe(process.env.RESTORE_BUCKET);
      expect(input.Key).toBe(fileData.object.key);
    }

  });
});
