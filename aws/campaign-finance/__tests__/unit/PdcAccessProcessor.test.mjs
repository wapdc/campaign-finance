import env from 'dotenv'

env.config();

import {mockClient} from "aws-sdk-client-mock";
import {jest, beforeEach, expect, test, describe} from "@jest/globals";
import {wapdcDb} from "../../src/WapdcDb.js";
import {LambdaClient, InvokeCommand, InvocationType} from "@aws-sdk/client-lambda";

const mockLambda = mockClient(LambdaClient)
import {pdcAccessProcessor} from "../../src/PdcAccessProcessor.js";

pdcAccessProcessor.stage = 'unit' //Change environment for unit testing purposes

const wapdcSettings = {
    apollo_url: "https://apollo-d8.lndo.site"
}

beforeEach(() => {
    jest.resetAllMocks();
    mockLambda.reset();
    jest.mock('../../src/WapdcDb.js');
    wapdcDb.getWapdcSetting = jest.fn().mockImplementation((property) => wapdcSettings[property]);

    wapdcDb.grantPdcAccess = jest.fn();
    wapdcDb.revokePdcAccess = jest.fn();
})

describe('Requesting PDC Access', () =>{
    test('grant pdc access to demo', async () =>{
        const committee_id = 35041
        const expiration_date = "2023-03-31"

        await pdcAccessProcessor.grantPdcAccess(committee_id, expiration_date);
        const calls = mockLambda.commandCalls(InvokeCommand);
        expect(calls).toHaveLength(1);
        const input = calls[0].args[0].input;

        expect(input.FunctionName).toBe('campaign-finance-demo-pdcAccess');

        let event = pdcAccessProcessor.getDemoPdcAccessPayload('grant', committee_id, expiration_date);
        expect(input.Payload).toBe(JSON.stringify(event));
        expect(input.InvocationType).toBe(InvocationType.Event);

        expect(wapdcDb.grantPdcAccess).toHaveBeenLastCalledWith(committee_id, expiration_date);
    })
    test('revoke pdc access to demo', async () => {
        const committee_id = 35041
        await pdcAccessProcessor.revokePdcAccess(committee_id)

        const calls = mockLambda.commandCalls(InvokeCommand);
        expect(calls).toHaveLength(1)
        const input = calls[0].args[0].input;

        expect(input.FunctionName).toBe('campaign-finance-demo-pdcAccess');
        let event = pdcAccessProcessor.getDemoPdcAccessPayload('revoke', committee_id);
        expect(input.Payload).toBe(JSON.stringify(event));
        expect(input.InvocationType).toBe(InvocationType.Event);

        expect(wapdcDb.revokePdcAccess).toHaveBeenLastCalledWith(committee_id);
    })
})