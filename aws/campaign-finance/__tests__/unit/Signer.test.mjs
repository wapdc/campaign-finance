import env from 'dotenv';
import candidateCampaign from "../data/sample-campaign.json" assert {type: "json"};
import {expect, test} from '@jest/globals';
import * as signer from '../../src/Signer'
env.config();

test('Can sign and verify data', async () =>{
  const token = await signer.sign(candidateCampaign, 'CAMPAIGN_ARCHIVE_SIGNING_HMAC');

  {
    const payload = await signer.decode(token);

    // Check to ensure we can access without the correct key.
    expect(payload.campaignFund[0]).toMatchObject(candidateCampaign.campaignFund[0])
  }

  {
    const payload = await signer.verify(token, 'CAMPAIGN_ARCHIVE_SIGNING_HMAC');

    // Check to ensure we can access without the correct key.
    expect(payload.campaignFund[0]).toMatchObject(candidateCampaign.campaignFund[0])
  }

});

