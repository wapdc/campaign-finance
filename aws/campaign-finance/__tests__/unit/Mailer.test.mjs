// Configuration for unit tests comes from .env in project root.
import env from 'dotenv';
env.config();

import {jest, describe, test, expect} from '@jest/globals';
import mailer from "../../src/Mailer.js";
import axios from 'axios';


jest.mock('axios');
describe('Mailer', () => {
  test('Sends to sendgrid', async() => {

    let sendGridURL, sendGridPayload, sendGridConfig;

    axios.post = jest.fn().mockImplementation((url, payload, config) => {
      sendGridURL = url;
      sendGridPayload = payload;
      sendGridConfig = config;
    });

    // Create a mock message for which to send mail.
    mailer.startMailMerge('mock-template-id');
    mailer.addRecipient('one@example.com', 'Recipient One', {data: 'data one'});
    mailer.addRecipient('two@example.com', 'Recipient Two', {data: 'data two'});
    await mailer.send();

    const expectedSendGridCall =
      {
        subject: null,
        from: {
          email: "pdc@pdc.wa.gov",
          name: "Washington Public Disclosure Commission"
        },
        mail_settings: {sandbox_mode: {enable: false}},
        template_id: "mock-template-id",
        personalizations: [
          {
            to: [{name: "Recipient One", email: "one@example.com"}],
            dynamic_template_data: {data: "data one"}
          },
          {
            to: [{name: "Recipient Two", email: "two@example.com"}],
            dynamic_template_data: {data: "data two"}
          }
        ]
      }

    const expectedConfig = {
      headers: {
        Authorization: process.env.SENDGRID_AUTHORIZATION_KEY,
        "Content-Type": "application/json"
      }
    }

    expect(axios.post).toHaveBeenCalledTimes(1);
    expect(sendGridURL).toBe('https://sendgrid.com/v3/mail/send');
    expect(sendGridPayload).toMatchObject(expectedSendGridCall);
    expect(sendGridConfig).toMatchObject(expectedConfig);
  });
});