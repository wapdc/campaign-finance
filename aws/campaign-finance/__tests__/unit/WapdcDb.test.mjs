import {wapdcDb} from "../../src/WapdcDb.js";
import env from 'dotenv';
import candidateCampaign from "../data/sample-campaign.json" assert {type: "json"};
import {beforeEach, afterEach, expect, test, describe} from '@jest/globals';

env.config();


beforeEach(async () =>{
  await wapdcDb.connect();
  await wapdcDb.beginTransaction();
});

afterEach( async () => {
  await wapdcDb.rollback();
  await wapdcDb.close();
});

describe('Can read and manipulate wapdc settings', function () {

  test('Get and set settings', async () => {
    const value =  await wapdcDb.getWapdcSetting('testing', 'testing');
    expect(value).toBeNull();

    await wapdcDb.setWapdcSetting('testing', 'testing');

    const newValue = await wapdcDb.getWapdcSetting('testing', 'testing');
    expect(newValue).toBe('testing');
  });

  test('Register URL', async () => {
    const url = 'https://register.example.com';
    await wapdcDb.registerUrl(url);
    const registeredUrl = await wapdcDb.getWapdcSetting('campaign_finance_aws_gateway');
    expect(registeredUrl).toBe(url);
  });

});

describe('Test building backups', () => {
  beforeEach(async () =>{
    await wapdcDb.query("select * from private.cf_delete_orca_campaign(1)")
    await wapdcDb.query("delete from private.trankeygen where trankeygen_id < 60");
    await wapdcDb.query("delete from private.reporting_obligation where obligation_id < 60");
  });

  test('restore request retrieval', async () => {
    {
      await wapdcDb.query("insert into private.restore_request(restore_request_id, fund_id, email, validate) values (-1, 12345,  'test@noreply.com', true)");
      const request = await wapdcDb.getRestoreRequest(-1);
      expect(request.fund_id).toBe(12345);
      expect(request.email).toBe('test@noreply.com');
      expect(request.validate).toBe(true);
    }
    {
      await wapdcDb.deleteRestoreRequest(-1);
      const request = await wapdcDb.getRestoreRequest(-1);
      expect(request).toBeNull();
    }
  })

  test('backup to json matches expected', async () => {
    await wapdcDb.restoreCampaign(1, candidateCampaign);
    const restoredCampaign = await wapdcDb.getCampaignArchive(1);
    for(let campaignTable in candidateCampaign){
      for(let item in candidateCampaign[campaignTable]) {
        for(let key in candidateCampaign[campaignTable][item]) {
          if (key === 'rds_updated' || key === 'updated') {
            // console.log(campaignTable + ' : ' + item + ' : ' + key); //useful for debugging if test fails
            if (candidateCampaign[campaignTable][item][key]) {
              expect(restoredCampaign[campaignTable][item][key].substring(0, 10)).toEqual((candidateCampaign[campaignTable][item][key]).substring(0, 10));
            } else {
              expect(restoredCampaign[campaignTable][item][key]).toEqual(null);
            }
          } else {
            expect(restoredCampaign[campaignTable][item][key]).toEqual(candidateCampaign[campaignTable][item][key]);
          }
        }
      }
    }
    // Register a backup filename with the database and make sure that works.
    await wapdcDb.recordCampaignArchive(1, 'backup_1');
    const rows = await wapdcDb.query("SELECT archive_file, archive_expiration from private.campaign_fund where fund_id=$1", [1]);
    expect(rows[0].archive_file).toBe('backup_1');
    expect(rows[0].archive_expiration).toBeTruthy();
  });
});

describe('Committee access controls', () => {
  let committee_id;
  beforeEach (async () => {
    //create committee
    const rows = await wapdcDb.query("INSERT INTO public.committee(name) values ('Test Committee') returning committee_id");
    committee_id = rows[0].committee_id;
  });
  test('Can grant and revoke PDC access on committee', async () =>{
    //call grant access function for PDC access
    let expiration_date = '2029-02-10';
    await wapdcDb.grantPdcAccess(committee_id, expiration_date);

    {
      //look in the committe_authorization table to validate if PDC access has been granted
      let auth = await wapdcDb.query("SELECT * FROM public.committee_authorization ca WHERE ca.committee_id = $1 AND uid = 'PDC' and realm = 'PDC'", [committee_id]);
      expect(auth[0].role).toBe('owner');
      expect(auth[0].expiration_date).toBe(expiration_date);
    }
    //call revoke function
    await wapdcDb.revokePdcAccess(committee_id);

    {
      //look in committee_authorization table is set to today
      const today = new Date();
      const year = today.toLocaleString("default", {year: "numeric"});
      const month = today.toLocaleString("default", {month: "2-digit"});
      const day = today.toLocaleString("default", {day: "2-digit"});
      const expectedDate = year + "-" + month + "-" + day;

      const auth = await wapdcDb.query("SELECT * FROM public.committee_authorization ca WHERE ca.committee_id = $1 AND uid = 'PDC' and realm = 'PDC'", [committee_id]);
      expect(auth[0].expiration_date).toBe(expectedDate)
    }
  })
})




