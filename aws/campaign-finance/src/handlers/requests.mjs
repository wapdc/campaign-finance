import {InvocationType, InvokeCommand, LambdaClient} from "@aws-sdk/client-lambda";
import {wapdcDb} from "../WapdcDb.js";
import {pdcAccessProcessor} from "../PdcAccessProcessor.js";

function defaultResponse() {
  return {
    headers: {
      "Content-Type" : "application/json",
      "Access-Control-Allow-Headers": "Content-Type,Authorization,X-Api-Key,Authorization,X-Requested-With",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET,POST,OPTIONS"
    },
    statusCode: 200,
    body: {
      success: false,
    }
  }
}

function getJsonPayload(event) {

    const payload = event.body ? JSON.parse(event.body) : {};
    if (event.requestContext?.authorizer) {
      for (const property in event.requestContext.authorizer) {
        payload[property] = event.requestContext.authorizer[property];
      }
    }
    return payload;
}

export const requestBackup = async (event) => {
  const apiResponse = defaultResponse();
  try {
    const stage = process.env.STAGE;
    const lambda = new LambdaClient({});
    let payload={};

    if (event.body) {
      payload = JSON.parse(event.body);
    }

    if (event.requestContext.authorizer) {
      if (event.requestContext.authorizer.fund_id) {
        payload.fund_id = parseInt(event.requestContext.authorizer.fund_id);
      }
    }

    // Existing dev environments do not support the triggering of lambdas manually, so we skip  for the dev case
    if (stage === 'dev') {
      apiResponse.body.message = 'Skipping backup in dev environment. To test backup creation manually invoke the lambda.';
      apiResponse.body.success = true;
    }
    else {
      const response = await lambda.send(
        new InvokeCommand({
          FunctionName: `campaign-finance-${stage}-generateCampaignBackup`,
          Payload: JSON.stringify(payload),
          InvocationType: InvocationType.EVENT
        })
      );

      if (response.StatusCode !== 200) {
        apiResponse.statusCode = 500;
        apiResponse.body.message = 'Unable to request archive';
        apiResponse.body.error = `Lambda invocation failed with status code ${response.StatusCode}`
      } else {
        apiResponse.body.message = "Campaign archive copy requested. Please check your email for your archive file."
        apiResponse.body.success = true;
      }
    }

  } catch (error) {
    console.error("Error invoking RequestBackupFunction:", error.message);
    apiResponse.statusCode = 500;
    apiResponse.body.success = false;
    apiResponse.body.message = "Unable to request backup";
    apiResponse.body.error=error.message;
  }
  apiResponse.body = JSON.stringify(apiResponse.body);
  return apiResponse;
};

export const pdcAccess = async (event) => {
  const apiResponse = defaultResponse();
  console.info("event: ", event);

  try {
    await wapdcDb.connect();
    const action = event.pathParameters.action;
    const payload = getJsonPayload(event);

    console.info("Committee info: ", payload.committee_id, action);
    switch (action) {
      case "grant":
        await pdcAccessProcessor.grantPdcAccess(payload.committee_id, payload.expiration_date);
        apiResponse.body.message = "Successfully granted PDC access";
        break;
      case "revoke":
        await pdcAccessProcessor.revokePdcAccess(payload.committee_id);
        apiResponse.body.message = "Successfully revoked PDC access";
        break;
      default:
        // noinspection ExceptionCaughtLocallyJS
        throw new Error("Invalid API call")
    }
    apiResponse.body.success = true;
  } catch(error){
    console.error("Error modifying PDC Access: ", error.message);
    apiResponse.statusCode = 500;
    apiResponse.body.success = false;
    apiResponse.body.message = "Unable to modify PDC access";
    apiResponse.body.error = error.message;
  }
  apiResponse.body = JSON.stringify(apiResponse.body);
  return apiResponse;
}