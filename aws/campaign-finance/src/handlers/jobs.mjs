import {wapdcDb} from "../WapdcDb.js";
import {backupProcessor} from "../BackupProcessor.js";

await wapdcDb.connect();
export const generateBackupFile = async(event) => {
  await backupProcessor.generateBackup(event);
}

/**
 * The payload for this output is expected to be the json that is
 * generated from the aws couldformation describe-stacks command for the
 * deployed instance.
 *
 * @param event
 * @returns {Promise<void>}
 */
export const registerUrl = async(event) => {
  console.info("Registration event", event);
  if (!event && !event.Stacks) {
    throw Error("Invalid or missing event");
  }
  const stack = event.Stacks[0];
  if (!stack || !stack.Outputs) {
    throw Error("Cannot find stack output to extract url");
  }
  const output = stack.Outputs.find(o => o.OutputKey === "ApiGatewayUrl");
  await wapdcDb.registerUrl(output.OutputValue);
}

/**
 * Restore a file given file uploaded by an S3 event.
 * @param event
 * @returns {Promise<void>}
 */
export const restoreCampaign = async(event) => {
  if (event.Records) {
    for (let e in event.Records) {
      console.info("Restoring ", event.Records[e].s3);
      await backupProcessor.restoreCampaign(event.Records[e].s3);
    }
  }
}
