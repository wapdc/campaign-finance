
import * as signer from '../Signer.js'

// Help function to generate an IAM policy
function generatePolicy(principalId, effect, resource, context) {
  var authResponse = {};

  authResponse.principalId = principalId;
  if (effect && resource) {
    var policyDocument = {};
    policyDocument.Version = '2012-10-17';
    policyDocument.Statement = [];
    var statementOne = {};
    statementOne.Action = 'execute-api:Invoke';
    statementOne.Effect = effect;
    statementOne.Resource = resource;
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }

  // Optional output with custom properties of the String, Number or Boolean type.
  authResponse.context = context;
  return authResponse;
}

export const authorize = async (event, context, callback) => {
  const token = event.authorizationToken;
  let effect, payload;
  try{
    payload = await signer.verify(token, 'HMAC_KEY');
    effect = 'Allow';
  }
  catch(e) {
    console.log(e);
    payload = null;
  }
  // If we didn't get any data, then deny the request.
  if (!payload) {
    effect = 'Deny';
    payload = {}
  }

  const policy = generatePolicy('user', effect, event.methodArn, payload);
  callback(null, policy);
}
