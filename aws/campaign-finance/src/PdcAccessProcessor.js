import {InvocationType, InvokeCommand, LambdaClient} from "@aws-sdk/client-lambda";
import {wapdcDb} from "./WapdcDb.js";

class PdcAccessProcessor {
  constructor() {
    this.stage = process.env.STAGE;
  }

  async grantPdcAccess(committee_id, expiration_date){
    await wapdcDb.grantPdcAccess(committee_id, expiration_date)
    // remove dev check for debugging purposes
    if(this.stage !== 'demo' && this.stage !== 'dev'){
      let payload = this.getDemoPdcAccessPayload('grant', committee_id, expiration_date)
      await this.requestDemoPdcAccess(payload)
    }
  }

  async revokePdcAccess(committee_id){
    await wapdcDb.revokePdcAccess(committee_id)
    // remove dev check for debugging purposes
    if(this.stage !== 'demo' && this.stage !== 'dev'){
      let payload = this.getDemoPdcAccessPayload('revoke', committee_id)
      await this.requestDemoPdcAccess(payload)
    }
  }

  async requestDemoPdcAccess(payload) {
    const lambda = new LambdaClient({});
      return await lambda.send(new InvokeCommand({
        FunctionName: 'campaign-finance-demo-pdcAccess',
        Payload: JSON.stringify(payload),
        InvocationType: InvocationType.Event
      }))
  }

  getDemoPdcAccessPayload(action, committeeId, expirationDate = null) {
    return {
      pathParameters: {
        action: action
      },
      body: `{\"committee_id\": ${committeeId}, \"expiration_date\": \"${expirationDate}\"}`
    }
  }
}

export const pdcAccessProcessor = new PdcAccessProcessor();