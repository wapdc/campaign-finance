import pg from 'pg';
import {getSecrets} from "./SecretsManager.js";
pg.types.setTypeParser(pg.types.builtins.DATE, str => str);

class WapdcDb {
    constructor() {
       this.stage = null;
    }

    async beginTransaction() {
        await this.db.query("BEGIN")
    }

    async commit() {
        await this.db.query("COMMIT");
    }

    async rollback() {
        await this.db.query("ROLLBACK")
    }

    async connect(){
        try{
            if(process.env.STAGE !== 'dev'){

                //pass in credentials for pg pool
                const secretCredentials = await getSecrets();
                this.db = new pg.Client({
                    host: secretCredentials.host,
                    user: secretCredentials.username,
                    password: secretCredentials.password,
                    port: secretCredentials.port,
                    database: secretCredentials.dbname,
                    ssl: {rejectUnauthorized: false},
                    max: 2
                })

            }else{
                this.db =  new pg.Client({});
            }
            await this.db.connect();
            await this.getStage();
        }catch(err){
            console.error('An error occurred when opening the connection: ', err);
            throw Error(err);
        }
    }

    async close(){
        await this.db.end();
    }

    async getStage() {
        const result = await this.db.query("SELECT value from wapdc_settings where property='stage'");
        this.stage = result.rows[0].value;
    }

    async getWapdcSetting(property) {
        const result = await this.db.query("SELECT value from wapdc_settings where stage=$1 and property=$2", [this.stage, property]);
        let value = null;
        if (result.rows && result.rows.length) {
            value = result.rows[0].value;
        }
        return value;
    }

    async setWapdcSetting(property, value) {
        const records = await this.db.query("SELECT value from wapdc_settings where stage = $1 and property=$2", [this.stage, property ]);
        if (records && records.rows && records.rows.length) {
            await this.db.query("UPDATE wapdc_settings set value = $1::text WHERE stage=$2 and property = $3", [value, this.stage, property]);
        }
        else {
            await this.db.query("INSERT into wapdc_settings(property, stage, value) values ($1, $2, $3)", [property, this.stage, value]);
        }
        return {
            stage: this.stage,
            property: property,
            value: value
        }
    }

    /**
     * Register the API Gateway URL for campaign finance data.
     * @param url
     *   The URL of the deployed API gateway.
     * @returns {Promise<{stage: string, property: *, value: *}>}
     */
    async registerUrl(url) {
        return await this.setWapdcSetting('campaign_finance_aws_gateway', url);
    }

    /**
     * Restore a campaign from a JSON representation of the campaign data
     * @param fundId
     *   The fund ID for which to restore the campaign.
     * @param campaignJSON
     *   Json data containing the transactions
     * @returns {Promise<void>}
     */
    async restoreCampaign(fundId, campaignJSON) {
        await this.query("select * from private.cf_restore_campaign($1, $2)", [campaignJSON, fundId]);
    }

    /**
     * Generates a json object containing the contents of the JSON data in a backup.
     * @param fundId
     *   The fund to restore
     * @returns {Promise<{
     *   accounts: Array,
     *   attachedTextPages: Array,
     *   auctionItems: Array,
     *   campaignFund: Array,
     *   contacts: Array,
     *   coupleContacts: Array,
     *   depositItems: Array,
     *   depositEvents: Array,
     *   electionParticipation: Array,
     *   expenditureEvents: Array,
     *   individualContacts: Array,
     *   limits: Array,
     *   loans: Array,
     *   properties: Array,
     *   receipts: Array,
     *   registeredGroupContacts: Array,
     *   trankeygen: Array
     * }>}
     *   Returns an object containing the JSON representations of all records in a campaign.
     */
    async getCampaignArchive(fundId) {
        let result = await this.query("select * from private.cf_backup_campaign($1)", [fundId]);
        return result[0].cf_backup_campaign;
    }

    /**
     * Record the filename for a campaign archive
     * @param fundId
     *   The fund id of the file being backed up
     * @param fileName
     *   The name of the file within the bucket that needs to be retrieved.
     * @returns {Promise<*>}
     */
    async recordCampaignArchive(fundId, fileName) {
        return this.db.query("UPDATE private.campaign_fund set archive_file=$2, archive_expiration=now() + interval '14 days' where fund_id=$1", [fundId, fileName ]);
    }

    async getRestoreRequest(requestId) {
        const {rows} = await this.db.query("select r.*, c from private.restore_request r left join fund f on f.fund_id=r.fund_id left join committee c on c.committee_id=f.fund_id where restore_request_id=$1", [requestId]);
        if (rows && rows.length) {
            return rows[0];
        }
        else {
            return null
        }
    }

    async deleteRestoreRequest(requestId) {
        await this.db.query("delete from private.restore_request where restore_request_id=$1", [requestId]);
    }

    async query(query, parameters){
        let {rows} = await this.db.query(query, parameters);
        return rows;
    }

    async grantPdcAccess(committee_id, expiration_date){
        await this.db.query("INSERT INTO public.committee_authorization(committee_id, realm, role, uid, approved, granted_date, expiration_date) values ($1, $2, $3, $4, true, now(), $5) on conflict(committee_id, realm, uid) do update set expiration_date = excluded.expiration_date", [committee_id, 'PDC', 'owner', 'PDC', expiration_date])
    }

    async revokePdcAccess(committee_id){
        await this.db.query("UPDATE public.committee_authorization SET expiration_date = current_date - 1 WHERE committee_id = $1 and uid = 'PDC' and realm = 'PDC'", [committee_id])
    }
}
export const wapdcDb = new WapdcDb();
