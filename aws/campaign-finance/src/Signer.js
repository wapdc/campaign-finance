
import {getSecrets} from "./SecretsManager.js";
import * as jose from "jose";

/**
 * Method for reading in our 64 byte hex keys into a
 * secret expected by the jwt verifier..
 * @param hex
 * @returns {Uint8Array}
 */
function decodeKey(hex)
{
  var bytes = [];

  for(var i=0; i< hex.length-1; i+=2)
    bytes.push(parseInt(hex.substr(i, 2), 16));

  return new Uint8Array(bytes);
}

/**
 * Loads the secret from the config
 * @param keyName
 *   Represents which key to use (e.g. HMAC_KEY or CAMPAIGN_ARCHIVE_HMAC)
 * @returns {Promise<Uint8Array>}
 */
async function getSecret(keyName) {
  let secretString;
  if(process.env.STAGE !== 'dev') {
    const credentials = await getSecrets();
    secretString = credentials[keyName.toLowerCase()];
  }
  else {
    secretString = process.env[keyName.toUpperCase()];
  }
  return decodeKey(secretString);
}

/**
 * Sign Data payload using JWT.
 * @param payload
 * @param keyName
 * @param expiration int|string
 *   duration of expiration as defined by jose (e.g. '2h')
 * @constructor
 */
export const sign = async (payload, keyName, expiration = null) => {
  const secret = await getSecret(keyName);
  const token =  await new jose.SignJWT(payload)
    .setProtectedHeader({ alg: "HS256" })
    .setIssuedAt()
    .setIssuer('pdc.wa.gov')
  if (expiration) {
    token.setExpirationTime(expiration);
  }
  return token.sign(secret);
}

/**
 * Decode JWT token
 * @param token
 *   Token to decode
 * @constructor
 */
export const decode = async (token) =>  {
  return await jose.decodeJwt(token);
}

export const verify = async (token, keyName) => {
  let payload;
  const secret = await getSecret(keyName);
  try{
    ({payload} = await jose.jwtVerify(token, secret));
    return payload
  }
  catch(e) {
    console.log(e);
    return null
  }
}