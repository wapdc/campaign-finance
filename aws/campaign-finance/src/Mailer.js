import axios from 'axios';
import {getSecrets} from './SecretsManager.js'

/**
 * Unit testable class for sending email via sendgrid.
 */
class Mailer {
  constructor() {
    this.apiUrl = 'https://sendgrid.com/v3';
    this.apiToken = null;
    this.template_id = null;
    this.personalizations = [];
    this.sandbox_mode = false;
    this.from = {
      email: "pdc@pdc.wa.gov",
      name: "Washington Public Disclosure Commission"
    }
  }

  async configure() {
    if (process.env.STAGE !== 'dev') {
      const secrets = await getSecrets();
      this.apiToken = secrets.sendgrid_authorization_key;
    }
    else {
      this.apiToken = process.env.SENDGRID_AUTHORIZATION_KEY;
    }
  }

  /**
   * Helper function for quickly sending a single user a message to the template.
   * @param templateId
   * @param emailAddress
   * @param name
   * @param parameters
   */
   async sendTemplatedMessage(templateId, emailAddress, name, parameters){
    this.startMailMerge(templateId);
    this.addRecipient(emailAddress, name, parameters);
    return this.send();
  }

  /**
   * Start to generate a form mail using a particular template
   * @param templateId
   */
  startMailMerge(templateId) {
    this.template_id = templateId;
    this.personalizations.length=0;
  }

  /**
   * Add a recipient to a template that has begun with a setMessageTemplate
   * call.
   * @param emailAddress
   *   Email address of the receiving user
   * @param name
   *   Name of the person who is receiving the mail
   * @param parameters
   *   Parameters used in the template.
   */
  addRecipient(emailAddress, name, parameters = {}) {
    this.personalizations.push({
      to: [{
        name: name,
        email: emailAddress
      }],
      dynamic_template_data: parameters
    });
  }

  /**
   * Sends the form email started with a setMessageTemplate call.
   * Recipients
   */
  async send() {
    const payload = {
      subject: null,
      from: this.from,
      mail_settings: {
        sandbox_mode: {enable: this.sandbox_mode},
      },
      personalizations: this.personalizations,
      template_id: this.template_id
    }
    if (!this.apiToken) {
      await this.configure();
    }


    if (this.apiToken && this.personalizations.length > 0) {
      return await axios.post(this.apiUrl + '/mail/send', payload, {headers: {
        "Authorization": this.apiToken,
        "Content-Type":  "application/json"
        }});
    }
    else {
      console.info("Skipping mail send due to missing API token");
      return null
    }
  }

}
const mailer = new Mailer();
export default mailer;