import { DeleteObjectCommand, GetObjectCommand, PutObjectCommand, S3Client, } from "@aws-sdk/client-s3";
import { wapdcDb } from "./WapdcDb.js";
import mailer from "./Mailer.js";
import pako from "pako";
import * as signer from "./Signer.js";

class BackupProcessor {
  /**
   * Generates a backup of a campaign and uploads it to S3.
   * @param {Object} backupRequest - The backup request object.
   * @returns {Object} - The S3 upload result object.
   * @throws {Error} - If any errors occur during the backup process.
   */
  async generateBackup(backupRequest) {
    try {
      const token = await this.#generateToken(backupRequest);
      const zipSignedCampaignData = pako.gzip(token);
      const command = {
        Bucket: process.env.BACKUP_BUCKET,
        Key: 'backup_' + backupRequest.fund_id + '.gz',
        Body: zipSignedCampaignData,
        ContentType: 'application/gzip',
      };
      await this.#uploadToS3(backupRequest, token, command);
      await this.#recordCampaignArchive(backupRequest, command.Key);
      await this.#sendBackupNotification(backupRequest);
      return { Key: command.Key };
    } catch (error) {
      await this.#handleError(error, `Error generating backup`, backupRequest);
      throw error;
    }
  }

  /**
   * Restores a campaign from S3 data.
   * @param {Object} s3Data - The S3 data object.
   * @param {Object} s3Data.object - The S3 object metadata.
   * @param {string} s3Data.object.key - The S3 object key.
   * @throws {Error} - If any errors occur during the restore process.
   */
  async restoreCampaign(s3Data) {
    let request;
    const fileName = s3Data.object.key;
    const requestId = this.#extractRequestId(fileName);
    request = await this.#getRestoreRequest(requestId);
    try {
      const fileContents = await this.#getS3FileContents(request, fileName);
      const payload = await this.#decodePayload(request, fileContents);
      await wapdcDb.restoreCampaign(request.fund_id, JSON.stringify(payload));
      await wapdcDb.deleteRestoreRequest(requestId);
      await this.#sendRestoreNotification(request);
      await this.#deleteS3File(fileName);
    } catch (error) {
      await this.#handleError(error, 'Error restoring campaign', request)
    }
  }

  /**
   * Generates a token for a campaign backup.
   * @param {Object} backupRequest - The backup request object.
   * @param {string} backupRequest.fund_id - The ID of the fund to back up.
   * @returns {string} - The generated token.
   */
  async #generateToken(backupRequest) {
    const campaignData = await wapdcDb.getCampaignArchive(
      backupRequest.fund_id
    );
    return await signer.sign(
      campaignData,
      "CAMPAIGN_ARCHIVE_SIGNING_HMAC"
    );
  }

  /**
   * Uploads a campaign backup to S3.
   * @param {Object} backupRequest - The backup request object.
   * @param {string} token - The token to upload.
   * @param {Object} command - The S3 command object.
   * @returns {Object} - The S3 upload result object.
   */
  async #uploadToS3(backupRequest, token, command) {
      const s3 = new S3Client({});
      return await s3.send(new PutObjectCommand(command));
  }

  /**
   * Records a campaign archive in the database.
   * @param {Object} backupRequest - The backup request object.
   * @param {string} key - The S3 object key of the backup.
   */
  async #recordCampaignArchive(backupRequest, key) {
      await wapdcDb.recordCampaignArchive(backupRequest.fund_id, key);
  }

  /**
   * Extracts the request ID from a file name.
   * @param {string} fileName - The file name to extract the ID from.
   * @returns {number} - The request ID.
   */
  #extractRequestId(fileName) {
    const requestId = Number.parseInt(fileName.replace(/[^0-9]/g, ""));
    if (isNaN(requestId)) {
      throw new Error("Invalid file name: Must contain a request id");
    }
    return requestId;
  }

  /**
   * Retrieves a restore request from the database.
   * @param {number} requestId - The ID of the restore request to retrieve.
   * @returns {Object} - The restore request object.
   */
  async #getRestoreRequest(requestId) {
    const request = await wapdcDb.getRestoreRequest(requestId);
    if (request === null) {
      throw new Error("Restore request not found in database");
    }
    return request;
  }

  /**
   * Retrieves the contents of a file from S3.
   * @param {Object} request - The restore request object.
   * @param {string} fileName - The name of the file to retrieve.
   * @returns {string} - The contents of the file.
   */
  async #getS3FileContents(request, fileName) {
    const s3 = new S3Client({});
    const response = await s3.send(
      new GetObjectCommand({
        Bucket: process.env.RESTORE_BUCKET,
        Key: fileName,
      })
    );
    const zippedContents = await response.Body.transformToByteArray();
    //unzip and decompress content
    return await pako.ungzip(zippedContents, { to: "string" });
  }

  /**
   * Decodes the payload of a campaign backup.
   * @param {Object} request - The restore request object.
   * @param {string} fileContents - The contents of the backup file.
   * @returns {Object} - The decoded payload object.
   */
  async #decodePayload(request, fileContents) {
      let payload;
      if (request.validate) {
        payload = await signer.verify(
          fileContents,
          "CAMPAIGN_ARCHIVE_SIGNING_HMAC"
        );
      } else {
        payload = await signer.decode(fileContents);
      }
      return payload;
  }

  /**
   * Deletes a file from S3.
   * @param {string} fileName - The name of the file to delete.
   */
  async #deleteS3File(fileName) {
    const s3 = new S3Client({});
    await s3.send(
      new DeleteObjectCommand({
        Bucket: process.env.RESTORE_BUCKET,
        Key: fileName,
      })
    );
  }

  /**
   * Sends a notification with the provided template ID and data.
   * @param {Object} requestData - The request object.
   * @param {string} requestData.templateId - The ID of the email template.
   * @param {Object} requestData.data - The data to be sent with the template.
   * @param {string} requestData.email - The email address to send the notification to.
   */
  async #sendNotification(requestData) {
    if (requestData.email) {
      await mailer.sendTemplatedMessage(
        requestData.templateId,
        requestData.email,
        null,
        requestData.data
      );
    } else {
      console.info("Skipping notification because there is no email address.");
    }
  }

  /**
   * Sends a backup notification with the given backup request data.
   * @param {Object} backupRequest - The backup request object.
   * @param {string} backupRequest.committee_name - The name of the committee.
   * @param {string} backupRequest.fund_id - The ID of the fund.
   * @param {string} backupRequest.email - The email address to send the notification to.
   */
  async #sendBackupNotification(backupRequest) {
    if (backupRequest.email) {
      const baseUrl = await wapdcDb.getWapdcSetting("apollo_url");
      const data = {
        committee: backupRequest.committee_name,
        url: baseUrl + "/campaigns/ORCA/campaign/" + backupRequest.fund_id + "/campaign-info#archive",
        email: backupRequest.email,
      };
      await this.#sendNotification({
        templateId: "d-6e7c6a0fb56e4100a210c937d4cdaf3d",
        email: backupRequest.email,
        data: data,
      });
    } else {
      console.info("Skipping notification because there is no email address.");
    }
  }

  /**
   * Sends an error notification with the given error message and recipient email.
   * @param {string} errorMessage - The error message to be included in the email.
   * @param {Object} mailData - The email address to send the error notification to.
   */
  async #sendErrorNotification(errorMessage, mailData) {
    const data = {
      message: errorMessage,
      committee_name: mailData.committee_name
    };
    await this.#sendNotification({
      templateId: "d-742e882455494204b0673d17a0571f09",
      email: mailData.email,
      data: data,
    });
  }

  /**
   * Sends a notification after a restore request is completed.
   * @param {Object} request - The restore request object.
   */
  async #sendRestoreNotification(request) {
    if (request.email) {
      const baseUrl = await wapdcDb.getWapdcSetting("apollo_url");
      const data = {
        committee: request.committee_name,
        url: baseUrl + "/campaigns/ORCA/campaign/" + request.fund_id,
        email: request.email,
      };
      await this.#sendNotification({
        templateId: "d-e4d5998a5b2b4ea39b7dfa37bc893577",
        email: request.email,
        data,
      });
    } else {
      console.info("Skipping notification because there is no email address.");
    }
  }

  /**
   Handles an error by logging and sending an error notification.
   @param {Error} error - The error object.
   @param {string} message - The error message.
   @param {Object} mailData - Data to be passed to form error email.
   */
  async #handleError(error, message, mailData) {
    const errorMessage = `${ message }: ${ error.message }`;
    console.error(errorMessage);
    if (mailData.email) await this.#sendErrorNotification(errorMessage, mailData);
  }
}

export const backupProcessor = new BackupProcessor();
