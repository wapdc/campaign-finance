import {GetSecretValueCommand, SecretsManagerClient} from "@aws-sdk/client-secrets-manager";

let secrets = null;

/**
 * Retrieve the secrets from the secrets manager in order or from process.env
 *
 * @returns {Promise<Object>}
 *   Returns the secrets that were stored in secrets manager or process.env in the dev
 *   environment.
 */
export async function getSecrets() {
  return new Promise((resolve, reject) => {
    // retrieve module cached version of secrets so that we don't retrieve secrets more than once
    if(secrets !== null) {
      resolve(secrets)
    }
    else {
      const client = new SecretsManagerClient({region: process.env.CAMPAIGN_FINANCE_AWS_REGION})
      client.send(new GetSecretValueCommand({SecretId: process.env.RDS_CONNECTION}))
        .then(secret => {
          //pass in credentials for pg pool
          secrets = JSON.parse(secret.SecretString);
          resolve(secrets);
        })
        .catch((err) => {
          console.error("Error retrieving secret", err);
          reject();
        });
    }
  });
}
