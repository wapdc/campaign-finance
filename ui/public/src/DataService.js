import axios from 'axios';

export default class DataService {

  service_host = 'https://apollod8.lndo.site';

  /**
   * Constructor
   *
   * Sets an appropriate url handling our development environment.
   */
  constructor() {

    if (location.host.search('localhost') >= 0) {
      // Assume ports that start with 818 (e.g. 8180-8189 will connect to demo environment)
      // so that devs with no lando work.
      if (location.host.search('818') > 0) {
        this.service_host = 'https://demo-apollo-d8.pantheonsite.io/';
      } else {
        this.service_host = 'https://apollod8.lndo.site/';
      }
    } else {
      this.service_host = '/';
    }
  }

  /**
   * Call a web service that expects to return json.
   * @param path
   *   Indicates the path of the webservice to be called.
   * @param callback
   *   A callable that processes parsed json data from axios
   * @param parms
   *   An object containing URL parameters to send with request.
   * @param headers
   *   An object containing request headers.
   */
  get(path, callback, parms = {}, headers = {}) {
    let url = this.service_host + path;
    axios.get(url, {withCredentials: true, params: parms, headers: headers}).then(result => callback(result.data));
  }

  async getDraftReport(jwt) {
    return axios.get( this.service_host +'public/service/campaign-finance/draft-report/data', {
      withCredentials: true,
      params: {},
      headers: {'Authorization': `Bearer ${jwt}`}
    });
  }

  post(path, callback, post_data, get_data = {}) {
    let url = this.service_host + path;
    axios.post(url, JSON.stringify(post_data), {
      withCredentials: true,
      params: get_data
    }).then(result => callback(result.data));
  }

  getRegistration(registration_id, callback) {
    this.get('public/service/public-registration-process/get-registration',
      data => {
        if (data.success) {
          callback(data.data);
        }
      },
      {registration_id: registration_id})
  }

  async getReportMetadata(report_id) {
    const {data} = await axios({
      method: 'get',
      url: `${this.service_host}public/service/reports/${report_id}/userdata`
    })
    return data
  }

}