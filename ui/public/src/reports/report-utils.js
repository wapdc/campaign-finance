export default {
  getAggregateTotal(contacts, contactId, electionPeriod) {
    if (contacts) {
      let data = contacts.find(c => c.contact_key === contactId);
      if (data) {
        if (electionPeriod.toUpperCase() === 'P') {
          return data.aggregate_primary;
        } else {
          return data.aggregate_general;
        }
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  },
  arrayHasItems(item) {
    return !!item?.length;
  },
  sortByDate(items) {
    if (this.arrayHasItems(items)) {
      items.sort(function (a, b) {
        return new Date(a.date) - new Date(b.date);
      });
    }
  }
};