import Vue from 'vue'
import Router from 'vue-router'
import Registration from './views/Registration.vue'
import ReportOverview from '@/views/ReportOverview'
import Report from '@/views/Report'
import SurplusReport from "@/views/SurplusReport";
import DraftReport from "@/views/DraftReport";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/registration',
      name: 'registration',
      component: Registration
    },
    {
      path: '/finance-report/:report_number',
      name: 'finance-report',
      component: ReportOverview
    },
    {
      path: '/campaign-finance-report/:report_number',
      name: 'campaign-finance-report',
      component: Report
    },
    {
      path: '/campaign-finance-surplus-report/:report_number',
      name: 'campaign-finance-surplus-report',
      component: SurplusReport
    },
    {
      path: '/campaign-finance/draft-report/view',
      name: 'campaign-finance-draft-report',
      component: DraftReport
    }
  ]
})
