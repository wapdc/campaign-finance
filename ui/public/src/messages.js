// Ready translated locale messages
export const messages = {
  en: {
    message: {
      proposal: {
        add_proposal_title: "Add Ballot Proposition",
        add_proposal_content: "Is this an initiative or referendum, a levy, bond or advisory vote, or a recall campaign?",
        add_initiative_title: "Add Initiative or Referendum",
        add_initiative_content: "Provide initiative or referendum details. If no ballot number has been assigned, just enter the topic of the initiative or referendum. If the election date is not certain, choose the election date the committee expects or hopes for.",
        jurisdiction_search_title: "Jurisdiction Search",
        jurisdiction_search_content: "Please type a little bit of the jurisdiction name and hit search. Don't be too specific, and remember that often words are abbreviated, e.g. 'school district' becomes 'sd' and 'fire district' becomes 'fd'. You may also limit your search to a specific type of jurisdiction.",
        add_levy_title: "Add Levy, Bond or Advisory Vote",
        add_levy_content: "For a levy, bond or advisory vote campaign, please enter the year, ballot number if known, and whether the committee is supporting or opposing measure.",
        add_recall_title: "Add Recall Campaign",
        add_recall_content: "For a recall campaign, please list the name of the jurisdiction and the elected official.",
        statewide: "Statewide Proposition",
        local: "Local Proposition",
        levy: "Levy, Bond or Advisory Vote",
        initiative: "Initiative or Referendum",
        recall: "Recall Campaign",
        proposal_number: "Ballot number",
        proposal_issue: "Ballot issue or topic",
        election_year: "Election Year",
        for: "Supporting",
        against: "Opposing",
        for_recall: "Supporting the Recall",
        against_recall: "Opposing the Recall",
        for_recall_official: "Supporting the Recall of {official}",
        against_recall_official: "Opposing the Recall of {official}",
        levy_jurisdiction_search: "Levy Jurisdiction Search",
        no_jurisdictions: "No jurisdictions were found using the given search criteria.",
        confirm_remove: "Remove this from the list of ballot proposals?",
        intro: "If the committee supports or opposes an initiative, referendum, levy, bond, advisory vote, or recall campaign, add it here. Even if the measure isn't completely filed, add it here anyway.",
        no_election: "No election specified",
      },
      ticket: {
        for: "Supporting Party Ticket",
        against: "Opposing Party Ticket",
      }
    }
  },
};