/**
 * The ServiceContainer class allows injection of dependent services
 * to the class.
 */
import DataService from "./DataService";
import Vue from 'vue';

export default class ServiceContainer {
  dataService = new DataService();
  wire = new Vue();
}