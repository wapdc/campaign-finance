import axios from 'axios';
import {publicApp as app} from './main';

const datasets = {
  IMAGED_DOCUMENTS: {
    live: 'j78t-andi',
    demo: 'j78t-andi'
  },
  REPORTING_HISTORY: {
    live: '7qr9-q2c9',
    demo: '7qr9-q2c9'
  },
  CAMPAIGN_FINANCE_SUMMARY: {
    live: '3h9x-7bvm',
    demo: '3h9x-7bvm'
  },
  CANDIDATE_SURPLUS_REPORTS: {
    live: '9kcu-2bem',
    demo: '9kcu-2bem'
  },
  CANDIDATE_SURPLUS_FUNDS: {
    live: '3cbn-54c3',
    demo: '3cbn-54c3'
  }
}

class SocrataDataService {

  service_host = 'https://data.wa.gov';

  context = '';

  pendingRequests = 0;

  errorReason = {};

  /**
   * Constructor
   *
   * Sets an appropriate url handling our development environment.
   */
  constructor() {
    if ((location.host.search('demo') >= 0) ||
      (location.host.search('lndo') >= 0) ||
      (location.host.search('localhost') >= 0)) {
      this.context = 'demo';
    } else {
      this.context = 'live';
    }
  }

  /**
   * Handle errors in all axios calls.
   * @param response
   */
  handleError(response) {
    if (response.status === 400) {
      app.$container.wire.$emit('data.fetch_failed');
    }
    this.resolvePending();
  }

  setPending() {
    if (this.pendingRequests === 0) {
      app.$container.wire.$emit('SocrataService.pending', true);
    }
    this.pendingRequests++;
  }

  resolvePending() {
    if (this.pendingRequests > 0) {
      this.pendingRequests--;
    }
    if (this.pendingRequests === 0) {
      app.$container.wire.$emit('SocrataService.pending', false);
    }
  }

  /**
   *
   * @param dataset
   * @param filer_id
   * @param election_year
   * @param period_start
   * @param period_end
   * @returns {Promise<AxiosResponse<any>>}
   */
  async getRecords(dataset, {filer_id, election_year, origin, period_start = '', period_end = ''}) {
    const url = `https://data.wa.gov/resource/${datasets[dataset][this.context]}.json?`;
    const query = `select * where election_year='${election_year}' and filer_id='${filer_id}' and origin='${origin}' ${period_start ? `and report_from='${period_start}'` : ''} ${period_end ? `and report_to='${period_end}'` : ''}`;
    this.setPending();
    const res = await axios.get(url, {
      params: {
        $query: query
      }
    }).catch(e => {
      this.handleError(e.response)
    });
    this.resolvePending();
    return res;
  }

  /**
   *
   * @param dataset
   * @param report_number
   * @returns {Promise<*>}
   */
  async getReport(dataset, {report_number}) {
    const url = `https://data.wa.gov/resource/${datasets[dataset][this.context]}.json?`;
    const query = `SELECT * WHERE report_number = "${report_number}"`;
    this.setPending();
    const {data: [report]} = await axios.get(url, {
      params: {
        $query: query
      }
    }).catch(e => {
      this.handleError(e.response);
    });
    this.resolvePending();
    return report;
  }

  // election year and committee id
  async getSurplusCommitteeData(dataset, committee_id) {
    const url = `https://data.wa.gov/resource/${datasets[dataset][this.context]}.json?`;
    const query = `SELECT * WHERE id = "${committee_id}"`;
    this.setPending();
    const {data: [report]} = await axios.get(url, {
      params: {
        $query: query
      }
    }).catch(e => {
      this.handleError(e.response);
    });
    this.resolvePending();
    return report;
  }

  // election year and committee id
  async getCommitteeData(dataset, committee_id, election_year) {
    const url = `https://data.wa.gov/resource/${datasets[dataset][this.context]}.json?`;
    const query = `SELECT * WHERE committee_id = "${committee_id}" and election_year = "${election_year}"`;
    this.setPending();
    const {data: [report]} = await axios.get(url, {
      params: {
        $query: query
      }
    }).catch(e => {
      this.handleError(e.response);
    });
    this.resolvePending();
    return report;
  }


}

const service = new SocrataDataService();

export default service;
