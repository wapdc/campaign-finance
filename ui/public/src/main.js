import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueI18n from 'vue-i18n'
import ServiceContainer from './ServiceContainer';
import globalFilters from './filters';
import {messages} from './messages'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false
Vue.use(VueI18n);

// Global filters are defined in filters.js
globalFilters.defineGlobalFilters();

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages
});

export const publicApp = new Vue({
  i18n,
  router,
  store,
  vuetify,
  render: h => h(App)
});

publicApp.$container = new ServiceContainer();

publicApp.$mount('#app');
