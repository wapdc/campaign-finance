import axios from 'axios';
import Vue from "vue";

const ieService = new Vue({
    data() {
      return {
        isPending: false,
        message: "",
      }
    }
  }
);

export default class IExpendituresDataService {

  constructor() {
    this.service_host = window.localStorage.apollo_url ??  'https://apollod8.lndo.site/';
    this.errorReason = {};
    this.pendingCount = 0;
    this.headers = {'Content-Type': 'application/json;charset=UTF-8'};
    if (location.host.search('localhost') >= 0) {
      this.service_host = window.localStorage.apollo_url ?? 'https://apollod8.lndo.site/';
    } else {
      this.service_host = '/';
    }
    this.base_url = this.service_host + "service/iexpenditures/";
  }

  /**
   * Handle errors in all axios calls.
   * @param r
   */
  handleError(r) {
    this.errorReason = r;
    alert(r.message);
    this.markPending(false);
  }

  markPending(isPending, message) {
    if (!isPending) {
      this.pendingCount--;
      if (this.pendingCount <= 0) {
        ieService.isPending = false;
        ieService.message = message;
      }
    } else {
      this.pendingCount++;
      ieService.message = message;
      ieService.isPending = true;
    }
  }
  async loadUserInfo(){
    let url = this.service_host + 'public/service/iexpenditures/ie-user';
    let config = { withCredentials: true };
    let result = await axios.get(url, config).catch(e => this.handleError(e))
    if(result.data){
      return result.data;
    } else {
      return null;
    }
  }


  async getSponsorData(sponsor_id, query = '', parameters = null) {
    let url = this.base_url + 'sponsor/' + sponsor_id + '/' + query;
    this.markPending(true);
    let config = {withCredentials: true}
    if (parameters) {
      config.params = parameters;
    }
    let result = await axios.get(url, config).catch(e => this.handleError(e));
    this.markPending(false);
    if (result.data) {
      return result.data;
    } else {
      return null;
    }
  }

  async getFilerData(query='', parameters = null) {
    let url = this.base_url + 'filer/' + query;
    this.markPending(true);
    let config = {withCredentials: true}
    if (parameters) {
      config.params = parameters;
    }
    let result = await axios.get(url, config ).catch( e => this.handleError(e));
    this.markPending(false);
    if (result.data) {
      return result.data;
    }
    else {
      return null;
    }
  }

  async filerApiPost(call, post_data) {
    let url = this.base_url + 'filer/' + call;
    this.markPending(true);
    let result = await axios.post(url,  JSON.stringify(post_data),{headers: this.headers, withCredentials: true })
      .catch(() =>{
        alert('Could not post to ' + call);
      });
    this.markPending(false);
    return result?.data;
  }

  async entityApiPost(entity_id, post_data, action='')  {
    let url = this.base_url + 'entity/' + entity_id + '/' + action;
    this.markPending(true);
    let result = await axios.post(url,  JSON.stringify(post_data),{headers: this.headers, withCredentials: true })
      .catch(() =>{
        alert('Could not post to entity/' + action);
      });
    this.markPending(false);
    return result?.data;
  }

  async entityApiPut(entity_id, post_data, action='')  {
    let url = this.base_url + 'entity/' + entity_id + '/' + action;
    this.markPending(true);
    let result = await axios.put(url,  JSON.stringify(post_data),{headers: this.headers, withCredentials: true })
      .catch(() =>{
        alert('Could not update entity/' + action);
      });
    this.markPending(false);
    return result?.data;
  }

  async adminApiPost(call, post_data) {
    let url = this.base_url + 'admin/'  + call;
    this.markPending(true);
    let result = await axios.post(url,  JSON.stringify(post_data),{headers: this.headers, withCredentials: true })
      .catch(() =>{
        alert('Could not post to admin/' + call);
      });
    this.markPending(false);
    return result?.data;
  }

  async getAdminData(query='', parameters = null) {
    let url = this.base_url + 'admin/' + query;
    this.markPending(true);
    let config = {withCredentials: true}
    if (parameters) {
      config.params = parameters;
    }
    let result = await axios.get(url, config ).catch( e => this.handleError(e));
    this.markPending(false);
    if (result.data) {
      return result.data;
    }
    else {
      return null;
    }
  }
}

const ieDataService = new IExpendituresDataService();
export {ieDataService, ieService}