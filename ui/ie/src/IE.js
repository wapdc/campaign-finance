import {reactive, ref} from "vue";
import {ieDataService} from "@/IExpendituresDataService";
import moment from 'moment';


/*
 * These are the reactive properties for the fund that can be used in the campaign
 */
const defaultSponsor = {
  sponsor_id: 0
};
const sponsor = reactive(defaultSponsor);
const user = reactive({
  authenticated: true,
  email: "anonymous@noreply.com",
  permissions: {
    access_content: true
  },
});

class IE {

  /**
   * Helper file for returning the user object
   * @returns {Promise<void>}
   */
  async loadUserInfo() {
    let result = await ieDataService.loadUserInfo();
    Object.assign(user, result);
  }

  /**
   * Load sponsor profile information
   * @param sponsor_id
   * @returns {Promise<unknown>}
   */
  async loadSponsor(sponsor_id) {
    let data = await ieDataService.getSponsorData(sponsor_id, 'sponsors');
    if (data && data.sponsor) {
      Object.assign(sponsor, data.sponsor)
    } else {
      Object.assign(sponsor, defaultSponsor);
    }
    // load the sponsor ex. IE.$emit('sponsorLoaded', this.sponsor);
    return new Promise(resolve => {
      resolve(sponsor)
    });
  }
}

const sponsor_types = ref([
  {value: "A", text: "Individual using own funds"},
  {value: "B", text: "Individual receiving donations"},
  {value: "C", text: "Business, union, group or association using own funds"},
  {value: "D", text: "Business, union, group or association receiving donations"},
  {value: "E", text: "Political committee filing C3 and C4 reports"},
  {value: "F", text: "Political committee filing C-5 reports"},
  {value: "G", text: "Other"}
]);

const validations = {
  required: (v) => !!v || "Required."
}

function sponsor_types_filter(sponsor_code) {
  let text = null;

  if (sponsor_code) {
    text = sponsor_types.value.find(e => e.value === sponsor_code)?.text;
  }
  return text;
}

function formatDate(value) {
  if (value) {
    return moment(value.toString()).format('MM/DD/YYYY');
  }
  return '';
}

function formatDateTime(value) {
  if (value) {
    return moment(value.toString()).format('MM/DD/YYYY h:mm a');
  }
  return '';
}

const ieToolbox = new IE(sponsor, user);
const useIE = function () {
  return {sponsor, ieToolbox, user, sponsor_types, sponsor_types_filter, formatDate, formatDateTime, validations};
}
export {useIE}