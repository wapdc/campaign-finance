import Vue from "vue";
import VueRouter from "vue-router";
import IndependentExpenditures from "@/views/IndependentExpenditures";
import UnverifiedSponsors from "@/admin/UnverifiedSponsors";
import VerifySponsor from "@/admin/VerifySponsor";
import SponsorProfile from "@/sponsor/SponsorProfile";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: IndependentExpenditures,
    component: IndependentExpenditures,
  },
  {
    path: "/sponsor-profile/:sponsor_id",
    name: 'sponsor-profile',
    component: SponsorProfile,
  },
  {
    path: '/admin',
    name: 'unverified-sponsors',
    component: UnverifiedSponsors
  },
  {
    path: '/admin/verify-sponsor/:sponsor_request_id',
    name: 'verify-sponsor',
    component: VerifySponsor
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
