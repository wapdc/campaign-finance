import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import {app} from './main';

export default new Vuex.Store({
  state: {
    dataLoaded: false,
    elections: [],
    authorizedUsers: [],
    multiPosition: false,
    multiLocation: false,
    api_keys: [],
    parties: [],
    start_year: 0,
    end_year: 0,
    continuing_start: 0,
    searchString: "",
    wizardMessages: {
      error: ''
    },
    pac_types: [
      {pac_type: "bonafide", label: "Bonafide"},
      {pac_type: "candidate", label: "Candidate"},
      {pac_type: "caucus", label: "Caucus"},
      {pac_type: "pac", label: "PAC"},
      {pac_type: "surplus", label: "Surplus"}
    ],
    bonafide_types: [
      {bonafide_type: "", label: "None"},
      {bonafide_type: "county", label: "County"},
      {bonafide_type: "district", label: "District"},
      {bonafide_type: "state", label: "State"},
    ],
    registration: {
      draft: false,
      submitted_at: "",
      certification: "",
      certification_email: "",
      attachments: [{
        alias: "",
        file_service_url: "",
        registration_id: "",
        description: "",
        access_mode: "",
        document_type: ""
      }],
      committee: {
        name: "",
        reporting_type: "",
        has_sponsor: '',
        sponsor: '',
        base_name: '',
        books_contact: {
          email: ''
        },
        contact: {
          phone: "",
          email: "",
          address: "",
          state: "",
          postcard: ""
        },
        proposals: [{
          proposal_id: '',
          relationship: 'proposal',
          proposal_type: '',
          number: '',
          title: '',
          jurisdiction_name: '',
          jurisdiction_id: '',
          election_code: '',
          verified: '',
          stance: '',
          disable_fields: false
        }],
        officers: [{
          name: "",
          title: "",
          role: "",
          email: "",
          phone: "",
          address: "",
          city: "",
          state: "",
          postcode: "",
          ministerial: "",
          treasurer: ""
        }],
        bank: {
          name: "",
          address: "",
          city: "",
          state: "",
          postcode: ""
        },
        affiliations_declared: "",
        affiliations: [{
          affiliate_id: "",
          affiliate_name: ""
        }],
        candidacy: {
          person_id: '',
          person: '',
          ballot_name: '',
          email: '',
          offcode: '',
          office: '',
          location: '',
          jurisdiction_id: '',
          position: '',
          position_id: null,
          partisan: '',
          party: '',
          party_id: null,
        },
        purpose: '',
        candidates: [{
          candidate_id: null,
          name: '',
          office: '',
          party: '',
          party_id: null,
          stance: ''
        }],
        party: {
          party_id: null,
          name: '',
          stance: ''
        },
        ticket: {
          party_id: null,
          name: '',
          stance: ''
        },
      },
      local_filing: {
        seec: {
          required: null,
          books_available: ""
        }
      },
      draftErrors: [{}],
      meta_data: {
        candidacy: {
          primary_result: "",
          general_result: "",
          declared_date: "",
          exit_date: "",
          exit_reason: ""
        }
      }
    },
    logs: [
      {
        action: '',
        message: '',
        user: '',
        updated: '',
      }
    ],
    user: {
      email: null,
      rights: {
        administer_wapdc_campaign_finance: false
      }
    },
    committeeCategories: [],
  },
  mutations: {
    SET_REGISTRATION(state, registration) {
      app.$container.wire.$emit('breadcrumbs', [{'title': registration.committee.name,to: { name: 'committee', query: {'committee_id': registration?.committee?.committee_id}}}])
      if (registration.committee.committee_type !== 'CA' && !registration.committee.hasOwnProperty("has_sponsor")) {
        registration.committee.has_sponsor = "";
      }
      if (registration.committee.committee_type === 'CA' && !registration.committee.candidacy) {
        registration.committee.candidacy = {
          treasurer: false,
          office: '',
          location: '',
          position: '',
        };
      }
      if (!registration.committee.hasOwnProperty('purpose')) {
        registration.committee.purpose = "";
      }
      if (!registration.committee.hasOwnProperty('proposals')) {
        registration.committee.proposals = [];
      }
      if (!registration.hasOwnProperty('draftErrors')) {
        registration.draftErrors = [];
      }
      if (!registration.hasOwnProperty('local_filing')) {
        registration.local_filing = {
          seec: {
            required: null,
            books_available: ""
          }
        }
      }
      if (!registration.committee.hasOwnProperty('affiliations')) {
        registration.committee.affiliations = [];
      }
      if (registration.admin_data && !registration.admin_data.hasOwnProperty('filer_id')) {
        registration.admin_data.filer_id = null;
      }
      if (!registration.hasOwnProperty('meta_data')) {
        registration.meta_data = {}
      }
      if (!registration.draft) {
        registration.draft = false
      }
      state.registration = registration;
      state.dataLoaded = true;
    },
    SET_ELECTIONS(state, elections) {
      state.elections = elections;
    },
    SET_SEARCH_STRING(state, value) {
      state.searchString = value;
    },
    SET_LOGS(state, logs) {
      state.logs = logs;
    },
    SET_VALIDATION(state, draftErrors) {
      state.registration.draftErrors = draftErrors;
    },
    SET_PARTIES(state, parties) {
      state.parties = parties;
    },
    SET_AUTHORIZED_USERS(state, users) {
      state.authorizedUsers = users;
    },
    SET_API_KEYS(state, api_keys) {
      state.api_keys = api_keys;
    },
    SET_DATALOADED(state, bool) {
      state.dataLoaded = bool;
    },
    SET_PERMISSIONS(state, perms_obj) {
      state.user.rights = perms_obj;
    },
    SET_COMMITTEE_CATEGORIES(state, committeeCategories) {
      state.committeeCategories = committeeCategories
    }
  },
  actions: {
    getDraftRegistration(context) {
      let committee_id = app.$route.params.id;
      app.$container.dataService.committeeDraft(
        committee_id,
        data => {
          context.commit('SET_REGISTRATION', data);
        }
      );
    },
    validateDraft(context) {
      app.$container.dataService.validateCommitteeDraft(
        this.state.registration,
        data => {
          context.commit('SET_VALIDATION', data);
        }
      );
    },
    amendRegistration() {
      app.$container.dataService.amendRegistration(
        this.state.registration.registration_id,
        this.state.registration.committee.committee_id,
        () => {
          window.location.href = "/campaigns/committee/" + this.state.registration.committee.committee_id + "/draft";
        });
    },
    updateCandidacyStatus(context, candidacyMetadata) {
      app.$container.dataService.updateCandidacyStatus(
        data => {
          if (context.state.registration && context.state.registration.meta_data && context.state.registration.meta_data.candidacy) {
            context.state.registration.meta_data.candidacy.primary_result = data.primary_result;
            context.state.registration.meta_data.candidacy.general_result = data.general_result;
            context.state.registration.meta_data.candidacy.declared_date = data.declared_date;
            context.state.registration.meta_data.candidacy.exit_date = data.exit_date;
            context.state.registration.meta_data.candidacy.exit_reason = data.exit_reason;
          }
        },
        {
          committee_id: this.state.registration.committee.committee_id,
          primary_result: candidacyMetadata.primary_result,
          general_result: candidacyMetadata.general_result,
          declared_date: candidacyMetadata.declared_date,
          exit_date: candidacyMetadata.exit_date,
          exit_reason: candidacyMetadata.exit_reason
        });
    },
    updateAdminMemo(context, memo) {
      app.$container.dataService.updateAdminMemo(
        data => {
          if (context.state.registration && context.state.registration.committee) {
            context.state.registration.committee.memo = data;
          }
        },
        {
          committee_id: this.state.registration.committee.committee_id,
          memo: memo
        });
    },
    saveDraftRegistration(context) {
      app.$container.dataService.saveCommitteeDraft(
        this.state.registration,
        data => {
          context.commit('SET_VALIDATION', data);
          app.$container.wire.$emit('RegistrationWizard.validateWizard');
        }
      );
    },
    getCurrentRegistration(context) {
      let committee_id = app.$route.query.committee_id;
      app.$container.dataService.currentRegistration(committee_id, result => {
        context.commit("SET_REGISTRATION", result);
      });
    },
    getRegistration(context, registration_id) {
      app.$container.dataService.registration(registration_id, result => {
        if (result.redirection_url) {
          window.location.href = result.redirection_url;
        } else {
          context.commit("SET_REGISTRATION", result);
        }
      });
    },
    mergeCommittees(context, targetCid) {
      app.$container.dataService.mergeCommittees(context.state.registration.registration_id, context.state.registration.committee.committee_id, targetCid, data => {
        context.commit("SET_REGISTRATION", data);
        app.$container.wire.$emit('registration_list', context.state.registration.registration_id);
      })
    },
    getRegistrationLogs(context) {
      app.$container.dataService.getRegistrationLogs(
        this.state.registration.registration_id,
        data => {
          context.commit('SET_LOGS', data)
        }
      );
    },
    getAuthorizedUsers(context) {
      app.$container.dataService.adminQuery(
        'authorized_users',
        data => {
          context.commit('SET_AUTHORIZED_USERS', data)
        },
        {committee_id: context.state.registration.committee.committee_id}
      );
    },
    getApiKeys(context) {
      app.$container.dataService.query(
        'committee_api_keys',
        data => {
          context.commit('SET_API_KEYS', data)
        },
        {committee_id: context.state.registration.committee.committee_id}
      );
    },
    getParties(context) {
      app.$container.dataService.query('party',
        data => {
          context.commit('SET_PARTIES', data)
        }
      );
    },
    getElections(context) {
      app.$container.dataService.query(
        'elections',
        data => {
          context.commit('SET_ELECTIONS', data)
        }
      );
    },
    getCommitteeCategories(context) {
      app.$container.dataService.query('committee_category',
        data => {
          context.commit('SET_COMMITTEE_CATEGORIES', data)
        }
      );
    },
    unverifyRegistration(context, deleteFilerId) {
      app.$container.dataService.unverifyRegistration(context.state.registration, deleteFilerId, data => {
        context.commit("SET_REGISTRATION", data);
        app.$container.wire.$emit('committee_registration_list', data.committee.committee_id);
      })
    },
    verifyRegistration(context) {
      app.$container.dataService.verifyRegistration(context.state.registration, data => {
        context.commit("SET_REGISTRATION", data);
      })
    },
    setDataLoaded(context, bool) {
      context.commit('SET_DATALOADED', bool)
    },
    setSearchString(context, value) {
      context.commit('SET_SEARCH_STRING', value);
    },
    wizardPrevious(context, options = {}) {
      app.$container.wire.$emit('RegistrationWizard.previousStep', options);
    },
    wizardNext(context, options = {}) {
      app.$container.wire.$emit('RegistrationWizard.nextStep', options);
    },
    wizardActivate(context, parameters) {
      app.$container.dataService.saveCommitteeDraft(
        this.state.registration,
        data => {
          context.commit('SET_VALIDATION', data);
        }
      );
      if (parameters.hasOwnProperty('options')) {
        app.$container.wire.$emit('RegistrationWizard.activate', parameters.step, parameters.options);

      } else {
        app.$container.wire.$emit('RegistrationWizard.activate', parameters.step);
      }
    },
    activateStepper(context, stepper) {
      this.state.wizardMessages.error = '';
      app.$container.wire.$emit('CommitteeRegistration.activateStepper', stepper);
    }


  }
})