import Vue from 'vue'
import Router from 'vue-router'
import DraftReport from "./views/DraftReport";
import Tasks from './views/Tasks'
import Admin from "./views/Admin";
import About from "./views/About"
import UnverifiedCommittees from "./admin/UnverifiedCommittees";
import Committee from "./views/Committee";
import Committees from "./views/Committees";
import Registration from "./views/Registration";
import CommitteeSearch from "./views/CommitteeSearch";
import ContactSearch from "./views/ContactSearch";
import PaperRegistrations from "./admin/PaperRegistrations";
import C6Reports from "./admin/C6Reports";
import CandidatesMissingFilerIds from "./admin/CandidatesMissingFilerIds";
import C6Report from './admin/C6Report'
import ReportOverview from "./finance/ReportOverview";
import Report from "./views/Report";
import RegistrationDraft from "./registration/RegistrationDraft";
import EditVendorDebtPayment from "./orca/debt/EditVendorDebtPayment";
import CommitteeFinances from "./views/CommitteeFinances";
import OnlineCampaignSetup from "./orca/OnlineCampaignSetup";
import CampaignChange from "./views/CampaignChange";
import SelectFinanceYear from "./finance/SelectFinanceYear";


// ORCA imports are grouped in the file to make sure they get loaded from a dynamically loaded web chunk
const CampaignDashboard = () => import(/*webpackChunkName: "group-orca" */ '@/orca/CampaignDashboard');
const ContactsSearch = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contacts/ContactsSearch');
const Desktop = () => import(/*webpackChunkName: "group-orca" */ '@/orca/Desktop');
const ContributionSearch = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contributions/ContributionSearch');
const RecentExpenditures = () => import(/*webpackChunkName: "group-orca" */ '@/orca/expenditures/RecentExpenditures');
const ExpenditureSearch = () => import(/*webpackChunkName: "group-orca" */ '@/orca/expenditures/ExpenditureSearch');
const CampaignInfo = () => import(/*webpackChunkName: "group-orca" */ '@/orca/CampaignInfo');
const EditExpenditure = () => import(/*webpackChunkName: "group-orca" */ '@/orca/expenditures/EditExpenditure');
const AdvertisementSearch = () => import(/*webpackChunkName: "group-orca" */ '@/orca/advertisements/AdvertisementSearch');
const EditAdvertisementPage = () => import(/*webpackChunkName: "group-orca" */ '@/orca/advertisements/EditAdvertisementPage');
const DeletedTransactionLog = () => import(/*webpackChunkName: "group-orca" */ '@/orca/DeletedTransactionLog');
const C3View = () => import(/*webpackChunkName: "group-orca" */ '@/orca/reports/C3View');
const C4View = () => import(/*webpackChunkName: "group-orca" */ '@/orca/reports/C4View');
const C3ReportActivity = () => import(/*webpackChunkName: "group-orca" */ '@/orca/reports/C3ReportActivity');
const C4ReportActivity = () => import(/*webpackChunkName: "group-orca" */ '@/orca/reports/C4ReportActivity');
const C3Reports = () => import(/*webpackChunkName: "group-orca" */ '@/orca/reports/C3Reports');
const C4Reports = () => import(/*webpackChunkName: "group-orca" */ '@/orca/reports/C4Reports');
const ContactView = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contacts/ContactView');
const EditContactPage = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contacts/EditContactPage');
const DuplicateContactAnalysis = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contacts/DuplicateContactAnalysis');
const EditMonetaryContribution = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contributions/EditMonetaryContribution');
const EditMonetaryGroupContribution = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contributions/EditMonetaryGroupContribution');
const EditInKindContribution = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contributions/EditInKindContribution');
const EditAnonymousContribution = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contributions/EditAnonymousContribution');
const FileMenusMoved  = () => import(/*webpackChunkName: "group-orca" */ "@/orca/FileMenusMoved");
const EditRefundContribution = () => import(/*webpackChunkName: "group-orca" */ '@/orca/contributions/EditRefundContribution');
const FundraiserSearch = () => import(/*webpackChunkName: "group-orca" */ '@/orca/fundraisers/FundraiserSearch');
const EditLowCostFundraiser = () => import(/*webpackChunkName: "group-orca" */ '@/orca/fundraisers/LowCostFundraiser');
const EditCashFundraiser = () => import(/*webpackChunkName: "group-orca" */ '@/orca/fundraisers/CashFundraiser');
const EditMiscOther = () => import(/*webpackChunkName: "group-orca" */ '@/orca/misc/MiscOther');
const EditVendorRefund = () => import(/*webpackChunkName: "group-orca" */ '@/orca/misc/EditVendorRefund');
const EditVendorDebtAdjustment = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/EditVendorDebtAdjustment');
const EditMiscBankInterest = () => import(/*webpackChunkName: "group-orca" */ '@/orca/misc/EditBankInterest');
const MiscSearch = () => import(/*webpackChunkName: "group-orca" */ '@/orca/misc/SearchMisc');
const SearchAdjustments = () => import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/SearchAdjustments');
const EditAccountTransfer = () => import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/EditAccountTransfer');
const EditDeposit = () =>  import(/*webpackChunkName: "group-orca" */ '@/orca/deposits/EditDeposit');
const EditMathAdjustment = () =>  import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/EditMathAdjustment');
const EditTransactionAdjustment = () =>  import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/EditTransactionAdjustment');
const EditLoanForgiveness = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/EditLoanForgiveness');
const EditCashLoan = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/EditCashLoan');
const EditInKindLoan = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/EditInKindLoan');
const DebtSearch = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/DebtSearch');
const VendorDebtView = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/VendorDebtView')
const EditVendorDebtForgiveness = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/EditVendorDebtForgiveness')
const EditLoanPayment = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/EditLoanPayment');
const EditCreditCardPayment = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/EditCreditCardPayment');
const FundraiserView = () => import(/*webpackChunkName: "group-orca" */ '@/orca/fundraisers/FundraiserView');
const EditFundraiserContribution = () => import(/*webpackChunkName: "group-orca" */ '@/orca/fundraisers/EditFundraiserContribution');
const LoanView = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/LoanView');
const EditLoanEndorser = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/EditLoanEndorser');
const AuctionView = () => import(/*webpackChunkName: "group-orca" */ '@/orca/fundraisers/AuctionView');
const EditAuction = () => import(/*webpackChunkName: "group-orca" */ '@/orca/fundraisers/EditAuction');
const EditAuctionItem = () =>  import(/*webpackChunkName: "group-orca" */ '@/orca/fundraisers/EditAuctionItem');
const PledgeSearch = () => import(/*webpackChunkName: "group-orca" */ '@/orca/pledges/PledgeSearch');
const CreditCardView = () => import(/*webpackChunkName: "group-orca" */ '@/orca/debt/CreditCardView');
const BankAccounts = () => import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/BankAccounts');
const EditAccount = () => import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/EditAccount');
const EditPledge = () => import(/*webpackChunkName: "group-orca" */ '@/orca/pledges/EditPledge');
const PledgeCancel = () => import(/*webpackChunkName: "group-orca" */ '@/orca/pledges/PledgeCancel');
const AccountSummary = () => import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/AccountSummary');
const AccountsForGroup = () => import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/AccountsForGroup');
const AccountTransactions = () => import(/*webpackChunkName: "group-orca" */ '@/orca/accounts/AccountTransactions');

  Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/committee/:committee_id/draft-report/:draft_id',
        name: 'draft-report',
        component: DraftReport
    },
    {
      name: 'committee.finances',
      path: '/committee/:committee_id/finances',
      component: CommitteeFinances,
    },
    {
      name: 'committee.finances.year',
      path: '/committee/:committee_id/finances/:election_code',
      component: CommitteeFinances,
    },
    {
      name: 'committee.finance-years',
      path: '/committee/:committee_id/finance-year',
      component: SelectFinanceYear
    },
    {
      path: '/report-overview',
      name: 'report-overview',
      component: ReportOverview
    },
    {
      path: '/fund/report/:report_number',
      name: 'report',
      component: Report
    },
    {
      path: '/',
      name: 'tasks',
      component: Tasks
    },
    {
      path: '/committees',
      name: 'committees',
      component: Committees,
    },
    {
      name: 'MyOrcaFunds',
      path: '/my-orca-funds',
      component: CampaignChange,
    },
    {
      path: '/committee/:id/draft',
      name: 'registration-draft',
      component: RegistrationDraft
    },
    {
      path: '/committee/:committee_id/report/:report_number',
      name: 'committee.report',
      component: Report
    },
    {
      path: '/contact-search',
      name: 'contact-search',
      component: ContactSearch,
    },
    {
      path: '/committee-search',
      name: 'committee-search',
      component: CommitteeSearch,
    },
    {
      path: '/committee',
      name: 'committee',
      component: Committee
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin
    },
    {
      path: '/admin/unverified-committees',
      name: 'admin.unverified-committees',
      component: UnverifiedCommittees,
    },
    {
      path: '/admin/paper-registrations',
      name: 'admin.paper-registrations',
      component: PaperRegistrations,
    },
    {
      path: '/admin/c6-reports',
      name: 'admin.c6-reports',
      component: C6Reports,
    },
    {
      path: '/admin/c6-report',
      name: 'admin.c6-report',
      component: C6Report,
    },
    {
      path: '/admin/candidates-missing-filer-ids',
      name: 'admin.candidates-missing-filer_ids',
      component: CandidatesMissingFilerIds,
    },
    {
      path: '/help',
      name: 'help',
      component: About
    },
    {
      path: '/committee/:committee_id/campaign-setup',
      name: 'online-campaign-setup',
      component: OnlineCampaignSetup
    },
    {
      path: '/ORCA/campaign/:fund_id/c3/:deposit_id',
      name: 'ORCA.C3View',
      component: C3View
    },
    {
      path: '/ORCA/campaign/:fund_id/c4/:period_id',
      name: 'ORCA.C4View',
      component: C4View
    },
    {
      path: '/ORCA/campaign',
      name: 'ORCA.campaign',
      component: CampaignDashboard
    },
    {
      path: '/ORCA/campaign/:fund_id',
      name: "ORCA.campaign.fund",
      component: CampaignDashboard
    },
    {
      path: '/ORCA',
      name: 'ORCA',
      component: Desktop
    },
    {
      path: '/ORCA/campaign/:fund_id/contact-search',
      name: 'ORCA.fund.contact-search',
      component: ContactsSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/campaign-info',
      name: 'orca.fund.campaign-info',
      component: CampaignInfo
    },
    {
      path: '/ORCA/campaign/:fund_id/c3-reports',
      name: 'orca.fund.c3-reports',
      component: C3Reports,
    },
    {
      path: '/ORCA/campaign/:fund_id/c4-reports',
      name: 'orca.fund.c4-reports',
      component: C4Reports,
    },
    {
      path: '/ORCA/campaign/:fund_id/contact/:contact_id',
      name: 'orca.fund.contact',
      component: ContactView,
    },
    {
      path: '/ORCA/campaign/:fund_id/contribution-search',
      name: 'ORCA.fund.contribution-search',
      component: ContributionSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/contribution-search',
      name: 'ORCA.fund.import-contribution',
      component: ContributionSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/recent-expenditures',
      name: 'ORCA.fund.recent-expenditures',
      component: RecentExpenditures
    },
    {
      path: '/ORCA/campaign/:fund_id/add-contact',
      name: 'ORCA.fund.add-contact',
      component: EditContactPage
    },
    {
      path: '/ORCA/campaign/:fund_id/edit-contact/:id',
      name: 'ORCA.fund.edit-contact',
      component: EditContactPage
    },
    {
      path: '/ORCA/campaign/:fund_id/add-expenditure',
      name: 'ORCA.fund.add-expenditure',
      component: EditExpenditure
    },
    {
      path: '/ORCA/campaign/:fund_id/expenditure/:id',
      name: 'ORCA.fund.edit-expenditure',
      component: EditExpenditure
    },
    {
      path: '/ORCA/campaign/:fund_id/add-surplus-expenditure',
      name: 'ORCA.fund.add-surplus-expenditure',
      component: EditExpenditure
    },
    {
      path: '/ORCA/campaign/:fund_id/surplus-expenditure/:id',
      name: 'ORCA.fund.edit-surplus-expenditure',
      component: EditExpenditure
    },
    {
      path: '/ORCA/campaign/:fund_id/add-invoiced-expense',
      name: 'ORCA.fund.add-invoiced-expense',
      component: EditExpenditure
    },
    {
      path: '/ORCA/campaign/:fund_id/invoiced-expense/:id',
      name: 'ORCA.fund.edit-invoiced-expense',
      component: EditExpenditure
    },
    {
      path: '/ORCA/campaign/:fund_id/invoiced-expense/:expense_event_id/adjustment/',
      name: 'ORCA.fund.add-invoiced-expense-adjustment',
      component: EditVendorDebtAdjustment
    },
    {
      path: '/ORCA/campaign/:fund_id/invoiced-expense/:expense_event_id/adjustment/:adjustment_id',
      name: 'ORCA.fund.edit-invoiced-expense-adjustment',
      component: EditVendorDebtAdjustment
    },
    {
      path: '/ORCA/campaign/:fund_id/expenditure-search',
      name: 'ORCA.fund.expenditure-search',
      component: ExpenditureSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/add-advertisement',
      name: 'ORCA.fund.add-advertisement',
      component: EditAdvertisementPage
    },
    {
      path: '/ORCA/campaign/:fund_id/advertisement/:id',
      name: 'ORCA.fund.edit-advertisement',
      component: EditAdvertisementPage
    },
    {
      path: '/ORCA/campaign/:fund_id/advertisement-search',
      name: 'ORCA.fund.advertisement-search',
      component: AdvertisementSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/deleted-transactions',
      name: 'ORCA.fund.deleted-transactions',
      component: DeletedTransactionLog
    },
    {
      path: '/ORCA/campaign/:fund_id/c3-report-activity/:deposit_id',
      name: 'ORCA.fund.c3-report-activity',
      component: C3ReportActivity
    },
    {
      path: '/ORCA/campaign/:fund_id/c4-report-activity/:reporting_obligation_id',
      name: 'ORCA.fund.c4-report-activity',
      component: C4ReportActivity
    },
    {
      path: '/ORCA/campaign/:fund_id/report/:report_number',
      name: 'orca.fund.report',
      component: Report
    },
    {
      path: '/ORCA/campaign/:fund_id/contact-duplicates',
      name: 'ORCA.fund.contact-duplicates',
      component: DuplicateContactAnalysis
    },
    {
      path: '/ORCA/campaign/:fund_id/add-monetary-contribution',
      name: 'ORCA.fund.add-monetary-contribution',
      component: EditMonetaryContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/monetary-contribution/:id',
      name: 'ORCA.fund.edit-monetary-contribution',
      component: EditMonetaryContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/monetary-group-contribution',
      name: 'ORCA.fund.add-monetary-group-contribution',
      component: EditMonetaryGroupContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/monetary-group-contribution/:id',
      name: 'ORCA.fund.edit-monetary-group-contribution',
      component: EditMonetaryGroupContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/add-inkind-contribution',
      name: 'ORCA.fund.add-inkind-contribution',
      component: EditInKindContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/inkind-contribution/:id',
      name: 'ORCA.fund.edit-inkind-contribution',
      component: EditInKindContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/add-anonymous-contribution',
      name: 'ORCA.fund.add-anonymous-contribution',
      component: EditAnonymousContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/anonymous-contribution/:id',
      name: 'ORCA.fund.edit-anonymous-contribution',
      component: EditAnonymousContribution
    },
    {
      path: "/ORCA/campaign/:fund_id/file-menus-moved",
      name: 'ORCA.fund.file-menus-moved',
      component: FileMenusMoved
    },
    {
      path: "/ORCA/file-menus-moved",
      name: 'ORCA.file-menus-moved',
      component: FileMenusMoved
    },
    {
      path: '/ORCA/campaign/:fund_id/add-refund-contribution',
      name: 'ORCA.fund.add-refund-contribution',
      component: EditRefundContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/refund-contribution/:id',
      name: 'ORCA.fund.edit-refund-contribution',
      component: EditRefundContribution
    },
    {
      path: '/ORCA/campaign/:fund_id/fundraiser-search',
      name: 'ORCA.fund.fundraiser-search',
      component: FundraiserSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/low-cost-fundraiser',
      name: 'ORCA.fund.add-low-cost-fundraiser',
      component: EditLowCostFundraiser
    },
    {
      path: '/ORCA/campaign/:fund_id/low-cost-fundraiser/:id',
      name: 'ORCA.fund.edit-low-cost-fundraiser',
      component: EditLowCostFundraiser
    },
    {
      path: '/ORCA/campaign/:fund_id/add-cash-fundraiser',
      name: 'ORCA.fund.add-cash-fundraiser',
      component: EditCashFundraiser
    },
    {
      path: '/ORCA/campaign/:fund_id/cash-fundraiser/:id',
      name: 'ORCA.fund.edit-cash-fundraiser',
      component: EditCashFundraiser
    },
    {
      path: '/ORCA/campaign/:fund_id/add-fundraiser-contribution',
      name: 'ORCA.fund.add-fundraiser-contribution',
      component: EditFundraiserContribution,
    },
    {
      path: '/ORCA/campaign/:fund_id/fundraiser-contribution/:id',
      name: 'ORCA.fund.fundraiser-contribution',
      component: EditFundraiserContribution,
    },
    {
      path: '/ORCA/campaign/:fund_id/misc-other',
      name: 'ORCA.fund.add-misc-other',
      component: EditMiscOther
    },
    {
      path: '/ORCA/campaign/:fund_id/misc-other/:id',
      name: 'ORCA.fund.edit-misc-other',
      component: EditMiscOther
    },
    {
      path: '/ORCA/campaign/:fund_id/misc-bank-interest',
      name: 'ORCA.fund.add-misc-bank-interest',
      component: EditMiscBankInterest,
    },
    {
      path: '/ORCA/campaign/:fund_id/misc-bank-interest/:id',
      name: 'ORCA.fund.edit-misc-bank-interest',
      component: EditMiscBankInterest,
    },
    {
      path: '/ORCA/campaign/:fund_id/vendor-refund',
      name: 'ORCA.fund.add-vendor-refund',
      component: EditVendorRefund
    },
    {
      path: '/ORCA/campaign/:fund_id/vendor-refund/:id',
      name: 'ORCA.fund.edit-vendor-refund',
      component: EditVendorRefund
    },
    {
      path: '/ORCA/campaign/:fund_id/search-misc-receipts',
      name: 'ORCA.fund.search-misc-receipts',
      component: MiscSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/search-adjustments',
      name: 'ORCA.fund.search-adjustments',
      component: SearchAdjustments
    },
    {
      path: '/ORCA/campaign/:fund_id/account-transfer',
      name: 'ORCA.fund.add-account-transfer',
      component: EditAccountTransfer,
    },
    {
      path: '/ORCA/campaign/:fund_id/account-transfer/:id',
      name: 'ORCA.fund.edit-account-transfer',
      component: EditAccountTransfer,
    },
    {
      path: '/ORCA/campaign/:fund_id/add-deposit',
      name: 'orca.fund.add-deposit',
      component: EditDeposit
    },
    {
      path: '/ORCA/campaign/:fund_id/edit-deposit/:id',
      name: 'orca.fund.edit-deposit',
      component: EditDeposit
    },
    {
      path: '/ORCA/campaign/:fund_id/math-error-adjustment',
      name: 'ORCA.fund.add-math-error-adjustment',
      component: EditMathAdjustment
    },
    {
      path: '/ORCA/campaign/:fund_id/math-error-adjustment/:id',
      name: 'ORCA.fund.math-error-adjustment',
      component: EditMathAdjustment
    },
    {
      path: '/ORCA/campaign/:fund_id/transaction-adjustment',
      name: 'ORCA.fund.add-transaction-adjustment',
      component: EditTransactionAdjustment
    },
    {
      path: '/ORCA/campaign/:fund_id/transaction-adjustment/:id',
      name: 'ORCA.fund.transaction-adjustment',
      component: EditTransactionAdjustment
    },
    {
      path: '/ORCA/campaign/:fund_id/add-cash-loan',
      name: 'ORCA.fund.add-cash-loan',
      component: EditCashLoan
    },
    {
      path: '/ORCA/campaign/:fund_id/cash-loan/:id',
      name: 'ORCA.fund.cash-loan',
      component: EditCashLoan
    },
    {
      path: '/ORCA/campaign/:fund_id/add-in-kind-loan',
      name: 'ORCA.fund.add-in-kind-loan',
      component: EditInKindLoan
    },
    {
      path: '/ORCA/campaign/:fund_id/in-kind-loan/:id',
      name: 'ORCA.fund.in-kind-loan',
      component: EditInKindLoan
    },
    {
      path: '/ORCA/campaign/:fund_id/search-debt',
      name: 'ORCA.fund.search-debt',
      component: DebtSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/vendor-debt-view/:id',
      name: 'ORCA.fund.vendor-debt-view',
      component: VendorDebtView
    },
    {
      path: '/ORCA/campaign/:fund_id/vendor-debt-payment/:id',
      name: 'ORCA.fund.vendor-debt-payment',
      component: EditVendorDebtPayment
    },
    {
      path: '/ORCA/campaign/:fund_id/vendor-debt-payment/',
      name: 'ORCA.fund.add-vendor-debt-payment',
      component: EditVendorDebtPayment
    },
    {
      path: '/ORCA/campaign/:fund_id/debt/:expenditure_event_id/vendor-debt-forgiveness',
      name: 'ORCA.fund.add-vendor-debt-forgiveness',
      component: EditVendorDebtForgiveness
    },
    {
      path: '/ORCA/campaign/:fund_id/debt/:expenditure_event_id/vendor-debt-forgiveness/:id',
      name: 'ORCA.fund.edit-vendor-debt-forgiveness',
      component: EditVendorDebtForgiveness
    },
    {
      path: '/ORCA/campaign/:fund_id/loan/:id',
      name: 'ORCA.fund.loan',
      component: LoanView
    },
    {
      path: '/ORCA/campaign/:fund_id/fundraiser/:id',
      name: 'ORCA.fund.fundraiser',
      component: FundraiserView
    },
    {
      path: '/ORCA/campaign/:fund_id/auction/:id',
      name: 'ORCA.fund.auction',
      component: AuctionView,
    },
    {
      path: '/ORCA/campaign/:fund_id/loan/:loan_id/loan-forgiveness',
      name:'ORCA.fund.add-loan-forgiveness',
      component: EditLoanForgiveness
    },
    {
      path: '/ORCA/campaign/:fund_id/loan/:loan_id/loan-forgiveness/:id',
      name:'ORCA.fund.edit-loan-forgiveness',
      component: EditLoanForgiveness
    },
    {
      path: '/ORCA/campaign/:fund_id/loan/:loan_id/loan-endorser',
      name:'ORCA.fund.add-loan-endorser',
      component: EditLoanEndorser
    },
    {
      path: '/ORCA/campaign/:fund_id/loan/:loan_id/loan-endorser/:id',
      name:'ORCA.fund.edit-loan-endorser',
      component: EditLoanEndorser
    },
    {
      path: '/ORCA/campaign/:fund_id/add-loan-payment',
      name: 'ORCA.fund.add-loan-payment',
      component: EditLoanPayment
    },
    {
      path: '/ORCA/campaign/:fund_id/loan-payment/:id',
      name: 'ORCA.fund.loan-payment',
      component: EditLoanPayment
    },
    {
      path: '/ORCA/campaign/:fund_id/add-auction',
      name: 'ORCA.fund.add-auction',
      component: EditAuction
    },
    {
      path: '/ORCA/campaign/:fund_id/edit-auction/:id',
      name: 'ORCA.fund.edit-auction',
      component: EditAuction
    },
    {
      path: '/ORCA/campaign/:fund_id/auction/:auction_id/item/:id',
      name: 'ORCA.fund.auction.edit-item',
      component: EditAuctionItem
    },
    {
      path: '/ORCA/campaign/:fund_id/auction/:auction_id',
      name: 'ORCA.fund.auction.add-item',
      component: EditAuctionItem
    },
    {
      path: '/ORCA/campaign/:fund_id/pledge-search',
      name: 'ORCA.fund.pledge-search',
      component: PledgeSearch
    },
    {
      path: '/ORCA/campaign/:fund_id/credit-card/:id',
      name: 'ORCA.fund.credit-card',
      component: CreditCardView
    },
    {
      path: '/ORCA/campaign/:fund_id/add-credit-card',
      name: 'ORCA.fund.add-credit-card',
      component: EditAccount
    },
    {
      path: '/ORCA/campaign/:fund_id/add-credit-card',
      name: 'ORCA.fund.add-bank-account',
      component: EditAccount
    },
    {
      path: '/ORCA/campaign/:fund_id/edit-account/:id',
      name: 'ORCA.fund.edit-account',
      component: EditAccount
    },
    {
      path: '/ORCA/campaign/:fund_id/add-credit-card-payment',
      name: 'ORCA.fund.add-credit-card-payment',
      component: EditCreditCardPayment
    },
    {
      path: '/ORCA/campaign/:fund_id/credit-card-payment/:id',
      name: 'ORCA.fund.credit-card-payment',
      component: EditCreditCardPayment
    },
    {
      path: '/ORCA/campaign/:fund_id/bank-accounts',
      name: 'ORCA.fund.bank-accounts',
      component: BankAccounts
    },
    {
      path: '/ORCA/campaign/:fund_id/pledge/add-pledge',
      name: 'ORCA.fund.add-pledge',
      component: EditPledge
    },
    {
      path: '/ORCA/campaign/:fund_id/pledge/:id',
      name: 'ORCA.fund.edit-pledge',
      component: EditPledge
    },
    {
      path: '/ORCA/campaign/:fund_id/pledge/:pledge_id/pledge-cancel/',
      name: 'ORCA.fund.pledge-cancel',
      component: PledgeCancel
    },
    {
      path: '/ORCA/campaign/:fund_id/pledge/:pledge_id/pledge-cancel/:id',
      name: 'ORCA.fund.edit-pledge-cancel',
      component: PledgeCancel
    },
    {
      path: '/ORCA/campaign/:fund_id/account-summary',
      name: 'ORCA.fund.account-summary',
      component: AccountSummary,
    },
    {
      path: '/ORCA/campaign/:fund_id/account-summary/:acctnum',
      name: 'ORCA.fund.accounts-for-group',
      component: AccountsForGroup,
    },
    {
      path: '/ORCA/campaign/:fund_id/account-summary/:acctnum/:account_id',
      name: 'ORCA.fund.account-transactions',
      component: AccountTransactions,
    },
    {
      path: '/ORCA/campaign/fund/:fund_id/draft/:draft_id',
      name: 'ORCA.fund.draft-preview',
      component: C3View,
    },
  ]
})
