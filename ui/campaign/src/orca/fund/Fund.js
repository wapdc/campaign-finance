import {reactive, ref} from 'vue';
import {orcaDataService} from "@/OrcaDataService";
import ORCA from "@/ORCABridge";
import ORCABridge from "@/ORCABridge";
import Papa from 'papaparse';


/*
 * These are the reactive properties for the fund that can be used in the campaign
 */
const defaultFund = {
  committee_id: 0,
  name: "",
  fund_id: 0,
  pac_type: "",
  start_year: "",
  election_code: "",
  properties: [],
  accounts: [],
  committee_treasurers: [],
  committee_contact_info: null,
  ads: [],
  continuing: false,
  isCandidate: false,
  isPac: false,
  in_primary: null,
  in_general: null,
  election_codes: []
};
const fund = reactive(defaultFund);
const loaded_fund_id = ref(0);

/*
 * This class provides business logic that can be used to evaluate
 * rules and conditions related to the fund.
 */
class Fund {

  constructor(fundData) {
    this.fund = fundData;
  }

  async reloadFund() {
    if (this.fund.fund_id) {
      this.loadFund(this.fund.fund_id);
    }
  }

  async onAccountSync() {
    if (this.fund.fund_id) {
      this.loadFund(this.fund.fund_id);
    }
  }

  async loadFund(fund_id) {
    let data = await orcaDataService.getFundData(fund_id, 'fund');
    if (data && data.fund) {
      Object.assign(this.fund, data.fund);
      this.fund.isCandidate = data.fund.pac_type === 'candidate';
      this.fund.isPac = data.fund.pac_type !== 'candidate' && data.fund.pac_type !== 'surplus';
      this.fund.updates = data.fund.updates;
      if (!this.fund.properties) this.fund.properties = [];
      loaded_fund_id.value = this.fund.fund_id;
      ORCA.$emit("fundLoaded", this.fund);
      if(data.fund.version >= 1.500){
        ORCA.setOrcaVersion(data.fund.version)
      } else {
        ORCA.setOrcaVersion(0);
      }
    } else {
      Object.assign(this.fund, defaultFund);
    }
    return new Promise(resolve => {
      resolve(this.fund)
    });
  }

  setProperties(properties) {
    properties.forEach((p) => {
      let f = fund.properties.find(ep => ep.name === p.name);
      if (f) {
        f.value = p.value;
      } else {
        if (p.value !== null && p.value !== undefined) {
          fund.properties.push(p);
        }
      }
    });
    ORCABridge.setDerbyProperties(properties);
  }

  async setValidations(fund_id, validations) {
    if (fund_id !== this.fund.fund_id) {
      await this.loadFund(fund_id);
    }

    if (validations.properties) {
      fund.properties = validations.properties;
    }
    if (validations.reportingPeriods) {
      fund.reportingPeriods = validations.reportingPeriods;
    }
    ORCA.$emit("propertiesUpdated", fund.properties);
    return new Promise(resolve => {
      resolve(true)
    });
  }

  accountName(account_id) {
    const acct = this.fund.accounts?.find(e => e.trankeygen_id === account_id);
    return acct ? acct.name : "";
  }
}

function generateCSV(fileName, rows, columns, ignore = [] ) {

  const data = rows.map(row => {
    let r = {};
    console.log(columns)
    columns.forEach(c => {
      if(c.text && !ignore.includes(c.value)) {
        switch (c.value) {
          case 'elekey':
            if (this.fund.election_code.length === 4) {
              if (row.elekey === 0) {
                r[c.text] =  "Primary"
              }
              else if (row.elekey === 1) {
                r[c.text] =  "General"
              }
            }
            else {
              r[c.text] = null
            }
            break;
          default:
            r[c.text] = row[c.value];

        }
      }
    });
    return r;
  })
  let csv = Papa.unparse(data);

  let blob = new Blob([csv], {type: 'text/csv;charset=utf-8;'});
  if (navigator.msSaveBlob) {
    navigator.msSaveBlob(blob, fileName);
  }
  else {
    let link = document.createElement("a");
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      let url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}

const orcaFund = new Fund(fund);
let registered = false;

const useOrcaFund = function () {
  if (!registered) {
    if (ORCA) {
      registered = true;
      ORCA.$on('accounts.change', () => {
        orcaFund.onAccountSync()
      });
    }
  }
  return {fund, orcaFund, fund_id: loaded_fund_id, generateCSV}
};

export {useOrcaFund, Fund} ;
