import {ref} from 'vue';
import ORCA from "@/ORCABridge";
import {orcaDataService} from "@/OrcaDataService";
import Papa from "papaparse";
import {useOrcaFund} from "../fund/Fund";
import moment from "moment/moment";

/**
 * Provide the basic actions for lists of contacts
 */
class ContactListActions {
    constructor(contacts) {
        this.fund_id = 0;
        this.contacts = contacts;
    }

    setFund(fundId) {
        if (fundId && fundId !== this.fund_id) {
            this.fund_id = fundId;
            this.loadContacts();
        } else {
            if (!this.contacts && this.fund_id) {
                this.loadContacts();
            }
        }
    }

    onFundLoaded(fund) {
        if (fund.fund_id) {
            this.setFund(fund.fund_id);
        }
    }

    loadContacts(updatedContact) {
        if (this.fund_id) {
            orcaDataService.getFundData(this.fund_id, "get-contacts-by-fund-id").then(data => {

                this.contacts.value = data['get-contacts-by-fund-id'].sort((x, y) => {
                return x.type === 'CAN' ? -1 : y.type === 'CAN' ? 1:0
              })
                if (updatedContact) {
                    ORCA.$emit('contacts.add', updatedContact);
                }
            });
        }
    }
  async saveContacts( contacts) {
    if (this.fund_id) {
      orcaDataService.markPending(true, "Importing contacts...");
      let savedContacts = await orcaDataService.postFundObject(this.fund_id, 'contacts', {contacts}, true);
      if (savedContacts && savedContacts.contacts && savedContacts.contacts.length) {
        let orcaContacts = JSON.parse(JSON.stringify(savedContacts.contacts));
        orcaContacts.map(c => {
          if (!c.orca_id) {
            c.orca_id = 0;
            c.rds_id = c.trankeygen_id;
          }
          c.contact1_id = c.contact1_orca_id ?? 0;
          c.contact2_id = c.contact2_orca_id ?? 0;
        });
        ORCA.bulkSave({contacts: orcaContacts});
        orcaDataService.markPending(false);
      }
      return savedContacts;
    }
  }

  updateContactEntry(contact) {
      if (contact && contact.trankeygen_id) {

        let foundContact = this.contacts.value.find(c => c.trankeygen_id === contact.trankeygen_id);
        if (foundContact) {
          Object.assign(foundContact, contact);
        }
        else {
          this.contacts.value.push(contact);
        }
      }

  }

  exportContacts(contacts) {
    let list = contacts.filter( c => c.type !== 'CPL' && c.type !== 'GRP');
    let data = list.map(c => (
      {
        type: c.type,
        name: c.name,
        prefix: c.prefix,
        firstname: c.firstname,
        middleinitial: c.middleinitial,
        lastname: c.lastname,
        suffix: c.suffix,
        street: c.street,
        city: c.city,
        state: c.state,
        zip: c.zip,
        phone: c.phone,
        email: c.email,
        occupation: c.occupation,
        employername: c.employername,
        employerstreet: c.employerstreet,
        employercity: c.employercity,
        employerstate: c.employerstate,
        employerzip: c.employerzip,
        memo: c.memo
      }
    ))
    let csv = Papa.unparse(data);
    let filename = 'Contacts';
    let blob = new Blob([csv], {type: 'text/csv;charset=utf-8;'});
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, filename);
    } else {
      let link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        let url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

  /**
   * Import contacts from a csv
   * @param file
   * @param results
   *   where to place results of parse.
   * @param allowElekey
   *   true implies that we are allowing the import of an elekey from the file,
   * @returns Promise<{{data, length}}>
   */
  async importContacts(file, results, allowElekey= false) {
    return new Promise(resolve => {

      Papa.parse(file, {
        header: true,
        skipEmptyLines: true,
        transformHeader: (header) => {
          if (header.toLowerCase() === 'date') {
            return 'date_';
          }
          if (header.toLowerCase() === 'election') {
            return 'elekey'
          }
          return header.toLowerCase().replace(' ', '');
        },
        complete: rows => {
          rows.data.map( c => {
              if (!c?.type && c.firstname || c.type?.toUpperCase() === 'IND') {
                c.type = 'IND'
                c.name = c.firstname?.trim() ?? "";
                if (c.middleinitial && c.middleinitial?.trim()) {
                  c.name += ' ' + c.middleinitial;
                }
                if (c.lastname?.trim()) {
                  c.name += ' ' + c.lastname;
                }
              }
              else {
                if (c.type) {
                  c.type = c.type.toUpperCase();
                }
              }
              if(c.state) {
                  c.state = stateHandler(c.state);
              }
              if(c.employerstate) {
                  c.employerstate = stateHandler(c.employerstate);
              }
              // Reformat date fields
              if (c?.date_ && c.date_.includes('/')) {
                c.date_ = moment(c.date_).format("YYYY-MM-DD");
              }
              // truncate occupation length
              if(c.occupation && c.occupation.length > 50){
                c.occupation = c.occupation.substring(0, 50);
              }
              if (c.hasOwnProperty('elekey')) {
                if (allowElekey) {
                  switch (c.elekey.toLowerCase()) {
                    case 'primary':
                      c.elekey = 0;
                      break;
                    case 'general':
                      c.elekey = 1;
                      break;
                    default:
                      c.elekey = null;
                  }
                }
                else c.elekey = null;
              }
            }
          );
          Object.assign(results, rows);

          resolve(results);
        }
      });
    });
  }
}


const contacts = ref([]);
const contactListActions = new ContactListActions(contacts);
ORCA.$on('contacts.change', (contact) => {contactListActions.loadContacts(contact)});
ORCA.$on('fundLoaded', (fund) => {contactListActions.onFundLoaded(fund) });

function useContactList()  {
  const {fund} = useOrcaFund();
  if (fund.fund_id) contactListActions.setFund(fund.fund_id);
  return {contacts: contacts, contactListActions};
}
const stateHandler = (stateString) => {
    let switchState = stateString.toUpperCase().trim();
    if(switchState.length > 2) {
        switch(switchState) {
            case 'ALASKA':
                return stateString = 'AK';
            case 'ALABAMA':
                return stateString = 'AL';
            case 'ARKANSAS':
                return stateString = 'AR';
            case 'ARIZONA':
                return stateString = 'AZ';
            case 'CALIFORNIA':
                return stateString = 'CA';
            case 'COLORADO':
                return stateString = 'CO';
            case 'CONNECTICUT':
                return stateString = 'CT';
            case 'DELAWARE':
                return stateString = 'DE';
            case 'FLORIDA':
                return stateString = 'FL';
            case 'GEORGIA':
                return stateString = 'GA';
            case 'HAWAII':
                return stateString = 'HI';
            case 'IOWA':
                return stateString = 'IA';
            case 'IDAHO':
                return stateString = 'ID';
            case 'ILLINOIS':
                return stateString = 'IL';
            case 'INDIANA':
                return stateString = 'IN';
            case 'KANSAS':
                return stateString = 'KS';
            case 'KENTUCKY':
                return stateString = 'KY';
            case 'LOUISIANA':
                return stateString = 'LA';
            case 'MASSACHUSETTS':
                return stateString = 'MA';
            case 'MARYLAND':
                return stateString = 'MD';
            case 'MAINE':
                return stateString = 'ME';
            case 'MICHIGAN':
                return stateString = 'MI';
            case 'MINNESOTA':
                return stateString = 'MN';
            case 'MISSOURI':
                return stateString = 'MO';
            case 'MISSISSIPPI':
                return stateString = 'MS';
            case 'MONTANA':
                return stateString = 'MT';
            case 'NORTH CAROLINA':
                return stateString = 'NC';
            case 'NORTH DAKOTA':
                return stateString = 'ND';
            case 'NEBRASKA':
                return stateString = 'NE';
            case 'NEW HAMPSHIRE':
                return stateString = 'NH';
            case 'NEW JERSEY':
                return stateString = 'NJ';
            case 'NEW MEXICO':
                return stateString = 'NM';
            case 'NEVADA':
                return stateString = 'NV';
            case 'NEW YORK':
                return stateString = 'NY';
            case 'OHIO':
                return stateString = 'OH';
            case 'OKLAHOMA':
                return stateString = 'OK';
            case 'OREGON':
                return stateString = 'OR';
            case 'PENNSYLVANIA':
                return stateString = 'PA';
            case 'RHODE ISLAND':
                return stateString = 'RI';
            case 'SOUTH CAROLINA':
                return stateString = 'SC';
            case 'SOUTH DAKOTA':
                return stateString = 'SD';
            case 'TENNESSEE':
                return stateString = 'TN';
            case 'TEXAS':
                return stateString = 'TX';
            case 'UTAH':
                return stateString = 'UT';
            case 'VIRGINIA':
                return stateString = 'VA';
            case 'VERMONT':
                return stateString = 'VT';
            case 'WASHINGTON':
                return stateString = 'WA';
            case 'WASHINGTON DC:':
            case 'WASHINGTON, D.C.':
            case 'D.C.':
            case 'D.C':
                return stateString = 'DC';
            case 'WISCONSIN':
                return stateString = 'WI';
            case 'WEST VIRGINIA':
                return stateString = 'WV';
            case 'WYOMING':
                return stateString = 'WY';
            default:
                return stateString;
        }
    }
    return stateString;
}

export {useContactList, ContactListActions};