import {useOrcaFund} from "@/orca/fund/Fund";
import {reactive, ref} from 'vue';
import {orcaDataService} from "@/OrcaDataService";
import ORCA from "@/ORCABridge";
import {useContactList} from "./ContactList";

const defaultContact = {
    orca_id: 0,
    pid: 0,
    type: null,
    name: "",
    prefix: "",
    firstname: "",
    middleinitial: "",
    lastname: "",
    suffix: "",
    street: "",
    city: "",
    state: "",
    zip: "",
    phone: "",
    email: "",
    memo: "",
}

const {contactListActions} = useContactList();

/**
 * Provide the basic actions for saving and accessing contact information
 */
class ContactActions {
    constructor(contact, fund) {
        this.contact = contact;
        this.fund = fund;
    }

    async saveContact() {
        const contact = this.contact;
        let newContact;
        if (!contact.trankeygen_id) {
            newContact = await orcaDataService.postFundObject(this.fund.fund_id, 'contact',
                this.contact);
        }
        else {
            newContact = await orcaDataService.putFundObject(
                this.fund.fund_id, 'contact',
                this.contact.trankeygen_id,
                this.contact);
        }
        if (newContact) {
            // Make sure the saved entry gets updated in the contact list
            contactListActions.updateContactEntry(newContact);
        }
        if (newContact) {
            if (typeof contact == 'string') {
                newContact = JSON.parse(contact)
            }
            // Transfer information back to existing object
            Object.assign(this.contact, newContact);

            // Create a copy, adjust ids and save to ORCA.
            let orcaContact = JSON.parse(JSON.stringify(contact));
            if (!orcaContact.orca_id) {
                orcaContact.orca_id = 0;
                orcaContact.rds_id = orcaContact.trankeygen_id;
            }
            orcaContact.contact1_id = orcaContact.contact1_orca_id ?? 0;
            orcaContact.contact2_id = orcaContact.contact2_orca_id ?? 0;
            //specific orca issue and will be removed when decommissioning ORCA
            orcaContact.lastname = orcaContact.lastname ?? ""

            await ORCA.saveContact(orcaContact);
            return true;
        }
        else {
            alert('Unable to save contact');
            return false;
        }
    }

    async deleteContact(contact = null) {
        if (!contact) {
            contact = this.contact;
        }
        await orcaDataService.deleteFundObject(this.fund.fund_id, 'contact', contact.trankeygen_id).then(result => {
            if (result.success) {
                ORCA.deleteContact(contact);
            } else{
                alert('Something went wrong when deleting the contact!')
            }
        });
    }

    async newContact() {
        Object.assign(this.contact, defaultContact);
    }

    async loadContact(trankeygen_id) {
        let e = await orcaDataService.getFundObject(this.fund.fund_id, 'contact', trankeygen_id)
        Object.assign(this.contact,e);
        return this.contact;
    }
}

const occupationsList =  ref([]);
orcaDataService.getGeneralData( "get-occupations-list").then(data => {
occupationsList.value = data['get-occupations-list'];
});

const contactTypes = ref( [
    {value: "IND",text: "Individual"},
    {value: "CPL",text: "Couple"},
    {value: "GRP", text: "Group Contact"},
    {value: "BUS",text: "Business"},
    {value: "CP", text: "County party committee"},
    {value: "FI", text: "Financial institution"},
    {value: "CAU",text: "Legislative caucus committee"},
    {value: "LDP",text: "Legislative district party committee"},
    {value: "MP", text: "Minor party committee"},
    {value: "OG", text: "Other organization"},
    {value: "PAC",text: "Political action committee"},
    {value: "SP", text: "State party committee"},
    {value: "TRB",text: "Tribal political committee"},
    {value: "UN", text: "Union"}
]);
function useContact()  {
    const {fund} = useOrcaFund();
    const contact = reactive({
        trankeygen_id: null,
        name: '',
        prefix: '',
        firstname: '',
        middleinitial: '',
        lastname: '',
        suffix: '',
        type: '',
        street: '',
        city: '',
        state: '',
        zip: '',
        phone: '',
        email: '',
        memo: '',
        occupation: '',
        employername: '',
        employerstreet: '',
        employercity: '',
        employerstate: '',
        employerzip: '',
        contact1_id: 0,
        contact2_id: 0,
        contact1_orca_id: 0,
        contact2_orca_id: 0
    });
    const contactActions = new ContactActions(contact, fund);
    return {contact: contact, contactActions, contactTypes, occupationsList};
}

export {useContact, ContactActions};
