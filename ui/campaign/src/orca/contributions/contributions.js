import {useOrcaFund} from "@/orca/fund/Fund";
import {reactive} from "vue";
import ORCA from "@/ORCABridge";
import {orcaDataService} from "@/OrcaDataService";
import Papa from "papaparse";

class ContributionsActions {
    constructor(contributionsData, fund) {
        this.contribution = contributionsData;
        this.fund=fund;
    }

    async loadContribution(trankeygen_id){
       let e = await orcaDataService.getFundObject(this.fund.fund_id, 'monetary-contribution', trankeygen_id)
       Object.assign(this.contribution, e);
       return this.contribution;
    }

    async loadRefund(trankeygen_id){
        let e = await orcaDataService.getFundObject(this.fund.fund_id, 'refund-contribution', trankeygen_id)
        Object.assign(this.contribution, e);
        return this.contribution;
    }

    saveContributionToOrca(savedContribution){
        // Deep clone the object to make sure we can get the ID's
        const contribution = JSON.parse(JSON.stringify(savedContribution));

        if (!contribution.orca_id) {
            contribution.orca_id = 0;
            contribution.rds_id = contribution.trankeygen_id;
        }

        contribution.contid = contribution.contid_orca;
        contribution.did = contribution.did_orca;
        contribution.cid = contribution.cid_orca;
        contribution.gid = contribution.gid_orca;
        contribution.pid = contribution.pid_orca;
        ORCA.saveContribution(contribution);
    }

    async saveSplitContribution(primary, general) {
      const contribution = this.contribution;
      contribution.primary_amount = primary;
      contribution.general_amount = general;
      const response = await orcaDataService.postFundObject(
        this.fund.fund_id,
        'split-monetary-contribution',
        this.contribution,
        true
      );

      if (response?.success) {
        const payload = {receipts :  JSON.parse(JSON.stringify(response.contributions))};
        // Change IDS on saved data
        payload.receipts.map(r => {
          if (!r.orca_id) {
            r.orca_id = 0;
          }
          r.rds_id = r.trankeygen_id;
          r.contid_rds = r.contid;
          r.contid = r.contid_orca ?? 0;
          r.cid = r.cid_orca ?? 0;
          r.did = r.did_orca ?? 0;
          r.pid = r.pid_orca ?? 0;
        });
        ORCA.bulkSave(payload);
        return true;
      }
      else {
        alert("Unable to save split contribution!");
        return false;
      }
    }

    async saveContribution() {
        const contribution = this.contribution;
        let savedContribution;

        if (contribution.trankeygen_id) {
            savedContribution = await orcaDataService.putFundObject(
                this.fund.fund_id, 'monetary-contribution',
                this.contribution.trankeygen_id,
                this.contribution);
        }
        else {
            savedContribution = await orcaDataService.postFundObject(this.fund.fund_id, 'monetary-contribution',
                this.contribution);
        }

        if (savedContribution) {
            if (typeof savedContribution == 'string') {
                savedContribution = JSON.parse(savedContribution)
            }
            this.saveContributionToOrca(savedContribution);
            return true;
        }
        else {
            alert('Unable to save monetary contribution');
            return false;
        }
    }

    async saveAnonymousContribution() {
        const contribution = this.contribution;
        let savedContribution;
        contribution.contid = -1;
        contribution.elekey = null;
        if (contribution.trankeygen_id) {
            savedContribution = await orcaDataService.putFundObject(
                this.fund.fund_id, 'anonymous-contribution',
                this.contribution.trankeygen_id,
                this.contribution);
        }
        else {
            savedContribution = await orcaDataService.postFundObject(this.fund.fund_id, 'anonymous-contribution',
                this.contribution);
        }

        if (savedContribution) {
            if (typeof savedContribution == 'string') {
                savedContribution = JSON.parse(savedContribution)
            }
            this.saveContributionToOrca(savedContribution);
            return true;
        }
        else {
            alert('Unable to save anonymous contribution');
            return false;
        }
    }

    async saveContributionRefund() {
        const contribution = this.contribution;
        let savedContribution;
        if (contribution.trankeygen_id) {
            savedContribution = await orcaDataService.putFundObject(
                this.fund.fund_id, 'refund-contribution',
                this.contribution.trankeygen_id,
                this.contribution);
        }
        else {
            savedContribution = await orcaDataService.postFundObject(this.fund.fund_id, 'refund-contribution',
                this.contribution);
        }

        if (savedContribution) {
            if (typeof savedContribution == 'string') {
                savedContribution = JSON.parse(savedContribution)
            }
            this.saveContributionToOrca(savedContribution);
            return true;
        }
        else {
            alert('Unable to save contribution refund');
            return false;
        }
    }

    async saveInKindContribution() {
        const contribution = this.contribution;
        let savedContribution;
        if (contribution.trankeygen_id) {
            savedContribution = await orcaDataService.putFundObject(
                this.fund.fund_id, 'in-kind-contribution',
                this.contribution.trankeygen_id,
                this.contribution);
        }
        else {
            savedContribution = await orcaDataService.postFundObject(this.fund.fund_id, 'in-kind-contribution',
                this.contribution);
        }

        if (savedContribution) {
            if (typeof savedContribution == 'string') {
                savedContribution = JSON.parse(savedContribution)
            }
            this.saveContributionToOrca(savedContribution);
            return true;
        }
        else {
            alert('Unable to save in-kind contribution');
            return false;
        }
    }

    async saveContributions(defaults, contributions) {
      if (this.fund.fund_id) {
        orcaDataService.markPending(true, "Importing contributions...");
        // Apply defaults
        contributions.map((c) => {
          for (const property in defaults) {
            if (!c[property]) {
              c[property] = defaults[property];
            }
          }
        });

        let savedContacts = await orcaDataService.postFundObject(this.fund.fund_id, 'monetary-contributions', {contributions}, true);
        orcaDataService.markPending(false);
        if (savedContacts) {
          // duplicating object for sending to ORCA>
          let response = JSON.parse(JSON.stringify(savedContacts));
          response.contacts.map(c => {
            if (!c.orca_id) {
              c.orca_id = 0;
            }
            c.rds_id = c.trankeygen_id;
            c.contact1_id = c.contact1_orca_id ?? 0;
            c.contact2_id = c.contact2_orca_id ?? 0;
          });
          response.receipts.map(r => {
            if (!r.orca_id) {
              r.orca_id = 0;
            }
            r.rds_id = r.trankeygen_id;
            r.contid_rds = r.contid;
            r.contid = r.contid_orca ?? 0;
            r.cid = r.cid_orca ?? 0;
            r.did = r.did_orca ?? 0;
            r.pid = r.pid_orca ?? 0;
          });
          ORCA.bulkSave(response);
        }
        return savedContacts;
      }
    }

    async deleteContribution(){
      const contribution =  this.contribution
      let deleted = await orcaDataService.deleteFundObject(this.fund.fund_id, 'contribution', contribution.trankeygen_id)
      // Orca delete contribution
      if(deleted?.success){
        await ORCA.deleteContribution(this.contribution)
      }
      return !!deleted;
    }
    newContribution() {
      // Clear out fields that we don't want to carry forward.
      this.contribution.contid = 0;
      this.contribution.orca_id = 0;
      this.contribution.trankeygen_id = 0;
      this.contribution.name = null;
      this.contribution.rds_id = 0;
      this.contribution.deposit_date = "";
      this.contribution.memo = null;
      this.contribution.checkno = null;
      this.contribution.amount = null;
      this.contribution.itemized = 0;
      this.contribution.description = "";
      this.contribution.pagg=0.0;
      this.contribution.gagg=0.0;
    }


  exportContributions(contributions) {
    let data = contributions.map(c => (
      {
        amount: c.amount,
        date: c.date_,
        type: c.type,
        name: c.name,
        prefix: c.prefix,
        firstname: c.firstname,
        middleinitial: c.middleinitial,
        lastname: c.lastname,
        suffix: c.suffix,
        street: c.street,
        city: c.city,
        state: c.state,
        zip: c.zip,
        phone: c.phone,
        email: c.email,
        occupation: c.occupation,
        employername: c.employername,
        employerstreet: c.employerstreet,
        employercity: c.employercity,
        employerstate: c.employerstate,
        employerzip: c.employerzip,
        memo: c.memo,
        election: c.elekey
      }
    ))
    let csv = Papa.unparse(data);
    let filename = 'Contributions';
    let blob = new Blob([csv], {type: 'text/csv;charset=utf-8;'});
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, filename);
    } else {
      let link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        let url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

}

function useContribution() {
    const {fund} = useOrcaFund();
    const contribution = reactive({
      trankeygen_id: 0,
      contid: 0,
      name: null,
      elekey: null,
      cid: null,
      date_: null,
      deposit_date: null,
      checkno: null,
      itemized: 0,
      pid: null,
      amount: null,
      memo: null,
      description: null,
      total_refunded: null
    });
    const contributionsActions = new ContributionsActions(contribution, fund);
    return {contribution, contributionsActions}
}

export {useContribution}