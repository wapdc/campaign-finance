import {useOrcaFund} from "@/orca/fund/Fund";
import {reactive} from "vue";
import {orcaDataService} from "@/OrcaDataService";
import ORCA from "@/ORCABridge";

class LoansActions {
  constructor(loansData, fund) {
    this.loan = loansData;
    this.fund = fund;
  }

  async loadLoan(trankeygen_id) {
    let e = await orcaDataService.getFundObject(this.fund.fund_id, 'loan', trankeygen_id)
    Object.assign(this.loan, e);

    if (this.loan.data === null || this.loan.data === undefined) {
      this.loan.data = {
        committee_name: null,
        election_year: null,
      }
    }

    return this.loan;
  }

  async saveCashLoan() {
    const loan = this.loan;
    let savedLoan;

    if (this.loan.carryforward === 0) {
      delete this.loan.data
    }

    if (loan.trankeygen_id) {
      savedLoan = await orcaDataService.putFundObject(this.fund.fund_id, 'monetary-loan', this.loan.trankeygen_id, this.loan, true)
    } else {
      savedLoan = await orcaDataService.postFundObject(this.fund.fund_id, 'monetary-loan', this.loan, true)
    }


    if (savedLoan) {
      if (typeof savedLoan == 'string') {
        savedLoan = JSON.parse(savedLoan);
      }
      Object.assign(this.loan, savedLoan.loan);
      this.saveLoanToOrca(savedLoan.loan);
      return true;
    } else {
      alert('Unable to save cash loan');
      return false
    }
  }

  async saveInKindLoan() {
    const loan = this.loan;
    let savedLoan;

    if (this.loan.carryforward === 0) {
      delete this.loan.data
    }

    if (loan.trankeygen_id) {
      savedLoan = await orcaDataService.putFundObject(this.fund.fund_id, 'in-kind-loan', this.loan.trankeygen_id, this.loan, true)
    } else {
      savedLoan = await orcaDataService.postFundObject(this.fund.fund_id, 'in-kind-loan', this.loan, true)
    }

    if (savedLoan) {
      if (typeof savedLoan == 'string') {
        savedLoan = JSON.parse(savedLoan);
      }
      Object.assign(this.loan, savedLoan.loan);
      this.saveLoanToOrca(savedLoan.loan);
      return true;
    } else {
      alert('Unable to in-kind cash loan');
      return false
    }
  }


  saveLoanToOrca(savedLoan) {
    // Deep clone the object to make sure we can get the ID's
    const loan = JSON.parse(JSON.stringify(savedLoan));

    if (!loan.orca_id) {
      loan.orca_id = 0;
      loan.rds_id = loan.trankeygen_id;
    }

    //accounts
    loan.did_rds = loan.loan_account_id;
    loan.forgiveness_rds = loan.forgiveness_account_id;
    loan.interest_rds = loan.interest_account_id;

    loan.contid = loan.contid_orca;
    loan.cid = loan.cid_orca;
    loan.did = loan.did_orca;
    loan.pid = loan.pid_orca;
    ORCA.saveLoan(loan);
  }

  newLoan() {
    // Clear out fields that we don't want to carry forward.
    this.loan.contid = 0;
    this.loan.orca_id = 0;
    this.loan.cid = null;
    this.loan.trankeygen_id = 0;
    this.loan.name = null;
    this.loan.rds_id = 0;
    this.loan.repaymentschedule = "As funds become available";
    this.loan.duedate = null;
    this.loan.deposit_date = "";
    this.loan.memo = null;
    this.loan.checkno = null;
    this.loan.amount = null;
    this.loan.description = "";
    this.loan.pagg = 0.0;
    this.loan.gagg = 0.0;
    this.loan.carryforward = 0;
    this.loan.interestrate = null;
    this.loan.balance = 0;
    this.loan.original_loan_date = '';
    this.loan.data = {
      committee_name: '',
      election_year: '',
    };
  }
}

function useLoan() {
  const {fund} = useOrcaFund();
  const loan = reactive({
    trankeygen_id: 0,
    contid: 0,
    cid: null,
    amount: null,
    date_: null,
    elekey: 0,
    interestrate: null,
    duedate: null,
    checkno: null,
    repaymentschedule: "As funds become available",
    carryforward: 0,
    memo: null,
    balance: 0,
    original_loan_date: null,
    data: {
      committee_name: null,
      election_year: null,
    }
  });
  const loansActions = new LoansActions(loan, fund);
  return {loan, loansActions}
}

export {useLoan}
