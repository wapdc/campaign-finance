import {useOrcaFund} from "@/orca/fund/Fund";
import {reactive} from "vue";
import ORCA from "@/ORCABridge";
import {orcaDataService} from "@/OrcaDataService";

class ExpenseActions {

  constructor(expenditureData, fund) {
    this.expenditure = expenditureData;
    this.fund = fund;
  }

  async loadExpenditure(trankeygen_id) {
    let e = await orcaDataService.getFundObject(this.fund.fund_id, 'expenditure', trankeygen_id)
    this.expenditure.expenditureEvent = e.expenditureEvent;
    this.expenditure.expenses = e.expenses;
    this.expenditure.originalAmount = e.expenditureEvent.amount;
    this.expenditure.originalBnkId = e.expenditureEvent.bnkid;
    await this.loadAvailableBalance();
    return this.expenditure;
  }

  newExpenditure() {
    let eEvent = this.expenditure.expenditureEvent;
    // Clear out fields that we don't want to carry forward.
    eEvent.contid = 0;
    eEvent.orca_id = 0;
    eEvent.trankeygen_id = 0;
    eEvent.rds_id = 0;
    eEvent.expense_id = 0;
    eEvent.originalAmount = 0.0;
    eEvent.originalBnkid = eEvent.bnkid;
    eEvent.date_ = "";
    eEvent.memo = "";
    eEvent.checkno = "";

    if (this.expenditure.expenses.length > 0) {
      this.expenditure.expenses = this.expenditure.expenses.slice(0, 1);
      this.expenditure.expenses[0].rds_id = null;
      this.expenditure.expenses[0].amount = null;
      this.expenditure.expenses[0].user_description = "";
    } else {
      this.expenditure.expenses.push(
        {
          cid: 0,
          amount: null,
          user_description: ""
        }
      )
    }
  }

  updateExpenditureBalance() {
    this.expenditure.expenditureEvent.amount = this.expenditure.expenses.reduce((s, i) => Number.parseFloat(i.amount) + s, 0.0);
  }

  async loadAvailableBalance() {
    const data = await orcaDataService.getFundData(this.fund.fund_id, 'get-account-balance', {
      fund_id: this.fund.fund_id,
      trankeygen_id: this.expenditure.expenditureEvent.bnkid
    });

    // Retrieve the current balance
    this.expenditure.acctnum = parseInt(data['get-account-balance'][0]['acctnum']);

    if (this.expenditure.acctnum < 2000) {
      this.expenditure.balance = parseFloat(data['get-account-balance'][0]['total']);
      // If we haven't changed the bank id then we need to add the current total amount
      // of the transaction back in so the check for available funds works right.
      if (this.expenditure.originalBnkId === this.expenditure.expenditureEvent.bnkid) {
        this.expenditure.balance += parseFloat(this.expenditure.originalAmount);
      }
    } else {
      this.expenditure.balance = -1 * parseFloat(data['get-account-balance'][0]['total']);
    }
  }

  hasSufficientFunds(addedAmount) {
    this.updateExpenditureBalance();
    let amount = parseFloat(this.expenditure.expenditureEvent.amount);
    if (addedAmount) {
      amount += parseFloat(addedAmount);
    }
    return (this.expenditure.acctnum >= 2000) || (amount <= this.expenditure.balance);
  }

  async deleteExpenditure() {
    const getAccountOrcaId = (id) => {
      let orca_id = 0;
      if (id) {
        const account = this.fund.accounts.find(a => Number.parseInt(a.trankeygen_id) === Number.parseInt(id));
        if (account) {
          orca_id = account.orca_id;
        }
      }
      return orca_id;
    }
    let deleted = await orcaDataService.deleteFundObject(this.fund.fund_id, 'expenditure', this.expenditure.expenditureEvent.trankeygen_id);
    //send over expenditureEvent so that orca_id can be mapped using existing ORCA model
    this.expenditure.expenditureEvent.bnkid = getAccountOrcaId(this.expenditure.expenditureEvent.bnkid);
    await ORCA.deleteExpenditure(this.expenditure.expenditureEvent);
    return !!deleted;
  }

  async saveExpenditure() {
    const expenditureEvent = this.expenditure.expenditureEvent;
    let expenditure;

    if (expenditureEvent.trankeygen_id) {
      expenditure = await orcaDataService.putFundObject(
        this.fund.fund_id, 'expenditure',
        expenditureEvent.trankeygen_id,
        {expenditure: this.expenditure});
    } else {
      expenditure = await orcaDataService.postFundObject(this.fund.fund_id, 'expenditure',
        {expenditure: this.expenditure});
    }
    if (expenditure) {
      if (typeof expenditure == 'string') {
        expenditure = JSON.parse(expenditure)
      }
      this.expenditure.expenditureEvent = expenditure.expenditureEvent;
      this.expenditure.expenses = expenditure.expenses;
      this.saveExpenditureToOrca();
      this.loadAvailableBalance();
      return true;
    } else {
      alert('Unable to save expenditure');
      return false;
    }
  }

  saveExpenditureToOrca() {
    const getAccountOrcaId = (id) => {
      let orca_id = 0;
      if (id) {
        const account = this.fund.accounts.find(a => Number.parseInt(a.trankeygen_id) === Number.parseInt(id));
        if (account) {
          orca_id = account.orca_id;
        }
      }
      return orca_id;
    }

    // Deep clone the object to make sure we can get the ID's
    const expenditure = JSON.parse(JSON.stringify(this.expenditure));

    const expenditureEvent = expenditure.expenditureEvent;
    expenditureEvent.contid = expenditure.expenditureEvent.contid_orca;

    // Replace the accounts on the objects with the account in ORCA.
    expenditureEvent.bnk_rds_id = expenditureEvent.bnkid;
    expenditureEvent.bnkid = getAccountOrcaId(expenditureEvent.bnkid);

    if (expenditure.expenses) {
      expenditure.expenses.map(expense => {
        expense.did = expenditureEvent.bnkid;
        expense.cid = getAccountOrcaId(expense.cid);
        expense.contid = expense.contid_orca;
      });
    }
    ORCA.saveExpenditure(expenditure);
  }

  getExpense(idx) {
    if (this.expenditure.expenses.length >= idx + 1) {
      return this.expenditure.expenses[idx];
    } else {
      return null;
    }
  }

  removeExpense(idx) {
    if (this.expenditure.expenses.length >= idx + 1) {
      this.expenditure.expenses.splice(idx, 1);
    }
  }
}

function useExpenditure() {
  const {fund} = useOrcaFund();
  const expenditure = reactive({
    expenditureEvent: {
      orca_id: 0,
      rds_id: 0,
      date_: "",
      bnkid: 0,
      contid: 0,
      checkno: "",
      acctnum: 0,
      memo: "",
      amount: 0.0,
      itemized: 0
    },
    expenses: [
      {
        cid: 0,
        amount: null,
        user_description: ""
      }
    ],
    balance: 0.0,
    originalBnkId: null,
    originalAmount: 0.0,
  });
  const expenseActions = new ExpenseActions(expenditure, fund);
  return {expenditure, expenseActions}
}

export {useExpenditure}