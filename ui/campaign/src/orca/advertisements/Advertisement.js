import {useOrcaFund} from "@/orca/fund/Fund";
import {reactive, ref} from "vue";
import {orcaDataService} from "@/OrcaDataService";

const TARGET_TYPES = {
  proposal: {
    id: 'proposal_id',
    target: 'proposals',
  },
  candidacy: {
    id: 'candidacy_id',
    target: 'candidacies',
  },
}

class Advertisement {

  constructor(fund) {
    this.advertisement = reactive({
      ad: {
        fund_id: null,
        first_run_date: null,
        last_run_date: null,
        description: "",
        outlets: ""
      },
      targets: {
        candidacies: [],
        proposals: [],
      },
      contributions: []
    });
    this.fund = fund;
    this.warnings = ref([]);
    this.errors = ref([]);
    this.instance = null;
    this.loadAdvertisement = this.loadAdvertisement.bind(this);
    this.deleteAdvertisement = this.deleteAdvertisement.bind(this);
    this.saveAdvertisement = this.saveAdvertisement.bind(this);
    this.recalculatePercentages = this.recalculatePercentages.bind(this);
    this.resetAdvertisement = this.resetAdvertisement.bind(this);
  }

  static getInstance(fund) {
    if (!this.instance) {
      this.instance = new Advertisement(fund);
    }
    return this.instance;
  }

  async loadAdvertisement(ad_id) {
    const {ad, targets, contributions} = await orcaDataService.getFundObject(this.fund.fund_id, 'advertisement', ad_id)
    this.advertisement.ad = ad;

    if (targets) {
      this.advertisement.targets = targets.reduce((acc, target) => {
        switch (target.target_type) {
          case 'candidacy':
            acc.candidacies.push(Object.assign({}, target, {
              display_name: `${target.ballot_name} (${target.office_title}, ${target.jurisdiction_name})`,
            }));
            break;
          case 'proposal':
            acc.proposals.push(target);
            break;
          default:
            throw Error('Attempted to load invalid target type from database.')
        }
        return acc;
      }, {candidacies: [], proposals: []});
      this.recalculatePercentages(this.advertisement.targets)
    } else {
      this.advertisement.targets = {
        candidacies: [],
        proposals: []
      };
    }

    if (contributions) {
      this.advertisement.contributions = contributions;
    } else {
      this.advertisement.contributions = [];
    }
    return this.advertisement;
  }

  /*
  * @
  * **/
  recalculatePercentages(targets) {
    // determine remaining available percentage and id's of targets requiring calculation
    let percentageSpecified = 0;
    const calculatedTargets = [];

    for (const type of Object.keys(TARGET_TYPES)) {
      const typeData = TARGET_TYPES[type];
      targets[typeData.target].forEach((target) => {
        if (target.percent) {
          // three decimal precision
          percentageSpecified += Math.round(target.percent * 1000);
          delete target.calculatedPercentage;
        } else {
          calculatedTargets.push(target);
        }
      });
    }

    // three decimal precision
    let percentageAvailable = Math.max(100000 - percentageSpecified, 0);
    for (let target of calculatedTargets) {
      target.calculatedPercentage = percentageAvailable / calculatedTargets.length / 1000;
    }
    // warn about totals outside of expected norms
    let hasCalculated = !!calculatedTargets.length;
    let percentageSpecifiedDisplay = (percentageSpecified / 1000).toPrecision(3) / 1;
    this.warnings.value = [];
    this.errors.value = [];
    if (percentageSpecified < 0) {
      this.errors.value.push('The percentages sum to ' + percentageSpecifiedDisplay + '%, when they are expected add up to more than zero.');
    } else if (hasCalculated && percentageSpecified >= 100000) {
      this.errors.value.push('The percentages sum to ' + percentageSpecifiedDisplay + '%, when they are expected to add up to less than 100%.');
    } else if (!hasCalculated && percentageSpecified >= 101000) {
      this.errors.value.push('The percentages sum to ' + percentageSpecifiedDisplay + '%, when they are expected add up to less than 101%.');
    } else if (!hasCalculated && percentageSpecified <= 99000) {
      this.warnings.value.push('The percentages sum to ' + percentageSpecifiedDisplay + '%, when they are expected add up to more than 99%.');
    }
  }

  async deleteAdvertisement() {
    let success;
    success = await orcaDataService.delete(this.fund.fund_id, 'advertisement', this.advertisement.ad.ad_id);
    if (success) {
      return true;
    } else {
      alert("Unable to delete advertisement");
      return false;
    }
  }

  resetAdvertisement() {
    this.advertisement = reactive({
      ad: {
        fund_id: null,
        first_run_date: null,
        last_run_date: null,
        description: "",
        outlets: ""
      },
      targets: {
        candidacies: [],
        proposals: [],
      }
    });
    const {fund} = useOrcaFund()
    this.instance = new Advertisement(fund)
  }

  async saveAdvertisement() {
    let ad;
    const targets = this.advertisement.targets ? Object.values(this.advertisement?.targets).reduce((acc, arr) => {
      return [...acc, ...arr]
    }, []) : [];
    ad = await orcaDataService.postFundObject(this.fund.fund_id, 'advertisement',
      {advertisement: {...this.advertisement, targets}});
    if (ad) {
      this.advertisement.ad = ad.ad;
      return true;
    } else {
      alert('Unable to save advertisement');
      return false;
    }
  }

}

function useAdvertisement() {
  const {fund} = useOrcaFund();
  return Advertisement.getInstance(fund);
}

export {useAdvertisement};
