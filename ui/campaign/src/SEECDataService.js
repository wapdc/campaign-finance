import axios from 'axios';
import {app} from "@/main";

export default class DataService {

  pendingCount = 0;

  seec_service_host = 'https://web6.seattle.gov/ethics/pdcElectionFilings/api/SEECApiFiling';

  /**
   * Constructor
   *
   * Sets an appropriate url handling our development environment.
   */
  constructor() {
    if (location.host.search('localhost') >= 0) {
      //post local server host
      this.seec_service_host = 'https://webqa6.seattle.gov/ethics/pdcElectionFilings/api/SEECApiFiling'
    } else if (location.host.search('demo-apollo-d8.pantheonsite.io') >= 0) {
      this.seec_service_host = 'https://webqa6.seattle.gov/ethics/pdcElectionFilings/api/SEECApiFiling'
    }
  }

  markPending(isPending, message) {
    if (!isPending) {
      this.pendingCount--;
      if (this.pendingCount <= 0) {
        app.$nextTick(() => {
          app.$container.wire.$emit('SeecService.pending', isPending, message);
        });
      }
    } else {
      this.pendingCount++;
      app.$container.wire.$emit('SeecService.pending', isPending, message);
    }
  }

  post(post_data, callback) {
    // Fix common UTF-8 problems prior to posting.
    const jsonString = JSON.stringify(post_data)
      .replace(/[\u200B-\u200F\u202A-\u202E\u2060-\u2064\u2066-\u2069]/g, '')
      .replace(/[\u0026]/g, '&')
      .replace(/[\u2009]/g, ' ');

    axios.post(this.seec_service_host, jsonString).then(result => {
      // Result is an XML string
      this.markPending(true);
      let data = {};
      let s = result.data;
      if (s.search("InstantiateFiling_PdcApi")) {
        data.success = true;
      } else {
        data.success = false;
        data.error = this.err_message;
      }
      this.markPending(false);
      callback(data);
    })
      .catch((err) => {
        this.markPending(false);
        let data = {};
        data.success = false;
        callback(data);
        console.log("AXIOS ERROR: ", err);
      });
  }

  /**
   * Try and upload the report to the city of seattle.
   *
   * @param report
   *   object of the report
   * @param user
   *   email of the currently logged in user.
   * @param stage
   *   indicates whether to post to SEEC dev servers
   * @returns {Promise<unknown>}
   *   Returns the results of the upload including any error messages that
   *   might be returned by the service.
   */
  async uploadReportToSeattle(report, user, stage) {
    // Deep clone the user data so that we don't modify it by mistake.
    const seattleReport = JSON.parse(JSON.stringify({report: report.user_data}));
    let host = this.seec_service_host;
    this.markPending(true);

    if (stage === 'dev') {
      host = "https://webdev6.seattle.gov/ethics/pdcElectionFilings/api/SEECApiFiling"
    } else if (stage === 'prod') {
      host = "https://web6.seattle.gov/ethics/pdcElectionFilings/api/SEECApiFiling"
    }

    // Populate the certification email.
    if (user) {
      seattleReport.certification_email = user;
    }

    // Fix common UTF-8 problems prior to posting.
    const jsonString = JSON.stringify(seattleReport)
      .replace(/[\u200B-\u200F\u202A-\u202E\u2060-\u2064\u2066-\u2069]/g, '')
      .replace(/[\u0026]/g, '&')
      .replace(/[\u2009]/g, ' ');

    let error_result;
    error_result = {
      message: "Sorry, this application is having trouble connecting to the SEEC upload server. Please contact the SEEC IT staff at  www.seattle.gov/ethics/contact-us. We appreciate your patience.",
    }
    // Attempt to send
    let result = await axios.post(host, jsonString, {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      }
    }).catch((e) => console.log(e));
    this.markPending(false);

    if (!result || !result.data) {
      console.log("Error", result);
      result = error_result;
    } else {
      result = JSON.parse(result.data);
      if (!result.message) {
        result.message = error_result.message;
      }
    }
    console.log("Returning", result);
    return new Promise(resolve => resolve(result));
  }
}