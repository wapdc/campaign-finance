import axios from 'axios';
import {app} from './main';

const datasets = {
  IMAGED_DOCUMENTS: {
    live: 'j78t-andi',
    demo: 'j78t-andi'
  },
  REPORTING_HISTORY: {
    live: '7qr9-q2c9',
    demo: 'sht2-chsv'
  }
}

class SocrataDataService {

  service_host = 'https://data.wa.gov';

  context = '';

  pendingRequests = 0;

  errorReason = {};

  /**
   * Constructor
   *
   * Sets an appropriate url handling our development environment.
   */
  constructor() {
    if ((location.host.search('demo') >= 0) ||
      (location.host.search('lndo') >= 0) ||
      (location.host.search('localhost') >= 0)) {
      this.context = 'demo';
    } else {
      this.context = 'live';
    }
  }

  /**
   * Handle errors in all axios calls.
   * @param r
   */
  handleError(response) {
    if (response.status === 400) {
      app.$container.wire.$emit('data.fetch_failed');
    }
    this.resolvePending();
  }

  setPending() {
    if (this.pendingRequests === 0) {
      app.$container.wire.$emit('SocrataService.pending', true);
    }
    this.pendingRequests++;
  }

  resolvePending() {
    if (this.pendingRequests > 0) {
      this.pendingRequests--;
    }
    if (this.pendingRequests === 0) {
      app.$container.wire.$emit('SocrataService.pending', false);
    }
  }

  /**
   *
   * @param dataset
   * @param filer_id
   * @param election_year
   * @returns {Promise<AxiosResponse<any>>}
   */
  async getRecords(dataset, {filer_id, election_year, origin, period_start = '', period_end = ''}) {
    const url = `https://data.wa.gov/resource/${datasets[dataset][this.context]}.json?`;
    const query = `select * where election_year='${election_year}' and filer_id='${filer_id}' and origin='${origin}' ${period_start ? `and report_from='${period_start}'` : ''} ${period_end ? `and report_to='${period_end}'` : ''}`;
    this.setPending();
    const res = await axios.get(url, {
      params: {
        $query: query
      }
    }).catch(e => {
      this.handleError(e.response)
    });
    this.resolvePending();
    return res;
  }
}

const service = new SocrataDataService();

export default service;