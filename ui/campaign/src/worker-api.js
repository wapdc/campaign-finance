import Worker from 'worker-loader!./worker';
import Vue from 'vue';

let webWorker;
/**
 * Worker api exposes a messaging interface for communicating with
 * a background process that performs discrete tasks, but without
 * blocking user interface.
 *
 * You call executeTask with a function name as the first parameter
 * in order execute a background task as defined in worker.js. Function
 * results get obtained by listening to events emitted the processor using
 * typical processor.$on calls.  The event names must be found in contact
 */

const initWorker = function () {
  webWorker = new Worker();
  webWorker.addEventListener('message', (e) => {
    console.log('listener got', e)
    if (e.data.event && e.data.message) {
      if (e.data.event === 'task-started') {
        worker.active = true;
        worker.progress = 0;
        worker.currentTask = e.data.message;
      }
      if (e.data.event === 'progress-updated') {
        worker.progress = e.data.message;
      }
      if (e.data.event === 'task-completed') {
        worker.active = false;
        worker.progress = 100;
      }
      worker.$emit(e.data.event, e.data.message);
    }
  });
}
// Exposes the request for the worker
const privateExecuteTask = function (task, data) {
  webWorker.postMessage({task, data})
}

// This view object is designed
const worker = new Vue(
  {
    data: () => {
      return {
        progress: 0,
        currentTask: "",
        active: false,
      }
    },
    methods: {
      executeTask(task, data) {
        privateExecuteTask(task, data);
      },
      terminateTask() {
        webWorker.terminate();
        initWorker();
        this.progress = 0;
        this.currentTask = "";
        this.active = false;
      }
    }
  }
);

export function useWorker() {
  initWorker();
  return {worker}
}