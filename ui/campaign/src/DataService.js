import axios from 'axios';
import {app} from './main';

export default class DataService {


  /**
   * Constructor
   *
   * Sets an appropriate url handling our development environment.
   */
  constructor() {
    this.headers = {
      'Content-Type': 'application/json;charset=UTF-8'
    };
    this.service_host = window.localStorage.apollo_url ?? 'https://apollod8.lndo.site/';
    this.pendingRequests = 0;
    this.errorReason = {};

    if (location.host.search('localhost') >= 0) {
      // Assume ports that start with 818 (e.g. 8180-8189 will connect to demo environment)
      // so that devs with no lando work.
      if (location.host.search('818') > 0) {
        this.service_host =  'https://demo-apollo-d8.pantheonsite.io/';
      } else {
        this.service_host = window.localStorage.apollo_url ?? 'https://apollod8.lndo.site/';
      }
    } else {
      this.service_host = '/';
    }
  }

  /**
   * Bump up the pending requests and notify the ui that there is a data request pending.
   */
  setPending() {
    if (this.pendingRequests === 0) {
      app.$container.wire.$emit('DataService.pending', true);
    }
    this.pendingRequests++;
  }

  /**
   * Tell the UI when all data requests have been resolved.
   */
  resolvePending() {
    if (this.pendingRequests > 0) {
      this.pendingRequests--;
    }
    if (this.pendingRequests === 0) {
      app.$container.wire.$emit('DataService.pending', false);
    }
  }

  /**
   * Handle errors in all axios calls.
   * @param r
   */
  handleError(r) {
    if (r.response && r.response.hasOwnProperty('status') && r.response.status === 403) {
      app.$container.wire.$emit('authenticate');
    }
    this.errorReason = r;
    this.resolvePending();
    alert(r.message);
  }

  /**
   * Call a web service that expects to return json.
   * This is used for axios call when you don't want
   * the throbber to popup.
   */
  getPassive(path, callback, parms = {}) {
    let url = this.service_host + path;
    axios.get(url, {withCredentials: true, params: parms})
      .then(result => {
        callback(result.data);
      })
      .catch(r => {
        console.log("Network Error:", r)
      });
  }

  /**
   * Call a web service that expects to return json.
   * @param path
   *   Indicates the path of the webservice to be called.
   * @param callback
   *   A callable that processes parsed json data from axios
   * @param parms
   *   An object containing URL parameters to send with request.
   */
  get(path, callback, parms = {}) {
    let url = this.service_host + path;
    this.setPending();
    axios.get(url, {withCredentials: true, params: parms})
      .then(result => {
        callback(result.data);
        this.resolvePending();
      })
      .catch(r => {
        this.handleError(r)
      });
  }

  async getDraftReport(committee_id, draft_id) {
      return axios.get( this.service_host +`service/campaign-finance/committee/${committee_id }/draft-report/${draft_id }`, {
          withCredentials: true,
      });
  }

  put(path, callback, post_data, get_data = {}) {
    let url = this.service_host + path;
    this.setPending();
    axios.put(url, JSON.stringify(post_data), {withCredentials: true, params: get_data})
      .then(result => {
        callback(result);
        this.resolvePending();
      })
      .catch(r => {
        this.handleError(r);
      });
  }

  /**
   * Make a delete web service call.
   * @param path
   *   Indicates the path of the webservice to be called.
   * @param callback
   *   A callable that processes parsed json data from axios
   * @param parms
   *   An object containing URL parameters to send with request.
   */
  delete(path, callback, parms = {}) {
    let url = this.service_host + path;
    this.setPending();
    axios.delete(url, {withCredentials: true, params: parms})
      .then(result => {
        callback(result);
        this.resolvePending();
      })
      .catch(r => {
        this.handleError(r)
      });
  }

  post(path, callback, post_data, get_data = {}) {
    let url = this.service_host + path;
    this.setPending();
    axios.post(url, JSON.stringify(post_data), {withCredentials: true, params: get_data})
      .then(result => {
        callback(result.data);
        this.resolvePending();
      })
      .catch(r => {
        this.handleError(r)
      });
  }

  /**
   * Promised base
   * @param committee_id
   * @param action
   * @param data
   * @returns {Promise<void>}
   */
  async campaignFinancePost(committee_id, action, data ) {
    const url = this.service_host + 'service/campaign-finance/committee/' + committee_id + '/' + action;
    this.setPending();
    const result =  await axios.post(url,  JSON.stringify(data), {headers: this.headers, withCredentials: true}).catch(() => {
      this.resolvePending();
      alert ('Error posting to ' + action);
    });
    this.resolvePending()
    return result?.data;
  }

  fileUpload(url, callback, form_data, catchErrorCallback) {
    this.setPending();
    axios.post(url, form_data)
      .then(result => {
        callback(result.data);
        this.resolvePending();
      })
      .catch(r => {
        this.handleError(r);
        catchErrorCallback()
      });
  }

  query(sql_base, callback, parms = {}) {
    this.get(
      'service/campaign-registration-query/' + sql_base,
      data => {
        if (data.success) {
          callback(data.data);
        } else {
          alert(data.errors);
        }
      }, parms);
  }

  adminQuery(sql_base, callback, parms = {}) {
    this.get(
      'service/campaign-admin-query/' + sql_base,
      data => {
        if (data.success) {
          callback(data.data);
        } else {
          alert(data.errors);
        }
      }, parms);
  }

  sponsors(callback, parms = {}) {
    this.get(
      'service/sponsors',
      data => {
        if (data.success) {
          callback(data.data);
        } else {
          alert(data.error);
        }
      }, parms);
  }

  reportDelete(callback, repno) {
    this.delete(
      'service/report/' + repno,
      (response) => {
        if (response.statusText === 'OK') {
          callback(response.data.data);
        } else {
          alert(response.error);
        }
      });
  }

  reportFetchAllData(callback, repno) {
    this.get(
      'service/report/' + repno,
      (data) => {
        if (data.success) {
          callback(data.data);
        } else {
          alert(data.error);
        }
      });
  }

  getUserToken(committee_id, callback, parms = {}) {
    this.get(
      'service/committee/' + committee_id + '/committee_api_keys',
      data => {
        if (data.success) {
          callback(data.data);
        } else {
          alert(data.errors);
        }
      }, parms);
  }

  getAuthorizedUsers(committee_id, callback, parms = {}) {
    this.get(
      'service/committee/' + committee_id + '/get_authorized_users',
      data => {
        if (data.success) {
          callback(data.data);
        } else {
          alert(data.errors);
        }
      }, parms);
  }

  amendRegistration(registrationId, committeeId, callback) {
    this.post('service/committee/' + committeeId + '/amend',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          alert(result.errors);
        }
      },
      {registration_id: registrationId, committee_id: committeeId}
    );
  }

  copyCommittee(registration, electionCode, callback) {
    let committee_id = registration.committee.committee_id;
    this.post('service/committee/' + committee_id + '/copy',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          alert(result.errors);
        }
      },
      {registration: registration, electionCode: electionCode});
  }

  committee(committee_id, callback) {
    this.get('service/committee/' + committee_id,
      data => {
        if (data.success) {
          callback(data.data);
        }
      },
      {committee_id: committee_id})
  }

  committeeDraft(committee_id, callback) {
    this.get('service/committee/' + committee_id + '/draft',
      data => {
        if (data.success) {
          callback(data.data);
        }
      })
  }

  createCommittee(data, callback) {
    this.post(
      'service/committee',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          alert(result.errors);
        }
      },
      data
    );
  }

  saveCommitteeDraft(data, callback) {
    let committee_id = data.committee.committee_id;
    this.post('service/committee/' + committee_id + '/draft',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          alert(result.errors);
        }
      },
      data
    );
  }

  deleteCommitteeDraft(committee_id, callback) {
    this.post('service/committee/' + committee_id + '/cancel-edit',
      result => {
        if (result.success) {
          callback(result);
        } else {
          alert(result.errors);
        }
      },
    );
  }

  submitDraft(registration, callback) {
    let committee_id = registration.committee.committee_id;
    this.post('service/committee/' + committee_id + '/register',
      callback,
      registration);
  }

  getRegistrationLogs(registrationId, callback) {
    this.query('registration_logs', callback, {registration_id: registrationId});
  }

  logRegistrationAction(committee_id, registrationId, log, callback) {
    this.post('service/committee/' + {committee_id} + '/log', callback, log);
  }

  addApiToken(committeeId, memo, callback) {
    this.post('service/committee/' + committeeId + '/generate-api-token',
      result => {
        if (result.success) {
          callback(result.data);
        }
      },
      {committee_id: committeeId, memo: memo}
    );
  }

  revokeApiToken(committee_id, tokenId, callback) {
    this.post('service/committee/' + committee_id + '/revoke-api-token',
      result => {
        if (result.success) {
          callback(result.data);
        }
      },
      {api_key_id: tokenId}
    );
  }

  authorizeUser(committee_id, email, role, reason, callback) {
    this.post('service/admin-process/authorize-user',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          callback(result.errors);
        }
      },
      {committee_id, email, role, reason});
  }

  registration(registration_id, callback) {
    this.get('service/get-registration',
      data => {
        if (data.success) {
          callback(data.data);
        }
      },
      {registration_id: registration_id});
  }


  currentRegistration(committee_id, callback) {
    this.get('service/committee/' + committee_id + '/current',
      data => {
        if (data.success) {
          callback(data.data);
        } else {
          alert(data.errors)
        }
      });
  }

  unverifyRegistration(registration, delete_filer_id, callback) {
    let data = {
      registration_id: registration.registration_id,
      admin_data: registration.admin_data,
      delete_filer_id: delete_filer_id
    };
    this.post('service/admin-process/unverify-registration',
      result => {
        if (result.success) {
          this.registration(data.registration_id, callback);
        } else {
          alert(result.errors);
        }
      }, data);
  }

  updateCandidacyStatus(callback, data) {
    this.post('service/admin-process/update-candidacy-status',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          alert(result.errors);
        }
      },
      {
        committee_id: data.committee_id,
        primary_result: data.primary_result,
        general_result: data.general_result,
        declared_date: data.declared_date,
        exit_date: data.exit_date,
        exit_reason: data.exit_reason
      });
  }

  updateAdminMemo(callback, data) {
    this.post('service/admin-process/update-admin-memo',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          callback(result.errors);
        }
      },
      {committee_id: data.committee_id, memo: data.memo});
  }

  editCommittee(callback, data) {
    this.post('service/admin-process/update-committee', result => {
        if (result.success) {
          callback(result);
        } else {
          callback(result);
        }
      },
      data);
  }

  committeeFinanceInfo(committee_id, callback) {
    this.get('service/campaign-finance/committee/' + committee_id + '/finance-info', callback);
  }


  listDraftReports(committee_id, callback) {
    this.get('service/campaign-finance/committee/' + committee_id + '/draft-reports', callback);
  }

  submittedReports(committee_id, callback) {
    this.get('service/campaign-finance/committee/' + committee_id + '/submitted-reports', callback);
  }

  deleteVendorDraftReport(callback, data){
      this.post(`service/campaign-finance/draft-report/delete/${data.draft_id}`,
          result => {
              if (result.success) {
                  callback(result.data);
              }
              else {
                  callback(result.errors);
              }
          },
      );
  }

  deleteFinanceReport(callback, data) {
    this.post('service/admin-process/delete-finance-report',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          callback(result.errors);
        }
      },
      {report_id: data.report_id});
  }

  verifyRegistration(registration, callback) {
    let data = {
      registration_id: registration.registration_id,
      admin_data: registration.admin_data
    };
    this.post(
      'service/admin-process/verify-committee',

      result => {
        if (result.success) {
          this.registration(data.registration_id, callback)
        } else {
          alert(result.errors);
        }
      },
      data
    );
  }

  /**
   *
   * @param context
   * Context object either contains:
   *   registration_id => integer
   *   and either
   *   person_id => integer
   *   or
   *   committee_id => integer
   */
  async getVerificationOptions(context) {
    const res = await axios({
      method: 'POST',
      data: JSON.stringify(context),
      url: this.service_host + 'service/admin-process/verification-options',
      withCredentials: true
    })
    return res.data
  }


  revertRegistration(data, callback) {
    this.post(
      'service/admin-process/revert-registration',

      result => {
        if (!result.success) {
          alert(result.errors);
        } else {
          callback();
        }
      },
      data
    );
  }

  mergeCommittees(registration_id, sourceCommitteeId, targetCommitteeId, callback) {
    this.post('service/admin-process/merge-committees',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          alert(result.errors);
        }
      },
      {registration_id: registration_id, sourceCommitteeId: sourceCommitteeId, targetCommitteeId: targetCommitteeId}
    );
  }

  revokeUserAccess(committee_id, user, callback) {
    this.post('service/admin-process/revoke-user',
      result => {
        if (result.success) {
          callback(true);
        } else {
          callback(result.errors);
        }
      },
      {committee_id: committee_id, user: user}
    );
  }

  mailMerge(data, options, callback) {
    let postData = {};
    postData.data = data;
    postData.options = options;
    this.post(
      'service/admin-process/mail-merge',
      result => {
        if (!result.success) {
          alert(result.errors);
        } else {
          callback(result.data)
        }
      },
      postData
    );
  }

  updateMiniFullPermission(callback, data) {
    this.post('service/admin-process/mini-full-permission',
      result => {
        if (result.success) {
          callback(result.data);
        } else {
          callback(result.errors);
        }
      },
      {committee_id: data.committee_id, mini_full_permission: data.boolean});
  }

  getCFReportData(callback, {repno}) {
    this.get(
      `public/service/reports/${repno}`,
      (data) => {
        if (data) {
          callback(data)
        } else {
          alert(data.error)
        }
      }
    )
  }

  async getCFReportUserData(callback, repno) {
    this.get(
      `public/service/reports/${repno}/userdata`,
      (data) => {
        if (data) {
          callback(data)
        } else {
          alert(data.error)
        }
      }
    )
  }

  async getCandidacies(year, search) {
    const url = this.service_host + 'public/service/candidacy';
    const params = {year, search};
    const {data} = await axios({
      url,
      method: 'GET',
      params,
      withCredentials: true,
      headers: {
        'content-type': 'application/json'
      }
    });
    return data;
  }

}
const dataService = new DataService();
export {dataService};
