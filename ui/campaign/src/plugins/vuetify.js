import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#006800',
        secondary: '#b0bec5',
        accent: '#FFC107',
        error: '#b71c1c',
        info: '#066bbe',
        success: '#006800',
        warning: '#FFC107'
      },
    },
  },
  icons: {
    iconfont: 'md',
  },
});
