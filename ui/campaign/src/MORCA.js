/* eslint-disable */
const MORCA = {

    //Variables
    mock: true,
    version: "1.500",
    title: "Test Title",
    setTitleCallback: null,
    navigateToCallback: null,
    popUpCallback: null,
    setSyncActiveCallback: null,
    orcaApiPutCallback: null,
    orcaPushChangesCallback: null,
    orcaSetupNewCampaignCallback: null,
    orcaSetupExistingCampaignCallback: null,
    orcaSaveValidationsCallback: null,
    showErrorCallback: null,
    handleOpenCampaignCallback: null,
    setDerbyUpdatedCallback: null,
    doSubmissionSyncCallback: null,

    data() {
        return {
            orcaEmbedded: false,
            title: "",
            version: 0,
            campaign: {},
            syncQueue: [],
            isSyncing: false,
        }
    },

    executeMenu(menuItem) {

    },
    closeFundDialog(result) {

    },
    navigateToVueApp() {

    },
    logout() {

    },
    createCampaign(campaignData) {

    },
    restoreCampaignFromRDS(campaign) {

    },
    restoreCampaign() {

    },
    committeeProperties() {

    },
    openCampaign() {

    },
    backupCampaign() {

    },
    importExportCampaign() {

    },
    setupWizard() {

    },
    loadFaq() {

    },
    checkForUpdates() {

    },
    closeCampaign() {

    },
    deleteCampaign() {

    },
    openMonetaryContributions() {

    },
    openGroupContributions() {

    },
    openInkindContributions() {

    },
    openCandidatePersonalFunds() {

    },
    openAnonymousContributions() {

    },
    cashLoan() {

    },
    inkindLoan() {

    },
    pledges() {

    },
    cashFundraiser() {

    },
    lowcostFundraiser() {

    },
    auction() {

    },
    saveAuctionItem() {

    },
    vendorRefund() {

    },
    otherReceipt() {

    },
    bankInterest() {

    },
    monetaryExpenditure() {

    },
    refundContribution() {

    },
    vendorDebt() {

    },
    bookKeepingAdjustments() {

    },
    mathCorrection() {

    },
    accountAdjustment() {

    },
    deposit() {

    },
    submitReport() {

    },
    reportLMC() {

    },
    individualContacts() {

    },
    couplesContacts() {

    },
    othersContacts() {

    },
    groupsContacts() {

    },
    chartOfAccounts() {

    },
    webConnectionError() {

    },
    getContacts() {

    },
    getContact() {

    },
    bulkSave(data) {

    },
    finishOpen(data) {

    },
    disableSync() {

    },
    enableSync() {

    },
    pushCampaign() {

    },
    processChanges(data) {

    },
    async finishSetupExistingCampaign(data) {

    },
    setDerbyProperties(properties) {

    },
    setDatabaseProperty(key, value) {

    },
    saveExpenditure(expenditure) {

    },
    deleteExpenditure(expenditure) {

    },
    executeUpdates(json) {

    },
    updateCarryForward(carryForward) {

    },
    mergeContacts(json) {

    },
    deleteContact(contact) {

    },
    saveContact(contactId) {

    },
    saveContribution(contribution) {

    },
    deleteContribution(contribution) {

    },
    deleteReceipt(receipt) {

    },
    saveLowCostFundraiser(fundraiser) {

    },
    deleteReceiptContactAccount(receipt) {

    },
    saveMiscOther(misc) {

    },
    saveMiscBankInterest(misc) {

    },
    saveAccountTransfer(transfer) {

    },
    deleteReceiptContact(receipt) {

    },
    saveLoan(loan) {

    },
    saveDepositEvent(deposit) {

    },
    deleteDepositEvent(data) {

    },
    setLoginStatus(loginStatus) {

    },
    doJSONSync(json) {

    },
    saveCorrection(correction) {

    },
    saveLoanTransaction(transaction) {

    },
    saveFundraiserEvent(fundraiser) {

    },
    deleteLoan(loan) {

    },
    saveFundraiserContribution(contribution) {

    },
    saveVendorRefund(vendor_refund) {

    },
    saveAuction(auction) {

    },
    saveCreditCard(credit_card) {

    },
    saveCreditCardPayment(payment) {

    },
    savePledge(pledge) {

    },
    savePledgeTransaction(item) {

    },
    saveReceipt(receipt) {

    },
    saveAccount(account) {

    },
    openApollo() {
        try {
            window.orcaJava.openApollo();
        } catch (e) {
            alert('Unable to open Apollo in browser. Please contact Support.');
            console.log("Error loading Apollo", e);
        }
    }
};

export default MORCA;