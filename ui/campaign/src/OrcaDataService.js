import axios from 'axios';
import {app} from "@/main";
import ORCA from "./ORCABridge";

class OrcaDataService {

  pendingCount = 0;

  // Used to detect conflicts in fund data.
  saveCount = 0;

  /**
   * Constructor
   *
   * Sets an appropriate url handling our development environment.
   */
  constructor() {
    this.service_host = 'https://apollod8.lndo.site/service/orca/';
    this.pendingRequests = 0;
    this.errorReason = {};
    this.headers = {
      'Content-Type': 'application/json;charset=UTF-8'
    }

    if (location.host.search('localhost') >= 0) {
      // Assume ports that start with 818 (e.g. 8180-8189 will connect to demo environment)
      // so that devs with no lando work.
      if (location.host.search('818') > 0) {
        this.service_host = 'https://demo-apollo-d8.pantheonsite.io/service/orca/';
      } else {
        this.service_host = 'https://apollod8.lndo.site/service/orca/';
      }
    } else {
      this.service_host = '/service/orca/';
    }
  }

  markPending(isPending, message) {
    if (!isPending) {
      this.pendingCount--;
      if (this.pendingCount <= 0) {
        app.$nextTick(() => {
          app.$container.wire.$emit('OrcaService.pending', isPending, message);
        });
      }
    } else {
      this.pendingCount++;
      app.$container.wire.$emit('OrcaService.pending', isPending, message);
    }
  }

  async getGeneralData(query = '', parameters = null) {
    let url = this.service_host + 'general/' + query;
    this.markPending(true);
    let config = {withCredentials: true}
    if (parameters) {
      config.params = parameters;
    }
    let result = await axios.get(url, config).catch(e => this.handleError(e));
    this.markPending(false);
    if (result.data) {
      return result.data;
    } else {
      return null;
    }
  }

  async getFundData(fund_id, query = '', parameters = null, blocking = true) {
    let url = this.service_host + 'fund/' + fund_id + '/' + query;
    if (blocking) {
      this.markPending(true);
    }
    let config = {withCredentials: true}
    if (parameters) {
      config.params = parameters;
    }
    let result = await axios.get(url, config).catch(error => {
      if (!error.response || (error.response && error.response.status !== 403)) {
        this.handleError(error);
      } else {
        this.markPending(false);
      }
      return Promise.reject(error);
    });
    if (query === 'fund' && result.data) {
      this.saveCount = result.data.fund.save_count ?? 0;
    }
    if (blocking) {
      this.markPending(false);
    }
    if (result.data) {
      return result.data;
    } else {
      return null;
    }
  }

  async getCommitteeData(committee_id, query = '', parameters = null) {
    let url = this.service_host + 'committee/' + committee_id + '/' + query;
    this.markPending(true);
    let config = {withCredentials: true}
    if (parameters) {
      config.params = parameters;
    }
    let result = await axios.get(url, config).catch(e => this.handleError(e));
    this.markPending(false);
    if (result.data) {
      return result.data;
    } else {
      return null;
    }
  }

  /**
   * Promised base
   * @param committee_id
   * @param action
   * @param data
   * @returns {Promise<void>}
   */
  async committeePost(committee_id, action, data ) {
    const url = this.service_host + 'committee/' + committee_id + '/' + action;
    this.markPending(true);
    const result =  await axios.post(url,  JSON.stringify(data), {headers: this.headers, withCredentials: true}).catch(() => {
      this.markPending(false);
      alert ('Error posting to ' + action);
    });
    this.markPending(false);
    return result?.data;
  }

  async getFundObject(fund_id, object, id) {
    let url = this.service_host + 'fund/' + fund_id + '/' + object + "/" + id;
    this.markPending(true);
    let result = await axios.get(url, {withCredentials: true})
      .catch(() => {
        alert("Unable to load " + object);
      });
    this.markPending(false);
    if (result && result.data && result.data[object]) {
      return result.data[object];
    } else {
      if (result.data.error) {
        alert(result.data.error);
      }
      return null;
    }
  }

  /**
   * Returns the raw response from a get fund object call.
   *
   * Calling code is responsible for determining what do do in an error case.
   * @param fund_id
   * @param object
   * @param id
   * @returns {Promise<null|any>}
   */
  async getAttachedPageObject(fund_id, object, id) {
    let url = this.service_host + 'fund/' + fund_id + '/' + object + '/' + id
    this.markPending(true);
    let result = await axios.get(url, {withCredentials: true})
      .catch(() => {
        alert("Unable to load attached text page");
      });
    this.markPending(false);
    if (result && result.data) {
      return result.data;
    } else {
      if (result.data.error) {
        alert(result.data.error);
      }
      return null;
    }
  }

  /**
   * Returns the raw response from a get fund object call.
   *
   * Calling code is responsible for determining what do do in an error case.
   * @param fund_id
   * @param object
   * @param id
   * @returns {Promise<null|any>}
   */
  async getFundObjectRaw(fund_id, object, id) {
    let url = this.service_host + 'fund/' + fund_id + '/' + object + '/' + id;
    this.markPending(true);
    let result = await axios.get(url, {withCredentials: true})
      .catch(() => {
        alert('Unable to load ' + object);
      });
    this.markPending(false);
    return result?.data;
  }

  handleDataConflict() {
    if (confirm("Campaign data was modified since this campaign was opened. Reload?")) {
      window.location.reload();
    }
  }

  /**
   * Emit messages for ORCA to the application to respond to change events.
   *
   * Based on the object name we determine which events to fire to notify the applications
   * We are careful not to send these in ORCA version < 1.500 so that we don't duplicate events
   * that would be fired on synchronization.
   *
   * @param object
   *   The name of the object being posted
   */
  emitChanges(object) {
    // Only send events after we have let go of the ORCA product
    if (ORCA.version >= 1.500) {
      if ( object.includes('contact')
        || object === 'monetary-contributions') {
        ORCA.$emit('contacts.change')
      }
      // Notify about changed receipt records
      if (
        object.includes('contribution')
        || object.includes('auction')
        || object.includes('correction')
        || object.includes('expenditure')
        || object.includes('expense')
        || object.includes('fundraiser')
        || object.includes('loan')
        || object.includes('misc')
        || object.includes('pledge')
        || object.includes('transaction')
        || object.includes('vendor')
      ) {
        ORCA.$emit('receipts.change')
      }
      // Notify about new or removed accounts
      if ( object.includes('account')
          || object.includes('fundraiser')
      ) {
        ORCA.$emit('accounts.change')
      }
      if ( object.includes('deposit')) {
        ORCA.$emit('depositEvents.change')
      }
    }

  }

  /**
   * Post an event to an object identified.
   *
   * Caller is responsible for analyzing the payload for success.
   * @param fund_id The fund ID of the ORCA fund for this object
   * @param object The type of object to which to post the dta
   * @param id The id of the object you need to post the data to.
   * @param data The object payload to post.  Json data will be stringified.
   * @returns {Promise<T|undefined>}
   */
  async postToFundObject(fund_id, object, id, data) {
    let url = this.service_host + 'fund/' + fund_id + '/' + object + '/' + id;
    this.markPending(true);
    data.saveCount = this.saveCount;
    let result = await axios.post(url, JSON.stringify(data), {headers: this.headers, withCredentials: true})
      .catch((e) => {
        if (e.response && (e.response.status === 409)) {
          this.handleDataConflict();
        } else {
          alert('Could not save ' + object);
        }
      });
    if (result.data.saveCount) {
      this.saveCount = result.data.saveCount;
      this.emitChanges(object);
    }
    this.markPending(false);
    return result?.data;
  }


  async postFundObject(fund_id, object, data, return_raw = false) {
    const depositTransactions = [
      'monetary-contribution',
      'monetary-contributions',
      'anonymous-contribution',
      'depositevent',
      'low-cost-fundraiser',
      'misc-other',
      'monetary-loan'
    ];
    // make sure we have data.
    if (!data) data = {};
    let url = this.service_host + 'fund/' + fund_id + '/' + object;
    this.markPending(true);
    data.saveCount = this.saveCount;
    let result = await axios.post(url, JSON.stringify(data), {
      headers: this.headers,
      withCredentials: true
    }).catch((e) => {
      this.markPending(false);
      if (e.response && (e.response.status === 409)) {
        this.handleDataConflict();
      }
      else {
        alert('Error saving ' + object);
        console.log(e);
      }
    })
    this.markPending(false);
    if (result && result.data && result.data.saveCount) {
      this.saveCount = result.data.saveCount;
    }
    if (result && result.data && result.data.success) {
      if (depositTransactions.includes(object)) {
        ORCA.$emit('deposited.change');
      }
      this.emitChanges(object)
      if (return_raw) {
        return result.data;
      } else {
        return result.data[object];
      }
    } else {
      if (result.data.error) {
        alert(result.data.error);
      }
      return null;
    }
  }

  async putFundObject(fund_id, object, id, data, return_raw = false) {
    let url = this.service_host + 'fund/' + fund_id + '/' + object + "/" + id;
    this.markPending(true);
    data.saveCount = this.saveCount;
    let result = await axios.put(url, JSON.stringify(data), {
      headers: this.headers,
      withCredentials: true
    }).catch((e) => {
      this.markPending(false);
      if (e.response && (e.response.status === 409)) {
        this.handleDataConflict();
      }
      else {
        alert('Error saving ' + object);
        console.log(e);
      }
    });
    this.markPending(false);
    if (result && result.data && data.saveCount) {
      this.saveCount = result.data.saveCount;
      this.emitChanges(object);
    }
    if (result && result.data) {
      if (return_raw) {
        return result.data;
      } else {
        return result.data[object];
      }
    } else {
      return null;
    }
  }

  async deleteFundObject(fund_id, object, id) {
    let url = this.service_host + 'fund/' + fund_id + '/' + object + "/" + id;
    this.markPending(true);
    let result = await axios.delete(url, {withCredentials: true}).catch((e) => {
      this.markPending(false);
      alert('Error deleting ' + object);
      console.log(e);
    });
    this.emitChanges(object);
    this.markPending(false);
    if (result && result.data && result.data[object]) {
      return result.data[object]
    } else if (result && result.data) {
      return {'success': result.data.success};
    } else {
      return null;
    }
  }

  async post(object, post_data) {
    let url = this.service_host + object;
    this.markPending(true)
    let result = await axios.post(url, JSON.stringify(post_data), {headers: this.headers, withCredentials: true});
    this.markPending(false);
    return result;
  }

  async put(object, id, post_data) {
    let url = this.service_host + object + '/' + id;
    return await axios.put(url, JSON.stringify(post_data), {headers: this.headers, withCredentials: true})
  }

  async delete(fund_id, object, id) {
    let url = this.service_host + 'fund/' + fund_id + '/' + object + '/' + id;
    return await axios.delete(url, {withCredentials: true});
  }

  async pushChanges(fund_id, json, result, version) {
    if (!fund_id) {
      console.log("No fund to sync to ");
      return new Promise(resolve => {
        resolve({success: false})
      });
    }
    let url = this.service_host + 'fund/' + fund_id + '/push-changes/' + version;
    let postResult = await axios.post(url, json, {headers: this.headers, withCredentials: true}).catch((e) => {
      console.log(e);
      result.success = false;
      result.errors = e;
    });
    if (postResult && postResult.data && postResult.data.success) {
      if (postResult.data.last_sync) {
        result.last_sync = postResult.data.last_sync > result.last_sync ? postResult.data.last_sync : result.last_sync;
      }
      if (postResult.data.errors) {
        result.errors.push(postResult.data.errors);
      }
    } else {
      result.success = false;
    }
  }

  async saveValidations(fund_id, json) {
    if (!fund_id) {
      console.log("No Fund id to save validations");
      return new Promise(resolve => {
        resolve({success: false})
      });
    }
    let url = this.service_host + 'fund/' + fund_id + '/save-validations';
    this.markPending(true);
    let result = await axios.post(url, json, {headers: this.headers, withCredentials: true}).catch(error => {
      this.markPending(false);
      console.log(error);
      return Promise.reject(error);
    });
    this.markPending(false);
    return result;
  }

  async setupExistingCampaign(json) {
    let url = this.service_host + 'general/fund';
    this.markPending(true);
    let result = await axios.post(url, json, {headers: this.headers, withCredentials: true});
    this.markPending(false);
    return new Promise(resolve => {
      resolve(result)
    });
  }

  async requestFundAccess(json) {
    let url = this.service_host + 'general/request-fund-access';
    this.markPending(true, "Checking fund access");
    let result = await axios.post(url, json, {headers: this.headers, withCredentials: true})
      .catch(error => {
        if (error && error.response && error.response.status !== 403) {
          this.handleError();
        } else {
          this.markPending(false);
          return false;
        }
      });
    this.markPending(false);
    return result && result.data && result.data.success;
  }

  async deleteCampaign(fund_id) {
    let url = this.service_host + 'fund/' + fund_id + '/delete-campaign';
    this.markPending(true);
    let result = await axios.delete(url, {withCredentials: true, params: {fund_id}});
    this.markPending(false);
    return result;
  }


  /**
   * Handle errors in all axios calls.
   * @param r
   */
  handleError(r) {
    this.errorReason = r;
    alert(r.message);
    this.markPending(false);
  }

  async getContributions(fund_id, search) {
    let url = this.service_host + 'fund/' + fund_id + '/contributions';
    let params = {fund_id, search};
    let {data} = await axios({
      url,
      method: 'GET',
      params,
      withCredentials: true,
      headers: {
        'content-type': 'application/json'
      }
    });
    return data;
  }

  async getAwsGatewayByFundId(fund_id) {

    const data = await this.getFundData(fund_id, 'backup-api', null, false);
    if (data && data.success) {
      this.awsGateway = data.api;
    }
    else {
      alert('Unable to contact backup request service. Contact support.');
    }
  }

  async getAwsGatewayByCommitteeId(committee_id){
    const data = await this.getCommitteeData(committee_id, 'get-committee-api', null)
    if(data && data.success){
      this.awsGateway = data.api;
    }else{
      alert('Unable to grant pdc access request service. Contact support.');
    }
  }


  async awsGatewayPost(url, request) {
    if (this.awsGateway.url) {
      return await axios.post(this.awsGateway.url + '/' + url, request, {
        headers: {
          "Authorization": this.awsGateway.token,
        }
      });
    }
    else return {}
  }



  async requestBackup(fund_id, email) {
    await this.getAwsGatewayByFundId(fund_id);
    let data = {};
    if (this.awsGateway) {
      this.markPending(true, 'Requesting Backup')
      try {
        const response = await this.awsGatewayPost('request-backup', {fund_id, email} );
        data = response.data;
      }
      catch (e) {
        data = {success: false, message: "Unable to request backup."}
        console.log(e);
      }
      this.markPending(false);
      return data;
    }
  }

  async pdcAccessPost( committee_id, action, payload = {}){
    payload.committee_id = committee_id;
    await this.getAwsGatewayByCommitteeId(committee_id);
    let data = {};
    if(this.awsGateway){
      this.markPending(true, `${action} access`)
      try{
        const response = await this.awsGatewayPost('pdc-access/' + action, payload)
        data = response.data;
      }catch(e){
        data = {success: false, message: "Unable to modify pdc access."}
      }
    }
    this.markPending(false);
    return data;
  }

  async archiveRestoreUpload(url, form_data){
    let result = {};
    try{
      this.markPending(true, 'uploading archive file')
      result = await axios.post(url, form_data);
      this.markPending(false);
    }catch(e){
      result = {success: false, message: "Unable to restore archive"}
      this.markPending(false);
    }
    return result;
  }
}

// Establish a new instance of OrcaDataService
const orcaDataService = new OrcaDataService();

// Export a named instance variable that can be used by other code.
export {orcaDataService};
