export default {
  requireString(v) {
    return !(!v || !v.trim()) || 'Required';
  },
  required(v) {
    return !!v || 'Required';
  },
  max255(v) {
    return !v || v.length <= 255 || 'Too long';
  },
  max1000(v) {
    return !v || v.length <= 1000 || 'Too long';
  },
  positiveAmount(v) {
    // determine the maximum possible number for a numeric(16,2)
    const maxNumber = 9999999999999999;

    // when v is not a valid decimal number
    if (isNaN(v)) return 'Invalid number'

    // convert the number to a float
    const floatNum = parseFloat(v)

    if (floatNum > maxNumber) return 'Invalid amount'
    if (floatNum.toString().split(".")[1]?.length > 2) return 'Dollar amounts cannot contain fractional cents'
    if (v === '0.0' || v === '0') return 'Amount must be greater than 0'

    return !v || parseFloat(v) >= 0 || 'Negative amounts are not allowed'
  },
  validState(v) {
    return (v || '').length <= 2 || '2 characters only'
  },
  states: ["AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC",
    "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA",
    "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE",
    "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC",
    "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"
  ]
}
