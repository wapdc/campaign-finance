import similarity from 'similarity';

/*
 * This code contains the worker that is loaded in a separate thread
 * for performing long running tasks.
 */


const sendMessage = function (event, message) {
  self.postMessage({event, message});
}

/**
 * This defines the entry points that are being worked on.
 * @type {{wait(*): Promise<unknown>}}
 */
const tasks = {
  async wait(seconds) {
    return new Promise(resolve => {
      setTimeout(() => resolve(seconds + ' seconds is a long time.'), seconds * 1000);
    })

  },
  analyzeDuplicates(data) {
    console.log('analyzeDuplicates', data)
    let existingContacts = data.existingContacts;
    let newContacts = data.newContacts;
    let contactProperty = data.contactProperty;

    existingContacts.map(c => {
      if (c.name) {
        c.n_address = (c.street + '').toLowerCase().replaceAll(new RegExp("[/,.'\"]", 'g'), "").replace(/\b(?:p o box|pobox)\b/gi, 'po box')
        c.name = c.name.replace('  ', ' ') //remove double spacing
        if (c.suffix) {
          c.name += ' ' + c.suffix
        }
      }
    })
    newContacts.map(c => {
      if (c.name) {
        c.n_address = (c.street + '').toLowerCase().replaceAll(new RegExp("[/,.'\"]", 'g'), "").replace(/\b(?:p o box|pobox)\b/gi, 'po box')
        c.name = c.name.replace('  ', ' ') //remove double spacing
        if (c.suffix) {
          c.name += ' ' + c.suffix
        }
      }
    })

    if (existingContacts && newContacts && newContacts.length > 0) {
      let arraySize = newContacts.length;
      newContacts.map((c, index) => {
        if (index % 100 === 0) {
          sendMessage('progress-updated', index / arraySize * 100);
        }
        if (!c || !c.name) return;
        let matchingContacts = existingContacts.filter((e, index, existingContacts) => {
          let nameSimilarityScore = similarity(e.name, c.name);
          let addressSimilarityScore;

          addressSimilarityScore = nameSimilarityScore > 0.5 && (c.city ?? '').toLowerCase() === (e.city ?? '').toLowerCase() && (c.state ?? '').toLowerCase() === (e.state ?? '').toLowerCase() ? similarity(e.n_address, c.n_address) : 0.0;
          //name > 0.9 || //name > 0.5 && addresses > 0.8
          if (nameSimilarityScore > 0.9 || (nameSimilarityScore > 0.5 && addressSimilarityScore > 0.8)) {
            existingContacts[index].n_similarity = nameSimilarityScore;
            existingContacts[index].a_similarity = addressSimilarityScore;
            return true;
          } else {
            return false;
          }
        })
        if (matchingContacts && matchingContacts.length > 0) {
          matchingContacts.sort((a, b) => {
            return (b.n_similarity - a.n_similarity)
              || (b.a_similarity - a.a_similarity)
              || (b.trankeygen_id - a.trankeygen_id)
          });
          if (matchingContacts[0].n_similarity === 1 && matchingContacts[0].a_similarity === 1) {
            c[contactProperty] = matchingContacts[0]?.trankeygen_id;
          } else {
            c.duplicate_id = matchingContacts[0]?.trankeygen_id;
          }
          c.duplicate_name = matchingContacts[0]?.name;
          c.similarity = similarity(matchingContacts[0]?.name, c.name);
          c.matches = matchingContacts;
        }
      });
    }
    console.log("returning ", newContacts);
    return newContacts;
  }
}

self.addEventListener('message', async e => {
  if (e.data) {
    console.log('worker got ', e.data);
    if (e.data.task && tasks[e.data.task]) {
      sendMessage('task-started', e.data.task);
      let results = await tasks[e.data.task](e.data.data);
      sendMessage('task-completed', results)
    } else {
      console.log('Task not found');
    }
  }
});