import Vue from 'vue';
import moment from 'moment';
import similarity from "similarity";

const {format} = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  maximumFractionDigits: 2
});

export default {
  filters: {
    describeContactType(type){
      return type === 'IND' ? 'Individual Contact' : type === 'GRP' ? 'Group Contact' : type === 'CPL' ? 'Couple Contact'  : 'Other Contact'
    },
    isNegative: function (balance, amount) {
      const available_bal = (typeof balance === 'number' && Math.sign(balance) === -1) ? balance : parseFloat(balance);
      const amt_entered = (typeof amount === 'number' && Math.sign(amount) === -1) ? amount : parseFloat(amount);
      let total = available_bal - amt_entered;

      return (typeof total === 'number' && Math.sign(total) === -1);
    },
    proposalFilter: function (value) {
      if (value) {
        return '#' + value;
      }
      return value;
     },
    // Yes or NO from boolean
    yesNo: value => {
      if (value) {
        return 'Yes'
      } else {
        return 'No'
      }
    },

    /**
     * Returns the Current date + 1 month
     * formatted as a ISO 8601 String
     * @returns {string}
     */
    currentDatePlusOneMonth: () =>  {
        return moment(new Date()).add(1, 'month').format('YYYY-MM-DD');
    },

    currentDatePlusTwoWeeks: () => {
      return moment(new Date()).add(2, 'weeks').format('YYYY-MM-DD');
    },

    today: () => {
      return moment(new Date()).format('YYYY-MM-DD');
    },
    formatDate: value => {
      if (value) {
        return moment(value.toString()).format('MM/DD/YYYY');
      }
      return '';
    },

    formatDateTime: value => {
      if (value) {
        return moment(value).format('MM/DD/YYYY h:mm a');
      }
      return '';
    },
    displayNoReport(value) {
      if (typeof value !== 'undefined' && value) {
        return value;
      } else {
        return 'None reported';
      }
    },
    electionFilter(code) {
      code = typeof code === 'number' ? code.toString() : code.toUpperCase();
      switch (code) {
        case 'P':
        case '0'  :
          return 'Primary'
        case 'G':
        case '1':
          return 'General'
        case '-1': // ORCA elekey value
          return ''
        case 'N':
          return 'Full election cycle'
        default:
          return code;
      }
    },
    formatCityStateZip(city, state, postcode){
      let address = "";
      if(city && city.trim()!=='') {
        address += city;
      }
      if(state && state.trim() !== ''){
        address += (address !=="") ? ', ' + state: state;
      }
      if(postcode && postcode.trim()!=='') {
        address += (address !=='') ? ' ' + postcode: postcode;
      }
      return address;
    },
    addParenthesis(val){
      const temp = val.replace('$','')
      if(temp < 0){
        return  "$(" + temp + ")"
      } else {
        return val;
      }
    },
    formatMoney(amount, defaultValue = '$0.00') {
      let formattedMoney = '';
      try {
        if (typeof amount !== undefined && amount && !isNaN(amount)) {
          formattedMoney = this.addParenthesis(format(amount));
        } else {
          formattedMoney = defaultValue;
        }
      } catch (e) {
        console.log(e)
      }
      return formattedMoney;
    },
    defaultText: (value, text) => {
      if (!value) {
        return text;
      }
      return value;
    },

    // Title Case a field or code.
    titleCase: value => {
      if (value && (value === value.toUpperCase() || value === value.toLowerCase())) {
        let str = value.toLowerCase().split(' ');
        for (let i = 0; i < str.length; i++) {
          str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
        }
        return str.join(' ');
      }
      return value;
    },

    // Create a (value) but only if the value exists.
    parenthetical: value => {
      if (value) {
        return '(' + value + ')';
      }
      return '';
    },

    // Display a label only if true.
    ifTrue: (value, label) => {
      if (value) {
        return label
      }
      return '';
    },

    // Display value only if equals comparator.
    ifEquals: (value, comparator) => {
      if (value === comparator) {
        return value;
      }
      return '';
    },

    // Display value only if not equal to comparator.
    ifNotEquals: (value, comparator) => {
      if (value !== comparator) {
        return value;
      }
      return '';
    },

    primaryElectionResult: (value) => {
      if (value === 'W') {
        return 'Qualified for general';
      }
      if (value === 'L') {
        return 'Lost in primary';
      }
      if (value === 'C') {
        return 'Judge certified before primary';
      }
      if (value === 'N') {
        return 'Not in primary';
      }
      if (value === 'U') {
        return 'Unopposed in primary';
      }
      if (value === '' || value === null) {
        return 'None reported';
      } else {
        return value;
      }
    },

    generalElectionResult: (value) => {
      if (value === 'W') {
        return 'Won in general';
      }
      if (value === 'L') {
        return 'Lost in general';
      }
      if (value === 'C') {
        return 'Judge certified before general';
      }
      if (value === 'U') {
        return 'Unopposed in general';
      }
      if (value === '' || value === null) {
        return 'None reported';
      } else {
        return value;
      }
    },

    // Suffix the value with a label if it is present.
    suffix: (value, label) => {
      if (value) {
        return value + label;
      }
      return '';
    },


    // Prefix the value with a label if it is present.
    prefix: (value, label) => {
      if (value) {
        return label + value;
      }
      return '';
    },

    email: value => {
      if (value) {
        let regex = /[,; ]+/g;
        value = value.replace(regex, ', ');
        return value.toLowerCase();
      }
      return '';
    },

    // Return the proper values for ministerial and treasurer in the proper formatting
    formatTreasurer: (treasurer, ministerial, title) => {
      let compareTitle = title ? title.toLowerCase() : '';
      if (compareTitle !== "treasurer" && treasurer && ministerial) {
        return ' (ministerial treasurer)';
      } else if (compareTitle === "treasurer" && treasurer && ministerial) {
        return ' (ministerial)';
      } else if (compareTitle !== "treasurer" && treasurer && !ministerial) {
        return ' (treasurer)';
      } else {
        return '';
      }
    },

    //Returns election title if there is an election otherwise returns continuing committee
    continuingTypeFormat: (election) => {
      if (election) {
        return election.title
      }
      return "Continuing Committee"
    },

    validation(value, validation, key = "value", label = "text") {
      if (!value) {
        return "";
      }
      if (!Array.isArray(value)) {
        value = [value];
      }
      let displayValues = validation.map((validationEntry, index, theArray) => {
        if (value.includes(validationEntry[key])) {
          return theArray[index][label];
        }
      });

      return displayValues.filter(Boolean).join(", ");
    },
    similarityFilter(item, queryText, itemText){
      if(itemText.name) {
        itemText = itemText.name;
      }

      let nameSimilarityScore = 0.0;
      if (queryText.length > 0){
        if(queryText.length > 7){
          nameSimilarityScore = similarity(itemText.toLocaleLowerCase(), queryText.toLocaleLowerCase())
        }
        if(itemText.toLocaleLowerCase().indexOf(queryText.toLocaleLowerCase()) > -1 || nameSimilarityScore > 0.5){
          return true;
        }
      }
    },
  },
  defineGlobalFilters() {
    for (let f in this.filters) {
      Vue.filter(f, this.filters[f]);
    }
  },

}
