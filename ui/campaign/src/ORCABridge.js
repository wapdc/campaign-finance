import Vue from 'vue';
import {orcaDataService} from './OrcaDataService';
import {useOrcaFund} from "@/orca/fund/Fund";
import MORCA from "@/MORCA";

/* eslint-disable */
const ORCA = new Vue({
  data() {
    return {
      orcaEmbedded: false,
      title: "",
      version: 0,
      campaign: {},
      syncQueue: [],
      isSyncing: false,
    }
  },
  methods: {
    setOrcaVersion(version){
      if (!this.orcaEmbedded){
        this.version = version;
        ORCA.$emit('init', this.version);
      }
    },
    installCallback(name, callback) {
      try {
        window.orcaJava[name] = callback;
      } catch (e) {
        console.log("Could not install callback: " + name);
      }
    },
    getJavaProperty(name) {
      try {
        return window.orcaJava[name];
      } catch {
        return null;
      }
    },
    init() {
      if (window.orcaJava) {
        this.version = this.getJavaProperty("version");
        try {
          this.orcaEmbedded = !window.orcaJava.mock
        } catch(e) {
          this.orcaEmbedded = true;
        }
        this.installCallback("handleOpenCampaignCallback", ORCA.handleOpenCampaign);
        this.installCallback("setTitleCallback", ORCA.setTitle);
        this.installCallback("navigateToCallback", ORCA.navigateTo);
        this.installCallback("setSyncActiveCallback", ORCA.setSyncActive);
        this.installCallback("orcaApiPostCallback", ORCA.orcaApiPost);
        this.installCallback("orcaApiPutCallback", ORCA.orcaApiPut);
        this.installCallback("orcaPushChangesCallback", ORCA.pushChanges);
        this.installCallback("orcaSetupNewCampaignCallback", ORCA.setupNewCampaign);
        this.installCallback("orcaSetupExistingCampaignCallback", ORCA.setupExistingCampaign);
        this.installCallback("orcaSaveValidationsCallback", ORCA.saveValidations);
        this.installCallback("showErrorCallback", window.alert);
        this.installCallback("setDerbyUpdatedCallback", ORCA.setDerbyUpdated);
        this.installCallback("doSubmissionSyncCallback", ORCA.doSubmissionSync);
        this.installCallback("popUpCallback", ORCA.popUp);
        ORCA.setLoginStatus(true);
        ORCA.setTitle(window.orcaJava.title);
        ORCA.$emit('init', this.version);
      }else{
          window.orcaJava = MORCA
          this.orcaEmbedded = false;
        }
    },

    // @TODO: add orca api calls so we can call orca functionality directly.
    async orcaApiPost(object, json) {
      json = JSON.parse(json);
      // Use data service to call the orca api passing the correct object and json data
      // OrcaAPI should always await a response
      return await orcaDataService.post(object, json);
    },

    async orcaApiPut(object, id, json) {
      json = JSON.parse(json);
      // Use data service to call the orca api passing the correct object and json data
      // OrcaAPI should always await a response
      return await orcaDataService.put(object, id, json);
    },

    setTitle(title) {
      this.title = title;
      ORCA.$emit('setTitle', title);
    },
    getTitle() {
      return this.title;
    },
    setSyncActive(active) {
      this.isSyncing = active;
      ORCA.$emit('setSyncActive', active);
    },
    navigateTo(location) {
      ORCA.$emit('navigateTo', location);
    },
    popUp(url) {
      console.log('url', url.toString());
      window.open(url, "_blank", "resizable=yes, top=50, left=50, width=1000, height=800");
    },

    closeFundDialog(result) {
      try {
        result = parseInt(result);
      } catch (e) {
        result = -1;
      }
      if (this.orcaFunctionExists('closeFundDialog')) {
        window.orcaJava.closeFundDialog(result);
      }
    },

    orcaFunctionExists(functionName) {
      try {
        return !!(window.orcaJava && window.orcaJava[functionName]);
      } catch (e) {
        console.log("Error detecting function", e);
        return false;
      }
    },

    executeMenu(menuItem) {
      let message = "";
      switch (menuItem) {
        case 'vendorDebt':
        case 'creditCardDebt':
        case 'bookKeepingAdjustments':
        case 'mathCorrection':
        case 'accountAdjustment':
        case 'deposit':
        case 'submitReport':
        case 'reportLMC':
        case 'pledges':
        case 'cashLoan':
        case 'inkindLoan':
        case 'cashFundraiser':
        case 'lowcostFundraiser':
        case 'auction':
        case 'vendorRefund':
        case 'otherReceipt':
        case 'bankInterest':
        case 'monetaryExpenditure':
        case 'refundContribution':
        case 'loadFaq':
        case 'setupWizard':
        case 'openCampaign':
        case 'backupCampaign':
        case 'restoreCampaign':
        case 'checkForUpdates':
        case 'importExportCampaign':
        case 'openAnonymousContributions':
        case 'openMonetaryContributions':
        case 'openCandidatePersonalFunds':
        case 'openGroupContributions':
        case 'openInkindContributions':
        case 'chartOfAccounts':
        case 'groupsContacts':
        case 'othersContacts':
        case 'couplesContacts':
        case 'individualContacts':
        if (window.orcaJava && window.orcaJava[menuItem]) {
          let fn = window.orcaJava[menuItem];
          fn();
        }
          break;
        case 'logout':
          this.logout();
          break;
      }
      if (message) {
        alert('Cannot open ORCA input screen.  Please contact Support.');
      }
    },

    setLoginStatus(loginStatus) {
      if (this.orcaFunctionExists("setLoginStatus")) {
        window.orcaJava.setLoginStatus(loginStatus);
      }
    },

    setDerbyProperties(properties) {
      let payload = {properties: properties};
      if (window.orcaJava && window.orcaJava.setDerbyProperties) {
        window.orcaJava.setDerbyProperties(JSON.stringify(payload));
      }
    },

    async syncPartial(fund_id, data, section, result) {

      let payload = {};
      payload[section] = data[section].splice(0, 1000);
      console.log("pushing" + this.version);
      await orcaDataService.pushChanges(fund_id, JSON.stringify(payload), result, this.version);
      return payload[section].length;
    },

    analyzeSync(syncOrder, data) {
      let total = 0;

      for (let section in syncOrder) {
        // noinspection JSUnfilteredForInLoop
        if (data.hasOwnProperty(section)) {
          // noinspection JSUnfilteredForInLoop
          syncOrder[section].records = data[section].length;
        } else {
          // noinspection JSUnfilteredForInLoop
          syncOrder[section].records = 0;
        }
        // noinspection JSUnfilteredForInLoop
        total += syncOrder[section].records;
      }

      return total;
    },

    async mergeContacts(json) {
      try {
        window.orcaJava.mergeContacts(JSON.stringify(json));
      } catch (e) {
        console.log("error merging contact", e);
      }
    },

    async processSyncQueue(fund_id) {
      if (this.syncQueue.length > 0 && !this.isSyncing) {
        let [payload] = this.syncQueue.splice(0, 1);
        console.log("Running sync from queue")
        this.pushChanges(fund_id, payload);
      } else {
        console.log("No sync to process")
      }
    },

    async pushChanges(fund_id, json) {
      let result = {
        last_sync: "",
        success: true,
        errors: []
      }
      let syncOrder = {
        contacts: {label: "Contacts"},
        accounts: {label: "Accounts"},
        expenditureEvents: {label: "Expenditures"},
        debtObligations: {label: "Debt Obligations"},
        receipts: {label: "Transactions"},
        loans: {label: "Loans"},
        auctionItems: {label: "Auction Items"},
        depositEvents: {label: "Deposit Events"},
        deletions: {label: "Transactions"},
      }

      if (this.isSyncing) {
        console.log("Duplicate sync detected");
        this.syncQueue.push(json);
        return;
      }

      this.setSyncActive(true);
      orcaDataService.markPending(true, 'Starting Sync of Campaign...');
      console.log("Starting Sync of fund_id: " + fund_id);
      let jsonObject = JSON.parse(json);

      let start = Date.now();

      const recordCount = this.analyzeSync(syncOrder, jsonObject);

      if (recordCount > 1500) {
        for (let section in syncOrder) {
          let count = 0;
          let total = syncOrder[section].records;
          while (jsonObject[section].length && result.success) {
            orcaDataService.markPending(true, "Uploading " + syncOrder[section].label + ": " + count + "/" + total);
            count += await this.syncPartial(fund_id, jsonObject, section, result).catch(e => {
              console.log(e);
              result.success = false;
            });
            orcaDataService.markPending(false);
          }
          if (!result.success) {
            result.last_sync = null;
            break;
          }
        }
      } else {
        orcaDataService.markPending(true, "Uploading transactions: 0/" + recordCount)
        json = JSON.stringify(jsonObject);
        await orcaDataService.pushChanges(fund_id, json, result, this.version).catch((e) => {
          console.log(e);
          result.success = false;
        });
        orcaDataService.markPending(false);
      }
      orcaDataService.markPending(false);
      this.setSyncActive(false);
      if (!result || !result.success) {
        this.webConnectionError();
      }

      console.log("Finished sync");
      console.log(result);

      if (result.success && result.last_sync) {
        let properties = [{name: "DERBY_TO_RDS", value: result.last_sync}];
        this.setDerbyProperties(properties);
      }
      for (let section in syncOrder) {
        if (jsonObject[section].length > 0) {
          ORCA.$emit(section + '.change', jsonObject[section][jsonObject[section].length - 1]);
        }
      }
      console.log(Math.floor((Date.now() - start) / 1000));
      this.processSyncQueue(fund_id);
    },

    async finishOpen(validationPayload) {
      try {
        if (this.version >= 1.420) {
          console.log("Override properties", validationPayload);
          window.orcaJava.finishOpen(JSON.stringify(validationPayload));
        } else {
          window.orcaJava.finishOpen();
        }
      } catch (e) {
        console.log("Error finishing open request", e);
      }
    },

    async runUpdates(updates) {
      window.orcaJava.executeUpdates(JSON.stringify(updates));
    },

    async setDerbyUpdated(updated) {
      let updatedObject = JSON.parse(updated);
      await orcaDataService.postFundObject(updatedObject.fund_id, 'derby-updated', updatedObject.update_id);
    },

    async saveValidations(fund_id, json) {
      this.setSyncActive(true);
      console.log(json);

      let result = await orcaDataService.saveValidations(fund_id, json).catch((error) => {
        if (!error.response || (error.response && error.response.status !== 403)) {
          this.webConnectionError();
        }
        this.setSyncActive(false);
        return Promise.reject(error);
      });
      if (!result || !result.data || !result.data.success) {
        this.webConnectionError();
      } else {
        const {orcaFund} = useOrcaFund();
        await orcaFund.setValidations(fund_id, JSON.parse(json));
      }
      this.setSyncActive(false);
      this.processSyncQueue(fund_id);
    },

    async setupNewCampaign() {
      ORCA.$emit('CampaignSetup');
    },

    async setupExistingCampaign(json) {
      this.setSyncActive(true);
      console.log("campaign setup: ", json);
      let result = await orcaDataService.setupExistingCampaign(json).catch(e => {
        this.setSyncActive(false);
        this.webConnectionError();
        console.log(e._getMessages());
      });
      if (!result || !result.data || !result.data.success) {
        this.webConnectionError();
      }

      let properties = [];
      let fund_id = "";
      if (result && result.data && result.data.fund_id) {
        fund_id = result.data.fund_id;
        properties.push({name: "FUND_ID", value: result.data.fund_id});
      }
      if (result && result.data && result.data.token) {
        properties.push({name: "TOKEN", value: result.data.token});
      }
      try {
        window.orcaJava.finishSetupExistingCampaign(JSON.stringify({properties: properties}));
      } catch (e) {
        this.setDerbyProperties(properties);
      }
      if (fund_id) {
        this.navigateTo("/ORCA/campaign/" + fund_id);
      }
      this.setSyncActive(false);

    },

    logout() {
      try {
        window.orcaJava.logout();
      } catch (e) {
        alert('Not yet implemented.  Quit ORCA instead.');
      }
    },

    navigateToVueApp() {
      try {
        window.orcaJava.navigateToVueApp();
      } catch (e) {
        alert('Cannot open application.  Please contact Support.');
        console.log(e);
      }
    },

    openCampaign() {
      try {
        window.orcaJava.openCampaign();
      } catch (e) {
        alert('Cannot open campaign.  Please contact Support.');
      }
    },
    closeCampaign() {
      try {
        window.orcaJava.closeCampaign();
      } catch (e) {
        console.log("Error closing campaign", e);
      }
    },
    restoreCampaignFromRDS(campaign) {
      try {
        window.orcaJava.restoreCampaignFromRDS(JSON.stringify(campaign));
      } catch (e) {
        alert('Cannot restore campaign. Please contact Support.');
        console.log("Error restoring campaign", e);
      }
    },
    restoreCampaign() {
      try {
        window.orcaJava.restoreCampaign();
      } catch (e) {
        alert('Cannot restore campaign. Please contact Support.');
        console.log("Error restoring campaign", e);
      }
    },
    backupCampaign() {
      try {
        window.orcaJava.backupCampaign();
      } catch (e) {
        alert('Cannot backup campaign. Please contact Support.');
        console.log("Error backing up campaign", e);
      }
    },
    createCampaign(campaignData) {
      try {
        window.orcaJava.createCampaign(JSON.stringify(campaignData));
      } catch (e) {
        alert('Cannot create new campaign. Please contact Support.');
        console.log("Error creating new campaign", e);
      }
    },
    importExportCampaign() {
      try {
        window.orcaJava.importExportCampaign();
      } catch (e) {
        alert('Cannot import or export campaign. Please contact Support.');
        console.log("Error import export", e);
      }
    },
    setupWizard() {
      try {
        window.orcaJava.setupWizard();
      } catch (e) {
        alert('Unable to set up new campaign. Please contact Support.');
        console.log("Error in setup", e);
      }
    },
    loadFaq() {
      try {
        window.orcaJava.loadFaq();
      } catch (e) {
        alert('Unable to load FAQ. Please contact Support.');
        console.log("Error loading faq", e);
      }
    },
    checkForUpdates() {
      try {
        window.orcaJava.checkForUpdates();
      } catch (e) {
        alert('Unable to check for software updates. Please contact Support.');
        console.log("Error checking for softare updates", e);
      }
    },
    openMonetaryContributions() {
      try {
        window.orcaJava.openMonetaryContributions();
      } catch (e) {
        alert('Unable to open monetary contributions. Please contact support.');
        console.log("Error opening monetary contributions", e);
      }
    },
    openGroupContributions() {
      try {
        window.orcaJava.openGroupContributions();
      } catch (e) {
        alert('Unable to open group contributions. Please contact support.');
        console.log("Error opening group contributions", e);
      }
    },
    openInkindContributions() {
      try {
        window.orcaJava.openInkindContributions();
      } catch (e) {
        alert('Unable to open in-kind contributions. Please contact support.');
        console.log("Error to opening in-kind contributions", e);
      }
    },
    openCandidatePersonalFunds() {
      try {
        window.orcaJava.openCandidatePersonalFunds();
      } catch (e) {
        alert("Unable to open candidate's personal funds. Please contact support.");
        console.log("Error opening candidate's personal funds", e);
      }
    },
    openAnonymousContributions() {
      try {
        window.orcaJava.openAnonymousContributions();
      } catch (e) {
        alert('Unable to open anonymous contributions. Please contact support.');
        console.log("Error to open anonymous contribu")
      }
    },
    chartOfAccounts() {
      try {
        window.orcaJava.chartOfAccounts();
      } catch (e) {
        alert('Cannot open chart of accounts.  Please contact Support.');
        console.log("Error opening chart", e);
      }
    },
    webConnectionError() {
      try {
        window.orcaJava.webConnectionError();
      } catch (e) {
        alert("Error reporting an error");
        console.log("Error opening web connection", e);
      }
    },
    getContacts() {
      try {
        let contacts = window.orcaJava.getContacts();
        return JSON.parse(contacts);
      } catch (e) {
        console.log("Error retrieving contacts");
      }
    },
    getContact(contactId) {
      try {
        let contact = window.orcaJava.getContact(parseInt(contactId));
        return JSON.parse(contact);
      } catch (e) {
        console.log("error getting contact", e);
      }
    },
    saveContact(contact) {
      try {
        window.orcaJava.saveContact(JSON.stringify(contact));
      } catch (e) {
        alert("Cannot save contact to ORCA");
        console.log("error saving contact to orca", e);
      }

    },
    disableSync() {
      try {
        window.orcaJava.disableSync();
      } catch (e) {
        console.log("Error disabling Sync", e);
      }
    },
    enableSync() {
      try {
        window.orcaJava.enableSync();
      } catch (e) {
        console.log("Error enabling sync ", e);
      }
    },
    bulkSave(data) {
      try {
        window.orcaJava.bulkSave(JSON.stringify(data));
      } catch (e) {
        console.log("error executing bulk save to ORCA", e);
      }
    },
    saveExpenditure(expenditure) {
      try {
        window.orcaJava.saveExpenditure(JSON.stringify(expenditure));
      } catch (e) {
        alert("Cannot save expenditure to ORCA!");
        console.log("Error saving expenditure", e);
      }
    },
    saveVendorRefund(vendor_refund) {
      try {
        window.orcaJava.saveVendorRefund(JSON.stringify(vendor_refund));
      } catch (e) {
        alert("Cannot save vendor refund to ORCA!");
        console.log("Error saving vendor refund", e);
      }
    },
    saveCreditCard(credit_card) {
      try {
        window.orcaJava.saveCreditCard(JSON.stringify(credit_card));
      } catch (e) {
        alert("Cannot save credit card to ORCA!");
        console.log("Error saving credit card", e);
      }
    },
    saveCreditCardPayment(payment) {
      try {
        window.orcaJava.saveCreditCardPayment(JSON.stringify(payment));
      } catch (e) {
        alert("Cannot save credit card payment to ORCA!");
        console.log("Error saving credit card payment", e);
      }
    },
    saveContribution(contribution) {
      try {
        window.orcaJava.saveContribution(JSON.stringify(contribution));
      } catch (e) {
        alert("Cannot save contribution to ORCA!");
        console.log("Error saving contribution", e);
      }
    },
    saveLowCostFundraiser(fundraiser) {
      try {
        window.orcaJava.saveLowCostFundraiser(JSON.stringify(fundraiser));
      } catch (e) {
        alert("Cannot save fundraiser to ORCA!");
        console.log("Error saving fundraiser", e);
      }
    },
    saveMiscOther(misc) {
      try {
        window.orcaJava.saveMiscOther(JSON.stringify(misc));
      } catch (e) {
        alert("Cannot save fundraiser to ORCA!");
        console.log("Error saving fundraiser", e);
      }
    },
    saveDepositEvent(deposit) {
      try {
        window.orcaJava.saveDepositEvent(JSON.stringify((deposit)));
      } catch (e) {
        alert("Cannot save deposit to ORCA!");
        console.log("error saving deposit", e);
      }
    },
    saveLoan(loan) {
      try {
        window.orcaJava.saveLoan(JSON.stringify(loan));
      } catch (e) {
        alert("Cannot save loan to ORCA!");
        console.log("error saving loan", e);
      }
    },
    saveLoanTransaction(transaction) {
      try {
        window.orcaJava.saveLoanTransaction(JSON.stringify(transaction))
      } catch (e) {
        alert("Cannot save loan transaction to ORCA!");
        console.log("error saving loan", e);
      }
    },
    saveFundraiserContribution(contribution) {
      try {
        window.orcaJava.saveFundraiserContribution(JSON.stringify(contribution))
      } catch (e) {
        alert("Cannot save fundraiser contribution to ORCA!");
        console.log("error saving fundraiser contrubution", e);
      }
    },
    deleteLoan(loan) {
      try {
        window.orcaJava.deleteLoan(JSON.stringify(loan))
      } catch (e) {
        alert("Cannot delete loan from ORCA!!");
        console.log("error deleting loan", e);
      }
    },
    deleteDeposit(deposit) {
      try {
        window.orcaJava.deleteDepositEvent(JSON.stringify((deposit)));
      } catch (e) {
        alert("Cannot delete deposit from ORCA!");
        console.log("error deleting deposit", e)
      }
    },
    saveMiscBankInterest(misc) {
      try {
        window.orcaJava.saveMiscBankInterest(JSON.stringify(misc));
      } catch (e) {
        alert("Cannot save bank interest to ORCA!");
        console.log("Error saving bank interest", e);
      }
    },
    saveAccountTransfer(transfer) {
      try {
        window.orcaJava.saveAccountTransfer(JSON.stringify(transfer));
      } catch (e) {
        alert("Cannot save bank interest to ORCA!");
        console.log("Error saving bank interest", e);
      }
    },
    saveCorrection(correction) {
      try {
        window.orcaJava.saveCorrection(JSON.stringify(correction));
      } catch (e) {
        alert("Cannot save correction to ORCA!");
        console.log("Error saving correction", e);
      }
    },
    saveReceipt(receipt) {
      try {
        window.orcaJava.saveReceipt(JSON.stringify(receipt));
      } catch (e) {
        alert("Cannot save receipt on ORCA!");
        console.log("Error saving receipt", e);
      }
    },
    deleteReceipt(receipt) {
      try {
        window.orcaJava.deleteReceipt(JSON.stringify(receipt));
      } catch (e) {
        alert("Cannot delete receipt on ORCA!");
        console.log("Error deleting receipt", e);
      }
    },
    deleteReceiptContact(receipt) {
      try {
        window.orcaJava.deleteReceiptContact(JSON.stringify(receipt));
      } catch (e) {
        alert("Cannot delete receipt on ORCA!");
        console.log("Error deleting receipt", e);
      }
    },
    deleteReceiptContactAccount(receipt) {
      try {
        window.orcaJava.deleteReceiptContactAccount(JSON.stringify(receipt));
      } catch (e) {
        alert("Cannot delete fundraiser on ORCA");
        console.log("Error deleting fundraiser", e);
      }
    },
    deleteContribution(contribution) {
      try {
        window.orcaJava.deleteContribution(JSON.stringify(contribution));
      } catch (e) {
        alert("Cannot delete contribution to ORCA!");
        console.log("error deleting contribution", e);
      }
    },
    handleOpenCampaign(json) {
      this.campaign = JSON.parse(json);
      ORCA.$emit('openORCACampaign');
    },
    deleteExpenditure(expenditure) {
      try {
        window.orcaJava.deleteExpenditure(JSON.stringify(expenditure))
      } catch (e) {
        alert("Cannot delete expenditure from ORCA");
        console.log('Error deleting expenditure', e);
      }
    },
    deleteContact(contact) {
      try {
        window.orcaJava.deleteContact(JSON.stringify(contact));
      } catch (e) {
        alert("Cannot delete contact from ORCA");
        console.log('Error deleting contact', e);
      }
    },
    updateCarryForward(carryForward) {
      try {
        console.log(carryForward);
        window.orcaJava.updateCarryForward(JSON.stringify(carryForward));
      } catch (e) {
        alert("Cannot save carry forward to ORCA!");
        console.log("Error saving carry forward", e);
      }
    },
    async doSubmissionSync(object) {
      let updatedObject = JSON.parse(object);
      let syncData = await orcaDataService.getFundObject(updatedObject.fund_id, 'sync');
      let syncPayload = {'orcaProperties': syncData};
      window.orcaJava.doJSONSync(JSON.stringify(syncPayload));
    },
    saveFundraiserEvent(fundraiser) {
      try {
        window.orcaJava.saveFundraiserEvent(JSON.stringify(fundraiser));
      } catch (e) {
        alert("Cannot save fundraiser event to ORCA!");
        console.log("Error saving fundraiser event", e);
      }
    },
    saveAuction(auction) {
      try {
        window.orcaJava.saveAuction(JSON.stringify(auction));
      } catch (e) {
        alert("Cannot save Auction to ORCA!");
        console.log("Error saving auction", e);
      }
    },
    saveAuctionItem(item) {
      try {
        console.log(item);
        window.orcaJava.saveAuctionItem(JSON.stringify(item));
      } catch (e) {
        alert("Cannot save auction item to ORCA!");
        console.log("Error saving auction item", e);
      }
    },
    savePledge(pledge) {
      try {
        window.orcaJava.savePledge(JSON.stringify(pledge));
      } catch (e) {
        alert("Cannot save pledge to ORCA!");
        console.log("Error saving pledge", e);
      }
    },
    savePledgeTransaction(item) {
      try {
        window.orcaJava.savePledgeTransaction(JSON.stringify(item));
      } catch (e) {
        alert("Cannot save cancelled pledge to ORCA!");
        console.log("error saving cancelled pledge", e);
      }
    },
    saveAccount(account) {
      try {
        window.orcaJava.saveAccount(JSON.stringify(account));
      } catch (e) {
        alert("Cannot save account to ORCA!");
        console.log("Error saving account", e);
      }
    },
    openApollo() {
      try {
        window.orcaJava.openApollo();
      } catch (e) {
        alert('Unable to open Apollo in browser. Please contact Support.');
        console.log("Error loading Apollo", e);
      }
    },
  }
});
window.ORCA = ORCA;
export default ORCA;