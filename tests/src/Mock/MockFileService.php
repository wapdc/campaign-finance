<?php

namespace Tests\WAPDC\CampaignFinance\Mock;


use WAPDC\Core\FileServiceInterface;

class MockFileService implements FileServiceInterface {

  public function getPubliclyAccessibleURL($url, $expires){
    return $url;
  }

  public function setFileAccess($url, $access_mode){
    return $url;
  }

  public function deleteFile($url){

  }

  public function getFilePostFormElements($filepath, $access_mode, $content_type, $meta_data = NULL){
    $form = new \stdClass();
    $form->url = 'http://www.pdc.wa.gov/';
    $form->formElements = new \stdClass();
    $form->formElements->formAttributes = new \stdClass();
    $form->formElements->formElements = new \stdClass();
    return $form;
  }

}
