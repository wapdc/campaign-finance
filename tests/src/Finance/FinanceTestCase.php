<?php


namespace Tests\WAPDC\CampaignFinance\Finance;


use DateTime;
use PHPUnit\Framework\TestCase;
use WAPDC\CampaignFinance\CFDataManager;
use \Exception;
use \stdClass;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\Contribution;
use WAPDC\CampaignFinance\Model\Fund;
use WAPDC\CampaignFinance\Model\Loan;
use WAPDC\CampaignFinance\Model\Report;
use WAPDC\CampaignFinance\Model\Sum;
use WAPDC\CampaignFinance\Model\Transaction;
use WAPDC\Core\Model\Candidacy;
use WAPDC\Core\Model\Person;
use WAPDC\CampaignFinance\CommitteeManager;


/**
 * Class FinanceTestCase
 *
 * Contains generalized testing functions for Campaign Finance portion of code.
 */
abstract class FinanceTestCase extends TestCase
{

  /** @var CFDataManager */
  protected $dm;

  /** @var Committee */
  protected $committee;

  /** @var Fund */
  protected $fund;

  /** @var string */
  protected $data_dir;

  private Person $person;

  public Object $thresholds;

  /** @var CommitteeManager */
  protected $committeeManager;


  /**
   * @throws Exception
   */
  protected function setUp(): void
  {
    parent::setUp();
    date_default_timezone_set('America/Los_Angeles');
    $this->data_dir = dirname(dirname(__DIR__)) . '/data';
    $this->dm = new CFDataManager();
    $this->committeeManager = new CommitteeManager($this->dm);
    $this->dm->beginTransaction();
    $this->thresholds = $this->getThresholds();
  }

  protected function tearDown(): void
  {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

  protected function assertProperties($fund_id, $test_properties){
    $query =  'select * from private.properties p where p.fund_id = :fund_id';
    $saved_properties = $this->dm->db->executeQuery($query, [$fund_id])->fetchAllAssociative();

    $saved_properties_reduced = array_reduce($saved_properties, function($acc, $cur) {
      $acc[$cur['name']] = $cur['value'];
      return $acc;
    }, []);
    foreach($test_properties as $property) {
      $this->assertEquals($property->value, $saved_properties_reduced[$property->name]);
    }
  }

    /**
     * Create a committee for use during testing.
     * @param \stdClass $committee_data
     *   Properties of the committee to set on creation.
     * @return Committee
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws Exception
     */
  public function createCommittee(stdClass $committee_data)
  {
          if (empty($committee_data->name)) {
              $committee_data->name = 'Test Committee';
          }
          $this->committee = new Committee($committee_data->name);
          foreach ($committee_data as $key => $value) {
            if (property_exists($this->committee, $key)) {
              $this->committee->$key = $value;
            };
          }

    $this->dm->em->persist($this->committee);
    $this->dm->em->flush($this->committee);
    return $this->committee;
  }
    /**
     * Create a committee for use during testing.
     * @param \stdClass $committee_data
     *   Properties of the committee to set on creation.
     * @return Committee
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws Exception
     */

    public function createFullCommittee(stdClass $committee_data)
    {
            $committee_id = $this->committeeManager->createCommitteeFromFile($committee_data);
            $this->committeeManager->saveCommitteeFromSubmission($committee_data);
            $this->committee = $this->dm->em->find(Committee::class, $committee_id);
            if ($submission->committee->registered_at ?? NULL){
                $this->committee->registered_at = new DateTime($committee_data->committee->registered_at);
            }
            $this->dm->em->flush($this->committee);
            return $this->committee;

    }

  /**
   * Create a fund for use during testing.
   * @param \stdClass $fund_data
   *   Properties of the fund data to set on creation.
   * @param $committee_id
   */
  public function createFund(stdClass $fund_data, $committee_id)
  {
    $fund = new Fund();
    $this->fund = $fund;
    foreach ($fund_data as $key => $value) {
      switch ($key) {
        default:
          $fund->$key = $value;
      }
    }
    $fund->committee_id = $committee_id;
    $this->dm->em->persist($this->fund);
    $this->dm->em->flush($this->fund);
    return $this->fund;
  }

  public function createPersonAndCandidacy(stdClass $candidacy_data, $committee_id) {
    $this->person = new Person();
    $this->person->name = "Test Person";
    $this->person->address = "1234 somestreet";
    $this->person->city = "SomeTown";
    $this->person->state = "ZZ";
    $this->person->postcode = "99999";

    $this->dm->em->persist($this->person);
    $this->dm->em->flush();

    $candidacy = new Candidacy();
    $candidacy->person_id = $this->person->person_id;
    $candidacy->committee_id = $committee_id;
    $candidacy->ballot_name = $candidacy_data->ballot_name ?? null;
    $candidacy->jurisdiction_id = $candidacy_data->jurisdiction_id ?? null;
    $candidacy->office_code = $candidacy_data->office_code;
    $this->dm->em->persist($candidacy);
    $this->dm->em->flush($candidacy);
    return $candidacy->candidacy_id;
  }

  protected function assertExpectedData($a, $b, $message) {
    foreach ($a as $key => $value) {
      switch (TRUE) {
        case (is_object($value)):
          $this->assertExpectedData($value, $b->$key, "$message:$key");
          break;
        case (is_array($value)):
          foreach($value as $k => $v) {
            $this->assertExpectedData($v, $b->$key[$k] ?? NULL, "$message:$key:$k");
          }
          break;
        case ($b === NULL):
          $this->assertIsObject($b, "$message:$key no object");
          break;
        default:
          $this->assertObjectHasAttribute($key, $b, "$message:$key");
          $this->assertSame($value, $b->$key, "$message:$key");
      }
    }
  }

  /**
   * Sets the property of an object or removes the property if the value is null.
   *
   * @param $data
   * @param $property
   * @param $value
   */
  protected function setProperty($data, $property, $value) {
    // Determine if the property has a '.' so we know if we need to drill further.
    if (strpos($property, '.')) {
      list($outer, $inner) = explode('.', $property, 2);
      // See if we have an array index.
      if ($outer === '0' || (int)$outer) {
        $this->setProperty($data[$outer], $inner, $value);
      } else {
        $this->setProperty($data->$outer, $inner, $value);
      }
    } else {
      if (isset($data->$property) && $value === NULL) {
        unset($data->$property);
      } else {
        $data->$property = $value;
      }
    }
  }

  protected function checkLoans($reportedLoans, Report $saved_report, $contacts) {
    /** @var Transaction[] $loanTransactions */
    $loanActs = ['receipt', 'endorsement', 'carry_forward'];

    $loanTransactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id,'category' => 'loan', 'act' => $loanActs], ['transaction_id' => 'asc']);

    // Transaction count should be all loans and all endorsements for each loan.
    $countReportedLoansAndEndorsements = array_reduce($reportedLoans, function ($carry, $loanItem) {
      return $carry + 1 + (isset($loanItem->endorser) && is_array($loanItem->endorser) ? count($loanItem->endorser) : 0);
    }, 0);
    $this->assertEquals($countReportedLoansAndEndorsements, count($loanTransactions));

    // The test is reliant on the order that the transactions are written to the database which is not ideal but works
    // correctly with the current report processing code. It assumes that when there are endorsements, the transaction
    // order is loan, endorsement, loan, endorsement, endorsement where the endorsements follow the loan they are
    // attached to.
    /** @var Transaction $currentTransaction */
    $currentTransaction = reset($loanTransactions);
    foreach ($reportedLoans as $reportedLoan) {
      // The value of $currentTransaction needs to be incremented for each loan or endorsement transaction

      /** @var Loan $loan_rec */
      $sum = $this->dm->em->find(Sum::class, $currentTransaction->transaction_id);
      $contrib = $this->dm->em->find(Contribution::class, $currentTransaction->transaction_id);
      $loan = $this->dm->em->find(Loan::class, $currentTransaction->transaction_id);

      $contact = $contacts[$reportedLoan->contact_key];
      $this->assertEquals($reportedLoan->amount, $currentTransaction->amount);
      $this->assertEquals($reportedLoan->in_kind ?? false, $currentTransaction->inkind);
      $this->assertEquals($reportedLoan->contact_key, $contact->contact_key);
      $this->assertEquals($reportedLoan->date, $currentTransaction->transaction_date->format('Y-m-d'));
      $this->assertEquals($reportedLoan->due_date, $loan->due_date->format('Y-m-d'));
      $this->assertEquals($reportedLoan->date, $loan->loan_date->format('Y-m-d'));
      $this->assertEquals($reportedLoan->interest_rate, $loan->interest_rate);
      $this->assertEquals($reportedLoan->payment_schedule, $loan->payment_schedule);
      $this->assertEquals($reportedLoan->election, $contrib->prim_gen);
      $this->assertEquals($contact->contributor_type, $contrib->contributor_type);
      $this->assertEquals($contact->name, $contrib->name);
      $this->assertEquals($contact->address, $contrib->address);
      $this->assertEquals($contact->city, $contrib->city);
      $this->assertEquals($contact->state, $contrib->state);
      $this->assertEquals($contact->postcode, $contrib->postcode);
      if (!empty($contact->employer)) {
        $this->assertEquals($contact->employer->name, $contrib->employer);
        $this->assertEquals($contact->employer->address ?? NULL, $contrib->employer_address);
        $this->assertEquals($contact->employer->city ?? NULL, $contrib->employer_city);
        $this->assertEquals($contact->employer->state ?? NULL, $contrib->employer_state);
        $this->assertEquals($contact->employer->postcode ?? NULL, $contrib->employer_postcode);
      }
      $aggregate = $contrib->prim_gen == 'P' ? $contact->aggregate_primary : $contact->aggregate_general;
      $this->assertEquals($aggregate, $sum->aggregate_amount);
      /** @var Transaction $parentTransaction */
      $parentTransaction = $currentTransaction;
      $currentTransaction = next($loanTransactions);

      if (isset($reportedLoan->endorser) && is_array($reportedLoan->endorser)) {
        foreach ($reportedLoan->endorser as $endorser) {
          $this->assertEquals($currentTransaction->parent_id, $parentTransaction->transaction_id);
          $contrib = $this->dm->em->find(Contribution::class, $currentTransaction->transaction_id);
          $contact = $contacts[$endorser->contact_key];
          $this->assertEquals($endorser->contact_key, $contact->contact_key);
          $this->assertEquals($endorser->election, $contrib->prim_gen);
          $this->assertEquals($contact->contributor_type, $contrib->contributor_type);
          $this->assertEquals($contact->name, $contrib->name);
          $this->assertEquals($contact->address, $contrib->address);
          $this->assertEquals($contact->city, $contrib->city);
          $this->assertEquals($contact->state, $contrib->state);
          $this->assertEquals($contact->postcode, $contrib->postcode);
          if (!empty($contact->employer)) {
            $this->assertEquals($contact->employer->name, $contrib->employer);
            $this->assertEquals($contact->employer->address ?? NULL, $contrib->employer_address);
            $this->assertEquals($contact->employer->city ?? NULL, $contrib->employer_city);
            $this->assertEquals($contact->employer->state ?? NULL, $contrib->employer_state);
            $this->assertEquals($contact->employer->postcode ?? NULL, $contrib->employer_postcode);
          }
          $currentTransaction = next($loanTransactions);
        }
      }
    }
  }
    /**
     * @return object
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    function getThresholds(): object
    {
        $thresholds = $this->dm->db->executeQuery('select property, amount from public.thresholds')->fetchAllAssociative();
        $arr = array();
        foreach ($thresholds as $threshold){
            $arr[$threshold['property']] = $threshold['amount'];
        }
        return (object)$arr;
    }
}