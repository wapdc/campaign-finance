<?php


namespace Tests\WAPDC\CampaignFinance\Finance;

use Doctrine\DBAL\Driver\Exception;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\FinanceReportingProcessor;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\Model\APIConsumer;
use WAPDC\CampaignFinance\Model\APIConsumerVersion;
use \DateTime;
use \stdClass;


/**
 * Class FinanceReportingProcessorTest
 *
 * @coversDefaultClass \WAPDC\CampaignFinance\FinanceReportingProcessor
 * @package Tests\WAPDC\CampaignFinance
 */
class FinanceReportingProcessorTest extends TestCase {

  /** @var CFDataManager */
  private $dm;

  /** @var FinanceReportingProcessor */
  protected $financeReportingProcessor;

  /**
   * @throws DBALException
   * @throws ORMException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->financeReportingProcessor = new FinanceReportingProcessor($this->dm, new CommitteeManager($this->dm));
  }

  /**
   * Processes a ORCA Soap payload and inserts the xml, date and fund_id
   *   and ensures that the data is logged to the database.
   *
   * @throws \Doctrine\DBAL\DBALException
   * @throws Exception
   */
  public function testSoapProcessor() {
    $dir = dirname(dirname(__DIR__)) . '/data/campaign_finance_submission';
    $expected_xml = file_get_contents("$dir/c3.xml");

    $payload = file_get_contents("$dir/c3_submission.xml");
    $type = 'xml';
    $committee_id = '-9999';
    $version = '1.29';
    $source = 'TEST';
    $ip_address = '1.2.3.4.5';
    $submission_id = $this->financeReportingProcessor->logSubmission($type, $payload, $source, $version, $committee_id, $ip_address);

    $query = $this->dm->db->executeQuery("SELECT * from campaign_finance_submission WHERE committee_id = $committee_id");
    $query_result = $query->fetchAllAssociative();
    $this->assertNotEmpty($query_result);
    $result = $query_result[0]['payload'];
    $this->assertEquals($result, $expected_xml);
    $this->assertEquals($source, $query_result[0]['source']);
    $this->assertEquals($type, $query_result[0]['type']);
    $this->assertEquals($ip_address, $query_result[0]['ip_address']);
    $updated = new DateTime();
    $expected_updated_at = $updated->format('Y-m-d H:i:s');
    $repno=-9999999;
    $fund_id=-999999;
    $this->financeReportingProcessor->logProcessResults($submission_id, true, 'Warning: Test response message from Orca', $repno, $fund_id);

    $query= $this->dm->db->executeQuery("SELECT submitted, report_id, fund_id, processed, message, updated_at, has_warnings from campaign_finance_submission WHERE submission_id = $submission_id");
    $query_result = $query->fetchAllAssociative();
    $this->assertNotEmpty($query_result);
    $this->assertEquals(true,$query_result[0]['submitted']);
    $this->assertEquals(true,$query_result[0]['processed']);
    $this->assertEquals($repno,$query_result[0]['report_id']);
    $this->assertEquals($fund_id, $query_result[0]['fund_id']);
    $this->assertTrue($query_result[0]['has_warnings']);
    $this->assertEquals('Warning: Test response message from Orca',$query_result[0]['message']);
    $this->assertGreaterThanOrEqual($expected_updated_at,$query_result[0]['updated_at']);
    $this->financeReportingProcessor->logProcessResults($submission_id, false, 'Error: Test response message from Orca', $repno);
    $query= $this->dm->db->executeQuery("SELECT submitted, report_id, processed, message, updated_at from campaign_finance_submission WHERE submission_id = $submission_id");
    $query_result = $query->fetchAllAssociative();
    $this->assertEquals(false,$query_result[0]['processed']);

  }


  /**
   * test the creation of a API consumer record.
   *   the database expects an array
   *
   * @throws \Doctrine\DBAL\DBALException
   * @throws \Exception
   * @throws Exception
   */
  public function testCreateConsumer() {
    // Create the Consumer
    $consumer = new stdClass();
    $consumer->application = "ORCALite";
    $consumer->vendor = "PDC";
    $consumer->vendor_email = "ITTech@pdc.wa.gov";
    $consumer->vendor_phone = "360-753-1111";
    $consumer->api_key = "8a7eht938i4n2039u4";
    $consumer->vendor_contact_name = "PDC IT Department";

    $consumer_id =  $this->financeReportingProcessor->createConsumer($consumer);

    // verify that the database has these values
    $query = $this->dm->db->executeQuery("SELECT * from api_consumer WHERE consumer_id = " . $consumer_id)->fetchAssociative();
    $query = (object) $query;

    $this->assertEquals($consumer->application, $query->application);
    $this->assertEquals($consumer->vendor, $query->vendor);
    $this->assertEquals($consumer->vendor_email, $query->vendor_email);
    $this->assertEquals($consumer->vendor_phone, $query->vendor_phone);
    $this->assertEquals($consumer->api_key, $query->api_key);
    $this->assertEquals($consumer->vendor_contact_name, $query->vendor_contact_name);


    // Create the Application for the consumer
    $version = new \stdClass();
    $version->application = $consumer->application;
    $version->consumer_id = $consumer_id;
    $version->release_date = "2020-01-01";
    $version->version = "1.026";
    $version->mandatory_update = true;
    $version->fix = "Updated jurisdictions. 
    Updated service connection information used when ORCALite submits information.";

    $version_id = $this->financeReportingProcessor->createVersion($version);

    $query = $this->dm->db->executeQuery("SELECT * from api_consumer_version WHERE version_id = " . $version_id)->fetchAssociative();
    $query = (object) $query;

    $this->assertEquals($version->consumer_id, $query->consumer_id);
    $this->assertEquals($version->release_date, $query->release_date);
    $this->assertEquals($version->version, $query->version);
    $this->assertEquals($version->mandatory_update, $query->mandatory_update);
    $this->assertEquals($version->fix, $query->fix);
  }



  /**
   * Receives a version string and checks for a newer version, returning a fixes change log list of items
   * @throws \Doctrine\DBAL\DBALException
   * @throws \Doctrine\ORM\ORMException
   */
  public function testGetUpdates() {
    $expectedSingleLineFix = 'single line fix';
    $expectedMultiLineFix = 'multi-line fix' . chr(10) . 'multi-line fix';
    $apiConsumer = new APIConsumer();
    $apiConsumer->application = 'test orca';
    $apiConsumer->vendor = 'test vendor';
    $this->dm->em->persist($apiConsumer);
    $this->dm->em->flush();

    $consumer = $this->dm->em->getRepository(apiConsumer::class)
      ->findOneBy(['application' => 'test orca', 'vendor' => 'test vendor']);

    $apiConsumerVersion = new APIConsumerVersion();
    $apiConsumerVersion->consumer_id = $consumer->consumer_id;
    $apiConsumerVersion->version = '1.1';
    $apiConsumerVersion->mandatory_update = 'TRUE';
    $apiConsumerVersion->fix = $expectedSingleLineFix;
    $this->dm->em->persist($apiConsumerVersion);
    $apiConsumerVersion = new APIConsumerVersion();
    $apiConsumerVersion->consumer_id =  $consumer->consumer_id;
    $apiConsumerVersion->version = '1.2';
    $apiConsumerVersion->mandatory_update = 'FALSE';
    $apiConsumerVersion->fix = $expectedMultiLineFix;
    $this->dm->em->persist($apiConsumerVersion);
    $this->dm->em->flush();
    $updates = $this->financeReportingProcessor->getApplicationUpdates('1.0','test orca', 'test vendor');
    $this->assertEquals($expectedSingleLineFix,$updates[0]['fix'],'Failed to assert the single line fix');
    $this->assertEquals($expectedMultiLineFix,$updates[1]['fix'],'Failed to assert the single line fix');


  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }


}