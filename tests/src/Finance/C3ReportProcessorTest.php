<?php


namespace Tests\WAPDC\CampaignFinance\Finance;


use WAPDC\CampaignFinance\Model\AuctionItem;
use WAPDC\CampaignFinance\Model\Contribution;
use WAPDC\CampaignFinance\Model\Loan;
use WAPDC\CampaignFinance\Model\Report;
use WAPDC\CampaignFinance\Model\Sum;
use WAPDC\CampaignFinance\Model\Transaction;
use WAPDC\CampaignFinance\Reporting\C3ReportProcessor;
use Symfony\Component\Yaml\Yaml;
use \DateTime;
use \Exception;
use WAPDC\CampaignFinance\Reporting\ReportProcessorBase;

class C3ReportProcessorTest extends FinanceTestCase {

  /** @var C3ReportProcessor */
  protected $processor;

  protected function setUp(): void {
    parent::setUp();
    $this->processor = new C3ReportProcessor($this->dm, 1.1);
  }

  public function testProcessor() {
    $this->assertInstanceOf(C3ReportProcessor::class, $this->processor);
  }

  protected function checkContributions($contributions, Report $saved_report, $contacts, $databaseNow) {
    // Find all of the transactions created by the report.
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id,'category' => 'contribution', 'act' => 'receipt'], ['transaction_id' => 'asc']);
    //$this->assertEquals(count($contributions), count($transactions));
    foreach ($transactions as $i => $transaction) {
      $c = $contributions[$i];
      $contact = $contacts[$c->contact_key];
      $this->assertEquals($c->amount, $transaction->amount);
      $this->assertEquals('receipt', $transaction->act);
      $this->assertEquals($saved_report->fund_id, $transaction->fund_id);
      $this->assertEquals($c->date, $transaction->transaction_date->format('Y-m-d'));
      $this->assertGreaterThanOrEqual($databaseNow, $transaction->updated_at->format('Y-m-d h:i:s'));
      $this->assertEquals(FALSE, $transaction->inkind);
      $this->assertEquals(TRUE, $transaction->itemized);

      /** @var Contribution $contribution */
      $contribution = $this->dm->em->find(Contribution::class, $transaction->transaction_id);
      $this->assertEquals($contact->name, $contribution->name);
      $this->assertEquals($contact->address ?? NULL, $contribution->address);
      $this->assertEquals($contact->city ?? NULL, $contribution->city);
      $this->assertEquals($contact->state ?? NULL, $contribution->state);
      $this->assertEquals($contact->postcode ?? NULL, $contribution->postcode);
      $this->assertEquals($c->contact_key, $contribution->contact_key);
      $this->assertEquals($contact->contributor_type, $contribution->contributor_type);
      $this->assertEquals($c->election, $contribution->prim_gen);

      if (!empty($contact->employer)) {
        $e = $contact->employer;
        $this->assertEquals($e->name, $contribution->employer);
        $this->assertEquals($e->city, $contribution->employer_city);
        $this->assertEquals($e->occupation, $contribution->occupation);
      }

      /** @var Sum $sum */
      $sum = $this->dm->em->find(Sum::class, $transaction->transaction_id);
      $this->assertEquals($contact->aggregate_general, $sum->aggregate_amount);
    }
  }

  protected function checkAuctions($auctions, Report $saved_report, $contacts, $phpNow) {
    if (!$auctions) {
      return;
    }
    $items = [];
    // Reorganize auction transactions
    foreach ($auctions as $auction) {
      foreach ($auction->items as $i) {
        $i->date = $auction->date;
        $items[] = $i;
      }

    }

    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id,'category' => 'contribution', 'act' => 'auction donation'], ['transaction_id' => 'asc']);
    $this->assertEquals(count($items), count($transactions));

    foreach ($transactions as $i =>  $d_transaction) {
      /** @var AuctionItem $item */
      $item = $this->dm->em->find(AuctionItem::class, $d_transaction->transaction_id);

      /** @var Contribution $d_contribution */
      $d_contribution = $this->dm->em->find(Contribution::class, $item->donation_transaction_id);

      /** @var Sum $d_sum */
      $d_sum = $this->dm->em->find(Sum::class, $item->donation_transaction_id);

      /** @var Transaction $b_transaction */
      $b_transaction = $this->dm->em->find(Transaction::class, $item->buy_transaction_id);

      /** @var Contribution $b_contribution */
      $b_contribution = $this->dm->em->find(Contribution::class, $item->buy_transaction_id);

      /** @var Sum $b_sum */
      $b_sum = $this->dm->em->find(Sum::class, $item->buy_transaction_id);

      $ai = $items[$i];

      // Auction item
      $this->assertEquals($ai->description, $item->item_description);
      $this->assertEquals($ai->market_value, $item->fair_market_value );
      $this->assertEquals($ai->sale_amount, $item->sale_amount);

      // Donation transaction
      $this->assertEquals($saved_report->fund_id, $d_transaction->fund_id);
      $this->assertEquals($ai->donate->amount,  $d_transaction->amount);
      $this->assertEquals('contribution', $d_transaction->category);
      $this->assertEquals('auction donation', $d_transaction->act);
      $this->assertEquals(true, $d_transaction->itemized);
      $this->assertEquals($ai->date, $d_transaction->transaction_date->format('Y-m-d'));
      $this->assertGreaterThanOrEqual($phpNow, $d_transaction->updated_at);

      // Donation Contribution
      $donor = $contacts[$ai->donate->contact_key];
      $this->assertEquals($donor->name, $d_contribution->name);
      $this->assertEquals($donor->address ?? null, $d_contribution->address);
      $this->assertEquals($donor->city, $d_contribution->city);
      $this->assertEquals($donor->state, $d_contribution->state);
      $this->assertEquals($donor->postcode, $d_contribution->postcode);
      if (!empty($donor->employer)) {
        $this->assertEquals($donor->employer->name, $d_contribution->employer);
        $this->assertEquals($donor->employer->address ?? NULL, $d_contribution->employer_address);
        $this->assertEquals($donor->employer->city ?? NULL, $d_contribution->employer_city);
        $this->assertEquals($donor->employer->state ?? NULL, $d_contribution->employer_state);
        $this->assertEquals($donor->employer->postcode ?? NULL, $d_contribution->employer_postcode);
      }
      $this->assertEquals($ai->donate->election, $d_contribution->prim_gen);
      $this->assertEquals($donor->contributor_type, $d_contribution->contributor_type);

      // Donation Sum
      $donor_aggregate = $d_contribution->prim_gen == 'P' ? $donor->aggregate_primary : $donor->aggregate_general;
      $this->assertEquals($donor_aggregate, $d_sum->aggregate_amount);

      // Buy transaction
      $this->assertEquals($saved_report->fund_id, $b_transaction->fund_id);
      $this->assertEquals($ai->buy->amount,  $b_transaction->amount);
      $this->assertEquals('contribution', $b_transaction->category);
      $this->assertEquals('auction buy', $b_transaction->act);
      $this->assertEquals(true, $b_transaction->itemized);
      $this->assertEquals($ai->date, $b_transaction->transaction_date->format('Y-m-d'));
      $this->assertGreaterThanOrEqual($phpNow, $b_transaction->updated_at);

      // Buyer Contribution
      $buyer = $contacts[$ai->buy->contact_key];
      $this->assertEquals($buyer->name, $b_contribution->name);
      $this->assertEquals($buyer->address, $b_contribution->address);
      $this->assertEquals($buyer->city, $b_contribution->city);
      $this->assertEquals($buyer->state, $b_contribution->state);
      $this->assertEquals($buyer->postcode, $b_contribution->postcode);
      if (!empty($buyer->employer)) {
        $this->assertEquals($buyer->employer->name, $b_contribution->employer);
        $this->assertEquals($buyer->employer->address ?? NULL, $b_contribution->employer_address);
        $this->assertEquals($buyer->employer->city ?? NULL, $b_contribution->employer_city);
        $this->assertEquals($buyer->employer->state ?? NULL, $b_contribution->employer_state);
        $this->assertEquals($buyer->employer->postcode ?? NULL, $b_contribution->employer_postcode);
      }

      $this->assertEquals($ai->buy->election, $b_contribution->prim_gen);
      $this->assertEquals($buyer->contributor_type, $b_contribution->contributor_type);

      // Buyer Sum
      $buyer_aggregate = $b_contribution->prim_gen == 'P' ? $buyer->aggregate_primary : $buyer->aggregate_general;
      $this->assertEquals($buyer_aggregate, $b_sum->aggregate_amount);
    }
  }

  protected function checkMiscellaneous($misc_records, Report $saved_report, $contacts, $phpNow) {
    // Find all of the transactions created by the report.
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id,'category' => 'contribution', 'act' => 'miscellaneous'], ['transaction_id' => 'asc']);
    $this->assertEquals(count($misc_records), count($transactions));
    foreach ($transactions as $i => $transaction) {
      $c = $misc_records[$i];
      $contact = $contacts[$c->contact_key];
      $this->assertEquals($c->amount, $transaction->amount);
      $this->assertEquals('miscellaneous', $transaction->act);
      $this->assertEquals($saved_report->fund_id, $transaction->fund_id);
      $this->assertEquals($c->date, $transaction->transaction_date->format('Y-m-d'));
      $this->assertGreaterThanOrEqual($phpNow, $transaction->updated_at->format('Y-m-d h:i:s'));
      $this->assertEquals(FALSE, $transaction->inkind);
      $this->assertEquals(TRUE, $transaction->itemized);

      /** @var Contribution $contribution */
      $contribution = $this->dm->em->find(Contribution::class, $transaction->transaction_id);
      $this->assertEquals($contact->name, $contribution->name);
      if (!empty($contact->address)) {
        $this->assertEquals($contact->address, $contribution->address );
        $this->assertEquals($contact->city, $contribution->city);
        $this->assertEquals($contact->state, $contribution->state);
        $this->assertEquals($contact->postcode, $contribution->postcode);
      }
      $this->assertEquals($c->contact_key, $contribution->contact_key);
      $this->assertEquals($contact->contributor_type, $contribution->contributor_type);
      $this->assertEquals(null, $contribution->prim_gen);

      if (!empty($contact->employer)) {
        $e = $contact->employer;
        $this->assertEquals($e->name, $contribution->employer);
        $this->assertEquals($e->city, $contribution->employer_city);
        $this->assertEquals($e->occupation, $contribution->occupation);
      }
    }
  }


  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function testSubmit() {
    $dir = $this->data_dir . "/fund";
    $test = Yaml::parseFile("$dir/deposit-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($test->committee);
    $fund = $this->createFund($test->fund, $committee->committee_id);
    $phpNow = (new DateTime())->format('Y-m-d h:i:s');
    $databaseNow = $this->dm->db->fetchFirstColumn("select to_char(now(), 'YYYY-MM-DD HH:MI:SS');")[0];
    $this->processor = new C3ReportProcessor($this->dm, $test->metadata->submission_version);
    $response = $this->processor->submitReport($test, $fund);

    $this->dm->em->clear();
    $this->assertTrue($response->success);
    $this->assertNotEmpty($response->messages);

    /** @var Report $saved_report */
    $saved_report = $this->dm->em->find(Report::class, $response->report_id);
    $this->assertInstanceOf(Report::class, $saved_report);
    $this->assertEquals('C3', $saved_report->report_type);
    $this->assertEquals($fund->fund_id, $saved_report->fund_id);
    $this->assertEquals($test->report->submitted_at, $saved_report->submitted_at->format('Y-m-d H:i:s'));
    $this->assertEquals($test->report->election_year, $saved_report->election_year);
    $this->assertEquals($test->report->treasurer->name, $saved_report->treasurer_name);
    $this->assertEquals($test->report->treasurer->date, $saved_report->treasurer_date->format('Y-m-d'));
    $this->assertEquals($test->report->period_start,$saved_report->period_start->format('Y-m-d'));
    $this->assertEquals($test->report->period_end, $saved_report->period_end->format('Y-m-d'));
    $this->assertEquals($test->report->committee->address,$saved_report->address1);
    $this->assertEquals($test->report->committee->city, $saved_report->city);
    $this->assertEquals($test->report->committee->state, $saved_report->state);
    $this->assertEquals($test->report->committee->postcode, $saved_report->postcode);
    $this->assertEquals($test->report->committee->name, $saved_report->filer_name);
    $this->assertEquals($test->report->external_id, $saved_report->external_id);
    //checking submission version and vendor values
    $this->assertEquals($test->metadata->submission_version, $saved_report->version);
    $this->assertEquals($test->metadata->vendor_name, $saved_report->source);

    $this->assertProperties($fund->fund_id, $test->report->properties);

    $this->assertNotEmpty($saved_report->user_data);
    $this->assertNotEmpty($saved_report->metadata);


    $contacts = [];
    foreach ($test->report->contacts as $contact) {
      $contacts[$contact->contact_key] = $contact;
    }

    if (!empty($test->report->contributions)) {
      $this->checkContributions($test->report->contributions, $saved_report, $contacts, $databaseNow);
    }
    if (!empty($test->report->auctions)) {
      $this->checkAuctions($test->report->auctions, $saved_report, $contacts, $phpNow);
    }
    if (!empty($test->report->loans)) {
      $this->checkLoans($test->report->loans, $saved_report, $contacts);
    }
    if (!empty($test->report->misc)) {
      $this->checkMiscellaneous($test->report->misc, $saved_report, $contacts, $phpNow);
    }
    if (!empty($test->sums)) {
      $this->checkSums($test->sums, $saved_report);
    }
  }

  /**
   * @return array
   */
  public function depositReportProvider() {
    return [
      ['deposit-pac'], ['deposit-candidate']
    ];
  }

  /**
   * @dataProvider depositReportProvider
   * @throws Exception
   */
  public function testSubmitContributionReport($scenario) {
    $dir = $this->data_dir . "/fund";
    $validations = Yaml::parseFile("$dir/$scenario.validations.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Object has tests property which contains an array of test condition.
    foreach($validations->tests as $validation_test) {
      // Start with the same initial data.
      $test = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
      $expected_errors = [];
      $expected_messages = [];

      // Each condition either an assertion of an error or and assertion of a particular initial state modified from
      // the base $submission
      foreach($validation_test as $condition => $value) {
        switch (true) {
          case (strpos($condition, '.') !== FALSE):
            $this->setProperty($test, $condition, $value);
            break;
          case ($condition == 'error'):
            if (is_array($value)) {
              foreach ($value as $error) {
                $expected_errors[] = $error;
              }
            } else {
              $expected_errors[] = $value;
            }
            break;
          case ($condition == 'message'):
            if (is_array($value)) {
              foreach ($value as $message) {
                $expected_messages[] = $message;
              }
            } else {
              $expected_messages[] = $value;
            }
            break;
        }
      }
      $committee = $this->createCommittee($test->committee);
      $fund = $this->createFund($test->fund, $committee->committee_id);
      if (isset($test->candidacy)) {
        $candidacy = $this->createPersonAndCandidacy($test->candidacy, $committee->committee_id);
      }
      $now = (new DateTime())->format('Y-m-d h:i:s');
      $response = $this->processor->submitReport($test, $fund);

      $this->dm->em->clear();
      if ($expected_errors && $expected_errors != ['']){
        $this->assertFalse($response->success, "$scenario: $value");
        foreach ($expected_errors as $expected_error) {
          $this->assertArrayHasKey($expected_error, $response->messages, "Missing expected errors for $scenario: $condition");
        }
      }
      else {
        $this->assertTrue($response->success);
        $this->assertNotEmpty($response->report_id);

        // Check for expected messages.
        if ($expected_messages && $expected_messages != ['']){
          foreach ($expected_messages as $expected_message) {
            $this->assertArrayHasKey($expected_message, $response->messages, "Missing expected messages for $scenario: $condition");
          }
        }

        /** @var Report $saved_report */
        $saved_report = $this->dm->em->find(Report::class, $response->report_id);
        $this->assertInstanceOf(Report::class, $saved_report);
        $this->assertEquals('C3', $saved_report->report_type);
        $this->assertEquals($fund->fund_id, $saved_report->fund_id);
        $this->assertEquals($test->report->submitted_at, $saved_report->submitted_at->format('Y-m-d H:i:s'));
        $this->assertEquals($test->report->election_year, $saved_report->election_year);
        $this->assertEquals($test->report->treasurer->name, $saved_report->treasurer_name);
        $this->assertEquals($test->report->treasurer->date, $saved_report->treasurer_date->format('Y-m-d'));
        $this->assertEquals($test->report->period_start, $saved_report->period_start->format('Y-m-d'));
        $this->assertEquals($test->report->period_end, $saved_report->period_end->format('Y-m-d'));
        $this->assertEquals($test->report->committee->address, $saved_report->address1);
        $this->assertEquals($test->report->committee->city, $saved_report->city);
        $this->assertEquals($test->report->committee->state, $saved_report->state);
        $this->assertEquals($test->report->committee->postcode, $saved_report->postcode);
        $this->assertEquals($test->report->external_id, $saved_report->external_id);

      }
      /** The item tally is correct and that there are receipts for 1B, 1C and 1D. (Verify this is desirable. What is item tally?)
       * If aggregate is over $25, contributor address exists.
       * If aggregate is over $100 per individual or over $200 for per couple, contributor occupation, employer and employer address exist.
       * Sum lines for candidate's personal funds (1B), loans (1C), misc + auctions (1D) match the sum of their transactions.
       * Sum line for contributions (C3.2) matches the sum of those transactions.
       * Corrected amount exists for all corrections.
       * For loan transactions, loan date and due date exist.
       * check for duplicates for sums
       * make sure the sum amounts are valid (in what way?)
       * Dates are valid and not more than 30 years in the future for filing date, report period start, report period end, and "MON/PARM1 DATE"
       * Amount code must be P (primary) or G (general).
       */
    }
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  protected function checkSums($sums, Report $saved_report) {
    $sql =
      "
       select t.transaction_id, t.transaction_date, t.amount, s.aggregate_amount, s.tx_count, upper(s.legacy_line_item) as legacy_line_item, c.contact_key as contact_key, c.prim_gen
       from report_sum s join transaction t on t.transaction_id = s.transaction_id
       left join contribution c on c.transaction_id = t.transaction_id
       where t.report_id = ? and  t.act in ('sum')
      ";
    $stmt = $this->dm->em->getConnection()->prepare($sql);
    $stmt->bindValue(1, $saved_report->report_id);
    $rows = $stmt->executeQuery()->fetchAllAssociative();
    $this->assertEquals(count(get_object_vars($sums)), count($rows));
    foreach ($rows as $row) {
      switch ($row['legacy_line_item']) {
        case '1A':
          $this->assertEquals($sums->anonymous->amount, (float)$row['amount']);
          $this->assertEquals((new DateTime($sums->anonymous->date))->format('Y-m-d'), $row['transaction_date']);
          $this->assertEquals($sums->anonymous->aggregate, (float)$row['aggregate_amount']);
          break;
        case '1B':
          $this->assertEquals($sums->personal->amount, (float)$row['amount']);
          $this->assertEquals((new DateTime($sums->personal->date))->format('Y-m-d'), $row['transaction_date']);
          $this->assertEquals($sums->personal->aggregate, (float)$row['aggregate_amount']);
          break;
        case '1C':
          $this->assertEquals($sums->loans->amount, (float)$row['amount']);
          if ($sums->loans->date) {
            $this->assertEquals((new Datetime($sums->loans->date))->format('Y-m-d'), null);
          } else {
            self::assertEmpty($row['transaction_date']);
          }
          $this->assertEquals($sums->loans->count, (float)$row['tx_count']);
          break;
        case '1D':
          $this->assertEquals($sums->misc_auction->amount, (float)$row['amount']);
          if ($sums->misc_auction->date)
          {
            $this->assertEquals((new Datetime($sums->misc_auction->date))->format('Y-m-d'), null);
          } else {
            self::assertEmpty($row['transaction_date']);
          }
          $this->assertEquals($sums->misc_auction->count, (float)$row['tx_count']);
          break;
        case '1E':
          $this->assertEquals($sums->small->amount, (float)$row['amount']);
          $this->assertEquals((new DateTime($sums->small->date))->format('Y-m-d'), $row['transaction_date']);
          $this->assertEquals($sums->small->count, (int)$row['tx_count']);
          break;
        case '2';
          $this->assertEquals($sums->itemized_contributions->amount, (float)$row['amount']);
          if ($sums->itemized_contributions->date) {
            $this->assertEquals((new DateTime($sums->itemized_contributions->date))->format('Y-m-d'), null);
          } else {
            self::assertEmpty($row['transaction_date']);
          }
          $this->assertEquals($sums->itemized_contributions->count, (float)$row['tx_count']);
      }
    }
  }
}