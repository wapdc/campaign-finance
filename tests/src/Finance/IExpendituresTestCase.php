<?php


namespace Tests\WAPDC\CampaignFinance\Finance;


use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use stdClass;
use Symfony\Component\Yaml\Yaml;
use PHPUnit\Framework\TestCase;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\IEProcessor;
use WAPDC\CampaignFinance\Model\Sponsor;
use WAPDC\Core\Model\Entity;
use WAPDC\Core\SendGrid\Mail;
use WAPDC\Core\SendGrid\NoCMS;

/**
 * Class IExpendituresTestCase
 *
 */
class IExpendituresTestCase extends TestCase
{

  /** @var CFDataManager */
  protected $dm;

  /** @var Sponsor */
  protected $sponsor;

  /** @var IEProcessor */
  protected $ieprocessor;
  
  /** @var string */
  protected $data_dir;

  public $entity;

  public $legacy;

  private $processor;

  /**
   * @throws Exception
   */
  protected function setUp(): void
  {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->data_dir = dirname(dirname(__DIR__)) . '/data/verification-options';
  }

  protected function tearDown(): void
  {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

  /**
   * @throws OptimisticLockException
   * @throws ORMException
   */
  public function createEntityAndSponsor(stdClass $sponsor_data, $sponsor_id) {
    $this->entity = new Entity();
    $this->entity->name = "Test Entity";
    $this->entity->address = "1234 entitystreet";
    $this->entity->city = "EntityTown";
    $this->entity->state = "En";
    $this->entity->postcode = "99999";
    $this->entity->is_person = "true";

    $this->dm->em->persist($this->entity);
    $this->dm->em->flush();

    $sponsor = new Entity();
    $sponsor->name = $sponsor_data->name;
    $sponsor->is_person = $sponsor_data->is_person;
    $sponsor->entity_id = $this->entity->entity_id;

    $sponsor->sponsor_id = $sponsor_id;
    $sponsor->sponsor_name = $sponsor_data->name ?? null;

    $this->dm->em->persist($sponsor);
    $this->dm->em->flush($sponsor);
    return $sponsor;
  }




}