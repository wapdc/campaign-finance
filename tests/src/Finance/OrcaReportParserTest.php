<?php


namespace Tests\WAPDC\CampaignFinance\Finance;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\Reporting\OrcaReportParser;
use Exception;
use stdClass;

class OrcaReportParserTest extends TestCase {

  /** @var OrcaReportParser */
  protected $parser;

  protected $data_dir;

  protected function setUp(): void {
    parent::setUp();
    $this->parser = new OrcaReportParser();
    $this->data_dir = dirname(dirname(__DIR__)) . "/data";
  }


  public function preprocessorProvider() {
    $filenames = [
      ['C4-msword-copy-paste'],
      ['C3-invalid-phone-format'],
      ['C3-invalid-zipcode-format'],
      ['C3-middle-name-too-long'],
      ['C4-missing-phone'],
    ];
    return $filenames;
  }

  public function testMalformedXml($scenario = 'malformed-c3'){

    $dir = dirname(dirname(__DIR__)) . '/data/orca_parser';
    $xrf = file_get_contents("$dir/$scenario.xml");

    $this->expectException(Exception::class);
    $report = $this->parser->parse($xrf);
    $this->assertInstanceOf(stdClass::class, $report);
  }

  public function testSchemaConstraintViolation($scenario = 'c3-schema-constraint-violation'){

    $dir = dirname(dirname(__DIR__)) . '/data/orca_parser';
    $xrf = file_get_contents("$dir/$scenario.xml");

    $this->expectException(Exception::class);
    $this->expectExceptionCode($this->parser::SCHEMA_PARSER_EXCEPTION_CODE);
    $report = $this->parser->parse($xrf);
    $this->assertInstanceOf(stdClass::class, $report);
  }


  public function testInvalidSchema($scenario = 'unparsable-schema-c4'){

    $dir = dirname(dirname(__DIR__)) . '/data/orca_parser';
    $xrf = file_get_contents("$dir/$scenario.xml");

    $this->expectException(Exception::class);
    $report = $this->parser->parse($xrf);
    $this->assertInstanceOf(stdClass::class, $report);
  }

  public function testProcessor() {
    $this->assertInstanceOf(OrcaReportParser::class, $this->parser);
  }

  public function testReportParser() {
    $dir = $this->data_dir . "/orca_parser";
    // c4
    $xml = file_get_contents("$dir/simple-c4.xml");
    $report = $this->parser->parse($xml);
    $this->assertInstanceOf(stdClass::class, $report);
    // c3
    $xml = file_get_contents("$dir/simple-c3.xml");
    $report = $this->parser->parse($xml);
    $this->assertInstanceOf(stdClass::class, $report);
  }

  protected function assertExpectedData($a, $b, $message) {
    foreach ($a as $key => $value) {
      switch (TRUE) {
        case (is_object($value)):
          $this->assertExpectedData($value, $b->$key, "$message:$key");
          break;
        case (is_array($value)):
          foreach($value as $k => $v) {
            $this->assertExpectedData($v, $b->$key[$k] ?? NULL, "$message:$key:$k");
          }
          break;
        case ($b === NULL):
          $this->assertIsObject($b, "$message:$key no object");
          break;
        default:
          $this->assertObjectHasAttribute($key, $b, "$message:$key");
          $this->assertSame($value, $b->$key, "$message:$key");
      }
    }
  }

  public function testParseC4Report() {
    // Report header information.
    $dir = $this->data_dir . "/orca_parser";
    $xml = file_get_contents("$dir/simple-c4.xml");
    $expected_report = Yaml::parseFile("$dir/simple-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $report = $this->parser->parse($xml);
    $this->assertExpectedData($expected_report->report, $report, "Report");
  }

  public function ParseProvider() {
    return [
      ['c3-complex'],
      ['c4-expense'],
      ['c3-auction'],
      ['simple-c3'],
      ['C3-loan'],
      ['c4-inkind-contribution'],
      ['c4-correction'],
      ['c4-loans'],
      ['c4-pledge'],
      ['c4-line-items'],
      ['c4-debt'],
      ['c4-ccdebt'],
      ['c4-debt-subdebt']
    ];
  }

  /**
   * @dataProvider ParseProvider
   * @param $scenario
   * @throws Exception
   */
  public function testParseReport($scenario) {
    $dir = $this->data_dir . "/orca_parser";
    $xml = file_get_contents("$dir/$scenario.xml");
    $expected_report = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $report = $this->parser->parse($xml);
    $this->assertExpectedData($expected_report->report, $report, "Scenario: $scenario");
  }
}