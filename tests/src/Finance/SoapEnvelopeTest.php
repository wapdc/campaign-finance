<?php


namespace Tests\WAPDC\CampaignFinance\Finance;


use PHPUnit\Framework\TestCase;
use WAPDC\CampaignFinance\SoapEnvelope;

class SoapEnvelopeTest extends TestCase {

  public function getSoapResult($xml) {
    $xmlDoc = new \SimpleXMLElement($xml);
    $xmlDoc->registerXPathNamespace('soap12', SoapEnvelope::SOAP12_NS);
    $resultNodes = $xmlDoc->xpath('//soap12:Body');
    $result = (string) $resultNodes[0]->SubmitResponse->SubmitResult;
    return $result;
  }

  public function testBuildResponse() {
    $envelope = new SoapEnvelope('Submit');

    $envelope->setSubmitResult('Hello World');
    $xml = $envelope->asXML();
    $this->assertNotEmpty($xml);
    $this->assertEquals('Hello World', $this->getSoapResult($xml));

    $envelope->setSubmitResult('<foo>Hello World</foo>');
    $xml = $envelope->asXML();
    $this->assertEquals('<foo>Hello World</foo>', $this->getSoapResult($xml));
  }

  public function testSetErrorResults() {
    $envelope = new SoapEnvelope('Submit');
    $envelope->setError('An error has occurred', 'DuplicateReportException');
    $xml = $envelope->asXML();
    $result = $this->getSoapResult($xml);
    $errorXml = new \SimpleXMLElement($result);
    $message = (string) $errorXml->Message;
    $this->assertEquals('An error has occurred', $message);
    $this->assertNotEmpty($errorXml->Exception);
    $class = (string) $errorXml->Exception['class'];
    $this->assertEquals('DuplicateReportException', $class);
    $time = (string) $errorXml->Exception['time'];
    $this->assertNotEmpty($time);
  }

  public function testSetResponse() {
    $envelope = new SoapEnvelope('Submit');
    $response = ['Repno' => 42, 'Message' => 'Quit trimming your NAILS!'];
    $envelope->setResponse($response);
    $xml = $envelope->asXML();
    $result = $this->getSoapResult($xml);
    $responseXML = new \SimpleXMLElement($result);
    $report_id = (string) $responseXML->Repno;
    $this->assertEquals(42, $report_id);
    $message = (string) $responseXML->Message;
    $this->assertEquals('Quit trimming your NAILS!', $message);
  }
}