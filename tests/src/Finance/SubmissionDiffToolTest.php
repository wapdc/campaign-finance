<?php


namespace Tests\WAPDC\CampaignFinance\Finance;


use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\SubmissionDiffTool;

class SubmissionDiffToolTest extends FinanceTestCase
{

  public function compareSubmissionProvider() {
    return [
      ['same'],
      ['different'],
      ['reduced'],
    ];
  }

  /**
   * @dataProvider compareSubmissionProvider
   * @param string $scenario
   */
  public function testCompareSubmission($scenario = 'different') {
    $data_dir = dirname(dirname(__DIR__)) . "/data/compare";
    $test = Yaml::parseFile("$data_dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $tool = new SubmissionDiffTool();

    $same = $tool->compareSubmissions($test->amendment, $test->report);

    $this->assertExpectedData($test->comparison, $test->amendment, "$scenario");

    $this->assertSame($test->is_same, $same);

    $this->assertSame($test->changes, count($tool->changeLog()));
  }

  public function testGetData() {
    $data_dir = dirname(dirname(__DIR__)) . "/data/compare";
    $test = Yaml::parseFile("$data_dir/different.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $tool = new SubmissionDiffTool();

    $data = $tool->changedData('report.contributions.2', $test->amendment);
    $this->assertEquals($test->amendment->contributions[2], $data);
  }

}