<?php


namespace Tests\WAPDC\CampaignFinance\Finance;


use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\Validator;

class ValidatorTest extends FinanceTestCase {

  /** @var Validator */
  protected $validator;
  protected function setUp(): void {
    parent::setUp();
    $this->validator = new Validator($this->dm);
  }

  public function testValidator() {
    $this->assertInstanceOf(Validator::class, $this->validator);
  }

  public function testMessageAPI() {
    $this->assertTrue($this->validator->isValid());
    $this->validator->addMessage("success", "Test Message");
    $this->assertTrue($this->validator->isValid());
    $this->validator->addError('error', "An error occurred.");
    $this->assertFalse($this->validator->isValid());
    $messages = $this->validator->getMessages();
    $this->assertEquals("Test Message", $messages['success']);
    $this->assertEquals("An error occurred.", $messages['error']);

    // Format as text
    $this->assertEquals("Test Message\nAn error occurred.\n", $this->validator->getMessageText());
  }

  public function testValidateDate() {
    $dir = $this->data_dir . "/fund";
    $validations = Yaml::parseFile("$dir/generic.data.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $data = $validations->data;

    $this->validator->validateDate($data->a_date, 'a_date', "A Date");
    $this->assertTrue($this->validator->isValid());

    $this->validator->clear();
    $data->a_date = "foo";
    $this->validator->validateDate($data->a_date, "a_date", "A Date is invalid ");
    $messages = $this->validator->getMessages();
    $this->assertArrayHasKey('a_date.invalid', $messages);
  }

  public function testValidateRequired() {
    $dir = $this->data_dir . "/fund";
    $validations = Yaml::parseFile("$dir/generic.data.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $data = $validations->data;

    $this->validator->validateRequired($data, ['a_date'], 'submission', "Submission");
    $this->assertTrue($this->validator->isValid());

    $this->validator->clear();
    $data->a_date = "";
    $this->validator->validateRequired($data, ['a_date'], 'submission', "Submission");
    $messages = $this->validator->getMessages();
    $this->assertArrayHasKey('submission.incomplete', $messages);
  }

  public function testValidateList() {
    $dir = $this->data_dir . "/fund";
    $validations = Yaml::parseFile("$dir/generic.data.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $data = $validations->data;
    $this->validator->validateList($data, 'report_type', ['c3', 'c4'], "submission", "Report Type");
    $this->assertTrue($this->validator->isValid());

    $this->validator->clear();

    $data->report_type="foo";
    $this->validator->validateList($data, 'report_type', ['c3', 'c4'], "submission", "Report Type");
    $this->assertFalse($this->validator->isValid());
    $messages = $this->validator->getMessages();
    $this->assertArrayHasKey('submission.report_type.invalid', $messages);
  }

  public function testValidateMoney() {
    $dir = $this->data_dir . "/fund";
    $validations = Yaml::parseFile("$dir/generic.data.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $data = $validations->data;

    $this->validator->validateMoney($data->money, 'amount', "Amount");
    $this->assertTrue($this->validator->isValid());

    $this->validator->clear();
    $data->money = "foo";
    $this->validator->validateMoney($data->money, "amount", "Amount is invalid ");
    $messages = $this->validator->getMessages();
    $this->assertArrayHasKey('amount.invalid', $messages);
  }

}