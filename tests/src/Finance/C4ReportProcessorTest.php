<?php


namespace Tests\WAPDC\CampaignFinance\Finance;


use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\TransactionRequiredException;
use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\Model\Contribution;
use WAPDC\CampaignFinance\Model\Correction;
use WAPDC\CampaignFinance\Model\Expense;
use WAPDC\CampaignFinance\Model\LoanPayment;
use WAPDC\CampaignFinance\Model\Report;
use WAPDC\CampaignFinance\Model\Sum;
use WAPDC\CampaignFinance\Model\Transaction;
use WAPDC\CampaignFinance\Reporting\C4ReportProcessor;

class C4ReportProcessorTest extends FinanceTestCase {

  /** @var C4ReportProcessor */
  protected $processor;

  protected function setUp(): void {
    parent::setUp();
    $this->processor = new C4ReportProcessor($this->dm, 1.1);
  }

  public function testProcessor() {
    $this->assertInstanceOf(C4ReportProcessor::class, $this->processor);
  }

  /**
   * @throws ORMException
   * @throws \Doctrine\ORM\ORMException
   * @throws Exception
   * @throws OptimisticLockException
   * @throws TransactionRequiredException
   * @throws \Doctrine\DBAL\Exception
   * @throws \Exception
   */
  public function testSubmit() {
    $dir = $this->data_dir . "/fund";
    $test = Yaml::parseFile("$dir/candidate-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createFullCommittee($test);
    $fund = $this->createFund($test->fund, $committee->committee_id);
    $this->processor = new C4ReportProcessor($this->dm, $test->metadata->submission_version);
    $response = $this->processor->submitReport($test, $fund);
    $this->assertTrue($response->success);
    $this->assertNotEmpty($response->messages);

    /** @var Report $saved_report */
    $saved_report = $this->dm->em->find(Report::class, $response->report_id);

    $this->assertInstanceOf(Report::class, $saved_report);
    $this->assertEquals('C4', $saved_report->report_type);
    $this->assertEquals($fund->fund_id, $saved_report->fund_id);
    $this->assertEquals($test->report->submitted_at, $saved_report->submitted_at->format('Y-m-d'));
    $this->assertEquals($test->report->election_year, $saved_report->election_year);
    $this->assertEquals($test->report->treasurer->name, $saved_report->treasurer_name);
    $this->assertEquals($test->report->treasurer->date, $saved_report->treasurer_date->format('Y-m-d'));
    $this->assertEquals($test->report->period_start, $saved_report->period_start->format('Y-m-d'));
    $this->assertEquals($test->report->period_end, $saved_report->period_end->format('Y-m-d'));
    $this->assertEquals($test->report->committee->address, $saved_report->address1);
    $this->assertEquals($test->report->committee->city, $saved_report->city);
    $this->assertEquals($test->report->committee->state, $saved_report->state);
    $this->assertEquals($test->report->committee->postcode, $saved_report->postcode);
    $this->assertEquals($test->report->external_id, $saved_report->external_id);
    $this->assertEquals($test->metadata->submission_version, $saved_report->version);
    $this->assertEquals($test->metadata->vendor_name, $saved_report->source);

    $this->assertProperties($fund->fund_id, $test->report->properties);

    $this->assertNotEmpty($saved_report->user_data);
    $this->assertNotEmpty($saved_report->metadata);

    $contacts = [];
    foreach ($test->report->contacts as $contact) {
      $contacts[$contact->contact_key] = $contact;
    }

    if (!empty($test->report->misc_receipts)) {
      $this->checkMiscReceipts($test->report->misc_receipts, $saved_report);
    }

    if (!empty($test->report->pledge)) {
      $this->checkPledges($test->report->pledge, $saved_report, $contacts);
    }

    if (!empty($test->report->debt)) {
      $this->checkDebt($test->report->debt, $saved_report, $contacts);
    }

    if (!empty($test->report->sums->itemized_deposits)) {
      $this->checkDepositSum($test->report->sums->itemized_deposits, $saved_report);
    }

    if(!empty($test->report->expenses)) {
      $this->checkExpenses($test->report->expenses, $saved_report, $contacts);
    }

    if (!empty($test->report->contribution_corrections)) {
      $this->checkExpenseCorrections($test->report->contribution_corrections, 'contribution', $saved_report, $contacts);
    }

    if (!empty($test->report->expense_corrections)) {
      $this->checkExpenseCorrections($test->report->expense_corrections, 'expense', $saved_report, $contacts);
    }

    if (!empty($test->report->expense_refunds)) {
      $this->checkExpenseRefunds($test->report->expense_refunds, $saved_report, $contacts);
    }

    if (!empty($test->report->loan)) {
      if (!empty($test->report->loan->receipts)) {
        $loanCarryForward = $this->dm->db->executeQuery("select * from wapdc.public.transaction where report_id = :report_id and category = 'loan' and act = 'carry_forward'", ['report_id' => $saved_report->report_id])->fetchAllAssociative();
        $this->assertNotEmpty($loanCarryForward);
        $this->checkLoans($test->report->loan->receipts, $saved_report, $contacts);
      }
      if (!empty($test->report->loan->payments)) {
        $this->checkLoanPayments($test->report->loan->payments, $saved_report, $contacts);
      }

      if (!empty($test->report->loan->balances)) {
        $this->checkLoanBalances($test->report->loan->balances, $saved_report, $contacts);
      }
    }

    if( !empty($test->report->contributions_inkind)) {
          $this->checkInKindContributions($test->report->contributions_inkind, $saved_report, $contacts);
    }

    if (!empty($test->report->debt)) {
      $this->checkDebt($test->report->debt, $saved_report, $contacts);
    }

    $this->checkSumItems($test->report->sums, $saved_report);

    $this->checkODSummaryTotals($committee, $test);


  }

  /**
   * @param $pledges
   * @param \WAPDC\CampaignFinance\Model\Report $saved_report
   * @param $contacts
   */
  public function checkPledges($pledges, Report $saved_report, $contacts) {
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id, 'category' => 'pledge', 'act' => 'receipt' ], ['transaction_id' => 'asc']);

    $this->assertEquals(count($pledges), count($transactions));
    foreach ($pledges as $p => $pledge) {
      $transaction = $transactions[$p];

      /** @var Contribution $contact */
      $contact = $this->dm->em->find(Contribution::class, $transaction->transaction_id);
      $c = $contacts[$pledge->contact_key];

      $this->assertEquals($transaction->amount, $pledge->amount );
      $this->assertEquals($transaction->transaction_date->format('Y-m-d'), $pledge->date);
      $this->assertEquals($c->contact_key, $contact->contact_key);
      $this->assertEquals($c->name, $contact->name);
      $this->assertEquals($c->address ?? null, $contact->address);
      $this->assertEquals($pledge->election, $contact->prim_gen);

      /** @var Sum $sum */
      $sum = $this->dm->em->find(Sum::class, $transaction->transaction_id);
      $this->assertEquals($sum->aggregate_amount, $contact->prim_gen === 'P' ? $c->aggregate_primary : $c->aggregate_general);

    }

  }

  public function checkInKindContributions($contributions_inkind, Report $saved_report, $contacts) {
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id, 'category' => 'contribution', 'act' => 'receipt', 'inkind' => TRUE ], ['transaction_id' => 'asc']);

    $this->assertEquals(count($contributions_inkind), count($transactions));

    foreach ($contributions_inkind as $c => $contribution) {
      $transaction = $transactions[$c];

      /** @var Contribution $contributions */
      $db_contact = $this->dm->em->find(Contribution::class, $transaction->transaction_id);
      $contact = $contacts[$contribution->contact_key];
      $this->assertEquals(1, $transaction->expense_type);
      $this->assertEquals($transaction->transaction_date->format('Y-m-d'), $contribution->date);
      $this->assertEquals($transaction->description, $contribution->description);
      $this->assertEquals(TRUE,$transaction->itemized);
      $this->assertEquals($saved_report->fund_id, $transaction->fund_id);
      $this->assertTrue($transaction->inkind);
      $this->assertEquals($transaction->amount, $contribution->amount );
      $this->assertEquals($contact->contact_key, $db_contact->contact_key);
      $this->assertEquals($contact->name, $db_contact->name);
      $this->assertEquals($contact->address ?? null, $db_contact->address);

      $this->assertEquals($db_contact->prim_gen, $contribution->election);

      if (!empty($contact->employer)) {
        $this->assertEquals($contact->employer->name, $db_contact->employer);
        $this->assertEquals($contact->employer->city, $db_contact->employer_city);
        $this->assertEquals($contact->employer->state, $db_contact->employer_state);
        $this->assertEquals($contact->employer->occupation, $db_contact->occupation);
      }

      /** @var Sum $sum */
      $sum = $this->dm->em->find(Sum::class, $transaction->transaction_id);
      $this->assertEquals($sum->aggregate_amount, $db_contact->prim_gen === 'P' ? $contact->aggregate_primary : $contact->aggregate_general);
    }
}

  /**
   * @param $misc_receipts
   * @param \WAPDC\CampaignFinance\Model\Report $saved_report
   * @throws Exception
   */
  public function checkMiscReceipts($misc_receipts, Report $saved_report) {
    $query = $this->dm->db->executeQuery("SELECT * from transaction t JOIN report_sum rs on t.transaction_id = rs.transaction_id WHERE t.report_id = $saved_report->report_id
    AND rs.legacy_line_item LIKE '%2.A1%' ORDER BY t.transaction_id");
    $query_result = $query->fetchAllAssociative();
    $this->assertNotEmpty($query_result);
    foreach ($query_result as $i => $transaction) {
      $ms = $misc_receipts[$i];
      $this->assertEquals($ms->date, $transaction['transaction_date']);
      $this->assertEquals($ms->amount, $transaction['amount']);
    }
  }

  /**
   * @throws OptimisticLockException
   * @throws ORMException
   * @throws Exception
   * @throws TransactionRequiredException
   * @throws \Doctrine\DBAL\Exception
   */
  public function checkDepositSum($itemized_deposit, $saved_report) {
    $sum_query = $this->dm->db->executeQuery("SELECT * from transaction t JOIN report_sum rs on t.transaction_id = rs.transaction_id WHERE t.report_id = $saved_report->report_id
    AND rs.legacy_line_item = '2'");
    $query_result = $sum_query->fetchAllAssociative();
    $this->assertNotEmpty($query_result);
    $this->assertEquals($query_result[0]['amount'], $itemized_deposit->amount);

    /** @var Sum $sum */
    $sum= $this->dm->em->find(Sum::class, $query_result[0]['transaction_id']);
    $this->assertEquals($itemized_deposit->count, $sum->tx_count);
    $this->assertEquals($itemized_deposit->amount, $sum->aggregate_amount);

  }

  /**
   * @param $debts
   * @param Report $saved_report
   * @param $contacts
   */
  public function checkDebt($debts, Report $saved_report, $contacts) {
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id, 'category' => 'expense', 'act' => 'invoice'], ['transaction_id' => 'asc']);

    $this->assertEquals(count($debts), count($transactions));
    foreach($debts as $d => $debt) {
      $transaction = $transactions[$d];

      /** @var Expense $expense */
      $expense = $this->dm->em->find(Expense::class, $transaction->transaction_id);
      $c = $contacts[$debt->contact_key];

      $this->assertEquals($transaction->amount, $debt->amount);
      $this->assertEquals($transaction->transaction_date->format('Y-m-d'), $debt->date);
      $this->assertEquals($c->contact_key, $expense->contact_key);
      $this->assertEquals($c->name, $expense->name);
      $this->assertEquals($c->address ?? null, $expense->address);
      $this->assertEquals($debt->category, $expense->category);

    }
  }

  public function testAmendments() {
    $dir = $this->data_dir . "/fund";
    $test = Yaml::parseFile("$dir/candidate-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($test->committee);
    $fund = $this->createFund($test->fund, $committee->committee_id);
    $test->report->amends = -1;
    if (!empty($test->candidacy)) {
      $this->createPersonAndCandidacy($test->candidacy, $committee->committee_id);
    }
    $response = $this->processor->submitReport($test, $fund);
    $this->assertNotEmpty($response->report_id, "Can file report with amendno of -1");

    $original_report_id = $response->report_id;

    // Amendment successfully filed amendment
    $test->report->amends = $original_report_id;
    $response = $this->processor->submitReport($test, $fund);
    $this->assertTrue($response->success);
    /** @var Report $original_report */
    $original_report = $this->dm->em->find(Report::class, $original_report_id);
    $this->assertEquals($response->report_id, $original_report->superseded_id);

    // Make sure we validate that report has already been filed.
    $response = $this->processor->submitReport($test, $fund);
    $this->assertFalse($response->success);
    $this->assertArrayHasKey('amendment.report-already-amended', $response->messages);


    // Make sure we cannot amend and existing report.
    $test->report->amends = -999;
    $response = $this->processor->submitReport($test, $fund);
    $this->assertFalse($response->success);
    $this->assertArrayHasKey('amendment.missing-report', $response->messages);

    // wrong fund
    $this->dm->em->clear();
    $this->dm->db->executeQuery('update report set superseded_id = null, fund_id = 4 where report_id = :report_id', ['report_id' => $original_report_id]);
    $test->report->amends = $original_report_id;
    $response = $this->processor->submitReport($test, $fund);
    $this->assertArrayHasKey('amendment.wrong-fund', $response->messages);
    $this->assertFalse($response->success);
  }

  public function testDuplicateReport() {
    $dir = $this->data_dir . "/fund";
    $test = Yaml::parseFile("$dir/candidate-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($test->committee);
    $fund = $this->createFund($test->fund, $committee->committee_id);
    if (!empty($test->candidacy)) {
      $this->createPersonAndCandidacy($test->candidacy, $committee->committee_id);
    }
    $this->processor->submitReport($test, $fund);

    // file a second time to make sure this should fail.
    $response = $this->processor->submitReport($test, $fund);
    $this->assertFalse($response->success);
    $this->assertArrayHasKey('duplicate-report', $response->messages);
  }

  public function validationProvider() {
    return [['pac-c4'], ['candidate-c4']];
  }

  /**
   * @dataProvider validationProvider
   *
   * @param string $scenario
   *
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testCommonReportValidations($scenario = 'candidate-c4') {
    $dir = $this->data_dir . "/fund";
    // Load validation file.
    $validations = Yaml::parseFile("$dir/$scenario.validations.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Object has tests property which contains an array of test condition.
    foreach ($validations->tests as $validation_test) {
      // Start with the same initial data.
      $test = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
      $expected_errors = [];

      // Each condition either an assertion of an error or and assertion of a particular initial state modified from
      // the base $submission
      foreach ($validation_test as $condition => $value) {
        switch (TRUE) {
          case (strpos($condition, '.') !== FALSE):
            $this->setProperty($test, $condition, $value);
            break;
          case ($condition == 'error'):
            if (is_array($value)) {
              foreach ($value as $error) {
                $expected_errors[] = $error;
              }
            } else {
              $expected_errors[] = $value;
            }
            break;
        }
      }
      $committee = $this->createCommittee($test->committee);
      $fund = $this->createFund($test->fund, $committee->committee_id);
      if (!empty($test->candidacy)) {
        $this->createPersonAndCandidacy($test->candidacy, $committee->committee_id);
      }
      $response = $this->processor->submitReport($test, $fund);

      if ($expected_errors && $expected_errors != ['']) {
        $this->assertFalse($response->success, "$scenario:$value");
        foreach ($expected_errors as $expected_error) {
          $this->assertArrayHasKey($expected_error, $response->messages, "Missing expected errors for $scenario: $condition");
        }
      } else {
        $this->assertTrue($response->success, "$scenario");
        $this->assertNotEmpty($response->report_id);
        $this->assertNotEmpty($response->fund_id);
      }
    }
  }

  protected function checkExpenses($expenses, Report $saved_report, $contacts) {

    // Cash expenses
    $cash_expenses = array_values(array_filter($expenses, function ($e) {return empty($e->credit);}));
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy([
        'report_id' => $saved_report->report_id,
        'category' => 'expense',
        'act' => 'payment',
        'legacy_form_type'=> 'A/GT50'
      ], ['transaction_id' => 'asc']);
    $this->assertEquals(count($cash_expenses), count($transactions));
    foreach ($transactions as $i => $transaction) {
      $cash_expense = $cash_expenses[$i];
      $this->assertNotEmpty($transaction->transaction_id);
      $this->assertEquals($cash_expense->date, $transaction->transaction_date->format('Y-m-d'));
      $this->assertEquals($cash_expense->amount, $transaction->amount);
      $this->assertEquals($cash_expense->description,$transaction->description);
      $this->assertEquals($transaction->report_id, $saved_report->report_id );
      $this->assertEquals($transaction->fund_id, $saved_report->fund_id);
      $payment_category = $cash_expense->payment_category ?? null;
      if (! empty($payment_category)) {
        $this->assertEquals(0, $transaction->expense_type);
      }
      else {
        $this->assertEquals(1, $transaction->expense_type);
      }

      $saved_expense = $this->dm->em->find(Expense::class, $transaction->transaction_id);

      $contact = $contacts[$cash_expense->contact_key];
      $payee = !empty($e->payee_key) ? $contacts[$e->payee_key] : null;
      $creditor = !empty($e->creditor_key) ? $contacts[$e->creditor_key] : null;

      $this->assertEquals($cash_expense->category ?? null, $saved_expense->category);
      $this->assertEquals($contact->name, $saved_expense->name);
      $this->assertEquals($contact->address, $saved_expense->address);
      $this->assertEquals($contact->city, $saved_expense->city);
      $this->assertEquals($contact->state, $saved_expense->state);
      $this->assertEquals($contact->postcode, $saved_expense->postcode);
      $this->assertEquals($contact->contact_key, $saved_expense->contact_key);
      if($payee){
        $this->assertEquals($payee->name, $saved_expense->payee);
      }
      if($creditor) {
        $this->assertEquals($creditor->name, $saved_expense->creditor);
      }
    }

    // Credit expenses
    $credit_expenses = array_values(array_filter($expenses, function ($e) {return !empty($e->credit);}));
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy([
        'report_id' => $saved_report->report_id,
        'category' => 'expense',
        'act' => 'charge',
        'legacy_form_type'=> 'A/GT50'
      ], ['transaction_id' => 'asc']);
    $this->assertEquals(count($credit_expenses), count($transactions));
    foreach ($transactions as $i => $transaction) {
      $e = $credit_expenses[$i];
      $this->assertNotEmpty($transaction->transaction_id);
      $this->assertEquals($e->date, $transaction->transaction_date->format('Y-m-d'));
      $this->assertEquals($e->amount, $transaction->amount);
      $this->assertEquals($e->description,$transaction->description);
      $this->assertEquals($transaction->report_id, $saved_report->report_id );
      $this->assertEquals($transaction->fund_id, $saved_report->fund_id);

      $saved_expense = $this->dm->em->find(Expense::class, $transaction->transaction_id);

      $contact = $contacts[$e->contact_key];
      $this->assertEquals($e->category ?? null,$saved_expense->category);
      $this->assertEquals($contact->name, $saved_expense->name);
      $this->assertEquals($contact->address, $saved_expense->address);
      $this->assertEquals($contact->city, $saved_expense->city);
      $this->assertEquals($contact->state, $saved_expense->state);
      $this->assertEquals($contact->postcode, $saved_expense->postcode);
      $this->assertEquals($contact->contact_key, $saved_expense->contact_key);
    }


    /** @var Transaction[] $transactions */
    $trans = $this->dm->em->getRepository(Transaction::class)
      ->findBy([
        'report_id' => $saved_report->report_id,
        'category' => 'expense',
        'act' => 'payment',
        'legacy_form_type'=> 'A/LE50'
      ], ['transaction_id' => 'asc']);
    $this->assertEquals(2, count($trans));
    /** @var Transaction $transaction */
    $transaction = $trans[0];
    $this->assertEquals(1, $transaction->expense_type);
    /** @var Expense $saved_expense */
    $saved_LT50expense = $this->dm->em->find(Expense::class, $transaction->transaction_id);

    $this->assertEquals($transaction->transaction_id, $saved_LT50expense->transaction_id);
    $this->assertNotEmpty($transaction->fund_id);
    $this->assertNotEmpty($transaction->report_id);
    $this->assertNull($transaction->origin);
    $this->assertLessThan(50.00, $transaction->amount);
    $this->assertFalse($transaction->inkind);
    $this->assertNotFalse($transaction->itemized);
    $this->assertNotEmpty($transaction->transaction_date->format('Y-m-d'));
    $this->assertNotEmpty($transaction->created_at);
    $this->assertNotEmpty($transaction->updated_at);
    $this->assertNull($transaction->description);

    $this->assertNotFalse($saved_LT50expense->anonymous);
    $this->assertNull($saved_LT50expense->contact_key);
    $this->assertEquals ($saved_LT50expense->name, 'EXPENSES OF $' . $this->thresholds->itemized_expenses . ' OR LESS');
    $this->assertNull ($saved_LT50expense->address);
    $this->assertNull ($saved_LT50expense->city);
    $this->assertNull ($saved_LT50expense->state);
    $this->assertNull ($saved_LT50expense->postcode);
    $this->assertEquals ($saved_LT50expense->category, 'A');
  }

  protected function checkExpenseCorrections($corrections, $category,  Report $saved_report, $contacts) {
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id, 'category' => $category, 'act' => 'correction'], ['transaction_id' => 'asc']);
    $this->assertEquals(count($corrections), count($transactions));
    foreach ($corrections as $c => $corr) {
      $transaction = $transactions[$c];
      /** @var Correction $correction */
      $correction = $this->dm->em->find(Correction::class, $transaction->transaction_id);
      $c = $contacts[$corr->contact_key];

      if ($category == 'expense' ) {
        /** @var Expense $contact */
        $contact = $this->dm->em->find(Expense::class, $transaction->transaction_id);
        $this->assertEquals(1, $transaction->expense_type);
      }
      else {
        $contact = $this->dm->em->find(Contribution::class, $transaction->transaction_id);
        $this->assertEquals(0, $transaction->expense_type);
      }

      $this->assertEquals($transaction->transaction_date->format('Y-m-d'), $corr->date);
      $this->assertEquals($corr->description, $transaction->description);
      $this->assertEquals($corr->difference, $transaction->amount);
      $this->assertEquals(TRUE,$transaction->itemized);
      $this->assertEquals($saved_report->fund_id, $transaction->fund_id);
      $this->assertFalse($transaction->inkind);
      $this->assertEquals($corr->reported_amount, $correction->original_amount );
      $this->assertEquals($corr->corrected_amount, $correction->corrected_amount);
      $this->assertEquals($c->contact_key, $contact->contact_key);
      $this->assertEquals($c->name, $contact->name);
      $this->assertEquals($c->address ?? null, $contact->address);
    }
  }

  protected function checkExpenseRefunds($refunds, Report $saved_report, array $contacts) {

    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(['report_id' => $saved_report->report_id, 'category' => 'expense', 'act' => 'refund'], ['transaction_id' => 'asc']);

    $this->assertEquals(count($refunds), count($transactions));
    foreach($refunds as $r => $refund) {
      $transaction = $transactions[$r];

      /** @var Expense $expense */
      $expense = $this->dm->em->find(Expense::class, $transaction->transaction_id);

      $this->assertEquals($saved_report->report_id, $transaction->report_id);
      $this->assertEquals(1, $transaction->expense_type);
      $this->assertEquals($refund->date, $transaction->transaction_date->format('Y-m-d'));
      $this->assertEquals(-1.0 * $refund->amount, $transaction->amount);

      $this->assertEquals($refund->contact_key, $expense->contact_key);
      $contact = $contacts[$refund->contact_key];
      $this->assertEquals($contact->name, $expense->name);
      $this->assertEquals($contact->address, $expense->address);

    }
  }

  protected function checkLoanPayments($loan_payments, Report $saved_report, array $contacts) {
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(
        ['report_id' => $saved_report->report_id, 'category' => 'loan', 'act' => ['payment', 'forgiven']],
        ['transaction_id' => 'asc']
      );
    $this->assertEquals(count($loan_payments), count($transactions));
    foreach ($loan_payments as $p => $pay) {
      $transaction = $transactions[$p];
      /** @var LoanPayment $payment */
      $payment = $this->dm->em->find(LoanPayment::class, $transaction->transaction_id);
      /** @var Expense $expense */
      $expense = $this->dm->em->find(Expense::class, $transaction->transaction_id);

      $this->assertFalse($transaction->inkind);
      # loan payments are only subtracted out on versions < 1.1
      $expected_type = 0;

      $this->assertEquals($expected_type, $transaction->expense_type);
      if (property_exists($pay, 'forgiven_amount') && $pay->forgiven_amount) {
        $this->assertEquals('forgiven', $transaction->act);
        $this->assertEquals('LOAN FORGIVEN', $transaction->description);
      }
      else {
        $this->assertEquals('payment', $transaction->act);
        $this->assertEquals('LOAN PAYMENT', $transaction->description);
      }
      $this->assertEquals($pay->date, $transaction->transaction_date->format('Y-m-d'));
      $this->assertEquals($payment->interest_paid + $payment->principle_paid, $transaction->amount);

      $this->assertEquals($pay->interest_paid ?? 0.0, $payment->interest_paid);
      $this->assertEquals($pay->principle_paid ?? 0.0, $payment->principle_paid);
      $this->assertEquals($pay->forgiven_amount ?? 0.0, $payment->forgiven_amount);
      $this->assertEquals($pay->balance ?? null, $payment->balance_owed);

      $contact = $contacts[$pay->contact_key];
      $this->assertEquals($contact->name, $expense->name);
      $this->assertEquals($contact->address, $expense->address);
      $this->assertEquals($contact->city, $expense->city);
      $this->assertEquals($contact->state, $expense->state);
      $this->assertEquals($contact->postcode, $expense->postcode);
      $this->assertEquals($contact->contact_key, $expense->contact_key);
    }
  }

  protected function checkLoanBalances($loan_balances, Report $saved_report, array $contacts) {
    /** @var Transaction[] $transactions */
    $transactions = $this->dm->em->getRepository(Transaction::class)
      ->findBy(
        ['report_id' => $saved_report->report_id, 'category' => 'loan', 'act' => 'balance'],
        ['transaction_id' => 'asc']
      );
    $this->assertEquals(count($loan_balances), count($transactions));
    foreach ($loan_balances as $p => $pay) {
      $transaction = $transactions[$p];
      /** @var LoanPayment $payment */
      $payment = $this->dm->em->find(LoanPayment::class, $transaction->transaction_id);
      /** @var Expense $expense */
      $expense = $this->dm->em->find(Expense::class, $transaction->transaction_id);

      $this->assertFalse($transaction->inkind);
      $this->assertEquals("LOAN BALANCE", $transaction->description);
      $this->assertEquals($pay->date, $transaction->transaction_date->format('Y-m-d'));
      $this->assertEquals($payment->interest_paid + $pay->principle_paid, $transaction->amount);

      $this->assertEquals( 0.0, $payment->interest_paid);
      $this->assertEquals( $pay->principle_paid, $payment->principle_paid);
      $this->assertEquals( 0.0, $payment->forgiven_amount);
      $this->assertEquals($pay->balance ?? null, $payment->balance_owed);

      $contact = $contacts[$pay->contact_key];
      $this->assertEquals($contact->name, $expense->name);
      $this->assertEquals($contact->address, $expense->address);
      $this->assertEquals($contact->city, $expense->city);
      $this->assertEquals($contact->state, $expense->state);
      $this->assertEquals($contact->postcode, $expense->postcode);
      $this->assertEquals($contact->contact_key, $expense->contact_key);
    }
  }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function checkODSummaryTotals($committee, $test) {
      //pulling the amounts from the open data view
      $od_finance_summary = (object)$this->dm->db->fetchAssociative('SELECT s.fund_id, s.expenditures_amount, s.contributions_amount, s.loans_amount, s.pledges_amount, s.debts_amount, s.carryforward_amount from od_campaign_finance_summary s where committee_id = :committee_id',['committee_id' => $committee->committee_id]);

      //test totals in open data vs the test file
      //carry-forward amount
      $this->assertEquals($od_finance_summary->carryforward_amount, $test->report->sums->previous_receipts->amount);

      //pledges
      $this->assertEquals($od_finance_summary->pledges_amount, $test->report->sums->pledges->amount);

      //loans
      $loans_info = (object)$test->report->loan;
      $loan_total = 0;
      //loans added in this period
      foreach ($loans_info->receipts as $receipt) {
          if($receipt->in_kind || $receipt->carry_forward){
              $loan_total += (double)$receipt->amount;
          }
      }
      //loan payments as well as forgiveness
      foreach ($loans_info->payments as $payment) {
          if(!empty($payment->principle_paid)) {
              $loan_total -= (double)$payment->principle_paid;
          }
          if(!empty($payment->forgiven_amount)) {
              $loan_total -= (double)$payment->forgiven_amount;
          }
      }
      //This total shows as negative due to the previous balances are not being populated from a non-existent C3 total
      $this->assertEquals($od_finance_summary->loans_amount, $loan_total);

      //expenditures
      //sum of expenses including corrections/refunds, loan payments, in-kind contributions, expenses under 50
      $expenditures_total = 0;
      //expenses
      foreach($test->report->expenses as $expense) {
        if (($expense->payment_category ?? null) !== 'debt') {
          $expenditures_total += (double)$expense->amount;
        }
      }
      //expense corrections
      foreach($test->report->expense_corrections as $correction) {
          $expenditures_total += (double)$correction->difference;
      }
      //expense refunds
      foreach($test->report->expense_refunds as $refund) {
          $expenditures_total -= (double)$refund->amount;
      }
      //sum in-kind_contributions
      foreach($test->report->contributions_inkind as $in_kind_contribution)
      $expenditures_total += (double)$in_kind_contribution->amount;
      //expenses under 50 in sums category
      if(!empty($test->report->version) && $test->report->version >= 1.1) {
          $expenditures_total += (double)$test->report->sums->non_itemized_cash_expenses->amount;
      } else {
          $expenditures_total += (double)$test->report->sums->expenses_under_50->amount;
      }
      if(!empty($test->report->sums->non_itemized_credit_expenses)) {
          $expenditures_total += (double)$test->report->sums->non_itemized_credit_expenses->amount;
      }

      // Loan principal payment amounts are in the expenditure data in the report but should not count as expenditures
      // as a part of the total. The only way to back them out is by negating by using the loan principal payment
      // amounts.
      foreach ($loans_info->payments as $payment) {
        if(!empty($payment->principle_paid)) {
          $expenditures_total -= (double)$payment->principle_paid;
        }
      }

      //assertion to compare totals
      $this->assertEquals($od_finance_summary->expenditures_amount, $expenditures_total);

      //contributions
      //contributions including corrections, expense refunds
      $contributions_total = 0;
      //in-kind contributions
      foreach ($test->report->contributions_inkind as $in_kind_contribution) {
          $contributions_total += (double)$in_kind_contribution->amount;
      }
      //contributions corrections
      foreach ($test->report->contribution_corrections as $contribution_correction) {
          $contributions_total += (double)$contribution_correction->difference;
      }
      //expense refunds
      foreach ($test->report->expense_refunds as $expense_refund) {
          $contributions_total -= (double)$expense_refund->amount;
      }
      //assertion
      $this->assertEquals($od_finance_summary->contributions_amount, $contributions_total);

      //debts
      $debts_total = 0;
      foreach ($test->report->debt as $debt) {
          $debts_total += (double)$debt->amount;
      }
      $this->assertEquals($od_finance_summary->debts_amount, $debts_total);
  }

  protected function checkSumItems($sums, Report $saved_report) {
    $sql = "select t.transaction_id, t.transaction_date, t.amount, s.aggregate_amount, s.tx_count, upper(s.legacy_line_item) as legacy_line_item
      from report_sum s join transaction t on t.transaction_id = s.transaction_id
      where t.act='sum' and t.report_id = ? and legacy_line_item not like '%.A1.%' order by legacy_line_item";
    $stmt = $this->dm->em->getConnection()->prepare($sql);
    $stmt->bindValue(1, $saved_report->report_id);
    $rows = $stmt->executeQuery()->fetchAllAssociative();
    // Decrementing count due to non_itemized expenses do not get included in the sums; this was originally passing due
    // to loan payments counting twice in the sums data. Line #4, and #14 (receipt and expenditure, respectively)
    $rowCount = count(get_object_vars($sums)) - 1;
    $this->assertEquals($rowCount, count($rows));
    if ($rows) {
      foreach ($rows as $row) {
        switch ($row['legacy_line_item']) {
          case '1':
            $this->assertEquals($sums->previous_receipts->amount, (float)$row['amount']);
            break;
          case '2':
            $this->assertEquals($sums->cash_receipts->amount, (float)$row['amount']);
            break;
          case '3':
            $this->assertEquals($sums->inkind_receipts->amount, (float)$row['amount']);
            break;
          case '4':
            $this->assertEquals($sums->total_receipts->amount, (float)$row['amount']);
            break;
          case '5':
            $this->assertEquals($sums->loan_principle_paid->amount, (float)$row['amount']); // ? principle_repaid
            break;
          case '6':
            $this->assertEquals($sums->receipt_corrections->amount, (float)$row['amount']);// ? corrections
            break;
          case '7':
            $this->assertEquals($sums->receipt_adjustments->amount, (float)$row['amount']);
            break;
          case '8':
            $this->assertEquals($sums->campaign_receipts->amount, (float)$row['amount']); // ? campaign_receipts_total
            break;
          case '9':
            $this->assertEquals($sums->pledges->amount, (float)$row['amount']);
            break;
          case '10':
            $this->assertEquals($sums->previous_expenditures->amount, (float)$row['amount']);
            break;
          case '11':
            $this->assertEquals($sums->cash_expenditures->amount, (float)$row['amount']);
            break;
          case '11.1':
            $this->assertEquals($sums->credit_expenditures->amount, (float)$row['amount']);
            break;
          case '12':
            $this->assertEquals($sums->inkind_expenditures->amount, (float)$row['amount']);
            break;
          case '13':
            $this->assertEquals($sums->total_expenditures->amount, (float)$row['amount']);
            break;
          case '14':
            $this->assertEquals($sums->loan_principle_paid->amount, (float)$row['amount']);
            break;
          case '15':
            $this->assertEquals($sums->expense_corrections->amount, (float)$row['amount']);
            break;
          case '16':
            $this->assertEquals($sums->expense_adjustments->amount, (float)$row['amount']);
            break;
          case '17':
            $this->assertEquals($sums->campaign_expenses->amount, (float)$row['amount']);
            break;
          case '18':
            $this->assertEquals($sums->cash_on_hand->amount, (float)$row['amount']);
            break;
          case '19':
            $this->assertEquals($sums->liabilities->amount, (float)$row['amount']);
            break;
          case '20':
            $this->assertEquals($sums->balance->amount, (float)$row['amount']);
            break;
        }
      }
    }
  }
}
