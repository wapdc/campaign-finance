<?php


namespace Tests\WAPDC\CampaignFinance\Finance;


use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Doctrine\Persistence\Mapping\MappingException;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\Core\Mock\MockMailer;
use WAPDC\CampaignFinance\FundProcessor;
use \Exception;
use \stdClass;
use WAPDC\CampaignFinance\Model\Fund;
use WAPDC\CampaignFinance\Model\Report;
use WAPDC\CampaignFinance\Reporting\C4ReportProcessor;
use WAPDC\CampaignFinance\Reporting\OrcaReportParser;
use WAPDC\CampaignFinance\SoapEnvelope;

class FundProcessorTest extends FinanceTestCase {

  /** @var FundProcessor */
  protected $processor;

  protected function setUp(): void {
    parent::setUp();
    $this->processor = new FundProcessor($this->dm);
  }

  public function testProcessor() {
    $this->assertInstanceOf(FundProcessor::class, $this->processor);
  }

  public function fundProvider() {
    return [
      ['candidate-c4'],
      ['pac'],
      ['surplus']
    ];
  }
  /**
   * @dataProvider fundProvider()
   * @throws Exception
   */
  public function testEnsureCommitteeFund($fund_scenario) {
    $dir = $this->data_dir . "/fund";
    $scenario = Yaml::parseFile("$dir/$fund_scenario.yml", YAML::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenario->committee);
    $this->assertNotEmpty($this->committee->committee_id);

    // First call to ensureCommitteeFund should create a new fund
    $election_code = $fund_scenario == 'surplus' ? '2007S2' : 2020;
    $fund_id = $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, $election_code, 'PDC');

    $this->dm->em->clear();

    /** @var Fund $fund */
    $fund = $this->dm->em->find(Fund::class, $fund_id);
    $this->assertInstanceOf(Fund::class, $fund);
    $this->assertEquals($committee->committee_id, $fund->committee_id);
    $this->assertEquals($committee->filer_id, $fund->filer_id);
    $this->assertEquals($fund->election_code, $election_code);
    // Testing new target type and target_id sync
    $this->assertEquals('committee', $fund->target_type);
    $this->assertEquals($committee->committee_id, $fund->target_id);

    // Second time we should only get one fund even though we called it a second time.
    if ($fund_scenario == 'surplus') {
      $election_code = 'whoevencaresanymore?!@#$%^&*();\\\;DELETE FROM foffice WHERE true;';
    }
    $second_fund_id = $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, $election_code, 'PDC');
    $this->assertEquals($fund_id, $second_fund_id);
    $this->assertEquals('PDC', $fund->vendor);

    // bad election code should generate exception for non-surplus committees
    // try-catch not expectException so the test will continue after the expected exception
    if ($fund_scenario <> 'surplus') {
      $exceptionThrown = false;
      try {
        $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, '2021S1', 'PDC');
      }
      catch (Exception $e) {
        $exceptionThrown = true;
      } finally {
        $this->assertTrue($exceptionThrown);
      }
    }

    // check good year again
    $third_fund_id = $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, $election_code);
    $this->assertEquals($fund_id, $third_fund_id);

    // single-year committees may not create funds outside of their election year
    if (!$committee->continuing) {
      $exceptionThrown = false;
      try {
        $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, '2021', 'PDC');
      }
      catch (Exception $e) {
        $exceptionThrown = true;
      } finally {
        $this->assertTrue($exceptionThrown);
      }
    }

    // continuing committees may not create funds outside of their registered years
    if ($committee->continuing && $committee->pac_type <> 'surplus') {
      // before the committee is valid
      $exceptionThrown = false;
      try {
        $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, '2007', 'PDC');
      }
      catch (Exception $e) {
        $exceptionThrown = true;
      } finally {
        $this->assertTrue($exceptionThrown);
      }
      // after the committee is valid
      $exceptionThrown = false;
      try {
        // next year
        $election_code = (int)date('Y') + 1;
        $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, $election_code);
      }
      catch (Exception $e) {
        $exceptionThrown = true;
      } finally {
        $this->assertTrue($exceptionThrown);
      }
    }

    // surplus with multiple funds should throw an exception
    if ($committee->pac_type == 'surplus') {
      $this->dm->db->executeQuery("insert into fund (committee_id, election_code)
        values (:committee_id, :election_code)", ['committee_id' => $committee->committee_id, 'election_code' => '2023']);
      try {
        $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, $election_code, 'PDC');
        $this->assertFalse(true);
      }
      catch (Exception $e) {
        $this->assertTrue(true);
      }
    }
  }

  /**
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testSubmitCommitteeReport($scenario='candidate-c4') {
    $dir = $this->data_dir . "/fund";
    // Load validation file.
    $validations = Yaml::parseFile("$dir/$scenario.validations.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Object has tests property which contains an array of test condition.
    foreach($validations->tests as $validation_test) {
      // Start with the same initial data.
      $test = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
      $expected_errors = [];

      // Each condition either an assertion of an error or and assertion of a particular initial state modified from
      // the base $submission
      foreach ($validation_test as $condition => $value) {
        switch (true) {
          case (strpos($condition, '.') !== FALSE):
            $this->setProperty($test, $condition, $value);
            break;
          case ($condition == 'error'):
            if (is_array($value)) {
              foreach ($value as $error) {
                $expected_errors[] = $error;
              }
            } else {
              $expected_errors[] = $value;
            }
            break;
        }
      }
      $committee = $this->createCommittee($test->committee);
      if ($test->fund) {
        $this->createFund($test->fund, $committee->committee_id);
      }
      if (!empty($test->candidacy)) {
        $this->createPersonAndCandidacy($test->candidacy, $committee->committee_id);
      }
      // Run through validator
      $response = $this->processor->validateCommitteeReport($committee->committee_id, $test->report, $test->metadata->submission_version);

      if ($expected_errors && $expected_errors != ['']) {
        $this->assertFalse($response->success);
        foreach ($expected_errors as $expected_error) {
          $this->assertArrayHasKey($expected_error, $response->messages, "Missing expected errors for $scenario: $condition");
        }
      } else {
        $this->assertNotEmpty($response->message);
        $this->assertNotEmpty($response->metadata->sums);
        $this->assertTrue($response->success);
      }
    }
  }

  /**
   * @throws Exception
   *
   * This test ensures that a report can be deleted without breaking any previous or later amendments it may have.
   */
  public function testDeleteReportSubmission($scenario='candidate-c4') {
    $c4processor = new C4ReportProcessor($this->dm, 1.1);
    $dir = $this->data_dir . "/fund";
    $test = Yaml::parseFile("$dir/candidate-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $committee = $this->createCommittee($test->committee);
    $fund = $this->createFund($test->fund, $committee->committee_id);
    if (!empty($test->candidacy)) {
      $this->createPersonAndCandidacy($test->candidacy, $committee->committee_id);
    }
    $report1 = $c4processor->submitReport($test, $fund);
    $this->assertTrue($report1->success);

    //Amend original report
    $original_report_id = $report1->report_id;
    $test->report->amends = $original_report_id;
    $report2 = $c4processor->submitReport($test, $fund);
    $this->assertTrue($report2->success);

    //Amend amendment
    $amended_report_id = $report2->report_id;
    $test->report->amends = $amended_report_id;
    $report3 = $c4processor->submitReport($test, $fund);
    $this->assertTrue($report3->success);

    /** @var Report $saved_report */
    $saved_report = $this->dm->em->find(Report::class, $report1->report_id);
    $this->assertInstanceOf(Report::class, $saved_report);
    /** @var Report $report_amendment_1 */
    $report_amendment_1 = $this->dm->em->find(Report::class, $report2->report_id);
    $this->assertInstanceOf(Report::class, $report_amendment_1);
    /** @var Report $report_amendment_2 */
    $report_amendment_2 = $this->dm->em->find(Report::class, $report3->report_id);
    $this->assertInstanceOf(Report::class, $report_amendment_2);

    // Delete report2. Assert it has been properly deleted. Assert report 1 is amended by report 3
    // by ensuring that the superseded id of the original report is the report id of report 3.
    $amendment1_id = $report_amendment_1->report_id;
    $this->processor->deleteSubmittedReport($amendment1_id);
    $amendment_1 = $this->dm->em->find(Report::class, $amendment1_id);
    $deletedAmendment1 = $this->dm->em->find(Report::class, $amendment1_id);
    $this->assertEmpty($deletedAmendment1);
    $this->assertEquals($report_amendment_2->report_id,$saved_report->superseded_id);

    // Delete report3. Assert it has been properly deleted. Assert report 1 is not amended
    // by ensuring that the superseded id of the original report is now null.
    $amendment2_id = $report_amendment_2->report_id;
    $this->processor->deleteSubmittedReport($amendment2_id);
    $deletedAmendment2 = $this->dm->em->find(Report::class, $amendment2_id);
    $this->assertEmpty($deletedAmendment2);
    $this->assertEquals(null,$saved_report->superseded_id);
  }

  public function testSendSuccessEmail() {
    $email_array = ['ben.livingston@pdc.wa.gov'];
    $email_template_vars = ['committee_id' => '12345', 'committee_name' => 'Citizens for Appropriate Testing', 'date_filed' => date('F j, Y'), 'report_id' => '123456789', 'report_period' => '2020-10-01 - 2020-10-31', 'response_message' => 'Everything about this test filing looked absolutely perfect.'];
    $this->processor->setMailer(new MockMailer());
    $email_sent = $this->processor->sendSuccessEmail($email_array, $email_template_vars);
    $this->assertContains($email_array[0], $email_sent);
  }

  /**
   * @throws OptimisticLockException
   * @throws ConnectionException
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws ORMException
   * @throws \Doctrine\DBAL\Exception
   */
  function testEnvelopeSubmission() {
    $committee = new \StdClass();
    $committee->name = 'People for Code Changes';
    $committee->pac_type = 'pac';
    $committee->election_code = '2020';
    $committee->filer_id = 'ABCD 1234';
    $committee = $this->createCommittee($committee);
    $fund_id = $this->processor->ensureCommitteeFundAndVendor($committee->committee_id, $committee->election_code);
    $this->processor->setMailer(new MockMailer());

    $dir = dirname(dirname(__DIR__)) . '/data/campaign_finance_submission';
    $payload = file_get_contents("$dir/C3SoapSubmission.txt");
    $clean_payload = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $payload);
    $xml_payload = simplexml_load_string($clean_payload);
    $data = new \stdClass();
    foreach ($xml_payload->Body->children() as $method => $parameters) {
      $data->method = (string)$method;
      foreach ($parameters as $key => $value) {
        $data->$key = (string)$value;
      }
    }
    $soap = new SoapEnvelope($data->method);
    $xml = $soap->extractXml($data->xrfZipBase64);
    $parser = new OrcaReportParser();
    $report = $parser->parse($xml);
    $report->election_year = $data->year;
    $report->filer_id = $data->filerId ?? $data->filer_id ?? null;
    if (!empty($data->amdno)) {
      $report->amends = $data->amdno;
    }
    $report->submitted_at = (new \DateTime())->format('Y-m-d');
    $response = $this->processor->submitCommitteeReport($committee->committee_id, $report, $data->email, 'some vendor', 1.0);
    $this->assertTrue($response->success, "Submit committee report failed.");
    $this->assertContains('david.metzler@pdc.wa.gov', $response->email_sent, "Email expected in response from submitCommitteeReport.");
    $query = $this->dm->db->executeQuery("select * from transaction where report_id = " . $response->report_id);
    $this->assertGreaterThan(0, count($query->fetchAllAssociative()), "Submitted report should have generated transactions.");
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testEditReport()
  {
    $dir = $this->data_dir . "/fund";
    $test = Yaml::parseFile("$dir/deposit-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $committee = $this->createCommittee($test->committee);
    $fund = $this->createFund($test->fund, $committee->committee_id);
    if (!empty($test->candidacy)) {
      $this->createPersonAndCandidacy($test->candidacy, $committee->committee_id);
    }

    $this->dm->db->executeStatement("insert into report (fund_id, period_start, period_end, report_type) values 
    (:fund_id, :period_start, :period_end, :report_type)", ["fund_id" => $fund->fund_id,
      "period_start" => "2021-12-06", "period_end" => "2021-12-06", "report_type" => "c3"]);

    $middle_report_id = $this->dm->db->lastInsertId();
    // Create report so we can edit it
    $this->dm->db->executeStatement("insert into report (fund_id, period_start, period_end, report_type, superseded_id) values 
    (:fund_id, :period_start, :period_end, :report_type, :superseded_id)", ["fund_id" => $fund->fund_id,
      "period_start" => "2021-12-06", "period_end" => "2021-12-06", "report_type" => "c3", "superseded_id" => $middle_report_id]);

    $amends = $this->dm->db->lastInsertId();

    $this->dm->db->executeStatement("insert into report (fund_id, period_start, period_end, report_type) values 
    (:fund_id, :period_start, :period_end, :report_type)", ["fund_id" => $fund->fund_id,
      "period_start" => "2021-12-06", "period_end" => "2021-12-06", "report_type" => "c3"]);

    $report_id = $this->dm->db->lastInsertId();
    $report_data = new stdClass();
    $report_data->submitted_at = "2021-12-20";

    $report_data->external_id = $test->report->external_id;

    // changing the reports amendment chain
    $report_data->amends_id = $amends;
    $report_data->input_external_id = '';
    $this->saveEditedReport($amends, $report_id, $middle_report_id, $report_data);

    // external id change
    $report_data->amends_id = null;
    $report_data->input_external_id = 123456;
    $this->saveEditedReport($amends, $report_id, $middle_report_id, $report_data);
  }

  public function saveEditedReport($amends, $report_id, $middle_report_id, $report_data){

    $this->processor->editReport($report_id, $report_data);

    [$updated_report] = $this->dm->db->executeQuery("select * from report where report_id = :report_id",
      ["report_id" => $report_id])->fetchAllAssociative();
    $this->assertEquals($report_data->submitted_at, date('Y-m-d', strtotime($updated_report['submitted_at'])));

    if($report_data->amends_id){
      [$amends_report] = $this->dm->db->executeQuery("select * from report where report_id = :report_id",
        ["report_id" => $report_data->amends_id])->fetchAllAssociative();
      $this->assertEquals($middle_report_id, $updated_report['superseded_id']);
      $this->assertEquals($report_id, $amends_report['superseded_id']);
    } else {
      $this->assertEquals($updated_report['external_id'], $report_data->input_external_id);
    }
  }

}
