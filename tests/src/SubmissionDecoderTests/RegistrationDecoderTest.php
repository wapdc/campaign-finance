<?php

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\RegistrationManager;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\RegistrationDraftManager;
use WAPDC\CampaignFinance\SubmissionDecoder\RegistrationDecoder;
use WAPDC\Core\FilerProcessor;
use WAPDC\Core\Model\User;

class RegistrationDecoderTest extends TestCase
{


  /**
   * @var CFDataManager
   */
  private $dm;

  /**
   * @var RegistrationManager
   */
  private $registrationManager;

  /**
   * @var CommitteeManager
   */
  private $committeeManager;

  /**
   * @var RegistrationDraftManager
   */
  private $registrationDraftManager;

  /** @var Committee */
  private $committee;

  /** @var Registration */
  private $registration;

  /**
   * @throws \Doctrine\DBAL\DBALException
   * @throws \Doctrine\ORM\ORMException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->committeeManager = new CommitteeManager($this->dm);
    $fp = new FilerProcessor($this->dm);
    $this->registrationDraftManager = new RegistrationDraftManager($this->dm, $this->committeeManager);
    $this->registrationManager = new RegistrationManager($this->dm, $this->committeeManager, $this->registrationDraftManager, $fp);
  }

  /**
   * @param string $scenario
   * @return \stdClass
   * @throws ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  function generateCommitteeFromSubmission($scenario, $name ='') {
    $dir = dirname(dirname(__DIR__ )) . '/data/registration_submission';
    $submission_file = "$dir/$scenario.yml";
    $verification_file = "$dir/$scenario.admin_data.yml";
    $submission = Yaml::parseFile($submission_file, YAML::PARSE_OBJECT_FOR_MAP);
    $submission_json = json_encode($submission);
    $verification = new \stdClass();
    $verification->committee = new \stdClass();
    if (file_exists($verification_file)) {
      $verification = Yaml::parseFile($verification_file, YAML::PARSE_OBJECT_FOR_MAP);
    };

    $committee_name = $name ?? $submission->committee->name;
    $committee_type = $submission->committee->committee_type;
    $election_code =  $submission->committee->election_code ?? NULL;

    $committee = new Committee($committee_name, $committee_type);
    $committee->election_code = $election_code;
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    $registration = new Registration();
    $registration->committee_id = $committee->committee_id;
    $registration->user_data = $submission_json;
    $registration->admin_data = json_encode($verification);
    $submission->admin_data = $verification;
    $this->dm->em->persist($registration);
    $this->dm->em->flush();
    $committee->registration_id = $registration->registration_id;
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    $this->committee = $committee;
    $this->registration = $registration;
    $submission->committee->committee_id = $committee->committee_id;

    $this->dm->em->persist($registration);
    $this->dm->em->flush();
    $submission->registration_id = $registration->registration_id;
    return $submission;
  }

  private function _compareObject($object1, $object2, $message = '') {

    foreach (get_object_vars($object1) as $property => $value) {
      $this->assertObjectHasAttribute($property, $object2, "$message.$property");
      switch (TRUE) {

        case (is_object($value)):
          $this->_compareObject($value, $object2->$property, "$message.$property");
          break;
        case (is_array($value)):
          foreach ($value as $i => $element) {
            // Arrays of objects should be compared as objects.
            $this->assertArrayHasKey($i, $object2->$property, "$message.{$property}[$i]");
            if (is_object($element)) {
              $this->_compareObject($element, $object2->$property[$i], "$message.{$property}[$i]");
            }
            // Assume an array of scalars
            else {
              $this->assertEquals($element, $object2->$property[$i], "$message.{$property}[$i]");
            }
          }
          break;
        default:
          $this->assertObjectHasAttribute($property, $object2, "$message.$property");
          $this->assertEquals($object1->$property, $object2->$property, "{$message}.{$property}");

      }
    }
  }

  /**
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function testRegistrationDecoder() {
    $submission = $this->generateCommitteeFromSubmission('political-committee');

    $decoder = new RegistrationDecoder();
    $result = $decoder->generateSubmission($this->registration);
    $this->assertInstanceOf(stdClass::class, $result);
    $this->_compareObject($result, $submission);
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

}