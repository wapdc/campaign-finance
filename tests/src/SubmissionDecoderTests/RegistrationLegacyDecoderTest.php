<?php

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\SubmissionDecoder\RegistrationLegacyDecoder;

class RegistrationLegacyDecoderTest extends TestCase
{

  /**
   * @var CFDataManager
   */
  private $dm;

  /**
   * @throws \Doctrine\DBAL\DBALException
   * @throws \Doctrine\ORM\ORMException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
  }

  /**
   * @param $object1
   * @param $object2
   * @param string $message
   */
  private function _compareObject($object1, $object2, $message = '') {

    foreach (get_object_vars($object1) as $property => $value) {
      $this->assertObjectHasAttribute($property, $object2, "$message.$property");
      switch (TRUE) {

        case (is_object($value)):
          $this->_compareObject($value, $object2->$property, "$message.$property");
          break;
        case (is_array($value)):
          foreach ($value as $i => $element) {
            // Arrays of objects should be compared as objects.
            $this->assertArrayHasKey($i, $object2->$property, "$message.{$property}[$i]");
            if (is_object($element)) {
              $this->_compareObject($element, $object2->$property[$i], "$message.{$property}[$i]");
            }
            // Assume an array of scalars
            else {
              $this->assertEquals($element, $object2->$property[$i], "$message.{$property}[$i]");
            }
          }
          break;
        default:
          $this->assertObjectHasAttribute($property, $object2, "$message.$property");
          $this->assertEquals($object1->$property, $object2->$property, "{$message}.{$property}");

      }
    }
  }

  /**
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testRegistrationLegacyDecoder() {
    $dir = dirname(dirname(__DIR__)) . "/data/registration_decoder";
    $xml = file_get_contents("$dir/c1pcRegistration.xml");
    $submission = Yaml::parseFile("$dir/c1pcSubmission.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = new Committee($submission->committee->name);
    $this->dm->em->persist($committee);
    $this->dm->em->flush($committee);
    $registration = new Registration();
    $registration->source = 'ORCA';
    $registration->committee_id = $committee->committee_id;
    $registration->user_data = $xml;
    $decoder = new RegistrationLegacyDecoder();
    $result = $decoder->generateSubmission($registration);
    $this->_compareObject($submission, $result);
  }

  /**
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testRegistrationLegacyDecoderC1(){
    $dir = dirname(dirname(__DIR__)) . "/data/registration_decoder";
    $xml = file_get_contents("$dir/c1Registration.xml");
    $submission = Yaml::parseFile("$dir/c1Submission.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = new Committee($submission->committee->name);
    $this->dm->em->persist($committee);
    $this->dm->em->flush($committee);
    $registration = new Registration();
    $registration->source = 'ORCA';
    $registration->committee_id = $committee->committee_id;
    $registration->user_data = $xml;
    $decoder = new RegistrationLegacyDecoder();
    $result = $decoder->generateSubmission($registration);
    $this->_compareObject($submission, $result);
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

}