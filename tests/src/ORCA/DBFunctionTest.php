<?php


namespace Tests\WAPDC\CampaignFinance\ORCA;


use PHPUnit\Framework\TestCase;
use WAPDC\CampaignFinance\CFDataManager;

class DBFunctionTest extends TestCase
{
  /** @var CFDataManager */
  private $dm;

  protected function setUp(): void
  {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->dm->db->executeStatement("delete from private.trankeygen where fund_id = -1");
  }

  public function ensureTrankeyGenProvider(): array {
    return [
      [NULL, -1,  NULL],
      [NULL, 1, 1],
      [NULL, 0, NULL],
      [NULL, NULL, 1]
    ];
  }

  /**
   * @param $orca_id
   *   ORCA ID
   * @param $rds_id
   *   RDS trankeygen id
   * @param $expected
   *   Expected return
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   * @dataProvider ensureTrankeyGenProvider
   */
  public function testEnsureTranKeyGen($rds_id, $orca_id, $expected) {
    $value = $this->dm->db->executeQuery("SELECT private.cf_ensure_trankeygen(:fund_id, :rds_id, :orca_id)", [
      'fund_id' => -1,
      'orca_id' => $orca_id,
      'rds_id' => $rds_id
    ])->fetchOne();
    $message = "cf_ensure_trankeygen(fund, rds_id: $rds_id, orca_id: $orca_id)";
    if ($expected) {
      $this->assertNotEmpty($value, $message);
    }
    else {
      $this->assertEmpty($value, $message);
    }

  }

  protected function tearDown(): void
  {
    $this->dm->rollback();
    parent::tearDown();

  }

}