<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class TransactionTest extends ORCATestCase {
  /**
   * @throws OptimisticLockException
   * @throws ORMException
   */
  public function testDeleteTransaction() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/transactions/transaction.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = Yaml::parseFile($this->data_dir . '/committees/continuing-pac.yml', Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee);
    $expectedReceipt = $scenario_data->receipts;
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->orcaProcessor->deleteTransaction($this->fund_id, $this->trankeygen_map[101]);
    $transaction = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :id', ['id' =>$this->trankeygen_map[101]])->fetchAllAssociative();
    $this->assertEmpty($transaction);
    $this->assertAccountBalance($this->trankeygen_map[1600], 0);
    $this->assertAccountBalance($this->trankeygen_map[4000], 0);
  }
}