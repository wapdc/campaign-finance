<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Exception;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class ExpenditureTest extends ORCATestCase {
  public function expenditureSaveProvider()
  {
    return [['expense'], ['expense-surplus']];
  }

  /**
   * @throws Exception|\Doctrine\DBAL\Driver\Exception
   * @dataProvider expenditureSaveProvider
   */
  public function testSaveExpenditure($scenario)
  {
    // Load test scenario data
    $contents = file_get_contents($this->data_dir . "/expenditures/$scenario.json");
    $data = json_decode($contents);
    $fund_id = -17;

    $this->makeContactsForJSON($fund_id, $data);
    // Make sure that the account -5 is a surplus account.
    $this->dm->db->executeStatement("update private.accounts set code = 'C-GEN' where trankeygen_id = -5");

    $savedExpenditureJson = $this->orcaProcessor->saveExpenditure(-17, $data);

    $savedExpenses = $savedExpenditureJson->expenditure->expenses;
    $savedExpenditure = $savedExpenditureJson->expenditure->expenditureEvent;

    $total = 0;
    foreach ($savedExpenses as $expense) {
      $total += $expense->amount;
    }
    $this->assertAccountBalance($savedExpenditure->bnkid, -1 * $total);
    $balanced = $this->dm->db->fetchOne("SELECT sum(total) from private.accounts a join private.trankeygen t ON a.trankeygen_id=t.trankeygen_id where t.fund_id=:fund_id ", ['fund_id' => $fund_id]);
    $this->assertEquals(0, $balanced);

    //make sure all values are set for the expenditure event
    $this->assertNotEmpty($savedExpenditure->trankeygen_id);
    foreach ($data->expenditure->expenditureEvent as $key => $value) {
      switch ($key) {
        case 'expenses':
          break;
        default:
          $this->assertEquals($value, $savedExpenditure->$key);
      }
    }

    // make sure that the expense is categorized as using surplus funds.
    $this->assertNotEquals(0, $savedExpenditure->amount);
    //make sure all expenses are saved
    $this->assertEquals(count($data->expenditure->expenses), count($savedExpenses));

    //make sure all values are set correctly for one of the expenses
    $originalExpense = $data->expenditure->expenses[0];
    $savedExpense = $savedExpenses[array_search($originalExpense->amount, array_column($savedExpenses, 'amount'))];
    foreach ($originalExpense as $key => $value) {
      switch ($key) {
        case 'expenses':
          break;
        case 'description':
          $this->assertEquals($originalExpense->user_description, $savedExpense->description);
          break;
        default:
          $this->assertEquals($value, $savedExpense->$key);
      }
    }
    $this->assertEquals($savedExpenditure->trankeygen_id, $savedExpense->pid);
    $this->assertEquals($savedExpenditure->bnkid, $savedExpense->did);
    $this->assertEquals(28, $savedExpense->type);
    $this->assertEquals(3, $savedExpense->nettype);
    $expenseTrankeyGens = $this->dm->db->executeQuery('select * from private.trankeygen where pid = :pid', ['pid' => $savedExpenditure->trankeygen_id])->rowCount();
    $this->assertEquals(count($data->expenditure->expenses), $expenseTrankeyGens);

    //Make sure that if we remove an expense, the saved result will have also removed the saved expense.
    unset($data->expenditure->expenses[1]);
    $savedExpenditure = $this->orcaProcessor->saveExpenditure($fund_id, $data);
    $this->assertEquals(count($data->expenditure->expenses), count($savedExpenditure->expenditure->expenses));
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function testDeleteExpenditure()
  {
    $contents = file_get_contents($this->data_dir . "/expenditures/expense.json");
    $fund_id = -17;
    $data = json_decode($contents);
    $this->makeContactsForJSON($fund_id, $data);
    $savedExpenditureJson = $this->orcaProcessor->saveExpenditure($fund_id, $data);
    $expenditureId = $savedExpenditureJson->expenditure->expenditureEvent->trankeygen_id;
    $this->orcaProcessor->deleteExpenditure($fund_id, $expenditureId);
    //assert expenditure event was deleted
    $expenditureEvents = $this->dm->db->executeQuery('select * from private.expenditureEvents where trankeygen_id = :id', ['id' => $expenditureId])->fetchAllAssociative();
    $this->assertEmpty($expenditureEvents);
    //assert receipts were deleted
    $receipts = $this->dm->db->executeQuery('select * from private.receipts where pid = :id', ['id' => $expenditureId])->fetchAllAssociative();
    $this->assertEmpty($receipts);
    //assert trankeygen records deleted
    $trankeygen = $this->dm->db->executeQuery('select * from private.trankeygen where trankeygen_id = :id or pid=:id', ['id' => $expenditureId])->fetchAllAssociative();
    $this->assertEmpty($trankeygen);
    $this->assertAccountBalance($savedExpenditureJson->expenditure->expenditureEvent->bnkid, 0);

    $accounts = $this->dm->db->executeQuery('select * from private.accounts where pid = :pid',['pid' => $expenditureId])->fetchAllAssociative();
    $this->assertEmpty($accounts);
  }
}