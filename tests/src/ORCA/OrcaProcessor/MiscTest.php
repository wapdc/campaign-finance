<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class MiscTest extends ORCATestCase {
  public function testSaveMiscellaneous()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/misc/miscellaneous-receipts.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // misc other
    $this->setTrankeygenIds($scenario_data->miscellaneous->other, ['cid']);
    $data = $scenario_data->miscellaneous->other;
    $savedMiscOther = $this->orcaProcessor->saveMiscOther($this->fund_id, $scenario_data->miscellaneous->other);
    $this->assertNotEmpty($savedMiscOther);
    $this->assertEquals($data->amount, $savedMiscOther->{'misc-other'}->amount);
    $this->assertEquals($data->date_, $savedMiscOther->{'misc-other'}->date_);
    $this->assertEquals($data->memo, $savedMiscOther->{'misc-other'}->memo);
    $this->assertEquals($data->name, $savedMiscOther->{'misc-other'}->name);
    $this->assertEquals($data->street, $savedMiscOther->{'misc-other'}->street);
    $this->assertEquals($data->city, $savedMiscOther->{'misc-other'}->city);
    $this->assertEquals($data->state, $savedMiscOther->{'misc-other'}->state);
    $this->assertEquals($data->zip, $savedMiscOther->{'misc-other'}->zip);
    $this->assertEquals(1, $savedMiscOther->{'misc-other'}->nettype);
    $createdContact = $this->dm->db->executeQuery('select * from private.contacts where trankeygen_id = :trankeygen_id', ["trankeygen_id" => $savedMiscOther->{'misc-other'}->contid])->fetchAssociative();
    $this->assertNotEmpty($createdContact);

    $this->assertAccountBalance($this->trankeygen_map[1600], $data->amount);
  }

  public function testSaveMiscBankInterest()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/misc/misc.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // Misc bank interest
    $this->setTrankeygenIds($scenario_data->misc->bank_interest, ['cid']);
    $this->setTrankeygenIds($scenario_data->misc->bank_interest, ['contid']);
    $data = $scenario_data->misc->bank_interest;
    $savedMiscBankInterest = $this->orcaProcessor->saveMiscBankInterest($this->fund_id, $scenario_data->misc->bank_interest);
    $this->assertNotEmpty($savedMiscBankInterest);
    $this->assertEquals($data->cid, $savedMiscBankInterest->{'misc-bank-interest'}->cid);
    $this->assertEquals($data->amount, $savedMiscBankInterest->{'misc-bank-interest'}->amount);
    $this->assertEquals($data->date_, $savedMiscBankInterest->{'misc-bank-interest'}->date_);
    $this->assertEquals($data->description, $savedMiscBankInterest->{'misc-bank-interest'}->description);
    $this->assertEquals(1,$savedMiscBankInterest->{'misc-bank-interest'}->nettype);

    $this->assertAccountBalance($this->trankeygen_map[1600], $data->amount);
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   */
  public function testSaveVendorRefund() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/misc/vendor-refund.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = Yaml::parseFile($this->data_dir . '/committees/continuing-pac.yml', Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->setTrankeygenIds($scenario_data->miscellaneous->vendor_refund, ['contid', 'cid', 'did', 'pid']);
    $savedVendorRefund = $this->orcaProcessor->saveVendorRefund($this->fund_id, $scenario_data->miscellaneous->vendor_refund);
    $data = $scenario_data->miscellaneous->vendor_refund;
    $this->assertEquals($this->trankeygen_map[11], $savedVendorRefund->{'vendor-refund'}->contid);
    $this->assertEquals($data->amount, $savedVendorRefund->{'vendor-refund'}->amount);
    $this->assertEquals($data->date_, $savedVendorRefund->{'vendor-refund'}->date_);
    $this->assertEquals($data->description, $savedVendorRefund->{'vendor-refund'}->description);
    $this->assertEquals($data->memo, $savedVendorRefund->{'vendor-refund'}->memo);
    $this->assertAccountBalance($this->trankeygen_map[1600], $data->amount);
    $this->assertAccountBalance($this->trankeygen_map[4300], -1 * $data->amount);
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   */
  public function testDeleteMiscReceipts()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/misc/miscellaneous-receipts.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $other_misc_contents = $scenario_data->miscellaneous->other;
    $bank_interest_contents = $scenario_data->miscellaneous->bank_interest;
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->setTrankeygenIds($bank_interest_contents, ['contid', 'cid']);
    // delete bank interest
    $savedBankInterest = $this->orcaProcessor->saveMiscBankInterest($this->fund_id, $bank_interest_contents);
    $savedBankInterestId = $savedBankInterest->{'misc-bank-interest'}->trankeygen_id;
    $this->orcaProcessor->deleteMiscBankInterest($this->fund_id, $savedBankInterestId);
    $bank_interest = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :id', ['id' => $savedBankInterestId])->fetchAllAssociative();
    $this->assertEmpty($bank_interest);
    // delete misc other receipt
    $savedOtherMisc = $this->orcaProcessor->saveMiscOther($this->fund_id, $other_misc_contents);
    $savedMiscOtherId = $savedOtherMisc->{'misc-other'}->trankeygen_id;
    $this->orcaProcessor->deleteMiscOther($this->fund_id, $savedMiscOtherId);
    $misc_other = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :id', ['id' => $savedBankInterestId])->fetchAllAssociative();
    $this->assertEmpty($misc_other);
  }
}