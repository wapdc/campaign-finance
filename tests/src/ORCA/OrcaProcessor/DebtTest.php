<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class DebtTest extends ORCATestCase {
  public function testSaveVendorDebt() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/vendor-debt.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    $this->setTrankeygenIds($scenario_data->expenditure, ['contid', 'bnkid', 'cid']);

    $result = $this->orcaProcessor->saveExpenditure($this->fund_id, $scenario_data);

    $this->assertNotEmpty($result);
    $this->assertTrue($result->success);

    // Check basic properties of expenditure
    $savedEvent = $result->expenditure->expenditureEvent;
    foreach ($scenario_data->expenditure->expenditureEvent as $key => $value) {
      $this->assertSame($value, $savedEvent->$key ?? null, $key);
    }

    foreach ($scenario_data->expenditure->expenses as $idx => $expense) {
      $savedExpense = $result->expenditure->expenses[$idx];
      foreach($expense as $key => $value) {
        $this->assertSame($value, $savedExpense->$key ?? null, "$idx: $key");
      }
      $this->assertEquals(28, $savedExpense->type);
      //need to figure out how to dynamically get this?
      $acct_num = $this->dm->db->fetchOne("SELECT acctnum from private.accounts where trankeygen_id = :account_id", ['account_id' => $savedEvent->bnkid]);
      if ($acct_num >= 2000 && $acct_num <= 2600) {
        $this->assertEquals(0, $savedExpense->nettype);
      }
      else {
        $this->assertEquals(3, $savedExpense->nettype);
      }
    }

    $this->assertAccountBalance($savedEvent->bnkid, $savedEvent->amount * -1);
  }

  /**
   * This test ensure that vendor debt forgiveness is saved to the database.
   * See vendor-debt-forgiveness.yml for requirements
   * */
  public function testSaveVendorDebtForgiveness() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/vendor-debt-forgiveness.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createExpenditureEvents($scenario_data->expenditureEvent ?? []);
    $this->createReceipts($scenario_data->receipts);
    $this->setTrankeygenIds($scenario_data->forgiven, ['pid']);
    $result = $this->orcaProcessor->saveVendorDebtForgiveness($this->fund_id, $scenario_data->forgiven);
    $this->assertNotEmpty($result);
    $this->assertTrue($result->success);

    $savedForgiven = $result->{'vendor-debt-forgiveness'};
    foreach ($scenario_data->forgiven as $key => $value) {
      $this->assertSame($value, $savedForgiven->$key ?? null, $key);
    }
    $this->assertSame(1, $savedForgiven->nettype);
    $this->assertSame(1, $savedForgiven->aggtype);
    $this->assertNotEmpty($savedForgiven->deptkey);
    $this->assertAccountBalance($this->trankeygen_map[2000], -70);
    $this->assertAccountBalance($this->trankeygen_map[4100], -30);
  }

  public function testSaveVendorDebtPayment() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/vendor-debt-payment.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createExpenditureEvents($scenario_data->expenditureEvent ?? []);

    $this->setTrankeygenIds($scenario_data->payment, ['did', 'pid']);

    $result = $this->orcaProcessor->saveInvoicedExpensePayment($this->fund_id, $scenario_data->payment);

    $this->assertNotEmpty($result);
    $this->assertTrue($result->success);

    $savedPayment = $result->{'invoiced-expense-payment'};
    foreach ($scenario_data->payment as $key => $value) {
      $this->assertSame($value, $savedPayment->$key ?? null, $key);
    }
    $this->assertSame(3, $savedPayment->nettype);
    $this->assertSame(0, $savedPayment->aggtype);

    $this->assertAccountBalance($this->trankeygen_map[100], 1000-11.01);
    $this->assertAccountBalance($this->trankeygen_map[201], -990);
    $this->assertAccountBalance($this->trankeygen_map[202], 1.01);
  }

  public function testSaveVendorDebtAdjustment(){
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/vendor-debt-adjustment.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createExpenditureEvents($scenario_data->expenditureEvent ?? []);

    $this->setTrankeygenIds($scenario_data->adjustment, ['pid']);

    //$query = $this->dm->db->executeQuery('select * from private.vreceipts where fund_id = :fund_id', ['fund_id'=>$this->fund_id])->fetchAllAssociative();

    $result = $this->orcaProcessor->saveInvoicedExpenseAdjustment($this->fund_id, $scenario_data->adjustment);
    $this->assertNotEmpty($result);
    $property = "invoiced-expense-adjustment";
    $this->assertNotEmpty($result->$property->contid);

    $this->assertAccountBalance($this->trankeygen_map[201], -600);
    $this->assertAccountBalance($this->trankeygen_map[5000], 600);

    $this->assertNotEmpty($result);
    $this->assertTrue($result->success);
  }
}