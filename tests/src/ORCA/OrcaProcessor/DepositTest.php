<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Exception;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class DepositTest extends ORCATestCase {
  public function testMakeADeposit()
  {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/deposits/deposit.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // save the deposit
    $expectedDeposit = $scenario_data->deposit;
    $this->setTrankeygenIds($expectedDeposit, ['account_id']);
    $this->setTrankeygenIds($expectedDeposit->depositItems, ['trankeygen_id']);
    $expectedDepositItemsOrca = $expectedDeposit->depositItemsOrca;
    $savedDeposit = $this->orcaProcessor->saveDeposit($this->fund_id, $scenario_data->deposit);

    $this->assertNotEmpty($savedDeposit);
    $this->assertTrue($savedDeposit->success);
    $this->assertNotEmpty($savedDeposit->depositEvent);
    $saved = $savedDeposit->depositEvent;
    $this->assertEquals($expectedDeposit->account_id, $saved->account_id);
    $this->assertEquals($expectedDeposit->date_, $saved->date_);
    $this->assertEquals($expectedDeposit->deptkey, $saved->deptkey);
    $this->assertEquals($expectedDeposit->memo, $saved->memo);
    $this->assertEquals(2, count($saved->depositItems));
    $this->assertEquals($this->trankeygen_map[101], $saved->depositItems[0]->trankeygen_id);
    $this->assertEquals($this->trankeygen_map[103], $saved->depositItems[1]->trankeygen_id);
    $this->assertEquals($expectedDepositItemsOrca, $saved->depositItemsOrca);
    $this->assertNotEmpty($saved->trankeygen_id);


    // Check the receipts to make sure they have a cid of the account for the deposit
    // And a deptkey that was generated for the deposit events
    $receipts = $this->dm->db->fetchAllAssociative(
      "SELECT * 
             from private.vdepositevents de 
                join private.deposititems di on de.trankeygen_id=di.deptid join private.receipts r on r.trankeygen_id=di.rcptid
             where de.fund_id = :fund_id",
      ['fund_id' => $this->fund_id]
    );

    // Make sure deptkey is saved
    $this->assertEquals(3, count($receipts));
    foreach ($receipts as $receipt) {
      $this->assertEquals($saved->deptkey, $receipt['deptkey']);
      if ($receipt['nettype'] !== 0) {
        $this->assertEquals($expectedDeposit->account_id, $receipt['cid']);
      }
    }
    $this->assertAccountBalance($this->trankeygen_map[1000], 55.00);
    $this->assertAccountBalance($this->trankeygen_map[1600], 25.00);

    $saved->depositItems[0]->trankeygen_id = $this->trankeygen_map[102];
    $saved->date_ = '2022-07-02';

    $savedDeposit = $this->orcaProcessor->saveDeposit($this->fund_id, $saved);
    $expectedDepositItemsOrca = [102, 103];
    $saved = $savedDeposit->depositEvent;
    $this->assertEquals($expectedDepositItemsOrca, $saved->depositItemsOrca);
    $this->assertAccountBalance($this->trankeygen_map[1000], 30.0);
    $this->assertAccountBalance($this->trankeygen_map[1600], 50.0);
    $this->assertEquals(1656720000002, $saved->deptkey);
  }

  public function testDeleteDeposit() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/deposits/deposit.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // save the deposit
    $expectedDeposit = $scenario_data->deposit;
    $this->setTrankeygenIds($expectedDeposit, ['account_id']);
    $this->setTrankeygenIds($expectedDeposit->depositItems, ['trankeygen_id']);
    $savedDeposit = $this->orcaProcessor->saveDeposit($this->fund_id, $scenario_data->deposit)->depositEvent;

    $this->orcaProcessor->deleteDeposit($this->fund_id, $savedDeposit->trankeygen_id);

    // Make sure did is updated on receipt
    $cid = $this->dm->db->fetchOne("SELECT cid FROM private.receipts r where r.trankeygen_id=:id", ['id' => $this->trankeygen_map[101]]);
    $this->assertEquals($cid, $this->trankeygen_map[1600]);

    $count = $this->dm->db->fetchOne("SELECT count(1) FROM private.depositevents where trankeygen_id = :id", ['id' => $savedDeposit->trankeygen_id]);
    $this->assertSame(0, $count);
    $this->assertAccountBalance($this->trankeygen_map[1000], 0);
    $this->assertAccountBalance($this->trankeygen_map[1600], 80.0);

    $savedDeposit = $this->orcaProcessor->saveDeposit($this->fund_id, $scenario_data->deposit)->depositEvent;
    $this->dm->db->executeStatement(
      "INSERT INTO report(report_type, fund_id, period_start, period_end, external_id)
          VALUES('C3', :fund_id, :deposit_date, :deposit_date, :external_id)",
      ["fund_id" => $this->fund_id, 'deposit_date' => $savedDeposit->date_, 'external_id' => $savedDeposit->trankeygen_id]
    );

    try {
      $this->orcaProcessor->deleteDeposit($this->fund_id, $savedDeposit->trankeygen_id);
      $this->assertFalse(true);
    } catch (Exception $e) {
      $this->assertStringContainsString('Cannot delete deposit that has reports', $e->getMessage());
    }
  }
}