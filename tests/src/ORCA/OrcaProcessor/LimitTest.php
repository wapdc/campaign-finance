<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Exception;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class LimitTest extends ORCATestCase {


  /**
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   * @throws \Doctrine\DBAL\Exception
   * @throws Exception
   */
  public function testSaveFundLimits()
  {
    //Get three jurisdiction ids to test the different limits cases
    $testDataDir = $this->data_dir;
    $scenarioData = Yaml::parseFile("$testDataDir/funds/generate-fund-candidate.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $this->createCommittee($scenarioData);
    $response_data = $this->orcaProcessor->generateOrcaFund($this->committee->committee_id, $scenarioData->userData, 'TEST');

    // Check to make sure a fund was created
    $fund = $response_data->fund;
    $committee_id = $fund->committee->committee_id;

    //test with judicial jurisdiction/office
    $this->dm->db->executeQuery('update candidacy set jurisdiction_id = 5119, office_code = 17 where committee_id = :committee_id', ['committee_id' => $committee_id]);
    $this->dm->db->executeQuery('delete from private.limits where fund_id = :fund_id', ['fund_id' => $fund->fund_id]);
    //test populate limits sql function
    $this->dm->db->executeQuery('select * from private.cf_populate_limits(:fund_id)', ['fund_id' => $fund->fund_id]);
    $limits = $this->dm->db->executeQuery('select * from private.limits where fund_id = :fund_id', ['fund_id' => $fund->fund_id])->fetchAllAssociative();
    //grab all contribution limits
    $contribution_limits = $this->dm->db->fetchAllAssociative("select * from contribution_limit where office_type = 'JD'");
    $expected_couple_limit = $contribution_limits[array_search('CPL', array_column($contribution_limits, 'type_code'))];
    $expected_noncouple_limit = $contribution_limits[array_search('IND', array_column($contribution_limits, 'type_code'))];
    $this->assertCount(11, $limits);
    foreach ($limits as $limit) {
      $this->assertEquals(0, $limit['aggbyvoters'], $limit['name']);
      if ($limit['name'] === 'Couple') {
        $this->assertEquals($expected_couple_limit['amount'], $limit['amount']);
      } else {
        $this->assertEquals($expected_noncouple_limit['amount'], $limit['amount']);
      }
    }

    $limitName = array('Union', 'Political Action Committee', 'Other Entities', 'Individuals', 'Business');
    //test with port jurisdiction/office
    $this->dm->db->executeQuery('update candidacy set jurisdiction_id = 3942, office_code = 47 where committee_id = :committee_id', ['committee_id' => $committee_id]);
    $this->dm->db->executeQuery('delete from private.limits where fund_id = :fund_id', ['fund_id' => $fund->fund_id]);
    $this->dm->db->executeQuery('select * from private.cf_populate_limits(:fund_id)', ['fund_id' => $fund->fund_id]);
    $limits = $this->dm->db->executeQuery('select * from private.limits where fund_id = :fund_id', ['fund_id' => $fund->fund_id])->fetchAllAssociative();
    $contribution_limits = $this->dm->db->fetchAllAssociative("select * from contribution_limit where office_type = 'PC'");
    $expected_couple_limit = $contribution_limits[array_search('CPL', array_column($contribution_limits, 'type_code'))];
    $expected_individual_limit = $contribution_limits[array_search('IND', array_column($contribution_limits, 'type_code'))];
    $expected_district_limit = $contribution_limits[array_search('LDP', array_column($contribution_limits, 'type_code'))];
    $expected_party_limit = $contribution_limits[array_search('MP', array_column($contribution_limits, 'type_code'))];
    foreach ($limits as $limit) {
      if ($limit['name'] === 'Couple') {
        $this->assertEquals($expected_couple_limit['amount'], $limit['amount']);
      } else if (in_array($limit['name'], $limitName)) {
        $this->assertEquals($expected_individual_limit['amount'], $limit['amount']);
      } else {
        if (in_array($limit['type'], ['CP', 'LDP'])) {
          $this->assertEquals($expected_district_limit['amount'], $limit['amount']);
        } else {
          $this->assertEquals($expected_party_limit['amount'], $limit['amount']);
        }
        $this->assertEquals(1, $limit['aggbyvoters']);
        $this->assertNotNull($limit['amount']);
      }
    }

    //test with local jurisdiction
    $this->dm->db->executeQuery('update candidacy set jurisdiction_id = 4679, office_code = 31 where committee_id = :committee_id', ['committee_id' => $committee_id]);
    $this->dm->db->executeQuery('delete from private.limits where fund_id = :fund_id', ['fund_id' => $fund->fund_id]);
    $this->dm->db->executeQuery('select * from private.cf_populate_limits(:fund_id)', ['fund_id' => $fund->fund_id]);
    $limits = $this->dm->db->executeQuery('select * from private.limits where fund_id = :fund_id', ['fund_id' => $fund->fund_id])->fetchAllAssociative();
    $contribution_limits = $this->dm->db->fetchAllAssociative("select * from contribution_limit where office_type = 'CL'");
    $expected_couple_limit = $contribution_limits[array_search('CPL', array_column($contribution_limits, 'type_code'))];
    $expected_individual_limit = $contribution_limits[array_search('IND', array_column($contribution_limits, 'type_code'))];
    $expected_district_limit = $contribution_limits[array_search('LDP', array_column($contribution_limits, 'type_code'))];
    $expected_party_limit = $contribution_limits[array_search('MP', array_column($contribution_limits, 'type_code'))];
    foreach ($limits as $limit) {
      if ($limit['name'] === 'Couple') {
        $this->assertEquals($expected_couple_limit['amount'], $limit['amount']);
      } else if (in_array($limit['name'], $limitName)) {
        $this->assertEquals($expected_individual_limit['amount'], $limit['amount']);
      } else {
        if (in_array($limit['type'], ['CP', 'LDP'])) {
          $this->assertEquals($expected_district_limit['amount'], $limit['amount']);
        } else {
          $this->assertEquals($expected_party_limit['amount'], $limit['amount']);
        }
        $this->assertEquals(1, $limit['aggbyvoters']);
        $this->assertNotNull($limit['amount']);
      }
    }
  }
}