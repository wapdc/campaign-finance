<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class CreditCardTest extends ORCATestCase {
  /**
   * @throws \Doctrine\DBAL\Exception
   */
  public function testSaveCreditCard()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/accounts/add-credit-card.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = Yaml::parseFile($this->data_dir . '/committees/continuing-pac.yml', Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $savedCreditCard = $this->orcaProcessor->saveAccount($this->fund_id, $scenario_data->credit_card);
    $this->assertNotEmpty($savedCreditCard->account->trankeygen_id);
    $account = $this->dm->db->fetchOne("SELECT contid from private.accounts where trankeygen_id = :trankeygen_id and acctnum = 2600", ['trankeygen_id' => $savedCreditCard->account->trankeygen_id]);
    $this->assertNotEmpty($account);
    $interest_expense = $this->dm->db->fetchOne("SELECT contid from private.accounts where pid = :trankeygen_id and acctnum = 5110", ['trankeygen_id' => $savedCreditCard->account->trankeygen_id]);
    $this->assertNotEmpty($interest_expense);
  }

  public function testSaveCreditCardPayment() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/credit_cards/credit-card-payment.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    $expectedPayment = $scenario_data->creditCardPayment;
    $this->setTrankeygenIds($expectedPayment, ['pid', 'did']);

    $savedPayment = $this->orcaProcessor->saveCreditCardPayment($this->fund_id, $scenario_data->creditCardPayment)->{'credit-card-payment'};

    foreach ($expectedPayment as $property => $value) {
      switch ($property) {
        default:
          $this->assertSame($value, $savedPayment->$property ?? NULL, $property);
      }
    }

    $this->assertAccountBalance($this->trankeygen_map[260], -500.0 + $expectedPayment->amount);
    $this->assertAccountBalance($this->trankeygen_map[511], $expectedPayment->interest);
    $this->assertAccountBalance($this->trankeygen_map[100], 1500 - ($expectedPayment->amount + $expectedPayment->interest));
  }
}