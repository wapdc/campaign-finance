<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Symfony\Component\Yaml\Yaml;
use stdClass;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class ContributionTest extends ORCATestCase {
  public function testSaveContribution()
  {
    $expected_balance = 50;
    $scenario_data = Yaml::parseFile($this->data_dir . "/contributions/contributions.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // Anonymous contribution
    $this->setTrankeygenIds($scenario_data->contributions->anonymous, ['cid']);
    $data = $scenario_data->contributions->anonymous;
    $savedAnonymousContribution = $this->orcaProcessor->saveAnonymousContribution($this->fund_id, $scenario_data->contributions->anonymous);
    $this->assertEquals($data->cid, $savedAnonymousContribution->{'anonymous-contribution'}->cid);
    $this->assertEquals($data->amount, $savedAnonymousContribution->{'anonymous-contribution'}->amount);
    $this->assertEquals($data->date_, $savedAnonymousContribution->{'anonymous-contribution'}->date_);
    $this->assertEquals($data->memo, $savedAnonymousContribution->{'anonymous-contribution'}->memo);
    $this->assertEquals(0, $savedAnonymousContribution->{'anonymous-contribution'}->deptkey);
    $expected_balance += $data->amount;
    $this->assertAccountBalance($data->cid, $expected_balance);

    // In-kind contribution
    $this->setTrankeygenIds($scenario_data->contributions->in_kind, ['contid', 'cid']);
    $data = $scenario_data->contributions->in_kind;
    $savedInKindContribution = $this->orcaProcessor->saveInKindContribution($this->fund_id, $scenario_data->contributions->in_kind);
    $this->assertEquals($data->elekey, $savedInKindContribution->{'in-kind-contribution'}->elekey);
    $this->assertEquals($data->contid, $savedInKindContribution->{'in-kind-contribution'}->contid);
    $this->assertEquals($data->amount, $savedInKindContribution->{'in-kind-contribution'}->amount);
    $this->assertEquals($data->date_, $savedInKindContribution->{'in-kind-contribution'}->date_);
    $this->assertEquals($data->itemized, $savedInKindContribution->{'in-kind-contribution'}->itemized);
    $this->assertEquals($data->cid, $savedInKindContribution->{'in-kind-contribution'}->cid);
    $this->assertEquals($data->description, $savedInKindContribution->{'in-kind-contribution'}->description);
    $this->assertEquals($data->memo, $savedInKindContribution->{'in-kind-contribution'}->memo);
    $this->assertNotEmpty( $savedInKindContribution->{'in-kind-contribution'}->deptkey);
    $this->assertAccountBalance($data->cid, $data->amount);

    // Monetary contribution
    $this->setTrankeygenIds($scenario_data->contributions->monetary_contribution, ['contid', 'cid']);
    $data = $scenario_data->contributions->monetary_contribution;
    $savedMonetaryContribution = $this->orcaProcessor->saveMonetaryContribution($this->fund_id, $scenario_data->contributions->monetary_contribution);
    $this->assertEquals($data->elekey, $savedMonetaryContribution->{'monetary-contribution'}->elekey);
    $this->assertEquals($data->contid, $savedMonetaryContribution->{'monetary-contribution'}->contid);
    $this->assertEquals($data->amount, $savedMonetaryContribution->{'monetary-contribution'}->amount);
    $this->assertEquals($data->date_, $savedMonetaryContribution->{'monetary-contribution'}->date_);
    $this->assertEquals($data->itemized, $savedMonetaryContribution->{'monetary-contribution'}->itemized);
    $this->assertEquals($data->checkno, $savedMonetaryContribution->{'monetary-contribution'}->checkno);
    $this->assertEquals($data->memo, $savedMonetaryContribution->{'monetary-contribution'}->memo);
    $this->assertSame(1, $savedMonetaryContribution->{'monetary-contribution'}->aggtype);
    $this->assertEquals(0, $savedMonetaryContribution->{'monetary-contribution'}->deptkey);
    $expected_balance += $data->amount;
    $this->assertAccountBalance($data->cid, $expected_balance);
    // Contribution refund
    $this->setTrankeygenIds($scenario_data->contributions->contribution_refund, ['contid', 'cid', 'pid']);
    $data = $scenario_data->contributions->contribution_refund;


    $savedContributionRefund = $this->orcaProcessor->saveContributionRefund($this->fund_id, json_encode($scenario_data->contributions->contribution_refund));
    $this->assertEquals($data->elekey, $savedContributionRefund->{'refund-contribution'}->elekey);
    $this->assertEquals($data->contid, $savedContributionRefund->{'refund-contribution'}->contid);
    $this->assertEquals($data->amount, $savedContributionRefund->{'refund-contribution'}->amount);
    $this->assertEquals($data->date_, $savedContributionRefund->{'refund-contribution'}->date_);
    $this->assertEquals($data->pid, $savedContributionRefund->{'refund-contribution'}->pid);
    $this->assertEquals($data->checkno, $savedContributionRefund->{'refund-contribution'}->checkno);
    $this->assertEquals($data->memo, $savedContributionRefund->{'refund-contribution'}->memo);
    $this->assertNotEmpty($savedContributionRefund->{'refund-contribution'}->deptkey);
    $expected_balance -= $data->amount;
    $this->assertAccountBalance($data->cid, $expected_balance);
  }

  public function testSplitNewContribution()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/contributions/contribution-split.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->setTrankeygenIds($scenario_data->contributions->anonymous, ['cid']);

    // Monetary contribution
    $this->setTrankeygenIds($scenario_data->contributions->monetary_contribution, ['contid', 'cid']);
    $data = $scenario_data->contributions->monetary_contribution;
    $savedMonetaryContributions = $this->orcaProcessor->addSplitContribution($this->fund_id, $scenario_data->contributions->monetary_contribution);
    foreach ($savedMonetaryContributions->contributions as $savedMonetaryContribution) {
      $this->assertEquals($data->contid, $savedMonetaryContribution->contid);
      if ($savedMonetaryContribution->elekey) {
        $this->assertEquals($data->general_amount, $savedMonetaryContribution->amount);
      } else {
        $this->assertEquals($data->primary_amount, $savedMonetaryContribution->amount);
      }
      $this->assertEquals($data->date_, $savedMonetaryContribution->date_);
      $this->assertEquals($data->itemized, $savedMonetaryContribution->itemized);
      $this->assertEquals($data->checkno, $savedMonetaryContribution->checkno);
      $this->assertEquals($data->memo, $savedMonetaryContribution->memo);
    }
    $this->assertAccountBalance($data->cid, $data->amount);
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   */
  public function testDeleteContribution()
  {
    //Monetary contribution
    $scenario_data = Yaml::parseFile($this->data_dir . "/contributions/contributions.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $contents = $scenario_data->contributions->monetary_contribution;

    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;

    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->setTrankeygenIds($contents, ['contid', 'cid']);

    //save Monetary Contribution
    $savedMonetaryContribution = $this->orcaProcessor->saveMonetaryContribution($this->fund_id, $contents);
    $savedMonetaryContributionId = $savedMonetaryContribution->{'monetary-contribution'}->trankeygen_id;

    $this->assertAccountBalance($contents->cid, 100);

    $this->orcaProcessor->deleteContribution($this->fund_id, $savedMonetaryContributionId);
    // Assert contribution was deleted
    $contribution = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :id', ['id' => $savedMonetaryContributionId])->fetchAllAssociative();
    $this->assertEmpty($contribution);

    // This is 50.00 because the scenario file has a $50 receipt that gets created that is not targeted for deletion.
    $this->assertAccountBalance($contents->cid, 50);
    $this->assertContactGagg($this->trankeygen_map[11], 50);
  }

  /**
   * Tests importing of contributions.
   * @param string $scenario
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testImportContributions($scenario = 'import_contributions')
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/contributions/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts);
    $this->setTrankeygenIds($scenario_data->import_message, ['duplicate_id', 'contid']);
    $results = $this->orcaProcessor->importContributions($this->fund_id, $scenario_data->import_message ?? []);
    $rows = $this->dm->db->executeQuery('select * from private.vcontacts where fund_id = :fund_id', ['fund_id' => $this->fund_id])->fetchAllAssociative();

    // One contact should be existing so we should have a 1 less contacts that were created as a result of hte import
    $this->assertCount(count($scenario_data->import_message) - 1, $results->contacts);

    // One of the two existing contacts should match so we should have one more than receipts entries.
    $this->assertCount(count($scenario_data->import_message) + 1, $rows);

    // Make sure all receipts were imported.
    $this->assertCount(count($scenario_data->import_message), $results->receipts);
  }

  public function testInKindPledgePayment() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/pledge-in-kind-payment.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts);
    $this->createAccountEntries($scenario_data->accounts);
    $this->createReceipts($scenario_data->receipts);

    $payment = $scenario_data->in_kind;
    $this->setTrankeygenIds($payment, ['pid', 'contid', 'cid']);

    $savedContribution = $this->orcaProcessor->saveInKindContribution($this->fund_id, $payment)
      ->{'in-kind-contribution'};
    $this->assertNotEmpty($savedContribution->trankeygen_id);
    $this->assertSame($payment->date_, $savedContribution->date_);
    $this->assertSame($payment->contid, $savedContribution->contid);
    $this->assertSame($payment->memo, $savedContribution->memo);
    $this->assertSame($payment->description, $savedContribution->description);
    $this->assertSame($payment->itemized, $savedContribution->itemized);
    $this->assertSame($payment->cid, $savedContribution->cid);
    $this->assertSame($this->trankeygen_map[102], $savedContribution->pid);

    $this->assertAccountBalance($this->trankeygen_map[101], 150);
    $this->assertAccountBalance($this->trankeygen_map[5000], 50);
  }

  public function testDeleteMonetaryGroupContributionParentWhenLastChildIsDeleted() {
    // Initial conditions
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // create one parent and one child receipt
    $scenario_data = Yaml::parseFile($this->data_dir . "/receipts/delete_receipt_parent_when_last_child_is_deleted.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $this->createReceipts($scenario_data->receipts);
    $this->setTrankeygenIds($scenario_data->delete, ['receipt_id']);
    $map = $this->trankeygen_map;

    $child_receipt = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where fund_id=:fund_id and pid is not null",
      ['fund_id' => $this->fund_id]
    )[0];

    $receipts = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where fund_id=:fund_id",
      ['fund_id' => $this->fund_id]
    );
    $this->assertCount(3, $receipts);
    // delete a child receipt and since there are two children, the parent is not deleted
    $this->dm->db->executeQuery(
      'select private.cf_delete_receipt(:id)', ['id' => $child_receipt['trankeygen_id']]
    );

    $child_receipt = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where fund_id=:fund_id and pid is not null",
      ['fund_id' => $this->fund_id]
    )[0];

    $receipts = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where fund_id=:fund_id",
      ['fund_id' => $this->fund_id]
    );
    $this->assertCount(2, $receipts);

    // delete the last child receipt and watch as the parent is also deleted
    $this->dm->db->executeQuery(
      'select private.cf_delete_receipt(:id)', ['id' => $child_receipt['trankeygen_id']]
    );

    $receipts = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where fund_id=:fund_id",
      ['fund_id' => $this->fund_id]
    );

    $this->assertCount(0, $receipts);
  }

  public function testMonetaryGroupContribution() {
    // setup environment
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    $scenario_data = Yaml::parseFile($this->data_dir . "/contributions/monetary_group_contribution.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $this->createAccountEntries($scenario_data->accounts);
    $this->createContacts($scenario_data->contacts);

    // map ids
    $this->setTrankeygenIds($scenario_data->addMembers, ['trankeygen_id']);
    $map = $this->trankeygen_map;
    $scenario_data->addMembers->add = array_map(
      function ($element) use ($map) {
        return $map[$element];
      },
      $scenario_data->addMembers->add
    );
    $scenario_data->createContribution->exclude_members = array_map(
      function ($member) use ($map) {
        return $map[$member];
      },
      $scenario_data->createContribution->exclude_members
    );
    $scenario_data->createContribution->group_contid = $map[4];

    $this->orcaProcessor->modifyGroupMembers($this->fund_id, $scenario_data->addMembers);

    // test: creating a group contribution with one individual child contribution
    $this->orcaProcessor->saveMonetaryGroupContribution($this->fund_id, $scenario_data->createContribution);

    $group_contribution = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where contid={$scenario_data->createContribution->group_contid}"
    )[0];

    $individual_contributions = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where pid={$group_contribution['trankeygen_id']}"
    );

    $this->assertEquals(3.00, $group_contribution['amount']);
    $this->assertCount(1, $individual_contributions);
    $this->assertAccountBalance($this->trankeygen_map[1600], 3.00);
    $this->assertAccountBalance($this->trankeygen_map[4000], -3.00);
    $this->assertContactGagg($this->trankeygen_map[2], 3.00);

    $update_contribution = $scenario_data->createContribution;
    $update_contribution->trankeygen_id = $group_contribution['trankeygen_id'];
    $update_contribution->exclude_members = [];

    // test: "adding" individual child contributions by having an empty exclusion list
    $this->orcaProcessor->saveMonetaryGroupContribution($this->fund_id, $update_contribution);

    $individual_contributions = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where pid={$group_contribution['trankeygen_id']}"
    );
    $this->assertCount(3, $individual_contributions);
    $this->assertAccountBalance($this->trankeygen_map[1600], 9.00);
    $this->assertAccountBalance($this->trankeygen_map[4000], -9.00);
    $this->assertContactGagg($this->trankeygen_map[1], 3.00);
    $this->assertContactGagg($this->trankeygen_map[2], 3.00);
    $this->assertContactGagg($this->trankeygen_map[3], 3.00);

    $update_contribution = $scenario_data->createContribution;
    $update_contribution->trankeygen_id = $group_contribution['trankeygen_id'];
    $update_contribution->exclude_members = [$map[1]];

    // test: "removing" individual child contributions by having a person removed in the exclusion list
    $this->orcaProcessor->saveMonetaryGroupContribution($this->fund_id, $update_contribution);

    $individual_contributions = $this->dm->db->fetchAllAssociative(
      "select * from private.vreceipts where pid={$group_contribution['trankeygen_id']}"
    );
    $this->assertCount(2, $individual_contributions);
    $this->assertAccountBalance($this->trankeygen_map[1600], 6.00);
    $this->assertAccountBalance($this->trankeygen_map[4000], -6.00);
    $this->assertContactGagg($this->trankeygen_map[2], 3.00);
    $this->assertContactGagg($this->trankeygen_map[3], 3.00);
  }

  public function testUpdateCarryForward()
  {
    $fund_id = -171;
    $contribution_id = -543212;
    $second_contribution_id = -212534;
    $debit_id = -55432;
    $this->dm->db->executeQuery('insert into private.trankeygen (trankeygen_id, fund_id, orca_id) values (:contribution_id, :fund_id, 2)',
      ['contribution_id' => $contribution_id, 'fund_id' => $fund_id]);
    $this->dm->db->executeQuery('insert into private.trankeygen (trankeygen_id, fund_id, orca_id) values (:second_contribution_id, :fund_id, 2)',
      ['second_contribution_id' => $second_contribution_id, 'fund_id' => $fund_id]);
    $this->dm->db->executeQuery('insert into private.trankeygen (trankeygen_id, fund_id) values (:debit_id, :fund_id)',
      ['debit_id' => $debit_id, 'fund_id' => $fund_id]);

    $this->dm->db->executeQuery('insert into private.accounts (trankeygen_id, acctnum) values (:contribution_id, 1000)',
      ['contribution_id' => $contribution_id]);
    $this->dm->db->executeQuery('insert into private.accounts (trankeygen_id, acctnum) values (:contribution_id, 1000)',
      ['contribution_id' => $second_contribution_id]);
    $this->dm->db->executeQuery('insert into private.accounts (trankeygen_id, acctnum) values (:debit_id, 4600)',
      ['debit_id' => $debit_id]);

    $request_data = new stdClass();
    $request_data->fund_id = $fund_id;
    $request_data->amount = 45;
    $request_data->memo = 'Testing this creation';
    $request_data->cid = $contribution_id;

    $response = $this->orcaProcessor->updateCarryForward($fund_id, $request_data);
    $receipt = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :trankeygen_id',
      ['trankeygen_id' => $response->trankeygen_id])->fetchAssociative();

    $this->assertEquals($request_data->amount, $receipt['amount']);
    $this->assertEquals($request_data->memo, $receipt['memo']);
    $this->assertEquals($contribution_id, $receipt['cid']);
    $this->assertEquals($debit_id, $receipt['did']);

    $request_data->amount = 91;
    $request_data->memo = 'Testing this update';
    $request_data->cid = $second_contribution_id;

    $response = $this->orcaProcessor->updateCarryForward($fund_id, $request_data);
    $receipt = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :trankeygen_id',
      ['trankeygen_id' => $response->trankeygen_id])->fetchAssociative();

    $this->assertAccountBalance($receipt['cid'], $receipt['amount']);
    $this->assertAccountBalance($receipt['did'], -136);
    $this->assertEquals($request_data->amount, $receipt['amount']);
    $this->assertEquals($request_data->memo, $receipt['memo']);
    $this->assertEquals($request_data->cid, $receipt['cid']);
  }

  /**
   * Test save personal funds
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @note Since personal fund is similar to monetary contribution, the processor call uses saveMonetaryContribution call.
   */
  public function testSavePersonalFund()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/funds/personalfund.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    // Personal fund contribution
    $this->setTrankeygenIds($scenario_data->contributions->personal, ['contid', 'cid']);
    $data = $scenario_data->contributions->personal;
    $savedPersonalFund = $this->orcaProcessor->saveMonetaryContribution($this->fund_id, $scenario_data->contributions->personal);
    $this->assertEquals($data->type, $savedPersonalFund->{'monetary-contribution'}->type);
    $this->assertEquals($data->elekey, $savedPersonalFund->{'monetary-contribution'}->elekey);
    $this->assertEquals($data->contid, $savedPersonalFund->{'monetary-contribution'}->contid);
    $this->assertEquals($data->amount, $savedPersonalFund->{'monetary-contribution'}->amount);
    $this->assertEquals($data->date_, $savedPersonalFund->{'monetary-contribution'}->date_);
    $this->assertEquals($data->memo, $savedPersonalFund->{'monetary-contribution'}->memo);
  }
}