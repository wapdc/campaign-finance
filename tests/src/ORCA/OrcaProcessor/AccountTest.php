<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class AccountTest extends ORCATestCase {
  public function accountAdjust($fund_id, $old_did, $old_cid, $old_amount, $new_did, $new_cid, $new_amount)
  {
    $this->dm->db->executeStatement("SELECT private.cf_account_adjust(:fund_id, cast(:old_did as integer), cast(:old_cid as integer), cast(:old_amount as numeric), :new_did, :new_cid, cast(:new_amount as numeric))",
      [
        'fund_id' => $fund_id,
        'old_did' => $old_did,
        'old_cid' => $old_cid,
        'old_amount' => $old_amount,
        'new_did' => $new_did,
        'new_cid' => $new_cid,
        'new_amount' => $new_amount,
      ]
    );
  }

  /**
   * This unit test covers a database function that is not actually used directly by the ORCA Processor.
   * It is here to make sure the counting works
   */
  public function testAccountAdjust()
  {
    // Load test scenario data
    $contents = file_get_contents($this->data_dir . "/expenditures/expense.json");
    $data = json_decode($contents);
    $fund_id = -17;
    $this->makeContactsForJSON($fund_id, $data);

    // Mimic initial creation of receipt.
    $this->accountAdjust($fund_id, null, null, null, -1, -3, 100.0);
    $this->assertAccountBalance(-1, -100.0);
    $this->assertAccountBalance(-3, 100.0);
    $this->assertAccountBalance(-6, 0.0);

    // Mimic changing the receipt to 50.
    $this->accountAdjust($fund_id, -1, -3, 100.0, -1, -3, 50.00);
    $this->assertAccountBalance(-1, -50.0);
    $this->assertAccountBalance(-3, 50.0);
    $this->assertAccountBalance(-6, 0.0);

    // Mimic just editing the account
    $this->accountAdjust($fund_id, -1, -3, 50.0, -1, -6, 50.00);
    $this->assertAccountBalance(-1, -50.0);
    $this->assertAccountBalance(-3, 0.0);
    $this->assertAccountBalance(-6, 50.0);

    // Mimic editing and changing balance and cid.
    $this->accountAdjust($fund_id, -1, -6, 50.00, -1, -3, 25.53);
    $this->assertAccountBalance(-1, -25.53);
    $this->assertAccountBalance(-3, 25.53);
    $this->assertAccountBalance(-6, 0.0);

    // Mimic changing everything
    $this->accountAdjust($fund_id, -1, -3, 25.53, -3, -6, 200.00);
    $this->assertAccountBalance(-1, 0.0);
    $this->assertAccountBalance(-3, -200.0);
    $this->assertAccountBalance(-6, 200.0);
  }

  public function testSaveAccount() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/accounts/account.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    $result = $this->orcaProcessor->saveAccount($this->fund_id, $scenario_data->account);

    $this->assertIsObject($result->account);
    foreach ($scenario_data->account as $key => $value) {
      $this->assertSame($value, $result->account->$key, $key);
    }
  }

  public function testSaveAccountTransfer()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/accounts/account_transfer.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // account transfer
    $this->setTrankeygenIds($scenario_data->accounting->account_transfer, ['cid', 'did']);
    $data = $scenario_data->accounting->account_transfer;
    $savedAccountTransfer = $this->orcaProcessor->saveAccountTransfer($this->fund_id, $scenario_data->accounting->account_transfer);
    $this->assertNotEmpty($savedAccountTransfer);
    $this->assertEquals($data->amount, $savedAccountTransfer->{'account-transfer'}->amount);
    $this->assertEquals($data->date_, $savedAccountTransfer->{'account-transfer'}->date_);
    $this->assertEquals($data->memo, $savedAccountTransfer->{'account-transfer'}->memo);
    $this->assertEquals($data->did, $savedAccountTransfer->{'account-transfer'}->did);
    $this->assertEquals($data->cid, $savedAccountTransfer->{'account-transfer'}->cid);

    $this->assertAccountBalance($this->trankeygen_map[5600], 75);
    $this->assertAccountBalance($this->trankeygen_map[5500], 25);
  }
}