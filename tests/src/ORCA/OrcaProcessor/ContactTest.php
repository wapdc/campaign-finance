<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Exception;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class ContactTest extends ORCATestCase {
  function testSaveContact() {
    $fund_id = -3;

    //Other contact
    $contents = file_get_contents($this->data_dir . "/contacts/other-contact.json");
    $data = json_decode($contents);
    $savedContact = $this->orcaProcessor->saveContact($fund_id, $contents)->contact;
    $this->assertEquals($data->name, $savedContact->name);
    $this->assertEquals($data->type, $savedContact->type);
    $this->assertEquals($data->street, $savedContact->street);
    $this->assertEquals($data->city, $savedContact->city);
    $this->assertEquals($data->state, $savedContact->state);
    $this->assertEquals($data->zip, $savedContact->zip);
    $this->assertEquals($data->phone, $savedContact->phone);
    $this->assertEquals($data->email, $savedContact->email);
    $this->assertEquals($data->memo, $savedContact->memo);

    //Individual contact
    $contents = file_get_contents($this->data_dir . "/contacts/individual-contact.json");
    $data = json_decode($contents);
    $savedContact = $this->orcaProcessor->saveContact($fund_id, $contents)->contact;
    $this->assertEquals($data->prefix, $savedContact->prefix);
    $this->assertEquals($data->firstname, $savedContact->firstname);
    $this->assertEquals($data->middleinitial, $savedContact->middleinitial);
    $this->assertEquals($data->lastname, $savedContact->lastname);
    $this->assertEquals($data->suffix, $savedContact->suffix);
    $this->assertEquals($data->type, $savedContact->type);
    $this->assertEquals($data->street, $savedContact->street);
    $this->assertEquals($data->city, $savedContact->city);
    $this->assertEquals($data->state, $savedContact->state);
    $this->assertEquals($data->zip, $savedContact->zip);
    $this->assertEquals($data->phone, $savedContact->phone);
    $this->assertEquals($data->email, $savedContact->email);
    $this->assertEquals($data->memo, $savedContact->memo);
    $this->assertEquals($data->occupation, $savedContact->occupation);
    $this->assertEquals($data->employername, $savedContact->employername);
    $this->assertEquals($data->employerstreet, $savedContact->employerstreet);
    $this->assertEquals($data->employercity, $savedContact->employercity);
    $this->assertEquals($data->employerstate, $savedContact->employerstate);
    $this->assertEquals($data->employerzip, $savedContact->employerzip);

    //Couple contact
    $contact1_id = $savedContact->trankeygen_id;
    $contact2_id = $this->orcaProcessor->saveContact($fund_id, $contents)->contact->trankeygen_id;
    $contents = file_get_contents($this->data_dir . "/contacts/couple-contact.json");
    $data = json_decode($contents);
    $data->contact1_id = $contact1_id;
    $data->contact2_id = $contact2_id;
    $contents = json_encode($data);
    $savedContact = $this->orcaProcessor->saveContact($fund_id, $contents)->contact;
    $this->assertEquals($contact1_id, $savedContact->contact1_id);
    $this->assertEquals($contact2_id, $savedContact->contact2_id);

    //contact_types:
    //(business,
    // co party committee,
    // financial institution,
    // leg caucus committee,
    // leg dist party committee,
    // minor party committee,
    //other organization ,
    // political action committee,
    // st party committee,
    // tribal political committee,
    // union)
  }

  /**
   * @param mixed $scenario
   * @covers \WAPDC\CampaignFinance\OrcaProcessor::importContacts
   * @throws Exception
   */
  public function testImportContacts($scenario = 'import_contacts')
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/contacts/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts);
    $this->setTrankeygenIds($scenario_data->bulkcontacts, ['duplicate_id', 'trankeygen_id']);
    $bulkContactResults = $this->orcaProcessor->importContacts($this->fund_id, $scenario_data->bulkcontacts ?? []);
    $rows = $this->dm->db->executeQuery('select * from private.vcontacts where fund_id = :fund_id', ['fund_id' => $this->fund_id])->fetchAllAssociative();
    $this->assertCount(count($scenario_data->bulkcontacts), $bulkContactResults->contacts);

    // One of the two existing contacts should match so we should have one more than is in the bulk save.
    $this->assertCount(count($scenario_data->bulkcontacts) + 1, $rows);

    // Make sure we have one duplicate that is marked as a duplicate.
    $duplicates = $this->dm->db->executeQuery("SELECT * from private.contact_duplicates d join private.trankeygen t on t.trankeygen_id = d.contact_id where t.fund_id = :fund_id",
      ['fund_id' => $this->fund_id])->fetchAllAssociative();
    $this->assertCount(1, $duplicates);
  }

  /**
   * @param mixed $scenario
   * @covers \WAPDC\CampaignFinance\OrcaProcessor::mergeContact
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function testMergeContacts($scenario = 'merge_contacts')
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/contacts/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts);
    $this->createCoupleContacts($scenario_data->couplecontacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createExpenditureEvents($scenario_data->expenditureevents ?? []);

    $contact_id = $this->trankeygen_map[$scenario_data->contacts[0]->id];
    $duplicate_id = $this->trankeygen_map[$scenario_data->contacts[1]->id];

    // call the orca-processors merge function to do the merging
    $this->orcaProcessor->mergeContacts($this->fund_id, $contact_id, $duplicate_id, true);

    // Test merge results;
    $contacts = $this->dm->db->executeQuery('select count(1) from private.vcontacts where fund_id = :fund_id and trankeygen_id=:contact_id', ['fund_id' => $this->fund_id, 'contact_id' => $duplicate_id])->fetchOne();
    // One of the two existing contacts should match as a duplicate so we should have one less than is in the contacts.
    $this->assertEquals(0, $contacts);

    // check and make sure that all expenditure events have transferred properly to the new contact - ids moved
    $exp_events = $this->dm->db->executeQuery('select trankeygen_id from private.vexpenditureevents where contid = :contact_id', ['contact_id' => $contact_id])->fetchAllAssociative();
    $this->assertCount(count($scenario_data->expenditureevents), $exp_events);

    // do the same for receipts - ids moved
    $receipts = $this->dm->db->executeQuery('select trankeygen_id from private.vreceipts where contid = :contact_id', ['contact_id' => $contact_id])->fetchAllAssociative();
    $this->assertCount(count($scenario_data->receipts), $receipts);

    // for couples as well (two ids - have to be moved)
    $couples = $this->dm->db->executeQuery(
      "select trankeygen_id from private.couplecontacts  where contact1_id = :contact_id or contact2_id=:contact_id",
      ['contact_id' => $contact_id]
    )->fetchAllAssociative();
    $this->assertCount(1, $couples);
  }

  /**
   * @param mixed $scenario
   * @covers \WAPDC\CampaignFinance\OrcaProcessor::mergeContact
   * @throws Exception
   */
  public function testVerifyNonDuplicateContacts($scenario = 'merge_contacts')
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/contacts/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createExpenditureEvents($scenario_data->expenditureevents ?? []);

    $contact_id = $this->trankeygen_map[$scenario_data->contacts[0]->id];
    $duplicate_id = $this->trankeygen_map[$scenario_data->contacts[1]->id];

    $this->dm->db->executeStatement(
      "INSERT INTO private.contact_duplicates(contact_id, duplicate_id, verified_not_duplicate) values (:contact_id, :duplicate_id, false) ",
      ['contact_id' => $contact_id, 'duplicate_id' => $duplicate_id]
    );

    // call the orca-processors merge function to do the merging
    $this->orcaProcessor->mergeContacts($this->fund_id, $contact_id, $duplicate_id, false);

    // Test merge results;
    $contacts = $this->dm->db->executeQuery('select count(1) from private.vcontacts where fund_id = :fund_id and trankeygen_id=:contact_id',
      ['fund_id' => $this->fund_id, 'contact_id' => $duplicate_id])->fetchOne();
    // One of the two existing contacts should match as a duplicate so we should have one less than is in the contacts.
    $this->assertEquals(1, $contacts);

    $verified = $this->dm->db->executeQuery(
      "select verified_not_duplicate from private.contact_duplicates where contact_id=:contact_id and duplicate_id=:duplicate_id",
      ['contact_id' => $contact_id, 'duplicate_id' => $duplicate_id]
    )->fetchOne();
    $this->assertEquals(true, $verified);
  }

  /**
   * @param string $scenario
   * @throws Exception|\Doctrine\DBAL\Driver\Exception
   */
  public function testDeleteContact($scenario = 'delete_contact')
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/contacts/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts);
    $this->createCoupleContacts($scenario_data->couplecontacts ?? []);
    $this->createGroupContacts($scenario_data->registeredgroupcontacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createLoans($scenario_data->loans ?? []);
    $this->assertTrue(true);

    $noTransactionsContact = $this->trankeygen_map[$scenario_data->contacts[0]->id];
    $receiptContact = $this->trankeygen_map[$scenario_data->contacts[1]->id];
    $loanContact = $this->trankeygen_map[$scenario_data->contacts[2]->id];
    $coupleContact = $this->trankeygen_map[$scenario_data->contacts[3]->id];
    $groupContact = $this->trankeygen_map[$scenario_data->contacts[6]->id];

    //delete the contact that has no transactions
    $deletion = $this->orcaProcessor->deleteContact($this->fund_id, $noTransactionsContact);
    $this->assertTrue($deletion->success);
    $deletedContact = $this->dm->db->executeQuery('select * from private.contacts where trankeygen_id = :trankeygen_id', ["trankeygen_id" => $noTransactionsContact])->fetchAllAssociative();
    $this->assertEmpty($deletedContact);

    //delete contact with receipt should have a failure
    try {
      $this->orcaProcessor->deleteContact($this->fund_id, $receiptContact);
      $this->assertTrue((bool)false); //Adding this to make sure we are correctly throwing exception
    } catch (Exception $e) {
      $deletedContact = $this->dm->db->executeQuery('select * from private.contacts where trankeygen_id = :trankeygen_id', ["trankeygen_id" => $receiptContact])->fetchAllAssociative();
      $this->assertNotEmpty($deletedContact);
      $this->assertNotNull($e);
    }

    //delete contact with loan should also have a failure
    try {
      $this->orcaProcessor->deleteContact($this->fund_id, $loanContact);
      $this->assertTrue((bool)false); //make sure we run into exception
    } catch (Exception $e) {
      $deletedContact = $this->dm->db->executeQuery('select * from private.contacts where trankeygen_id = :trankeygen_id', ["trankeygen_id" => $loanContact])->fetchAllAssociative();
      $this->assertNotEmpty($deletedContact);
      $this->assertNotNull($e);
    }

    //delete member of couple contact
    try {
      $this->orcaProcessor->deleteContact($this->fund_id, $coupleContact);
      $this->assertTrue((bool)false); //make sure we run into exception
    } catch (Exception $e) {
      $deletedContact = $this->dm->db->executeQuery('select * from private.contacts where trankeygen_id = :trankeygen_id', ["trankeygen_id" => $coupleContact])->fetchAllAssociative();
      $this->assertNotEmpty($deletedContact);
      $this->assertNotNull($e);
    }
    $this->orcaProcessor->deleteContact($this->fund_id, $groupContact);
    $this->assertTrue($deletion->success);
    $deletedGroupContact = $this->dm->db->executeQuery('select * from private.contacts where trankeygen_id = :trankeygen_id', ["trankeygen_id" => $groupContact])->fetchAllAssociative();
    $this->assertEmpty($deletedGroupContact);
    $deletedGroupMemberContact = $this->dm->db->executeQuery('select * from private.registeredgroupcontacts where gconid = :gconid', ["gconid" => $groupContact])->fetchAllAssociative();
    $this->assertEmpty($deletedGroupMemberContact);
  }

  public function testModifyGroupMembers() {
    // Initial conditions
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    $scenario_data = Yaml::parseFile($this->data_dir . "/contacts/modify-group-members.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $this->createContacts($scenario_data->contacts);

    $this->setTrankeygenIds($scenario_data->addMembers, ['trankeygen_id']);
    $map = $this->trankeygen_map;
    $scenario_data->addMembers->add = array_map(function ($element) use ($map) {return $map[$element];}, $scenario_data->addMembers->add);

    // Add some group members
    $this->orcaProcessor->modifyGroupMembers($this->fund_id, $scenario_data->addMembers);

    $contacts = $this->dm->db->fetchAllAssociative("select * from private.registeredgroupcontacts r where r.gconid=:id order by r.contid", ['id' => $this->trankeygen_map[4]]);
    $this->assertCount(3, $contacts);

    // Add some group members again to make sure we don't get dup key violations
    $this->orcaProcessor->modifyGroupMembers($this->fund_id, $scenario_data->addMembers);

    $contacts = $this->dm->db->fetchAllAssociative("select * from private.registeredgroupcontacts r where r.gconid=:id order by r.contid", ['id' => $this->trankeygen_map[4]]);
    $this->assertCount(3, $contacts);

    // Remove some group members;
    $this->setTrankeygenIds($scenario_data->removeMembers, ['trankeygen_id']);
    $map = $this->trankeygen_map;
    $scenario_data->removeMembers->remove = array_map(function ($element) use ($map) {return $map[$element];}, $scenario_data->removeMembers->remove);

    // Add some group members
    $this->orcaProcessor->modifyGroupMembers($this->fund_id, $scenario_data->removeMembers);

    $contacts = $this->dm->db->fetchAllAssociative("select * from private.registeredgroupcontacts r where r.gconid=:id order by r.contid", ['id' => $this->trankeygen_map[4]]);
    $this->assertCount(2, $contacts);
  }

  /**
   * @param mixed $scenario
   * @covers \WAPDC\CampaignFinance\OrcaProcessor::mergeContact
   * @throws Exception
   */
  public function testNoVerify($scenario = 'merge_contacts')
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/contacts/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createExpenditureEvents($scenario_data->expenditureevents ?? []);

    $contact_id = $this->trankeygen_map[$scenario_data->contacts[0]->id];
    $duplicate_id = $this->trankeygen_map[$scenario_data->contacts[1]->id];


    // call the orca-processors merge function to do the merging
    $result = $this->orcaProcessor->mergeContacts($this->fund_id, $duplicate_id, $contact_id, true);

    $this->assertFalse($result->success);

    // Test merge results;
    $contacts = $this->dm->db->executeQuery('select count(1) from private.vcontacts where fund_id = :fund_id and trankeygen_id=:contact_id',
      ['fund_id' => $this->fund_id, 'contact_id' => $duplicate_id])->fetchOne();
    // One of the two existing contacts should match as a duplicate so we should have one less than is in the contacts.
    $this->assertEquals(1, $contacts);
  }
}