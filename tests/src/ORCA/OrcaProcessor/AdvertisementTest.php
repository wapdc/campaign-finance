<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Exception;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class AdvertisementTest extends ORCATestCase {
  public function makeValidCandidates($data)
  {
    foreach ($data as $candidate) {
      if (!empty($candidate->candidacy_id)) {
        $name = $candidate->name ?? ($candidate->ballot_name ?? 'John Doe');
        $ballot_name = $candidate->ballot_name ?? ($candidate->name ?? 'John Doe');
        $person_id = $this->dm->db->executeQuery('insert into person (name) values (:name) returning person_id', ['name' => $name])->fetchOne();
        $candidate_id = $this->dm->db->executeQuery('insert into candidacy (person_id, election_code, ballot_name) values (:person_id, 2020, :ballot_name) returning candidacy_id',
          ['person_id' => $person_id, 'ballot_name' => $ballot_name])->fetchOne();
        $candidate->candidacy_id = $candidate_id;
      }
    }
  }

  public function trueFalseProvider()
  {
    return [[true], [false]];
  }

  /**
   * Sets the property of an object or removes the property if the value is null.
   *
   * @param $data
   * @param $property
   * @param $value
   */
  protected function setProperty($data, $property, $value)
  {
    // Determine if the property has a '.' so we know if we need to drill further.
    if (strpos($property, '.')) {
      list($outer, $inner) = explode('.', $property, 2);
      // See if we have an array index.
      if ($outer === '0' || (int)$outer) {
        $this->setProperty($data[$outer], $inner, $value);
      } else {
        $this->setProperty($data->$outer, $inner, $value);
      }
    } else {
      if (isset($data->$property) && $value === NULL) {
        unset($data->$property);
      } else {
        $data->$property = $value;
      }
    }
  }

  public function testValidateAdvertisement()
  {
    $validations = Yaml::parseFile("{$this->data_dir}/advertisements/advertisement.validations.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $contents = file_get_contents("{$this->data_dir}/advertisements/advertisement.json");

    foreach ($validations->tests as $validation_test) {
      $expected_errors = [];
      // Start with the same initial data.
      $ad_data = json_decode($contents);
      $this->makeValidCandidates($ad_data->advertisement->targets);
      // Each condition either an assertion of an error or and assertion of a particular initial state modified from
      // the base $submission
      foreach ($validation_test as $condition => $value) {
        switch (true) {
          case (strpos($condition, '.') !== FALSE):
            $this->setProperty($ad_data, $condition, $value);
            break;
          case ($condition == 'error'):
            if (is_array($value)) {
              foreach ($value as $error) {
                $expected_errors[] = $error;
              }
            } else {
              $expected_errors[] = $value;
            }
            break;
        }
      }
      $savedAdvertisement = $this->orcaProcessor->saveAdvertisement(-17, json_encode($ad_data));
      if ($expected_errors && $expected_errors != ['']) {
        foreach ($expected_errors as $expected_error) {
          $this->assertEquals($expected_error, $savedAdvertisement->error, "Missing expected errors: $condition");
        }
      } else {
        $this->assertTrue($savedAdvertisement->success, "savedAdvertisement error: $savedAdvertisement->error");
        $this->assertEmpty($savedAdvertisement->error);
      }
    }
  }

  public function testSaveAdvertisement()
  {
    $fund_id = -17;
    $contribution_id = -54321;
    $sql = 'insert into private.trankeygen (trankeygen_id, fund_id) values (:contribution_id, :fund_id)';
    $this->dm->db->executeQuery($sql, ['contribution_id' => $contribution_id, 'fund_id' => $fund_id]);
    $sql = 'insert into private.receipts (trankeygen_id, type, amount) values (:contribution_id, 1, 50)';
    $this->dm->db->executeQuery($sql, ['contribution_id' => $contribution_id]);

    $contents = file_get_contents($this->data_dir . "/advertisements/advertisement.json");
    $data = json_decode($contents);
    $this->makeValidCandidates($data->advertisement->targets);
    $savedAdvertisement = $this->orcaProcessor->saveAdvertisement($fund_id, json_encode($data));
    $this->assertEquals($fund_id, $savedAdvertisement->advertisement->ad->fund_id);
    $this->assertEquals($data->advertisement->ad->description, $savedAdvertisement->advertisement->ad->description);
    $this->assertEquals($data->advertisement->ad->first_run_date, $savedAdvertisement->advertisement->ad->first_run_date);
    $this->assertEquals($data->advertisement->ad->last_run_date, $savedAdvertisement->advertisement->ad->last_run_date);
    $this->assertEquals($data->advertisement->ad->outlets, $savedAdvertisement->advertisement->ad->outlets);
    // contribution id
    $this->assertEquals($contribution_id, $savedAdvertisement->advertisement->contributions[0]->contribution_id);

    //make sure ad_target records are created correctly
    $targets = $this->dm->db->executeQuery('select * from private.ad_target where ad_id = :ad_id', ['ad_id' => $savedAdvertisement->advertisement->ad->ad_id])->fetchAllAssociative();
    $this->assertEquals(count($data->advertisement->targets), count($targets));
    foreach ($data->advertisement->targets as $selectedAdTarget) {
      $adIndex = array_search($selectedAdTarget->ballot_name, array_column($targets, 'ballot_name'));
      $savedAdTarget = (object)$targets[$adIndex];
      $this->assertEquals($selectedAdTarget->candidacy_id ?? null, $savedAdTarget->candidacy_id);
      $this->assertEquals($selectedAdTarget->ballot_name, $savedAdTarget->ballot_name);
      $this->assertEquals($selectedAdTarget->stance ?? null, $savedAdTarget->stance);
    }

    //make sure update works and doesn't create new record
    $newDescription = 'I need sleep.';
    $savedAdvertisement->advertisement->ad->description = $newDescription;
    $ad_target_id = $savedAdvertisement->advertisement->targets[0]->ad_target_id;
    $updated_ballot_name = "A new ballot name";
    $savedAdvertisement->advertisement->targets[0]->ballot_name = $updated_ballot_name;
    $updatedAdvertisement = $this->orcaProcessor->saveAdvertisement(-17, json_encode($savedAdvertisement));
    $this->assertEquals($newDescription, $updatedAdvertisement->advertisement->ad->description);
    $this->assertEquals($savedAdvertisement->advertisement->ad->ad_id, $updatedAdvertisement->advertisement->ad->ad_id);
    $this->assertEquals(count($savedAdvertisement->advertisement->targets), count($updatedAdvertisement->advertisement->targets));
    $candidate_index = array_search($ad_target_id, array_column($updatedAdvertisement->advertisement->targets, 'ad_target_id'));
    $this->assertEquals($updated_ballot_name, $updatedAdvertisement->advertisement->targets[$candidate_index]->ballot_name);
    //make sure we get error for invalid date
    $savedAdvertisement->advertisement->ad->first_run_date = 'ponies';
    try {
      $this->orcaProcessor->saveAdvertisement(-17, json_encode($savedAdvertisement));
    } catch (Exception $e) {
      $this->assertNotEmpty($e->getMessage(), 'DateTime::__construct(): Failed to parse time string (ponies) at position 0 (p): The timezone could not be found in the database');
    }
  }

  /**
   * @param $hasExpense
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   * @dataProvider trueFalseProvider
   */
  public function testDeleteAdvertisement($hasExpense)
  {
    $fund_id = -17;
    $contents = file_get_contents($this->data_dir . "/advertisements/advertisement.json");
    $data = json_decode($contents);
    $this->makeValidCandidates($data->advertisement->targets);

    $savedAdvertisement = $this->orcaProcessor->saveAdvertisement($fund_id, json_encode($data));
    $ad = $savedAdvertisement->advertisement->ad;
    $this->assertNotEmpty($ad->ad_id);

    if ($hasExpense) {
      $expenseContents = file_get_contents($this->data_dir . "/expenditures/expense.json");
      $expenseData = json_decode($expenseContents);
      $expenseData->expenditure->expenses[0]->ad_id = $ad->ad_id;
      $this->makeContactsForJSON($fund_id, $expenseData);
      $savedExpenditureJson = $this->orcaProcessor->saveExpenditure($fund_id, $expenseData);
      $adIndex = array_search($ad->ad_id, array_column($savedExpenditureJson->expenditure->expenses, 'ad_id'));
      $this->assertNotEmpty($savedExpenditureJson->expenditure->expenses[$adIndex]);
      $retrievedAdExpenditure = $this->dm->db->executeQuery('select * from private.ad_expenditure where ad_id = :ad_id', ['ad_id' => $ad->ad_id])->fetchAllAssociative();
      $this->assertNotEmpty($retrievedAdExpenditure);
    }
    $response = $this->orcaProcessor->deleteAdvertisement($fund_id, $ad->ad_id);
    $this->assertEquals($hasExpense, !($response->success));
    if (!$hasExpense) {
      //When run in test deleteAdvertisement causes an exception that ends the transaction
      $fetched = $this->dm->db->executeQuery('select * from private.ad where ad_id = :ad_id', ['ad_id' => $ad->ad_id]);
      $fetchedAd = $fetched->fetchAllAssociative();
      $this->assertEmpty($fetchedAd);
    }
  }
}