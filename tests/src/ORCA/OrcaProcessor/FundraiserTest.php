<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class FundraiserTest extends ORCATestCase {
  public function testSaveFundraiser()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/fundraisers.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // Low-cost fundraiser
    $this->setTrankeygenIds($scenario_data->fundraisers->low_cost, ['cid']);
    $data = $scenario_data->fundraisers->low_cost;
    $savedLowCostFundraiser = $this->orcaProcessor->saveLowCostFundraiser($this->fund_id, $scenario_data->fundraisers->low_cost);
    $this->assertNotEmpty($savedLowCostFundraiser);
    $this->assertEquals($this->trankeygen_map[1600], $savedLowCostFundraiser->{'low-cost-fundraiser'}->cid);
    $this->assertEquals($data->amount, $savedLowCostFundraiser->{'low-cost-fundraiser'}->amount);
    $this->assertEquals($data->date_, $savedLowCostFundraiser->{'low-cost-fundraiser'}->date_);
    $this->assertEquals($data->memo, $savedLowCostFundraiser->{'low-cost-fundraiser'}->memo);
    $this->assertEquals($data->name, $savedLowCostFundraiser->{'low-cost-fundraiser'}->name);
    $this->assertEquals($data->street, $savedLowCostFundraiser->{'low-cost-fundraiser'}->street);
    $this->assertEquals($data->city, $savedLowCostFundraiser->{'low-cost-fundraiser'}->city);
    $this->assertEquals($data->state, $savedLowCostFundraiser->{'low-cost-fundraiser'}->state);
    $this->assertEquals($data->zip, $savedLowCostFundraiser->{'low-cost-fundraiser'}->zip);
    $this->assertEquals(1, $savedLowCostFundraiser->{'low-cost-fundraiser'}->nettype);
    $this->assertAccountBalance($savedLowCostFundraiser->{'low-cost-fundraiser'}->cid, $data->amount);
    $createdAccount = $this->dm->db->executeQuery('select * from private.accounts where trankeygen_id = :trankeygen_id', ["trankeygen_id" => $savedLowCostFundraiser->{'low-cost-fundraiser'}->contid])->fetchAssociative();
    $this->assertNotEmpty($createdAccount);
    $createdAccount = (object)$createdAccount;
    $this->assertEquals(4097, $createdAccount->acctnum);
    $this->assertEquals($data->memo, $createdAccount->memo);
    $this->assertEquals($data->date_, $createdAccount->date_);

    // cash fundraiser
    $data = $scenario_data->fundraisers->cash;
    $savedCashFundraiser = $this->orcaProcessor->saveCashFundraiser($this->fund_id, $scenario_data->fundraisers->cash);
    $this->assertNotEmpty($savedCashFundraiser);
    $this->assertEquals($data->date_, $savedCashFundraiser->{'cash-fundraiser'}->date_);
    $this->assertEquals($data->memo, $savedCashFundraiser->{'cash-fundraiser'}->memo);
    $this->assertEquals($data->name, $savedCashFundraiser->{'cash-fundraiser'}->name);
    $this->assertEquals($data->street, $savedCashFundraiser->{'cash-fundraiser'}->street);
    $this->assertEquals($data->city, $savedCashFundraiser->{'cash-fundraiser'}->city);
    $this->assertEquals($data->state, $savedCashFundraiser->{'cash-fundraiser'}->state);
    $this->assertEquals($data->zip, $savedCashFundraiser->{'cash-fundraiser'}->zip);
    $createdAccount = $this->dm->db->executeQuery('select * from private.accounts where trankeygen_id = :trankeygen_id', ["trankeygen_id" => $savedCashFundraiser->{'cash-fundraiser'}->contid])->fetchAssociative();
    $this->assertNotEmpty($createdAccount);
    $createdAccount = (object)$createdAccount;
    $this->assertEquals(4096, $createdAccount->acctnum);
    $this->assertEquals($data->memo, $createdAccount->memo);
    $this->assertEquals($data->date_, $createdAccount->date_);
  }

  public function testSaveFundraiserContribution() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/fundraiser-contribution.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    $expectedContribution = $scenario_data->contribution;
    $this->setTrankeygenIds($expectedContribution, ['pid', 'contid']);

    $savedContribution = $this->orcaProcessor->saveFundraiserContribution($this->fund_id, $scenario_data->contribution)->{'fundraiser-contribution'};

    foreach ($expectedContribution as $property => $value) {
      switch ($property) {
        default:
          $this->assertSame($value, $savedContribution->$property ?? NULL, $property);
      }
    }

    $this->assertAccountBalance($this->trankeygen_map[101], -1 * $expectedContribution->amount);
    $this->assertAccountBalance($this->trankeygen_map[1600], $expectedContribution->amount);
  }

  public function testDeleteLowCostFundraiser() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/low-cost-fundraiser-delete.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = Yaml::parseFile($this->data_dir . '/committees/continuing-pac.yml', Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createReceipts($scenario_data->receipts ??  []);
    $this->setTrankeygenIds($scenario_data->delete, ['receipt_id']);

    $this->orcaProcessor->deleteLowCostFundraiser($this->fund_id, $scenario_data->delete->receipt_id);

    $receipt = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :id', ['id' =>$this->trankeygen_map[101]])->fetchOne();
    $this->assertFalse($receipt);

    $accounts = $this->dm->db->executeQuery('select * from private.accounts where trankeygen_id = :id', ['id' =>$this->trankeygen_map[4097]])->fetchOne();
    $this->assertFalse($accounts);

    $this->assertAccountBalance($this->trankeygen_map[1600], 0);
  }

  /**
   * @throws OptimisticLockException
   * @throws ORMException
   */
  public function testDeleteCashFundraiser() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/fundraisers.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);

    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $data = $scenario_data->fundraisers->cash;
    $fundraiser_id = $this->trankeygen_map[4100];
    $this->orcaProcessor->deleteCashFundraiser($this->fund_id, $this->trankeygen_map[4100]);
    $cash_fundraiser = $this->dm->db->fetchOne("SELECT trankeygen_id from private.accounts where trankeygen_id=:id", ['id' => $fundraiser_id]);
    $this->assertEmpty($cash_fundraiser);
  }
}