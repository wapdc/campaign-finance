<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Exception;
use stdClass;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;
use WAPDC\CampaignFinance\Model\Fund;

class OrcaTest extends ORCATestCase {
  /**
   * Scenarios for generating funds from ORCA
   */
  public function generateFundProvider()
  {
    return [
      ['generate-fund-surplus'],
      ['generate-fund-continuing'],
      ['generate-fund-continuing-new'],
      ['generate-fund-candidate']
    ];
  }

  /**
   * Scenarios for generating funds from oRCA
   */
  public function generateExistingFundProvider()
  {
    return [
      ['generate-existing-fund-surplus'],
      ['generate-existing-fund-continuing'],
      ['generate-existing-fund-candidate']
    ];
  }

  /**
   * @param $scenario
   * @dataProvider generateFundProvider
   *
   * @throws Exception
   */
  public function testGenerateOrcaFund($scenario)
  {
    $testDataDir = $this->data_dir;
    $scenarioData = Yaml::parseFile("$testDataDir/funds/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $this->createCommittee($scenarioData);
    $response_data = $this->orcaProcessor->generateOrcaFund($this->committee->committee_id, $scenarioData->userData, 'TEST');

    // Check to make sure a fund was created
    $fund = $response_data->fund;
    $this->assertNotEmpty($fund->fund_id);
    $saved_fund = $this->dm->em->find(Fund::class, $fund->fund_id);
    $this->assertNotNull($saved_fund);

    // Check to make sure fund contains the right fund properties
    foreach ($scenarioData->fund as $key => $value) {
      switch ($key) {
        // Ignore fields that are nested objects or generated values.
        case "fund_id":
        case "committee":
          break;
        case "election_codes":
          $election_codes = $this->dm->db->executeQuery('select json_agg(election_code) as "election_codes" from private.election_participation where fund_id = :fund_id', ['fund_id' => $fund->fund_id])->fetchOne();
          $this->assertEquals($value, json_decode($election_codes));
          break;
        default:
          $this->assertSame($value, $fund->$key);
      }
    }

    // Check to make sure committee record contains the right properties
    foreach ($scenarioData->fund->committee as $key => $value) {
      switch ($key) {
        case "committee_id":
          break;
        default:
          $this->assertSame($value, $response_data->fund->committee->$key);
      }
    }

    // Check to make sure all fo the properties have been created.
    $this->assertIsArray($response_data->orcaProperties);
    $propertiesMap = [];
    foreach ($response_data->orcaProperties as $property) {
      $propertiesMap[$property->name] = $property->value;
    }
    foreach ($scenarioData->orcaProperties as $property) {
      $this->assertArrayHasKey($property->name, $propertiesMap);
      if ($property->name == 'TOKEN') {
        $this->assertGreaterThanOrEqual(8, strlen($property->value));
      } elseif ($property->name == 'FUND_ID') {
        $this->assertEquals($response_data->fund->fund_id, $propertiesMap[$property->name]);
      } else {
        $this->assertSame($property->value ?? NULL, $propertiesMap[$property->name], "$scenario property: $property->name");
      }
    }
  }

  /**
   * @param $scenario string
   *   Scenario file to use for testing.
   * @dataProvider generateExistingFundProvider
   */
  public function testGenerateOrcaFundExisting($scenario)
  {
    $testDataDir = $this->data_dir;
    $scenarioData = Yaml::parseFile("$testDataDir/funds/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $this->createCommittee($scenarioData);
    $scenarioData->userData->token = $scenarioData->api_token[0]->token;
    $response_data = $this->orcaProcessor->generateOrcaFundExisting($scenarioData->userData, 'TEST');

    // Make sure we can successfully get a fund
    $this->assertInstanceOf(stdClass::class, $response_data, $scenario);
    $this->assertNotEmpty($response_data->fund_id, $scenario);
    $this->assertEquals($scenarioData->committee->filer_id, $response_data->filer_id);
    $fund_id = $response_data->fund_id;

    // Make sure that if we use an invalid token with the same data that we get a new token returned.
    // And that the same fund id is used.
    $scenarioData->userData->token = 'zzz testing invalid token';
    $response_data = $this->orcaProcessor->generateOrcaFundExisting($scenarioData->userData, 'TEST');
    $this->assertInstanceOf(stdClass::class, $response_data, $scenario);
    $this->assertEquals($fund_id, $response_data->fund_id, $scenario);
    $this->assertEquals($scenarioData->committee->filer_id, $response_data->filer_id);
    $this->assertNotEquals($scenarioData->userData->token, $response_data->token ?? NULL);
    $this->assertNotEmpty($response_data->token ?? NULL);
  }
}
