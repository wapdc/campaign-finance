<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class ElectionTest extends ORCATestCase {
  public function testGetBallotTypeFromElectionCodes()
  {
    $result = $this->orcaProcessor->getBallotTypeFromElectionCodes('2021');
    $this->assertEquals(4, $result->value);
    $this->assertEquals('CAMPAIGNINFO:BALLOTTYPE', $result->name);

    $result = $this->orcaProcessor->getBallotTypeFromElectionCodes('2021P');
    $this->assertEquals(1, $result->value);
    $this->assertEquals('CAMPAIGNINFO:BALLOTTYPE', $result->name);

    $result = $this->orcaProcessor->getBallotTypeFromElectionCodes('2021S2');
    $this->assertEquals(1, $result->value);
    $this->assertEquals('CAMPAIGNINFO:MONTHOFSPECIAL', $result->name);

    $result = $this->orcaProcessor->getBallotTypeFromElectionCodes('2021S12');
    $this->assertEquals(11, $result->value);
    $this->assertEquals('CAMPAIGNINFO:MONTHOFSPECIAL', $result->name);

    $result = $this->orcaProcessor->getBallotTypeFromElectionCodes('2021,2021P');
    $this->assertEquals(3, $result->value);
    $this->assertEquals('CAMPAIGNINFO:BALLOTTYPE', $result->name);

    $result = $this->orcaProcessor->getBallotTypeFromElectionCodes('2022S4,2022S2');
    $this->assertEquals(0, $result->value);
    $this->assertEquals('CAMPAIGNINFO:BALLOTTYPE', $result->name);

    $result = $this->orcaProcessor->getBallotTypeFromElectionCodes('2021P,2021S12');
    $this->assertEquals(0, $result->value);
    $this->assertEquals('CAMPAIGNINFO:BALLOTTYPE', $result->name);
  }
}