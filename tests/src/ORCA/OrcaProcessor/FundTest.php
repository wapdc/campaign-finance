<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use stdClass;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;
use WAPDC\Core\Model\User;

class FundTest extends ORCATestCase {
  public function testRequestFundAccess($scenario = 'generate-existing-fund-continuing')
  {
    $testDataDir = $this->data_dir;
    $scenarioData = Yaml::parseFile("$testDataDir/funds/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $this->createCommittee($scenarioData);

    // Start with a user for the tests
    $pdc_user = new User();
    $pdc_user->realm = 'test';
    $pdc_user->uid = '12345';
    $pdc_user->user_name = 'test';

    // Assume we start out with no access
    $this->assertFalse($this->committeeManager->userHasRole($this->committee, $pdc_user, 'owner'));

    // Generate a token for use with tests
    $token = $this->committeeManager->generateAPIToken($this->committee->committee_id, 'testing token', $pdc_user->user_name);

    $request_data = new stdClass();

    // Verify that a bad token returns false
    $request_data->token = 'bad token';
    $request_data->filer_id = $this->committee->filer_id;
    $data = $this->orcaProcessor->requestFundAccess($request_data, $pdc_user);
    $this->assertFalse($data->success, 'Returns failure');
    $this->assertStringContainsString('invalid', strtolower($data->error), 'Correct error message');
    $this->assertFalse($this->committeeManager->userHasRole($this->committee, $pdc_user, 'owner'));

    // Verify that an incorrect filer_id passed returns a wrong committee error.
    $request_data->filer_id = 'bad filer id';
    $request_data->token = $token;
    $data = $this->orcaProcessor->requestFundAccess($request_data, $pdc_user);
    $this->assertFalse($data->success);
    $this->assertStringContainsString('invalid', strtolower($data->error));
    $this->assertFalse($this->committeeManager->userHasRole($this->committee, $pdc_user, 'owner'));

    // Verify that a good token returns true and that user has been granted access
    $request_data->filer_id = $this->committee->filer_id;
    $request_data->token = $token;
    $data = $this->orcaProcessor->requestFundAccess($request_data, $pdc_user);
    $this->assertTrue($data->success, 'Check success');
    $this->assertTrue($this->committeeManager->userHasRole($this->committee, $pdc_user, 'owner'), 'user has role');
  }
}