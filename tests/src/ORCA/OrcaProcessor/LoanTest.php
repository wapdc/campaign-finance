<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class LoanTest extends ORCATestCase {
  /**
   * @throws OptimisticLockException
   * @throws ORMException
   * @throws Exception
   * @throws \Exception
   */
  public function testSaveMonetaryLoan() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/loan.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // misc other
    $this->setTrankeygenIds($scenario_data->loan, ['contid']);

    $saved_loan = $this->orcaProcessor->saveMonetaryLoan($this->fund_id, $scenario_data->loan);
    $saved_did = $saved_loan->loan->did;
    $saved_receipt = $this->dm->db->fetchAllAssociative("select * from private.receipts where did=:did ", ['did' => $saved_did])[0];

    $this->assertNotEmpty($saved_loan);
    foreach($scenario_data->loan as $property => $value) {
      switch ($property) {
        case 'id':
          $loan_id = $value;
          $this->assertNotEmpty($loan_id, $property);
          break;
        case 'date_':
          $this->assertSame($scenario_data->loan->date_, $saved_receipt['date_']);
          break;
        case 'original_loan_date':
          $this->assertSame($scenario_data->loan->original_loan_date, $saved_loan->loan->original_loan_date);
          break;
        case 'data':
          $saved_receipt_data = json_decode($saved_receipt['data']);
          $this->assertSame($value->committee_name, $saved_receipt_data->committee_name);
          $this->assertSame($value->election_year, $saved_receipt_data->election_year);
          break;
        default:
          $this->assertSame($value, $saved_loan->loan->$property ?? NULL, $property);
      }
    }

    // Check loan balance.
    $this->assertAccountBalance($saved_loan->loan->loan_account_id, -1 * $scenario_data->loan->amount);
    $this->assertAccountBalance($this->trankeygen_map[1000], $scenario_data->loan->amount);

    $scenario_data->loan->carryforward = 0;
    $scenario_data->loan->data = null;
    $saved_loan = $this->orcaProcessor->saveMonetaryLoan($this->fund_id, $scenario_data->loan);

    // Check loan balance.
    $this->assertAccountBalance($saved_loan->loan->loan_account_id, -1 * $scenario_data->loan->amount);
    $this->assertAccountBalance($this->trankeygen_map[1600], $scenario_data->loan->amount);
  }

  public function testSaveInKindLoan() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/in-kind-loan.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // misc other
    $this->setTrankeygenIds($scenario_data->loan, ['contid', 'cid']);


    $saved_loan = $this->orcaProcessor->saveInKindLoan($this->fund_id, $scenario_data->loan);
    $this->assertNotEmpty($saved_loan);

    $saved_did = $saved_loan->loan->did;
    $saved_receipt = $this->dm->db->fetchAllAssociative("select * from private.receipts where did=:did ", ['did' => $saved_did])[0];

    foreach($scenario_data->loan as $property => $value) {
      switch ($property) {

        case 'id':
          $loan_id = $value;
          $this->assertNotEmpty($loan_id, $property);
          break;
        case 'date_':
          $this->assertSame($scenario_data->loan->date_, $saved_receipt['date_']);
          break;
        case 'original_loan_date':
          $this->assertSame($scenario_data->loan->original_loan_date, $saved_loan->loan->original_loan_date);
          break;
        case 'data':
          $saved_receipt_data = json_decode($saved_receipt['data']);
          $this->assertSame($value->committee_name, $saved_receipt_data->committee_name);
          $this->assertSame($value->election_year, $saved_receipt_data->election_year);
          break;

        default:
          $this->assertSame($value, $saved_loan->loan->$property ?? NULL, $property);
      }
    }
    $this->assertNotEmpty($saved_loan->loan->deptkey);

    // Check loan balance.
    $this->assertAccountBalance($saved_loan->loan->loan_account_id, -1 * $scenario_data->loan->amount);
    $this->assertAccountBalance($this->trankeygen_map[5000], $scenario_data->loan->amount);
  }

  public function testSaveLoanPayment() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/loan-payment.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createLoans($scenario_data->loans);

    $expectedPayment = $scenario_data->loanPayment;
    $this->setTrankeygenIds($expectedPayment, ['pid', 'did']);

    $savedPayment = $this->orcaProcessor->saveLoanPayment($this->fund_id, $scenario_data->loanPayment)->{'loan-payment'};

    foreach ($expectedPayment as $property => $value) {
      $this->assertSame($value, $savedPayment->$property ?? null, $property);
    }
    $this->assertNotEmpty($savedPayment->deptkey);

    $this->assertAccountBalance($this->trankeygen_map[201], -1000.0 + $expectedPayment->amount);
    $this->assertAccountBalance($this->trankeygen_map[202], $expectedPayment->interest);
    $this->assertAccountBalance($this->trankeygen_map[1000], 1000 - ($expectedPayment->amount + $expectedPayment->interest));
  }

  public function testSaveLoanForgiveness(){
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/loan-forgiveness.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createLoans($scenario_data->loans);

    $expectedForgiveness = $scenario_data->loanForgiveness;
    $this->setTrankeygenIds($expectedForgiveness, ['pid', 'did']);

    $savedForgiveness = $this->orcaProcessor->saveLoanForgiveness($this->fund_id, $scenario_data->loanForgiveness)->{'loan-forgiveness'};

    foreach ($expectedForgiveness as $property => $value) {
      switch ($property) {
        default:
          $this->assertSame($value, $savedForgiveness->$property ?? NULL, $property);
      }
    }

    $this->assertAccountBalance($this->trankeygen_map[201], -1000.0 + $expectedForgiveness->amount);
    $this->assertAccountBalance($this->trankeygen_map[4000], -1000 + (1000 - $expectedForgiveness->amount));
  }
  public function testSaveLoanEndorser(){
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/loan-endorser.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createLoans($scenario_data->loans);

    $expectedEndorser = $scenario_data->loanEndorser;
    $this->setTrankeygenIds($expectedEndorser, ['pid', 'contid']);

    $savedEndorser = $this->orcaProcessor->saveLoanEndorser($this->fund_id, $scenario_data->loanEndorser)->{'loan-endorser'};

    foreach ($expectedEndorser as $property => $value) {
      switch ($property) {
        default:
          $this->assertSame($value, $savedEndorser->$property ?? NULL, $property);
      }
    }
  }

  /**
   * @throws OptimisticLockException
   * @throws ORMException
   * @throws \Doctrine\DBAL\Exception
   */
  public function testDeleteLoan() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/debts/loan-delete.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createLoans($scenario_data->loans);

    $loan_id = $this->trankeygen_map[101];

    $this->orcaProcessor->deleteLoan($this->fund_id, $this->trankeygen_map[101]);

    $loan = $this->dm->db->fetchOne("SELECT trankeygen_id from private.loans where trankeygen_id=:id", ['id' => $loan_id]);
    $this->assertEmpty($loan);

    $accounts = $this->dm->db->fetchAllAssociative("SELECT * from private.vaccounts where pid = :pid" , ['pid' => $loan_id]);
    $this->assertEmpty($accounts);

    $this->assertAccountBalance($this->trankeygen_map[1600], 0);
    $this->assertAccountBalance($this->trankeygen_map[1000], 0);
  }
}
