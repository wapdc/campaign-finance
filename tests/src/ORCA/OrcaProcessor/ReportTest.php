<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Exception;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class ReportTest extends ORCATestCase {
  public function potentialPROProvider() {
    return [
      ['simple'],
      ['all-filed'],
      ['monthly-combined'],
      ['monthly-june'],
      ['monthly-july'],
      ['combined-monthly-june'],
      ['no-elections-after-reporting'],
      ['continuing-february-special'],
    ];
  }

  public function attachmentProvider()
  {
    return [
      ['attached-page-deposits'],
      ['attached-page-obligations']
    ];
  }

  public function testSaveReportingPeriods()
  {
    $election_code = '2021';
    $committee_id = $this->dm->db->executeQuery("insert into committee(name, election_code, committee_type, continuing) values ('test committee', :election_code, 'CA', false) returning committee_id", ['name' => 'test committee', 'election_code' => $election_code])->fetchOne();
    $fund_id = $this->createFund($election_code, $committee_id);
    $this->dm->db->executeQuery('update fund set committee_id = :committee_id where fund_id = :fund_id', ['committee_id' => $committee_id, 'fund_id' => $fund_id]);
    $this->dm->db->executeQuery("insert into private.properties (fund_id, name, value) values (:fund_id, 'CAMPAIGNINFO:BALLOTTYPE', '3')", ['fund_id' => $fund_id]);
    $this->dm->db->executeQuery("insert into private.properties (fund_id, name, value) values (:fund_id, 'CAMPAIGNINFO:STARTDATE', '2021-01-01')", ['fund_id' => $fund_id]);

    $contents = file_get_contents($this->data_dir . "/reports/sync-reporting-periods.json");
    $this->orcaProcessor->saveValidations($fund_id, $contents);

    $data = json_decode($contents);
    $map = [];
    foreach ($data->reportingPeriods as $period) {
      $map[$fund_id] = $period;
    }
    $results = $this->dm->db->executeQuery("select * from private.c4reportingperiods where fund_id=:fund_id", ['fund_id' => $fund_id]);
    $this->assertEquals(count($data->reportingPeriods), $results->rowCount());
    foreach ($results as $saved_period) {
      $saved_period = (object)$saved_period;
      $this->assertArrayHasKey($fund_id, $map);
      $period = $map[$fund_id];
      $this->assertSame($period->reported, $saved_period->reported);
      $this->assertSame($period->startdate, $saved_period->startdate);
      $this->assertSame($period->enddate, $saved_period->enddate);
      $this->assertSame($period->duedate, $saved_period->duedate);
    }
    $contents = file_get_contents($this->data_dir . '/reports/sync-reporting-periods-altered.json');
    $this->orcaProcessor->saveValidations($fund_id, $contents);
    $data = json_decode($contents);
    $map = [];

    foreach ($data->reportingPeriods as $period) {
      $map[$period->orca_id] = $period;
    }
    $data = json_decode($contents);
    $results = $this->dm->db->executeQuery("select * from private.c4reportingperiods where fund_id=:fund_id", ['fund_id' => $fund_id]);
    $this->assertEquals(count($data->reportingPeriods), $results->rowCount());
    foreach ($results as $saved_period) {
      $saved_period = (object)$saved_period;
      $this->assertArrayHasKey($period->orca_id, $map);
      $reportingPeriod = $map[$period->orca_id];
      $this->assertSame($reportingPeriod->reported, $saved_period->reported);
    }
  }

  function testAddReportingPeriod()
  {
    $election_code = '2021';
    $committee_id = $this->dm->db->executeQuery("insert into committee(name, election_code, committee_type, continuing) values ('test committee', :election_code, 'CA', false) returning committee_id", ['name' => 'test committee', 'election_code' => $election_code])->fetchOne();
    $fund_id = $this->createFund($election_code, $committee_id);
    $this->dm->db->executeQuery('update fund set committee_id = :committee_id where fund_id = :fund_id', ['committee_id' => $committee_id, 'fund_id' => $fund_id]);
    $this->dm->db->executeQuery("insert into private.properties (fund_id, name, value) values (:fund_id, 'CAMPAIGNINFO:BALLOTTYPE', '3')", ['fund_id' => $fund_id]);
    $this->dm->db->executeQuery("insert into private.properties (fund_id, name, value) values (:fund_id, 'CAMPAIGNINFO:STARTDATE', '2021-01-01')", ['fund_id' => $fund_id]);

    $contents = file_get_contents($this->data_dir . "/reports/sync-reporting-periods.json");
    $this->orcaProcessor->saveValidations($fund_id, $contents);

    $obligations = $this->dm->db->executeQuery("select * from private.reporting_obligation where fund_id=:fund_id", ['fund_id' => $fund_id]);
    $this->orcaProcessor->addOneReportingObligation($fund_id);

    $results = $this->dm->db->executeQuery("select * from private.reporting_obligation where fund_id=:fund_id", ['fund_id' => $fund_id]);
    $this->assertEquals($obligations->rowCount() + 1, $results->rowCount());
  }

  public function testCombineReportingPeriod($scenario = 'combine_reporting_periods')
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/reports/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    $this->createFund($election_code, $committee->committee_id);
    // Saving the campaign start date should generate reporting periods for the campaign
    $this->orcaProcessor->saveCampaignSettings($this->fund_id, json_encode($scenario_data->settings));
    $get_obligations_sql = "select * from private.reporting_obligation where fund_id=:fund_id order by startdate";
    $reporting_obligations = $this->dm->db->executeQuery($get_obligations_sql, ['fund_id' => $this->fund_id])->fetchAllAssociative();

    $jan = $reporting_obligations[0];
    $may = $reporting_obligations[4];

    $obligation_id = $may['obligation_id'];
    $start_date = $jan['startdate'];

    // Stretch the final reporting obligation to cover jan through may
    $this->orcaProcessor->combineReportingPeriods($this->fund_id, $obligation_id, $start_date);

    $modified_obligations = $this->dm->db->executeQuery($get_obligations_sql, ['fund_id' => $this->fund_id])->fetchAllAssociative();
    $stretched = $modified_obligations[0];
    $this->assertEquals($obligation_id, $stretched['obligation_id']);
    $this->assertEquals($jan['startdate'], $stretched['startdate']);
    $this->assertEquals($may['enddate'], $stretched['enddate']);
    $this->assertCount(count($reporting_obligations) - 4, $modified_obligations);
  }

  /**
   * @dataProvider potentialPROProvider
   * @param $scenario
   * @return void
   */
  public function testPotentialReportingObligations($scenario)  {
    $data = Yaml::parseFile($this->data_dir . "/reports/reporting_obligations/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = Yaml::parseFile($this->data_dir . '/committees/continuing-pac.yml', Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee);
    $election_code = '2022';
    $this->createFund($election_code, $committee->committee_id);
    $this->orcaProcessor->updateProperties($this->fund_id, $data->properties);
    foreach($data->reports as $report) {
      $this->dm->db->executeStatement(
        "INSERT INTO report(fund_id, report_type, period_start, period_end) values (:fund_id, 'C4', :period_start, :period_end)",
        [
          'fund_id' => $this->fund_id,
          'period_start' => $report->period_start,
          'period_end' => $report->period_end,
        ]
      );
    }
    foreach ($data->election_participation as $election_code) {
      $this->dm->db->executeStatement(
        "INSERT INTO private.election_participation(fund_id, election_code) values (:fund_id, :election_code)",
        ['fund_id' => $this->fund_id, 'election_code' => $election_code]
      );
    }

    $rows = $this->dm->db->fetchAllAssociative(
      "SELECT * FROM private.cf_potential_reporting_obligations_by_fund(:fund_id)",
      ['fund_id' => $this->fund_id]
    );
    foreach ($data->reporting_obligations as $idx=>$ro) {
      $this->assertArrayHasKey($idx, $rows, "RO Present");
      $actual_ro = $rows[$idx];
      foreach ($ro as $property => $value) {
        $this->assertEquals($ro->$property, $actual_ro[$property], "$scenario:$property");
      }
    }

    // Make sure all rows have a reporting period id and that it doesn't duplicate.
    $used_ids = [];
    foreach($rows as $row) {
      $this->assertNotEmpty($row['reporting_period_id']);
      $this->assertTrue(empty($used_ids[$row['reporting_period_id']]), $scenario . ":" .$row['reporting_period_id']);
      $used_ids[$row['reporting_period_id']] = 1;
    }
  }

  /**
   * @param $scenario
   * @dataProvider attachmentProvider
   *
   * @throws Exception
   */
  public function testSaveAttachedPage($scenario)
  {
    // Load test scenario data
    $contents = file_get_contents($this->data_dir . "/reports/$scenario.json");
    $data = json_decode($contents);

    // Save Case
    $savedAttachedPage = $this->orcaProcessor->saveAttachedPage($data->attachedPageData->attachedPage->fund_id, $contents);

    $this->assertNotEmpty($savedAttachedPage->attachedPageData->attachedPage->target_id);
    // Checking if values are correct
    $this->assertEquals($data->attachedPageData->attachedPage->target_id, $savedAttachedPage->attachedPageData->attachedPage->target_id);
    $this->assertEquals($data->attachedPageData->attachedPage->target_type, $savedAttachedPage->attachedPageData->attachedPage->target_type);
    $this->assertEquals($data->attachedPageData->attachedPage->text_data, $savedAttachedPage->attachedPageData->attachedPage->text_data);

    // Update case
    $updateData = json_decode($contents);
    $updateData->attachedPageData->attachedPage->text_data = 'I hope this unit test doesnt fail...please';

    $updateAttachedPage = $this->orcaProcessor->saveAttachedPage($updateData->attachedPageData->attachedPage->fund_id, json_encode($updateData));

    // Check if record updated instead of created
    $query_result = $this->dm->db->executeQuery('select * from private.attachedtextpages where fund_id = :fund_id and target_id = :id and target_type = :target_type',
      ['fund_id' => $data->attachedPageData->attachedPage->fund_id, 'id' => $data->attachedPageData->attachedPage->target_id, 'target_type' => $data->attachedPageData->attachedPage->target_type])->fetchAllAssociative();

    $this->assertCount(1, $query_result);

    // Checking if values are correct
    $this->assertEquals($data->attachedPageData->attachedPage->target_id, $updateAttachedPage->attachedPageData->attachedPage->target_id);
    $this->assertEquals($data->attachedPageData->attachedPage->target_type, $updateAttachedPage->attachedPageData->attachedPage->target_type);
    $this->assertNotEquals($data->attachedPageData->attachedPage->text_data, $updateAttachedPage->attachedPageData->attachedPage->text_data);

    // Delete case
    $deleteData = json_decode($contents);
    $deleteData->attachedPageData->attachedPage->text_data = ' ';
    $this->orcaProcessor->saveAttachedPage($deleteData->attachedPageData->attachedPage->fund_id, json_encode($deleteData));

    $query_result = $this->dm->db->executeQuery('select * from private.attachedtextpages where fund_id = :fund_id and target_id = :id and target_type = :target_type',
      ['fund_id' => $deleteData->attachedPageData->attachedPage->fund_id, 'id' => $deleteData->attachedPageData->attachedPage->target_id, 'target_type' => $deleteData->attachedPageData->attachedPage->target_type])->fetchAllAssociative();

    $this->assertEmpty($query_result);
  }
}
