<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class CorrectionTest extends ORCATestCase {
  public function correctionAdjustmentProvider()
  {
    return [
      ['math-adjustment'],
      ['transaction-adjustment']
    ];
  }

  /**
   * @param string $scenario
   * @dataProvider correctionAdjustmentProvider
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testCorrectionAdjustment($scenario)
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/corrections/{$scenario}.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // Save math error adjustment
    $expectedCorrectionAdjustment = $scenario_data->correction;

    $this->setTrankeygenIds($expectedCorrectionAdjustment, ['reported_id']);

    //Debug purposes
    //$rows = $this->dm->db->fetchAllAssociative('Select * from private.vaccounts where fund_id = :fund_id', ['fund_id' => $this->fund_id]);

    $savedCorrectionAdjustment = $this->orcaProcessor->saveCorrection($this->fund_id, $scenario_data->correction);

    $this->assertNotEmpty($savedCorrectionAdjustment);
    $this->assertTrue($savedCorrectionAdjustment->success);
    $this->assertNotEmpty($savedCorrectionAdjustment->{'correction'});

    // evaluating correction record
    $savedCorrectionError = $savedCorrectionAdjustment->{'correction'};
    if (empty($expectedCorrectionAdjustment->reported_id)) {
      $this->assertEquals($expectedCorrectionAdjustment->type, $savedCorrectionError->type);
      $this->assertEquals($expectedCorrectionAdjustment->reported_date, $savedCorrectionError->reported_date);
      $this->assertEquals($expectedCorrectionAdjustment->reported_amount, $savedCorrectionError->reported_amount);
    }
    if (!empty($expectedCorrectionAdjustment->reported_id)) {
      $this->assertEquals($this->trankeygen_map[1600], $savedCorrectionError->cid);
      $this->assertEquals($this->trankeygen_map[4000], $savedCorrectionError->did);

      $this->assertAccountBalance($this->trankeygen_map[1600], 40.0);
      $this->assertAccountBalance($this->trankeygen_map[4000], -40);
    }
    $this->assertEquals($expectedCorrectionAdjustment->discovered_date, $savedCorrectionError->discovered_date);
    $this->assertEquals($expectedCorrectionAdjustment->corrected_amount, $savedCorrectionError->corrected_amount);
    $this->assertEquals($expectedCorrectionAdjustment->description, $savedCorrectionError->description);
    $this->assertEquals($expectedCorrectionAdjustment->memo, $savedCorrectionError->memo);
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   */
  public function testDeleteCorrection()
  {
    $scenario_data = Yaml::parseFile($this->data_dir . "/corrections/transaction-adjustment.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    // Save math error adjustment
    $expectedCorrectionAdjustment = $scenario_data->correction;

    $this->setTrankeygenIds($expectedCorrectionAdjustment, ['reported_id']);

    //Debug purposes
    //$rows = $this->dm->db->fetchAllAssociative('Select * from private.vreceipts where fund_id = :fund_id', ['fund_id' => $this->fund_id]);

    $savedCorrectionAdjustment = $this->orcaProcessor->saveCorrection($this->fund_id, $scenario_data->correction);

    $this->assertNotEmpty($savedCorrectionAdjustment);
    $this->assertTrue($savedCorrectionAdjustment->success);

    // evaluating correction record
    $savedCorrectionErrorId = $savedCorrectionAdjustment->{'correction'}->trankeygen_id;

    $this->orcaProcessor->deleteCorrection($this->fund_id, $savedCorrectionErrorId);
    $correction = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :id', ['id' => $savedCorrectionErrorId])->fetchAllAssociative();

    $this->assertEmpty($correction);
    $this->assertAccountBalance($this->trankeygen_map[1600], 50);
  }
}