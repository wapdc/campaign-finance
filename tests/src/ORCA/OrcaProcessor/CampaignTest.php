<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use stdClass;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;
use DateTime;

class CampaignTest extends ORCATestCase
{
    protected function createTestPac()
    {
        $data = Yaml::parseFile($this->data_dir . '/funds/generate-existing-fund-continuing.yml', Yaml::PARSE_OBJECT_FOR_MAP);
        return $this->createCommittee($data);
    }

    public function testUpgradeCampaign()
    {
        // Initial conditions
        $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        // Create a committee to attach the fund to
        $committee = $this->createCommittee($committee_data);
        $election_code = $committee->start_year;
        // Make fund for saving the data.
        $this->fund_id = $this->createFund($election_code, $committee->committee_id);

        $scenario_data = Yaml::parseFile($this->data_dir . "/campaigns/upgrade.yml", Yaml::PARSE_OBJECT_FOR_MAP);

        $this->dm->db->executeStatement("insert into private.campaign_fund(fund_id, version) VALUES (:fund_id, :version)", ['fund_id' => $this->fund_id, 'version' => $scenario_data->upgrade->version]);
        $this->dm->db->executeStatement("insert into private.properties(fund_id, name, value) VALUES (:fund_id, 'VNUM', :version)", ['fund_id' => $this->fund_id, 'version' => (string)$scenario_data->upgrade->version]);

        $this->orcaProcessor->upgradeCampaign($this->fund_id);

        // Make sure the version is updated.
        $version = $this->dm->db->fetchOne("SELECT version from private.campaign_fund where fund_id = :fund_id", ['fund_id' => $this->fund_id]);
        $this->assertEquals(1.500, $version);

        // Make sure we removed the private properties entry
        $version = $this->dm->db->fetchOne("SELECT value from private.properties WHERE fund_id = :fund_id", ['fund_id' => $this->fund_id]);
        $this->assertEmpty($version);
    }

    public function testSaveCount()
    {
        // Initial conditions
        $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        // Create a committee to attach the fund to
        $committee = $this->createCommittee($committee_data);
        $election_code = $committee->start_year;
        // Make fund for saving the data.
        $this->fund_id = $this->createFund($election_code, $committee->committee_id);

        // Beginning save count defaults to 0.
        $this->dm->db->executeStatement("insert into private.campaign_fund(fund_id, version) VALUES (:fund_id, :version)", ['fund_id' => $this->fund_id, 'version' => 1.500]);

        // Calling validateSave increments the save count and returns the next value.
        $this->assertEquals(1, $this->orcaProcessor->validateSave($this->fund_id, 0));
        // Subsequent calls to validate save tell ORCA that it can't save properly.
        $this->assertEquals(0, $this->orcaProcessor->validateSave($this->fund_id, 0));
        $this->assertEquals(2, $this->orcaProcessor->validateSave($this->fund_id, 1));
    }

    public function testUpdate1400Campaign()
    {
        $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        // Create a committee to attach the fund to
        $committee = $this->createCommittee($committee_data);
        $election_code = $committee->start_year;
        // Make fund for saving the data.
        $this->fund_id = $this->createFund($election_code, $committee->committee_id);

        $scenario_data = Yaml::parseFile($this->data_dir . "/campaigns/upgrade1400.yml", Yaml::PARSE_OBJECT_FOR_MAP);

        $this->dm->db->executeStatement("insert into private.campaign_fund(fund_id, version) VALUES (:fund_id, :version)", ['fund_id' => $this->fund_id, 'version' => $scenario_data->upgrade->version]);
        $this->dm->db->executeStatement("insert into private.properties(fund_id, name, value) VALUES (:fund_id, 'VNUM', :version)", ['fund_id' => $this->fund_id, 'version' => (string)$scenario_data->upgrade->version]);

        $this->createContacts($scenario_data->contacts ?? []);
        $this->createAccountEntries($scenario_data->accounts ?? []);
        $this->createDebtObligations($scenario_data->debtobligations ?? []);
        $this->createReceipts($scenario_data->receipts ?? []);

        //Upgrade Campaign
        $this->orcaProcessor->upgradeCampaign($this->fund_id);

        //Make sure version is updated.
        $version = $this->dm->db->fetchOne("SELECT version from private.campaign_fund where fund_id = :fund_id", ['fund_id' => $this->fund_id]);
        $this->assertEquals(1.500, $version);

        $version = $this->dm->db->fetchOne("SELECT value from private.properties WHERE fund_id = :fund_id", ['fund_id' => $this->fund_id]);
        $this->assertEmpty($version);

        //spot checking accounts and contacts
        $accountInsert = (object)$this->dm->db->fetchAssociative("SELECT * FROM private.vaccounts va WHERE va.fund_id = :fund_id and va.acctnum = :acctnum", ['fund_id' => $this->fund_id, 'acctnum' => 5033]);
        $accountUpdate = (object)$this->dm->db->fetchAssociative("SELECT * FROM private.vaccounts va WHERE va.fund_id = :fund_id and va.code = :code", ['fund_id' => $this->fund_id, 'code' => 'C-CHARITY']);
        $contactUpdate = (object)$this->dm->db->fetchAssociative("SELECT * FROM private.contacts c JOIN private.vaccounts va on c.trankeygen_id = va.contid WHERE va.fund_id = :fund_id and c.name = :name", ['fund_id' => $this->fund_id, 'name' => 'Charity/community organization']);

        $this->assertNotEmpty($accountInsert);
        $this->assertEquals('Disposal of surplus funds to charity', $accountInsert->description);
        $this->assertNotEmpty($accountUpdate);
        $this->assertEquals('C-CHARITY', $accountUpdate->code);
        $this->assertNotEmpty($contactUpdate);

        //--------------Vendor Debt/Credit Card---------------------//

        $vendorDebtExpenditures = $this->dm->db->fetchAllAssociative("SELECT * FROM private.vexpenditureevents ve WHERE ve.fund_id = :fund_id and ve.acctnum = 2000", ['fund_id' => $this->fund_id]);
        $this->assertNotEmpty($vendorDebtExpenditures);
        $this->assertCount(2, $vendorDebtExpenditures);

        $debtDetails = $this->dm->db->fetchAllAssociative("SELECT * FROM private.vreceipts vr where vr.fund_id = :fund_id and vr.type in (20, 28, 29, 44)", ['fund_id' => $this->fund_id]);

        $this->assertNotEmpty($debtDetails);
        $this->assertCount(7, $debtDetails);

        foreach ($debtDetails as $debtDetail) {
            $detail = (object)$debtDetail;
            switch ($detail->type) {
                case 20:
                    $baseVendorReceipt = $this->dm->db->fetchOne("SELECT * FROM private.vreceipts vr where vr.fund_id = :fund_id and vr.trankeygen_id = :pid", ['fund_id' => $this->fund_id, 'pid' => $detail->pid]);
                    $this->assertNotEmpty($baseVendorReceipt);
                    $this->assertEquals(2000, $detail->did_acctnum);
                    break;
                case 28:
                    $expenditureEvent = $this->dm->db->fetchOne("SELECT * FROM private.vexpenditureevents ve where ve.fund_id = :fund_id and ve.trankeygen_id = :pid", ['fund_id' => $this->fund_id, 'pid' => $detail->pid]);
                    $this->assertNotEmpty($expenditureEvent);
                    $this->assertThat($detail->did_acctnum, $this->logicalOr(2000, 2600));
                    break;
                case 29:
                case 44:
                    //check if pid of base vendor debt record, payment or forgiven is connected to the expenditure event
                    $expenditureEvent = $this->dm->db->fetchOne("SELECT * FROM private.vexpenditureevents ve where ve.fund_id = :fund_id and ve.trankeygen_id = :pid", ['fund_id' => $this->fund_id, 'pid' => $detail->pid]);
                    $this->assertNotEmpty($expenditureEvent);
                    $this->assertEquals(2000, $detail->cid_acctnum);
                    break;
            }
        }

        //check HDN type conversion to BUS
        $convertedContactType = $this->dm->db->fetchOne("SELECT vc.type FROM private.vcontacts vc WHERE vc.fund_id = :fund_id and vc.trankeygen_id = :trankeygen_id", ['fund_id' => $this->fund_id, 'trankeygen_id' => $this->trankeygen_map[13]]);
        $this->assertEquals('BUS', $convertedContactType);

        //check if there are any old vendor debt 19 record
        $oldDebtRecords = $this->dm->db->fetchAllAssociative("SELECT * FROM private.vreceipts vr where vr.fund_id = :fund_id and vr.type in (19, 21)", ['fund_id' => $this->fund_id]);
        $this->assertEmpty($oldDebtRecords);

        //Check if there are existing debt obligation
        $debtObligationsRecords = $this->dm->db->fetchAllAssociative("SELECT * FROM private.debtobligation dob join private.trankeygen t on t.trankeygen_id = dob.pid WHERE t.fund_id = :fund_id", ['fund_id' => $this->fund_id]);
        $this->assertEmpty($debtObligationsRecords);
    }

    public function testCreateOrcaCampaign()
    {
        // Initial conditions
        $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        // Create a committee to attach the fund to
        $committee = $this->createCommittee($committee_data);
        $election_code = $committee->start_year;
        $parameters = new stdClass();
        $parameters->election_code = $election_code;
        $parameters->campaignStartDate = $election_code . '-02-23';
        $orcaFormatDate = (new DateTime($parameters->campaignStartDate))->format('m/d/Y');
        $parameters->carryForwardAmount = '250';
        $parameters->election_codes = [$election_code . 'P', $election_code];
        // Create orca campaign
        $this->orcaProcessor->createOrcaCampaign($committee->committee_id, $parameters);
        // Make sure fund exists
        $fund = $this->dm->db->executeQuery('select * from fund where committee_id = :committee_id and election_code = :election_code',
            ['committee_id' => $committee->committee_id, 'election_code' => $election_code])->fetchAllAssociative();
        $this->assertCount(1, $fund, 'expected exactly one fund for created campaign');
        // Make sure we have the right number of accounts & contacts
        $accounts = $this->dm->db->executeQuery('select * from private.accounts a join private.trankeygen t on a.trankeygen_id = t.trankeygen_id where fund_id = :fund_id order by t.trankeygen_id',
            ['fund_id' => $fund[0]['fund_id']])->fetchAllAssociative();
        $this->assertCount(50, $accounts, 'expected newly created campaign to have 50 accounts');
        $contacts = $this->dm->db->executeQuery('select * from private.contacts a join private.trankeygen t on a.trankeygen_id = t.trankeygen_id where fund_id = :fund_id order by t.trankeygen_id desc',
            ['fund_id' => $fund[0]['fund_id']])->fetchAllAssociative();
        $this->assertCount(51, $contacts, 'expected newly created pac campaign to have 51 contacts');
        // Spot check an account
        $postage_index = array_search('Postage, mail permits, stamps, etc.', array_column($contacts, 'name'));
        $postage = (object)$contacts[$postage_index];
        $postage_account_index = array_search($postage->trankeygen_id, array_column($accounts, 'trankeygen_id'));
        $postage_account = (object)$accounts[$postage_account_index];
        $this->assertEquals(2, $postage_account->style);
        $this->assertEquals(5190, $postage_account->acctnum);
        $this->assertEquals('P', $postage_account->code);
        // Make sure carry forward receipt was created
        $receipts = $this->dm->db->executeQuery('select * from private.receipts r join private.trankeygen t on r.trankeygen_id = t.trankeygen_id where fund_id = :fund_id',
            ['fund_id' => $fund[0]['fund_id']])->fetchAllAssociative();
        $this->assertCount(1, $receipts, 'expected newly created campaign to have 1 carry forward receipt');
        // Check election participation
        $election_participation = $this->dm->db->executeQuery('select * from private.election_participation where fund_id = :fund_id',
            ['fund_id' => $fund[0]['fund_id']])->fetchAllAssociative();
        $this->assertCount(2, $election_participation);
        // Check start date
        $startDate = $this->dm->db->executeQuery('select * from private.properties where fund_id = :fund_id and name = :name',
            ['fund_id' => $fund[0]['fund_id'], 'name' => 'CAMPAIGNINFO:STARTDATE'])->fetchAssociative();
        $this->assertEquals($orcaFormatDate, $startDate['value']);
        // Make sure we have reporting obligations
        $reportingObligations = $this->dm->db->executeQuery('select * from private.reporting_obligation where fund_id = :fund_id',
            ['fund_id' => $fund[0]['fund_id']])->fetchAssociative();
        $this->assertNotEmpty($reportingObligations);

        //test candidate campaign
        $candidate_committee_data = Yaml::parseFile($this->data_dir . "/committees/candidate.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        $candidate_committee = $this->createCommittee($candidate_committee_data);
        $election_code = $committee->start_year;
        $parameters = new stdClass();
        $parameters->election_code = $election_code;
        $parameters->election_codes[0] = null;
        $parameters->campaignStartDate = $election_code . '-03-04';
        // Create orca campaign
        $this->orcaProcessor->createOrcaCampaign($candidate_committee->committee_id, $parameters);
        $fund = $this->dm->db->executeQuery('select * from fund where committee_id = :committee_id and election_code = :election_code',
            ['committee_id' => $candidate_committee->committee_id, 'election_code' => $election_code])->fetchAllAssociative();
        //make sure election defaulted
        $election_participation = $this->dm->db->executeQuery('select * from private.election_participation where fund_id = :fund_id',
            ['fund_id' => $fund[0]['fund_id']])->fetchAssociative();
        $this->assertNotEmpty($election_participation);
        //make sure we carry over seec filer property
        $seec = $this->dm->db->executeQuery("select value from private.properties where fund_id = :fund_id and name = 'SEEC_FILER'",
          ['fund_id' => $fund[0]['fund_id']])->fetchOne();
        $this->assertEquals('true', $seec);

        // Test surplus campaign
      $surplus_committee_data = Yaml::parseFile($this->data_dir . "/committees/surplus.yml", Yaml::PARSE_OBJECT_FOR_MAP);
      $surplus_committee = $this->createCommittee($surplus_committee_data);
      $election_code = $surplus_committee->start_year;
      $parameters = new stdClass();
      $parameters->election_code = $election_code;
      $parameters->election_codes[0] = null;
      $parameters->campaignStartDate = $election_code . '-03-04';
      // Create orca campaign
      $this->orcaProcessor->createOrcaCampaign($surplus_committee->committee_id, $parameters);
      $fund = $this->dm->db->executeQuery('select * from fund where committee_id = :committee_id and election_code = :election_code',
        ['committee_id' => $candidate_committee->committee_id, 'election_code' => $election_code])->fetchAllAssociative();
      //make sure election defaulted
      $election_participation = $this->dm->db->executeQuery('select * from private.election_participation where fund_id = :fund_id',
        ['fund_id' => $fund[0]['fund_id']])->fetchAssociative();
      $this->assertNotEmpty($election_participation);
      //make sure we carry over seec filer property
      $seec = $this->dm->db->executeQuery("select value from private.properties where fund_id = :fund_id and name = 'SEEC_FILER'",
        ['fund_id' => $fund[0]['fund_id']])->fetchOne();
      $this->assertEquals('true', $seec);
    }

    /**
     * Test Adding and saving properties
     * @covers \WAPDC\CampaignFinance\OrcaProcessor::generateProperty
     * @covers \WAPDC\CampaignFinance\OrcaProcessor::updateProperties
     */
    public function testUpdateProperties()
    {
        $this->createTestPac();
        $fund_id = $this->createFund('2021', $this->committee->committee_id);

        $this->dm->db->executeStatement("INSERT INTO private.properties (fund_id, name, value) VALUES (:fund_id, :name, :value)",
            ['fund_id' => $fund_id, 'name' => 'EXISTINGPROPERTY', 'value' => 'EXISTINGVALUE']);
        $properties = [];
        $properties[] = $this->orcaProcessor->generateProperty("TESTNAME", "TESTVALUE");
        $properties[] = $this->orcaProcessor->generateProperty("EXISTINGPROPERTY", "MODIFIEDVALUE");

        $this->orcaProcessor->updateProperties($fund_id, $properties);
        $fund = $this->orcaProcessor->getFundData($fund_id);

        // Find the property in the loaded data so we know it saved.
        [$testname] = array_values(array_filter($fund->properties, function ($el) {
            return $el->name == "TESTNAME";
        }));
        $this->assertSame("TESTVALUE", $testname->value);

        // Find the modified in the loaded data so we know it saved.
        [$testname] = array_values(array_filter($fund->properties, function ($el) {
            return $el->name == "EXISTINGPROPERTY";
        }));
        $this->assertSame("MODIFIEDVALUE", $testname->value);
    }

    public function importScenarioProvider(): array
    {
        return [
            ['importContacts'],
            ['importData'],
            ['newCampaign'],
        ];
    }

    /**
     * @param $import_scenario
     * @throws Exception
     * @dataProvider importScenarioProvider
     */
    public function testImportPriorCampaignDataContinuing($import_scenario)
    {
        // The data from the previous campaign to be imported
        $committee_data = Yaml::parseFile($this->data_dir . '/committees/continuing-pac.yml', Yaml::PARSE_OBJECT_FOR_MAP);
        $import_committee = $this->createCommittee($committee_data);
        $election_code = $import_committee->start_year;
        $import_fund_id = $this->createFund($election_code, $import_committee->committee_id);
        $import_data = Yaml::parseFile($this->data_dir . "/campaigns/import-continuing-campaign.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        $this->createContacts($import_data->contacts ?? []);
        $this->createCoupleContacts($import_data->couple_contacts ?? []);
        $this->createGroupContacts($import_data->group_contacts ?? []);
        $this->createReceipts($import_data->receipts ?? []);
        $this->createAccountEntries($import_data->accounts ?? []);
        $this->createLoans($import_data->loans ?? []);

        // Initial conditions for current fund
        $new_election_code = $import_data->election_code;
        $import_option = $import_scenario;
        $parameters = new stdClass();
        $parameters->election_code = $new_election_code;
        $parameters->campaignStartDate = $new_election_code . '-02-02';
        $parameters->election_codes = [$new_election_code . 'P', $new_election_code];
        $parameters->importFund = $import_fund_id;
        $parameters->import = $import_option;
        $new_fund_id = $this->orcaProcessor->createOrcaCampaign($import_committee->committee_id, $parameters);

        if ($import_option != 'newCampaign') {
            $contacts = $this->dm->db->fetchAllAssociative("select * from private.vcontacts where fund_id = :fund_id AND type != 'FI'", ['fund_id' => $new_fund_id]);
            $individual_contacts = $this->dm->db->fetchAllAssociative('select * from private.individualcontacts ic join private.trankeygen vc on vc.trankeygen_id = ic.trankeygen_id where vc.fund_id = :fund_id', ['fund_id' => $new_fund_id]);
            $couple_contacts = $this->dm->db->fetchAllAssociative('select * from private.couplecontacts cc join private.trankeygen t on t.trankeygen_id = cc.trankeygen_id where t.fund_id = :fund_id', ['fund_id' => $new_fund_id]);
            $group_contacts = $this->dm->db->fetchAllAssociative('select * from private.registeredgroupcontacts gc join private.trankeygen t on t.trankeygen_id = gc.gconid where t.fund_id = :fund_id', ['fund_id' => $new_fund_id]);

            //confirm import count
            $this->assertCount(6, $contacts);

            foreach ($contacts as $contact) {
                $new_contact = (object)$contact;
                $old_contact = (object)$this->dm->db->fetchAssociative("select * from private.vcontacts vc where vc.fund_id = :fund_id and vc.trankeygen_id = :trankeygen_id", ['fund_id' => $import_fund_id, 'trankeygen_id' => $new_contact->contact_key]);

                $this->assertNotEmpty($old_contact);
                $this->assertEquals($old_contact->name, $new_contact->name);
                $this->assertEquals($old_contact->type, $new_contact->type);

            }

            //confirm IND type count
            $this->assertCount(2, $individual_contacts);

            //confirm couple contact count and mapping
            $this->assertCount(1, $couple_contacts);

            //confirm group contact count and mapping
            $this->assertCount(4, $group_contacts);

            if ($import_option == 'importData') {

                //testing bank account balance carry forward (Sum if there are multiple 1000 accounts)
                $carry_forward_bank_balance = $this->dm->db->fetchOne('select sum(va.amount) from private.vreceipts va WHERE va.fund_id = :fund_id AND va.did_acctnum = 4600 AND va.cid_acctnum = 1000', ['fund_id' => $new_fund_id]);
                $this->assertEquals(12345.00 + 6789.00, $carry_forward_bank_balance);

                //testing multiple bank accounts
                $carry_forward_bank_accounts = $this->dm->db->fetchAllAssociative('select va.*, c.contact_key FROM private.vaccounts va join private.contacts c ON c.trankeygen_id = va.trankeygen_id WHERE va.fund_id = :fund_id AND va.acctnum = 1000 ORDER BY va.trankeygen_id', ['fund_id' => $new_fund_id]);
                $this->assertCount(2, $carry_forward_bank_accounts);

                foreach ($carry_forward_bank_accounts as $new_bank_account) {
                    $new_bank = (object)$new_bank_account;
                    $old_bank = (object)$this->dm->db->fetchAssociative("select * from private.vaccounts va join private.contacts c on c.trankeygen_id = va.trankeygen_id where va.fund_id = :fund_id and va.trankeygen_id = :trankeygen_id", ['fund_id' => $import_fund_id, 'trankeygen_id' => $new_bank->contact_key]);

                    $first_old_bank = $this->dm->db->fetchOne("select MIN(va.trankeygen_id) from private.vaccounts va where va.fund_id = :fund_id AND va.acctnum = 1000", ['fund_id' => $import_fund_id]);

                    $this->assertNotEmpty($old_bank);
                    $this->assertEquals($old_bank->name, $new_bank->name);
                    if($new_bank->contact_key == $first_old_bank) {
                        $this->assertEquals($old_bank->total, ($new_bank->total - 420.00));

                    } else {
                        $this->assertEquals($old_bank->total, $new_bank->total);
                    }
                }

                //testing petty cash carry forward
                $carry_forward_petty_cash = $this->dm->db->fetchOne('select sum(va.amount) from private.vreceipts va WHERE va.fund_id = :fund_id AND va.cid_acctnum = 1800', ['fund_id' => $new_fund_id]);
                $this->assertEquals(943.33, $carry_forward_petty_cash);

                //testing 2100, 2200 loan account creation w/ carry forward
                $carry_forward_debt_account_balance = $this->dm->db->fetchOne('select a.total from private.accounts a LEFT JOIN private.trankeygen t ON t.trankeygen_id = a.trankeygen_id WHERE t.fund_id = :fund_id AND a.acctnum = 5001', ['fund_id' => $new_fund_id]);
                $loan_accounts = $this->dm->db->fetchAllAssociative('select va.* from private.vaccounts va WHERE va.fund_id = :fund_id AND va.acctnum IN (2100, 2200)', ['fund_id' => $new_fund_id]);
                $loan_import_receipts = $this->dm->db->fetchAllAssociative('select * from private.vreceipts vr WHERE vr.fund_id = :fund_id AND vr.type IN (3, 5) ', ['fund_id' => $new_fund_id]);
                $this->assertCount(2, $loan_accounts);
                $this->assertCount(2, $loan_import_receipts);
                $this->assertEquals(321.12, $carry_forward_debt_account_balance);

                //check payment on cash loan
                $cash_loan_amount_with_payment = $this->dm->db->fetchOne('select va.total from private.vaccounts va WHERE va.fund_id = :fund_id AND va.acctnum = 2100', ['fund_id' => $new_fund_id]);
                $this->assertEquals(-432.23 + 12.23, $cash_loan_amount_with_payment);
            };
        }
        // Make sure new non-imported fund exists
        $fund = $this->dm->db->executeQuery('select * from fund where committee_id = :committee_id and election_code = :election_code',
            ['committee_id' => $import_committee->committee_id, 'election_code' => $election_code])->fetchAllAssociative();
        $this->assertCount(1, $fund, 'expected exactly one fund for created campaign');

    }

    /**
     * @param $import_scenario
     * @throws Exception
     * @dataProvider importScenarioProvider
     */
    public function testImportPriorCampaignDataCandidate($import_scenario)
    {
        //test candidate campaign
        $candidate_committee_data = Yaml::parseFile($this->data_dir . "/committees/candidate.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        $import_candidate_committee = $this->createCommittee($candidate_committee_data);
        $election_code = '2020';
        $import_fund_id = $this->createFund($election_code, $import_candidate_committee->committee_id);
        $import_data = Yaml::parseFile($this->data_dir . "/campaigns/import-candidate-campaign.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        $this->createContacts($import_data->contacts ?? []);
        $this->createReceipts($import_data->receipts ?? []);
        $this->createAccountEntries($import_data->accounts ?? []);
        $this->createLoans($import_data->loans ?? []);

        // Initial conditions for current fund
        $new_candidate_committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-candidate.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        $target_committee = $this->createCommittee($new_candidate_committee_data);
        $new_election_code = '2021';
        $import_option = $import_scenario;
        $parameters = new stdClass();
        $parameters->election_code = $new_election_code;
        $parameters->election_codes[0] = null;
        $parameters->campaignStartDate = $election_code . '-03-04';
        $parameters->importFund = $import_fund_id;
        $parameters->import = $import_option;
        $new_fund_id = $this->orcaProcessor->createOrcaCampaign($target_committee->committee_id, $parameters);
        if ($import_option != 'newCampaign') {
            $contacts = $this->dm->db->fetchAllAssociative("select * from private.vcontacts where fund_id = :fund_id AND type != 'FI'", ['fund_id' => $new_fund_id]);
            $individual_contacts = $this->dm->db->fetchAllAssociative('select * from private.individualcontacts ic join private.trankeygen vc on vc.trankeygen_id = ic.trankeygen_id where vc.fund_id = :fund_id', ['fund_id' => $new_fund_id]);

            //confirm import count
            $this->assertCount(2, $contacts);

            foreach ($contacts as $contact) {
                $new_contact = (object)$contact;
                $old_contact = (object)$this->dm->db->fetchAssociative("select * from private.vcontacts vc where vc.fund_id = :fund_id and vc.trankeygen_id::text = :contact_key", ['fund_id' => $import_fund_id, 'contact_key' => $new_contact->contact_key]);

                $this->assertNotEmpty($old_contact);
                $this->assertEquals($old_contact->name, $new_contact->name);
                $this->assertEquals($old_contact->type, $new_contact->type);

            }

            //confirm IND type count
            $this->assertCount(1, $individual_contacts);

            if ($import_option == 'importData') {

                //testing bank account balance carry forward (Sum if there are multiple 1000 accounts)
                $carry_forward_bank_balance = $this->dm->db->fetchOne('select sum(va.amount) from private.vreceipts va WHERE va.fund_id = :fund_id AND va.did_acctnum = 4600 AND va.cid_acctnum = 1000', ['fund_id' => $new_fund_id]);
                $this->assertEquals(12345.00 + 6789.00, $carry_forward_bank_balance);

                //testing multiple bank accounts
                $carry_forward_bank_accounts = $this->dm->db->fetchAllAssociative('select va.*, c.contact_key FROM private.vaccounts va join private.contacts c ON c.trankeygen_id = va.trankeygen_id WHERE va.fund_id = :fund_id AND va.acctnum = 1000 ORDER BY va.trankeygen_id', ['fund_id' => $new_fund_id]);
                $this->assertCount(2, $carry_forward_bank_accounts);

                foreach ($carry_forward_bank_accounts as $new_bank_account) {
                    $new_bank = (object)$new_bank_account;
                    $old_bank = (object)$this->dm->db->fetchAssociative("select * from private.vaccounts va join private.contacts c on c.trankeygen_id = va.trankeygen_id where va.fund_id = :fund_id and va.trankeygen_id = :trankeygen_id", ['fund_id' => $import_fund_id, 'trankeygen_id' => $new_bank->contact_key]);

                    $first_old_bank = $this->dm->db->fetchOne("select MIN(va.trankeygen_id) from private.vaccounts va where va.fund_id = :fund_id AND va.acctnum = 1000", ['fund_id' => $import_fund_id]);

                    $this->assertNotEmpty($old_bank);
                    $this->assertEquals($old_bank->name, $new_bank->name);
                    if($new_bank->contact_key == $first_old_bank) {
                        $this->assertEquals($old_bank->total, ($new_bank->total - 420.00));

                    } else {
                        $this->assertEquals($old_bank->total, $new_bank->total);
                    }
                }


                //testing petty cash carry forward
                $carry_forward_petty_cash = $this->dm->db->fetchOne('select sum(va.amount) from private.vreceipts va WHERE va.fund_id = :fund_id AND va.cid_acctnum = 1800', ['fund_id' => $new_fund_id]);
                $this->assertEquals(943.33, $carry_forward_petty_cash);

                //testing 2100, 2200 loan account creation w/ carry forward
                $carry_forward_debt_account_balance = $this->dm->db->fetchOne('select a.total from private.accounts a LEFT JOIN private.trankeygen t ON t.trankeygen_id = a.trankeygen_id WHERE t.fund_id = :fund_id AND a.acctnum = 5001', ['fund_id' => $new_fund_id]);
                $loan_accounts = $this->dm->db->fetchAllAssociative('select va.* from private.vaccounts va WHERE va.fund_id = :fund_id AND va.acctnum IN (2100, 2200)', ['fund_id' => $new_fund_id]);
                $loan_import_receipts = $this->dm->db->fetchAllAssociative('select * from private.vreceipts vr WHERE vr.fund_id = :fund_id AND vr.type IN (3, 5) ', ['fund_id' => $new_fund_id]);
                $this->assertCount(2, $loan_accounts);
                $this->assertCount(2, $loan_import_receipts);
                $this->assertEquals(321.12, $carry_forward_debt_account_balance);

                //check payment on cash loan
                $cash_loan_amount_with_payment = $this->dm->db->fetchOne('select va.total from private.vaccounts va WHERE va.fund_id = :fund_id AND va.acctnum = 2100', ['fund_id' => $new_fund_id]);
                $this->assertEquals(-432.23 + 12.23, $cash_loan_amount_with_payment);
            };
        }
        // Make sure new non-imported fund exists
        $fund = $this->dm->db->executeQuery('select * from fund where committee_id = :committee_id and election_code = :election_code',
            ['committee_id' => $import_candidate_committee->committee_id, 'election_code' => $election_code])->fetchAllAssociative();
        $this->assertCount(1, $fund, 'expected exactly one fund for created campaign');

    }
    public function testUpdateCampaignVendor()
    {
        // Initial conditions
        $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
        // Create a committee to attach the fund to
        $committee = $this->createCommittee($committee_data);
        $election_code = $committee->start_year;
        // Make fund for saving the data.
        $this->fund_id = $this->createFund($election_code, $committee->committee_id);

        $scenario_data = Yaml::parseFile($this->data_dir . "/campaigns/update-campaign-vendor.yml", Yaml::PARSE_OBJECT_FOR_MAP);

        $this->orcaProcessor->updateCampaignVendor($this->fund_id, $scenario_data->vendor1);

        // Update first
        $vendor = $this->dm->db->fetchOne("SELECT vendor from public.fund where fund_id = :fund_id", ['fund_id' => $this->fund_id]);
        $this->assertEquals($scenario_data->vendor1->vendor, $vendor);

        $this->orcaProcessor->updateCampaignVendor($this->fund_id, $scenario_data->vendor2);

        // Try update again
        $vendor2 = $this->dm->db->fetchOne("SELECT vendor from public.fund where fund_id = :fund_id", ['fund_id' => $this->fund_id]);
        $this->assertEquals($scenario_data->vendor2->vendor, $vendor2);
    }

  /**
   * @throws OptimisticLockException
   * @throws Exception
   * @throws ORMException
   */
  public function testRestoreCampaign() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;

    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    $user = new stdClass();
    $user->user_name = 'some@email.com';

    $this->orcaProcessor->createRestoreRequestEntry($this->fund_id, $user);

    $rows = $this->dm->db->fetchAllAssociative(
        'select * from private.restore_request where fund_id = :fund_id and email = :email',
        ['fund_id' => $this->fund_id, 'email' => $user->user_name]
    );

    $this->assertCount(1, $rows);
    $entry = reset($rows);
    $this->assertEquals($this->fund_id, $entry['fund_id']);
    $this->assertEquals($user->user_name, $entry['email']);
    $this->assertEquals(false, $entry['validate']);
  }
}
