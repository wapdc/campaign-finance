<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class PledgeTest extends ORCATestCase {
  public function testSavePledge() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/pledge.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    $expectedPledge = $scenario_data->pledge;
    $this->setTrankeygenIds($expectedPledge, ['contid']);

    $savedPledge = $this->orcaProcessor->savePledge($this->fund_id, $scenario_data->pledge);
    $savedPledge = $savedPledge->pledge;
    foreach ($expectedPledge as $property => $value){
      switch($property){
        default:
          $this->assertSame($value, $savedPledge->$property ?? null, $property);
      }
    }

    $this->assertNotEmpty($savedPledge->deptkey);
    $this->assertAccountBalance($this->trankeygen_map[1], -10);
    $this->assertAccountBalance($savedPledge->cid, 10);
  }

  public function testSavePledgePayment() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/pledge-payment.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Setup committee
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    $payment = $scenario_data->payment;
    //Change the pointers for the pid and contid to the generated trankeygen values
    $this->setTrankeygenIds($payment, ['contid', 'pid']);

    $savedContribution = $this->orcaProcessor->saveMonetaryContribution($this->fund_id, $payment)->{'monetary-contribution'};
    $this->assertNotEmpty($savedContribution->trankeygen_id);
    $this->assertSame($payment->date_, $savedContribution->date_);
    $this->assertSame($payment->amount, $savedContribution->amount);
    $this->assertSame($payment->checkno, $savedContribution->checkno);
    $this->assertSame($payment->memo, $savedContribution->memo);
    $this->assertSame($payment->pid, $savedContribution->pid);
    $this->assertSame(0,$savedContribution->aggtype);


    $this->assertAccountBalance($this->trankeygen_map[101], 15);
    $this->assertAccountBalance($this->trankeygen_map[1600], 5);
  }

  public function testSaveInKindPledgePayment() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/pledge-in-kind-payment.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Setup committee
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);

    $payment = $scenario_data->payment;
    //Change the pointers for the pid and contid to the generated trankeygen values
    $this->setTrankeygenIds($payment, ['contid', 'pid', 'cid']);

    $savedContribution = $this->orcaProcessor->saveInKindContribution($this->fund_id, $payment)->{'in-kind-contribution'};
    $this->assertNotEmpty($savedContribution->trankeygen_id);
    $this->assertSame($payment->date_, $savedContribution->date_);
    $this->assertSame($payment->amount, $savedContribution->amount);
    $this->assertSame($payment->memo, $savedContribution->memo);
    $this->assertSame($payment->pid, $savedContribution->pid);
    $this->assertSame(0, $savedContribution->aggtype);

    $this->assertAccountBalance($this->trankeygen_map[101], 195);
    $this->assertAccountBalance($this->trankeygen_map[5000], 5);
  }

  public function testDeletePledge() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/pledge-delete.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = Yaml::parseFile($this->data_dir . '/committees/continuing-pac.yml', Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee);
    $election_code = $committee->start_year;
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createReceipts($scenario_data->receipts ??  []);
    $this->setTrankeygenIds($scenario_data->delete, ['receipt_id']);
    $this->orcaProcessor->deletePledge($this->fund_id, $scenario_data->delete->receipt_id);

    $receipt = $this->dm->db->executeQuery('select * from private.receipts where trankeygen_id = :id', ['id' =>$this->trankeygen_map[101]])->fetchOne();
    $this->assertFalse($receipt);

    $accounts = $this->dm->db->executeQuery('select * from private.accounts where pid = :id', ['id' =>$this->trankeygen_map[4900]])->fetchOne();
    $this->assertFalse($accounts);

    $this->assertAccountBalance($this->trankeygen_map[1], 0);
  }

  public function testCancelPledge(){
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/pledge-cancel.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createReceipts($scenario_data->receipts ??  []);

    $expectedPledgeCancel = $scenario_data->cancel;

    $this->setTrankeygenIds($expectedPledgeCancel, ['pid', 'contid']);

    $savedPledgeCancel = $this->orcaProcessor->savePledgeCancel($this->fund_id, $scenario_data->cancel);

    $savedPledgeCancel = $savedPledgeCancel->{'pledge-cancel'};

    foreach ($expectedPledgeCancel as $property => $value){
      switch($property){
        default:
          $this->assertSame($value, $savedPledgeCancel->$property ?? null, $property);
      }
    }

    $this->assertNotEmpty($savedPledgeCancel->deptkey);
    $this->assertAccountBalance($this->trankeygen_map[4900], 15);
    $this->assertAccountBalance($this->trankeygen_map[1], -15);
  }
}