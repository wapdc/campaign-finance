<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Tests\WAPDC\CampaignFinance\ORCA\OrcaProcessor;

use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;

class AuctionTest extends ORCATestCase {
  public function testSaveAuction() {
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/auction.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    $savedAuction = $this->orcaProcessor->saveAuction($this->fund_id, $scenario_data->auction)->auction;

    foreach ($scenario_data->auction as $property => $value) {
      $this->assertSame($value, $savedAuction->$property ?? NULL, $property);
    }
  }

  public function testDeleteAuction(){
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/auction-delete.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    $this->createAccountEntries($scenario_data->accounts ?? []);
    //create auction items
    $this->createAuctionItems($scenario_data->auctionitems ?? []);
    //create receipts accordingly
    $this->createReceipts($scenario_data->receipts ?? []);

    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $auction = $this->orcaProcessor->saveAuction($this->fund_id, $scenario_data->auction)->auction;

    $auctionId = $auction->trankeygen_id;

    $this->assertNotEmpty($auctionId);

    $this->orcaProcessor->deleteAuction($this->fund_id, $auctionId);

    $auction = $this->dm->db->fetchOne("select cast(private.cf_get_fundraiser(:fund_id, :trankeygen_id) as text)", ["fund_id" => $this->fund_id, "trankeygen_id" => $auctionId]);

    $this->assertEmpty($auction);

    $accounts = $this->dm->db->fetchAllAssociative("SELECT * from private.vaccounts where pid = :pid" , ['pid' => $auctionId]);
    $this->assertEmpty($accounts);

    $this->assertAccountBalance($this->trankeygen_map[1600], 0);
    $this->assertAccountBalance($this->trankeygen_map[4098], 0);
  }

  public function testSaveAuctionItem() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenario_data = Yaml::parseFile($this->data_dir . "/fundraisers/auction-item.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Create a committee to attach the fund to
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;

    // Make fund for saving the data.
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // Make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts);
    $this->createAccountEntries($scenario_data->accounts);

    $expectedAuctionItem = $scenario_data->auctionItem;
    $this->setTrankeygenIds($expectedAuctionItem, ['pid', 'donor_contid', 'buyer_contid']);

    $savedAuctionItem = $this->orcaProcessor->saveAuctionItem($this->fund_id, $scenario_data->auctionItem)->{'auction-item'};

    $this->assertNotEmpty($savedAuctionItem->donor_rds_id);
    $this->assertNotEmpty($savedAuctionItem->buyer_rds_id);
    $this->assertEquals(10, $savedAuctionItem->donor_donation);
    $this->assertEquals(1, $savedAuctionItem->buyer_donation);

    foreach ($expectedAuctionItem as $property => $value) {
      $this->assertSame($value, $savedAuctionItem->$property ?? null, $property);
    }

    //donor amount
    $this->assertAccountBalance($this->trankeygen_map[1600], 11);
    $this->assertContactPagg($this->trankeygen_map[11], 10.00);

    //buyer amount
    $this->assertAccountBalance($this->trankeygen_map[4098], -11);
    $this->assertContactPagg($this->trankeygen_map[12], 1.00);
  }

  public function testDeleteAuctionItem(){
    // fetch auction scenario
    $scenario_data = Yaml::parseFile($this->data_dir . "/auctions/auction-item-delete.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // create committee
    $committee = $this->createCommittee($committee_data);
    $election_code = $committee->start_year;
    // create fund
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    // create contact
    $this->createContacts($scenario_data->contacts ?? []);
    // create account entry
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createAuctionItems($scenario_data->auctionitems ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $auction_item_id = $this->trankeygen_map[11];

    $this->orcaProcessor->deleteAuctionItem($this->fund_id, $auction_item_id);

    // fetch and assert auction is empty
    $auction_items = $this->dm->db->fetchOne("SELECT trankeygen_id from private.auctionitems where trankeygen_id=:id", ['id' => $auction_item_id]);
    $this->assertEmpty($auction_items);

    $receipts = $this->dm->db->fetchOne("Select trankeygen_id from private.vreceipts where pid=:id", ['id' => $auction_item_id ]);
    $this->assertEmpty($receipts);

    $accounts = $this->dm->db->fetchAllAssociative("SELECT * from private.vaccounts where pid = :pid" , ['pid' => $auction_item_id]);
    $this->assertEmpty($accounts);

    $this->assertAccountBalance($this->trankeygen_map[1600], 0);
    $this->assertContactGagg($this->trankeygen_map[21], 0);
    $this->assertContactGagg($this->trankeygen_map[22], 0);
  }
}