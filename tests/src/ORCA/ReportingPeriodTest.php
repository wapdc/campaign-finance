<?php

namespace Tests\WAPDC\CampaignFinance\ORCA;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use stdClass;
use Symfony\Component\Yaml\Yaml;

class ReportingPeriodTest extends ORCATestCase
{
  /**
   * given a fund, populate the reporting obligations. If an incorrect_type is specified,
   * the reporting periods will be populated mimicking how reporting periods were
   * previously populated incorrectly when start & end date
   * for a monthly reporting and election based reporting period perfectly aligned.
   *
   * @param $fund
   * @param $incorrect_type
   * @return void
   * @throws \Doctrine\DBAL\Exception
   */
  private function populateFundWithObligations($fund, $incorrect_type=null) {
    //populate reporting obligations normally
    $this->dm->db->executeQuery("select * from private.cf_populate_reporting_obligations(:fund_id)",
      ['fund_id' => $fund]);
    if ($incorrect_type =='continuing-pac') {
      //update the obligation due on 4/10 to be due on 4/1 as was previously incorrectly being done
      $this->dm->db->executeQuery("update private.reporting_obligation set 
                                      reporting_period_id = 186,
                                      duedate = '04-01-2025'
                                    where fund_id = :fund_id and duedate = '04-10-2025'",
        ['fund_id' => $fund]);
    } else if ($incorrect_type == 'candidate') {
      //update obligations due to be a combined reporting period from march 1 to the end of april as was previously being done
      $this->dm->db->executeQuery("
                                    delete from private.reporting_obligation where fund_id = :fund_id and duedate = '04-10-2025'; 
                                    ",
        ['fund_id' => $fund]);
      $this->dm->db->executeQuery("
                                    update private.reporting_obligation set 
                                      startdate = '03-01-2025'
                                    where fund_id = :fund_id and duedate = '05-12-2025'; 
                                    ",
        ['fund_id' => $fund]);
    }
  }

  /**
   * Scenarios for generating funds from oRCA
   */
  private function generateFundProvider(): array
  {
    return [
      ['candidate'],
      ['continuing-pac'],
    ];
  }

  /**
   * Test continuing committee and candidate committee can add & remove election participation
   * and get expected reporting periods
   *
   * @param $scenario
   * @throws Exception
   * @throws \Doctrine\DBAL\Exception
   * @throws ORMException
   * @throws OptimisticLockException
   * @dataProvider generateFundProvider
   *
   */
  public function testReportingPeriodsElectionParticipation($scenario){
    $testDataDir = $this->data_dir;
    $scenarioData = Yaml::parseFile("$testDataDir/committees/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenarioData->committee->election_code = "2025";
    $scenarioData->committee->end_year = "2025";
    $committee = $this->createCommittee($scenarioData);
    $fund = $this->createFund('2025', $committee->committee_id);
    $this->populateFundWithObligations($fund, $scenario);
    $json = new stdClass();
    $json->campaign_start_date = '2025-01-01';
    $json->election_codes = ['2025'];
    $this->orcaProcessor->saveCampaignSettings($fund, json_encode($json));
    $obligations = $this->dm->db->fetchAllAssociative('select * from private.reporting_obligation where fund_id = :fund_id',
      ['fund_id' => $fund]);
    $this->assertEquals('2025-04-10', $obligations[2]['duedate']);
    $json->election_codes = ['2025', '2025S4'];
    $this->orcaProcessor->saveCampaignSettings($fund, json_encode($json));
    $newObligations = $this->dm->db->fetchAllAssociative('select * from private.reporting_obligation where fund_id = :fund_id',
      ['fund_id' => $fund]);
    $this->assertEquals('2025-04-01', $newObligations[2]['duedate']);
  }

  //

  /**
   * Ensure we get expected reporting periods when using combined reporting periods
   *
   * @return void
   * @throws Exception
   * @throws ORMException
   * @throws OptimisticLockException
   * @throws \Doctrine\DBAL\Exception
   */
  public function testCombinedReportingPeriods(): void
  {
    $scenarioData = Yaml::parseFile("$this->data_dir/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($scenarioData);
    $fund = $this->createFund('2025', $committee->committee_id);
    $this->populateFundWithObligations($fund);
    $obligations = $this->dm->db->fetchAllAssociative('select * from private.reporting_obligation where fund_id = :fund_id',
      ['fund_id' => $fund]);
    $this->orcaProcessor->combineReportingPeriods($fund, $obligations[2]['obligation_id'], '2025-02-01');
    $combinedObligations = $this->dm->db->fetchAllAssociative('select * from private.reporting_obligation where fund_id = :fund_id',
      ['fund_id' => $fund]);
    $this->assertEquals('2025-04-10', $combinedObligations[1]['duedate']);
  }

  /**
   * Verify that if you change election participation on a committee that has already filed monthly (e.g. 2024)
   * that the partial reporting period dates alter correctly.
   *
   * @return void
   * @throws Exception
   * @throws ORMException
   * @throws OptimisticLockException
   * @throws \Doctrine\DBAL\Exception
   */
  public function testAlreadyFiled(): void
  {
    $scenarioData = Yaml::parseFile("$this->data_dir/committees/candidate.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $scenarioData->committee->election_code = "2025";
    $committee = $this->createCommittee($scenarioData);
    $fund = $this->createFund('2025', $committee->committee_id);
    $this->populateFundWithObligations($fund, 'candidate');
    $this->dm->db->executeStatement(
      "INSERT INTO report(fund_id, report_type, period_start, period_end) values (:fund_id, 'C4', :period_start, :period_end)",
      [
        'fund_id' => $this->fund_id,
        'period_start' => '2025-01-01',
        'period_end' => '2025-01-31'
      ]);
    $this->dm->db->executeStatement(
      "INSERT INTO report(fund_id, report_type, period_start, period_end) values (:fund_id, 'C4', :period_start, :period_end)",
      [
        'fund_id' => $this->fund_id,
        'period_start' => '2024-12-01',
        'period_end' => '2024-12-31'
      ]);
    $json = new stdClass();
    $json->campaign_start_date = '2024-01-01';
    $json->election_codes = array("2025");
    $this->orcaProcessor->saveCampaignSettings($fund, json_encode($json));
    $obligations = $this->dm->db->fetchAllAssociative('select * from private.reporting_obligation where fund_id = :fund_id',
      ['fund_id' => $fund]);
    $this->assertEquals('2025-04-10', $obligations[14]['duedate']);
  }

}