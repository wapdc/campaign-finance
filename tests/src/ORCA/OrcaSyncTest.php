<?php

namespace Tests\WAPDC\CampaignFinance\ORCA;

use \Exception;
use PHPUnit\Framework\TestCase;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\Model\Fund;
use WAPDC\CampaignFinance\OrcaProcessor;

class OrcaSyncTest extends TestCase
{

  /** @var CFDataManager */
  private $dm;

  /** @var Fund */
  private $fund;

  /** @var OrcaProcessor */
  protected $orcaProcessor;


  /**
   * @var string
   */
  private $data_dir;

  /**
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->data_dir = dirname(dirname(__DIR__)) . '/data/orca';
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->orcaProcessor = new OrcaProcessor($this->dm);
  }

  private function createFund() {
    $fund = $this->fund =  new Fund();
    $fund->election_code = '2021';
    $fund->committee_id = 11111;
    $fund->filer_id = 'TEST.FILER.ZZZZ';
    $this->dm->em->persist($fund);
    $this->dm->em->flush($fund);
  }

  public function accountSyncProvider() {
    return [[null],[1.470]];
  }

  /**
   * @dataProvider accountSyncProvider
   * @param $version
   * @throws \Doctrine\DBAL\Exception
   */
  public function testSyncAccounts( $version) {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/accounts/sync-accounts.json");
    $date = $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents, $version);
    $this->assertNotEmpty($date);

    $data = json_decode($contents);
    // Create a map based on ORCA id so we can access them easily for assertions
    $map = [];
    foreach ($data->accounts as $account) {
      $map[$account->orca_id] = $account;
    }

    $sql = "SELECT a.*, t.orca_id, t.derby_updated, tp.orca_id as orca_pid, tc.orca_id as orca_contid 
      from private.accounts a join private.trankeygen t on t.trankeygen_id = a.trankeygen_id 
        left join private.trankeygen tp on tp.trankeygen_id=t.pid
        left join private.trankeygen tc on tc.trankeygen_id=a.contid
     where t.fund_id = :fund_id order by t.orca_id";

    // Retrieve saved values in the database
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_account) {
      // Orca should be one we have saved.
      $saved_account = (object)$saved_account;
      $this->assertArrayHasKey($saved_account->orca_id, $map);
      $account = $map[$saved_account->orca_id];
      if (!empty($account->pid)) {
        $this->assertSame($account->pid, $saved_account->orca_pid);
      }
      else {
        $this->assertEmpty($saved_account->orca_pid);
      }
      $this->assertSame($account->acctnum, $saved_account->acctnum);
      $this->assertSame($account->contid, $saved_account->orca_contid);
      $this->assertSame($account->code, $saved_account->code);
      $this->assertSame($account->description, $saved_account->description);
      $this->assertSame($account->style, $saved_account->style);
      $this->assertEquals($account->total, $saved_account->total);
      $this->assertSame($account->memo, $saved_account->memo);
      $this->assertSame($account->date_, $saved_account->date_);
      $this->assertSame($account->derby_updated, $saved_account->derby_updated);
    }

    // Second save for updates
    $this->dm->db->executeStatement("update private.accounts set pid=-1, acctnum=0, contid=-1, code=null, description=null, style=0, total=0, memo=null, date_=null where trankeygen_id in (select trankeygen_id from private.trankeygen where fund_id=:fund_id) ",['fund_id' => $this->fund->fund_id]);
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents, $version);


    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_account) {
      // Orca should be one we have saved.
      $saved_account = (object)$saved_account;
      $this->assertArrayHasKey($saved_account->orca_id, $map);
      $account = $map[$saved_account->orca_id];
      if (!empty($account->pid)) {
        $this->assertSame($account->pid, $saved_account->orca_pid);
      }
      else {
        $this->assertEmpty($saved_account->orca_pid);
      }
      $this->assertSame($account->acctnum, $saved_account->acctnum);
      $this->assertSame($account->contid, $saved_account->orca_contid);
      $this->assertSame($account->code, $saved_account->code);
      $this->assertSame($account->description, $saved_account->description);
      $this->assertSame($account->style, $saved_account->style);
      $this->assertEquals($account->total, $saved_account->total);
      $this->assertSame($account->memo, $saved_account->memo);
      $this->assertSame($account->date_, $saved_account->date_);
      $this->assertSame($account->derby_updated, $saved_account->derby_updated);
    }
  }

  public function testSyncContacts() {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/contacts/sync-contacts.json");
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);

    $data = json_decode($contents);
    // Create a map based on ORCA id so we can access them easily for assertions
    $map = [];
    foreach ($data->contacts as $contact) {
      $map[$contact->orca_id] = $contact;
    }

    $sql = "SELECT a.*, t.orca_id, t.derby_updated, ic.firstname, ic.lastname, ic.middleinitial, ic.prefix, ic.suffix,
          cc.contact1_id, cc.contact2_id, tc1.trankeygen_id as c1_tran, tc2.trankeygen_id as c2_tran
        from private.contacts a join private.trankeygen t on t.trankeygen_id = a.trankeygen_id 
          left join private.individualcontacts ic on ic.trankeygen_id = t.trankeygen_id
          left join private.couplecontacts cc on cc.trankeygen_id = t.trankeygen_id
          left join private.trankeygen tc1 on cc.contact1_id = tc1.trankeygen_id
          left join private.trankeygen tc2 ON cc.contact2_id = tc2.trankeygen_id
        where t.fund_id = :fund_id order by orca_id";

    // Retrieve saved values in the database
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_contact) {
      // Orca should be one we have saved.
      $saved_contact = (object)$saved_contact;
      $this->assertArrayHasKey($saved_contact->orca_id, $map);
      $contact = $map[$saved_contact->orca_id];

      $this->assertSame($contact->type, $saved_contact->type);
      $this->assertSame($contact->name, $saved_contact->name);
      $this->assertSame($contact->memo, $saved_contact->memo);

      if($contact->type == 'IND' || $contact->type == 'BUS') {
        $this->assertSame($contact->street, $saved_contact->street);
        $this->assertSame($contact->city, $saved_contact->city);
        $this->assertSame($contact->state, $saved_contact->state);
        $this->assertSame($contact->zip, $saved_contact->zip);
        $this->assertNull($saved_contact->c1_tran);
        $this->assertNull($saved_contact->c2_tran);
      }

      if($contact->type == 'IND') {
        $this->assertSame($contact->occupation, $saved_contact->occupation);
        $this->assertSame($contact->employername, $saved_contact->employername);
        $this->assertSame($contact->employerstreet, $saved_contact->employerstreet);
        $this->assertSame($contact->employercity, $saved_contact->employercity);
        $this->assertSame($contact->employerzip, $saved_contact->employerzip);
        $this->assertSame($contact->firstname, $saved_contact->firstname);
        $this->assertSame($contact->lastname, $saved_contact->lastname);
        $this->assertSame($contact->middleinitial, $saved_contact->middleinitial);
        $this->assertSame($contact->prefix, $saved_contact->prefix);
        if (!empty($contact->pagg) && !empty($contact->gagg)) {
          $this->assertEquals($contact->pagg, (float) $saved_contact->pagg);
          $this->assertEquals($contact->gagg, (float) $saved_contact->gagg);
        }
      }

      if ($contact->type == 'GRP') {
           $saved_members = $this->dm->db->executeQuery(
          "SELECT * from private.registeredgroupcontacts rgc
                 where gconid = :contact_id
               ",
          ['contact_id' => $saved_contact->trankeygen_id]
        )->fetchAllAssociative();
        $this->assertEquals(count($contact->members), count($saved_members));
      }

      if ($contact->type == 'CPL') {
        $this->assertNotEmpty($saved_contact->contact1_id);
        $this->assertNotEmpty($saved_contact->contact2_id);
        $this->assertNotEmpty($saved_contact->c1_tran);
        $this->assertNotEmpty($saved_contact->c2_tran);
      }


      $this->assertSame($contact->derby_updated, $saved_contact->derby_updated);
    }

    // Second save for updates
    $this->dm->db->executeStatement("update private.contacts set  type='xxx', name=null, street=null, city=null, 
                         state=null, zip=null, occupation=null, employername=null, employercity=null, employerstate=null, employerzip=null, employerstreet=null, email=null
where trankeygen_id in (select trankeygen_id from private.trankeygen where fund_id=:fund_id) ",['fund_id' => $this->fund->fund_id]);
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);


    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same numbe+
    r of saved items");
    foreach ($results as $saved_contact) {
      // Orca should be one we have saved.
      $saved_contact = (object)$saved_contact;
      $this->assertArrayHasKey($saved_contact->orca_id, $map);
      $contact = $map[$saved_contact->orca_id];
      $this->assertSame($contact->type, $saved_contact->type);
      $this->assertSame($contact->name, $saved_contact->name);
      $this->assertSame($contact->memo, $saved_contact->memo);

      if($contact->type == 'IND' || $contact->type == 'BUS') {
        $this->assertSame($contact->street, $saved_contact->street);
        $this->assertSame($contact->city, $saved_contact->city);
        $this->assertSame($contact->state, $saved_contact->state);
        $this->assertSame($contact->zip, $saved_contact->zip);
        $this->assertSame($contact->phone, $saved_contact->phone);
        $this->assertSame($contact->email, $saved_contact->email);
      }
      if($contact->type == 'IND') {
        $this->assertSame($contact->occupation, $saved_contact->occupation);
        $this->assertSame($contact->employername, $saved_contact->employername);
        $this->assertSame($contact->employerstreet, $saved_contact->employerstreet);
        $this->assertSame($contact->employercity, $saved_contact->employercity);
        $this->assertSame($contact->employerzip, $saved_contact->employerzip);
        $this->assertSame($contact->firstname, $saved_contact->firstname);
        $this->assertSame($contact->lastname, $saved_contact->lastname);
        $this->assertSame($contact->middleinitial, $saved_contact->middleinitial);
        $this->assertSame($contact->prefix, $saved_contact->prefix);
      }

      $this->assertSame($contact->derby_updated, $saved_contact->derby_updated);
    }
  }

  public function testSyncExpenditureEvents() {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/expenditures/sync-expenditureevents.json");
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);

    $data = json_decode($contents);
    // Create a map based on ORCA id so we can access them easily for assertions
    $map = [];
    foreach ($data->expenditureEvents as $expenditure) {
      $map[$expenditure->orca_id] = $expenditure;
    }

    $sql = "SELECT a.*,  t.orca_id, t.derby_updated, 
         case when a.contid > 0 then tc.orca_id else a.contid end  as orca_contid, coalesce(tp.orca_id,0) as orca_pid, coalesce(tb.orca_id, 0) as orca_bnkid 
      from private.expenditureevents a 
        join private.trankeygen t on t.trankeygen_id = a.trankeygen_id 
        left join private.trankeygen tp ON t.pid= tp.trankeygen_id
        left join private.trankeygen tc ON a.contid = tc.trankeygen_id
        left join private.trankeygen tb ON a.bnkid = tb.trankeygen_id
      where t.fund_id = :fund_id order by orca_id";

    // Retrieve saved values in the database
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_expenditure) {
      // Orca should be one we have saved.
      $saved_expenditure = (object)$saved_expenditure;
      $this->assertArrayHasKey($saved_expenditure->orca_id, $map);
      $expenditure = $map[$saved_expenditure->orca_id];
      $this->assertSame($expenditure->pid, $saved_expenditure->orca_pid);
      $this->assertSame($expenditure->contid, $saved_expenditure->orca_contid);
      $this->assertSame($expenditure->bnkid, $saved_expenditure->orca_bnkid);
      $this->assertEquals($expenditure->amount, $saved_expenditure->amount);
      $this->assertSame($expenditure->date_, $saved_expenditure->date_);
      $this->assertSame($expenditure->itemized, $saved_expenditure->itemized);
      $this->assertSame($expenditure->memo, $saved_expenditure->memo);
      $this->assertSame($expenditure->derby_updated, $saved_expenditure->derby_updated);
    }

    // Second save for updates
    $this->dm->db->executeStatement("update private.expenditureevents set contid=-1, bnkid=0, amount=0.00, date_=null, checkno=null, itemized=0, memo=null where trankeygen_id in (select trankeygen_id from private.trankeygen where fund_id=:fund_id) ",['fund_id' => $this->fund->fund_id]);
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);


    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_expenditure) {
      // Orca should be one we have saved.
      $saved_expenditure = (object)$saved_expenditure;
      $this->assertArrayHasKey($saved_expenditure->orca_id, $map);
      $expenditure = $map[$saved_expenditure->orca_id];
      $this->assertSame($expenditure->pid, $saved_expenditure->orca_pid);
      $this->assertSame($expenditure->contid, $saved_expenditure->orca_contid);
      $this->assertSame($expenditure->bnkid, $saved_expenditure->orca_bnkid);
      $this->assertEquals($expenditure->amount, $saved_expenditure->amount);
      $this->assertSame($expenditure->date_, $saved_expenditure->date_);
      $this->assertSame($expenditure->itemized, $saved_expenditure->itemized);
      $this->assertSame($expenditure->memo, $saved_expenditure->memo);
      $this->assertSame($expenditure->derby_updated, $saved_expenditure->derby_updated);
      $this->assertSame($expenditure->derby_updated, $saved_expenditure->derby_updated);
    }
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function testSyncReceipts() {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/receipts/sync-receipts.json");

    // Create a receipt in advance to manipulate.
    $data = json_decode($contents);
    $rds_id = $this->dm->db->executeQuery(
      "INSERT INTO private.trankeygen(fund_id, rds_updated ) VALUES (:fund_id, now()) RETURNING trankeygen_id",[ 'fund_id' => $this->fund->fund_id] )
      ->fetchOne();
    $this->dm->db->executeStatement("INSERT INTO private.receipts( trankeygen_id, type) VALUES (:rds_id, 0)", ['rds_id' => $rds_id]);

    $data->receipts[0]->rds_id = $rds_id;
    $this->orcaProcessor->pushChanges($this->fund->fund_id, json_encode($data));

    // Create a map based on ORCA id so we can access them easily for assertions
    $map = [];
    foreach ($data->receipts as $receipt) {
      $map[$receipt->orca_id] = $receipt;
    }

    $sql = "SELECT r.*, 
         t.orca_id, 
         t.derby_updated,
         case when r.contid > 0 then tc.orca_id else -1 end as orca_contid, 
         CASE WHEN t.pid > 0 THEN tp.orca_id else 0 END as orca_pid,
         tcr.orca_id  as orca_cid,
         td.orca_id as orca_did, 
         CASE WHEN R.gid > 0 THEN tg.orca_id else -1 END as orca_gid
       from private.receipts r 
       join private.trankeygen t on t.trankeygen_id = r.trankeygen_id 
         left join private.trankeygen tc on r.contid = tc.trankeygen_id
         left join private.trankeygen tp on t.pid = tp.trankeygen_id
         left join private.trankeygen tcr on r.cid = tcr.trankeygen_id
         left join private.trankeygen td on r.did = td.trankeygen_id
         left join private.trankeygen tg on r.gid = tg.trankeygen_id
where t.fund_id = :fund_id order by orca_id";

    // Retrieve saved values in the database
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");


    foreach ($results as $saved_receipt) {
      // Orca should be one we have saved.
      $saved_receipt = (object)$saved_receipt;
      $this->assertArrayHasKey($saved_receipt->orca_id, $map);
      $receipt = $map[$saved_receipt->orca_id];
      $this->assertSame($receipt->type, $saved_receipt->type);
      $this->assertSame($receipt->aggtype, $saved_receipt->aggtype);
      $this->assertSame($receipt->nettype, $saved_receipt->nettype);
      $this->assertEquals($receipt->pid, $saved_receipt->orca_pid);
      $this->assertSame($receipt->gid, $saved_receipt->orca_gid);
      $this->assertSame($receipt->cid, $saved_receipt->orca_cid);
      $this->assertSame($receipt->did, $saved_receipt->orca_did);
      $this->assertSame($receipt->contid, $saved_receipt->orca_contid);
      $this->assertEquals($receipt->amount, $saved_receipt->amount);
      $this->assertSame($receipt->date_, $saved_receipt->date_);
      $this->assertSame($receipt->elekey, $saved_receipt->elekey);
      $this->assertSame($receipt->itemized, $saved_receipt->itemized);
      $this->assertSame($receipt->checkno, $saved_receipt->checkno);
      $this->assertSame($receipt->description, $saved_receipt->description);
      $this->assertSame($receipt->memo, $saved_receipt->memo);
      $this->assertSame($receipt->carryforward, $saved_receipt->carryforward);
      $this->assertSame($receipt->deptkey, $saved_receipt->deptkey);
      $this->assertSame($receipt->description, $saved_receipt->user_description);
    }


    //Second save for updates
    $this->dm->db->executeStatement("
    update private.receipts 
    set type=6, 
        aggtype=1, 
        nettype=2, 
        pid=null, 
        gid=null,
        cid=null,
        did=null,
        contid=null,
        amount=52,
        date_='2020-05-05',
        elekey=0,
        itemized=0,
        checkno='5442',
        description='Focused on this',
        user_description='User updated this value',
        memo='Even thirteen cents is too much for you',
        carryforward=0,
        deptkey=1588662000103
      where trankeygen_id in (select trankeygen_id from private.trankeygen where fund_id = :fund_id) ",['fund_id' => $this->fund->fund_id]);
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);

    $results=$this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_receipt) {
      // Orca should be one we have saved.
      $saved_receipt = (object)$saved_receipt;
      $this->assertArrayHasKey($saved_receipt->orca_id, $map);
      $receipt = $map[$saved_receipt->orca_id];
      $this->assertSame($receipt->type, $saved_receipt->type);
      $this->assertSame($receipt->aggtype, $saved_receipt->aggtype);
      $this->assertSame($receipt->nettype, $saved_receipt->nettype);
      $this->assertSame($receipt->pid, $saved_receipt->orca_pid);
      $this->assertSame($receipt->gid, $saved_receipt->orca_gid);
      $this->assertSame($receipt->cid, $saved_receipt->orca_cid);
      $this->assertSame($receipt->did, $saved_receipt->orca_did);
      $this->assertSame($receipt->contid, $saved_receipt->orca_contid);
      $this->assertEquals($receipt->amount, $saved_receipt->amount);
      $this->assertSame($receipt->date_, $saved_receipt->date_);
      $this->assertSame($receipt->elekey, $saved_receipt->elekey);
      $this->assertSame($receipt->itemized, $saved_receipt->itemized);
      $this->assertSame($receipt->checkno, $saved_receipt->checkno);
      $this->assertSame($receipt->description, $saved_receipt->description);
      $this->assertSame($receipt->memo, $saved_receipt->memo);
      $this->assertSame($receipt->carryforward, $saved_receipt->carryforward);
      $this->assertSame($receipt->deptkey, $saved_receipt->deptkey);
      //make sure we don't overwrite existing user description on orca sync
      $this->assertNotSame($receipt->description, $saved_receipt->user_description);
    }
  }

  public function testSyncAuctionItems() {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/auctions/sync-auction-items.json");
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);
    $data = json_decode($contents);
    // Create a map based on ORCA id so we can access them easily for assertions
    $map = [];
    foreach ($data->auctionItems as $auctionItem) {
      $map[$auctionItem->orca_id] = $auctionItem;
    }

    $sql = "SELECT ai.*, t.orca_id, t.derby_updated, tp.orca_id as orca_pid from private.auctionitems ai
      join private.trankeygen t on t.trankeygen_id = ai.trankeygen_id 
      left join private.trankeygen tp on tp.trankeygen_id=t.pid
      where t.fund_id = :fund_id order by orca_id";

    // Retrieve saved values in the database
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");

    foreach ($results as $saved_auction_item) {
      // Orca should be one we have saved.
      $saved_auction_item = (object)$saved_auction_item;
      $this->assertArrayHasKey($saved_auction_item->orca_id, $map);
      $receipt = $map[$saved_auction_item->orca_id];
      $this->assertSame($receipt->pid, $saved_auction_item->orca_pid);
      $this->assertSame($receipt->itemnumber, $saved_auction_item->itemnumber);
      $this->assertSame($receipt->itemdescription, $saved_auction_item->itemdescription);
      $this->assertEquals($receipt->marketval, $saved_auction_item->marketval);
      $this->assertSame($receipt->memo, $saved_auction_item->memo);
    }
    //Second save for updates
    $this->dm->db->executeStatement("
    update private.auctionitems 
    set 
        pid=-13, 
        itemnumber='fuzziness',
        itemdescription='Tribble do belong in the workplace',
        marketval=68.01,
        memo='Hershey kisses on toast'
    where trankeygen_id in (select trankeygen_id from private.trankeygen where fund_id = :fund_id) ",['fund_id' => $this->fund->fund_id]);
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);

    $results=$this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_auction_item) {
      // Orca should be one we have saved.
      $saved_auction_item = (object)$saved_auction_item;
      $this->assertArrayHasKey($saved_auction_item->orca_id, $map);
      $receipt = $map[$saved_auction_item->orca_id];
      $this->assertSame($receipt->pid, $saved_auction_item->orca_pid);
      $this->assertSame($receipt->itemnumber, $saved_auction_item->itemnumber);
      $this->assertSame($receipt->itemdescription, $saved_auction_item->itemdescription);
      $this->assertEquals($receipt->marketval, $saved_auction_item->marketval);
      $this->assertSame($receipt->memo, $saved_auction_item->memo);
    }
  }



  public function testSyncLoans() {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/loans/sync-loans.json");
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);

    $data = json_decode($contents);
    // Create a map based on ORCA id so we can access them easily for assertions
    $map = [];
    foreach ($data->loans as $loan) {
      $map[$loan->orca_id] = $loan;
    }

    $sql = "SELECT l.*, 
       t.orca_id, 
       t.derby_updated,
       case when l.contid > 0 then tc.orca_id else l.contid end as orca_contid,
       tcr.orca_id as orca_cid, 
       td.orca_id as orca_did
       
       
    from private.loans l
    join private.trankeygen t on t.trankeygen_id = l.trankeygen_id 
    left join private.trankeygen tc on tc.trankeygen_id = l.contid
    left join private.trankeygen tcr on tcr.trankeygen_id = l.cid
    left join private.trankeygen td on td.trankeygen_id = l.did
    where t.fund_id = :fund_id order by orca_id";

    // Retrieve saved values in the database
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");

    foreach ($results as $saved_loan) {
      // Orca should be one we have saved.
      $saved_loan = (object)$saved_loan;
      $this->assertArrayHasKey($saved_loan->orca_id, $map);
      $loan = $map[$saved_loan->orca_id];
      $this->assertSame($loan->type, $saved_loan->type);
      $this->assertSame($loan->cid, $saved_loan->orca_cid);
      $this->assertSame($loan->did, $saved_loan->orca_did);
      $this->assertSame($loan->contid, $saved_loan->orca_contid);
      $this->assertEquals($loan->amount, $saved_loan->amount);
      $this->assertSame($loan->date_, $saved_loan->date_);
      $this->assertSame($loan->elekey, $saved_loan->elekey);
      $this->assertEquals($loan->interestrate, $saved_loan->interestrate);
      $this->assertSame($loan->duedate, $saved_loan->duedate);
      $this->assertSame($loan->checkno, $saved_loan->checkno);
      $this->assertSame($loan->repaymentschedule, $saved_loan->repaymentschedule);
      $this->assertSame($loan->description, $saved_loan->description);
      $this->assertSame($loan->memo, $saved_loan->memo);
      $this->assertSame($loan->carryforward, $saved_loan->carryforward);
    }

    //Second save for updates
    $this->dm->db->executeStatement("
    update private.loans
    set type=6, 
        cid=-9,
        did=-8,
        contid=-7,
        amount=99.0,
        date_='2001-02-02',
        elekey=-11,
        interestrate=102,
        duedate='1999-09-09',
        checkno='999',
        repaymentschedule='nary nary',
        description='quite contrary',
        memo='dave is not here',
        carryforward=-33
   
          where trankeygen_id in (select trankeygen_id from private.trankeygen where fund_id = :fund_id) ", ['fund_id' => $this->fund->fund_id]);
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);

    $results=$this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_receipt) {
      // Orca should be one we have saved.
      $saved_receipt = (object)$saved_receipt;
      $this->assertArrayHasKey($saved_receipt->orca_id, $map);
      $receipt = $map[$saved_receipt->orca_id];
      $this->assertSame($receipt->type, $saved_receipt->type);
      $this->assertSame($loan->cid, $saved_loan->orca_cid);
      $this->assertSame($loan->did, $saved_loan->orca_did);
      $this->assertSame($loan->contid, $saved_loan->orca_contid);
      $this->assertEquals($receipt->amount, $saved_receipt->amount);
      $this->assertSame($receipt->date_, $saved_receipt->date_);
      $this->assertSame($receipt->elekey, $saved_receipt->elekey);
      $this->assertEquals($receipt->interestrate, $saved_receipt->interestrate);
      $this->assertSame($receipt->duedate, $saved_receipt->duedate);
      $this->assertSame($receipt->checkno, $saved_receipt->checkno);
      $this->assertSame($receipt->repaymentschedule, $saved_receipt->repaymentschedule);
      $this->assertSame($receipt->description, $saved_receipt->description);
      $this->assertSame($receipt->memo, $saved_receipt->memo);
      $this->assertSame($receipt->carryforward, $saved_receipt->carryforward);
    }
  }

  public function testSyncDepositEvents() {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/deposits/sync-deposit-events.json");

    $data = json_decode($contents);

    $last_report = null;
    foreach ($data->depositEvents as $de) {
      if ($de->reports) foreach ($de->reports as $report_id) {
        // insert a report so that we can check to make sure the external id gets synced.
        $this->dm->db->executeStatement(
          "INSERT INTO report(report_id, report_type,submitted_at, fund_id, period_start, period_end, superseded_id)
                values (:report_id, 'C3', now(), :fund_id, '2020-01-01', '2020-01-01', :last_report)",
          ['fund_id' => $this->fund->fund_id, 'report_id' => $report_id, 'last_report' => $last_report]
        );
        $last_report = $report_id;
      }
    }


    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);


    // Create a map based on ORCA id so we can access them easily for assertions
    $map = [];
    foreach ($data->depositEvents as $depositEvent) {
      $map[$depositEvent->orca_id] = $depositEvent;
    }

    $sql = "SELECT * from private.vdepositevents de where de.fund_id = :fund_id order by de.orca_id";

    $itemsSql = "SELECT orca_id FROM private.deposititems di JOIN private.trankeygen t ON t.trankeygen_id=di.rcptid WHERE di.deptid=:trankeygen_id ";

    // Retrieve saved values in the database
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id])->fetchAllAssociative();
    $this->assertSame(count($map), count($results), "Same number of saved items");

    foreach ($results as $saved_deposit_event) {
      // Orca should be one we have saved.
      $saved_deposit_event = (object) $saved_deposit_event;
      $this->assertArrayHasKey($saved_deposit_event->orca_id, $map);
      $depositEvent = $map[$saved_deposit_event->orca_id];
      $this->assertSame($depositEvent->account_id, $saved_deposit_event->account_orca_id);
      $this->assertEquals($depositEvent->amount, $saved_deposit_event->amount);
      $this->assertSame($depositEvent->date_, $saved_deposit_event->date_);
      $this->assertSame($depositEvent->deptkey, $saved_deposit_event->deptkey);
      $this->assertSame($depositEvent->memo, $saved_deposit_event->memo);

      $items = $this->dm->db->executeQuery($itemsSql, ['trankeygen_id' => $saved_deposit_event->trankeygen_id]);
      $this->assertEquals(count($depositEvent->depositItems), $items->rowCount());
      foreach ($items as $item) {
        $this->assertTrue(array_search($item['orca_id'], $depositEvent->depositItems) !== FALSE);
      }
    }
    //Second save for updates
    $this->dm->db->executeStatement("
    update private.depositevents
    set account_id=2, 
        amount=40.0, 
        date_='2020-04-05',
        deptkey=10,
        memo='This is some icky fire water'
      where trankeygen_id in (select trankeygen_id from private.trankeygen where fund_id = :fund_id) ",
    ['fund_id' => $this->fund->fund_id]);
        $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);
    $results=$this->dm->db->executeQuery("
      SELECT de.* from private.vdepositevents de  where fund_id = :fund_id order by de.orca_id", ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_deposit_event) {
      // Orca should be one we have saved.
      $saved_deposit_event = (object)$saved_deposit_event;
      $this->assertArrayHasKey($saved_deposit_event->orca_id, $map);
      $depositEvent = $map[$saved_deposit_event->orca_id];
      $this->assertSame($depositEvent->account_id, $saved_deposit_event->account_orca_id);
      $this->assertEquals($depositEvent->amount, $saved_deposit_event->amount);
      $this->assertSame($depositEvent->date_, $saved_deposit_event->date_);
      $this->assertSame($depositEvent->memo, $saved_deposit_event->memo);
      $this->assertSame($depositEvent->deptkey, $saved_deposit_event->deptkey);

      $items = $this->dm->db->executeQuery($itemsSql, ['trankeygen_id' => $saved_deposit_event->trankeygen_id]);
      $this->assertEquals(count($depositEvent->depositItems), $items->rowCount());
      foreach ($items as $item) {
        $this->assertTrue(array_search($item['orca_id'], $depositEvent->depositItems) !== FALSE);
      }

      $reports = $this->dm->db->executeQuery(
        "select * from report where fund_id = :fund_id and external_id = :deposit_id",
        ['fund_id' => $this->fund->fund_id, 'deposit_id' => (string)$saved_deposit_event->trankeygen_id]
      )->fetchFirstColumn();
      $this->assertEquals(count($depositEvent->reports), count($reports));
    }
  }

  public function testSyncDebtObligations1490() {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/debts/sync-debt-1490.json");
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents, 1.490);

    $data = json_decode($contents);


    $sql = "SELECT * from private.vexpenditureevents e where fund_id = :fund_id order by e.trankeygen_id";
    $results = $this->dm->db->fetchAllAssociative($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertNotEmpty($results);
    $es = (object)$results[0];

    $ee = $data->expenditureEvents[0];
    $this->assertSame($ee->date_, $es->date_);
    $this->assertSame($ee->memo, $es->memo);
    $this->assertSame($ee->itemized, $es->itemized);

    $sql = "SELECT t.fund_id, r.pid, t.orca_id, r.contid FROM private.receipts r 
        JOIN private.trankeygen t ON t.trankeygen_id=r.trankeygen_id                              
      WHERE t.fund_id = :fund_id AND r.type in (20,44,29) order by r.trankeygen_id";
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id])->fetchAllAssociative();
    $this->assertEquals(3, count($results));
    foreach ($results as $record) {
      $this->assertSame($es->trankeygen_id, $record['pid']);
      $this->assertSame($es->contid, $record['contid']);
    }
  }

  public function testSyncDebtObligations() {
    $this->createFund();
    $contents = file_get_contents($this->data_dir . "/debts/sync-debt-obligations.json");
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);

    $data = json_decode($contents);
    // Create a map based on ORCA id so we can access them easily for assertions
    $map = [];
    foreach ($data->debtObligations as $debtObligation) {
      $map[$debtObligation->contid] = $debtObligation;
    }

    $sql = "SELECT t.fund_id, t.orca_id, r.contid, d.contid as d_contid FROM private.receipts r JOIN private.trankeygen t ON t.trankeygen_id=r.trankeygen_id
      JOIN private.debtobligation d ON r.pid = d.debtobligation_id 
      WHERE t.fund_id = :fund_id AND r.type in (20,44,29)";
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id])->fetchAllAssociative();
    $this->assertEquals(3, count($results));

    $sql = "SELECT d.*, tp.orca_id as orca_pid, tc.orca_id as orca_contid FROM private.debtobligation d 
       JOIN private.trankeygen tp ON tp.trankeygen_id=d.pid 
       JOIN private.trankeygen tc on tc.trankeygen_id = d.contid 
       where tp.fund_id = :fund_id order by d.pid";

    // Retrieve saved values in the database
    $results = $this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");

    foreach ($results as $saved_debt_obligation) {
      // Orca should be one we have saved.
      $saved_debt_obligation = (object) $saved_debt_obligation;
      $this->assertArrayHasKey($saved_debt_obligation->orca_contid, $map);
      $debtObligation = $map[$saved_debt_obligation->orca_contid];
      $this->assertSame($debtObligation->pid, $saved_debt_obligation->orca_pid);
      $this->assertSame($debtObligation->contid, $saved_debt_obligation->orca_contid);
      $this->assertEquals($debtObligation->amount, $saved_debt_obligation->amount);
      $this->assertSame($debtObligation->date_, $saved_debt_obligation->date_);
      $this->assertSame($debtObligation->descr, $saved_debt_obligation->descr);
      $this->assertSame($debtObligation->memo, $saved_debt_obligation->memo);
    }

    //Second save for updates
    $contents = file_get_contents($this->data_dir . "/debts/sync-debt-obligations-altered.json");
    $this->orcaProcessor->pushChanges($this->fund->fund_id, $contents);
    $data = json_decode($contents);
    $map = [];
    foreach ($data->debtObligations as $debtObligation) {
      $map[$debtObligation->contid] = $debtObligation;
    }

    $results=$this->dm->db->executeQuery($sql, ['fund_id' => $this->fund->fund_id]);
    $this->assertSame(count($map), $results->rowCount(), "Same number of saved items");
    foreach ($results as $saved_debt_obligation) {
      // Orca should be one we have saved.
      $saved_debt_obligation = (object)$saved_debt_obligation;
      $this->assertArrayHasKey($saved_debt_obligation->orca_contid, $map);
      $debtObligation = $map[$saved_debt_obligation->orca_contid];
      $this->assertSame($debtObligation->pid, $saved_debt_obligation->orca_pid);
      $this->assertSame($debtObligation->contid, $saved_debt_obligation->orca_contid);
      $this->assertEquals($debtObligation->amount, $saved_debt_obligation->amount);
      $this->assertSame($debtObligation->date_, $saved_debt_obligation->date_);
      $this->assertSame($debtObligation->descr, $saved_debt_obligation->descr);
      $this->assertSame($debtObligation->memo, $saved_debt_obligation->memo);
    }
  }

  public function testSaveProperties() {
    $this->createFund();
    $committee_id = $this->dm->db->executeQuery("insert into committee(name, election_code, committee_type, continuing) values ('test committee', :election_code, 'CA', false) returning committee_id", ['name' => 'test committee', 'election_code' => '2022'])->fetchOne();
    $this->dm->db->executeQuery('update fund set committee_id = :committee_id where fund_id = :fund_id', ['committee_id' => $committee_id, 'fund_id' => $this->fund->fund_id]);
    $contents = file_get_contents($this->data_dir . "/properties/sync-properties.json");
    $this->orcaProcessor->saveValidations($this->fund->fund_id, $contents);

    $data = json_decode($contents);
    $map = [];
    foreach ($data->properties as $property) {
      $map[$property->name] = $property->value;
    }

    $saved_properties = $this->dm->db->executeQuery("select * from private.properties where fund_id=:fund_id", ['fund_id' => $this->fund->fund_id]);
    $this->assertEquals(count($data->properties), $saved_properties->rowCount());
    foreach ($saved_properties as $property) {
      $property = (object) $property;
      $this->assertArrayHasKey($property->name, $map);
      $this->assertSame($map[$property->name], $property->value);
    }

    $contents = file_get_contents($this->data_dir . '/properties/sync-properties-altered.json');
    $this->orcaProcessor->saveValidations($this->fund->fund_id, $contents);

    $data = json_decode($contents);
    $map = [];
    foreach ($data->properties as $property) {
      $map[$property->name] = $property->value;
    }
    $data = json_decode($contents);
    $saved_properties = $this->dm->db->executeQuery("select * from private.properties where fund_id=:fund_id", ['fund_id' => $this->fund->fund_id]);
    $this->assertEquals(count($data->properties), $saved_properties->rowCount());
    foreach ($saved_properties as $property) {
      $property = (object) $property;
      $this->assertArrayHasKey($property->name, $map);
      $this->assertSame($map[$property->name], $property->value);
    }

  }
    public function testSaveLimits() {
      $this->createFund();
      $committee_id = $this->dm->db->executeQuery("insert into committee(name, election_code, committee_type, continuing) values ('test committee', :election_code, 'CA', false) returning committee_id", ['name' => 'test committee', 'election_code' => $this->fund->election_code])->fetchOne();
      $this->dm->db->executeQuery('update fund set committee_id = :committee_id where fund_id = :fund_id', ['committee_id' => $committee_id, 'fund_id' => $this->fund->fund_id]);
      $this->dm->db->executeQuery("insert into private.properties (fund_id, name, value) values (:fund_id, 'CAMPAIGNINFO:BALLOTTYPE', '0')", ['fund_id' => $this->fund->fund_id]);
      $this->dm->db->executeQuery("insert into private.properties (fund_id, name, value) values (:fund_id, 'CAMPAIGNINFO:STARTDATE', '2021-01-01')", ['fund_id' => $this->fund->fund_id]);

      $contents = file_get_contents($this->data_dir . "/limits/sync-limits.json");
      $this->orcaProcessor->saveValidations($this->fund->fund_id, $contents);

      $data = json_decode($contents);
      $map = [];
      foreach($data->limits as $limit) {
        $map[$this->fund->fund_id] = $limit;
      }
      $results = $this->dm->db->executeQuery("select * from private.limits where fund_id=:fund_id", ['fund_id' => $this->fund->fund_id]);
      $this->assertEquals(count($data->limits), $results->rowCount());
      foreach($results as $saved_limit) {
        $saved_limit = (object)$saved_limit;
        $this->assertArrayHasKey($this->fund->fund_id, $map);
        $limit = $map[$this->fund->fund_id];
        $this->assertSame($limit->type, $saved_limit->type);
        $this->assertSame($limit->name, $saved_limit->name);
        $this->assertEquals($limit->amount, $saved_limit->amount);
        $this->assertSame($limit->aggbyvoters, $saved_limit->aggbyvoters);
      }
      $contents = file_get_contents($this->data_dir . '/limits/sync-limits-altered.json');
      $this->orcaProcessor->saveValidations($this->fund->fund_id, $contents);
      $data = json_decode($contents);
      $map = [];
      foreach($data->limits as $limit) {
        $map[$limit->orca_id] = $limit;
      }
      $data = json_decode($contents);
      $results = $this->dm->db->executeQuery("select * from private.limits where fund_id=:fund_id", ['fund_id' => $this->fund->fund_id]);
      $this->assertEquals(count($data->limits), $results->rowCount());
      foreach($results as $saved_limit) {
        $saved_limit = (object)$saved_limit;
        $this->assertArrayHasKey($limit->orca_id, $map);
        $limit = $map[$limit->orca_id];
        $this->assertSame($limit->type, $saved_limit->type);
      }
    }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }
}
