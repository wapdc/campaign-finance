<?php


namespace Tests\WAPDC\CampaignFinance\ORCA;


use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\Yaml\Yaml;

class ORCASubmissionGeneratorTest extends ORCATestCase {
  /**
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->data_dir = dirname(dirname(__DIR__)) . '/data/orca_submission';
    $this->trankeygen_map = [];
    $this->debtobligation_map = [];
  }


  /**
   * Create the reporting obligations for test coverage.
   * @param array $reporting_obligations
   */
  protected function createReportingObligations(array $reporting_obligations) {
    foreach ($reporting_obligations as $ro) {
      $this->dm->db->executeStatement("INSERT INTO private.reporting_obligation(fund_id, startdate, enddate, duedate)
        VALUES (:fund_id, :startdate, :enddate, :duedate)",
      [
        'fund_id'  => $this->fund_id,
        'startdate' => $ro->startdate,
        'enddate' => $ro->enddate,
        'duedate' => $ro->duedate ?? null,
      ]);
      $ro->reporting_obligation_id = $this->dm->db->lastInsertId();
    }
  }

  protected function createProperties(array $properties) {
    $this->orcaProcessor->updateProperties($this->fund_id, $properties);
  }

  protected function createCoupleContacts(array $coupleContacts) {
    foreach ($coupleContacts as $cc) {
      $trankeygen_id = $this->ensureTranKeyGen($cc->id);
      $contact1_id = $this->ensureTranKeyGen($cc->contact1_id);
      $contact2_id = $this->ensureTranKeyGen($cc->contact2_id);

      $this->dm->db->executeStatement("INSERT 
            INTO private.couplecontacts (trankeygen_id, contact1_id, contact2_id) 
            VALUES (:id, :contact1_id, :contact2_id)",
        ['id' => $trankeygen_id, 'contact1_id' => $contact1_id, 'contact2_id' => $contact2_id]
      );
    }
  }

  public function c3SubmissionProvider()
  {
    return [
      ['c3_auctions'],
      ['c3_loans'],
      ['c3_contributions'],
      ['c3_couples_basic'],
      ['c3_simple'],
      ['c3_miscellaneous'],
      ['c3_small_personal_anonymous'],
    ];
  }

  /**
   * @TODO implement unit tests for C3 submission.
   * @param $scenario
   * @dataProvider c3SubmissionProvider
   * @covers \WAPDC\CampaignFinance\OrcaProcessor::generateC3Submission
   */
  public function testC3SubmissionGenerator($scenario) {
    //the following statements were necessary to make the tests pass after the new thresholds were put in place.
    $this->dm->db->executeStatement("update thresholds set amount = 25 where property = 'itemized_contributions'");
    $this->dm->db->executeStatement("update thresholds set amount = 100 where property = 'report_occupation'");

    $scenario_data = Yaml::parseFile($this->data_dir . "/{$scenario}_data.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Create a committee to atttach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;

    // Make fund that we can generate a submission from
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);

    // make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createCoupleContacts($scenario_data->couplecontacts ?? []);
    $this->createDepositEvents($scenario_data->depositevents);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createAuctionItems($scenario_data->auctionitems ?? []);
    $this->createLoans($scenario_data->loans ?? []);

    $deposit_id = $this->ensureTranKeyGen($scenario_data->depositevents[0]->id);

    // Call the function
    $report = $this->orcaProcessor->generateC3Preview($this->fund_id, $deposit_id);

    // Make sure the data looks like a submission that we expect to see.
    $submission = $report->user_data;
    $this->assertNotEmpty($submission);

    $expected_submission = Yaml::parseFile($this->data_dir . "/{$scenario}_results.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    // Fix the submission assertion data to that the ids we are looking for are the
    // trankeygen ids that were assigned when we set up the initial condition.
    $this->setTrankeygenIds($expected_submission->contacts ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->contributions ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->misc ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->loans ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->auctions ?? [], ['contact_key']);

    //$rows = $this->dm->db->executeQuery("select * from private.vreceipts where fund_id=:fund_id", ['fund_id' =>$this->fund_id])->fetchAllAssociative();

    // Finally make sure the submission contains all the fields indicated in the assertioin file.
    $this->_compareObject($expected_submission, $submission, $scenario);
  }

  public function c4SubmissionProvider()
  {
    return [
      ['c4_simple'],
      ['c4_corrections'],
      ['c4_debt_legacy'],
      ['c4_debt'],
      ['c4_loans'],
      ['c4_inkind_contributions'],
      ['c4_pledges'],
      ['c4_credit_card'],
    ];
  }

  /**
   * C4
   * @param $scenario
   * @throws ConnectionException
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   * @throws ORMException
   * @throws OptimisticLockException
   * @dataProvider c4SubmissionProvider
   * @covers \WAPDC\CampaignFinance\OrcaProcessor::generateC4Submission
   */
  public function testC4SubmissionGenerator($scenario) {
    //the following statements were necessary to make the tests pass after the new thresholds were put in place.
    $this->dm->db->executeStatement("update thresholds set amount = 50 where property = 'itemized_expenses'");
    $scenario_data = Yaml::parseFile($this->data_dir . "/{$scenario}_data.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Create a committee to attach the fund to
    $committee = $this->createCommittee($scenario_data);
    $election_code = $committee->start_year;

    // Make fund that we can generate a submission from
    $this->createFund($election_code, $committee->committee_id);

    // make sure we have all the right kinds of transactions in an ORCA fund
    $this->createContacts($scenario_data->contacts ?? []);
    $this->createCoupleContacts($scenario_data->couplecontacts ?? []);
    $this->createDepositEvents($scenario_data->depositevents ?? []);
    $this->createExpenditureEvents($scenario_data->expenditureevents ?? []);
    $this->createReportingObligations($scenario_data->reporting_obligation ?? []);
    // Debt obligations (below) MUST be created before receipts so we can map pids properly
    $this->createDebtObligations($scenario_data->debtobligations ?? []);
    $this->createReceipts($scenario_data->receipts ?? []);
    $this->createAccountEntries($scenario_data->accounts ?? []);
    $this->createAuctionItems($scenario_data->auctionitems ?? []);
    $this->createLoans($scenario_data->loans ?? []);

    // Create the reporting period
    $reporting_obligation_id = $scenario_data->reporting_obligation[0]->reporting_obligation_id;

    // Call the function
    $report = $this->orcaProcessor->generateC4Preview($this->fund_id, $reporting_obligation_id);

    // Make sure the data looks like a submission that we expect to see.
    $submission = $report->user_data;
    $this->assertNotEmpty($submission);

    $expected_submission = Yaml::parseFile($this->data_dir . "/{$scenario}_results.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Fix the submission assertion data to that the ids we are looking for are the
    // trankeygen ids that were assigned when we set up the initial condition.
    $this->setTrankeygenIds($expected_submission->contacts ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->expenses ?? [], ['contact_key', 'payee_key', 'creditor_key']);
    $this->setTrankeygenIds($expected_submission->loan ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->debt ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->contributions_inkind ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->pledge ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->contribution_corrections ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->expense_corrections ?? [], ['contact_key']);
    $this->setTrankeygenIds($expected_submission->expense_refunds ?? [], ['contact_key']);

    // Finally make sure the submission contains all the fields indicated in the assertion file.
    // $expected_submission is the results file, $submission comes from c4_build_report and contains report->user_data
    $this->_compareObject($expected_submission, $submission, $scenario);
  }
}
