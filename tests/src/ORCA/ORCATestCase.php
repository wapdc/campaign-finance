<?php


namespace Tests\WAPDC\CampaignFinance\ORCA;


use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;
use stdClass;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\OrcaProcessor;


abstract class ORCATestCase extends TestCase {
  /** @var CFDataManager */
  protected $dm;

  /** @var OrcaProcessor */
  protected $orcaProcessor;

  /** @var CommitteeManager */

  protected $committeeManager;

  protected $fund_id;

  protected $trankeygen_map = [];

  protected $debtobligation_map = [];

  /**
   * @var Committee
   */
  protected $committee;

  protected string $data_dir;

  /**
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->committeeManager = new CommitteeManager($this->dm);
    $this->orcaProcessor = new OrcaProcessor($this->dm);
    $this->data_dir = dirname(__DIR__, 2) . '/data/orca';
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

  /**
   * Create a committee for use during testing.
   * @param stdClass $committee_data
   *   Properties of the committee to set on creation.
   * @return Committee
   *
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws Exception
   */
  protected function createCommittee($submission)
  {
    $committee_id = $this->committeeManager->createCommitteeFromFile($submission);
    $this->committeeManager->saveCommitteeFromSubmission($submission);
    $this->committee = $this->dm->em->find(Committee::class, $committee_id);
    if ($submission->committee->registered_at ?? NULL){
      $this->committee->registered_at = new DateTime($submission->committee->registered_at);
    }
    $this->dm->em->flush($this->committee);
    return $this->committee;
  }

  protected function ensureTranKeyGen($id) {
    if (!empty($this->trankeygen_map[$id])) {
      return $this->trankeygen_map[$id];
    } else {
      [$trankeygen_id] = $this->dm->db->executeQuery("SELECT private.cf_ensure_trankeygen(:fund_id, CAST(NULL AS INTEGER), :orca_id)",
        ['fund_id' => $this->fund_id, 'orca_id' => $id]
      )
        ->fetchFirstColumn();
      $this->trankeygen_map[$id] = $trankeygen_id;
      return $trankeygen_id;
    }
  }

  /**
   * Replaces the fields indicated with the generated value
   * @param mixed $data
   *   The data that needs to be replaced.
   * @param $fields
   *   array of property names that need to be translated as trankeygen values.
   */
  protected function setTrankeygenIds($data, $fields) {
    if (is_array($data)) {
      foreach($data as $element) {
        $this->setTrankeygenIds($element, $fields);
      }
    }
    if (is_object($data)) {
      foreach ($data as $field => $value) {
        if (!empty($value)) {
          if (is_object($value) || is_array($value)){
            $this->setTrankeygenIds($value, $fields);
          } elseif (array_search($field, $fields) !== FALSE) {
            $data->$field = $this->trankeygen_map[$value] ?? null;
          }
        }
      }
    }
  }


  public function createContacts(array $contacts) {
    foreach($contacts as $c) {
      $trankeygen_id = $this->ensureTranKeyGen($c->id);
      $this->dm->db->executeStatement(
        "insert into private.contacts (
                              trankeygen_id, 
                              type, name, street, city, state, 
                              zip, email, occupation, 
                              employername, 
                              employercity, employerstate, 
                              employerzip, contact_key
                              ) 
                              values (:trankeygen_id, 
                              :type, :name, :street, :city, :state, 
                              :zip, :email, :occupation, 
                              :employername, 
                              :employercity, :employerstate, 
                              :employerzip, :contact_key)",
        [
          'trankeygen_id' => $trankeygen_id,
          'type' => $c->type,
          'name' => $c->name ?? null,
          'street' => $c->street ?? null,
          'city' => $c->city ?? null,
          'state' => $c->state ?? null,
          'zip' => $c->zip ??  null,
          'email' => $c->email ?? null,
          'occupation' => $c->occupation ?? null,
          'employername' => $c->employername ?? null,
          'employercity' => $c->employercity ?? null,
          'employerstate' => $c->employerstate ?? null,
          'employerzip' => $c->employerzip ?? null,
            'contact_key' => $c->contact_key ?? $trankeygen_id
        ]);
      if($c->type == 'IND') {
          $this->dm->db->executeStatement(
              "insert into private.individualcontacts (trankeygen_id, prefix, firstname, middleinitial, lastname, suffix)
                    values (
                            :trankeygen_id, 
                            :prefix,    
                            :firstname,
                            :middleinitial,
                            :lastname,
                            :suffix
                              )",
              [
                  'trankeygen_id' => $trankeygen_id,
                  'prefix' => $c->prefix ?? null,
                  'firstname' => $c->firstname ?? null,
                  'middleinitial' => $c->middleinitial ?? null,
                  'lastname' => $c->lastname ?? null,
                  'suffix' => $c->suffix ?? null
              ]);
      }
    }
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   */
  protected function createFund($election_code, $committee_id) {
    $this->dm->db->executeStatement("INSERT INTO fund(updated_at, election_code, committee_id) values (now(), :election_code, :committee_id)",
      ['election_code' => $election_code, 'committee_id' => $committee_id]);
    $this->fund_id = $this->dm->db->lastInsertId();
    return $this->fund_id;
  }

  /**
   * @param $object1
   * @param $object2
   * @param string $message
   */
  protected function _compareObject($object1, $object2, $message = '') {

    foreach (get_object_vars($object1 ?? new stdClass()) as $property => $value) {
      $this->assertIsObject($object2, "$message.$property");
      switch (TRUE) {

        case (is_object($value)):
          $this->_compareObject($value, $object2->$property, "$message.$property");
          break;
        case (is_array($value)):
          foreach ($value as $i => $element) {
            // Arrays of objects should be compared as objects.
            $this->assertArrayHasKey($i, $object2->$property ?? [], "$message.{$property}[$i]");
            if (is_object($element)) {
              $this->_compareObject($element, $object2->$property[$i], "$message.{$property}[$i]");
            }
            // Assume an array of scalars
            else {
              $this->assertEquals($element, $object2->$property[$i], "$message.{$property}[$i]");
            }
          }
          break;
        default:
          if (isset($value)) {
            $this->assertObjectHasAttribute($property, $object2, "$message.$property");
            $this->assertEquals($object1->$property, $object2->$property, "{$message}.{$property}");
          }
          else {
            $this->assertFalse(isset($object2->$property), "$message.$property");
          }
      }
    }
  }

  protected function createDepositEvents(array $deposit_events) {
    foreach($deposit_events as $de) {
      // Trankeygen id generated.
      $trankeygen_id = $this->ensureTranKeyGen($de->id);
      $account_id = $this->ensureTranKeyGen($de->account_id);
      $this->dm->db->executeStatement("INSERT INTO private.depositevents(trankeygen_id, account_id, amount, date_, deptkey, memo) 
         VALUES (:trankeygen_id, :account_id, :amount, :date_, :deptkey, :memo)",
        [
          'trankeygen_id' => $trankeygen_id,
          'account_id' => $account_id,
          'amount' => $de->amount,
          'date_' => $de->date_,
          'deptkey' => $de->deptkey,
          'memo' => $de->memo ?? NULL
        ]
      );
    }
  }

  protected function createExpenditureEvents(array $expenditure_events) {
    foreach ($expenditure_events as $ex) {
      $trankeygen_id = $this->ensureTranKeyGen($ex->id);
      $contid = $this->ensureTranKeyGen($ex->contid);
      $bnkid = $this->ensureTranKeyGen($ex->bnkid);
      $this->dm->db->executeStatement("INSERT INTO private.expenditureevents(trankeygen_id, contid, bnkid, amount, date_, checkno, itemized, memo, uses_surplus_funds)
       VALUES(:trankeygen_id, :contid, :bnkid, :amount, :date_, :checkno, :itemized, :memo, :uses_surplus_funds)",
        [
          'trankeygen_id' => $trankeygen_id,
          'contid' => $contid,
          'bnkid' => $bnkid,
          'amount' => $ex->amount,
          'date_' => $ex->date_,
          'checkno' => $ex->checkno ?? NULL,
          'itemized' => $ex->itemized ?? 0,
          'memo' => $ex->memo ?? NULL,
          'uses_surplus_funds' => $ex->uses_surplus_funds ?? 0
        ]
      );
    }
  }
  protected function createReceipts(array $receipts) {
    $debt_types_array = array(20,29,44);
    foreach($receipts as $re){
      // Trankeygen id generated
      $trankeygen_id = $this->ensureTranKeyGen($re->id);
      if(in_array($re->type, $debt_types_array)) {
        $pid = $this->debtobligation_map[$re->pid] ?? NULL;
      } else {
        $pid = $re->pid ?? -1 > 0 ? $this->ensureTranKeyGen($re->pid) : $re->pid ?? null;
      };
      $cid = $this->ensureTranKeyGen($re->cid);
      $did = $this->ensureTranKeyGen($re->did);
      $contid = $re->contid ?? -1 > 0 ? $this->ensureTranKeyGen($re->contid) : $re->contid ?? null;
      $data = !empty($re->data) ? json_encode($re->data) : null;

      $this->dm->db->executeStatement("INSERT INTO private.receipts(
                             trankeygen_id, type, aggtype, nettype, 
                             pid, gid, cid, did, contid, 
                             amount, date_, elekey, 
                             itemized, checkno, description, 
                             memo, carryforward, deptkey, 
                             user_description, data) 
        VALUES (:trankeygen_id, :type, :aggtype, :nettype,
                :pid, :gid, :cid, :did, :contid, 
                :amount, :date_, :elekey,
                :itemized, :checkno, :description,
                :memo, :carryforward, :deptkey,
                :user_description, :data)",
        [
          'trankeygen_id' => $trankeygen_id, 'type' => $re->type, 'aggtype' => $re->aggtype, 'nettype' => $re->nettype,
          'pid' => $pid, 'gid' => $re->gid ?? null, 'cid' => $cid, 'did' => $did, 'contid' => $contid,
          'amount' => $re->amount, 'date_' =>  $re->date_, 'elekey' => $re->elekey ?? -1,
          'itemized' => $re->itemized ?? 0, 'checkno' => $re->checkno ?? null, 'description' => $re->description ?? null,
          'memo' => $re->memo ?? null, 'carryforward' => $re->carryforward ?? 0, 'deptkey' => $re->deptkey ?? 0,
          'user_description' => $re->user_description ??  null, 'data' => $data
        ]);
    }
  }

  protected function createDebtObligations(array $debtobligations) {
    foreach ($debtobligations as $do) {
      $pid = $this->ensureTranKeyGen($do->pid);
      $contid = $this->ensureTranKeyGen($do->contid);
      $this->dm->db->executeStatement("INSERT INTO private.debtobligation (   
            pid,
            contid,                     
            amount, 
            date_,
            descr,
            memo ) VALUES (
            :pid,
            :contid,                     
            :amount, 
            :date_,
            :descr,
            :memo
            )",
        [
          "pid" => $pid,
          "contid" => $contid,
          "amount" => $do->amount,
          "date_" => $do->date_,
          "descr" => $do->descr,
          "memo" => $do->memo
        ]
      );
      $this->debtobligation_map[$do->id] = $this->dm->db->lastInsertId();
    }
  }


  protected function createCoupleContacts(array $coupleContacts) {
    foreach ($coupleContacts as $cc) {
      $trankeygen_id = $this->ensureTranKeyGen($cc->id);
      $contact1_id = $this->ensureTranKeyGen($cc->contact1_id);
      $contact2_id = $this->ensureTranKeyGen($cc->contact2_id);

      $this->dm->db->executeStatement("INSERT 
            INTO private.couplecontacts (trankeygen_id, contact1_id, contact2_id) 
            VALUES (:id, :contact1_id, :contact2_id)",
        ['id' => $trankeygen_id, 'contact1_id' => $contact1_id, 'contact2_id' => $contact2_id]
      );
    }
  }

    protected function createGroupContacts(array $groupContacts) {
        foreach ($groupContacts as $gc) {
            $trankeygen_id = $this->ensureTranKeyGen($gc->gconid);
            $contid = $this->ensureTranKeyGen($gc->contid);

            $this->dm->db->executeStatement("INSERT 
            INTO private.registeredgroupcontacts (gconid, contid)
            VALUES (:gconid, :contid)",
                ['gconid' => $trankeygen_id, 'contid' => $contid]
            );
        }
    }

    protected function createAccountEntries(array $accounts) {
        foreach($accounts as $ac){
            $trankeygen_id = $this->ensureTranKeyGen($ac->id);
            $pid = $this->ensureTranKeyGen($ac->pid ?? null);
            $contid = $this->ensureTranKeyGen($ac->contid);

            $this->dm->db->executeStatement("INSERT INTO private.accounts(
      trankeygen_id, 
      pid, 
      acctnum, 
      contid,
      code, 
      description,
      style,
      total,
      memo,
      date_) VALUES (
                     :trankeygen_id, 
                     :pid, 
                     :acctnum, 
                     :contid,
                     :code, 
                     :description,
                     :style,
                     :total,
                     :memo,
                     :date_
                     )", [
                'trankeygen_id'=>$trankeygen_id,
                'pid'=>$pid,
                'acctnum'=>$ac->acctnum,
                'contid'=>$contid,
                'code'=>$ac->code ?? null,
                'description'=>$ac->description ?? null,
                'style'=>$ac->style ?? 3,
                'total'=>$ac->total ?? 0,
                'memo'=>$ac->memo ?? null,
                'date_'=>$ac->date_ ?? null
            ]);
        }
    }

  protected function createLoans(array $loans)
  {
    foreach ($loans as $loan) {
      $trankeygen_id = $this->ensureTranKeyGen($loan->id);
      $contid = $this->ensureTranKeyGen($loan->contid);
      $this->dm->db->executeStatement("INSERT INTO private.loans (
                      trankeygen_id,
                      type,
                      cid,
                      did,
                      contid,                     
                      amount, 
                      date_,
                      elekey,
                      interestrate,
                      duedate,
                      checkno,
                      repaymentschedule,
                      description,                         
                      memo,
                      carryforward
          ) VALUES (
                      :trankeygen_id,
                      :type,
                      :cid,
                      :did,
                      :contid,                     
                      :amount, 
                      :date_,
                      :elekey,
                      :interestrate,
                      :duedate,
                      :checkno,
                      :repaymentschedule,
                      :description,                         
                      :memo,
                      :carryforward
            )",
        [
          "trankeygen_id" => $trankeygen_id,
          "type" => $loan->type,
          "cid" => $loan->cid,
          "did" => $loan->did,
          "contid" => $contid,
          "amount" => $loan->amount,
          "date_" => $loan->date_,
          "elekey" => $loan->elekey,
          "interestrate" => $loan->interestrate,
          "duedate" => $loan->duedate,
          "checkno" => $loan->checkno ?? null,
          "repaymentschedule" => $loan->repaymentschedule ?? "As funds become available",
          "description" => $loan->description,
          "memo" => $loan->memo ?? null,
          "carryforward" => $loan->carryforward ?? 0
        ]
      );
    }
  }

  protected function createAuctionItems(array $auctionsItems){
    foreach($auctionsItems as $ai){
      $trankeygen_id = $this->ensureTranKeyGen($ai->id);
      $pid = $this->ensureTranKeyGen($ai->pid);

      $this->dm->db->executeStatement("INSERT INTO private.auctionitems(
                                 trankeygen_id, 
                                 pid, 
                                 itemnumber, 
                                 itemdescription, 
                                 marketval, 
                                 memo) 
                                 VALUES (
                                         :trankeygen_id, 
                                         :pid, 
                                         :itemnumber, 
                                         :itemdescription,
                                         :marketval, 
                                         :memo)",
        [
          'trankeygen_id' => $trankeygen_id,
          'pid'=>$pid,
          'itemnumber'=> $ai->itemnumber,
          'itemdescription'=> $ai->itemdescription,
          'marketval'=> $ai->marketval,
          'memo'=> $ai->memo ?? null]
      );
    }
  }

  public function makeContactsForJSON($fund_id, $data)
  {
    $this->makeContact($fund_id, $data->expenditure->expenditureEvent->bnkid, true);
    $this->makeContact($fund_id, $data->expenditure->expenditureEvent->contid);
    foreach ($data->expenditure->expenses as $e) {
      $this->makeContact($fund_id, $e->cid, true);
      $this->makeContact($fund_id, $e->contid);
    }
  }

  private function makeContact($fund_id, $trankeygen_id, $isAccount = false)
  {
    $this->dm->db->executeStatement("INSERT INTO private.trankeygen(fund_id, trankeygen_id) VALUES (:fund_id, :trankeygen_id) on conflict (trankeygen_id) do nothing",
      ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]);
    $this->dm->db->executeStatement("INSERT INTO private.contacts(trankeygen_id, type) VALUES (:trankeygen_id, :type) on conflict (trankeygen_id) do nothing",
      ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id, 'type' => $isAccount ? 'HDN' : 'IND']);
    if ($isAccount) {
      $this->dm->db->executeStatement("INSERT INTO private.accounts(trankeygen_id) VALUES (:trankeygen_id) on conflict (trankeygen_id) do nothing",
        ['fund_id' => $fund_id, 'trankeygen_id' => $trankeygen_id]);
    }
  }

  /**
   * Test to ensure that the account specified has a specific total
   *
   * @param integer $account_id
   *   the trankegen_id of the account to be tested
   * @param $total
   *   the balance to be tested against
   * @return void
   * @throws \Doctrine\DBAL\Exception
   */
  public function assertAccountBalance($account_id, $total)
  {
    $actual = $this->dm->db->fetchOne("SELECT total from private.accounts where trankeygen_id = :account_id", ['account_id' => $account_id]);
    $this->assertTrue($actual !== false, "Missing account with trankeygen_id: $account_id");
    $this->assertEquals($total, $actual, "Account: $account_id");
  }

  public function assertContactGagg($contid, $expectedAmount) {
    $actualAmount = $this->dm->db->fetchOne("SELECT gagg from private.contacts where trankeygen_id = :contid", ['contid' => $contid]);
    $this->assertTrue($actualAmount !== false, "Missing contact with trankeygen_id: $contid");
    $this->assertEquals($expectedAmount, $actualAmount, "Contact: $contid");
  }

  public function assertContactPagg($contid, $expectedAmount) {
    $actualAmount = $this->dm->db->fetchOne("SELECT pagg from private.contacts where trankeygen_id = :contid", ['contid' => $contid]);
    $this->assertTrue($actualAmount !== false, "Missing contact with trankeygen_id: $contid");
    $this->assertEquals($expectedAmount, $actualAmount, "Contact: $contid");
  }
}
