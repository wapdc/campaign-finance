<?php

namespace Tests\WAPDC\CampaignFinance\VendorSubmission;

use Doctrine\DBAL\ConnectionException;
use Exception;
use stdClass;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\ORCA\ORCATestCase;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\Fund;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\CampaignFinance\VendorSubmissionProcessor;
use WAPDC\CampaignFinance\Model\Report;

class VendorSubmissionProcessorTest extends ORCATestCase
{
  /**
   * @var VendorSubmissionProcessor
   */
  private VendorSubmissionProcessor $vendorSubmissionProcessor;


  /**
   * @throws Exception
   */
  protected function setUp(): void
  {
    parent::setUp();
    $this->data_dir = dirname(dirname(__DIR__)) . '/data/orca';
    $this->vendorSubmissionProcessor = new VendorSubmissionProcessor($this->dm);
  }

  /**
   * @throws ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

  public function testValidateDraftReportStructure(){
    $C3Report = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c3.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $this->vendorSubmissionProcessor->validateDraftReportStructure($C3Report);
    $C4Report = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $this->vendorSubmissionProcessor->validateDraftReportStructure($C4Report);
    $this->assertTrue(true);
  }

  public function testSaveDraftReport() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee_data);
    $fund_id_21 = $this->createFund('2021', $committee->committee_id);

    $C4Report = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $view_only = false;
    $draftReportId = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C4Report, $view_only)->draft_id;
    $draftReport = (object) $this->dm->db->fetchAssociative('select * from private.draft_report where draft_id = :id', ['id' => $draftReportId]);
    $this->assertEquals($fund_id_21, $draftReport->fund_id);
    $this->assertEquals($C4Report->report->period_start, $draftReport->period_start);
    $this->assertEquals($C4Report->report->period_end, $draftReport->period_end);
    $this->assertEquals(strtotime("+2 weeks", strtotime($draftReport->received_date)),strtotime($draftReport->expire_date));
    $this->assertTrue($draftReport->processed);
    $this->assertEquals($view_only, $draftReport->view_only);
    $this->assertNull($draftReport->messages);
    $user_data = json_decode($draftReport->user_data);
    $metadata = json_decode($draftReport->metadata);
    foreach($C4Report->report as $key => $value) {
      $this->assertSame($value, $user_data->$key, $key);
    }
    foreach($C4Report->metadata as $key => $value) {
      $this->assertSame($value, $metadata->$key, $key);
    }

    //check updated vendor for C4 in fund table
    $vendor_name_c4 = $C4Report->metadata->vendor_name;
    $updated_vendor_c4 = $this->dm->db->fetchOne('SELECT f.vendor from public.fund f where f.fund_id = :fund_id', ['fund_id' => $fund_id_21]);
    $this->assertEquals($vendor_name_c4, $updated_vendor_c4);

      //update c4 and make sure new reportDraft not created
    $C4Report->report->period_start='2021-12-05';
    $draftReportId = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C4Report, $view_only)->draft_id;
    $updatedDraftReport = (object) $this->dm->db->fetchAssociative('select * from private.draft_report where draft_id = :id', ['id' => $draftReportId]);
    $this->assertEquals($draftReport->draft_id, $updatedDraftReport->draft_id);
    $this->assertEquals($C4Report->report->period_start, $updatedDraftReport->period_start);
    $this->assertEquals(strtotime("+2 weeks", strtotime($updatedDraftReport->received_date)),strtotime($updatedDraftReport->expire_date));

    //make sure invalid metadata gives errors
    $C4Report->metadata = null;
    $response = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C4Report, $view_only);
    $draftReportId = $response->draft_id;
    $updatedDraftReport = (object) $this->dm->db->fetchAssociative('select * from private.draft_report where draft_id = :id', ['id' => $draftReportId]);
    $this->assertFalse($updatedDraftReport->processed);
    $this->assertTrue(array_key_exists('metadata.required', $response->validation->messages));
    $messages = json_decode($updatedDraftReport->messages);
    $this->assertTrue(isset($messages->{'metadata.required'}));

    //do the same as above but with C3s
    $C3Report = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c3.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $view_only = true;
    $draftReportId = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C3Report, $view_only)->draft_id;
    $draftC3Report = (object) $this->dm->db->fetchAssociative('select * from private.draft_report where draft_id = :id', ['id' => $draftReportId]);
    $this->assertEquals($fund_id_21, $draftC3Report->fund_id);
    $this->assertEquals($C3Report->report->period_start, $draftC3Report->period_start);
    $this->assertEquals($C3Report->report->period_end, $draftC3Report->period_end);
    $this->assertEquals($C3Report->report->external_id, $draftC3Report->external_id);
    $this->assertTrue($draftC3Report->processed);
    $this->assertEquals($view_only, $draftC3Report->view_only);
    $user_data = json_decode($draftC3Report->user_data);
    $metadata = json_decode($draftC3Report->metadata);
    foreach($C3Report->report as $key => $value) {
      if (!is_array($value) && !is_object($value)) {
        $this->assertSame($value, $user_data->$key, $key);
      } elseif (is_object($value)) {
        foreach ($value as $k => $v) {
          if (!is_object($v)) {
            $this->assertSame($v, $user_data->$key->$v, $key . ': ' . $k);
          }
        }
      }
    }
    foreach($C3Report->metadata as $key => $value) {
      $this->assertSame($value, $metadata->$key, $key);
    }

    //check updated vendor for C3 in fund table
    $vendor_name_c3 = $C3Report->metadata->vendor_name;
    $updated_vendor_c3 = $this->dm->db->fetchOne('SELECT f.vendor from public.fund f where f.fund_id = :fund_id', ['fund_id' => $fund_id_21]);
    $this->assertEquals($vendor_name_c3, $updated_vendor_c3);

    //test update
    $C3Report->report->period_start = '2021-02-27';
    $C3Report->report->period_end = '2021-02-27';
    $draftReportId = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C3Report, $view_only)->draft_id;
    $updatedDraftC3Report = (object) $this->dm->db->fetchAssociative('select * from private.draft_report where draft_id = :id', ['id' => $draftReportId]);
    $this->assertEquals($draftC3Report->draft_id, $updatedDraftC3Report->draft_id);
    $this->assertEquals($C3Report->report->period_start, $updatedDraftC3Report->period_start);
    $this->assertEquals($C3Report->report->period_end, $updatedDraftC3Report->period_end);
  }

  public function testSubmitVendorReport() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee_data);
    $election_code = '2021';
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $C4Report = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $view_only = false;
    $payload = new stdClass();
    $payload->treasurer = "Lindy Myers";
    $draftReportId = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C4Report, $view_only)->draft_id;
    $response = $this->vendorSubmissionProcessor->submitVendorReport($draftReportId, $committee->committee_id, '1111', $payload);
    //make sure submission saves as coming from vendor_name field in submission / submission_version specified
    $submission = (object) $this->dm->db->fetchAssociative('select * from campaign_finance_submission where report_id = :report_id', ['report_id' => $response->report_id]);
    $this->assertEquals($C4Report->metadata->vendor_name, $submission->source);
    $this->assertEquals($C4Report->metadata->submission_version, $submission->submission_version);
    //make sure we saved a report with expected values
    $report = (object) $this->dm->db->fetchAssociative('select * from report where report_id = :report_id', ['report_id' => $response->report_id]);
    $this->assertNotEmpty($report);
    $this->assertEquals($C4Report->report->report_type, strtolower($report->report_type));
    $this->assertEquals($this->fund_id, $report->fund_id);
    $this->assertEquals($C4Report->report->period_start, $report->period_start);
    $this->assertEquals($C4Report->report->period_end, $report->period_end);
    $this->assertEquals($election_code, $report->election_year);
    //check the source (vendor) and submission_version against scenario file
    $this->assertEquals($C4Report->metadata->vendor_name, $report->source);
    $this->assertEquals($C4Report->metadata->submission_version, $report->version);

      //make sure we delete the report draft
    $draft = $this->dm->db->fetchAssociative('select * from private.draft_report where draft_id = :draft_id', ['draft_id' => $draftReportId]);
    $this->assertEmpty($draft);
  }

  /**
   * Tests to make sure that a C3 with the same external_id produces an amendment.
   * @return void
   * @throws \Doctrine\DBAL\Exception
   */
  public function testAmendment() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee_data);
    $election_code = '2021';
    $this->fund_id = $this->createFund($election_code, $committee->committee_id);
    $payload = new stdClass();
    $payload->treasurer = "Lindy Myers";
    $C3Report = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c3.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $view_only = false;

    $originalDraftId = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C3Report, $view_only)->draft_id;
    $originalId = $this->vendorSubmissionProcessor
      ->submitVendorReport($originalDraftId, $committee->committee_id, '1111', $payload)
      ->report_id;

    $amendmentDraftId = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C3Report, $view_only)->draft_id;
    $amendmentId = $this->vendorSubmissionProcessor
      ->submitVendorReport($amendmentDraftId, $committee->committee_id, '1111', $payload)
      ->report_id;

    $original = $this->dm->em->find(Report::class, $originalId);

    $this->assertEquals($amendmentId, $original->superseded_id,
      "The superseded_id of original was not set to report_id of amendment");
  }

  /**
   * Draft reports should be rejected when the committee is not verified
   * @return void
   */
  public function testRejectDraftForUnverifiedCommittee() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data->committee->filer_id = null;
    $committee = $this->createCommittee($committee_data);
    $registration = $this->dm->em->find(Registration::class, $committee->registration_id);
    $this->assertFalse($registration->verified);
    $C4Report = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $view_only = false;
    $draftReport = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C4Report, $view_only);

    $this->assertFalse($draftReport->validation->success);
    $this->assertArrayHasKey('committee.unverified', $draftReport->validation->messages);
  }

  /**
   * A fund should be automatically created when we accept a draft report, if the committee is verified.
   * @return void
   */
  public function testEnsureCommitteeHasFundWhenAcceptingDraft() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee = $this->createCommittee($committee_data);
    $election_code = '2021';
    $C4Report = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $view_only = false;
    $draftReportId = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $C4Report, $view_only)->draft_id;
    $draftReport = (object) $this->dm->db->fetchAssociative(
      'select * from private.draft_report where draft_id = :draft_id', ['draft_id' => $draftReportId]);
    $this->assertNotEmpty($draftReport->fund_id, 'Missing fund_id');
  }

  public function testSingleToContinuingPostsToCorrectFund() {
    $committee_data = Yaml::parseFile($this->data_dir . "/committees/continuing-pac.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $committee_data->committee->election_code = '2020S1';
    $committee_data->committee->start_year = 2020;
    $committee = $this->createCommittee($committee_data);

    $report2019 = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $report2019->report->period_start = "2019-02-01";
    $report2019->report->period_end = "2019-02-01";
    $report2020 = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $report2020->report->period_start = "2020-02-01";
    $report2020->report->period_end = "2020-02-01";
    $report2021 = Yaml::parseFile($this->data_dir . "/reports/vendor_reporting/vendor-c4.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $report2021->report->period_start = "2021-02-01";
    $report2021->report->period_end = "2021-02-01";

    // 2019 report should save to 2020 fund as single election year committee report
    $draftId2019 = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $report2019, false)->draft_id;
    $fundId2019 = ((object) $this->dm->db->fetchAssociative(
      'select * from private.draft_report where draft_id = :draft_id', ['draft_id' => $draftId2019]))->fund_id;
    $fund2019 = $this->dm->em->find(Fund::class, $fundId2019);
    $this->assertEquals("2020", $fund2019->election_code, 'Incorrect election code');

    // 2020 report should save to 2020 fund as single election year committee report
    $draftId2020 = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $report2020, false)->draft_id;
    $fundId2020 = ((object) $this->dm->db->fetchAssociative(
      'select * from private.draft_report where draft_id = :draft_id', ['draft_id' => $draftId2020]))->fund_id;
    $fund2020 = $this->dm->em->find(Fund::class, $fundId2020);
    $this->assertEquals("2020", $fund2020->election_code, 'Incorrect election code');

    // 2021 report should save to 2021 fund as single election year committee report
    $draftId2021 = $this->vendorSubmissionProcessor->saveDraftReport($committee->committee_id, $report2021, false)->draft_id;
    $fundId2021 = ((object) $this->dm->db->fetchAssociative(
      'select * from private.draft_report where draft_id = :draft_id', ['draft_id' => $draftId2021]))->fund_id;
    $fund2021 = $this->dm->em->find(Fund::class, $fundId2021);
    $this->assertEquals("2021", $fund2021->election_code, 'Incorrect election code');
  }

}
