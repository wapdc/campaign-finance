<?php

namespace Tests\WAPDC\CampaignFinance\Registration;

use Doctrine\ORM\ORMException;
use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\RegistrationManager;
use WAPDC\CampaignFinance\SubmissionErrors;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\RegistrationDraftManager;
use WAPDC\CampaignFinance\Model\CommitteeRegistrationDraft;
use WAPDC\Core\FilerProcessor;
use WAPDC\Core\Model\JurisdictionOffice;
use WAPDC\Core\Model\User;
use \stdClass;
use \DateTime;
use \DateInterval;


/**
 * Class RegistrationDraftManagerTest
 * @package Tests\WAPDC\CampaignFinance
 */
class RegistrationDraftManagerTest extends CommitteeTestCase {

  /**
   * @var RegistrationDraftManager
   */
  private $registrationDraftManager;

  /**
   * @var RegistrationManager
   */
  private $registrationManager;

  /**
   * @var CommitteeManager
   */
  private $committeeManager;

  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->committeeManager = new CommitteeManager($this->dm);
    $this->registrationDraftManager = new RegistrationDraftManager($this->dm, $this->committeeManager);
    $fp = new FilerProcessor($this->dm);
    $this->registrationManager = new RegistrationManager($this->dm, $this->committeeManager, $this->registrationDraftManager, $fp);
  }


  /**
   * @return User
   * @throws \Exception
   */
  public function generateUser(){
    $u = new User();
    $u->realm = 'test.pdc,wa.gov';
    $u->uid = 999;
    $u->user_name = 'u_name';
    $this->dm->em->persist($u);
    $this->dm->em->flush();
    return $u;
  }

  /**
   * @param string $scenario
   * @return \stdClass
   * @throws ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  function generateDraftFromSubmission($scenario) {
    $dir = dirname(dirname(__DIR__ )) . '/data/registration_submission';
    $submission_file = "$dir/$scenario.yml";
    $submission = Yaml::parseFile($submission_file, YAML::PARSE_OBJECT_FOR_MAP);

    $committee_name = $submission->committee->name;
    $committee_type = $submission->committee->committee_type;
    $election_code =  $submission->committee->election_code ?? NULL;

    // Manufacture committee
    $committee = new Committee($committee_name, $committee_type);
    $committee->election_code = $election_code;
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    // Manufacture registration record
    $draft = new CommitteeRegistrationDraft($committee->committee_id);
    $draft->setRegistrationData($submission);
    $this->dm->em->persist($draft);
    $this->dm->em->flush();

    $this->committee = $committee;
    $submission->committee->committee_id = $committee->committee_id;
    return $submission;
  }



  /**
   * @throws ORMException
   * @throws \Exception
   */
  public function testSaveCommitteeRegistrationDraft() {
    //start with a fresh committee
    $draftToBeSaved = $this->generateDraftFromSubmission('candidate');
    $draftToBeSaved->committee->acronym = 'test';
    $draftToBeSaved->committee->contact->address = '123 street ave';
    $draftToBeSaved->committee->contact->state = 'WA';
    $draftToBeSaved->committee->contact->city = 'Olympia';
    $draftToBeSaved->committee->contact->postcode = '98503';
    $draftToBeSaved->committee->contact->phone = '3605554444';
    $draftToBeSaved->committee->contact->email = 'email@gmail.com';

    $u = $this->generateUser();

    $this->registrationDraftManager->saveCommitteeRegistrationDraft($draftToBeSaved, $u);
    $updatedDraft = $this->registrationDraftManager->getCommitteeRegistrationDraft($draftToBeSaved->committee->committee_id);
    $this->assertEquals($draftToBeSaved->committee->acronym, $updatedDraft->committee->acronym);
    $this->assertEquals($draftToBeSaved->committee->contact->address, $updatedDraft->committee->contact->address);
    $this->assertEquals($draftToBeSaved->committee->contact->state, $updatedDraft->committee->contact->state);
    $this->assertEquals($draftToBeSaved->committee->contact->city, $updatedDraft->committee->contact->city);
    $this->assertEquals($draftToBeSaved->committee->contact->postcode, $updatedDraft->committee->contact->postcode);
    $this->assertEquals($draftToBeSaved->committee->contact->phone, $updatedDraft->committee->contact->phone);
    $this->assertEquals($draftToBeSaved->committee->contact->email, $updatedDraft->committee->contact->email);
  }


  /**
   * @dataProvider validationScenarioProvider
   * @param string $scenario
   *   Scenario to test
   * @throws \Exception
   */
public function testCopyRegistrationCommitteeDraft($scenario) {
    $user = new stdClass();
    $user->realm = 'test.pdc.wa.gov';
    $user->uid = '1';
    $user->user_name = 'what';
    $election_code = null;

    $submission = $this->generateCommitteeFromScenario($scenario);

    if (isset($submission->committee->election_code)) {
      $election_code = 2022;
    }
    $committee_id = $this->registrationDraftManager->copyCommitteeRegistrationDraftToNewCommittee($submission, $election_code, $user);
    $new_submission = $this->registrationDraftManager->getCommitteeRegistrationDraft($committee_id);


    $sc = $submission->committee;
    $new_sc = $new_submission->committee;
    //make sure the committee_ids on the drafts are different.
    $this->assertNotEquals($sc->committee_id, $new_sc->committee_id);
    //make sure the year on the new committee is correct.
    $this->assertEquals($election_code, $new_sc->election_code);

    //name, acronym, sponsorship
    if (isset($sc->has_sponsor)) {
      $this->assertEquals($sc->has_sponsor, $new_sc->has_sponsor);
      if ($sc->has_sponsor == 'yes') {
        $this->assertEquals($sc->sponsor, $new_sc->sponsor);
        $this->assertEquals($sc->base_name, $new_sc->base_name);
      }
    }
    $this->assertEquals($sc->name, $new_sc->name);
    if (isset($sc->acronym)){
      $this->assertEquals($sc->acronym, $new_sc->acronym);
    }
    //committee contact
    $this->assertEquals($sc->contact->email, $new_sc->contact->email);
    $this->assertEquals($sc->contact->phone, $new_sc->contact->phone);
    $this->assertEquals($sc->contact->address, $new_sc->contact->address);
    $this->assertEquals($sc->contact->city, $new_sc->contact->city);
    $this->assertEquals($sc->contact->state, $new_sc->contact->state);
    $this->assertEquals($sc->contact->postcode, $new_sc->contact->postcode);
    //bank
    $this->assertEquals($sc->bank->name, $new_sc->bank->name);
    $this->assertEquals($sc->bank->address, $new_sc->bank->address);
    $this->assertEquals($sc->bank->city, $new_sc->bank->city);
    $this->assertEquals($sc->bank->state, $new_sc->bank->state);
    $this->assertEquals($sc->bank->postcode, $new_sc->bank->postcode);
    //books
    $this->assertEquals($sc->books_contact->email, $new_sc->books_contact->email);
    //reporting_type
    $this->assertEquals($sc->reporting_type, $new_sc->reporting_type);
    //officers
    $this->assertEquals(count($sc->officers), count($new_sc->officers));
    //candidacy
    if ($sc->committee_type == 'CA') {
      $this->assertEquals($sc->candidacy->person, $new_sc->candidacy->person);
      if (isset($sc->candidacy->ballot_name)) {
        $this->assertEquals($sc->candidacy->ballot_name, $new_sc->candidacy->ballot_name);
      }
      $this->assertEquals($sc->candidacy->email, $new_sc->candidacy->email);
      $this->assertEquals($sc->candidacy->jurisdiction, $new_sc->candidacy->jurisdiction);
      $this->assertEquals($sc->candidacy->office, $new_sc->candidacy->office);
    }
  }

  /**
   * @throws \Exception
   */
  public function testDeleteDraft(){
    $user_name = 'u_name';

    //Test delete draft for committee amendment to delete draft, but not committee.
    $submission = $this->generateDraftFromSubmission('candidate');
    $submission->submitted_at = "";
    $committee_id = $submission->committee->committee_id;
    $committee = $this->dm->em->find(Committee::class, $committee_id);
    $certification_email = 'ben.livingston@pdc.wa.gov';
    $registration_id = $this->registrationManager->createRegistrationFromSubmission($submission, $certification_email, $user_name);
    $committee->registration_id = $registration_id;
    $committee->c1_sync = new DateTime;

    $draft = new CommitteeRegistrationDraft($committee_id);
    $draft->setRegistrationData($submission);
    $this->dm->em->persist($draft);
    $this->dm->em->flush();

    $draft = $this->dm->em->find(CommitteeRegistrationDraft::class, $committee_id);
    $this->assertNotEmpty($draft);

    $this->registrationDraftManager->deleteDraft($committee_id);
    $deletedDraft = $this->dm->em->find(CommitteeRegistrationDraft::class, $committee_id);
    $this->assertEmpty($deletedDraft);
    $committee = $this->dm->em->find(Committee::class, $committee_id);
    $this->assertNotEmpty($committee);

    //Test delete draft for new never registered committees also deletes committee
    $newSub = $this->generateDraftFromSubmission('candidate');
    $newSub->type = 'draft';
    $newCommitteeId = $newSub->committee->committee_id;
    $draft = $this->dm->em->find(CommitteeRegistrationDraft::class, $newCommitteeId);
    $this->assertNotEmpty($draft);

    $this->registrationDraftManager->deleteDraft($newCommitteeId);
    $deletedDraft = $this->dm->em->find(Committee::class, $newCommitteeId);
    $this->assertEmpty($deletedDraft);
    $committee = $this->dm->em->find(Committee::class, $newCommitteeId);
    $this->assertEmpty($committee);
  }

  /**
   * Test to make sure that non-partisan candidacies that have a party on record will be able to amend committee
   * Make sure verification of registration amendment passes verification.
   * @throws /Exception
   */
  public function testValidateAmendedNonpartisan(){
    $draft = $this->generateCommitteeFromScenario('candidate-np');
    $draft->committee->candidacy->party = 'PITY PARTY'; //other party = party_id 21
    $r = $this->generateNewRegistrationForCommittee($draft, $draft->committee->committee_id);
    //create candidacy for committee
    $this->verifyReg($r->registration_id);
    $user = $this->generateUser();
    $q = $this->registrationManager->amendRegistration($r->registration_id, $r->committee->committee_id, $user);
    $se = new SubmissionErrors();
    $this->registrationDraftManager->validateDraft($q, $se);
    $this->assertEquals(true, $se->hasErrors());
  }

  public function validationScenarioProvider(){
    return [
      ['candidate'],
      ['political-committee'],
      ['issue-committee'],
      ['slate-committee'],
      ['slate-committee-proposals']
    ];
  }

  /**
   * @dataProvider validationScenarioProvider
   * @param string $scenario
   *   Scenario to test
   * @throws \Exception
   */
  public function testValidateDraft($scenario) {
    $dir = dirname(dirname(__DIR__))  . "/data/registration_submission";
    $validations = Yaml::parseFile("$dir/$scenario.validations.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Object has tests property which contains an array of test condition.
    foreach($validations->tests as $test) {
      // Start with the same initial data.
      $submission = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
      $expected_errors = [];

      // Each condition either an assertion of an error or and assertion of a particular initial state modified from
      // the base $submission
      foreach($test as $condition => $value) {

        switch(true) {
          case ($condition == 'name'):
            break;

          case (strpos($condition, '.')!==FALSE):
            $this->setSubmissionValue($submission, $condition, $value);
            break;
          case ($condition == 'error'):
            if (is_array($value)) {
              foreach($value as $error) {
                $expected_errors [] = $error;
              }
            }
            else {
              $expected_errors [] = $value;
            }
            break;
        }

      }

      // Manufacture committee
      $committee = new Committee($submission->committee->name ?? '', $submission->committee->committee_type ?? 'candidate');
      $committee->election_code = $submission->committee->election_code ?? NULL;
      $this->dm->em->persist($committee);
      $this->dm->em->flush($committee);
      $submission->committee->committee_id = $committee->committee_id;

      $errorBus = new SubmissionErrors();
      $this->registrationDraftManager->validateDraft($submission, $errorBus);
      $errors = $errorBus->getErrors();
      $conditions = print_r($errors, 1);
      if ($expected_errors && $expected_errors != ['']){
        foreach ($expected_errors as $expected_error) {
          $this->assertArrayHasKey($expected_error, $errors, "Missing expected errors for $scenario: $conditions");
        }
      }
      else {
        $this->assertEmpty($errors, "Unexpected error(s) for $scenario : $conditions");
      }

    }
  }

  /**
   * Sets values of conditions of the form object.property to the value defined.
   * @param \stdClass $registration
   * @param string $condition
   * @param mixed $value
   */
  public function setSubmissionValue($registration, $condition, $value) {
    list($object, $property) = explode('.', $condition, 2);
    switch ($object){
      case '':
      case 'registration':
        $this->setProperty($registration, $property, $value);
        break;
      case 'committee':
        $this->setProperty($registration->committee, $property, $value);
        break;
      case 'candidacy':
        $this->setProperty($registration->committee->candidacy, $property, $value);
        break;
      case 'officers':
        for ($i = 0; $i<sizeof($registration->committee->officers); $i++) {
          $this->setProperty($registration->committee->officers[$i], $property, $value);
        }
        break;
      case 'contact':
        $this->setProperty($registration->committee->contact, $property, $value);
        break;
      case 'bank':
        $this->setProperty($registration->committee->bank, $property, $value);
        break;
      case 'books':
        $this->setProperty($registration->committee->books_contact, $property, $value);
        break;
      case 'affiliation':
        $this->setProperty($registration->committee->affiliations[0], $property, $value);
        break;
      case 'seec':
        $this->setProperty($registration->local_filing->seec, $property, $value);
    }
  }

  /**
   * Sets the property of an object or removes the property if the value is null.
   *
   * @param $object
   * @param $property
   * @param $value
   */
  protected function setProperty($object, $property, $value) {
    if (isset($object->$property) && $value === NULL) {
      unset($object->$property);
    }
    else {
      $object->$property = $value;
    }
  }

  public function testAmendJurisdictionOffice(){
    $draft = $this->generateCommitteeFromScenario('candidate-np');
    $draft->committee->candidacy->party = null;
    $r = $this->generateNewRegistrationForCommittee($draft, $draft->committee->committee_id);

    //create candidacy for committee
    $this->verifyReg($r->registration_id);

    // Updating the candidacy after the fact to simulate a candidacy that has been matched by the declarations process.
    $this->dm->db->executeStatement("UPDATE candidacy set sos_candidate_code='test_code' where committee_id = :committee_id",
      ['committee_id' => $draft->committee->committee_id]);

    $this->dm->em->clear();

    $user = $this->generateUser();
    $draft = $this->registrationManager->amendRegistration($r->registration_id, $r->committee->committee_id, $user);
    $draft->committee->candidacy->jurisdiction_id = 4010;
    $se = new SubmissionErrors();
    $this->registrationDraftManager->validateDraft($draft, $se);
    $this->assertEquals(true, $se->hasErrorId('candidacy.jurisdictionoffice.change'));
  }

  public function testAmendRegistration() {
    $draft = $this->generateCommitteeFromScenario('political-committee');
    // verify that redirection_url is cleared out on amend
    $draft->redirection_url = "ignore me";
    $r = $this->generateNewRegistrationForCommittee($draft, $draft->committee->committee_id);
    $this->verifyReg($r->registration_id);
    $this->assertEquals(true, $r->committee->continuing);
    $c = $this->dm->em->find(Committee::class, $r->committee->committee_id);
    $this->assertEquals(true, $c->continuing);
    // change committee to single year and the draft should prefer that over the registration continuing field
    $c->continuing = false;
    $c->election_code = $c->end_year = $c->start_year;
    $this->dm->em->flush($c);
    $user = $this->generateUser();
    $q = $this->registrationManager->amendRegistration($r->registration_id, $r->committee->committee_id, $user);
    // amend should not copy redirection_url
    $redirection_url = $q->redirection_url ?? null;
    $this->assertNull($redirection_url);
    $this->assertEquals(false, $q->committee->continuing);
    $se = new SubmissionErrors();
    $this->registrationDraftManager->validateDraft($q, $se);
    $this->assertEquals(false, $se->hasErrors());
  }

  public function testTemporarllyInvalidJurisdictionOffice() {
    // jurisdiction_office expired yesterday
    $date = new DateTime();
    $date->sub(new DateInterval('P1D'));
    $jo = $this->dm->em->getRepository(JurisdictionOffice::class)->findOneBy(['jurisdiction_id' => 1000, 'offcode' => '03']);
    $jo->valid_to = $date;
    $this->dm->em->flush($jo);

    // load submission
    $dir = dirname(dirname(__DIR__))  . "/data/registration_submission";
    $submission = Yaml::parseFile($dir . '/candidate.yml', Yaml::PARSE_OBJECT_FOR_MAP);

    // draft committee
    $committee = new Committee($submission->committee->name, $submission->committee->committee_type);
    $committee->election_code = $submission->committee->election_code;
    $this->dm->em->persist($committee);
    $this->dm->em->flush($committee);

    // set committee id on submission
    $submission->committee->committee_id = $committee->committee_id;

    // load error bus
    $se = new SubmissionErrors();

    // validate and check for errors
    $this->registrationDraftManager->validateDraft($submission, $se);
    $this->assertEquals(true, $se->hasErrorId('candidacy.jurisdictionoffice.invalid'));

    // jurisdiction_office starts tomorrow
    $date->add(new DateInterval('P2D'));
    $jo->valid_from = $date;
    $jo->valid_to = null;
    $this->dm->em->flush($jo);

    // validate and check for errors
    $se2 = new SubmissionErrors();
    $this->registrationDraftManager->validateDraft($submission, $se2);
    $this->assertEquals(true, $se2->hasErrorId('candidacy.jurisdictionoffice.invalid'));
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }
}