<?php
/**
 * Created by PhpStorm.
 * User: nick.holmquist
 * Date: 1/10/2019
 * Time: 2:37 PM
 */

namespace Tests\WAPDC\CampaignFinance\Registration;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\TestCase;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\CommitteeRegistrationDraft;
use WAPDC\CampaignFinance\Model\Registration;

class CFDataManagerTest extends TestCase {


  /**
   * @var CFDataManager
   */
  private $dm;

  /**
   * @var CommitteeManager
   */
  private $committeeManager;

  /**
   * @throws \Doctrine\DBAL\DBALException
   * @throws \Doctrine\ORM\ORMException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->committeeManager = new CommitteeManager($this->dm);
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

  public function testCreateCommittee() {

    $committee = new Committee('test committee');

    $this->dm->em->persist($committee);
    $this->dm->em->flush();
    $this->dm->em->clear();
    /** @var Committee $createdCommittee*/
    $createdCommittee = $this->dm->em->find(Committee::class, $committee->committee_id);

    $this->assertInstanceOf(Committee::class, $createdCommittee);
    $this->assertEquals($committee->name, $createdCommittee->name);

  }

  public function testCreateDraftCommittee() {

    $committee = new Committee('test committee');

    $this->dm->em->persist($committee);
    $this->dm->em->flush();
    $object = new \stdClass();
    $object->name = 'test name';
    $draft = new CommitteeRegistrationDraft($committee->committee_id);
    $draft->setRegistrationData($object);
    $draft->verified = true;
    $draft->username = 'test user';
    $draft->attachment_reference = 'test attachment';
    $draft->updated_at = new \DateTime('2012-12-12T06:00:00Z');

    $this->dm->em->persist($draft);
    $this->dm->em->flush();

    $this->dm->em->clear();
    /** @var CommitteeRegistrationDraft $createdDraft */
    $createdDraft = $this->dm->em->find(CommitteeRegistrationDraft::class, $draft->committee_id);

    $this->assertTrue($createdDraft->verified);
    $saved_object = $createdDraft->getRegistrationData();
    $this->assertEquals($object->name, $saved_object->name);
    $this->assertEquals('test user', $createdDraft->username);
    $this->assertEquals('test attachment', $createdDraft->attachment_reference);
    $this->assertEquals(new \DateTime('2012-12-12T06:00:00Z'), $draft->updated_at);

  }

}