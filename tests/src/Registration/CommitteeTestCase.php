<?php


namespace Tests\WAPDC\CampaignFinance\Registration;

use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\Core\Mock\MockMailer;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\Core\FilerProcessor;
use stdClass;
use WAPDC\Core\Model\Candidacy;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\CampaignFinance\RegistrationDraftManager;
use WAPDC\CampaignFinance\RegistrationManager;
use WAPDC\Core\Model\Entity;

/**
 * Class CommitteeTestCase
 *
 * Base class for loading committees based on sample data.
 * @package Tests\WAPDC\CampaignFinance
 */
abstract class CommitteeTestCase extends TestCase {

  protected $populate_committee = FALSE;


  /** @var Committee */
  protected $committee;


  /** @var CFDataManager */
  protected $dm;

  /**
   * @var Entity
   */
  protected $entity;

  /**
   * @var Candidacy
   */
  protected $candidacy;

  /**
   * @var Registration
   */
  protected $registration;


  protected function generateEntity($entity)
  {
    if ($entity) {
      $this->entity = new Entity();
      $this->dm->em->persist($this->entity);
      $this->entity->name = $entity->name ?? 'Test Entity';
      $this->entity->is_person = $entity->is_person ?? true;
      $this->entity->filer_id = $entity->filer_id ?? null;
      $this->dm->em->flush($this->entity);
      $entity->entity_id = $this->entity->entity_id;
    }
    return $this->entity;
  }


  protected function generateCommitteeRegistration(stdClass $submission, stdClass $admin_data = null, $unverified = false) {
    $submission_json = json_encode($submission);
    $committee = new Committee($submission->committee->name, $submission->committee->committee_type);
    $committee->reporting_type = $submission->committee->reporting_type ?? 'full';

    if (!$unverified && $this->entity) {
      $committee->person_id = $this->entity->entity_id;
    }

    $committee->filer_id = $unverified ? null : ($registration->committee->filer_id ?? null);
    $committee->updated_at = new DateTime();
    $committee->election_code = $submission->committee->election_code ?? null;
    $committee->start_year = $submission->committee->start_year;
    $committee->end_year = $submission->committee->end_year ?? NULL;
    $committee->pac_type = $submission->committee->pac_type;
    if (!empty($submission->committee->continuing) || $submission->committee->pac_type == 'surplus') {
      $committee->continuing = true;
    } else {
      $committee->continuing = false;
    }
    if ($submission->committee->pac_type == "bonafide") {
      $committee->bonafide_type = $submission->committee->bonafide_type ?? NULL;
      $committee->exempt = !empty($submission->committee->exempt) ? true : false;
    }
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    $registration = new Registration();
    $registration->committee_id = $committee->committee_id;
    $registration->user_data = $submission_json;
    $registration->admin_data = json_encode($admin_data);
    $registration->submitted_at = new DateTime();
    $registration->source = 'test';
    $registration->reporting_type = $submission->committee->reporting_type;
    $registration->certification_email = 'test@noreply.com';
    $registration->username = 'test@noreply.com';
    $this->dm->em->persist($registration);
    $committee->registration_id = $registration->registration_id;
    $this->dm->em->flush();

    $this->committee = $committee;
    $this->registration = $registration;
    $submission->committee->committee_id = $committee->committee_id;

    $this->dm->em->persist($registration);
    $this->dm->em->flush();
    $submission->registration_id = $registration->registration_id;
    return $this->registration;
  }

  protected function loadTestScenario($scenario, $path='registration_submission') {
    $submission_file = dirname(dirname(__DIR__)) . "/data/$path/$scenario.yml";
    return Yaml::parseFile($submission_file, YAML::PARSE_OBJECT_FOR_MAP);
  }


  /**
   * @param string $scenario
   * @param null $name
   * @param false $set_filer_id
   * @param string $path
   * @return stdClass
   * @throws \Exception
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  protected function generateCommitteeFromScenario($scenario, $name = NULL, $set_filer_id = FALSE, $path = '/data/registration_submission') {
    $dir = dirname(dirname(__DIR__ )) . $path;
    $submission_file = "$dir/$scenario.yml";
    $verification_file = "$dir/$scenario.admin_data.yml";
    $submission = Yaml::parseFile($submission_file, YAML::PARSE_OBJECT_FOR_MAP);
    $verification = new stdClass();
    $verification->committee = new stdClass();
    $submission->admin_data = $verification;
    if (file_exists($verification_file)) {
      $verification = Yaml::parseFile($verification_file, YAML::PARSE_OBJECT_FOR_MAP);
    };

    if ($name) {
      $submission->committee->name = $name;
    }

    $registration = $this->generateCommitteeRegistration($submission, $verification, !$set_filer_id);

    if ($this->populate_committee) {
      $fp = new FilerProcessor($this->dm);
      $cm = new CommitteeManager($this->dm);
      $dm = new RegistrationDraftManager($this->dm, $cm);
      $rm = new RegistrationManager($this->dm, $cm, $dm, $fp);
      $rm->setMailer(new MockMailer());
      $rm->verifyRegistration($registration->registration_id);
    }
    return $submission;
  }

  /**
   * @param $registration_id
   * @throws \Exception
   */
  public function verifyReg($registration_id) {
    $fp = new FilerProcessor($this->dm);
    $cm = new CommitteeManager($this->dm);
    $dm = new RegistrationDraftManager($this->dm, $cm);
    $rm = new RegistrationManager($this->dm, $cm, $dm, $fp);
    $rm->setMailer(new MockMailer());
    $rm->verifyRegistration($registration_id);
}

  /**
   * @param $submission
   * @param $committee_id
   * @return mixed
   * @throws \Exception
   */
  public function generateNewRegistrationForCommittee($submission, $committee_id){
    $submission_json = json_encode($submission);
    $verification = new stdClass();
    $verification->committee = new stdClass();

    $committee = $this->dm->em->find(Committee::class, $committee_id);
    $committee->updated_at = new DateTime();
    $committee->end_year = $submission->committee->end_year ?? NULL;
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    $registration = new Registration();
    $registration->committee_id = $committee->committee_id;
    $registration->user_data = $submission_json;
    $registration->admin_data = json_encode($verification);
    $registration->submitted_at = new DateTime();
    $submission->admin_data = $verification;
    $registration->source = 'test';
    $registration->username = 'test@noreply.com';
    $this->dm->em->persist($registration);
    $this->dm->em->flush();
    $committee->registration_id = $registration->registration_id;
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    $this->committee = $committee;
    $this->registration = $registration;
    $submission->committee->committee_id = $committee->committee_id;
    $submission->registration_id = $registration->registration_id;

    return $submission;
  }

}