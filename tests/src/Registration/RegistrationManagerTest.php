<?php

namespace Tests\WAPDC\CampaignFinance\Registration;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\CampaignFinance\Mock\MockFileService;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\CommitteeContact;
use WAPDC\CampaignFinance\Model\CommitteeRegistrationDraft;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\Model\RegistrationAttachment;
use WAPDC\CampaignFinance\RegistrationDraftManager;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\CampaignFinance\RegistrationManager;
use WAPDC\CampaignFinance\SubmissionDecoder\RegistrationDecoder;
use WAPDC\CampaignFinance\SubmissionDecoder\RegistrationLegacyDecoder;
use WAPDC\Core\FilerProcessor;
use WAPDC\Core\Model\Candidacy;
use WAPDC\Core\Model\Person;
use WAPDC\Core\Model\Entity;
use WAPDC\Core\Model\Position;
use WAPDC\Core\Messenger;
use Tests\WAPDC\Core\Mock\MockMailer;
use WAPDC\Core\Model\User;
use \DateTime;
use \stdClass;
use \Exception;

/**
 * Class RegistrationManagerTest
 * @package Tests\WAPDC\CampaignFinance
 */
class RegistrationManagerTest extends CommitteeTestCase {

  /** @var MockMailer */
  private $mailer;

  /**
   * @var RegistrationManager
   */
  private $registrationManager;

  /**
   * @var CommitteeManager
   */
  private $committeeManager;

  /**
   * @var RegistrationDraftManager
   */
  private $registrationDraftManager;

  public $filerProcessor;

  public $messenger;


  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->committeeManager = new CommitteeManager($this->dm);
    $this->registrationDraftManager = new RegistrationDraftManager($this->dm, $this->committeeManager);
    $this->filerProcessor = new FilerProcessor($this->dm);
    $this->registrationManager = new RegistrationManager($this->dm, $this->committeeManager, $this->registrationDraftManager, $this->filerProcessor, new MockFileService());
    $this->mailer = new MockMailer();
    $this->registrationManager->setMailer($this->mailer);
    $this->messenger = new Messenger($this->dm);
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

  private function getCandidacyAndPersonByCommittee($committee_id)
  {
    $committee = $this->dm->em->find(Committee::class, $committee_id);
    $candidacy = $this->dm->em->getRepository(Candidacy::class)->findOneBy(['committee_id' => $committee_id]);
    $person = $candidacy ? $this->dm->em->getRepository(Person::class)->findOneBy(['person_id' => $candidacy->person_id]) : null;
    return [$committee, $candidacy, $person];
  }

  /**
   * Generates a verified committee, registration and, in the case of candidacy committees, a candidacy.
   * @param $submission
   * @throws ORMException
   */
  protected function generateVerifiedCommittee($submission )
  {
    $committee = new Committee($submission->committee->name, $submission->committee->committee_type);
    $this->dm->em->persist($committee);
    $this->dm->em->flush($committee);
    $committee->filer_id = $submission->committee->filer_id;
    $committee->start_year = $submission->committee->start_year;
    $committee->end_year = $submission->committee->end_year ?? null;
    $committee->election_code = $submission->committee->election_code ?? Null;
    $committee->pac_type = $submission->committee->pac_type;
    $committee->person_id = $this->entity->entity_id;
    $committee->reporting_type = $submission->committee->reporting_type ?? 'mini';
    if (!empty($submission->committee->continuing)) {
      $committee->continuing = true;
    }

    $registration = new Registration();
    $this->dm->em->persist($registration);
    $registration->committee_id = $committee->committee_id;
    $registration->verified = true;

    $person = $this->entity;
    if ($submission->committee->pac_type === 'candidate') {
        $candidacy = new Candidacy();
        $this->dm->em->persist($candidacy);
        $candidacy->jurisdiction_id =  $submission->committee->candidacy->jurisdiction_id;
        $candidacy->office_code = $submission->committee->candidacy->office_code;
        $candidacy->committee_id = $committee->committee_id;
        $candidacy->election_code = $submission->committee->election_code;
        $candidacy->person_id = $person->entity_id;
        $candidacy->filer_id = $person->filer_id;
    }

    $committee->person_id = $person->entity_id ?? null;


    $this->dm->em->flush();

    $result = [$committee, $registration];
    if (!empty($candidacy)) $result[] = $candidacy;

    return $result;
  }

  /**
   * Create a committee record with a registration.
   * @param $committee_name
   * @param $committee_type
   * @param $xml
   *
   * @return Registration
   * @throws \Exception
   */
  protected function generateCommitteeWithRegistration($committee_name, $committee_type, $data) {

    $committee = new Committee($committee_name, $committee_type);
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    $registration = new Registration();
    $registration->committee_id = $committee->committee_id;
    $registration->user_data = $data;
    $this->dm->em->persist($registration);
    $this->dm->em->flush();

    $this->committee = $committee;
    $this->registration = $registration;

    return $registration;
  }

  /**
   * @param string $scenario
   * @return stdClass
   * @throws ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  function generateDraftFromSubmission($scenario) {
    $dir = dirname(dirname(__DIR__ )) . '/data/registration_submission';
    $submission_file = "$dir/$scenario.yml";
    $submission = Yaml::parseFile($submission_file, YAML::PARSE_OBJECT_FOR_MAP);

    $committee_name = $submission->committee->name;
    $committee_type = $submission->committee->committee_type;
    $pac_type = $submission->committee->pac_type;
    $election_code =  $submission->committee->election_code ?? NULL;

    // Manufacture committee
    $committee = new Committee($committee_name, $committee_type);
    $committee->election_code = $election_code;
    $committee->pac_type = $pac_type;
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    // Manufacture registration record
    $draft = new CommitteeRegistrationDraft($committee->committee_id);
    $draft->setRegistrationData($submission);
    $this->dm->em->persist($draft);
    $this->dm->em->flush();

    $this->committee = $committee;
    $submission->committee->committee_id = $committee->committee_id;
    return $submission;
  }

  /**
   * @throws ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  function createPersonAndCandidacy($election_code, $jurisdiction_id, $office_code, $campaign_start_date = '2020-05-11', $filer_id = null) {

    // Person record
    $this->entity = new Entity();
    $this->entity->name = "Test Person";
    $this->entity->address = "1234 somestreet";
    $this->entity->city = "SomeTown";
    $this->entity->state = "ZZ";
    $this->entity->is_person = true;
    $this->entity->postcode = "99999";
    if ($filer_id) {
      $this->entity->filer_id = $filer_id;
    }

    $this->dm->em->persist($this->entity);
    $this->dm->em->flush();

    // Candidate record
    $this->candidacy =  new Candidacy();
    $this->candidacy->person_id = $this->entity->entity_id;
    $this->candidacy->election_code = $election_code;
    $this->candidacy->jurisdiction_id = $jurisdiction_id;
    $this->candidacy->office_code = $office_code;
    $this->candidacy->campaign_start_date = new DateTime($campaign_start_date);
    $this->dm->em->persist($this->candidacy);
    $this->dm->em->flush();
  }

  /**
   * @throws \Exception
   */
  public function testSaveAdminData() {
    $this->generateCommitteeFromScenario('candidate');
    $submission = $this->registrationManager->getRegistrationSubmission($this->registration->registration_id);
    $submission->admin_data->committee->filer_id = "foo";
    $this->registrationManager->saveAdminData($submission);
    $registration = $this->registrationManager->getRegistrationSubmission($submission->registration_id);
    $this->assertNotEmpty($registration->admin_data);
  }

  /**
   * @throws \Exception
   */
  function testUnverifyRegistration(){
    $user_name = 'this is a test';
    $this->populate_committee = TRUE;
    $this->generateCommitteeFromScenario('candidate');
    // candidacy should exist
    $sql = 'select * from candidacy where committee_id = :committee_id';
    $candidacy = $this->dm->db->executeQuery($sql, ['committee_id' => $this->committee->committee_id])->fetchAssociative();
    $this->assertNotEmpty($candidacy);

    $r = $this->dm->em->find(Registration::class, $this->registration->registration_id);
    $r->verified = true;
    $this->dm->em->flush($r);
    $this->assertEquals($this->registration->registration_id, $r->registration_id);
    $this->assertEquals(true, $r->verified);
    $this->registrationManager->unverifyRegistration($this->registration->registration_id, true, $user_name);
    $newr = $this->dm->em->find(Registration::class, $r->registration_id);
    $committee = $this->dm->em->find(Committee::class, $r->committee_id);
    $this->assertEquals(false, $newr->verified);
    $this->assertEmpty($committee->filer_id);
    $this->assertFalse($committee->autoverify);
    $this->assertEmpty($committee->person_id);
    $this->assertEmpty($newr->admin_data);

    // candidacy should get deleted (unless it's an sos candidacy)
    $sql = 'select candidacy_id from candidacy where candidacy_id = :candidacy_id';
    $candidacy_deleted = $this->dm->db->executeQuery($sql, ['candidacy_id' => $candidacy['candidacy_id']])->fetchOne();
    $this->assertEmpty($candidacy_deleted);
  }

  public function verificationOptionsProvider() {

    return [
      ['candidate'],
      ['candidate-2file'],
      ['candidate-mergeable'],
      ['candidate-mergeable-mini-to-full'],
      ['candidate-no-match'],
      ['committee-single-year'],
      ['committee-continuing-mergeable'],
      ['committee-mergeable'],
      ['committee-mergeable-mini-to-full'],
      ['committee-continuing-to-single'],
      ['committee-single-to-continuing'],
      ['committee-no-match'],
      ['committee-two-file'],
      ['surplus-mergeable'],
      ['surplus'],
    ];
  }

  /**
   * @dataProvider verificationOptionsProvider
   * @param $scenario
   * @throws ORMException
   */
  public function testGetVerificationOptions($scenario)
  {

    $admin_data = new stdClass();

    $submission = $this->generateCommitteeFromScenario($scenario, NULL, null, '/data/verification');
    $this->generateEntity($submission->entity ?? null);
    $outcomes = $submission->outcomes;

    $committee_id = null;
    if (!empty($submission->existing_committee)) {
      [$committee] = $this->generateVerifiedCommittee($submission->existing_committee);
      $committee_id = $committee->committee_id;
    }

    // Setting up for merge case
    if ($outcomes->mergeable ?? false === true) {
      $admin_data->entity = $this->entity->entity_id;
      $outcomes->committee_id = $committee_id;
    }

    $admin_data->entity_id = !empty($this->entity->entity_id) ? $this->entity->entity_id : null;
    $admin_data->registration_id = $this->registration->registration_id;

    $options = $this->registrationManager->getVerificationOptions($admin_data);

    $this->assertEquals($outcomes->mergeable ?? false, $options['mergeable'], $scenario);
    if (!empty($outcomes->mergeable)) {
      $this->assertEquals($outcomes->committee_id, $options['committee_id'] ?? null, $scenario);
    }

    $this->assertEquals($outcomes->needs_2_file ?? false, $options['needs_2_file'] ?? false, $scenario);
    if (!empty($this->outcomes->entity_id)) {
        $this->assertEquals($this->entity->entity_id, $options['entity_id'], $scenario);
    }
    if (!empty($outcomes->continuing)) {
      $this->assertEquals($outcomes->continuing, $options['continuing'] ?? null  , $scenario);
    }
    if (!empty($outcomes->continuing_end)) {
      $this->assertEquals($outcomes->continuing_end, $options['continuing_end'] ?? null  , $scenario);
    }
    if (!empty($outcomes->error)) {
      $this->assertEquals($outcomes->error, $options['error'] ?? NULL, $scenario);
    }
    $this->assertEquals($outcomes->filer_id, $options['filer_id'], $scenario);
  }


  /**
   * @throws \Exception
   */
  function testVerifyCandidacyDuplicatesAreNotBeingCreated() {
    $submission = $this->generateCommitteeFromScenario('candidate');

    $this->registrationManager->verifyRegistration($this->registration->registration_id);

    $submission->committee->name = 'Ignored registration';
    $registration = new Registration();
    $registration->committee_id = $this->committee->committee_id;
    $registration->user_data = json_encode($submission);
    $registration->submitted_at = new DateTime('now - 1 day');
    $registration->certification_email = "david.metzler@pdc.wa.gov";
    $registration->username = 'test@noreply.com';
    $this->dm->em->persist($registration);
    $this->dm->em->flush();

    $this->committee->registration_id = $registration->registration_id;
    $this->dm->em->persist($registration);
    $this->dm->em->flush();

    $this->registrationManager->verifyRegistration($registration->registration_id);

    // did the mailer send the email
    /** @var MockMailer $m */
    $m = $this->mailer->getMailSentTo("david.metzler@pdc.wa.gov");
    $this->assertEquals('d-f976dc73795248f3aabeed0e631e25a2', $m->message_template);
    $this->assertEquals($this->registration->certification_email, $this->mailer->mails_sent[0]->email);


    $candidacies = $this->dm->em->getRepository(Candidacy::class);
    $candidaciesCount = $candidacies->findBy(['committee_id' => $this->committee->committee_id]);

    $this->assertCount(1, $candidaciesCount);

  }

  public function pacScenarios() {
    return [
      ['political-committee', 'Continuing Committee'],
      ['political-committee-party-set', 'Continuing Committee'],
      ['bonafide-exempt', 'Republican Leg District Party'],
      ['issue-committee', 'Statewide Ballot Measure'],
      ['political-committee-jurisdiction', 'Education'],
      ['slate-committee-proposals', 'Single Election Committee'],
    ];
  }

  /**
   * @throws OptimisticLockException
   */
  function testVerifyRegistrationMiniToFullWithoutPermission() {
    $this->generateCommitteeFromScenario('political-committee');
    $this->registration->reporting_type = 'full';
    $this->dm->em->flush($this->registration);
    $this->registration->certification_email = "david.metzler@pdc.wa.gov";

    try {
      $this->registrationManager->verifyRegistration($this->registration->registration_id);
      $this->assertFalse(true, 'Should have raised exception');
    } catch (Exception $e) {
      $this->assertEquals('This is a mini to full reporting change without permission, which is highly unexpected. Please contact IT.', $e->getMessage());
    }
  }

  /**
   * @throws ORMException
   * @throws \Exception
   */
  function testVerifyOldRegistration() {
    // Start with a new
    $submission = $this->generateCommitteeFromScenario('political-committee', 'A Different Name');

    // Simulate a registration that is in the past and not the current registration id on the committee
    $submission->committee->name = 'Ignored registration';
    $registration = new Registration();
    $registration->committee_id = $this->committee->committee_id;
    $registration->user_data = json_encode($submission);
    $registration->submitted_at = new DateTime('now - 1 day');
    $registration->certification_email = "david.metzler@pdc.wa.gov";
    $this->dm->em->persist($registration);
    $this->dm->em->flush();

    $this->registrationManager->verifyRegistration($registration->registration_id);

    // Make sure that we didn't change the registration data but rather re-used the registration
    // data from the current registration.
    $committee = $this->dm->em->find(Committee::class, $this->registration->committee_id);
    $this->assertNotEquals($committee->name, $submission->committee->name);

    // look up the first registration to make sure it is verified.
    $verified_registration = $this->dm->em->find(Registration::class, $registration->registration_id);
    $this->assertTrue($verified_registration->verified);
  }

  /**
   * @throws \Exception
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testGetAutoVerifyEligibility(){
    //make sure that a candidate without a filer_id can't auto-verify.
    $submission = $this->generateCommitteeFromScenario('candidate');
    $registration_id = $this->registration->registration_id;
    $canVerify = $this->registrationManager->getAutoVerifyEligibility($registration_id);
    $this->assertEquals(false, $canVerify);

    //make sure that a candidate with a filer_id, but no candidacy can't auto-verify.
    $committee = $this->dm->em->find(Committee::class, $submission->committee->committee_id);
    $committee_id = $committee->committee_id;
    $committee->filer_id =  'TEST--000';
    $this->dm->em->flush($committee);
    $canVerify = $this->registrationManager->getAutoverifyEligibility($registration_id);
    $this->assertEquals(false, $canVerify);

    //make sure that a candidate with a filer_id and a candidacy with matching jurisdiction/office on record can auto-verify.
    $this->registrationManager->verifyRegistration($registration_id);
    [,$candidacy] = $this->getCandidacyAndPersonByCommittee($this->committee->committee_id);
    $this->generateNewRegistrationForCommittee($submission, $committee_id);
    $this->assertNotEmpty($candidacy->filer_id);
    $this->assertEquals($submission->committee->candidacy->jurisdiction_id, $candidacy->jurisdiction_id);
    $canVerify = $this->registrationManager->getAutoVerifyEligibility($this->registration->registration_id);
    $this->assertEquals(true, $canVerify);

    //make sure that a candidate with a filer_id and a candidacy with a mis-matched office is not autoverified.
    $submission->committee->candidacy->office = 'DISTRICT COURT JUDGE';
    $this->generateNewRegistrationForCommittee($submission, $committee_id);
    $this->assertNotEmpty($submission->committee->filer_id);
    $this->assertNotEmpty($candidacy->filer_id);
    $committee->registration_id = $registration_id;
    $this->registrationManager->verifyRegistration($registration_id);
    $canVerify = $this->registrationManager->getAutoVerifyEligibility($this->registration->registration_id);
    $this->assertEquals(false, $canVerify);

    //make sure that a candidate with a filer_id and candidacy with a mis-matched jurisdiction cannot auto-verify.
    $submission->committee->candidacy->jurisdiction = 'COWLITZ CO DISTRICT COURT';
    $this->generateNewRegistrationForCommittee($submission, $committee_id);
    $this->assertNotEmpty($candidacy->filer_id);
    $this->assertEquals($submission->committee->candidacy->jurisdiction_id, $candidacy->jurisdiction_id);
    $committee->registration_id = $registration_id;
    $this->registrationManager->verifyRegistration($registration_id);
    $canVerify = $this->registrationManager->getAutoVerifyEligibility($this->registration->registration_id);
    $this->assertEquals(false, $canVerify);

    //make sure a committee without a filer_id can't auto-verify
    $submission = $this->generateCommitteeFromScenario('political-committee');
    $this->assertEmpty($this->committee->filer_id);
    $canVerify = $this->registrationManager->getAutoVerifyEligibility($this->registration->registration_id);
    $this->assertEquals(false, $canVerify);

    //  Make sure a committee with a filer_id can auto-verify
    $committee_id = $submission->committee->committee_id;
    $this->registrationManager->verifyRegistration($this->registration->registration_id);
    [$committee] = $this->getCandidacyAndPersonByCommittee($committee_id);
    $this->generateNewRegistrationForCommittee($submission, $committee_id);
    $this->assertNotEmpty($committee->filer_id);
    $canVerify = $this->registrationManager->getAutoVerifyEligibility($this->registration->registration_id);
    $this->assertEquals(true, $canVerify);

    //make sure that a committee with a mini->full reporting type can't auto-verify
    $submission->committee->reporting_type = "full";
    $this->registrationManager->verifyRegistration($registration_id);
    $this->generateNewRegistrationForCommittee($submission, $committee_id);
    $this->assertTrue($committee->reporting_type === 'mini');
    $canVerify = $this->registrationManager->getAutoVerifyEligibility($this->registration->registration_id);
    $this->assertEquals(false, $canVerify);

    // verifying a committee should set it autoverify eligibility
    $committee = $this->dm->em->find(Committee::class, $committee_id);
    $this->assertEquals(true, $committee->autoverify);
    // unverifying a committee should discard its autoverify eligibility
    $this->registrationManager->unverifyRegistration($committee->registration_id, false, 'ben.livingston@pdc.wa.gov');
    $canVerify = $this->registrationManager->getAutoVerifyEligibility($committee->registration_id);
    $this->assertEquals(false, $canVerify);

  }

  /**
   * @throws \Exception
   */
  public function testAutoVerifySubmission(){
    $uname = 'test';
    $email = 'test@example.com';
    //new submission should not be auto-verified...
    $submission = $this->generateDraftFromSubmission('candidate');
    $submission->submitted_at = "";
    $this->assertNotEmpty($submission->committee->contact->email);
    $submission->certification_email = $email;
    $registration_id = $this->registrationManager->createRegistrationFromSubmission($submission, $email, $uname);
    $this->dm->em->clear();
    $registration = $this->dm->em->find(Registration::class, $registration_id);
    $this->assertEquals(false, $registration->verified);
    $this->assertEquals($submission->committee->reporting_type, $registration->reporting_type);

    //create committee that should be auto-verified...
    $submission = $this->generateCommitteeFromScenario('candidate');
    $submission->submitted_at = "";
    $registration_id = $this->registration->registration_id;
    $committee = $this->dm->em->find(Committee::class, $submission->committee->committee_id);
    $committee->filer_id =  'TEST--002';
    $this->dm->em->flush($committee);
    $this->dm->em->persist($committee);
    $this->registrationManager->verifyRegistration($registration_id);

    //create new submission for committee...
    $draft = new CommitteeRegistrationDraft($committee->committee_id);
    $draft->setRegistrationData($submission);
    // Changing the office code so that we can verify that the candidacy was updated.
    $submission->committee->candidacy->offcode="04";
    $submission->committee->candidacy->office = "LIEUTENANT GOVERNOR";
    $this->dm->em->persist($draft);
    $this->dm->em->flush();
    $this->dm->em->clear();
    $registration_id = $this->registrationManager->createRegistrationFromSubmission($submission, $email, $uname);
    $registration = $this->dm->em->find(Registration::class, $registration_id);
    $this->assertEquals(false, $registration->verified);
    $this->assertEquals($submission->committee->reporting_type, $registration->reporting_type);

    // Check to make sure that the candidacy was modified by auto-verification
    $candidacy = (object) $this->dm->db->executeQuery("SELECT * FROM CANDIDACY where committee_id=:committee_id",
      [
        'committee_id' => $this->committee->committee_id
      ])
      ->fetchAllAssociative()[0];
    $this->assertNotEquals($submission->committee->candidacy->offcode, $candidacy->office_code);
  }

  public function testGetSubmissionDecoder() {
    $reg = new Registration();
    $reg->source = 'ORCA';
    $reg->user_data = '<?xml version="1.0"?>';

    $decoder = $this->registrationManager->getSubmissionDecoder($reg);

    $this->assertInstanceOf(RegistrationLegacyDecoder::class, $decoder);

    $reg->source = 'foo';
    $reg->user_data = "{}";

    $decoder = $this->registrationManager->getSubmissionDecoder($reg);

    $this->assertInstanceOf(RegistrationDecoder::class, $decoder);
  }

  /**
   * @throws \Exception
   */
  public function testGetRegistrationSubmission(){
    $submission = $this->generateCommitteeFromScenario('political-committee');
    $committee= $this->dm->em->find(Committee::class, $submission->committee->committee_id);
    $submission = $this->registrationManager->getRegistrationSubmission($committee->registration_id);
    $this->assertNotEmpty($submission->committee->committee_id);
  }

  /**
   * @throws \Exception
   */
  public function testCreateRegistrationFromSubmission(){
    $user_name = 'u_name';

    $this->dm->db->executeStatement("delete from messenger_contact where target in ('ben.livingston@pdc.wa.gov', 'david.metzler@pdc.wa.gov')");
    $submission = $this->generateDraftFromSubmission('political-committee');
    $submission->submitted_at = "";
    $this->assertNotEmpty($submission->committee->contact->email);
    $certification_email = 'ben.livingston@pdc.wa.gov';
    $submission->certification_email = $certification_email;
    $registration_id = $this->registrationManager->createRegistrationFromSubmission($submission, $certification_email, $user_name);

    $this->dm->em->clear();
    /** @var Registration $registration */
    $registration = $this->dm->em->Find(Registration::class, $registration_id);
    $committee_id = $registration->committee_id;
    $s = json_decode($registration->user_data);
    $this->assertInstanceOf(stdClass::class, $s, 'encoded json data in submission');
    $draft = $this->dm->em->find(CommitteeRegistrationDraft::class, $committee_id);
    $this->assertEmpty($draft);
    $this->assertNotEmpty($committee_id);
    $updatedCommittee = $this->dm->em->Find(Committee::class, $committee_id);
    $this->assertNotEmpty($updatedCommittee);
    $this->assertEquals($registration_id, $updatedCommittee->registration_id);
    $this->assertEquals(false, $registration->verified);
    $this->assertEquals($submission->committee->reporting_type, $registration->reporting_type);

    // did the mailer send the email
    /** @var MockMailer $m */
    $m = $this->mailer->getMailSentTo("david.metzler@pdc.wa.gov");
    $this->assertEquals('d-37776fb5108e43618bf3af03c8b6edfd', $m->message_template);
    $m = $this->mailer->getMailSentTo("ben.livingston@pdc.wa.gov");
    $this->assertEquals('d-37776fb5108e43618bf3af03c8b6edfd', $m->message_template);

    // Make sure we get an exception if submitted_at date is not valid.
    $this->expectException(Exception::class);
    $submission->submitted_at = 'puppies';
    $this->registrationManager->createRegistrationFromSubmission($submission, $certification_email, $user_name);
  }

  /**
   * @throws \Exception
   */
  public function testLogRegistrationAction() {
    $user_name = 'test_user';
    $action = 'test action';
    $message = 'test message';
    $file = dirname(dirname(__DIR__ )) . '/data/registration_submission/candidate.yml';
    $registration= $this->generateCommitteeWithRegistration('TEST COMMITTEE', 'CA', file_get_contents($file));
    $this->registrationManager->logRegistrationAction($registration->registration_id, $user_name, $action, $message);
    $rows = $this->dm->db->fetchAllAssociative("SELECT * from committee_log WHERE committee_id = :committee_id", ['committee_id' => $registration->committee_id]);
    $this->assertCount(1, $rows);
    $log = reset($rows);
    $this->assertEquals($registration->registration_id, $log['transaction_id']);
    $this->assertEquals($user_name, $log['user_name']);
    $this->assertEquals($action, $log['action']);
    $this->assertEquals($message, $log['message']);
    $this->assertEquals('registration', $log['transaction_type']);
    $this->assertNotEmpty($log['updated_at']);
  }

  /**
   * @throws \Exception
   */
  public function testGetCurrentRegistrationJSONByToken(){
    $dir = dirname(dirname(__DIR__))  . "/data/registration_submission";
    $submission = Yaml::parseFile("$dir/political-committee.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Save a committee as a submission
    $this->committeeManager->saveCommitteeFromSubmission($submission);

    $token = $this->committeeManager->generateAPIToken($submission->committee->committee_id, 'test', 'testUser');
    $data = $this->registrationManager->getCurrentRegistrationJSONByToken($token);
    $this->assertNotEmpty($data);
    $this->assertEquals($submission->committee->name, $data->committee->name);
  }

  /**
   * @throws \Exception
   */
  public function testRevertRegistration() {
    $this->generateCommitteeFromScenario('political-committee', 'TEST Committee');
    $user = new User();
    $user->user_name = 'test user';
    $user->realm = 'test';

    $data = new stdClass();
    $data->registration_id = $this->registration->registration_id;

    $expected_pac_type = 'bonafide';
    $expected_bonafide_type = 'state';
    $expected_exempt = false;
    $expected_committee_type = 'CO';
    $expected_election_code = null;
    $expected_end_year = null;
    $expected_start_year = (new DateTime())->format('Y');
    $expected_continuing = true;

    $data->election_code = $expected_election_code;
    $data->pac_type = $expected_pac_type;
    $data->bonafide_type = $expected_bonafide_type;
    $data->exempt = $expected_exempt;

    $this->registrationManager->revertRegistration($data, $user);

    $this->dm->em->clear(Registration::class);

    $r = $this->dm->em->find( Registration::class, $data->registration_id);
    $this->assertEmpty($r);

    $draft = $this->registrationDraftManager->getCommitteeRegistrationDraft($this->registration->committee_id);

    /** @var Committee $c */
    $c = $this->dm->em->find(Committee::class, $this->registration->committee_id);
    $this->assertEmpty($c->registration_id);

    // Check to make sure we properly change values
    $this->assertEquals($expected_election_code, $c->election_code);
    $this->assertEquals($expected_start_year, $c->start_year);
    $this->assertEquals($expected_end_year, $c->end_year);
    $this->assertEquals($expected_pac_type, $c->pac_type);
    $this->assertEquals($expected_bonafide_type, $c->bonafide_type);
    $this->assertEquals($expected_exempt, $c->exempt);
    $this->assertEquals($expected_committee_type, $c->committee_type);
    $this->assertEquals($expected_continuing, $c->continuing);

    // Check to make sure we properly change committee type
    $this->assertEquals($expected_election_code, $draft->committee->election_code);
    $this->assertEquals($expected_start_year, $draft->committee->start_year);
    $this->assertEquals($expected_end_year, $draft->committee->end_year);
    $this->assertEquals($expected_pac_type, $draft->committee->pac_type);
    $this->assertEquals($expected_bonafide_type, $draft->committee->bonafide_type);
    $this->assertEquals($expected_exempt, $draft->committee->exempt);
    $this->assertEquals($expected_committee_type, $draft->committee->committee_type);
    $this->assertEquals($expected_continuing, $draft->committee->continuing);
    $this->assertFalse(isset($draft->committee->candidacy));
  }

  /**
   * @throws /Exception
   */
  public function testRevertToCandidacy(){
    $this->generateCommitteeFromScenario('political-committee', 'TEST Committee');

    $user = new User();
    $user->user_name = 'test user';
    $user->realm = 'test';

    $data = new stdClass();
    $data->registration_id = $this->registration->registration_id;

    $expected_pac_type = 'candidate';
    $expected_bonafide_type = null;
    $expected_exempt = null;
    $expected_committee_type = 'CA';
    $expected_election_code = '2019';
    $expected_end_year = 2019;
    $expected_start_year = 2019;
    $expected_continuing = false;

    $data->election_code = $expected_election_code;
    $data->pac_type = $expected_pac_type;
    $data->bonafide_type = $expected_bonafide_type;
    $data->exempt = $expected_exempt;

    $this->registrationManager->revertRegistration($data, $user);
    $this->dm->em->clear(Registration::class);

    $r = $this->dm->em->find( Registration::class, $data->registration_id);
    $this->assertEmpty($r);

    $draft = $this->registrationDraftManager->getCommitteeRegistrationDraft($this->registration->committee_id);

    /** @var Committee $c */
    $c = $this->dm->em->find(Committee::class, $this->registration->committee_id);
    $this->assertEmpty($c->registration_id);

    // Check to make sure we properly change values
    $this->assertEquals($expected_election_code, $c->election_code);
    $this->assertEquals($expected_start_year, $c->start_year);
    $this->assertEquals($expected_end_year, $c->end_year);
    $this->assertEquals($expected_pac_type, $c->pac_type);
    $this->assertEquals($expected_bonafide_type, $c->bonafide_type);
    $this->assertEquals($expected_exempt, $c->exempt);
    $this->assertEquals($expected_committee_type, $c->committee_type);
    $this->assertEquals($expected_continuing, $c->continuing);

    // Check to make sure we properly change committee type
    $this->assertEquals($expected_election_code, $draft->committee->election_code);
    $this->assertEquals($expected_start_year, $draft->committee->start_year);
    $this->assertEquals($expected_end_year, $draft->committee->end_year);
    $this->assertEquals($expected_pac_type, $draft->committee->pac_type);
    $this->assertEquals($expected_bonafide_type, $draft->committee->bonafide_type);
    $this->assertEquals($expected_exempt, $draft->committee->exempt);
    $this->assertEquals($expected_committee_type, $draft->committee->committee_type);
    $this->assertEquals($expected_continuing, $draft->committee->continuing);
    $this->assertNotEmpty($draft->committee->candidacy);
    $this->assertFalse(isset($draft->committee->proposals));
    $this->assertFalse(isset($draft->committee->affiliations));
  }

  /**
   * @throws \Exception
   */
  public function testConvertSingleElectionCommitteeToContinuing() {
    $email = 'nope@example.com';
    $user = 'you';
    //create initial single year committee.
    $submission = $this->generateCommitteeFromScenario('issue-committee');
    $registration_id = $this->registration->registration_id;
    $committee = $this->registrationManager->verifyRegistration($registration_id);
    $this->assertEquals($submission->committee->end_year, $committee->end_year);
    $this->assertEquals(false, $committee->continuing);

    //make new submission for continuing
    $submission->committee->continuing = true;
    $draft = new CommitteeRegistrationDraft($committee->committee_id);
    $draft->setRegistrationData($submission);
    $this->dm->em->persist($draft);
    $this->dm->em->flush();
    //create new registration
    $registration_id = $this->registrationManager->createRegistrationFromSubmission($submission, $email, $user);
    $new_committee = $this->registrationManager->verifyRegistration($registration_id);
    //make sure the committee gets updated with the new end_year from the registration
    $this->assertEquals($committee->end_year, $new_committee->end_year);
    $this->assertEquals(true, $committee->continuing);
  }

  /**
   * @throws \Exception
   */
  public function testAttachment(){
    $this->generateCommitteeFromScenario('candidate');
    $input_data = new stdClass();
    $input_data->registration_id = $this->registration->registration_id;
    $input_data->file_name = 'mail in submission.pdf';
    $input_data->file_service_url = 'https://a.file.url/mail%20in%20submission.pdf';
    $input_data->access_mode = 'public';
    $input_data->description = 'mailed in on the 20th';
    $input_data->document_type = 'C1 Registration';
    $this->registrationManager->addRegistrationAttachment($input_data);

    $alias = $input_data->registration_id . '/' . $input_data->file_name;
    /** @var RegistrationAttachment $attachment */
    $attachment = $this->dm->em->find(RegistrationAttachment::class, $alias);
    $this->assertInstanceOf(RegistrationAttachment::class, $attachment);
    $this->assertSame($input_data->registration_id, $attachment->registration_id);
    $this->assertSame($alias, $attachment->alias);
    $this->assertSame($input_data->file_service_url, $attachment->file_service_url);
    $this->assertSame($input_data->description, $attachment->description);
    $this->assertSame($input_data->document_type, $attachment->document_type);
    $this->dm->em->clear();

    $input_data->description = 'mail in submission correction.pdf';
    $input_data->document_type = 'Initial C1 Submission';

    $this->registrationManager->modifyRegistrationAttachment($input_data);
    $modified_attachment = $this->dm->em->find(RegistrationAttachment::class, $alias);
    $this->assertInstanceOf(RegistrationAttachment::class, $modified_attachment);
    $this->dm->em->clear();

    /** @var RegistrationAttachment $attachment */
    $attachment_after_modified = $this->dm->em->getRepository(RegistrationAttachment::class)->find($alias);
    $this->assertInstanceOf(RegistrationAttachment::class, $attachment_after_modified);
    $this->assertSame($input_data->registration_id, $attachment_after_modified->registration_id);
    $this->assertSame($alias, $attachment_after_modified->alias);
    $this->assertSame($input_data->file_service_url, $attachment_after_modified->file_service_url);
    $this->assertSame($input_data->description, $attachment_after_modified->description);
    $this->assertSame($input_data->document_type, $attachment_after_modified->document_type);

    $this->registrationManager->deleteRegistrationAttachment($input_data);
    $deleted_attachment = $this->dm->em->find(RegistrationAttachment::class, $alias);
    $this->assertNull($deleted_attachment);
  }

  function testPositionChangeAmendment() {
    $submission = $this->generateCommitteeFromScenario('candidate-np');
    // create new position
    $current_position = $this->dm->em->find(Position::class, $submission->committee->candidacy->position_id);
    $new_position = new Position();
    $new_position->title = 'Test Position 2';
    $new_position->jurisdiction_office_id = $current_position->jurisdiction_office_id;
    $this->dm->em->persist($new_position);
    $this->dm->em->flush();
    $submission->committee->candidacy->position_id = $new_position->position_id;
    // verify new registration
    $r = $this->generateNewRegistrationForCommittee($submission, $submission->committee->committee_id);
    $this->verifyReg($r->registration_id);
    $this->dm->em->clear();
    // candidacy should reflect the new position
    $candidacy = $this->dm->em->getRepository(Candidacy::class)->findOneBy(['committee_id' => $submission->committee->committee_id]);
    $this->assertEquals($new_position->position_id, $candidacy->position_id);
  }

  public function verifyProvider() {
    return [
      ['candidate'],
      ['candidate-2file'],
      ['candidate-no-match'],
      ['committee-no-match'],
      ['committee-single-year'],
      ['bonafide-exempt'],
      ['committee-education'],
      ['committee-continuing-to-single'],
      ['committee-slate-proposals'],
      ['surplus'],
      ['surplus-no-match'],
    ];
  }

  /**
   * @dataProvider verifyProvider
   */
  public function testVerify($scenario) {
    $data = $this->loadTestScenario($scenario, 'verification');

    // Create entity and any pre-existing committees and candidacies
    $this->generateEntity($data->entity ?? null);
    if (!empty($data->existing_committee)) {
      $this->generateVerifiedCommittee($data->existing_committee);
    }

    $admin_data = new stdClass();
    $admin_data->entity_id = !empty($data->outcomes->entity_id) ? $this->entity->entity_id  : null;
    $admin_data->filer_id = $data->outcomes->filer_id ?? null;

    // Create the registration and committee that needs verifying
    $this->generateCommitteeRegistration($data, $admin_data, true);

    // Verify the registration.
    $this->registrationManager->verifyRegistration($this->registration->registration_id, false, true, 'test@noreply.com');

    // did the mailer send the email
    /** @var stdClass $m */
    $m = $this->mailer->getMailSentTo("test@noreply.com");
    $this->assertEquals('d-f976dc73795248f3aabeed0e631e25a2', $m->message_template);
    $this->assertEquals($this->registration->certification_email, $this->mailer->mails_sent[0]->email);
    $this->assertEquals($data->committee->name, $m->data['committee_name']);
    $this->assertEquals($data->committee->reporting_type, $m->data['reporting_type']);
    $this->assertEquals($data->committee->pac_type, $m->data['pac_type']);

    // Verify that the committee was properly verified.
    /** @var Committee $committee */
    $committee = $this->dm->em->find(Committee::class, $this->registration->committee_id);
    $this->assertEquals($committee->name, $data->committee->name);

    // Make sure committee category is correct.
    if (!empty($data->verification->category)) {
      $this->assertEquals($data->verification->category, $committee->category);
    }

    // Correct filer id and entity
    if ($data->outcomes->needs_2_file ?? null ) {
      $this->assertEquals($this->entity->entity_id, $committee->person_id, $scenario);
      $this->assertNotEquals($this->entity->filer_id, $committee->filer_id, $scenario);
    }
    elseif (!empty($data->outcomes->entity_id)) {
      $this->assertEquals($this->entity->entity_id, $committee->person_id, $scenario);
      $this->assertEquals($data->outcomes->filer_id, $committee->filer_id, $scenario);
    }
    else {
      $this->assertNotEmpty($committee->person_id);
      $this->assertNotEmpty($committee->filer_id);
    }

    //Prior continuing committee is ended.
    if (!empty($data->outcomes->continuing_end)) {
      $end_year = $this->dm->db->fetchOne("SELECT end_year from committee where person_id = :entity_id and continuing", ['entity_id' => $committee->person_id]);
      $this->assertEquals(null, $end_year );
    }

    if ($committee->pac_type === 'pac') {
      $entity = $this->dm->em->find(Entity::class, $committee->person_id);
      $committee_contact = $this->dm->em->getRepository(CommitteeContact::class)
        ->findOneBy(['committee_id' => $committee->committee_id, 'role' => 'committee']);
      $this->assertEquals($entity->entity_id, $committee->person_id, $scenario);
      if (empty($data->outcomes->entity_id)) {
        $this->assertEquals($entity->name, $committee->name, $scenario);
        $this->assertEquals($entity->address, $committee_contact->address, $scenario);
        $this->assertEquals($entity->email, $committee_contact->email, $scenario);
        $this->assertEquals($entity->state, $committee_contact->state, $scenario);
        $this->assertEquals($entity->postcode, $committee_contact->postcode, $scenario);
        $this->assertEquals($entity->city, $committee_contact->city, $scenario);
      }
    }
    elseif ($committee->pac_type == 'candidate') {
      // The verification process should have created a new candidacy record with the committee on it.
      $candidacies = $this->dm->em->getRepository(Candidacy::class);
      /** @var Candidacy $savedCandidacy */
      $savedCandidacy = $candidacies->findOneBy(['committee_id' => $this->committee->committee_id]);
      $candidacy = $data->committee->candidacy;
      if (!empty($data->outcomes->entity_id)) {
        $this->assertEquals($this->entity->entity_id, $savedCandidacy->person_id);
      }
      else {
        $this->assertNotEmpty($savedCandidacy->person_id);
      }
      $this->assertEquals(trim($data->committee->contact->address), $savedCandidacy->address);
      $this->assertEquals(trim($data->committee->contact->city), $savedCandidacy->city);
      $this->assertEquals(trim($data->committee->contact->state), $savedCandidacy->state);
      $this->assertEquals(trim($data->committee->contact->postcode), $savedCandidacy->postcode);
      $this->assertEquals(trim($data->committee->candidacy->ballot_name), $savedCandidacy->ballot_name);
      $this->assertEquals($data->committee->candidacy->office_code, $savedCandidacy->office_code);
      $this->assertEquals($data->committee->candidacy->jurisdiction_id, $savedCandidacy->jurisdiction_id);
      $this->assertEquals($data->committee->election_code, $savedCandidacy->election_code);
      $this->assertEquals('Candidate', $committee->category);

      // check that committee_contact role=candidacy has name
      $candidacy_contact = $this->dm->em->getRepository(CommitteeContact::class)->findOneBy(['committee_id' => $this->committee->committee_id, 'role' => 'candidacy']);
      $this->assertNotEmpty($candidacy_contact->name);
      $this->assertEquals(trim($candidacy->ballot_name), $candidacy_contact->name);
    }
  }

}
