<?php

namespace Tests\WAPDC\CampaignFinance\Registration;

use DateTime;
use Exception;
use SimpleXMLElement;
use stdClass;
use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\Model\Committee;
use WAPDC\CampaignFinance\Model\CommitteeContact;
use WAPDC\CampaignFinance\Model\CommitteeRelation;
use WAPDC\CampaignFinance\Model\Registration;
use WAPDC\CampaignFinance\Model\CommitteeAuthorization;
use WAPDC\CampaignFinance\Model\CommitteeAPIAuthorization;
use WAPDC\Core\EntityProcessor;
use WAPDC\Core\Model\Candidacy;
use WAPDC\Core\Model\Entity;
use WAPDC\Core\Model\Person;
use WAPDC\Core\Model\User;
use WAPDC\Core\PersonProcessor;


/**
 * Class CommitteeManagerTest
 * @package Tests\WAPDC\CampaignFinance
 */
class CommitteeManagerTest extends CommitteeTestCase {

  /**
   * @var CommitteeManager
   */
  private $committeeManager;
  private $committee_id;
  /** @var User */
  private $user;

  public $processor;

  /**
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->committeeManager = new CommitteeManager($this->dm);

    $this->committee = $c = new Committee("People for Making Tests");
    $this->dm->em->persist($this->committee);
    $this->committee_id = $c->committee_id;
    $this->user = $u = new User();
    $u->realm = 'test.pdc,wa.gov';
    $u->uid = 999;
    $u->user_name = 'u_name';
    $this->dm->em->persist($this->user);
    $this->dm->em->flush();
    $this->dm->em->clear();
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

  /**
   * @throws Exception
   */
  public function testCommitteeAuthorization() {
    // Create entity
    $entity = new stdClass();
    $entity->name = 'Test Committee';
    $entity->is_person = false;
    $entityProcessor = new EntityProcessor($this->dm);
    $entity_id = $entityProcessor->saveEntity($entity);
    $committee = new Committee ( 'Test Committee');
    $committee->person_id = $entity_id;
    $committee->committee_type = 'CO';
    $committee->election_code = '2018';
    $this->dm->em->persist($committee);
    $this->dm->em->flush();
    //@TODO: These should probably be refactored to use a first class user object once we have merged that model to master.
    $user = new stdClass();
    $user->user_name = 'testUser';
    $user->realm = 'test.pdc.wa.gov';
    $user->uid = '1';

    $this->assertFalse($this->committeeManager->userHasRole($committee, $user, 'owner'));

    $this->committeeManager->requestAccess($committee, $user, 'owner');

    $this->assertFalse($this->committeeManager->userHasRole($committee, $user, 'owner'), 'Requested access still is denied');

    $expiration_date = new \DateTime('2077-01-01');
    $this->committeeManager->grantAccess($committee, $user, 'owner', $expiration_date);

    $this->assertTrue($this->committeeManager->userHasRole($committee, $user, 'owner'));

    $role = $this->dm->db->executeQuery("select role from entity_authorization where uid=:uid and realm=:realm and entity_id=:entity_id",
      [
        'uid' => $user->uid,
        'realm' => $user->realm,
        'entity_id' => $entity_id,
      ])->fetchOne();
    $this->assertEquals("cf_reporter", $role);

    $this->committeeManager->revokeAllAccess($committee, $user);

    $this->assertFalse($this->committeeManager->userHasRole($committee, $user, 'owner'));

      //Expiration Date
      $expiration_date = new \DateTime('2023-01-01');
      $this->committeeManager->grantAccess($committee, $user, 'owner', $expiration_date);

      $this->assertFalse($this->committeeManager->userHasRole($committee, $user, 'owner'));
      $this->committeeManager->revokeAllAccess($committee, $user);

      //Grant without expiration date
      $this->committeeManager->grantAccess($committee, $user, 'owner');
      $this->assertTrue($this->committeeManager->userHasRole($committee, $user, 'owner'));
  }

  /**
   * @throws Exception
   */
  public function testMetaData(){
    $submission = $this->generateCommitteeFromScenario('candidate');
    $person = new Person();
    $person->name = 'Test person';
    $this->dm->em->persist($person);
    $this->dm->em->flush();
    $candidacy = new Candidacy();
    $candidacy->committee_id = $submission->committee->committee_id;
    $candidacy->general_result = 'L';
    $candidacy->primary_result = 'W';
    $candidacy->office_code = '03';
    $candidacy->jurisdiction_id = 1000;
    $candidacy->election_code = 2018;
    $candidacy->campaign_start_date = new DateTime('2020-05-01');
    $candidacy->person_id = $person->person_id;
    $this->dm->em->persist($candidacy);
    $this->dm->em->flush();
    $this->committeeManager->saveCommitteeFromSubmission($submission);
    $this->dm->em->clear();

    $retrieved_submission = $this->committeeManager->getCommitteeSubmission($submission->committee->committee_id);

    // make sure the submission has metadata on it
    $this->assertObjectHasAttribute('meta_data', $retrieved_submission);
    $this->assertEquals('L', $retrieved_submission->meta_data->candidacy->general_result);
    $this->assertEquals('W', $retrieved_submission->meta_data->candidacy->primary_result);
    $this->assertEquals('03', $retrieved_submission->meta_data->candidacy->office_code);
    $this->assertEquals(1000, $retrieved_submission->meta_data->candidacy->jurisdiction_id);
  }

  public function testMemoRedaction() {
    $committee = new Committee('Non admin committee');
    $committee->memo = 'This is a memo';
    $this->dm->em->persist($committee);
    $this->dm->em->flush();

    $userSubmission = $this->committeeManager->getCommitteeSubmission($committee->committee_id);

    $this->assertNull($userSubmission->committee->memo);

    $adminSubmission = $this->committeeManager->getCommitteeSubmission($committee->committee_id, true);

    $this->assertEquals('This is a memo', $adminSubmission->committee->memo);
  }

  /**
   * @throws Exception
   */
  public function testAPIToken(){
    $committee = new Committee ( 'Test Committee');
    $committee->committee_type = 'CO';
    $this->dm->em->persist($committee);
    $this->dm->em->flush();
    $memo = 'Test Committee Token';
    $api_key = $this->committeeManager->generateAPIToken($committee->committee_id, $memo, 'test');

    $valid = $this->committeeManager->validateAPIToken($api_key);
    $this->assertEquals($committee->committee_id, $valid);

    $valid = $this->committeeManager->validateAPIToken( 'FooBar');
    $this->assertFalse($valid);

    $tokenId = $this->dm->db->fetchOne('SELECT api_key_id FROM committee_api_key WHERE committee_id = :committee_id', ['committee_id' => $committee->committee_id]);
    $this->committeeManager->revokeAPIToken($tokenId, 'test');

    $valid = $this->committeeManager->validateAPIToken($api_key);
    $this->assertFalse($valid);
  }

  /**
   * @throws Exception
   */
  function testHashingAlgorithm(){
    $expected_hashed_token = "nb1fdGA/A7bqi0PFq2x9bgy0j2hUHMgBLagWLdJTBaI="; // .net generated test hashed key
    $user_token = "thisismytoken"; // key test
    $hmac_key ="thisisthekeythisisthekeythisisthekeythisisthekey";

    $committee = new Committee ( 'Test Committee');
    $committee->committee_type = 'CO';
    $this->dm->em->persist($committee);
    $this->dm->em->flush();
    $memo = 'Test Committee Token';
    $this->committeeManager->generateAPIToken($committee->committee_id, $memo, 'test');

    $_ENV["HMAC_KEY"] = $hmac_key;

    $committee_authorization = $this->dm->em->getRepository(CommitteeAPIAuthorization::class)
      ->findOneBy(["committee_id" => $committee->committee_id]);

    $committee_authorization->hash_value = $expected_hashed_token;
    $this->dm->em->persist($committee_authorization);
    $this->dm->em->flush();

    $valid = $this->committeeManager->validateAPIToken($user_token);
    $this->assertEquals($committee->committee_id, $valid);
  }

  /**
   * @throws \Exception
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function testAuthorizeCommitteesByFilerID() {
    $user = new stdClass();
    $user->realm = 'test.pdc.wa.gov';
    $user->uid = '1';
    $user->user_name = null;

    $committeeOne = new Committee ( 'Test Committee One');
    $committeeOne->committee_type = 'CO';
    $committeeOne->filer_id = 'TESTID1234';
    $this->dm->em->persist($committeeOne);
    $this->dm->em->flush();

    $committeeTwo = new Committee ( 'Test Committee Two');
    $committeeTwo->committee_type = 'CO';
    $committeeTwo->filer_id = 'TESTID1234';
    $this->dm->em->persist($committeeTwo);
    $this->dm->em->flush();

    $result = $this->committeeManager->authorizeCommitteesByFilerID('testid1234', $user);
    $this->assertTrue($this->committeeManager->userHasRole($committeeOne, $user, 'owner'));
    $this->assertTrue($this->committeeManager->userHasRole($committeeTwo, $user, 'owner'));
    $this->assertTrue($result->success);
  }

  /**
   * @throws Exception
   */
  public function testGetRegistrationXml() {
    $committee = new Committee('test Committee', 'CO');
    $this->dm->em->persist($committee);
    $this->dm->em->flush();
    // Set up a sample registration record
    $file = dirname(dirname(__DIR__)) . '/data/candidate.xml';
    $registration = new Registration();
    $registration->committee_id = $committee->committee_id;
    $registration->submitted_at = new DateTime();
    $registration->user_data = file_get_contents($file);
    $this->dm->em->persist($registration);
    $this->dm->em->flush($registration);

    // Get the xml
    $xml = $this->committeeManager->getRegistrationXml($registration->registration_id);

    $this->assertInstanceOf(SimpleXMLElement::class, $xml);
  }

  /**
   * @throws Exception
   */
  public function testGetCommittee(){
    $committee = $this->committeeManager->getCommittee($this->committee_id);
    $this->assertNotEmpty($committee);
    $this->assertEquals($committee->name, $this->committee->name);
  }

  /**
   * @throws Exception
   */
  public function testDeleteCommittee(){
    $committee = new Committee ( 'Committee to Delete');
    $this->dm->em->persist($committee);
    $this->dm->em->flush();
    $committee_id =$committee->committee_id;
    $persisted = $this->committeeManager->getCommittee($committee_id);
    $this->committeeManager->deleteCommittee($persisted->committee_id, 'test');
    $result = $this->committeeManager->getCommittee($committee_id);
    $this->assertEmpty($result);
  }

  /**
 * @throws Exception
 * @throws \Doctrine\ORM\OptimisticLockException
 */
  public function testCreateCommitteeFromData(){
    $name = "People for Testing Code";
    $committee_type = 'CO';
    $pac_type = 'bonafide';
    $bonafide_type = 'county';
    $exempt = true;
    $create_time = new DateTime();
    $data = ['name' => $name, 'committee_type' => $committee_type, 'pac_type' => $pac_type, 'bonafide_type' => $bonafide_type, 'exempt' => $exempt, 'election_code' => null];
    $committee = $this->committeeManager->createCommitteeFromData($data, $this->user);
    $this->assertEquals($name, $committee->name);
    $this->assertEquals($pac_type, $committee->pac_type);
    $this->assertEquals($bonafide_type, $committee->bonafide_type);
    $this->assertEquals($exempt, $committee->exempt);
    $this->assertEquals($create_time->format('Y'), $committee->start_year);
  }

  /**
   * @throws Exception
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testCreateElectionCommitteeFromData(){
    $name = "People for Testing Code";
    $committee_type = 'CO';
    $pac_type = 'pac';
    $bonafide_type = '';
    $exempt = true;
    $election_code = '2019';
    $data = ['name' => $name, 'committee_type' => $committee_type, 'pac_type' => $pac_type, 'bonafide_type' => $bonafide_type, 'exempt' => $exempt, 'election_code' => $election_code];
    $committee = $this->committeeManager->createCommitteeFromData($data, $this->user);
    $this->assertEquals($name, $committee->name);
    $this->assertEquals($pac_type, $committee->pac_type);
    $this->assertEquals($bonafide_type, $committee->bonafide_type);
    $this->assertNull($committee->exempt);
    $this->assertEquals($election_code, $committee->election_code);
    $this->assertEquals($election_code, $committee->start_year);

  }

  public function testUpdateCommittee() {
    $name = "People for Testing Code";
    $committee_type = 'CO';
    $pac_type = 'pac';
    $start_year = 2019;
    $data = new stdClass();
    $data->committee = $committee =  new stdClass();
    $data->committee->name =  $name;
    $committee->committee_type =$committee_type;
    $committee->pac_type = $pac_type;
    $committee->category =  'Democratic Caucus Affiliated';
    $committee->start_year = $start_year;

    $committee_id = $this->committeeManager->createCommitteeFromFile($data);


    $updateData = new stdClass();
    $updateData->committee_id = $committee_id;
    $updateData->category = $committee->category;
    $updateData->memo = "Memo field";
    $updateData->end_year = 2020;
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertTrue($results->success);

    $this->dm->em->clear();
    $saved_committee = $this->dm->em->find(Committee::class, $committee->committee_id);
    $this->assertSame( $updateData->memo, $saved_committee->memo);
    $this->assertSame( $updateData->end_year, $saved_committee->end_year);

    // Make sure we can non-update the year if we don't specify a value
    unset($updateData->end_year);
    $this->committeeManager->updateCommittee($updateData);
    $this->dm->em->clear();
    $saved_committee = $this->dm->em->find(Committee::class, $committee->committee_id);
    $this->assertSame( $updateData->memo, $saved_committee->memo);
    $this->assertSame( 2020, $saved_committee->end_year);

    // make sure year must be number
    $updateData->end_year = 'foo';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertFalse($results->success);
    $this->assertEquals("Invalid end year!", $results->message);

    // Make sure that end year is greater than start year
    $updateData->end_year = 1900;
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertFalse($results->success);
    $this->assertEquals("Invalid end year!", $results->message);

    // Make sure that we can clear the end_year
    $updateData->end_year = "";
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->dm->em->clear();
    $saved_committee = $this->dm->em->find(Committee::class, $committee->committee_id);
    $this->assertTrue($results->success);
    $this->assertSame( $updateData->memo, $saved_committee->memo);
    $this->assertNull( $saved_committee->end_year);

    $updateData->category = 'Minor Party';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertFalse($results->success);
    $this->assertEquals("Invalid pac type for selected category", $results->message);

    $updateData->category = 'Democratic Caucus Affiliated';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->dm->em->clear();
    $saved_committee = $this->dm->em->find(Committee::class, $committee->committee_id);
    $this->assertSame( $updateData->memo, $saved_committee->memo);
    $this->assertTrue($results->success);
    $this->assertEquals(3, $saved_committee->party);

    $name = "People for Testing Code";
    $committee_type = 'CO';
    $pac_type = 'bonafide';
    $committee->name = $name;
    $committee->committee_type =  $committee_type;
    $committee->pac_type = $pac_type;
    $committee->election_code = NULL;
    $committee_id  = $this->committeeManager->createCommitteeFromFile($data);

    $updateData->committee_id = $committee_id;
    $updateData->category = 'Minor Party';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertTrue($results->success);
    $this->assertEquals($updateData->category, $results->data);

    $updateData->category = 'Education';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertFalse($results->success);
    $this->assertEquals("Invalid pac type for selected category", $results->message);

    $name = "People for Testing Code";
    $committee_type = 'CO';
    $pac_type = 'caucus';
    $bonafide_type = 'county';
    $exempt = true;
    $data = ['name' => $name, 'committee_type' => $committee_type, 'pac_type' => $pac_type, 'bonafide_type' => $bonafide_type, 'exempt' => $exempt, 'election_code' => null];
    $committee = $this->committeeManager->createCommitteeFromData($data, $this->user);

    $updateData->committee_id = $committee->committee_id;
    $updateData->category = 'Democratic Caucus';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertTrue($results->success);
    $this->assertEquals($updateData->category, $results->data);

    $updateData->category = 'Continuing Committee (Union)';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertFalse($results->success);
    $this->assertEquals("Invalid pac type for selected category", $results->message);

    $name = "People for Testing Code";
    $committee_type = 'CA';
    $pac_type = 'candidate';
    $election_code='2019';
    $data = ['name' => $name, 'committee_type' => $committee_type, 'pac_type' => $pac_type, 'bonafide_type' => NULL, 'exempt' => NULL, 'election_code' => $election_code];
    $committee = $this->committeeManager->createCommitteeFromData($data, $this->user);

    $updateData->committee_id = $committee->committee_id;
    $updateData->category = 'Candidate';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertTrue($results->success);
    $this->assertEquals($updateData->category, $results->data);

    $updateData->category = 'Surplus';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertFalse($results->success);
    $this->assertEquals("Invalid pac type for selected category", $results->message);

    $name = "People for Testing Code";
    $committee_type = 'CA';
    $pac_type = 'surplus';
    $election_code='2019';
    $data = ['name' => $name, 'committee_type' => $committee_type, 'pac_type' => $pac_type, 'bonafide_type' => NULL, 'exempt' => NULL, 'election_code' => $election_code];
    $committee = $this->committeeManager->createCommitteeFromData($data, $this->user);

    $updateData->committee_id = $committee->committee_id;
    $updateData->category = 'Surplus';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertTrue($results->success);
    $this->assertEquals($updateData->category, $results->data);

    $updateData->category = 'Candidate';
    $results = $this->committeeManager->updateCommittee($updateData);
    $this->assertFalse($results->success);
    $this->assertEquals("Invalid pac type for selected category", $results->message);
  }

  /**
   * @throws Exception
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function testCreateCandidateCommitteeFromData(){
    $name = "People for Testing Code";
    $committee_type = 'CA';
    $pac_type = 'candidate';
    $election_code='2019';
    $data = ['name' => $name, 'committee_type' => $committee_type, 'pac_type' => $pac_type, 'bonafide_type' => NULL, 'exempt' => NULL, 'election_code' => $election_code];
    $committee = $this->committeeManager->createCommitteeFromData($data, $this->user);
    $this->assertEquals($name, $committee->name);
    $this->assertEquals($pac_type, $committee->pac_type);
    $this->assertNull($committee->bonafide_type);
    $this->assertEquals(NULL, $committee->exempt);
    $this->assertEquals($election_code, $committee->election_code);
    $this->assertEquals('2019', $committee->start_year);
    $this->assertEquals('2019', $committee->end_year);
  }

  private function _compareObject($object1, $object2, $message = '') {

    foreach (get_object_vars($object1) as $property => $value) {
      $this->assertObjectHasAttribute($property, $object2, "$message.$property");
      switch (TRUE) {

        case (is_object($value)):
          $this->_compareObject($value, $object2->$property, "$message.$property");
          break;
        case (is_array($value)):
          foreach ($value as $i => $element) {
            // Arrays of objects should be compared as objects.
            $this->assertArrayHasKey($i, $object2->$property, "$message.{$property}[$i]");
            if (is_object($element)) {
              $this->_compareObject($element, $object2->$property[$i], "$message.{$property}[$i]");
            }
            // Assume an array of scalars
            else {
              $this->assertEquals($element, $object2->$property[$i], "$message.{$property}[$i]");
            }
          }
          break;
        default:
          // Ignoring a couple of properties that aren't set the same if unverified
          if ($property != 'has_sponsor' && $property != 'affiliations_declared') {

            $this->assertObjectHasAttribute($property, $object2, "$message.$property");
            $this->assertEquals($object1->$property, $object2->$property, "{$message}.{$property}");
          }

      }
    }
  }

  public function submissionScenarios() {
    return [['candidate'], ['candidate-np'], ['political-committee'], ['issue-committee'], ['slate-committee'], ['candidate-support-committee'], ['party-ticket-committee']];
  }

  /**
   * Test creation and submission of a committee.
   * @param $scenario
   *   Name of scenario to test
   * @dataProvider submissionScenarios
   * @throws Exception
   */
  public function testCommitteeSubmission($scenario) {
    $dir = dirname(dirname(__DIR__)) . "/data/registration_submission";
    $submission = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Save a committee as a submission
    $this->committeeManager->saveCommitteeFromSubmission($submission);

    // Make a registration
    $registration = new Registration();
    $this->dm->em->persist($registration);
    $registration->committee_id = $submission->committee->committee_id;
    $registration->verified = true;
    $registration->user_data = json_encode($submission);
    $registration->certification = 'i certify';
    $registration->name = $submission->committee->name;
    $this->dm->em->flush($registration);
    $this->dm->db->executeStatement("update committee set registration_id = :registration_id where committee_id =:committee_id",
     ['registration_id' => $registration->registration_id, 'committee_id' => $registration->committee_id]);

    if (!empty($submission->meta_data->candidacy->exit_reason)) {
      /** @var Candidacy $c */
      $c = $this->dm->em->getRepository(Candidacy::class)->findOneBy([
        'committee_id' => $submission->committee->committee_id
      ]);
      $c->exit_reason = $submission->meta_data->candidacy->exit_reason;
      $this->dm->em->flush($c);
    }

    $this->dm->em->clear();

    $retrieved_submission = $this->committeeManager->getCommitteeSubmission($submission->committee->committee_id);

    $this->_compareObject($submission->committee, $retrieved_submission->committee, "$scenario committee");
    if (!empty($submission->local_filing)) {
      $this->assertObjectHasAttribute('local_filing', $retrieved_submission);
      $this->_compareObject($submission->local_filing, $retrieved_submission->local_filing);
    }
    if (!empty($submission->meta_data)) {
      $this->assertSame($submission->meta_data->candidacy->exit_reason, $retrieved_submission->meta_data->candidacy->exit_reason);
    }
  }

  /**
   * Test a candidate with no name or email
   * @throws Exception
   */
  public function testCandidateWithNoNameOrEmail() {
    $dir = dirname(dirname(__DIR__)) . "/data/registration_submission";
    $submission = Yaml::parseFile("$dir/candidate-no-name-or-email.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Save a committee as a submission
    $this->committeeManager->saveCommitteeFromSubmission($submission);

    // Make a registration
    $registration = new Registration();
    $registration->committee_id = $submission->committee->committee_id;
    $registration->verified = true;
    $registration->user_data = json_encode($submission);
    $registration->certification = 'i certify';
    $registration->name = $submission->committee->name;
    $this->dm->em->persist($registration);
    $this->dm->em->flush($registration);
    $this->dm->db->executeStatement(
      "update committee set registration_id = :registration_id where committee_id =:committee_id",
      ['registration_id' => $registration->registration_id, 'committee_id' => $registration->committee_id]
    );

    // Need to save twice due to the chicken and egg problem for committees and registrations
    // there is code in the save function that uses the registration committee_id to generate the name
    // but the registration gets the committee_id after the committee was created
    $this->committeeManager->saveCommitteeFromSubmission($submission);

    // Get the committee contacts
    $contacts = $this->dm->em->getRepository(CommitteeContact::class)->findBy(['committee_id' => $registration->committee_id]);

    foreach ($contacts as $contact) {
      if ($contact->role === 'candidacy') {
        // the contact name skips the candidacy->name because it is an empty string
        // the contact name skips the candidacy->ballot_name because it is an empty string
        // the contact name takes the entity name
        $this->assertEquals($contact->name, 'Bob Bobberson');
        // the contact phone skips the candidacy->phone because it is an empty string
        // the contact phone takes the registration->user_data->committee->contact->phone
        $this->assertEquals($contact->phone, '5555551212');
      }
    }
  }

  /**
   * @throws Exception
   */
  public function testDoubleCommitteeSubmission() {
    $dir = dirname(dirname(__DIR__))  . "/data/registration_submission";
    $submission = Yaml::parseFile("$dir/issue-committee.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Save a committee as a submission
    $this->committeeManager->saveCommitteeFromSubmission($submission);
    $committee_id = $submission->committee->committee_id;
    //Submit the submission again
    $this->committeeManager->saveCommitteeFromSubmission($submission);

    //Get the committee contacts
    $contacts = $this->dm->em->getRepository(CommitteeContact::class)->findBy(['committee_id' => $committee_id]);

    //Make sure the committee has one committee contact
    $cc = array_filter($contacts, function($c) {return $c->role == 'committee';});
    $this->assertEquals(1, count($cc));
    //Make sure the committee has one bank contact
    $bank = array_filter($contacts, function($c) {return $c->role == 'bank';});
    $this->assertEquals(1, count($bank));
    //Make sure the committee has one book contact
    $books = array_filter($contacts, function($c) {return $c->role == 'books';});
    $this->assertEquals(1, count($books));
    //Make sure there are the same # of officers as in the submission
    $officers = array_filter($contacts, function($c) {return $c->role == 'officer';});
    $this->assertEquals(count($submission->committee->officers), count($officers));

    //Get the committee relations
    $relations = $this->dm->em->getRepository(CommitteeRelation::class)->findBy(['committee_id' => $committee_id]);
    //Make sure we didn't duplicate the proposal
    $proposals = array_filter($relations, function($r) {return $r->relation_type == 'proposal';});
    $this->assertEquals(1, count($proposals));
    //Make sure we didn't duplicate the affiliation
    $affiliations = array_filter($relations, function($r) {return $r->relation_type == 'committee';});
    $this->assertEquals(1, count($affiliations));
    $this->dm->em->clear();
  }

  /**
   * @throws Exception
   */
  public function testMergeCommittees() {
    $user = new stdClass();
    $user->realm = 'test.pdc.wa.gov';
    $user->uid = '1';
    $user->user_name = null;

    $targetCommittee = new Committee ( 'Test Old Committee');
    $targetCommittee->committee_type = 'CO';
    $this->dm->em->persist($targetCommittee);

    $sourceCommittee = new Committee ( 'Test New Committee');
    $sourceCommittee->committee_type = 'CO';
    $this->dm->em->persist($sourceCommittee);
    $this->dm->em->flush();

    $targetAuthorization = new CommitteeAuthorization($targetCommittee->committee_id, $user->realm, $user->uid);
    $targetAuthorization->role = 'full';
    $targetAuthorization->committee_id = $targetCommittee->committee_id;
    $targetAuthorization->uid = $user->uid;
    $this->dm->em->persist($targetAuthorization);

    $sourceAuthorization = new CommitteeAuthorization($sourceCommittee->committee_id, $user->realm, $user->uid);
    $sourceAuthorization->role = 'full';
    $sourceAuthorization->committee_id = $sourceCommittee->committee_id;
    $sourceAuthorization->uid = $user->uid;
    $this->dm->em->persist($sourceAuthorization);

    $sourceAPIAuthorization = new CommitteeAPIAuthorization($sourceCommittee->committee_id, 'hash!');
    $this->dm->em->persist($sourceAPIAuthorization);
    $targetAPIAuthorization = new CommitteeAPIAuthorization($targetCommittee->committee_id, 'hashing!');
    $this->dm->em->persist($targetAPIAuthorization);

    $registration = new Registration();
    $registration->committee_id = $sourceCommittee->committee_id;
    $registration->submitted_at = new DateTime('2019-05-01');
    $this->dm->em->persist($registration);

    $sourceCommittee->registration_id = $registration->registration_id;
    $this->dm->em->persist($sourceCommittee);
    $this->dm->em->flush();

    $new_cid = $sourceCommittee->committee_id;
    $old_cid = $targetCommittee->committee_id;

    $needs_verify = $this->committeeManager->mergeCommittees($sourceCommittee->committee_id, $targetCommittee->committee_id, $user->user_name);
    $this->assertTrue($needs_verify);
    // source committee should go away
    $testCommittee = $this->dm->em->find(Committee::class, $new_cid);
    $this->assertNull($testCommittee);

    // target committee should have new registration ID
    $testCommittee = $this->dm->em->find(Committee::class, $old_cid);
    $this->assertEquals($registration->registration_id, $testCommittee->registration_id);

    // no registrations for source committee
    $testRegistrations = $this->dm->em->getRepository(Registration::class)->findBy(['committee_id' => $new_cid]);
    $this->assertEmpty($testRegistrations);

    // one registration on target committee
    $testRegistrations = $this->dm->em->getRepository(Registration::class)->findBy(['committee_id' => $old_cid]);
    $this->assertCount(1, $testRegistrations);

    // target committee authorization exists
    $testAuthorization = $this->dm->em->getRepository(CommitteeAuthorization::class)->findBy(['committee_id' => $targetCommittee->committee_id, "uid" => $user->uid, "realm" => $user->realm]);
    $this->assertCount(1, $testAuthorization);

    // source committee authorizations are gone
    $testAuthorization = $this->dm->em->getRepository(CommitteeAuthorization::class)->findBy(['committee_id' => $new_cid]);
    $this->assertCount(0, $testAuthorization);

    // target committee now has two api keys
    $testAPIAuthorization = $this->dm->em->getRepository(CommitteeAPIAuthorization::class)->findBy(['committee_id' => $targetCommittee->committee_id]);
    $this->assertCount(2, $testAPIAuthorization);

    // source committee now has zero api keys
    $testAPIAuthorization = $this->dm->em->getRepository(CommitteeAPIAuthorization::class)->findBy(['committee_id' => $sourceCommittee->committee_id]);
    $this->assertCount(0, $testAPIAuthorization);
  }

  /**
   * @throws Exception
   */
  function testAutoGrantCommitteeAuthorization() {
    $user_name = 'noreply@pdc.wa.gov';
    $user_realm = 'test.pdc.wa.gov';
    $uid = '888';

    // committees
    $committeeOne = new Committee ( 'Test Committee One');
    $committeeOne->committee_type = 'CO';
    $committeeTwo = new Committee ( 'Test Committee Two');
    $committeeTwo ->committee_type = 'CO';
    $committeeThree = new Committee ( 'Test Committee Three');
    $committeeThree ->committee_type = 'CA';
    $this->dm->em->persist($committeeOne);
    $this->dm->em->persist($committeeTwo);
    $this->dm->em->persist($committeeThree);
    $this->dm->em->flush();

    // treasurer contact
    $contact = new CommitteeContact();
    $contact->committee_id = $committeeOne->committee_id;
    $contact->role = 'officer';
    $contact->treasurer = true;
    $contact->email = $user_name;
    $this->dm->em->persist($contact);
    $this->dm->em->flush();

    // candidate contact
    $contact = new CommitteeContact();
    $contact->committee_id = $committeeTwo->committee_id;
    $contact->role = 'candidacy';
    $contact->email = $user_name;
    $this->dm->em->persist($contact);
    $this->dm->em->flush();

    // declared candidacy
    $candidacy = new Candidacy();
    $candidacy->declared_email = $user_name;
    $candidacy->committee_id = $committeeThree->committee_id;
    $this->dm->em->persist($candidacy);
    $this->dm->em->flush();

    $this->committeeManager->autoGrantCommitteeAuthorization($uid, $user_name, $user_realm);

    // treasurers
    $authorization = $this->dm->em->getRepository(CommitteeAuthorization::class)
      ->findOneBy(['committee_id' => $committeeOne->committee_id , 'uid' => $uid, 'realm' => $user_realm, 'role' => 'owner']);
    $this->assertEquals($committeeOne->committee_id, $authorization->committee_id);

    // candidate contacts
    $authorization = $this->dm->em->getRepository(CommitteeAuthorization::class)
      ->findOneBy(['committee_id' => $committeeTwo->committee_id , 'uid' => $uid, 'realm' => $user_realm, 'role' => 'owner']);
    $this->assertEquals($committeeTwo->committee_id, $authorization->committee_id);

    // declared candidacies
    $authorization = $this->dm->em->getRepository(CommitteeAuthorization::class)
      ->findOneBy(['committee_id' => $committeeThree->committee_id , 'uid' => $uid, 'realm' => $user_realm, 'role' => 'owner']);
    $this->assertEquals($committeeThree->committee_id, $authorization->committee_id);
  }


  /**
   * @throws Exception
   */
  public function testCopyCommittee() {
    $user = new stdClass();
    $user->realm = 'test.pdc.wa.gov';
    $user->uid = '1';
    $user->user_name = 'super name';

    $submission = $this->generateCommitteeFromScenario('candidate', "Candidate Candidate");

    $cid = $submission->committee->committee_id;
    $c = $this->dm->em->find(Committee::class, $cid);

    $new_cid = $this->committeeManager->copyCommittee($cid, 2025, $user);
    $new_c = $this->dm->em->find(Committee::class, $new_cid);
    $this->assertEquals(2025, $new_c->election_code);
    $this->assertEquals(2025, $new_c->end_year);
    $this->assertEquals(2025, $new_c->start_year);
    $this->assertEquals($c->committee_type, $new_c->committee_type);
    $this->assertEquals($c->pac_type, $new_c->pac_type);
    $this->assertEquals($c->pac_type, $new_c->pac_type);
    $this->assertEquals($c->bonafide_type, $new_c->bonafide_type);
    $this->assertEquals($c->pac_type, $new_c->pac_type);
    $this->assertEquals($c->exempt, $new_c->exempt);
    $this->assertEquals($c->name, $new_c->name);
  }

  public function committeeFromFileProvider() {
    return [['candidate'],['political-committee'], ['surplus']];
  }

  /**
   * @dataProvider committeeFromFileProvider
   * @throws Exception
   */
  public function testCreateCommitteeFromFile($scenario = 'candidate') {
    $dir = dirname(dirname(__DIR__))  . "/data/registration_submission";
    $submission = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $submission->user = [];
    $userOne = new stdClass();
    $userOne->user_name = 'Test Vendor One';
    $userOne->realm = 'apollo';
    $userOne->uid = 'TestVendorOneID';
    array_push($submission->user, $userOne);
    $submission->api_token = [];
    $api_tokenOne = new stdClass();
    $api_tokenOne->token = 'TestTokenOne';
    $api_tokenOne->memo = 'Test memo for one';
    array_push($submission->api_token, $api_tokenOne);

    $returnedCommittee_id = $this->committeeManager->createCommitteeFromFile($submission);
    $retrieved_submission = $this->committeeManager->getCommitteeSubmission($returnedCommittee_id);
    $this->assertEquals($returnedCommittee_id, $retrieved_submission->committee->committee_id);
    if ($submission->committee->pac_type=='candidate') {
      $this->assertEquals($submission->committee->candidacy->ballot_name, $retrieved_submission->committee->candidacy->ballot_name);
    }

    $authorizations = $this->dm->em->getRepository(CommitteeAuthorization::class)
      ->findBy(['committee_id' => $retrieved_submission->committee->committee_id]);
    $this->assertNotEmpty($authorizations);
    foreach ($submission->api_token as $api) {
      $valid = $this->committeeManager->validateAPIToken($api->token);
      $this->assertEquals($retrieved_submission->committee->committee_id, $valid);
    }
    $contacts = $this->dm->em->getRepository(CommitteeContact::class)->findBy(['committee_id' => $retrieved_submission->committee->committee_id]);
    if ($submission->committee->pac_type == 'candidate') {
      $this->assertCount(5, $contacts);
    }
    else {
      $this->assertCount(2, $contacts);
    }

    /** @var Entity $entity */
    $entity = $this->dm->em->getRepository(Entity::class)->findOneBy(['entity_id' => $retrieved_submission->committee->person_id]);
    $this->assertNotEmpty($entity->name?? NULL, $scenario);
    if ($submission->committee->committee_type=='CO') {
      $this->assertEquals($submission->committee->name, $entity->name);
      $contact = $submission->committee->contact ?? null;
      if ($contact) {
        $this->assertEquals(($submission->committee->pac_type == 'candidate'), $entity->is_person);
        $this->assertEquals($contact->email ?? null, $entity->email);
        $this->assertEquals($contact->address ?? null, $entity->address);
        $this->assertEquals($contact->premise ?? null, $entity->premise);
        $this->assertEquals($contact->city ?? null, $entity->city);
        $this->assertEquals($contact->state ??null, $entity->state);
        $this->assertEquals($contact->postcode ?? null, $entity->postcode);
        // Core has a bug where phone numbers are not saved in entity_save.
        //$this->assertEquals($contact->phone ?? null, $entity->phone);

      }
    }

    if ($submission->person ?? null) {
      $person = $this->dm->em->getRepository(Entity::class)->findOneBy(['filer_id' => $submission->person->filer_id ]);
      $this->assertEquals($person->entity_id, $retrieved_submission->committee->person_id);
      $this->assertEquals($submission->person->filer_id ?? NULL, $person->filer_id);
    }
  }

  /**
   * @throws Exception
   */
  public function testUpdateCandidacyStatus() {
    $submission = $this->generateCommitteeFromScenario('candidate');
    $person = new Person();
    $person->name = 'Test person';
    $this->dm->em->persist($person);
    $this->dm->em->flush();
    $candidacy = new Candidacy();
    $candidacy->committee_id = $submission->committee->committee_id;
    $candidacy->election_code = $submission->committee->election_code;
    $candidacy->primary_result = 'W';
    $candidacy->general_result = 'L';
    $candidacy->declared_date = new DateTime('2019-05-10');
    $candidacy->exit_date = new DateTime('2019-07-03');
    $candidacy->exit_reason = "withdrew";
    $candidacy->office_code = '03';
    $candidacy->jurisdiction_id = 1000;
    $candidacy->person_id = $person->person_id;
    $this->dm->em->persist($candidacy);
    $this->dm->em->flush();

    $this->committeeManager->saveCommitteeFromSubmission($submission);
    $this->dm->em->clear();

    $retrieved_submission = $this->committeeManager->getCommitteeSubmission($submission->committee->committee_id);
    $this->assertEquals('W', $retrieved_submission->meta_data->candidacy->primary_result);
    $this->assertEquals('L', $retrieved_submission->meta_data->candidacy->general_result);
    $this->assertEquals('2019-05-10', $retrieved_submission->meta_data->candidacy->declared_date);
    $this->assertEquals('2019-07-03', $retrieved_submission->meta_data->candidacy->exit_date);
    $this->assertEquals('withdrew', $retrieved_submission->meta_data->candidacy->exit_reason);

    $updated_at = $retrieved_submission->updated_at;
    $payload = (object)[
      'committee_id' => $submission->committee->committee_id,
      'primary_result' => 'L',
      'general_result' => 'W',
      'declared_date' => '2019-05-11',
      'exit_date' => '2019-07-04',
      'exit_reason' => 'deceased'
      ];
    $this->committeeManager->updateCandidacyStatus($payload);

    $retrieved_submission = $this->committeeManager->getCommitteeSubmission($submission->committee->committee_id);
    $this->assertEquals('L', $retrieved_submission->meta_data->candidacy->primary_result);
    $this->assertEquals('W', $retrieved_submission->meta_data->candidacy->general_result);
    $this->assertEquals('2019-05-11', $retrieved_submission->meta_data->candidacy->declared_date);
    $this->assertEquals('2019-07-04', $retrieved_submission->meta_data->candidacy->exit_date);
    $this->assertEquals('deceased', $retrieved_submission->meta_data->candidacy->exit_reason);

    $committee = $this->dm->em->find(Committee::class, $submission->committee->committee_id);
    $this->assertGreaterThan($updated_at, $committee->updated_at);
  }

  function testSavePerson() {
    $person_info = (object)[
      'name' => 'Name Test',
      'address' => '123 Main St',
      'city' => 'Olympia',
      'state' => 'WA',
      'postcode' => '98501',
      'email' => 'a@example.com'
    ];
    $this->processor = new PersonProcessor($this->dm);
    $pid = $this->processor->savePerson($person_info);
    $retrieved_person = $this->processor->getPerson($pid);
    $this->assertEquals($person_info->address, $retrieved_person->address);
    $this->assertEquals($person_info->city, $retrieved_person->city);
    $this->assertEquals($person_info->state, $retrieved_person->state);
    $this->assertEquals($person_info->postcode, $retrieved_person->postcode);
    $this->assertEquals($person_info->email, $retrieved_person->email);
  }

  function testMiniFullPermission() {
    $user = new stdClass();
    $user->user_name = 'loggable_jones';
    $submission = $this->generateCommitteeFromScenario('political-committee');
    $retrieved_submission = $this->committeeManager->getCommitteeSubmission($submission->committee->committee_id);
    $this->assertEquals(false, $retrieved_submission->admin_data->mini_full_permission);
    $this->committeeManager->miniFullPermission($submission->committee->committee_id, true, $user);
    $retrieved_submission = $this->committeeManager->getCommitteeSubmission($submission->committee->committee_id);
    $this->assertEquals(true, $retrieved_submission->admin_data->mini_full_permission);
  }

  public function testSurplusPersonSave() {
    $dir = dirname(dirname(__DIR__))  . "/data/registration_submission";
    $submission = Yaml::parseFile("$dir/surplus.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Save a committee as a submission
    $this->committeeManager->saveCommitteeFromSubmission($submission);
    $committee = $this->dm->em->getRepository(Committee::class)->find($submission->committee->committee_id);
    $person_id = $committee->person_id;

    //Submit the submission again
    $this->committeeManager->saveCommitteeFromSubmission($submission);
    $new_person_id = $committee->person_id;

    $this->assertEquals($person_id, $new_person_id);
  }
}
