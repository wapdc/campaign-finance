<?php

namespace Tests\WAPDC\CampaignFinance\Registration;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\OrcaRegistrationManager;
use WAPDC\CampaignFinance\CommitteeManager;
use WAPDC\CampaignFinance\RegistrationDraftManager;
use WAPDC\CampaignFinance\RegistrationManager;
use WAPDC\Core\FilerProcessor;


/**
 * Class RegistrationManagerTest
 * @package Tests\WAPDC\CampaignFinance
 */
class OrcaRegistrationManagerTest extends TestCase {

  /**
   * @var CFDataManager
   */
  private $dm;

  /**
   * @var OrcaRegistrationManager
   */
  private $orcaRegistrationManager;


  /**
   * @var CommitteeManager
   */
  private $committeeManager;

  /**
   * @var RegistrationDraftManager
   */
  private $registrationDraftManager;

  /**
   * @var RegistrationManager
   */
  private $registrationManager;

  /**
   * @throws \Doctrine\DBAL\DBALException
   * @throws \Doctrine\ORM\ORMException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dm = new CFDataManager();
    $this->dm->beginTransaction();
    $this->committeeManager = new CommitteeManager($this->dm);
    $this->registrationDraftManager = new RegistrationDraftManager($this->dm, $this->committeeManager);
    $fp = new FilerProcessor($this->dm);
    $this->registrationManager = new RegistrationManager($this->dm, $this->committeeManager, $this->registrationDraftManager, $fp);
    $this->orcaRegistrationManager = new OrcaRegistrationManager($this->dm, $this->committeeManager, $this->registrationManager);
  }

  /**
   * @throws \Exception
   */
  public function testConvertToXML(){
    $dir = dirname(dirname(__DIR__))  . "/data/registration_submission";
    $submission = Yaml::parseFile("$dir/political-committee.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Save a committee as a submission
    $this->committeeManager->saveCommitteeFromSubmission($submission);

    $token = $this->committeeManager->generateAPIToken($submission->committee->committee_id, 'test', 'testUser');
    $xml = $this->orcaRegistrationManager->getRegistrationXML($token);
    $this->assertNotEmpty($xml);
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }
}