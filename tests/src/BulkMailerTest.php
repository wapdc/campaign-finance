<?php


namespace Tests\WAPDC\CampaignFinance;


use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\Core\Mock\MockMailer;
use WAPDC\CampaignFinance\BulkMailer;

/**
 * Class BulkMailerTest
 * @coversDefaultClass BulkMailer
 */
class BulkMailerTest extends TestCase {

  /** @var BulkMailer */
  protected $bulkMailer;

  /** @var MockMailer */
  protected $mailer;

  protected function setUp(): void {
    parent::setUp();
    $this->mailer = new MockMailer();
    $this->bulkMailer = new BulkMailer($this->mailer);
  }

  public function testMailMerge() {
    $file = dirname(__DIR__) . '/data/email/mail-merge-test.yml';
    $data = Yaml::parseFile($file, YAML::PARSE_OBJECT_FOR_MAP);
    $options = new \stdClass();
    $options->template_id = 'foo';

    $mails_sent = $this->bulkMailer->mailMerge($data->emails, $options);

    $this->assertCount(2, $this->mailer->mails_sent);
    foreach ($data->emails as $row) {
      $email = $row->email;
      $mail = $this->mailer->getMailSentTo($email);
      $this->assertEquals('foo', $mail->message_template);
    }

    $this->assertEquals(['david.metzler@pdc.wa.gov', 'ben.livingston@pdc.wa.gov'], $mails_sent);
  }


}