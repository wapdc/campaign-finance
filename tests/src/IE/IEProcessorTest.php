<?php


namespace Tests\WAPDC\CampaignFinance\IE;


use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use WAPDC\CampaignFinance\CFDataManager;
use WAPDC\CampaignFinance\IEProcessor;
use \stdClass;
use WAPDC\Core\EntityProcessor;
use WAPDC\Core\Model\User;

class IEProcessorTest extends TestCase {
  /** @var IEProcessor  */
  protected $processor;
  protected $dm;
  protected $data_dir;
  /** @var User  */
  protected $user;

  protected function setUp(): void {
    parent::setUp();
    $this->data_dir = dirname(dirname(__DIR__)) . '/data/ie';
    $this->dm = new CFDataManager();
    $this->processor = new IEProcessor($this->dm);
    $this->dm->beginTransaction();
    $this->user = new User();
    $this->user->uid = '1';
    $this->user->realm = 'test';
    $this->user->user_name = 'test.user@noreply.com';
    // Create a test user for all interactions
    $this->dm->db->executeStatement("insert into pdc_user(uid, realm, user_name) VALUES (:uid, :realm, :user_name) on conflict  do nothing",
      [
        'uid' => $this->user->uid,
        'realm' => $this->user->realm,
        'user_name' => $this->user->user_name,
      ]
    );
  }

  /**
   * @param string $scenario
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function testRequestAccess($scenario = 'new-sponsor') {
    $data = Yaml::parseFile($this->data_dir . "/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $request = $data->request;
    $request->uid = $this->user->uid;
    $request->realm = $this->user->realm;

    /** @var stdClass $response */
    $request_access_id = $this->processor->requestAccess($data->request);
    $this->assertGreaterThan(0, $request_access_id);

    [$saved_request] = $this->dm->db->executeQuery(
      "SELECT * from c6_sponsor_request c6 where uid=:uid and realm=:realm",
      ['uid' => $this->user->uid, 'realm' => $this->user->realm]
    )->fetchAllAssociative();

    $this->assertSame($request->sponsor_type,$saved_request['sponsor_type']);
    $this->assertSame($request->name,$saved_request['name']);
    $this->assertSame($request->email, $saved_request['email']);
    $this->assertSame($request->address, $saved_request['address']);
    $this->assertSame($request->premise, $saved_request['premise']);
    $this->assertSame($request->city, $saved_request['city']);
    $this->assertSame($request->state, $saved_request['state']);
    $this->assertSame($request->postcode, $saved_request['postcode']);
  }

  public function testHasC6Access($scenario = 'existing-entity') {
    $data = Yaml::parseFile($this->data_dir . "/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Create the entity
    $entityProcessor = new EntityProcessor($this->dm);
    $entity = $data->entity;
    $entity_id = $entityProcessor->saveEntity($entity);

    // Call processor before access is granted.
    $this->assertFalse($this->processor->checkC6Access($entity_id, 'cf_editor', $this->user));
    $this->assertFalse($this->processor->checkC6Access($entity_id, 'cf_reporter', $this->user));
    $this->assertFalse($this->processor->checkC6Access($entity_id, 'owner', $this->user));


    // Add owner permission
    $this->dm->db->executeStatement("insert into wapdc.public.entity_authorization (entity_id, realm, role, uid) values (:entity_id, :realm, :role, :uid)",
      [
        'entity_id' => $entity_id,
        'uid' => $this->user->uid,
        'realm' => $this->user->realm,
        'role' => 'owner'
      ]
    );

    $this->assertTrue($this->processor->checkC6Access($entity_id, 'cf_editor', $this->user));
    $this->assertTrue($this->processor->checkC6Access($entity_id, 'cf_reporter', $this->user));
    $this->assertTrue($this->processor->checkC6Access($entity_id, 'owner', $this->user));

    // Change to reporter permission
    $this->dm->db->executeStatement("update entity_authorization set role=:role where entity_id=:entity_id and uid=:uid and realm=:realm",
      [
        'entity_id' => $entity_id,
        'uid' => $this->user->uid,
        'realm' => $this->user->realm,
        'role' => 'cf_reporter'
      ]
    );

    $this->assertTrue($this->processor->checkC6Access($entity_id, 'cf_reporter', $this->user));
    $this->assertTrue($this->processor->checkC6Access($entity_id, 'cf_editor', $this->user));
    $this->assertFalse($this->processor->checkC6Access($entity_id, 'owner', $this->user));

    // Change to editor permission
    $this->dm->db->executeStatement("update entity_authorization set role=:role where entity_id=:entity_id and uid=:uid and realm=:realm",
      [
        'entity_id' => $entity_id,
        'uid' => $this->user->uid,
        'realm' => $this->user->realm,
        'role' => 'cf_editor'
      ]
    );

    $this->assertTrue($this->processor->checkC6Access($entity_id, 'cf_editor', $this->user));
    $this->assertFalse($this->processor->checkC6Access($entity_id, 'cf_reporter', $this->user));
    $this->assertFalse($this->processor->checkC6Access($entity_id, 'owner', $this->user));


  }

  public function testStateAbbreviations()
  {
    $this->assertSame('WA', $this->processor->getStateAbbreviation('WA'));
    $this->assertSame('WA', $this->processor->getStateAbbreviation(' washington '));
    $this->assertSame('--', $this->processor->getStateAbbreviation('WaterTonia'));
    $this->assertSame('WA', $this->processor->getStateAbbreviation('Washingston'));
  }

  protected function tearDown(): void {
    $this->dm->rollback();
    $this->dm->db->close();
    parent::tearDown();
  }

}