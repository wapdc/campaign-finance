# ORCA Submission Testing Strategy

Primary considerations when testing ORCA submissions include:
1. There must first exist a fund.
2. Each first class entity that is relevant for testing the submission must first begin its life as an entry in the `trankeygen` table.
3. When setting up initial conditions, there must be some mechanism to map the `trankeygen_id` of contacts, which do not initially exist, back to the associated receipts.

## Initial Conditions Data
Consider the following example of initial conditions data.
```yaml
contacts:
  - name: 'Jane Doe'
    id: 1
    trankeygen_id: 
  - name: 'John Doe'
    id: 2
receipts:
  - name: 'Donation from donor X'
    amount: 500
    id: 3
    contid: 1
    cid: 4
    did: 3
  - name: 'Contribution from Y'
    amount: 500
    id: 4
    contid: 1
```

There must be a generator function that parses the above file and associates the `trankeygen_id`'s of the newly created contacts with the relevant receipts. The strategy for doing this is as follows:
1. Pass the receipts to the relevant generator function. This function is responsible for iterating the receipts and creating them, while building a hashmap containing the following data:
    ```php
      [
        "1" => "abc1234",
        "2" => "def4567",
      ]
    ```
     Additionally, another data structure is required in order to instruct the generator function as to which records should be passed to `ensureTrankeygenIdExists`. Alternatively, this can (perhaps should) be part of the `ensureTrankeygenIdExists` implementation. 
      ```php
      [
        "receipts" => ["cid", "did", "contid"]
      ]

      ```
2. As the function iterates through the receipts, it passes the id of the arbitrary id `id` of each record it needs into a validator function `ensureTrankeygenIdExists`. This function will check the hashmap to see if there is a corresponding `trankeygen_id`. If there is, it will return this value. Otherwise, it will create a `trankeygen` record and insert an entry into the hashmap which looks like an item in the above example. 
3. As the contact records are created, using a function such as `generateContacts`, each placeholder item that should be a `trankeygen_id` will pass its value int othe `ensureTrankeygenIdExists` function, which will return either the corresponding value from the hashmap or the newly generated `trankeygen_id`.

## Assertions
There must also be a means by which to associate the contacts and contributions in the initial conditions data with the corresponding objects in the assertions file (`orca_deposit_submission_assertions.yml`).
On inspecting the file, you will see that there are arbitrary integer values int the `contact_key` fields of each contribution. Further, there the same integer values are in the `contact_key` field of each contact.
```yaml
contacts:
   - contact_key: 1
     aggregate_general: 30.00
     aggregate_primary: 0.00
     contributor_type: "I"
     name: "Jane Doe"
     address: "8802 W KLAMATH AVE"
     city: "KENNEWICK"
     state: "WA"
     postcode: "98122"
     employer:
        name: "RETIRED"
        city: "Seattle"
        state: "WA"
        occupation: "RETIRED"
contributions:
   - amount: "30.00"
     contact_key: 1
     date: "2020-02-25"
     election: "G"
```
The contacts can be correlated to the contributions by referring to the hashmap constructed during the initial trankegen record generation (performed by `ensureTrankeygenIdExists`) in the previous step.

## Outcomes
> Refer to `tests > data > orca_submission` directory.

The outcome of the above process is to produce an object that is acceptable to the `ReportProcessor` and its subclasses (`C3ReportProcessor`, `C4ReportProcessor`). The structure should roughly match what is found in `tests > data > orca_submission > orca_deposit_submission_assertions.yml` or the related C4 version of this file.
