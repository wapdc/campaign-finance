INSERT INTO registration (name, xml, submitted_at, source) SELECT 'PEOPLE FOR DEMONSTRABLE SOFTWARE', xml, NOW(), 'Test Data' FROM registration WHERE registration_id=497;
INSERT INTO committee(name) VALUES ('PEOPLE FOR DEMONSTRABLE SOFTWARE');
UPDATE registration SET committee_id = (SELECT committee_id FROM committee WHERE name='PEOPLE FOR DEMONSTRABLE SOFTWARE') WHERE name='People for Demonstrable Software';
UPDATE committee SET registration_id = (SELECT registration_id FROM registration WHERE name='PEOPLE FOR DEMONSTRABLE SOFTWARE') WHERE name='People for Demonstrable Software';
