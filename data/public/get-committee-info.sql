select committee_id,
       election_code,
       start_year,
       end_year,
       name,
       pac_type,
       (select row_to_json(x)
        from (select address, city, state, postcode, phone, email
              from committee_contact
              where committee_id = :committee_id
                and role = 'committee') x)                     as contact,
       (select json_agg(x)
        from (select name,
                     role,
                     title,
                     ministerial,
                     address,
                     city,
                     state,
                     postcode,
                     phone,
                     email
              from committee_contact
              where committee_id = :committee_id
                and role = 'officer') x)                       as officers,
       case
         when committee_type = 'CA' then
           (select json_agg(x)
            from (select election_code,
                         ballot_name,
                         declared_email,
                         offtitle                           as office,
                         office_code,
                         j.name                             as jurisdiction,
                         c.jurisdiction_id,
                         c.filer_id,
                         (select partisan
                          from jurisdiction_office jo
                          where c.jurisdiction_id = jo.jurisdiction_id
                            and c.office_code = jo.offcode) as partisan
                  from candidacy c
                         join jurisdiction j on c.jurisdiction_id = j.jurisdiction_id
                         join foffice o on o.offcode = c.office_code
                         join jurisdiction_office jo
                              on jo.jurisdiction_id = c.jurisdiction_id and jo.offcode = c.office_code
                  where c.committee_id = :committee_id) x) end as candidacy
from committee
where committee_id = :committee_id