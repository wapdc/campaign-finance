select c.*, j.name as jurisdiction_name, initcap(fo.offtitle) as office_title
from candidacy c
  join jurisdiction j
    on c.jurisdiction_id = j.jurisdiction_id
  join foffice fo
    on c.office_code = fo.offcode
where left(c.election_code, 4) = :year
  and c.ballot_name ilike :search;


