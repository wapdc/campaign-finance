select
    r.election_year as year,
    a.con_type as type,
    count(a.ident) as records
from auct a
         join reports r on a.repno = r.repno
where r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, a.con_type
order by r.election_year, a.con_type
