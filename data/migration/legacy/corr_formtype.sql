select
    r.election_year,
    r.form as form_type,
    sum(c.rcv_report) + sum(c.exp_report) as org_amt,
    sum(c.rcv_correc) + sum(c.exp_correc) as corr_amt,
    sum(c.refund_amt) as refund_amt
from reports r
    join corr c on c.repno = r.repno
where (r.election_year between :start_year and :end_year)
group by r.election_year, r.form
order by r.election_year desc, r.form