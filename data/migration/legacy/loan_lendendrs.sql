select
    r.election_year as year,
    count(l.ident) as records,
    l.lend_endrs as endorser
from loan l
         join reports r on l.repno = r.repno
where r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, l.lend_endrs
order by r.election_year, l.lend_endrs


