select
    r.election_year,
    c4.rec_type,
    count(c4.ident) as records,
    count(distinct c4.repno) as repno,
    sum(cast(c4.repno as float)) as repno_sum,
    count(distinct r.filer_id) as filer_id,
    count(case when c4.entity_cd is null then 1 end) as null_entity_cd,
    count(case when c4.entity_cd is not null then 1 end) as other_entity_cd,
    count(distinct trim(c4.filer_name)) as filer_name,
    count(case when c4.address2 is not null then 1 end) as address2,
    count(case when c4.zip4 is not null then 1 end) as zip4,
    count(distinct c4.office) as office,
    count(case when c4.tres_name is not null then 1 end) as tres_name,
    count(case when c4.tres_date is not null then 1 end) as tres_date,
    count(case when c4.tres_phone is not null then 1 end) as tres_phone,
    count(distinct coalesce(c4.from_date,r.perfrom)) as period_from,
    count(distinct coalesce(c4.thru_date,r.perthru)) as period_thru,
    count(case when c4.final_yn is null then 0
               when c4.final_yn = 'N' then 0 end) as null_final,
    count(case when c4.final_yn = 'Y' then 1
               when c4.final_yn = 'X' then 1 end) as final,
    count(distinct cand_name) as candidate
from c4 c4
join reports r on r.repno = c4.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, c4.rec_type
order by 1 desc, 2 desc