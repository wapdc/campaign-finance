select
    rep.election_year,
    r.form_type,
    count(1) as records,
    sum(amount) as amount
from wapdc.dbo.rcpt r
         join reports rep on rep.repno = r.repno
where rep.election_year BETWEEN :start_year AND :end_year
group by election_year,r.form_type

