select
    rep.election_year,
    prim_gen,
    count(1) as records,
    sum(amount) as amount
from wapdc.dbo.rcpt r
         join reports rep on rep.repno = r.repno
where rep.election_year BETWEEN :start_year AND :end_year
group by election_year,prim_gen
order by election_year desc, prim_gen asc