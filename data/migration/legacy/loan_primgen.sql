select
    r.election_year as year,
    count(r.repno) as records,
    l.prim_gen as prim_gen
from reports r
join loan l on l.repno = r.repno
where r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, l.prim_gen
order by r.election_year, l.prim_gen


