
select
    election_year,
    count(1) records,
    count(distinct repno) as repno,
    sum(cast(repno as float)) as repno_sum,
    count(superseded) as superseded,
    sum(cast(superseded as float)) as superseded_sum,
    count(case when superseded is null then 1  end) as null_superseded,
    count(distinct filer_id) as filer_id,
    count(case when filer_id is null then 1 end) as null_filerid,
    count(distinct date_filed) as date_filed,
    MIN(date_filed) AS min_date_filed,
    MAX(date_filed) AS max_date_filed,
    count(distinct form) as form
from reports
WHERE election_year BETWEEN :start_year AND :end_year
AND (form = 'C3' OR form = 'C4')
group by election_year
order by election_year DESC