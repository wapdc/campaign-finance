select
  rep.election_year as year,
  count(distinct r.repno) as repno,
  count(distinct r.rcpt_date) as rcpt_date,
  count(distinct trim(r.name)) as name,
  count(distinct trim(r.address)) as address,
  count(distinct trim(r.city)) as city,
  count(distinct trim(r.zip4)) as zip4,
  count(distinct trim(r.descrip)) as descrip,
  count(distinct trim(r.employer)) as employer,
  count(distinct trim(r.occup)) as occup,
  count(distinct trim(r.empl_city)) as empl_city,
  count(distinct trim(r.empl_state)) as empl_state,
  count(distinct r.contributor_location_id) as location,
  sum(amount) as amount,
  sum(aggregate) as aggregate,
  count(distinct prim_gen) as prim_gen,
  count(distinct form_type) as form_type,
  count(distinct code) as code
  from rcpt r
  join reports rep on rep.repno = r.repno
  where rep.election_year BETWEEN :start_year AND :end_year
group by rep.election_year
order by rep.election_year desc


