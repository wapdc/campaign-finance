select
    r.election_year as year,
    min(ident) as min_ident,
    max(ident) as max_ident,
    count(ident) as records,
    count(distinct r.repno) as repno,
    count(distinct coalesce(sum_date,r.perthru)) as sum_date,
    sum(amount) as amount,
    sum(s.aggregate) as aggregate,
    min(coalesce(sum_date,r.perthru)) as min_t_date,
    max(coalesce(sum_date,r.perthru)) as max_t_date
from sums s
join reports r on s.repno = r.repno
where election_year between :start_year and :end_year
group by r.election_year
order by r.election_year desc
