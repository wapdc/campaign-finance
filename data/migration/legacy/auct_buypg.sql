select
    r.election_year as year,
    a.buy_pg as prim_gen,
    count(a.ident) as records
from auct a
         join reports r on a.repno = r.repno
where r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, a.buy_pg
order by r.election_year, a.buy_pg


