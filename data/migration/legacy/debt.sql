select
    r.election_year as year,
    count(d.ident) as ident,
    count(distinct d.repno) as repno,
    count(distinct vend_name) as vendor_name,
    count(distinct trim(d.address)) as address,
    count(distinct trim(d.city)) as city,
    count(distinct d.state) as state,
    count(distinct trim(d.zip4)) as zip,
    count(distinct d.date_paid) as date_paid,
    count(distinct trim(d.expn_desc)) as description,
    sum(amount) as amount
from debt d
    join reports r on r.repno = d.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
group by r.election_year
order by election_year desc
