select
    r.election_year as year,
    count(x.ident) as ident,
    count(distinct x.repno) as repno,
    count(distinct x.rec_type) as rec_type,
    count(distinct trim(x.name)) as name,
    count(distinct trim(x.address)) as address,
    count(distinct trim(x.city)) as city,
    count(distinct trim(x.zip4)) as zip,
    count(distinct x.date_paid) as date_paid,
    count(distinct trim(x.expn_desc)) as description,
    count(distinct x.vendor_location_id) as location,
    count(distinct x.form_type) as form,
    count(distinct x.expn_code) as expn_code,
    sum(amount) as amount
from expn x
join reports r on r.repno = x.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
group by r.election_year
order by election_year desc
