select
    r.election_year as year,
    d.expn_code as category,
    count(d.ident) as records
from debt d
         join reports r on d.repno = r.repno
where r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, d.expn_code
order by r.election_year, d.expn_code


