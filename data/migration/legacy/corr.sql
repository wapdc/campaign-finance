select
r.election_year as year,
c.form_type,
count(distinct c.repno) as repno,
count(distinct c.name) as name,
count(distinct trim(c.descrip)) as description,
count(distinct c.corr_date) as date,
org_amt = CASE
    WHEN c.form_type = 'C.1' THEN
        sum(c.rcv_report)
    WHEN c.form_type = 'C.2' THEN
        sum(c.exp_report)
    END,
corr_amt = CASE
    WHEN c.form_type = 'C.1' THEN
        sum(c.rcv_correc)
    WHEN c.form_type = 'C.2' THEN
        sum(c.exp_correc)
    END,
sum(c.refund_amt) as refund_amt
from corr c
join reports r on r.repno = c.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, c.form_type
order by election_year desc
