select
    r.election_year as year,
    e.expn_code as category,
    count(e.ident) as records
from expn e
         join reports r on e.repno = r.repno
where r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, e.expn_code
order by r.election_year, e.expn_code


