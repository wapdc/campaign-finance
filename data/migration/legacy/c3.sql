select
    r.election_year,
    c3.rec_type,
    --grouping(rec_type),
    count(c3.ident) as records,
    count(distinct c3.repno) as repno,
    sum(cast(c3.repno as float)) as repno_sum,
    count(distinct r.filer_id) as filer_id,
    count(case when c3.entity_cd is null then 1 end) as null_entity_cd,
    count(case when c3.entity_cd is not null then 1 end) as other_entity_cd,
    count(distinct trim(c3.filer_name)) as filer_name,
    count(case when c3.address2 is not null then 1 end) as address2,
    count(case when c3.zip4 is not null then 1 end) as zip4,
    count(distinct c3.office) as office,
    count(case when c3.tres_name is not null then 1 end) as tres_name,
    count(case when c3.tres_date is not null then 1 end) as tres_date,
    count(case when c3.tres_phone is not null then 1 end) as tres_phone,
    count(distinct coalesce(c3.from_date,r.perfrom)) as period_from,
    count(distinct coalesce(c3.thru_date,r.perthru)) as period_thru
from c3 as c3
join reports r on r.repno = c3.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, c3.rec_type
order by 1 desc, 2 desc

