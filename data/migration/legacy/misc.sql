select
    r.election_year as year,
    count(m.ident) as ident,
    count(distinct m.repno) as repno,
    count(distinct trim(m.name)) as name,
    count(distinct trim(m.address)) as address,
    count(distinct trim(m.city)) as city,
    count(distinct trim(m.state)) as state,
    count(distinct trim(m.zip4)) as zip,
    count(distinct m.misc_date) as date,
    count(distinct trim(m.misc_desc)) as description,
    sum(m.amount) as amount
from misc m
         join reports r on r.repno = m.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
  and m.amount IS NOT NULL
group by r.election_year
order by election_year desc
