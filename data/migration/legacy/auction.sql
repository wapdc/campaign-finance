select
    r.election_year as year,
    count(distinct a.ident) as ident,
    count(distinct a.repno) as repno,
    count(distinct a.form_type) as form_type,
    count(distinct a.auct_date) as auct_date,
    count(distinct trim(a.con_name)) as con_name,
    count(distinct trim(a.con_addr)) as con_addr,
    count(distinct a.con_city) as con_city,
    count(distinct a.con_st) as con_state,
    count(distinct a.con_zip4) as con_zip,
    count(distinct trim(a.buy_name))  as buy_name,
    count(distinct trim(a.buy_addr)) as buy_addr,
    count(distinct a.buy_city) as buy_city,
    count(distinct a.buy_st) as buy_state,
    count(distinct a.buy_zip4) as buy_zip,
    count(distinct trim(item_desc)) as item_desc,
    count(distinct trim(a.con_emp)) as con_employer,
    count(distinct trim(a.con_occ)) as con_occ,
    count(distinct trim(a.con_emp_city)) as con_city,
    count(distinct a.con_emp_st) as con_state,
    count(distinct a.buy_emp) as buy_employer,
    count(distinct trim( a.buy_occ)) as buy_occ,
    count(distinct trim(a.buy_emp_city)) as buy_city,
    count(distinct a.buy_emp_st) as buy_state,
    count(distinct a.auctions_con_location_id) as con_location,
    count(distinct a.auctions_buyer_location_id) as buy_location,
    sum(a.fair_mkt) as fair_market,
    sum(a.sale_amt) as sale_amount,
    sum(a.con_aggre) as con_aggregate,
    sum(a.buy_aggre) as buy_aggregate
from auct a
         join reports r on r.repno = a.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
group by r.election_year
order by election_year desc
