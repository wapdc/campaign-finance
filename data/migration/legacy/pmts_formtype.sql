select
    r.election_year,
    p.form_type,
    sum(p.orig_amt) as orig_amt,
    sum(p.prin_paid) as prin_paid,
    sum(p.total_paid) as total_paid,
    sum(p.forgiv_amt) as forgiv_amt,
    sum(p.int_paid) as int_paid
from pmts p
    join reports r on r.repno = p.repno
where r.election_year between :start_year and :end_year
group by r.election_year, p.form_type
order by r.election_year desc, p.form_type
