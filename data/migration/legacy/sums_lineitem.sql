select
    r.election_year as year,
    s.line_item,
    count(ident) as records,
    sum(amount) as amount,
    sum(aggregate) as aggregate
from sums s
         join reports r on s.repno = r.repno
where r.election_year between :start_year and :end_year
group by r.election_year, s.line_item
order by r.election_year desc, s.line_item