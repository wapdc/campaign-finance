select
r.election_year as year,
count(ident) as records,
count(distinct r.repno) as repno,
count(distinct trim(name)) as name,
count(distinct trim(address)) as address,
count(distinct trim(city)) as city,
count(distinct trim(state)) as state,
count(distinct trim(zip4)) as zip4,
count(distinct loan_date) as loan_date,
sum(orig_amt) as orig_amt,
sum(prin_paid) as prin_paid,
sum(total_paid) as total_paid,
sum(forgiv_amt) as forgiv_amt,
sum(int_paid) as int_paid
from wapdc.dbo.pmts p
join reports r on r.repno = p.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
group by r.election_year
order by election_year desc

