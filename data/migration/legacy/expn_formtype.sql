select
    r.election_year as year,
    e.form_type as form_type,
    count(e.ident) as records,
    count(distinct e.date_paid) as expn_date,
    sum(e.amount) as amount
from expn e
join reports r on e.repno = r.repno
where r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, e.form_type
order by r.election_year, e.form_type

