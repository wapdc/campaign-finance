select
    r.election_year as year,
    coalesce(s.form_type, r.form) as form_type,
    count(s.ident) as records,
    count(distinct coalesce(sum_date,r.perthru)) as sum_date,
    sum(amount) as amount,
    sum(aggregate) as aggregate
from sums s
    join reports r on s.repno = r.repno
where r.election_year BETWEEN :start_year AND :end_year
group by r.election_year, coalesce(s.form_type, r.form)
order by 1,2