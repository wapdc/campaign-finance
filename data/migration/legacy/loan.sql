select
    r.election_year as year,
    count(l.ident) as ident,
    count(distinct l.repno) as repno,
    count(distinct case when form_type is not null then 1 end) as form_type,
    count(distinct case when form_type is null then 0 end) as null_form_type,
    count(distinct l.rec_type) as rec_type,
    count(distinct l.name) as name,
    count(distinct l.address) as address,
    count(distinct trim(l.city)) as city,
    count(distinct l.state) as state,
    count(distinct l.zip4) as zip,
    count(distinct l.loan_date) as loan_date,
    count(distinct l.intr_rate) as int_rate,
    count(distinct loan_sched) as loan_schedule,
    count(distinct loan_due) as due_date,
    count(distinct employer) as employer,
    count(distinct occup) as occupation,
    count(distinct empl_city) as empl_city,
    count(distinct empl_state) as empl_state,
    sum(loan_amt) as loan_amt,
    sum(liable_amt) as liable_amt,
    sum(aggregate) as aggregate
from loan l
    join reports r on r.repno = l.repno
WHERE r.election_year BETWEEN :start_year AND :end_year
group by r.election_year
order by election_year desc
