SELECT
    r.filer_id, r.election_year, s.ident, s.repno, coalesce(s.form_type, r.form) as form_type, s.line_item, coalesce(s.sum_date, r.perthru) as sum_date, 'SUM' as rec_type, COALESCE(s.amount,0) as amount, s.aggregate, s.pers_count
FROM reports r JOIN sums s ON S.repno = r.repno
WHERE r.filer_id = :filer_id and r.election_year=:election_year
ORDER BY r.election_year desc, s.repno, s.line_item
