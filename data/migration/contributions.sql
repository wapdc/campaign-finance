select
  trim(r.filer_id) as filer_id,r.election_year,ident,t.repno,rec_type,form,form_type,rpt_date,trim(name) as name,trim(address) as address, nullif(trim(city), '') as city,state,zip4,prim_gen,rcpt_date,coalesce(amount,0) as amount,aggregate,trim(descrip) as descrip,trim(employer) as employer,trim(occup) as occup,trim(empl_city) as empl_city,empl_state,memo,code,contributor_location_id
FROM rcpt t join reports r on r.repno = t.repno
  WHERE r.election_year=:election_year and r.filer_id=:filer_id

