select e.ident, e.repno, e.rec_type, e.form_type, trim(e.name) as name, trim(e.address) as address, nullif(trim(e.city), '') as city,
       e.state, e.zip4, e.date_paid, COALESCE(e.amount,0.0) as amount, expn_code, trim(expn_desc) as expn_desc, vendor_location_id, r.filer_id, r.election_year
from wapdc.dbo.expn e
  join reports r on r.repno = e.repno
where r.filer_id = :filer_id
  and r.election_year = :election_year
order by e.date_paid
