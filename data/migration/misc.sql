select
  r.filer_id, r.election_year, m.ident, m.repno, m.rec_type, m.form_type, r.date_filed, trim(m.name) as name, trim(m.address) as address, trim(m.city) as city, m.state, trim(m.zip4) as zip4, m.misc_date, m.amount, trim(m.misc_desc) as misc_desc
from misc m join reports r on r.repno = m.repno
where r.election_year=:election_year and r.filer_id=:filer_id