SELECT
  r.filer_id, r.election_year, r.repno, a.ident, a.form_type, a.auct_date,
  trim(a.con_name) as con_name, trim(a.con_addr) as con_addr, trim(a.con_city) as con_city, a.con_st, a.con_zip4, a.con_pg, a.con_aggre, a.auctions_con_location_id,
  trim(a.con_occ) as con_occ, trim(a.con_emp) as con_emp, a.con_emp_city, a.con_emp_st,a.con_type,
  trim(a.buy_name) as buy_name, trim(a.buy_addr) as buy_addr, a.buy_city, a.buy_st, a.buy_zip4, a.buy_pg, a.buy_aggre, a.auctions_buyer_location_id,
  trim(a.buy_occ) as buy_occ, trim(a.buy_emp) as buy_emp, a.buy_emp_city, a.buy_emp_st,a.buy_type,
  trim(a.item_desc) as item_desc,COALESCE(a.fair_mkt, 0.0) as fair_mkt, COALESCE(a.sale_amt, 0.0) as sale_amt
FROM reports r join auct a on a.repno=r.repno
  WHERE r.filer_id = :filer_id and r.election_year = :election_year
order by a.repno, ident