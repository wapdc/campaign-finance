SELECT
    r.repno,
    r.superseded,
    r.filer_id,
    r.election_year,
    r.date_filed,
    r.form,
    COALESCE(C4.from_date, c3.from_date, r.perfrom) as perfrom,
    COALESCE(C4.thru_date, C3.thru_date, r.perthru) as perthru,
    COALESCE(c3.rec_type,c4.rec_type) as record_type,
    COALESCE(c3.form_type,c4.form_type) as form_type,
    COALESCE(c3.entity_cd,c4.entity_cd) as entity_code,
    trim(COALESCE(c3.filer_name,c4.filer_name)) as filer_name,
    COALESCE(c3.address1,c4.address1) as address1,
    COALESCE(c3.address2,c4.address2) as address2,
    COALESCE(c3.city,c4.city) as city,
    COALESCE(c3.state,c4.state) as state,
    COALESCE(c3.zip4,c4.zip4) as postcode,
    COALESCE(c3.office,c4.office) as office_code,
    COALESCE(c3.tres_name,c4.tres_name) as treasurer_name,
    COALESCE(c3.tres_date,c4.tres_date) as treasurer_date,
    COALESCE(c3.tres_phone,c4.tres_phone) as treasurer_phone,
    CASE WHEN c4.final_yn = 'X' then 1
         WHEN c4.final_yn = null then 0
         WHEN c4.final_yn = 'N' then 0
         WHEN c4.final_yn = 'Y' then 1
        END AS final_report,
    c4.cand_name as candidate_name,
    CASE WHEN c4.ind = 'X' then 1
         WHEN c4.ppc_so_yn = 'X' then 1
         WHEN c4.ind IS NULL then 0
         WHEN c4.ppc_so_yn IS NULL then 0
        END AS ind_exp,
    r.howfiled
FROM reports r
         LEFT JOIN C3 c3 ON c3.repno = r.repno
         LEFT JOIN C4 c4 on c4.repno = r.repno
WHERE form IN ('C3', 'C4')
  AND r.filer_id = :filer_id and r.election_year = :election_year
ORDER BY repno desc, r.date_filed;