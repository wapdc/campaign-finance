select
  r.filer_id, r.election_year, l.ident, l.repno, l.rec_type, l.form_type, r.date_filed, l.lend_endrs, trim(l.name) as name, trim(l.address) as address, trim(l.city) as city, l.state, trim(l.zip4) as zip4, l.prim_gen, l.loan_date, coalesce(l.loan_amt, 0.0) as loan_amt, case when l.intr_rate <> 'NA' then l.intr_rate end as intr_rate , l.loan_sched, l.loan_due, coalesce(l.liable_amt,0.0) as liable_amt, coalesce(l.aggregate,0.0) as aggregate, trim(l.employer) as employer, l.occup, trim(l.empl_city) as empl_city, l.empl_state
from loan l join reports r on r.repno = l.repno
where r.election_year=:election_year and r.filer_id=:filer_id
