select r.filer_id, r.election_year,p.ident,p.repno,p.filer_name,p.rec_type,p.form_type,p.name,trim(p.address) as address,trim(p.city) as city,trim(p.state) as state,p.zip4,p.loan_date as date_paid,
       p.orig_amt,p.prin_paid,p.forgiv_amt,COALESCE(p.total_paid, 0.0) as amount,p.bal_owed, p.int_paid, null as expn_desc
from pmts p join reports r on r.repno = p.repno
where r.election_year=:election_year and r.filer_id=:filer_id
