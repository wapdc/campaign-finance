select
    r.election_year,
    t.legacy_form_type,
    sum(p.original_amount) as orig_amt,
    sum(p.principle_paid) as prin_paid,
    sum(t.amount) as total_paid,
    sum(p.forgiven_amount) as forgiv_amt,
    sum(p.interest_paid) as int_paid
from report r
join transaction t on t.report_id = r.report_id
left join payment p on p.transaction_id = t.transaction_id
WHERE r.election_year BETWEEN :start_year AND :end_year
  and legacy_table='pmts'
group by r.election_year, t.legacy_form_type
order by r.election_year desc, t.legacy_form_type

