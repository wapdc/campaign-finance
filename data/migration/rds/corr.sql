select
r.election_year as year,
t.legacy_form_type as form_type,
count(distinct t.report_id) as repno,
COUNT (distinct
    CASE
        WHEN t.category = 'contribution' then
            cont.name
        WHEN t.category = 'expense' then
            e.name end) as name,
count(distinct upper(t.description)) as description,
count(distinct t.transaction_date) as date,
sum(c.original_amount) as org_amt,
sum(c.corrected_amount) as corr_amt,
sum(case when t.act='refund' then t.amount end)
from report r
join transaction t on t.report_id = r.report_id
join correction c on c.transaction_id = t.transaction_id
left join contribution cont on cont.transaction_id = t.transaction_id
left join expense e on e.transaction_id = t.transaction_id
where (r.election_year between :start_year and :end_year)
and t.legacy_table = 'corr'
group by r.election_year, t.legacy_form_type
order by r.election_year desc
