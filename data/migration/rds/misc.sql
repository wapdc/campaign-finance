select
    r.election_year as year,
    count(t.legacy_id) as ident,
    count(distinct r.report_id) as repno,
    count(distinct upper(trim(c.name))) as name,
    count(distinct upper(trim(c.address))) as address,
    count(distinct upper(trim(c.city))) as city,
    count(distinct upper(trim(c.state))) as state,
    count(distinct upper(trim(c.postcode))) as zip,
    count(distinct t.transaction_date) as date,
    count(distinct upper(trim(t.description))) as description,
    sum(t.amount) as amount
from report r
         join transaction t on r.report_id = t.report_id
         join contribution c on c.transaction_id = t.transaction_id
where r.election_year between :start_year and :end_year
  and t.legacy_table = 'misc'
group by r.election_year
order by r.election_year desc
