select
    r.election_year,
    t.legacy_form_type as form_type,
    count(1) as records,
    sum(t.amount) as amount
from report r
         join transaction t on t.report_id = r.report_id
where r.election_year BETWEEN :start_year AND :end_year
and t.legacy_table='rcpt'
group by r.election_year,t.legacy_form_type

