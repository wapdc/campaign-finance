select
    r.election_year as year,
    count(distinct r.repno) as repno,
    count(distinct t.transaction_date) as rcpt_date,
    count(distinct upper(trim(c.name))) as name,
    count(distinct upper(trim(c.address))) as address,
    count(distinct upper(trim(c.city))) as city,
    count(distinct upper(trim(c.postcode))) as zip4,
    count(distinct upper(trim(t.description))) as descrip,
    count(distinct upper(trim(c.employer))) as employer,
    count(distinct upper(trim(c.occupation))) as occup,
    count(distinct upper(trim(c.employer_city))) as empl_city,
    count(distinct upper(trim(c.employer_state))) as empl_state,
    count(distinct c.geocode_hash_key) as location,
    sum(t.amount) as amount,
    sum(rs.aggregate_amount) as aggregate,
    count(distinct c.prim_gen) as prim_gen,
    count(distinct t.legacy_form_type) as form_type,
    count(distinct c.contributor_type) as code
from transaction t
    join report r on r.report_id = t.report_id
    left join contribution c on c.transaction_id = t.transaction_id
    left join report_sum rs on t.transaction_id = rs.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
   and legacy_table='rcpt'
group by r.election_year
order by r.election_year desc

