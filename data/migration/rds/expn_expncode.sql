select
    r.election_year as year,
    e.category as category,
    count(e.transaction_id) as records
from report r
         join transaction t on t.report_id = r.report_id
         join expense e on e.transaction_id = t.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
  and t.legacy_table = 'expn'
group by r.election_year, e.category
order by r.election_year, e.category