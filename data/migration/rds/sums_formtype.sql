select
    r.election_year as year,
    t.legacy_form_type as form_type,
    count(t.transaction_id) as records,
    count(distinct t.transaction_date) as sum_date,
    sum(t.amount) as amount,
    sum(rs.aggregate_amount) as aggregate
from report r
    join transaction t on t.report_id = r.report_id
    join report_sum rs on rs.transaction_id = t.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
  and t.legacy_table = 'sums'
group by r.election_year,t.legacy_form_type


