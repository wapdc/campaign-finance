select
    r.election_year,
    r.record_type as rec_type,
    count(r.report_id) as records,
    count(distinct r.repno) as repno,
    sum(cast(r.repno as float)) as repno_sum,
    count(distinct f.filer_id) as filer_id,
    count(case when r.entity_code is null then 1 end) as null_entity_cd,
    count(case when r.entity_code is not null then 1  end) as other_entity_cd,
    count(distinct upper(trim(r.filer_name))) as filer_name,
    count(case when r.address2 is not null then 1 end) as address2,
    count(case when r.postcode is not null then 1 end) as zip4,
    count(distinct r.office_code) as office,
    count(case when r.treasurer_name is not null then 1 end) as tres_name,
    count(case when r.treasurer_date is not null then 1 end) as tres_date,
    count(case when r.treasurer_phone is not null then 1 end) as tres_phone,
    count(distinct r.period_start) as period_from,
    count(distinct r.period_end) as period_thru,
    count(case when r.final_report = '0' then 0 end) as null_final,
    count(case when r.final_report = '1' then 1 end) as final,
    count(distinct candidate_name) as candidate
from report r
left join fund f on f.fund_id = r.fund_id
WHERE r.election_year BETWEEN :start_year AND :end_year
AND form_type = 'C4'
group by r.election_year, r.record_type
order by 1 desc, 2 desc