select
    r.election_year as year,
    min(t.legacy_id) as min_ident,
    max(t.legacy_id) as max_ident,
    count(t.transaction_id) as records,
    count(distinct t.report_id) as repno,
    count(distinct t.transaction_date) as sum_date,
    sum(t.amount) as amount,
    sum(rs.aggregate_amount) as aggregate,
    min(t.transaction_date) as min_t_date,
    max(t.transaction_date) as max_t_date
from report r
join transaction t on t.report_id = r.report_id
left join report_sum rs on rs.transaction_id = t.transaction_id
WHERE election_year BETWEEN :start_year AND :end_year
  and t.legacy_table = 'sums'
group by r.election_year
order by r.election_year desc


