select
    r.election_year as year,
    t.legacy_form_type as form_type,
    count(e.transaction_id) as records,
    count(distinct t.transaction_date) as expn_date,
    sum(t.amount) as amount
from report r
         join transaction t on t.report_id = r.report_id
         join expense e on e.transaction_id = t.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
  and t.legacy_table = 'expn'
group by r.election_year,t.legacy_form_type