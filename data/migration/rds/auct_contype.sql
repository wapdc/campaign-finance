select
    r.election_year as year,
    c.contributor_type as type,
    count(td.report_id) as records
from auction_item item
         join transaction td on td.transaction_id = item.donation_transaction_id
         join report r on r.report_id = td.report_id
         join contribution c on c.transaction_id = td.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
  and td.legacy_table = 'auct'
group by r.election_year, c.contributor_type
order by r.election_year, c.contributor_type