select
    r.election_year,
    r.form_type,
    sum(c.original_amount) as org_amt,
    sum(c.corrected_amount) as corr_amt,
    sum(case when t.act='refund' then t.amount end) as refund_amt
from report r
         join transaction t on t.report_id = r.report_id
         join correction c on c.transaction_id = t.transaction_id
where (r.election_year between :start_year and :end_year)
and t.legacy_table = 'corr'
group by r.election_year, r.form_type
order by r.election_year desc, r.form_type
