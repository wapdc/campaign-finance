select
    r.election_year,
    count(1) records,
    count(distinct r.repno) as repno,
    sum(cast(r.repno as float)) as repno_sum,
    count(r.superseded_repno) as superseded,
    sum(cast(r.superseded_repno as float)) as superseded_sum,
    count(case when r.superseded_repno is null then 1 end) as null_superseded,
    count(distinct f.filer_id) as filer_id,
    count(case when f.filer_id is null then 1 end) as null_filerid,
    count(distinct r.submitted_at) as date_filed,
    MIN(r.submitted_at) AS min_date_filed,
    MAX(r.submitted_at) AS max_date_filed,
    count(distinct r.form_type) as form
from report r
  left join fund f on f.fund_id = r.fund_id
WHERE election_year BETWEEN :start_year AND :end_year
GROUP BY r.election_year
order by r.election_year desc

