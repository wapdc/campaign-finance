select
    r.election_year as year,
    c.contributor_type as type,
    count(tb.report_id) as records
from auction_item item
         join transaction tb on tb.transaction_id = item.buy_transaction_id
         join report r on r.report_id = tb.report_id
         join contribution c on c.transaction_id = tb.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
  and tb.legacy_table = 'auct'
group by r.election_year, c.contributor_type
order by r.election_year, c.contributor_type