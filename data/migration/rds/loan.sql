select
    r.election_year as year,
    count(t.legacy_id) as ident,
    count(distinct r.report_id) as repno,
    count(distinct t.legacy_form_type) as form_type,
    count(distinct case when t.legacy_form_type is null then 1 end) as null_form_type,
    count(distinct r.record_type) as rec_type,
    count(distinct c.name) as name,
    count(distinct c.address) as address,
    count(distinct upper(trim(c.city))) as city,
    count(distinct c.state) as state,
    count(distinct c.postcode) as zip,
    count(distinct l.loan_date) as loan_date,
    count(distinct l.interest_rate) as int_rate,
    count(distinct l.payment_schedule) as loan_schedule,
    count(distinct l.due_date) as due_date,
    count(distinct c.employer) as employer,
    count(distinct c.occupation) as occupation,
    count(distinct c.employer_city) as empl_city,
    count(distinct c.employer_state) as empl_state,
    sum(amount) as loan_amt,
    sum(liable_amount)  as liable_amt,
    sum(rs.aggregate_amount)  as aggregate
from report r
    join transaction t on r.report_id = t.report_id
    join loan l on l.transaction_id = t.transaction_id
    join contribution c on c.transaction_id = t.transaction_id
    left join report_sum rs on rs.transaction_id = t.transaction_id
where r.election_year between :start_year and :end_year
  and t.legacy_table = 'loans'
group by r.election_year
order by r.election_year desc
