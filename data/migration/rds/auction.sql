select
r.election_year as year,
count(distinct td.legacy_id) as ident,
count(distinct td.report_id) as repno,
count(distinct r.form_type) as form_type,
count(distinct td.transaction_date) as auct_date,
count(distinct upper(cd.name)) as con_name,
count(distinct upper(cd.address)) as con_addr,
count(distinct upper(cd.city)) as con_city,
count(distinct cd.state) as con_state,
count(distinct cd.postcode) as con_zip,
count(distinct upper(cb.name)) as buy_name,
count(distinct upper(cb.address)) as buy_addr,
count(distinct cb.city) as buy_city,
count(distinct cb.state) as buy_state,
count(distinct cb.postcode) as buy_zip,
count(distinct upper(item.item_description)) as item_desc,
count(distinct upper(cd.employer)) as con_employer,
count(distinct upper(cd.occupation)) as con_occ,
count(distinct cd.employer_city) as con_city,
count(distinct cd.employer_state) as con_state,
count(distinct upper(cb.employer)) as buy_employer,
count(distinct upper(cb.occupation)) as buy_occ,
count(distinct cb.employer_city) as buy_city,
count(distinct cb.employer_state) as buy_state,
count(distinct cd.geocode_hash_key) as con_location,
count(distinct cb.geocode_hash_key) as buy_location,
sum(item.fair_market_value) as fair_market,
sum(item.sale_amount) as sale_amount,
sum(rsd.aggregate_amount) as con_aggregate,
sum(rsb.aggregate_amount) as buy_aggregate
from auction_item item
join transaction td on td.transaction_id = item.donation_transaction_id
join transaction tb on tb.transaction_id = item.buy_transaction_id
join report r on r.report_id = td.report_id
join fund f on r.fund_id = f.fund_id
join contribution cb on cb.transaction_id = tb.transaction_id
join contribution cd on cd.transaction_id = td.transaction_id
join report_sum rsd on rsd.transaction_id = td.transaction_id
join report_sum rsb on rsb.transaction_id = tb.transaction_id
where r.election_year between :start_year and :end_year
  and (tb.legacy_table = 'auct' or td.legacy_table = 'auct')
group by r.election_year
order by r.election_year desc
