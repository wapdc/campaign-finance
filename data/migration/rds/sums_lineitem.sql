select
    r.election_year as year,
    rs.legacy_line_item as line_item,
    count(t.transaction_id) as records,
    sum(t.amount) as amount,
    sum(rs.aggregate_amount) as aggregate
from report r
    join transaction t on t.report_id = r.report_id
    join report_sum rs on rs.transaction_id = t.transaction_id
    where r.election_year between :start_year and :end_year
     AND t.legacy_table='sums'
group by r.election_year, rs.legacy_line_item
order by r.election_year desc, rs.legacy_line_item
