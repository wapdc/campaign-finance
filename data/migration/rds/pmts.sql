select
   r.election_year as year,
   count(t.transaction_id) as records,
   count(distinct r.repno) as repno,
   count(distinct upper(trim(e.name))) as name,
   count(distinct upper(trim(e.address))) as address,
   count(distinct upper(trim(e.city))) as city,
   count(distinct upper(trim(e.state))) as state,
   count(distinct upper(trim(e.postcode))) as zip4,
   count(distinct t.transaction_date) as loan_date,
   sum(p.original_amount) as orig_amt,
   sum(p.principle_paid) as prin_paid,
   sum(t.amount) as total_paid,
   sum(p.forgiven_amount) as forgiv_amt,
   sum(p.interest_paid) as int_paid
from report r
join transaction t on r.report_id = t.report_id
left join expense e on e.transaction_id = t.transaction_id
left join payment p on p.transaction_id = t.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
and t.legacy_table = 'pmts'
group by r.election_year
order by r.election_year desc
