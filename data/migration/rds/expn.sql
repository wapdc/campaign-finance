select
 r.election_year as year,
 count(t.legacy_id) as ident,
 count(distinct r.report_id) as repno,
 count(distinct r.record_type) as rec_type,
 count(distinct upper(x.name)) as name,
 count(distinct upper(x.address)) as address,
 count(distinct upper(x.city)) as city,
 count(distinct x.postcode) as zip,
 count(distinct t.transaction_date) as date_paid,
 count(distinct upper(t.description)) as description,
 count(distinct x.geocode_hash_key) as location,
 count(distinct t.legacy_form_type) as form,
 count(distinct x.category) as expn_code,
 sum(t.amount) as amount
from report r
join transaction t on r.report_id = t.report_id
join expense x on x.transaction_id = t.transaction_id
where r.election_year between :start_year and :end_year
and t.legacy_table = 'expn'
group by r.election_year
order by r.election_year desc
