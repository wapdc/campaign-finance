select
    r.election_year as year,
    c.prim_gen,
    count(1) as records,
    sum(t.amount) as amount
from transaction t
         join report r on r.report_id = t.report_id
         left join contribution c on c.transaction_id = t.transaction_id
         left join report_sum rs on t.transaction_id = rs.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
  and legacy_table='rcpt'
group by r.election_year, c.prim_gen
order by r.election_year desc, prim_gen