select
    r.election_year as year,
    count(l.transaction_id) as records,
    c.prim_gen
from report r
         join transaction t on t.report_id = r.report_id
         join loan l on l.transaction_id = t.transaction_id
         left join contribution c on c.transaction_id = t.transaction_id
where r.election_year BETWEEN :start_year AND :end_year
  and t.legacy_table = 'loans'
group by r.election_year, c.prim_gen
order by r.election_year, c.prim_gen
