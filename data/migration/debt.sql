select
    r.filer_id, r.election_year, d.ident, d.repno, d.rec_type, d.form_type, r.date_filed, d.vend_name, trim(d.address) as address, trim(d.city) as city, trim(d.state) as state, d.zip4,
    d.date_paid, COALESCE(d.amount,0.0) AS amount, d.expn_code, trim(d.expn_desc) as expn_desc
from debt d join reports r on r.repno = d.repno
where r.election_year=:election_year and r.filer_id=:filer_id;