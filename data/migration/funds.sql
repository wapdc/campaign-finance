select distinct filer_id, election_year
  from wapdc.dbo.reports r
  where form in ('C3', 'C4')
    AND election_year between :start_year and :end_year
    AND filer_id = COALESCE(:filer_id, filer_id)
order by election_year desc, filer_id
