SELECT
    cc.*,
    c.name as committee_name,
    ROUND(CAST(SIMILARITY(normalized_name(:search), normalized_name(cc.name)) AS NUMERIC), 3) as similarity_name
  FROM committee_contact cc
  left join committee c on cc.committee_id = c.committee_id
  WHERE (SIMILARITY(normalized_name(:search), normalized_name(cc.name)) >= :similarity
    and c.committee_type != 'OS'
  OR (UPPER(cc.address) LIKE '%' || UPPER(:search) || '%')
  OR (cc.phone LIKE '%' || :search || '%')
  OR (UPPER(cc.title) LIKE '%' || UPPER(:search) || '%')
  OR :search = cc.committee_id::text)
  AND cc.role = COALESCE(:role, cc.role)
ORDER BY similarity_name desc;
