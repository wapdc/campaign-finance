SELECT
  cand.candidacy_id,
  comm.committee_id AS committee_id,
  COALESCE(cand.ballot_name,p.name) AS name,
  cand.filer_id AS filer_id,
  comm.filer_id AS filer_id,
  comm.election_code,
  cand.declared_email
FROM candidacy cand
  LEFT JOIN committee comm ON cand.committee_id = comm.committee_id
  LEFT JOIN person p ON cand.person_id = p.person_id
WHERE
  comm.registration_id IS NULL
  AND comm.c1_sync IS NULL
  AND (cand.election_code = :year)