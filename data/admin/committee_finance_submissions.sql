select s.*, r.report_type
from campaign_finance_submission s
  left join report r
    on r.report_id = s.report_id
where s.committee_id=:committee_id
order by submitted_at desc