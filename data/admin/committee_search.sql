select
  c.name as name,
  p.name as person,
  case when upper(p.name) <> upper(ca.ballot_name) then ca.ballot_name end  as ballot_name,
  c.filer_id,
  c.reporting_type,
  c.committee_type,
  c.committee_id,
  c.pac_type,
  c.registered_at,
  c.election_code,
  c.start_year,
  c.end_year,
  c.continuing,
  c.exempt,
  c.bonafide_type,
  e.title as election,
  fo.offtitle as office,
  pos.position_id,
  pos.title as position,
  j.name as jurisdiction,
  cc2.contact_json,
  cc2.treasurer_name,
  cc.address,
  cc.city,
  cc.state,
  cc.postcode,
  prop.prop_json,
  cc.email
from committee c
  left join election e on e.election_code = c.election_code
  left join candidacy ca on ca.committee_id = c.committee_id
  left join person p on p.person_id = ca.person_id
  left join person ps on ps.person_id = c.person_id
  left join committee_contact cc on c.committee_id = cc.committee_id and cc.role='committee'
  left join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
  left join foffice fo ON fo.offcode = ca.office_code
  left join position pos ON pos.position_id = ca.position_id
  left join registration r ON c.registration_id = r.registration_id
  left join (select cr.committee_id,
                    json_agg(json_build_object('title', pro.title, 'number', pro.number, 'stance', cr.stance)) as prop_json,
                    array_agg(jp.name) as p_juris_names,
                    array_agg(jp.category) as p_juris_cat
             from committee_relation cr
    join proposal pro ON cr.target_id = pro.proposal_id and cr.relation_type = 'proposal'
      --@if $proposal_title
      and (fuzzy_match(:proposal_title, pro.title) > :similarity)
      --@end
      --@if $proposal_type
      and :proposal_type = pro.proposal_type
      --@end
      --@if $proposal_number
      and :proposal_number = pro.number
      --@end
    left join jurisdiction jp ON pro.jurisdiction_id = jp.jurisdiction_id
    group by cr.committee_id) as prop ON c.committee_id = prop.committee_id
  left join (select committee_id, STRING_AGG(DISTINCT case when treasurer=true then name end, ', ') treasurer_name,
             json_agg(committee_contact) contact_json
      from committee_contact
      group by committee_id
  ) cc2 on CC2.committee_id=c.committee_id
where
  1=1
  --@if $include_unfiled != 'true'
  and c.filer_id is not null
  --@end
  --@if $election_year
  and c.start_year <= :election_year and (c.end_year >= :election_year or c.end_year is null)
  --@end
  --@if is_array($pac_type) && !empty($pac_type)
  and c.pac_type in (:pac_type)
  --@if $name
  and (
    (fuzzy_match(:name, c.name) > :similarity)
    or (fuzzy_name_match(:name, ca.ballot_name) > :similarity)
    or (fuzzy_name_match(:name, p.name) > :similarity)
    or (fuzzy_name_match(:name, ps.name) > :similarity)
  )
  --@end
  --@if $email
  and (fuzzy_match(:email, cc.email) > 0.8)
  --@end
  --@if $filer_id
  and (fuzzy_match(:filer_id, c.filer_id) > 0.2)
  --@end
  --@if $jurisdiction
  and ((fuzzy_match(:jurisdiction, j.name) > :similarity) or (fuzzy_match_multiple(prop.p_juris_names, :jurisdiction) > :similarity))
  --@end
  --@if $address
  and (fuzzy_address_match(:address, cc.address, cc.city, cc.state, cc.postcode) > :similarity)
  --@end
  --@if $contact_name
  and (fuzzy_name_match(:contact_name, cc2.treasurer_name) > :similarity)
  --@end
  --@if $jurisdiction_category
  and (:jurisdiction_category = j.category or :jurisdiction_category = any(prop.p_juris_cat))
  --@end
  --@if $office_code
  and :office_code = fo.offcode
  --@end
  --@if $submitted_days
  and r.submitted_at >= (now() - cast( (:submitted_days || ' days') as interval))
  --@end
  --@if $proposal_title || $proposal_type || $proposal_number
  and prop.committee_id is not null
  --@end
  and c.committee_type <> 'OS'
  order by
  --@if $name
    greatest(fuzzy_match(:name, c.name),
      fuzzy_name_match(:name, ca.ballot_name),
      fuzzy_name_match(:name, p.name),
      fuzzy_name_match(:name, ps.name))desc,
  --@end
  --@if $filer_id
  fuzzy_match(:filer_id, c.filer_id) desc,
  --@end
  --@if $email
    fuzzy_match(:email, cc.email) desc,
  --@end
  --@if $contact_name
  fuzzy_name_match(:contact_name, cc.name) desc,
  --@end
  --@if $jurisdiction
    fuzzy_match(:jurisdiction, j.name) desc,
  --@end
  --@if $address
    fuzzy_address_match(:address, cc.address, cc.city, cc.state, cc.postcode) desc,
  --@end
  --@if $submitted_days
    r.submitted_at desc,
  --@end
    c.start_year desc,
    c.name
limit 5000
