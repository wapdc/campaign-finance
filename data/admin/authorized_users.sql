SELECT u.*, a.role, a.granted_date, a.expiration_date  FROM pdc_user u JOIN
                (SELECT uid, realm, role, granted_date, expiration_date FROM committee_authorization
                 WHERE committee_id = :committee_id
                 AND (expiration_date is null or expiration_date >= now() or (uid = 'PDC' and expiration_date >= current_date))) a
                ON u.uid = a.uid
                AND u.realm = a.realm
                ORDER BY CASE WHEN a.role ILIKE 'OWNER' THEN 0 ELSE 1 END, u.user_name;