select r.report_id, max(r.submitted_at) as submitted_at, sum(t.amount) as amount
from public.report ro
         join public.report r on ro.fund_id = r.fund_id and ro.report_type = r.report_type
         join transaction t on t.report_id = r.report_id and t.act = 'sum'
         left join public.report_sum rs on rs.transaction_id = t.transaction_id
where
  ro.period_start = r.period_start
  and ro.report_id = :report_id
  and ro.report_id <> r.report_id
  and ro.report_type = 'C3'
group by r.report_id
order by max(r.submitted_at);