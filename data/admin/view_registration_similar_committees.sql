select
  sr.*
FROM
(
  SELECT
    e.entity_id,
    e.name,
    e.filer_id,
    SIMILARITY(UPPER(:compare_name), UPPER(e.name)) as similarity_value,
    jv.jurisdiction
  FROM entity e join committee cor ON cor.committee_id = :committee_id
    left join (select  c.person_id, max(j.name) as jurisdiction from committee c
       join committee_relation cr on c.committee_id=cr.committee_id and c.continuing is not true
       join proposal p on p.proposal_id = cr.target_id and cr.relation_type='proposal'
       join jurisdiction j on p.jurisdiction_id = j.jurisdiction_id
    group by person_id
    ) jv on jv.person_id=e.entity_id

  WHERE (SIMILARITY(UPPER(:compare_name), UPPER(e.name)) > 0.4
    OR length(:compare_name) > 3 AND UPPER(e.name) LIKE '%' || UPPER(:compare_name) || '%')
  AND e.entity_id not in (select c2.person_id from committee c2 where c2.committee_type = 'OS')
) sr
ORDER BY sr.similarity_value DESC