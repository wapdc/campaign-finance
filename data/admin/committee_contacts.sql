select 'Candidacy' as title, ca.ballot_name as name, ca.address, ca.city, ca.state, ca.postcode, ca.declared_email as email, null as phone
from candidacy ca
where ca.committee_id=:committee_id
union all
SELECT coalesce(cc.title, initcap(cc.role)) as title, cc.name, cc.address, cc.city, cc.state, cc.postcode, cc.email, cc.phone
FROM committee_contact cc
  LEFT JOIN committee c
    ON c.committee_id = cc.committee_id
WHERE c.committee_id=:committee_id
  and cc.role in ('committee','officer')