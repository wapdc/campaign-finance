select p.name, c.ballot_name, c.candidacy_id, c.election_code, o.title as office_title, j.name as jurisdiction_name,
       p.address, p.city, p.state, p.postcode, c.filer_id
from person p
inner join candidacy c on c.person_id = p.person_id
inner join jurisdiction j on c.jurisdiction_id = j.jurisdiction_id
inner join jurisdiction_office o on c.jurisdiction_id = o.jurisdiction_id and c.office_code = o.offcode
left join position po on c.position_id = po.position_id
where p.person_id = :person_id
order by c.election_code;
