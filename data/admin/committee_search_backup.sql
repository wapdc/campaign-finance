select
  c.name,
  COALESCE(ca.ballot_name, p.name) as ballot_name,
  COALESCE(c.filer_id,'') as filer_id,
  COALESCE(c.reporting_type,'') AS reporting_type,
  c.committee_type,
  c.committee_id,
  c.pac_type,
  COALESCE(c.election_code, '') AS election_code,
  c.start_year,
  c.end_year,
  COALESCE(e.title,'') as election_title,
  greatest(
    similarity(upper(:search), upper(c.name)),
    similarity(upper(:search), upper(ca.ballot_name)),
    similarity(upper(:search), upper(p.name)),
    similarity(upper(:search), upper(c.filer_id))
    ) as similarity_name
from committee c
  left join election e on e.election_code = c.election_code
  left join candidacy ca on ca.committee_id = c.committee_id
  left join person p on p.person_id = ca.person_id
where (cast(:election_code as varchar) is null or cast(:election_code as varchar) like substring(c.election_code,1,5))
  and (
    cast(:search as varchar) is null
    or (upper(c.name) like '%' || upper(:search) || '%')
    or (similarity(upper(:search), upper(c.name)) > :similarity)
    or (similarity(upper(:search), upper(ca.ballot_name)) > :similarity)
    or (similarity(upper(:search), upper(c.filer_id)) > :similarity_filer_id)
  )
  order by greatest(
    similarity(upper(:search), upper(c.name)),
    similarity(upper(:search), upper(ca.ballot_name)),
    similarity(upper(:search), upper(p.name)),
    similarity(upper(:search), upper(c.filer_id))
  ) desc
