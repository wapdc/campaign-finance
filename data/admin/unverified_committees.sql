select r.registration_id, c.committee_id, c.name, c.election_code, r.submitted_at, c.committee_type,
       c.pac_type, r.verified, c.filer_id from registration r
inner join committee c on r.committee_id = c.committee_id and c.registration_id = r.registration_id
where (r.verified = false OR r.verified IS NULL) and c.pac_type != 'out-of-state'
