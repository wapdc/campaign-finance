SELECT c.committee_id, com.name,  ct.phone, ct.email, com.start_year, com.end_year
FROM candidacy c
join committee com
  on com.committee_id = c.committee_id
left join committee_contact ct
  on ct.committee_id = c.committee_id
    and ct.role = 'committee'
where position_id is null
  and c.election_code = :election_year
AND ( c.office_code, c.jurisdiction_id)
in (select offcode, jurisdiction_id
  FROM jurisdiction_office jo
  join position p on p.jurisdiction_office_id = jo.jurisdiction_office_id
  group by jo.offcode, jo.jurisdiction_id
  having count(1) > 1);