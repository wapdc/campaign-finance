SELECT
    c.candidacy_id,
    c.person_id,
    coalesce(c.ballot_name, p.name) as ballot_name,
    COALESCE(c.filer_id, p.filer_id) as filer_id,
    p.address,
    p.city,
    p.state,
    p.postcode,
  case
    WHEN c.ballot_name is null then p.name end as name
FROM candidacy c JOIN person p ON c.person_id = p.person_id
WHERE (SIMILARITY(UPPER(:search), UPPER(ballot_name)) > :similarity
  OR SIMILARITY(UPPER(:search), UPPER(name)) > :similarity)
  AND election_code = :election_code