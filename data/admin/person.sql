SELECT
  p.person_id,
  p.name,
  COALESCE(ca.filer_id, p.filer_id) as filer_id,
  CASE WHEN p.filer_id IS NOT NULL
    AND cap.filer_id IS NOT NULL THEN true
    ELSE false
    END as N_FILER
FROM person p
  LEFT JOIN candidacy ca ON ca.person_id = p.person_id
    AND election_code = :election_code
    AND ca.office_code = :office_code
    AND jurisdiction_id = :jurisdiction_id
  LEFT JOIN candidacy cap ON cap.person_id = p.person_id
    AND cap.filer_id = p.filer_id
    AND cap.election_code = :election_code
    AND (cap.office_code <> :office_code OR cap.jurisdiction_id <> :jurisdiction_id)
WHERE (fuzzy_name_match(:search, p.name) > :similarity)
  OR (fuzzy_name_match(:search, ca.ballot_name) > :similarity)
ORDER BY greatest(fuzzy_name_match(:search, p.name),
  fuzzy_name_match(:search, ca.ballot_name)) DESC,
  p.person_id asc
