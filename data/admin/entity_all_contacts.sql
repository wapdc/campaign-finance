select distinct email, address, city, state, postcode, source from
    (select declared_email as email, address, city, state, postcode, concat('Candidacy ', e.title) as source
     from candidacy left join election e on candidacy.election_code = e.election_code where person_id = :entity_id
     union all select email, address, city, state, postcode, 'Official' as source
     from official where person_id = :entity_id
     union all select email, address, city, state, postcode, 'Person' as source
     from person where person_id = :entity_id
     union all select email, trim(coalesce(address_1, '') || ' ' || coalesce(address_2, '')) as address,  city, state, postcode, 'Lobbyist employer' as source from lobbyist_employer where entity_id=:entity_id
     union all select email, trim(coalesce(address_1, '') || ' ' || coalesce(address_2, '')) as address, city, state, postcode, 'Lobbyist Firm' as source from lobbyist_firm where entity_id=:entity_id
     union all select email, address, city, state, postcode,
                      concat('Committee ', case when c.pac_type = 'surplus' then 'surplus ' end, string_agg(cast(c.start_year as text), ', ')) as source
     from committee_contact cc join committee c on c.committee_id = cc.committee_id left join election e2 on c.election_code = e2.election_code
     where c.person_id = :entity_id and cc.role in ('committee','candidacy')
     group by c.person_id, cc.email, cc.address, cc.city, cc.state, cc.postcode, c.pac_type
    ) v
where email is not null or address is not null
order by source desc
