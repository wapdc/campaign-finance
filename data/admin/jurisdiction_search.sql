select jurisdiction_id, name, round(100 * similarity(:search, name)) as name_similarity, county
from jurisdiction
where similarity(:search, name) > .2
order by name_similarity desc
