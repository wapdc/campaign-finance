select coalesce(r.name, c.name) as name, r.memo, r.verified, r.submitted_at, r.registration_id
from registration r join committee c on r.committee_id = c.committee_id
where r.committee_id = :committee_id
