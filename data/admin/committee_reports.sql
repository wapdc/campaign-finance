select r.*, cfs.processed, cfs.message, ra.report_id as amends
from fund f join report r ON r.fund_id = f.fund_id
  JOIN election e ON F.election_code = e.election_code
  LEFT JOIN campaign_finance_submission cfs ON r.report_id = cfs.report_id
  LEFT JOIN report ra ON ra.superseded_id = r.report_id
where
  f.committee_id = :committee_id
  order by r.period_end desc, r.repno desc, r.submitted_at desc