select COALESCE(e.name, trim(coalesce(s.fname, '') || s.name)) as name, s.sponsor_id,
       fuzzy_name_match(:search,  COALESCE(e.name, trim(coalesce(s.fname, '') || s.name))) as similarity
from c6_sponsor s
  left join entity e on s.entity_id = e.entity_id
-- exclude entities we already have access to.
where s.entity_id not in
  (Select entity_id from wapdc.public.entity_authorization where uid=:uid and realm = :realm)
  and fuzzy_name_match(:search,  COALESCE(e.name, trim(coalesce(s.fname, '') || s.name))) >= 0.3
order by 3 desc