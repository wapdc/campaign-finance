select e.entity_id, e.name, e.filer_id from
  entity e left join c6_sponsor s on s.entity_id=e.entity_id
  where
    s.sponsor_id is null
    and e.entity_id in
        (select c.person_id from committee c join committee_authorization ca on c.committee_id = ca.committee_id
          where c.committee_type='CO' and ca.uid=:uid and ca.realm=:realm and c.filer_id is not null)

