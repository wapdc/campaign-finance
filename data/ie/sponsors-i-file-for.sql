SELECT e.entity_id, s.name, s.sponsor_id, e.email, e.address, e.city, e.state, e.postcode, e.phone, s.sponsor_desc as sponsor_type
  FROM c6_sponsor s JOIN entity e on e.entity_id=s.entity_id
    JOIN entity_authorization a on a.entity_id=e.entity_id
    where uid = :uid
      and realm = :realm;
