select r.*, pu.user_name from c6_sponsor_request r
    join pdc_user pu on r.realm = pu.realm and pu.uid=r.uid
where r.uid = :uid
  and r.realm = :realm;
