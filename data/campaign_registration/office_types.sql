select o.offtitle, o.offcode
from foffice o
where offcode in
  (select jo.offcode
   from jurisdiction_office jo
     join jurisdiction j on j.jurisdiction_id = jo.jurisdiction_id
   where j.category not in (14, 15, 16))
  and o.on_off_code = true
order by o.offtitle