select r.jurisdiction_id,
  r.name,
  json_agg(
    json_build_object('offcode', r.offcode, 'offtitle', r.offtitle, 'partisan', r.partisan)
  ) as offices
from (
  select j.jurisdiction_id, j.name, o.offcode, o.offtitle, jo.partisan
  from jurisdiction j
    join jurisdiction_office jo on j.jurisdiction_id = jo.jurisdiction_id
    join foffice o on jo.offcode = o.offcode
  where j.inactive is null
    and j.valid_to is null
    and jo.valid_to is null
  order by j.name
) r
group by r.jurisdiction_id, r.name
