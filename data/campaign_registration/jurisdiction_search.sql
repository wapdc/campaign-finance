SELECT
  jurisdiction_id,
  name,
  short_name
FROM jurisdiction
WHERE
  inactive IS NULL AND
  (valid_to is null or valid_to > now()::date - interval '1 year 1 week') and
  (CAST(:search AS VARCHAR) IS NULL
    OR (SIMILARITY(UPPER(:search), UPPER(name)) > .3)
    OR (SIMILARITY(UPPER(:search), UPPER(short_name)) > .3) )
  AND
  (CAST(:jurisdiction_category AS INTEGER) IS NULL
   OR :jurisdiction_category = category)
