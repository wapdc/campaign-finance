select p.name, c.ballot_name, c.candidacy_id, c.election_code, o.offtitle as office_title, j.name as jurisdiction_name, e.title as election,
       p.address, p.city, p.state, p.postcode
from person p
         inner join candidacy c on c.person_id = p.person_id
         inner join foffice o on c.office_code = o.offcode
         inner join jurisdiction j on c.jurisdiction_id = j.jurisdiction_id
         inner join election e on c.election_code = e.election_code
         left join position po on c.position_id = po.position_id
where similarity(upper(:search), upper(p.name)) > :similarity and (cast(:election_code as varchar) = c.election_code or
        cast(:election_code as varchar) is null)
order by c.election_code desc, p.name

