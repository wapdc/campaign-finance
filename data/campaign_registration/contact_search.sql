select
  cc.*,
  c.name as committee_name,
  round(cast(similarity(upper(:search), upper(cc.name)) as numeric), 3) as similarity_name
from committee_contact cc
    left join committee c on cc.committee_id = c.committee_id
where cc.role = coalesce(:role, cc.role)
  and c.committee_type != 'OS'
  and coalesce(cc.treasurer, false) = coalesce(:treasurer, coalesce(cc.treasurer, false))
  and (
    similarity(upper(:search), upper(cc.name)) >= :similarity
    or (upper(cc.address) like '%' || upper(:search) || '%')
    or (cc.phone like '%' || :search || '%')
    or (upper(cc.title) like '%' || upper(:search) || '%')
    or :search = cc.committee_id::text
  )
order by similarity_name desc;

