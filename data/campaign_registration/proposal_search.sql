select j.name as jurisdiction_name, p.title, p.jurisdiction_id, p.election_code, p.number, p.proposal_type, p.proposal_id
from proposal p
  join jurisdiction j on j.jurisdiction_id = p.jurisdiction_id
where (CAST(:search AS VARCHAR) IS NULL OR (SIMILARITY(UPPER(:search), UPPER(p.title)) > .3))
  and ((CAST(:election_code AS varchar) IS NULL) OR (:election_code = p.election_code))
  and j.inactive is null
  and (j.valid_to is null or j.valid_to > now()::date - interval '1 year 1 week')
