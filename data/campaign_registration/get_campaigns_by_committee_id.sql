SELECT f.fund_id, c2.name, f.election_code, c.committee_id,
       concat(c2.name, case when c.continuing is distinct from true
                        and c.pac_type != 'candidate' then concat( ' - ', e.title)
           else concat(' ', f.election_code) end) AS campaign_name
FROM committee c
    JOIN committee_authorization ca ON ca.committee_id = c.committee_id
    JOIN pdc_user pu ON pu.uid = ca.uid AND ca.realm = pu.realm
    JOIN committee c2 ON c2.person_id = c.person_id
    JOIN fund f ON c2.committee_id = f.committee_id
    LEFT JOIN private.campaign_fund cf ON cf.fund_id = f.fund_id
    LEFT JOIN private.properties p ON p.fund_id = f.fund_id AND p.name = 'RDS_TO_DERBY'
    LEFT JOIN election e on e.election_code = c.election_code
        WHERE c.committee_id = :committee_id
        AND pu.uid = :current_user
        AND pu.realm = :current_realm
        --Done for those election codes with 'S#' appended
        AND substring(f.election_code, 1, 4) <= substring(:election_code::text, 1, 4)
        AND (cf.version is not null OR p.name is not null)
        ORDER BY f.election_code DESC;
