select
  c.committee_id,
  c.name,
  c.election_code,
  e.title,
  c.start_year,
  c.end_year,
  cd.committee_id as draft,
  r.verified as registration_verified,
  r.registration_id,
  case
      WHEN can.office_code is null then null
      WHEN j.name is not null then initcap(concat(j.name, ' - ', fo.offtitle))
      else initcap(fo.offtitle)
  end as position,
  case
    when cd.committee_id is not null then '3'
    when r.verified = true or c.registration_id is null then '1'
    else '2'
  end as verified_sort
from pdc_user u
  join committee_authorization a on a.uid = u.uid
    and a.realm = u.realm
    and (a.expiration_date is null or expiration_date > NOW())
  join committee c on a.committee_id = c.committee_id
  left join candidacy can on c.committee_id = can.committee_id
  left join jurisdiction j on j.jurisdiction_id = can.jurisdiction_id
  left join foffice fo on fo.offcode = can.office_code
  left join election e on e.election_code = c.election_code
  left join committee_registration_draft cd on c.committee_id = cd.committee_id
  left join registration r on c.registration_id = r.registration_id

where u.realm = :current_realm
  and u.uid = :current_user;


