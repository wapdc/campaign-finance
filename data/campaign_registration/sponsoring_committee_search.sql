SELECT name, filer_id, reporting_type, committee_type, committee_id, election_code
FROM committee
WHERE
    (CAST(:search AS VARCHAR) IS NULL OR (SIMILARITY(UPPER(:search), UPPER(NAME)) > .3))
ORDER BY name
LIMIT 50

