-- @deprecated
-- There is a jurisdiction_all search that is similar to this that would be best for future use
SELECT j.name, jo.jurisdiction_office_id, j.jurisdiction_id, o.offtitle, jo.partisan
FROM jurisdiction j
  JOIN jurisdiction_office jo ON j.jurisdiction_id = jo.jurisdiction_id
  JOIN foffice o ON jo.offcode = o.offcode
WHERE o.offcode::int = :office_code
  and j.inactive is null
  and (j.valid_to is null or j.valid_to > now()::date - interval '1 year 1 week')
  and (jo.valid_to is null or jo.valid_to > now()::date - interval '1 year 1 week')
ORDER BY j.name