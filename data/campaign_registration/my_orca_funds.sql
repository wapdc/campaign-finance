
select cf.fund_id, f.election_code, ca.committee_id, c.name, c.start_year, cf.version, e.title
from pdc_user pu
    join committee_authorization ca on pu.realm = ca.realm and pu.uid = ca.uid
    join committee c on ca.committee_id = c.committee_id
    join fund f on c.committee_id = f.committee_id
    join private.campaign_fund cf on cf.fund_id = f.fund_id
    left join election e on e.election_code = c.election_code
where pu.uid = :current_user AND pu.realm = :current_realm AND cf.version >= 1.500
ORDER BY f.election_code DESC;
