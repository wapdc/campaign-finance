select p.*
from jurisdiction_office jo
  join position p on p.jurisdiction_office_id = jo.jurisdiction_office_id
where jo.jurisdiction_id = :jurisdiction_id
  and jo.offcode::int = :office_code
  and (p.valid_to is null or p.valid_to > now())
order by p.title
