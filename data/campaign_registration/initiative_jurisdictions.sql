SELECT j.name, j.jurisdiction_id
FROM jurisdiction j
WHERE j.initiative = TRUE
  and j.inactive is null
  and (j.valid_to is null or j.valid_to > now()::date - interval '1 year 1 week')
ORDER BY (CASE WHEN j.name='State of Washington' THEN 1 ELSE 2 END), j.name