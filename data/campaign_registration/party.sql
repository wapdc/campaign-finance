select * from party where valid_to > (current_date - interval '7 months') or valid_to is null
  AND NAME not ilike 'NON PARTISAN'
order by CASE WHEN name ILIKE 'NONE' then 0
  WHEN name ILIKE 'OTHER' THEN 2 else 1 end, name;