select
  -- uncomment when selecting invalids
  *
  -- uncomment when selecting totals
  -- postcode_category, postcode_status, count(*)
from (
  -- report postcode
  select r.report_id, null as transaction_id, 'report' as postcode_category, r.postcode,
    case
      when (r.postcode ~ '^[0-9]{5}-?(?:[0-9]{4})?$') then 'valid'
      when (r.postcode is null or r.postcode = '') then 'empty'
      else 'invalid'
    end as postcode_status
  from report r
  where r.report_id > 110000000
    and r.report_id < 999999999
  -- expense postcode
  union all
  select t.report_id, t.transaction_id, 'expense' as postcode_category, e.postcode,
    case
      when (e.transaction_id is not null and e.postcode ~ '^[0-9]{5}-?(?:[0-9]{4})?$') then 'valid'
      when (e.transaction_id is not null and (e.postcode is null or e.postcode = '')) then 'empty'
      when (e.transaction_id is not null) then 'invalid'
      else null
    end as postcode_status
  from expense e
    join transaction t
      on e.transaction_id = t.transaction_id
  where t.report_id > 110000000
    and t.report_id < 999999999
  -- contribution postcode
  union all
  select t.report_id, c.transaction_id, 'contribution' as postcode_category, c.postcode,
    case
      when (c.transaction_id is not null and c.postcode ~ '^[0-9]{5}-?(?:[0-9]{4})?$') then 'valid'
      when (c.transaction_id is not null and (c.postcode is null or c.postcode = '')) then 'empty'
      when (c.transaction_id is not null) then 'invalid'
      else null
    end as postcode_status
  from contribution c
    join transaction t
      on c.transaction_id = t.transaction_id
  where t.report_id > 110000000
    and t.report_id < 999999999
  -- contribution employer postcode
  union all
  select t.report_id, c.transaction_id, 'contribution_employer' as postcode_category, c.postcode,
    case
      when (c.transaction_id is not null and c.employer_postcode ~ '^[0-9]{5}-?(?:[0-9]{4})?$') then 'valid'
      when (c.transaction_id is not null and (c.employer_postcode is null or c.employer_postcode = '')) then 'empty'
      when (c.transaction_id is not null) then 'invalid'
      else null
    end as postcode_status
  from contribution c
    join transaction t
      on c.transaction_id = t.transaction_id
  where t.report_id > 110000000
    and t.report_id < 999999999
  ) v
-- uncomment when selecting invalids
where postcode_status = 'invalid'
-- uncomment when selecting counts
-- group by postcode_category, postcode_status
;
