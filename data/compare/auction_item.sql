SELECT
  a.fair_market_value,
  UPPER(a.item_description),
  a.sale_amount
FROM auction_item a JOIN transaction t ON a.donation_transaction_id = t.transaction_id
WHERE report_id = :report_id
ORDER BY
  t.category, t.act, upper(a.item_description), a.fair_market_value, a.sale_amount
