select
  case when rs.legacy_line_item is not null then t.amount end amount,
  rs.legacy_line_item,
  -- loan transactions dont populate aggregates properly
  case when t.category = 'loan' and rs.legacy_line_item is null then null
    else COALESCE(rs.aggregate_amount, 0.00) end as aggregate_amount
from transaction t
  JOIN report_sum rs ON t.transaction_id = rs.transaction_id JOIN report r ON r.report_id = t.report_id
WHERE r.report_id=:report_id AND (legacy_line_item IS NULL OR legacy_line_item != '2' OR r.report_type = 'C3')
order by CASE WHEN rs.legacy_line_item is not null then rs.legacy_line_item else t.category||t.act end,
          rs.aggregate_amount