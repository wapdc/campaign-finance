select
  t.category,
  t.act,
  t.amount,
  t.transaction_date,
  -- loan table did not have any in-kind indicator
  case when category='loan' and act='receipt' then NULL
    else t.inkind end as inkind
from transaction t
left join report_sum rs on t.transaction_id = rs.transaction_id
where legacy_line_item is null
  and t.report_id = :report_id
  and t.amount <> 0
order by t.category, t.act, t.amount, t.transaction_date
