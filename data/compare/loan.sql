SELECT
  lower(l.payment_schedule) as payment_schedule,
  l.interest_rate,
  l.lender_endorser,
  l.liable_amount,
  l.loan_date
FROM loan l join transaction t ON l.transaction_id = t.transaction_id
WHERE t.report_id=:report_id
  ORDER BY t.category, t.act, l.lender_endorser, l.loan_date, l.liable_amount