select
-- the names in this query should run through the normalize function
 r.election_year,
 r.fund_id,
 r.period_start,
 r.period_end,
 r.report_type,
 normalized_name(r.treasurer_name) as treasurer_name,
 CASE WHEN r.postcode IS NULL THEN 'empty' ELSE 'valid' end as postcode,
 UPPER(trim(r.address1)) as address1,
 UPPER(r.address2) as address2,
 UPPER(r.city) as city,
 upper(r.state) as state,
 CASE
   -- Sometimes we get better data than the legacy system so only report "empty" for non-legacy candidate committees
   WHEN r.candidate_name is null and r.superseded_id is null and c.committee_type='CA' then 'empty'
   when trim(r.candidate_name) = '' then 'invalid'
   ELSE 'valid' end as  candidate_name,
--  r.entity_code, -- this is up for debate.
CASE
    WHEN r.filer_name is null then 'empty'
    WHEN trim(r.filer_name) <> r.filer_name THEN 'invalid'
    WHEN r.filer_name IS NOT NULL THEN 'valid'
END as filer_name,
CASE
    WHEN r.final_report IS NULL THEN false
    ELSE r.final_report
END as final_report,
 r.superseded_id
 from report r
    left join fund f on f.fund_id = r.fund_id
    left join committee c on c.committee_id = f.committee_id
where report_id=:report_id
