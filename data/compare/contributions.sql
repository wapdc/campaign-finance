select
    act,
    case
      when t.act = 'correction' then null
      else lower(c.city)
    end as city,
    case
      when t.act = 'correction' then null
      else c.state
    end as state,
    case when r.election_year = '2016' and c.name is not null  then 'valid'
      when r.election_year = '2016' and c.name is null then 'empty'
      else translate(lower(c.name), '. ', '')
    end as name,
    case
        when t.act like '%miscellaneous%' then null
        when t.act = 'correction' then null
        when r.superseded_id is not null then COALESCE(c.prim_gen, 'N')
        else c.prim_gen
    end as prim_gen,
  case
    -- corrections had no address data on original migration
    when t.act = 'correction' then null
    -- make sure we always have contributor type for new reports
    when r.superseded_id is null and c.contributor_type is null then 'invalid'
    -- auction tables did not include contributor type
    when act like 'auction%' then 'valid'
    -- Loan tables did not include contributor type
    when category = 'loan' and act='receipt' then 'valid'
    -- Misc tables did not include contributor type
    when act like 'miscellaneous%' then 'valid'
    else c.contributor_type
  end as contributor_type,
  case
    when act = 'miscellaneous' then 'valid'
    when act = 'correction' then 'valid'
    else nullif(lower(c.employer), '')
  end as employer,
  case
    when act = 'correction' then 'valid'
    else c.employer_address
  end as employer_address,
  case
    when act ilike '%auction%' then 'valid'
    when act = 'miscellaneous' then 'valid'
    when act = 'correction' then 'valid'
    else lower(c.employer_city)
  end as employer_city,
  case
    when act ilike '%auction%' then 'valid'
    when act = 'miscellaneous' then 'valid'
    when act = 'correction' then 'valid'
    else lower(c.employer_state)
  end as employer_state,
  case
    when act = 'correction' then 'valid'
    else c.employer_postcode
  end as employer_postcode
from transaction t
   join contribution c on t.transaction_id = c.transaction_id
   join report r on r.report_id = t.report_id
where t.report_id = :report_id
  and t.amount <> 0
order by t.category, t.act, t.inkind, lower(c.name), t.amount, c.prim_gen, lower(c.city), lower(employer)