SELECT
    p.balance_owed,
    p.forgiven_amount,
    p.interest_paid,
    p.original_amount,
    p.principle_paid
FROM payment p JOIN transaction t ON p.transaction_id = t.transaction_id
               left join contribution c ON t.transaction_id = c.transaction_id
WHERE report_id = :report_id
ORDER BY
    t.category, t.act, c.name, p.interest_paid, p.principle_paid, p.forgiven_amount