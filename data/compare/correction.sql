SELECT
  c.original_amount,
  c.corrected_amount
FROM correction c JOIN transaction t ON c.transaction_id = t.transaction_id
WHERE report_id = :report_id
  and (original_amount is not null or corrected_amount is not null)
  and original_amount <> corrected_amount
ORDER BY t.transaction_date, t.category, t.act, c.original_amount, c.corrected_amount