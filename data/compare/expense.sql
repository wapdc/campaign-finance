SELECT
    e.category,
    translate(lower(e.name), ' ?', '')  as name,
    case
      when t.act in ('correction', 'refund') then null
      else upper(e.city)
    end as city,
    case
      when t.act in ('correction', 'refund') then null
      else e.state
    end as state
FROM expense e join transaction t ON e.transaction_id = t.transaction_id
WHERE report_id = :report_id
  and t.amount <> 0
ORDER BY t.category, t.act, lower(e.name), t.amount, lower(t.description), lower(e.city), e.category