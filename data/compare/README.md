# Submission Comparison Notes

## Differences we intentionally ignore

** Postcode on everything - The postcode comparison was simplified to whether the postcode existed or not, but even that proved problematic. In the legacy system, when a postcode did not pass schema validation (including empty strings and Canadian postcodes) it was replaced by 00000. Because of that, and the myriad formats a postcode could appear in PDC data, it was decided to ignore the postcode differences during validation. Instead, we decided to research the number of transactions with postcodes after report processing.

** Address data on corrections - The legacy corr table contains no address data, but that data is now populated via the contact id that is passed to the soap processor.

** Employer information on misc - The legacy misc table contains no employer information, but that data is now populated via the contact id that is passed to the soap processor.

** Office code on contribution reports - The legacy system accepted whatever office code the user submitted, while the new system populates the correct office code.

