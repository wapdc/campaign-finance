select e.*, s.sponsor_desc as sponsor_type, s.sponsor_id, v.saw_email from
  entity e left join c6_sponsor s on e.entity_id = s.entity_id
    left join (select ea.entity_id, string_agg(distinct pu.user_name, ', ') as saw_email from entity_authorization ea
    join pdc_user pu on ea.realm = pu.realm and ea.uid = pu.uid
    group by ea.entity_id) v on e.entity_id = v.entity_id
where (fuzzy_name_match(:search, e.name) > 0.3)
order by fuzzy_name_match(:search, e.name) desc;
