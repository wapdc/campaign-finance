-- List all transactions affecting a particular account.
select r.*,
    case when r.did = ac.trankeygen_id then -1 else 1 end * amount * g.negate as account_amount,
    case when r.did = ac.trankeygen_id then -1 else 1 end as direction
from
    private.vaccounts ac
        join private.account_groups g on ac.acctnum = g.acctnum
        join private.vreceipts r on r.fund_id=ac.fund_id
        and (r.did = ac.trankeygen_id or r.cid = ac.trankeygen_id)
where ac.fund_id=:fund_id
  and (cast(:date as date) is null or r.date_ <= :date)
   and ac.trankeygen_id = :account_id
order by R.date_
