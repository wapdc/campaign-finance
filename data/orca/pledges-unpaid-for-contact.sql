select r.*, a.total
from private.vreceipts r
         join private.accounts a on r.cid=a.trankeygen_id
where type=12
  and r.fund_id=:fund_id and r.contid=:contid
  and a.total > 0
