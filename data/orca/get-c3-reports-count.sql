select count(1) as items
from private.vdepositevents d left join public.report r on r.report_id = d.report_id
where d.fund_id = :fund_id
and r.report_id is null
and d.date_ <= now()