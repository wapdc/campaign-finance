select r.*, a.total from private.vaccounts a
    join private.vreceipts r on a.trankeygen_id = r.cid
where r.fund_id = :fund_id
and type = 12
and r.contid = :contid;