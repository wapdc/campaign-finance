select v.*, c.street, c.city, c.state, c.zip
from private.vreceipts v
  left join private.contacts c on c.trankeygen_id=v.contid
where fund_id=:fund_id
  and v.type in (1, 2, 4, 6, 11, 13, 16, 17, 27)
  and (:start_date <= v.date_ or cast(:start_date as date) is null)
  and (:end_date >= v.date_ or cast(:end_date as date) is null)
  and (:min_value <= v.amount or cast(:min_value as numeric) is null)
  and (:max_value >= v.amount or cast(:max_value as numeric) is null)
  and (:type = v.type or cast(:type as numeric) is null)
  and (fuzzy_name_match(:name, v.name) > 0.4 or cast(:name as varchar) is null)
  and (v.elekey = :elekey or cast(:elekey as varchar) is null)
order by date_ desc, trankeygen_id desc;
