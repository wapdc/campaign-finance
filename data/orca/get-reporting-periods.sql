select
    startdate,
    enddate
from private.reporting_obligation
where fund_id = :fund_id AND obligation_id = :obligation_id
