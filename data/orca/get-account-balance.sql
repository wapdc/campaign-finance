select a.total, a.acctnum, a.trankeygen_id from private.accounts a
    join private.trankeygen t on t.trankeygen_id = a.trankeygen_id
where t.fund_id = :fund_id and a.trankeygen_id = :trankeygen_id
