select r.report_id, r.report_type, case when r.user_data is not null then r.metadata end as metadata, r.submitted_at, rs.report_id as amends, case when rs.user_data is not null then rs.metadata end as amends_metadata, cfs.message, cfs.has_warnings, cfs.processed
from report r
left join campaign_finance_submission cfs on r.report_id = cfs.report_id
left join public.report rs on r.report_id = rs.superseded_id
where r.external_id = :deposit_id
and r.fund_id = :fund_id
order by r.submitted_at desc, r.report_id desc;


