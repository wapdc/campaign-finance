-- List account totals by group for a specified date for use in account summary page
select a.name, a.total * g.negate as total, a.account_id
from (
         select ac.trankeygen_id, max (ac.name) as name, ac.acctnum, sum(case when r.did = ac.trankeygen_id then -1 else 1 end * amount) as total, count(1) as accounts, min(ac.trankeygen_id) as account_id from
             private.vaccounts ac
                 join private.vreceipts r on r.fund_id=ac.fund_id
                 and (r.did = ac.trankeygen_id or r.cid = ac.trankeygen_id)
         where ac.fund_id=:fund_id
           and r.date_ <= :date
           and ac.acctnum=:acctnum
         group by ac.acctnum, ac.trankeygen_id) a
         join private.account_groups g on g.acctnum = a.acctnum
where g.acctnum <> 0
order by 1