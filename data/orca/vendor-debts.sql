select ve.fund_id,
       ve.trankeygen_id,
       ve.name,
       ve.contid,
       ve.amount,
       ve.date_,
       ve.memo,
       ve.checkno
from private.vexpenditureevents ve
       join private.accounts a on a.trankeygen_id = ve.bnkid
where ve.fund_id = :fund_id and a.acctnum = 2000
order by ve.date_ DESC, ve.trankeygen_id DESC;
