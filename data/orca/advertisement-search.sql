SELECT * FROM private.ad a WHERE fund_id=:fund_id
                                           and (:start_date <= first_run_date or cast(:start_date as date) is null)
                                           and (:end_date >= first_run_date or cast(:end_date as date) is null)
order by first_run_date DESC, ad_id DESC