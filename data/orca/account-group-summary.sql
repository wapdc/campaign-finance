-- Find the of all totals regardless of date for display in the campaign account summary page
select g.acctnum, g.title, a.total * g.negate as total, a.accounts, a.account_id, g.required, g.deprecated
  from (select acctnum, sum(total) as total, count(1) as accounts, min(trankeygen_id) as account_id from private.vaccounts where fund_id=:fund_id group by acctnum) a
  join private.account_groups g on g.acctnum=a.acctnum
where g.acctnum <> 0
order by 1
