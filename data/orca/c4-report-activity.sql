select ro.*, r.submitted_at, r.report_type, case when r.user_data is not null then r.metadata end as metadata, r.report_id, rs.report_id as amends,  case when rs.user_data is not null then rs.metadata end as amends_metadata, cfs.has_warnings, cfs.processed, cfs.message from
private.reporting_obligation ro
inner join public.report r on r.fund_id = ro.fund_id
left join campaign_finance_submission cfs on r.report_id = cfs.report_id
left join public.report rs on r.report_id = rs.superseded_id
where (ro.startdate, ro.enddate + 1) OVERLAPS (r.period_start::DATE, r.period_end::DATE + 1)
and ro.fund_id = :fund_id
and ro.obligation_id = :obligation_id
and r.report_type = 'C4'
order by r.submitted_at desc, r.report_id desc;

