select c.*, t.orca_id
from private.vcontacts c
    join private.trankeygen t on t.trankeygen_id = c.trankeygen_id
where t.fund_id = :fund_id
    and (fuzzy_name_match(:name, c.name) > 0.4 or
         cast(:name as varchar) is null)
    and (type = :contact_type or cast(:contact_type as varchar) is null)
    and type <> 'HDN'
order by fuzzy_name_match(:name, c.name) desc,c.name