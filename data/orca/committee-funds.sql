select c.*, f.fund_id, f.election_code, f.vendor,
       GREATEST(p2.value::text, f.updated_at::text, cf.updated::text) as last_updated,
       coalesce(cf.version::Text,p3.value) as orca_version,
       ca.role, ca.expiration_date,
       (case when cf.fund_id is not null then true else false end) as campaign_fund
  from committee c
  JOIN fund f ON f.committee_id=c.committee_id
  LEFT JOIN public.committee_authorization ca on ca.committee_id = c.committee_id and ca.realm = 'PDC' and ca.uid = 'PDC'
  LEFT JOIN private.properties p ON p.fund_id=f.fund_id and p.name='FUND_ID'
  LEFT JOIN private.campaign_fund cf on cf.fund_id=f.fund_id
  LEFT JOIN private.properties p2 ON p2.fund_id=f.fund_id AND p2.name = 'DERBY_TO_RDS'
  LEFT JOIN private.properties p3 ON p3.fund_id=f.fund_id AND p3.name = 'VNUM'
WHERE c.committee_id=:committee_id
ORDER BY c.election_code, f.election_code desc;

