select *
from private.vreceipts
where fund_id=:fund_id
  and type not in (12)
  and (cast(:start_date as date) is null or :start_date <= date_ )
  and (cast(:end_date as date) is null or :end_date >= date_ )
  and ( cast(:min_value as numeric) is null or :min_value <= amount)
  and (cast(:max_value as numeric) is null or :max_value >= amount)
  and (cast(:type as integer) is null or :type = type)
  and (cast(:contid as integer) is null or :contid = contid )
  and (cast(:name as varchar) is null or fuzzy_name_match(:name, name) > 0.4 )
order by date_ desc, trankeygen_id desc;
