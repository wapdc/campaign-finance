-- Find sum of all transactions up to a specified date grouped by account number for display in campaign summary
select g.acctnum, g.title, a.total * g.negate as total, a.accounts, a.account_id, g.required, g.deprecated
from (
  select acctnum, sum(case when r.did = ac.trankeygen_id then -1 else 1 end * amount) as total, count(distinct ac.trankeygen_id) as accounts, min(ac.trankeygen_id) as account_id from
    private.vaccounts ac
     join private.vreceipts r on r.fund_id=ac.fund_id
        and (r.did = ac.trankeygen_id or r.cid = ac.trankeygen_id)
    where ac.fund_id=:fund_id
      and r.date_ <= :date
    group by acctnum) a

         join private.account_groups g on g.acctnum=a.acctnum
where g.acctnum <> 0
order by 1