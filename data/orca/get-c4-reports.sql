SELECT
    ro.obligation_id,
    ro.fund_id,
    'C4' as report_type,
    r.report_id,
    case when r.user_data is not null then metadata end as metadata,
    ro.startdate,
    ro.enddate,
    ro.duedate,
    r.submitted_at,
    p.value,
    ro.startdate
from private.reporting_obligation ro
  left join private.properties p
      on p.fund_id=ro.fund_id and p.name='CAMPAIGNINFO:STARTDATE'
  left join public.report r
    on r.fund_id = ro.fund_id
      and (r.period_start = ro.startdate
        or (to_date(p.value, 'MM/DD/YYYY') between ro.startdate and ro.enddate and to_date(p.value, 'MM/DD/YYYY') between r.period_start and r.period_end)
      )
      and r.report_type = 'C4'
      and r.superseded_id is null
where ro.fund_id = :fund_id
order by r.submitted_at nulls first,
  ro.startdate

