-- List all accounts that match a particular account number for use in display of accounts by group without a filtered date
select a.name, a.trankeygen_id as account_id, a.total * g.negate as total from private.vaccounts a
  join private.account_groups g on g.acctnum=a.acctnum
where fund_id=:fund_id
  and a.acctnum=:acctnum