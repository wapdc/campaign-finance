select
    f.fund_id,
    f.committee_id,
    p.value as guid
from fund f
  LEFT JOIN private.properties p ON p.fund_id=f.fund_id and p.name='GUID'
where f.committee_id = :committee_id
  and f.election_code = :election_code