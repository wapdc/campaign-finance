select *
from private.vreceipts
where fund_id=:fund_id
  and type in (14, 15, 16, 18)
  and (:start_date <= date_ or cast(:start_date as date) is null)
  and (:end_date >= date_ or cast(:end_date as date) is null)
  and (:min_value <= amount or cast(:min_value as numeric) is null)
  and (:max_value >= amount or cast(:max_value as numeric) is null)
  and (:receipt_type = type or cast(:receipt_type as numeric) is null)
  and (fuzzy_name_match(:name, name) > 0.4 or cast(:name as varchar) is null)
order by date_ desc, trankeygen_id desc;
