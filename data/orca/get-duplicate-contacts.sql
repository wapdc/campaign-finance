select
       dup.contact_id       as contact_id,
       tc.orca_id           as contact_orca_id,
       c.name               as contact_name,
       c.street             as contact_street,
       c.city               as contact_city,
       c.state              as contact_state,
       dup.duplicate_id     as duplicate_id,
       td.orca_id           as duplicate_orca_id,
       d.name               as duplicate_name,
       d.street             as duplicate_street,
       d.city               as duplicate_city,
       d.state              as duplicate_date,
       dup.name_similarity  as name_similarity,
       dup.verified_not_duplicate as flag
from private.contact_duplicates dup
join private.contacts c on c.trankeygen_id = dup.contact_id
join private.contacts d on d.trankeygen_id = dup.duplicate_id
join private.trankeygen tc on tc.trankeygen_id = c.trankeygen_id
join private.trankeygen td on td.trankeygen_id = d.trankeygen_id
where dup.verified_not_duplicate = false
  and tc.fund_id = :fund_id
  and td.fund_id = :fund_id;