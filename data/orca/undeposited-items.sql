select r.*, di.deptid
  from private.vreceipts r
      left join private.deposititems di on di.rcptid=r.trankeygen_id
      left join private.auctionitems au on r.type in (7,8) and r.pid = au.trankeygen_id
      left join private.receipts ro on ro.type in (7,8) and ro.type <> r.type and ro.pid=au.trankeygen_id
  where r.fund_id = :fund_id and di.deptid is null
   and r.carryforward=0
   and r.type in (
     1, -- Monetary Cont
     2, -- Personal funds
     3, -- Monetary Loan
     4, -- Fundraiser Contribution
     7, -- Auction Item Donor
     8, -- Auction Item Buyer
     11,-- Monetary group contribution
     13,-- Anonymous Contribution
     14,-- Other Receipt
     15,-- Bank Interest
     17,-- Monetary Pledge Payment
     18,-- Refund from vendor
     38 -- Low cost fundraiser
   )
   -- Exclude unsold auction items
   and (r.type not in (7,8) or (ro.amount + r.amount) > 0);
