select a.*, c.street, c.city, c.state, c.zip
from private.vaccounts a
  left join private.contacts c on a.contid=c.trankeygen_id
where a.fund_id=:fund_id
  and a.acctnum in (1000, 1800, 2600) order by acctnum, a.name
