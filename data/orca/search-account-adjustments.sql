select r.*,
       r2.amount original_amount,
       r2.type as reported_type,
       r2.date_ as reported_date,
       r2.trankeygen_id as reported_id,
       r.amount + r2.amount as corrected_amount
from private.vreceipts r
         left join private.receipts r2 on r2.trankeygen_id=r.pid
where fund_id=:fund_id
  and r.type in (23, 33, 42, 35)
  and (:start_date <= r.date_ or cast(:start_date as date) is null)
  and (:end_date >= r.date_ or cast(:end_date as date) is null)
  and (:min_value <= r.amount or cast(:min_value as numeric) is null)
  and (:max_value >= r.amount or cast(:max_value as numeric) is null)
  and (:receipt_type = r.type or cast(:receipt_type as numeric) is null)
  and (fuzzy_name_match(:name, r.name) > 0.4 or cast(:name as varchar) is null)
order by r.date_ desc, r.trankeygen_id desc;
