select ve.fund_id,
       ve.trankeygen_id,
       ve.name,
       ve.contid,
       ve.amount,
       ve.date_,
       ve.memo,
       ve.checkno,
       ve.acctnum,
       ve.bnkid,
       vr.expenses,
       c.street,
       c.city,
       c.state,
       c.zip
from private.vexpenditureevents ve
join (select pid,
             max(fuzzy_name_match(:name, name)) sim_name,
             json_agg(vreceipts.*) as           expenses
      from private.vreceipts
      where fund_id = :fund_id
      group by pid) vr on ve.trankeygen_id = vr.pid
left join private.contacts c on c.trankeygen_id = ve.contid
where ve.fund_id = :fund_id
  and (:start_date <= ve.date_ or cast(:start_date as date) is null)
  and (:end_date >= ve.date_ or cast(:end_date as date) is null)
  and (:min_value <= ve.amount or cast(:min_value as numeric) is null)
  and (:max_value >= ve.amount or cast(:max_value as numeric) is null)
  and (fuzzy_name_match(:name, ve.name) > 0.4 or sim_name > 0.4 or
       cast(:name as varchar) is null)
order by ve.date_ DESC, ve.trankeygen_id DESC;
