select
    a.name,
    a.trankeygen_id,
    vr.trankeygen_id as receipt_id,
    vr.pid,
    a.acctnum,
    vr.amount as pledge,
    coalesce(vr.date_, a.date_) as date_,
    a.memo,
    a.total as balance,
    vr.contid,
    a.orca_id as contid_orca,
    vr.orca_id
from private.vaccounts a
         left join private.vreceipts vr on vr.fund_id = a.fund_id AND (vr.cid = a.trankeygen_id and vr.type = 12)
where a.fund_id=:fund_id
  and a.acctnum IN (4900)
  and (:start_date <= a.date_ or cast(:start_date as date) is null)
  and (:end_date >= a.date_ or cast(:end_date as date) is null)
  and (fuzzy_name_match(:name, a.name) > 0.4 or cast(:name as varchar) is null)
order by a.date_ desc, vr.date_, a.trankeygen_id desc;