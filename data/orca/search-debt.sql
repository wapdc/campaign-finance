select r.trankeygen_id, a.trankeygen_id as account_id,  a.name, a.acctnum,  -1.0 * a.total as balance,  r.amount, r.date_, r.description,
  coalesce(r.transaction_type, case when a.acctnum=2600 then 'Credit card debt' when a.acctnum=2000 then 'Vendor debt' end) as transaction_type
from private.vaccounts a
  left join private.vreceipts r on r.did=a.trankeygen_id
    and r.type in (3, 5, 19)
where a.fund_id=:fund_id
  and a.acctnum between 2000 and 2999
order by case when r.date_ is null then 0 else 1 end, r.date_ desc,  a.trankeygen_id desc;
