select DISTINCT c.committee_id,
       c.name,
       c.election_code,
       e.title,
       c.start_year,
       c.end_year,
       c.continuing,
       c.pac_type,
       r.verified      as registration_verified,
       r.registration_id,
       coalesce(c2.campaign_start_date, c.registered_at) as campaign_start_date
from pdc_user u
    join committee_authorization a on a.uid = u.uid and a.realm = u.realm
         join committee c on a.committee_id = c.committee_id
         left join election e on e.election_code = c.election_code
         left join registration r on c.registration_id = r.registration_id
         left join candidacy c2 ON c.committee_id = c2.committee_id
where u.realm = :current_realm
  and u.uid = :current_uid
  and c.filer_id is not null
order by c.election_code, c.start_year


