select
    a.name,
    a.trankeygen_id,
    vr.trankeygen_id as receipt_id,
    vr.pid,
    a.acctnum,
    a.total * -1 as amount,
    coalesce(vr.date_, a.date_) as date_,
    a.memo,
    case when a.acctnum = 4096 THEN 'Fundraiser'
         when a.acctnum = 4097 THEN 'Low cost fundraiser'
         when a.acctnum = 4098 THEN 'Auction'
        end transaction_type,
    vr.deposit_date as deposit_date,
    a.orca_id as contid_orca,
    vr.orca_id
from private.vaccounts a
left join private.vreceipts vr on vr.fund_id = a.fund_id AND (vr.pid = a.trankeygen_id and vr.type = 38)
where a.fund_id=:fund_id
  and a.acctnum IN (4096, 4097, 4098)
  and (:start_date <= a.date_ or cast(:start_date as date) is null)
  and (:end_date >= a.date_ or cast(:end_date as date) is null)
  and (:fundraiser_type = a.acctnum or cast(:fundraiser_type as numeric) is null) --change to acctnum
  and (fuzzy_name_match(:name, a.name) > 0.4 or cast(:name as varchar) is null)
order by a.date_ desc, vr.date_, a.trankeygen_id desc;