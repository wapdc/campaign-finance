select
       p.trankeygen_id,
       c.name,
       c.street,
       c.city,
       c.state,
       c.zip,
       c.phone,
       c.email,
       c.occupation
from private.contacts c
left join private.trankeygen p on p.trankeygen_id = c.trankeygen_id
where p.trankeygen_id = :trankeygen_id;
