select d.*, case when r.user_data is not null then metadata end as metadata from private.vdepositevents d
left join public.report r on r.report_id = d.report_id
where d.fund_id=:fund_id
order by d.submitted_at desc nulls first,
  d.date_ DESC, d.trankeygen_id DESC