SELECT count(1) as items
from private.reporting_obligation ro
  left join private.properties p
      on p.fund_id=ro.fund_id and p.name='CAMPAIGNINFO:STARTDATE'
  left join public.report r
    on r.fund_id = ro.fund_id
      and (r.period_start = ro.startdate
        or (r.period_start = to_date(p.value, 'MM/DD/YYYY') and  to_date(p.value, 'MM/DD/YYYY') between ro.startdate and ro.enddate))
      and r.report_type = 'C4'
      and r.superseded_id is null
where ro.fund_id = :fund_id
and r.report_id is null
and ro.enddate + 1 < now()

