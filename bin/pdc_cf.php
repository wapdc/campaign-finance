#!/usr/bin/env php
<?php

use Symfony\Component\Yaml\Yaml;

require_once ('vendor/autoload.php');
date_default_timezone_set('America/Los_Angeles');


if ($argc < 2) {
  $cmd = $argv[0];
  echo "Usage: $cmd <command>\n";
  echo "   where command is: ";
  echo "     import-committees - Used to create (testing) committees from YML files.\n; ";
  echo "     create-testing-tokens - Generate tokens that are the same as filer ID's for an election year. \n";
  echo "     reprocess-report-submission - Reprocess a report submission from the campaign finance submission>\n";
  echo "     submission-to-json - Convert report xml submission data to json and persist on report record.\n";
  exit(1);
}
else {
  // Instantiate a service

  $command = strtolower($argv[1]);
  echo "Running $command\n";
  $migration = strpos($command, 'restore') !== FALSE;
  $service = \WAPDC\CampaignFinance\CampaignFinance::service($migration);
  switch ($command) {
    case 'import-committees':
      $dir = dirname(__DIR__) . $argv[2];
      $fileList = array_diff(scandir($dir), array('.', '..'));
      foreach ($fileList as $file) {
        $file_parts = pathinfo($file);
        if ($file_parts['extension'] === "yml") {
          $submission = Yaml::parseFile("$dir/$file", Yaml::PARSE_OBJECT_FOR_MAP);
          $service->getCommitteeManager()->CreateCommitteeFromFile($submission);
        }
      }
      break;
    case 'create-testing-tokens':
      $election_year = $argv[2];
      $service->getCommitteeManager()->CreateTestingTokens($election_year);
      break;

    case 'reprocess-report-submission':
      $submission_id = $argv[2];
      $amend_report_id = $argv[3] ?? NULL;
      $response = $service->getFundProcessor()
        ->reprocessReportSubmission($submission_id);
      echo json_encode($response) . "\n";
      break;

    case 'submission-to-json':
      ini_set("memory_limit","256M");
      if (isset($argv[2])) {
        $method = strtolower($argv[2]) === 'all' ? 'allSubmissionToJson': 'submissionToJson';
        try {
          $response = $service->getFundProcessor()
            ->$method($argv[2]);
        } catch (\Exception $e) {
            echo "Failed to process report: " . $e->getMessage();
            exit(1);
        }
      } else {
        echo "Argument required: \n 1. report_id : Process single report.\n 2. all : to process all available records. \n";
      }
      break;

    default:
      echo "Unknown command $command";
  }
}
