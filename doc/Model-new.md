# Campaign Finance Data Model

This document describes the initial data model designed for the implementation of the ORCA replacement.  General 
object model will be described here.

**Current Status**: at the time of this writing, this document is intended to 
describe the model used for reported campaigns as well as 


## Committee

Describes a political committee as implemented in the Campaign Registration code.

**committee_id** - primary key for the committee
 
  
## Campaign Fund

Describes a campaign fund and expenditures.

**fund_id** - The unique identifier that describes the fund.

**fund_type** - Identifies the type of fund. (e.g. committee)

**filer_id** - Filer id associated with the fund. (legacy)

**election_year** - election year associated with the original fund. Note: we might want to change this to not create 
  separate funds for existing committees every time a year changes? 

##  Report
Keeps a batch of transactions together

**report_id** - Primary key

**repno** - Legacy conversion field

**superseded** - Report id of the superseding report. 

**superseded_repno** - Repno of the superseding report (legacy conversion)

**report_type** - e.g. C3, C4

**submitted_at** - Date report first submitted? 

**fund_id** - replaces filer_id/election_year to indicate the matching orca file. 

**period_start** - Starting point of the report

**period_end** - Ending point covered by report

## Transaction

General Table for describing contributions, expenditures, 

**transaction_id** - Primary Key

**fund_id** - The ID of the fund that this transaction belongs to

**report_id** - Indicates the C3 or C4 reporting ID that includes the report originating this transaction

**origin** - PDC identifier used to track where the transaction came from. (e..g. C3 AUB C4)

**category** - Category of transaction, e.g. contribution, expenditure, loan, debt, pledge, auction.
               Category + action is used to generate legacy form_type.

**action** - Modification made by the transaction, e.g. receipt, payment, refund, forgiveness, carry_forward

**transaction_date** - The effective date of the transaction.  Used to represent dates such as 
  receipt_date or loan_date or payment_date

**created_at** - Date transaction created

**updated_at** - Date transaction last altered

**parent_id** - Indicates the parent transaction for example, a loan payment will have a parent_id pointing to the 
  initial loan. 

**external_guid** - External unique identifier of transaction. 

**external_type** - An external representation of the type of transaction. 

**contact_id** - Contact for this transaction (could be nullable for legacy transactions?)

**contact_name** - Name at the time of the transaction

**contact_address** - Address at the time of the transaction

**contact_city** - City at the time of the transaction

**contact_state** - State at the time of the transaction

**contact_postcode** - Postal code at the time of the transaction

**amount**

**inkind** - In-kind

**itemized** - Legacy form_type/origin is calculated 

## Expense

Metadata associated with a funds expense. 

**transaction_id** - Primary key transaction for the expense

**name** - Name at the time of the transaction

**address** - Address at the time of the transaction

**city** - City at the time of the transaction

**state** - State at the time of the transaction

**postcode** - Postal code at the time of the transaction

**description** - The purpose of expense

**category** - Category of expenditure. Calculates legacy expn_code.
   
**vendor_location_id** - Vendor address geocoded
   
## Contribution

**transaction_id** - Primary key transaction for the contribution

**name** - Name at the time of the transaction

**address** - Address at the time of the transaction

**city** - City at the time of the transaction

**state** - State at the time of the transaction

**postcode** - Postal code at the time of the transaction

**occupation** - Contributor's occupation

**employer** - Contributor's employer name

**employer_address** - Contributor's employer address?

**employer_city** - Contributor's employer city

**employer_state** - Contributor's employer state

**employer_postcode** - Contributor's employer postal code?

**prim_gen**  - Election cycle to which contribution 

**aggregate** - Legacy contributor/cycle aggregate total contributions

**contributor_type** - Limits-related code

**geocoded_address** - Contributor address geocoded

## Loans

**loan_id** - Primary key

**transaction_id** - Initial transaction for the loan

### Loan payment

Loan payments are expressed as a transaction with a type of 'loan-payment'. The parent_id
of the loan then points to the initial loan transaction. Payments then are simply 
the sum of the transaction. 

## Pledge

**pledge_id** - Primary key

## Loan forgiveness 

Loan forgiveness is recorded as a transaction of type loan-forgiveness

## Debt

## Corrections

**correction_id**

**transaction_id**

**original_amount**

**corrected_amount**





