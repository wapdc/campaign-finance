# Adds and Electioneering Communications Design Document

Registered (C1) committees need to file a special report for political advertising and electioneering communications
when the committee makes independent expenditures on ads appearing within 21 days of the election or expenditures on
electioneering communications appearing within 60 days of the election.

To that end we are designing a data entry experience that will allow users to enter this information for all advertisements. 
Although the data may eventually be destined for a C6 the idea is to create a data entry experience that allows ORCA 
users to enter this data for any advertising expense.  Future versions of ORCA would facilitate saving this data both on 
a C6 and/or a C4 report. 

Guidelines for what expenditures meet these criteria can be found at: 

[ Independent Expenditure Ads & Electioneering Communications ](https://www.pdc.wa.gov/learn/publications/independent-expenditure-ads-electioneering-communications)

## Ad

Expenses for a political ad reported on a single C6 may occur across multiple expenditure events and involve more than
one vendor.  In order to prepare or track the multiple expenses for the ad, we would need to create an **Advertisement**
which could be attached to one or more expense items within an expenditure event.

Although not technically accurate in the model, each political ad is created with a delivery method and a run date that
would include how and when the ad was delivered.  Here a ad is loosely defined as a type of mass communication 
that would be used to convince people to vote in a particular way. The term is meant to encompass advertisements that 
might be reported in the future as either an independent expenditure ads or electioneering communication

Each advertisement would identify:

- description: Describes the nature of the ad. 
- first_run_date : First date/time of the appearance of the ad 
- target_type: Does the ad target candidate(s) or ballot measure(s)
- ad_type: Delivery method or category of the ad.  
  
With additional tables for keeping track of:  
- List of candidates or proposals identified in the ad (identified_entity) and any stance taken, and percentage or amount of money allocated (i.e. )
- Contribution sources towards the ad.
- Expense items to make the ad happen.


## Identified entities

For each political ad we will track the identified entities associated with the ad.  These would either be candidates or 
ballot propositions/initiatives.

Information included: 

- Candidacy_id: points to the candidate identified in the ad
- Ballot Name (either ballot name or candidate name) depending on type
- jurisdiction_id 
- Stance - for or against (null when candidates are identified by not named on EC)
- Amount(amount of expense over the ad)
- Percent (Percent of ad on this entity)

Distribution of funds allocated for the ad would be primarily tracked as a percentage of the ad.

## Tracking expenditure event items

The ad_expenses table would keep track of all expense items that were associated with the advertisement. 

Columns include

expense_id - trankeygen_id of the receipt table entry corresponding to the expense. 
ad_id - Foreign key to the advertising table

## Tracking contribution event items

A separate table ad_sources would indicate the contribution event that was tied to the creation of this ad.  information 
would include: 

- ad_id: the id of the ad 
- contribution_id: the trankeygen id of the receipt record that indicates the monetary contribution targeting the ad. 






