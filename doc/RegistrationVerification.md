# Registration Verification

## Candidate Verification Process

Customer services chooses a matching person:
- From this person, we'll be able to decide what to do based on the initial candidacies and committees that exist.

Consider the candidate Bob Bobberson, candidate for Mayor of Olympia:
There are a few ways we can learn about Bob's candidacy for the first time

1. Financial Affairs
   - Candidacy records from F1
   - Filer ID on person
   - Filer ID on candidacy possibly...
   - No committee ID on candidacy record at this point in time.
2. Declarations
   - Makes us aware of a candidacy (creates candidacy records)
   - Depending on if we run the publish step, there may or may not be a filer_id on the candidacy.
   - No committee ID
3. Registration
   - It's possible that someone else, or the filer under a different SAW login, created a registration attached to a different committee.
   - Should be: existing candidacy, with committee pointing to this person.
   - Committee_id not null, committee_id on candidacy that has a filer_id that is the same as the one on the committee committee_id on candidacy that has a filer_id that is the same as the one on the committee.

It is possible that Bob has other candidacies... 
If Bob has another candidacy that is already registered for an office that is not the Mayor of Olympia (his current candidacy) in the same year, we can't use the person filer_id on the new registration. We have to use a different one.
This describes the infamous "2file".

## Common Problems

- Filers sometimes get their own jurisdiction and/or office wrong.
   - If we don't catch this, we may verify it in error.
   - Customer service staff may unverify and revert it at which point in time we will see a registration that has may have the committee_id still on it, registration will be `verified: false`.
   - When the user submits the draft with the corrected data, we may need to alter the jurisdiction and office of the candidacy that the committee record points to.  
    
# Verification Process

- The user is provided with a search tool that helps them find a person in the case of a candidate committee. 
- When the user has chosen a person the application calls the verification-options endpoint which will return if a filer_id needs to be assigned and the person record.
- Once the verification options have been returned we should know the person and know if we have a candidacy or if we need to create one and at this point we will call 
the core library to generate the new filer_id if it is needed.
- This core libraries job will be to assign the filer id and assign new ones
- The core library will be used to generate the new filer id and return it, in the case of a new candidacy it will be used by the campaign finance process to assign it
to the candidacy via admin data before it is officially created
  












