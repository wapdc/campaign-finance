
## Chart of Accounts

trankeygen record, accounts record, contact record all bearing the same id.

The accounts record keeps track of the totals of the transactions that are tagged with that account. For example,  the account 
record with account number of 5010 would keep track of all the expenses that are tagged with that account. Note that it is
possible to create more than one account with the same account number. 

The contact record keeps track of the name/description associated with the account along with any contact information
associated with the account. For example, a credit card account contact record would contain address of the credit card company.

For transaction that have balances like pledges debt and loans account entries are created to maintain the balances of those transactions. In these cases
the pid of the account points to the original transaction, ie debt, loan, pledge. The contact id on the account points to the contact who originated
the transaction. All of the account entries created for these cases bear the same account number. There will also exist an account with no pid that summarizes
routine expenses that were tagged with that account as a credit or debit.


## Expenses
Creation of expenditureevents entry which holds the contact id (contid), bank id (bnkid), transaction expense balance (amount), transaction date (date_),
check number (checkno), itemized flag and memo.

Each item is created as a receipt entry with type = 28. The parent id of the receipt points to the expenditureevent item. On the receipt record the contid and
date and the cid are carried forward from the base expenditure event (bank id becomes did).  In the event of sub-vendor 
transactions we are proposing to use the contact ID of the sub-vendor on each receipt item.  The expenditureevent record
would hold the contactid of the parent entity, while the receipts contact would contain the contact id of the subvendor. 


## Independent expenditures (proposed)

The current C6 expenditure form lists multiple expenditure events on a single report. It then proceeds to apportion the total of all the expenses across the 
different candidates and propositions. Campaigns would enter multiple expenditure events and then proceed to a screen to report all the qualifying expenditure 
events onto a report.

Each expense (receipt entry) would be stamped with the report id of the submitted report. The distribution of the candidates or proposals
identified would be stored in an identified_entity table which would be keyed by the report id of the submitted report. As with C3 and C4 reports, sum totals
for those reports would be stored in the transaction/report_sum table keyed to the report. The identified_entity table could be created as a trankeygen-based
table instead. It would need to include contact_id pointers and well as stance, office and proposal information.

Alternatively, proposals and candidates could be stored as contacts and accounts in a similar fashion to the way the sums from debt (and other) transactions
in the ORCA database itself. Report sums could also be stored in this fashion. If we do that, we will still need to find places to store party and stance, as
well as office and jurisdiction. 

Regardless we need to create a new representation that describes the campaign that a committee is engaged in. The 
campaign or relationship id would be added to the receipts(expense) record to be able to track what an individual expense 
purposed to. The "ad campaign" or ad campaign activity would also be attached to the individual expense item. 

Mapping from the C6 paper form:

- Date made:  receipts.date_
- Date first presented/mailed: receipts.date_presented*
- Name/Address of vendor/recipient: contact(the one attached to expenditureevent field)
- Description of expenditure: receipt.description
- Amount or value: receipts.amount
- Expenditures $100 or less not itemized above: report_sum
- Total this report: transaction.amount (category=sum)
- Total including prior reports: report_sum.aggregate_amount
- Existing repno: expenditureevents.repno (Each expenditure event needs to be tagged with a repno that corresponds to the report on which the expense was 
  reported)
- Candidate/Proposition: identified_entity.name (new table)
- Office/District: identified_entity.office_code
- Proposition number: identified_entity.proposal_number
- Party: identified_entity: identified_entity.party_id
- Support/Oppose: identified_entity.stance
- Portion of current expense: identified_entity.amount
- Total C6 expenses related to each candidate: tbd



* = new field


