## Error messages 
This is a list of error messages that can be reported when submitting C3 or C4 reports via ORCA.<br/>


### Common Error Messages
**The date the report was submitted must be a valid date. (report.submitted_at)**<br/>
Indicates that the date the report was submitted does not appear to be a valid date.  

**This report has been submitted for the incorrect election year. (report.election_year.invalid)**<br/>
For non-surplus committees, the election year (aka reporting year) is between the committee start year and end year. 
Note that single election committees always have the same start and end year. Continuing committees need to not have been ended.

For continuing committees, the deposit date or reporting period end date is not after the election year.

**This report is not a C3 or C4. (report.report_type.invalid)**<br/>
The submitted report type was not either C3 or C4.  

**The committee submitting this report does not have a verified registration on file (committee.unverified)**<br/>
The committee for the report has not been verified.

**The original report cannot be found to amend: {$original_report_id} (amendment.missing-report)**<br/>
This happens when a user amends a report, when the original does not seem to exist.  Probably the report was deleted or
an incorrect report number was entered in ORCA when overriding this value. 

**An amendment already exists for the report: {$original_report_id} (amendment.report-already-amended)**<br/>
This happens when a user tries to retransmit an amendment a second time.  For some reason ORCA does not have the correct 
report number on the report.  Find the latest report number and have the user manually specify it when amending the 
report.   

**A report already exists for the reporting period beginning {date}. Perhaps you meant to file an amendment? (duplicate-report)**<br/>
Happens when a report is submitted as a new report (not amendment) when we already have a report on file for the same 
C4 coverage dates.  

**The amended report's committee name does not match the original report.**<br/>
The committee for the original report matches the committee for the amending report. (committee.name.invalid)
   
**The amended report records the deposit in a different fund than the original report**<br/>
The fund for the original report matches the fund for the amending report. (report.fund)

### C3 (contribution) Reporting Error Messages
