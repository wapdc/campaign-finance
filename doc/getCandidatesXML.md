# Candidates and Committee Registrations API



## The XML format
```xml
<committee>
    <candidate_committee title="FAGERLIE SARA R, SCHOOL DIRECTOR - 2019 General" id="23347" filer_id="FAGES 290" election_year="2019">
      <body>party:NONE jusridiction:SNOHOMISH SD 201</body>
    </candidate_committee>
    <candidate_committee title="COUTURE JON A, FIRE COMMISSIONER - 2019 General" id="23265" filer_id="COUTJ 606" election_year="2019">
      <body>party:NONE jusridiction:CLARK FIRE PROT DIST 03</body>
    </candidate_committee>
    <candidate_committee title="Jeff Winmill, SECRETARY OF STATE - 2020 General" id="25506" filer_id="WINMJ--102" election_year="2020">
      <body>party:DEMOCRATIC jusridiction:State of Washington</body>
    </candidate_committee>
</committee>
```

## Drupal Feed

Drupals Feeds module parses XML provided by a function in the database that is exposed by an endpoint. You can test the feed by visiting one this url:

* `/public/service/candidate/feed.xml?days=120`

The parameter days returns the number of days back the candidates updated_at date must be in order be selected in the feed. If data's returned output of the data can be viewed by right clicking to `view-source`.

## SOLR indexing

This data is only for SOLR indexing. The content is never displayed. 