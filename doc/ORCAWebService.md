# ORCA Web Service Replacement Design 

This document describes the strategy for replacing the the .NET based ORCA web service with suitable PHP replacement. 
By writing a replacement for the existing .NET application we are able to move the data to modern cloud hosted solutions 
in preparation for writing the web service. 


## Class Overview

The following basic object model describes the roles of the class files.  See these fies for further documentation 
on public methods: 

**[\Drupal\campaign_finance_accounting\CampaignFinanceSoapController](../drupal-8/campaign_finance_accounting/src/Controller/CampaignFinanceSoapController.php)** - 
Provides the SOAP endpoints that ORCA uses to file reports.  This class is responsible for decoding the payload which 
is provided as a Base 64 encoded zip file, from which is extracted a single XML document.  The XML document is the "report"
as provided by ORCA. 

**[\WAPDC\CampaignFinance\OrcaRegistrationManager](../src/OrcaRegistrationManager.php)** - 
Looks up information on the Committee Registration and generates XML that ORCA needs to understand the registered 
campaign. 

**[\WAPDC\CampaignFinance\FinanceReportingProcessor](../src/FinanceReportingProcessor.php)** - 
Provides processing of the generic PHP object with validation. It provides mechanisms for basic report saving along with 
common functions used by the DepositReportProcessor and the C3ReportProcessor or the C4ReportProcessor.  

**[\WAPDC\CampaignFinance\reporting\C3ReportProcessor](../src/Reporting/C3ReportProcessor.php)** - 
Converts a C3 Report of deposit into contributions, auctions and pledge records.  Extends the FinanceReportingProcessor. 

**[\WAPDC\CampaignFinance\reporting\C4ReportProcessor](../src/Reporting/C4ReportProcessor.php)** - 
Converts a C4 Report of Expenses into expense expense records.  Extends the FinanceReportingProcessor. 

**[\WAPDC\CampaignFinance\reporting\OrcaReportParser](../src/Reporting/OrcaReportParser.php)** - Responsible for parsing the XML makes up the report, validating it against 
an XML schema and formatting it as a stdClass PHP object for use by either the reporting processors described above. 

**[\WAPDC\CampaignFinance\Validator](../src/Validator.php)** - 
Responsible for basic validation functions that would be used either by web service submissions or by front end web code. 
It should be capable of validating against database provided validation tables as well. 

**[\WAPDC\CampaignFinance\FundProcessor](../src/FundProcessor.php)** - 
Responsible for performing transactional work associated with a fund.  This will be the future home of the logic that 
drives future web based campaign finance reporting system. 


Given this structure, here are some guidelines for where the logic is intended to go. 

* If it is about XML parsing or pre-schema XML validation it goes in the OrcaReportParser. 

* If it is about building a report, or common functionality used in both the C3 and the C4, find it in the FinanceReportingProcessor

* If it is specific to the C3 report put it in the C3ReportProcessor

* If it is specific to the C4 report but it in the C4ReportProcessor

* If it is forward thinking and not about reproducing what we think of as a C3 or C4 report put it in the FundProcessor



