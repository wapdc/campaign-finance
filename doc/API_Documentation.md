# REST API Documentation

The Campaign Finance REST API provides a mechanism for accessing data regarding candidates and committees that have 
registered with the Washington State Public Disclosure Commission. In the current API we are in the middle of a
transition between SOAP web services and JSON web services.

## API Tokens 

Candidates and committees that file registrations can allow access to their committees by generating an API token using 
the campaign registration application https://apollo.pdc.wa.gov/campaign/committees : 

* From the list of "Committees I File For, view a committee that you have registered with the PDC. 
* Select the access tab at the top of the screen.
* Add you must add a token.

Tokens generated using this interface are only displayed one time, however tokens can easily be generated or revoked 
from this screen. 

API tokens should be provided using the "x-api-key" custom request header with each request as int the following example: 

```
x-api-token: abcdefgabcdefgabcdefg
```


## Get Committee Registration

JSON REST endpoint allowing the download and validation of a token: 

**URL**: https://apollo.pdc.wa.gov/

[See Example](CommitteeRegistration.md) for additional information. 

## Contribution and Expenditures API

SOAP Endpoint allowing the submission of campaign finance records. 

**URL**: 

@TODO: Complete this documentation

