# Campaign Finance Access Model

Having signed up for and logged into Secure Access Washington, the user should see a list of campaigns they currently 
file for. The user should then see an "add a campaign" option which will allow for access to existing campaigns. 
 
There are three distinct use cases for how people may be given access to register and/or request access to file 
registrations for committees.  They all start with the user noticing the campaign committee is not on their existing list
and indicating that they want to add a committee.  Having done that there are three possible cases: 

* A campaign finance user wants to register a new Campaign with the PDC
* A campaign finance user wants to be able to submit C3, C4 or registrations for a committee that has 
  been registered by another user. 
* A campaign finance user wants to be able to file amendments to a registration that has already been filed with 
  the PDC prior to the go-live, and they know the filer ID and password for the existing registration.

# Registering a New Campaign

Having logged into SAWS, the user sees a list of campaigns they file for.  

* The user notices that the committee/year that they are interested is not in the list. 
* They are asked if the Campaign has ever registered with the PDC in a prior year
* If they choose yes, they are prompted to search for the old campaign
* Access is granted to only the current years committee, and filings for that campaign are immediately. 

# Filing access for already registered campaigns

The primary difference in this use case is that we believe that a committee exists.  That is determined by the fact that 
the user identified the committee as one that already exists within the year that they are filing for. In this case the 
user should be able to request access to the existing committee. 

* The requesting user requests access to the committee in question, and we record that the have "requested" access in the 
role table. 
* Either another user with full filing access, or a PDC staff member can "grant" their requested access by accessing the original 
committee record, viewing those who have "requested" access and granting them access. 

We could decide to build a way for PDC staff to find the request either by pdc userid, saws email ro committee.   

# Filing access for legacy campaigns

In this case we can develop custom functionality to "validate" the access of a legacy filing record.  If the user 
identified the committee as one that already exists within the year that they are filing for, the user should be able 
to indicate the filer ID, user name and password of the existing filer record and verify that they have access. If
they do so successfully we can update the committee authorization for the filer.  