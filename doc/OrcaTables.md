#Data Model

## Data Diagrams

- [Contributions](contributions.png)
- [Expenditures](expenditures.png)


## Accounts

- **trankeygen_id**: Serialized primary key
- **pid**: Points to trankeygen ID of the parent record 
- **acctnum**: Chart of accounts account number for transaction 
- **contid**: Points to trankeygen_id of contact record for this account
- **code**: Code used for expense category (deprecated)
- **description**: Description for account
- **style**
- **total**
- **memo**
- **date**


## Transactions
**Most** tables use ids generated by the TRANKEYGEN table, which also has the ability to express parent
child relationsips between transactions using ID and PID fields in this table. For example, there is an
entry in TRANSKEYGEN for every entry in the Contact table, Receipts table, the Expenditure Event table and so on.



## Contacts
Contacts contain contact information for both expenditures, contributions and any
other entity that ORCA tracks.

**ID** - The unique ID for the contact, often called CONTID.

**TYPE** - Indicates the type of the contact with the following values:

- ACT - Not sure... probably abandoned no code references found
- CAN - Candidate contact, holds contact information for the candidate
- CPL - Couple, used for indicating married couples with joint couples
- GRP - Group contacts, groups of other contacts that you can look at total giving across
- HDN - Hidden contacts, used for chart of accounts and expense categorization
- IND - Individual or business contact, used in both expenditures and contributions


**NAME** -  Full name of the contact

**Address data** Expressed as STREET, CITY, STATE, ZIP< PHONE, EMAIL

**Employer data** Expressed as OCCUPATION, EMPLOYERNAME, EMPLOYERSTREET, EMPLOYERCITY< EMPLOYERSTATE, EMPLOYERZIP

**MEMO** Descriptive text information

**PAGG, GAGG** - Primary and general aggregate totals for the contact.

### COUPLECONTACTS
A samll table used to express the relationship between married couples for the purposes of sharing household campaign
limits.  CONTACT1_ID and CONTACT2_ID represent the connection between entities.

## Receipts - The generic transaction detail table
Although you might think that this table holds receipts, it is used to store several different types of transactions
details. For example, it holds the details for expenditure events, that is the itemization of expenditures.

**ID** Primary key referenced to TRANKEYGEN

**TYPE** Indicates the type of record for the receipt:

- Monetary contribution = 1
- Personal Funds = 2
- Monetary Loan = 3
- Fundraiser Contribution = 4
- Auction Item Donor = 7
- Auction Item Buyer = 8
- Loan Endorsement = 9
- Monetary Couple Contribution (no longer used) = 10
- Monetary Group Contribution = 11
- Pledge = 12
- Debt Forgiven = 44
- Anonymous Contribution = 13
- Other Receipt = 14
- Bank Interest = 15
- Monetary Pledge = 17
- Refund from vendor = 18
- Correction Receipt = 23
- Correction Receipt Math error = 24
- Carry forward cash = 22
- Pledge Cancelled = 25
- Loan Payment = 26
- Refund Contribution = 27
- Expenditure = 28
- Debt Payment = 29
- Interest Expense = 30
- Credit Card Payment = 31
- Debt Interest = 32
- Correction Expense = 33
- Correction Expense Math Error
- Debt = 19
- Credit Card Debt = 21
- Debt Adjustment = 20
- In Kind Contribution = 6
- In Kind Loan = 5
- In Kind Payment = 16
- Correction In Kind = 35
- Auction = 36
- Auction Item = 37
- Low Cost Fundraiser = 38
- Loan Forgiven = 39
- Regular Fundraiser = 40
- Monetary Group Contribution Detail = 41
- Account Adjustment = 42
- Expenditure Event = 43 (believed to be unused 03/17/21)

**AGGTYPE** - Controls how the transaction is applied to contribution limits.  It is generally multiplied against the
amount during summation:

- 1 increase the  total contributions,
- -1 decrease total contributions
- 0 has no effect.

**NETTYPE** - A type that indicates how the amount adjusts total receipts or total expenditures, particularly in the C4.

- 0 Don't include
- 1 adjust Receipts
- 2 adjust expenditures
- 3 adjust both

**PID** - Parent transaction id. (e.g expenditure event the item was pointing to)

**GID** - Not sure -1 in most cases unsure if used.

**CID** - Trankeygen id of the crediting account of the transaction.

**DID** - Trankeygen id of the debiting account of the transaction.

**CONTID** - Id of the contact related to this transaction.

**AMOUNT** - Amount of transaction

**DATE** - Transaction Date

**ELEKEY** - Indicator of PRIM(0) or GEN(1) N(-1) relevance of the transaction

**ITEMIZED** - Indicator of whether the transaction was itemized.

**CHECKNO** - Check number

**DESCRIPTION** - Transaction Description

**MEMO** - Memo field for the transaction (long text)

**CARRYFORWARD** - 1 indicates that the transaction is to establish initial balance
and doesn't count towards year to date receipts etc?

## Expenditure Events

Note that this table only creates initial expenditures. Item

**ID** - Transaction ID for expenditure

**CONTID** - Contact info for the expense.

**BNKID** - Bank account ID that the funds go against.

**AMOUNT** - Total amount of expenditure.  Itemizations are stored in the receipts table.

**DATE** - Date of expenditure

**CHECKNO** - Check number

**ITEMIZED** - Flag indicating whether the expenses are itemized.

**MEMO** - Larger text description for the transaction. 



 

