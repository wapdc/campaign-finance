# Committee Registration

This JSON REST web service may be used to retrieve the  information associated provided when the committee or candidate
registered with the Washington State Public Disclosure Commission. 

## URL Request Format: 



## HTTP Status Codes Returned: 

@TODO: Add a table that fills out this information. 


## JSON Response
````json
{
  "committee": {
    "name": "string",
    "filer_id": "filer_id",
    "committee_type": "committee_type",
    "pac_type": "pac_type",
    "bonafide_type": "bonafide_type",
    "exempt": "string",
    "jurisdiction_id": "jurisdiction_id",
    "reporting_type": "reporting_type",
    "url": "string",
    "acronym": "string",
    "memo": "string",
    "has_sponsor": "yes/no",
    "continuing": "boolean",
    "contact": {
      "email": "string",
      "address": "string",
      "city": "string",
      "state": "string",
      "postcode": "string",
      "phone": "string"
      },
    "bank": {
      "name": "string",
      "address": "string",
      "city": "string",
      "state": "string",
      "postcode": "string"
      },
    "books_contact": {
      "email": "string"
      },
    "officers": {
        "role": "role",
        "email": "string",
        "name": "string",
        "title": "string",
        "ministerial": "boolean",
        "treasurer": "boolean",
        "address": "string",
        "city": "string",
        "postcode": "string"
        },
    "proposals": {
        "ballot_number": "ballot_number",
        "proposal_type": "proposal_type",
        "proposal_issue": "string",
        "jurisdiction_id": "jurisdiction_id",
        "jurisdiction": "jurisdiction",
        "election_code": "election_code",
        "stance": "stance"
        },
    "affiliations_declared": "yes/no",
    "affiliations": {
        "affiliate_name": "string"
      }
    },
  "local_filing": {
    "seec": {
      "required": "boolean",
      "books_available": "books_available"
      }
    }
}

````

## Field definitions

Worth noting, the term "committee" includes candidates, which are technically candidate committees. In front-end user interfaces, the text often distinguishes candidates and committees to help users make correct choices. On the backend, they are committees with a different pac_type.

Legal requirements that underly most of these fields can be found in [RCW 42.17A.205](https://app.leg.wa.gov/rcw/default.aspx?cite=42.17A.205) and [WAC 390-16](https://apps.leg.wa.gov/wac/default.aspx?cite=390-16). City of Seattle filing requirements are found in [SMC 2.04.160](https://library.municode.com/wa/seattle/codes/municipal_code?nodeId=TIT2EL_CH2.04ELCACO_SUBCHAPTER_IIICADI_2.04.160POCOFISTOR).

### Committee General Fields

These fields live on the committee root.

* **name**: Committee name.
* **filer_id**: Legacy filer ID with the Public Disclosure Commission.
* **committee_type**: CA, CO. Candidate or political committee.
* **pac_type**: Candidate, surplus, pac, bonafide, caucus.
* **bonafide_type**: State, county, district. For bonafide parties only.
* **exempt**: 1, 0. Bonafide parties usually have two committees: one for exempt expenditures and one for non-exempt.
* **reporting_type**: Full, mini.
* **url**: Web address.
* **acronym**: Optional shortened name of committee.
* **memo**: Internal notes.
* **has_sponsor**: Yes, no. Is this a sponsored committee?
* **continuing**: 1, 0. Is this a continuing committee?
* **affiliations_declared**: Has the committee declared any affiliations?
* **start_year**: Year committee started.
* **end_year**: Year committee ends.
* **election_code**: General = 2020, Primary = 2020P, Feb Special = 2020S2, April Special = 2020S4 for example. If targetting a specific election.
* **jurisdiction_id**: Jurisdiction ID, if targeting a specific jurisdiction.

### Contact

Contact information for the committee.

* **email**
* **address**
* **city**
* **state**
* **postcode**
* **phone**

### Bank

The name and address of the committee bank or, if no bank account exists, a bank the committee intends to open an account at if it receives contributions.

* **name**
* **address**
* **city**
* **state**
* **postcode**

### Books_Contact

Committee books must be available for scheduled public inspection in the ten days prior to the election.

* **email**: The email address to contact to schedule inspection of committee books.

### Officers (Array)

Officers include any person who alone or in conjunction with other persons makes, directs, or authorizes contribution, expenditure, strategic or policy decisions on behalf of the committee.

* **role**: Officer.
* **title**: Officer's job title.
* **ministerial**: Is this officer a ministerial treasurer?
* **treasurer**: Is this officer a treasurer?
* **name**
* **email**
* **phone**
* **address**
* **city**
* **state**
* **postcode**
* **country**

### Candidacy

*For candidate committees only.* For what position is the candidate running?

* **person**: Candidate name.
* **ballot_name**: Ballot name, if different.
* **email**: Candidate email address.
* **office**: Governor, state senator, county council member, for example.
* **location**: State of Washington, City of Seattle, Spokane County, for example.
* **party**: Democrat, Republican, Libertarian, for example.

### Proposals (Array)

*For non-candidates only.* Proposals include initiatives, referendum, levies, bonds and recall campaigns.

* **proposal_type**: Initiative, levy, recall. (Initiative encompasses referendum and levy encompasses bonds.)
* **ballot_number**: The assigned ballot number for an initiative, levy or other ballot proposition, if it exists.
* **proposal_issue**: Description of proposal, if the ballot number is not known.
* **election_code**: General = 2020, Primary = 2020P, Feb Special = 2020S2, April Special = 2020S4 for example. 
* **jurisdiction_id**: Jurisdiction ID, if known.
* **jurisdiction**: Jurisdiction name, if the jurisdiction ID is not known.
* **stance**: For, against.

### Affiliations (Array)

The names and addresses of all related or affiliated committees or other persons, and the nature of the relationship or affiliation.

* **affiliate_name**: Name of related or affiliated committee or person.

### Local_Filing

Seattle has additional registration requirements.

* **seec**: Container for Seattle Ethics and Elections Commission filing requirements.
* **required**: Committee is required to file with this local regulator.
* **books_available**: Address and hours when campaign books will be open for public inspection.

