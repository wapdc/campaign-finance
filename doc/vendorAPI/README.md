# Campaign finance third-party submission API

The Public Disclosure Commission (PDC) provides an application programming interface (API) for the submission of 
campaign finance reports by third parties. Third parties are usually independent software vendors who provide campaign 
management solutions. This API supports:

* Retrieving a list of valid occupations.
* Retrieving a list of valid expenditure codes with accompanying metadata.
* Retrieving a list of contributor types and type codes.
* Retrieving information about a campaign.
* Preview and transmission of report (C3), Cash receipts and monetary contributions.
* Preview and transmission of report (C4), Summary, full report receipts and expenditures (C4).

---
## Signing up to use the third-party API

After reviewing the documentation, please email to pdc@pdc.wa.gov, indicating that you would like to use the campaign 
finance, third-party API. We will provision a test account for you in our test/demo system so that you can begin 
development and testing your application. 

You will receive authentication information for test committees. These committees exist only in the test/demo system.

The PDC does not certify or approve vendors. It is the responsibility of the vendor to ensure that their submissions are 
correct.
---
## Endpoint URLs

There are two distinct endpoints, one for testing and another for production. These operate against different databases, 
so you can safely submit reports to the test URL without impacting the production data or submitting a report. The URLs
only differ by the hostname so pay careful attention during testing. These are the base URL. In all examples in this 
document, the URL is the testing URL.

### Testing
`https://demo-apollo-d8.pantheonsite.io`

### Production
`https://apollo.pdc.wa.gov`

---
## Formatting of examples in this document

Examples of using the API are provided as curl commands that can be run in any shell supporting curl. In cases when a
filer authentication token is required, you must supply a valid token.

Placeholder values are identified with angular brackets such as `<user-supplied-value>`. Do not include the "<" or ">" 
in the actual value.

---
## Authentication

The API requires authentication to send reports and to retrieve committee information. Authentication is performed using
a bearer token. Vendors will receive an authentication token for each test committee. The token identifies the 
campaign/committee and is the only identifier necessary to send a report. For production use, filers can generate a 
token for each campaign/committee they file for. They can also revoke and generate additional tokens.

For example:
```shell
curl -H "Accept: application/json" -H "Authorization: Bearer <authentication-token>" 'https://demo-apollo-d8.pantheonsite.io/public/service/campaign-finance/committee-info' 
```

---
## Informational API methods

These methods provide information for a specific committee or general information that applies to any 
campaign/committee. They are provided for convenience to third parties for use in their own applications. The data may 
be cached for later use but keep in mind that the information may change. We recommend refreshing the data at least once
every 24 hours so that you will have the latest information.

---
### GET /public/service/campaign-finance/contributor-types
Contact records must have a contributor type specified. The contributor type is a one character code that must be sent
as the `contributor_type` property of the contact object when sending report data. The contributor types API provides a 
list of all valid values for `contributor_type` along with a textual label for each. The `type_code` property is for 
internal use and should be ignored.

Example request:
```shell
curl -H "Accept: application/json" 'https://demo-apollo-d8.pantheonsite.io/public/service/campaign-finance/contributor-types'
```

Example response:
```json
[
  { "type_code": "IND", "label": "Individuals", "contributor_type": "I" },
  { "type_code": "BUS", "label": "Business", "contributor_type": "B" },
  { "...": "..." }
]
```

---
### GET /public/service/campaign-finance/occupations
Contact records may require an occupation property depending on the type of the contact and if the contact is associated
with any contributions. Determining if sending occupation information is required for a contact is a compliance issue 
not a technical issue. The occupations API provides a list of all valid values for occupation in the "label" property
and an "employed" property. Occupations that have `"employed": false` would not typically have an employer address so 
this field can be used to determine if the filer should provide an employer address. For example, the occupation 
"RETIRED" does not need an address provided but "ACCOUNTANT" does.

Example request:
```shell
curl -H "Accept: application/json" 'https://demo-apollo-d8.pantheonsite.io/public/service/campaign-finance/occupations'
```

Example response:
```json
[
  { "label": "ACADEMIC ADVISOR", "employed": true },
  { "label": "NOT EMPLOYED", "employed": false },
  { "...": "..." }
]
```

---
### GET /public/service/campaign-finance/expense-categories
Expenditure reporting requires both a category code and a description for each expenditure. The expense-categories 
API provides a list of all valid expense category codes along with helpful metadata.

* **label** is the human-readable meaning of the code and can be displayed to a user to select the correct category.
* **code** is the value to provide in the `category` property of the expense record when sending the report.
* **default_description** when provided can generally be sent as the `description` for the expenditure without any 
additional detail or modification. These default descriptions are provided as a convenience, but it is ultimately up to
the filer to determine if the description is sufficient to meet the requirements under their particular circumstances. 
When this property is not provided, the description is entirely up to the filer to determine.
* **help_text** is intended as a helpful aid to determine what the category code means and what additional information 
the filer might be required to include in the description of an expenditure.
* **cat_lookup** can be used to determine which category codes make sense in a particular context. Any categories with a
`"cat_lookup": "surplus"` should only be used when reporting an expenditure from a surplus funds account.

Using the example response below, the third party software could ask the user to select the type of expenditure, 
displaying the choices of "Filing Fees" and "Robocalls". 

If the user selects "Filing Fees", you can provide the default
description of the expenditure as "Filing Fees". The resulting "expense" record in the submission would contain 
`"category": "FEE", "description": "Filing Fees"`

If the user selects  "Robocalls", you can display the help text "Provide number of calls made and period covered." to 
help them determine what details they may need to include in the description of the expenditure. The resulting "expense" 
record in the submission would contain `"category": "B-ROBO", "description": "<user-provided-description>"`

Example request:
```shell
curl -H "Accept: application/json" 'https://demo-apollo-d8.pantheonsite.io/public/service/campaign-finance/expense-categories'
```

Example response:
```json
[
  {
    "code": "FEE", "label": "Filing fees", "cat_lookup": "e",
    "help_text": "", "default_description": "Filing fees"
  },
  {
    "code": "B-ROBO", "label": "Robocalls", "cat_lookup": "e",
    "help_text": "Provide number of calls made and period covered."
  },
  {
    "...": "..."
  }
]
```

---
### GET /public/service/campaign-finance/committee-info
The committee info API can be used to get information about a particular committee by using the committee authentication
token. This can be helpful for presenting information to the user, so they know that they have provided the correct 
token.

Example request:
```shell
curl -H "Accept: application/json" -H "Authorization: Bearer <user-supplied-token>" 'https://demo-apollo-d8.pantheonsite.io/public/service/campaign-finance/expense-categories'
```

Example response:
```json
{
  "committee": {
    "election_code": "2022",
    "start_year": 2022,
    "end_year": 2022,
    "name": "Reluctant Friends of Dave",
    "pac_type": "candidate",
    "contact": {
      "address": "100 Main St",
      "city": "Olympia",
      "state": "WA",
      "postcode": "98501",
      "phone": "5555551212",
      "email": "david.smith@example.com"
    },
    "officers": [
      {
        "email": "david.smith@example.com",
        "name": "David Smith",
        "title": "President",
        "address": "123 Officer St",
        "city": "Olympia",
        "state": "WA",
        "postcode": "98501",
        "country": "USA",
        "phone": "5555551212",
        "treasurer": true
      }
    ],
    "candidacy": {
      "person": "Alex Jones",
      "ballot_name": "Alex \"Mickey\" Jones",
      "email": "alex_jones@example.com",
      "office": "MAYOR",
      "jurisdiction": "CITY OF OLYMPIA"
    }
  }
}
```

---
## Reporting API methods

The reporting API is used to transmit reports to the PDC. 

The basic reporting workflow is that the third-party application sends the report data to the PDC using the reporting 
API. The PDC synchronously responds to the request with a JSON response containing a status code, any validation 
warnings and a URL where the filer can preview the report data. The report is not "submitted" at this point and the 
filer must use a web browser with the included URL to access the report in the PDC reporting system and certify and 
submit the report.

### Amending
When submitting a C3 report of deposit, if the filer has already submitted a report with the same `external_id` 
property, the new report will be considered an amendment of the previous report. This only applies to reports that have 
been certified by the campaign. Previous draft (uncertified) reports will simply be overwritten with the new draft.

When submitting a C4 summary report, if the filer has already submitted a report for the same reporting period, the new
report will be considered an amendment of the previous report. This only applies to reports that have been certified by
the campaign. Previous draft (uncertified) reports will simply be overwritten with the new draft.

### Submitting Reports
In the examples below, it is assumed that there is a file, "report_data.json" that contains a valid C3 or C4 report in
the proper JSON format. The format of the JSON is covered in the C3 and C4 specific sections of the documentation.

The reporting API supports both uncompressed and gzip compressed POST requests. It is highly recommended to compress 
with gzip when sending large reports. See the second "send" example for sending compressed data. 

---
#### POST /public/service/campaign-finance/draft-report/send

For the examples below, assume there is a file `report.json` that contains the properly structured and complete json. 
The structure and requirements for the file are covered in the C3 and C4 sections of the documentation. If there are
warnings or errors, an object will be passed back in the "messages" property.

Example request (uncompressed):
```shell
curl -X POST --data-binary @report.json \
-H "Content-Type: application/json" \
-H "Authorization: Bearer <user-supplied-token>" \
'https://demo-apollo-d8.pantheonsite.io/public/service/campaign-finance/draft-report/send'
```

Example response:
```json
{
  "success": true,
  "messages": {
    "contact.1.address.incomplete":"Warning: BOB SMITH requires address when aggregate contributions exceed $25.",
    "contact.1.employer.incomplete":"Warning: BOB SMITH requires occupation information when contributions exceed $100.",
    "contacts.1.over_limit":"BOB SMITH is over the contribution limit of $1,000.00"
  },
  "url": "https:\/\/demo-apollo-d8.pantheonsite.io\/public\/service\/campaign-finance\/report\/preview\/?jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2Njk3NjM1MDUsImV4cCI6MTY3MDk3MzEwNSwiZHJhZnRfaWQiOjExfQ.e5NJAGTlB0wNhRU_uyuDaSIl9qXwp5phko3Jyk_AwKo"
}
```

**The same submission using gzip compression.** 

This is recommended, especially if you are transmitting large reports. Be sure whatever HTTP client you are using, sets
the `Content-Encoding: gzip` header and sends a gzip compressed data stream, not a gzipped file.

Example request (gzip compressed):
```shell
cat report.json | gzip | curl -X POST --data-binary @- \
-H "Content-Encoding: gzip" \
-H "Content-Type: application/json" \
-H "Authorization: Bearer <user-supplied-token>" \
'https://demo-apollo-d8.pantheonsite.io/public/service/campaign-finance/draft-report/send'
```

---
#### POST /public/service/campaign-finance/draft-report/send/preview
This method is nearly identical to "send". The only difference in the two methods is that this method only generates a 
preview. The filer will not be able to certify and submit the report. We recommend just using the "send" method. The 
filer can preview the report and if they are not ready to certify and submit it, they can just delete it or re-send a 
new one which overwrites the older draft. All documentation for "send" also applies to "send/preview".

No example is provided. it is identical to `POST /public/service/campaign-finance/report/send` with `/preview` appended
to the URL.

## C3 and C4 sample submissions
Both C3 and C4 report formats include a version number, which indicates 
the  version of submission documentation employed. These files are stored in
separate folders named with reference to the version.

Sample report submission formats are provided in both JSON and YAML.  Although the data must be submitted using content 
type of JSON, YAML supports comments, so we include it as a mechanism to document the nature of the submission. 

**Version 1.0 sample submissions (current)**

- [c3_complete.json](v_1.0/c3_complete.json)
- [c3_complete.yml](v_1.0/c3_complete.yml)
- [c4_complete.json](v_1.0/c4_complete.json)
- [c4_complete.yml](v_1.0/c4_complete.yml)

**Version 1.1 sample submissions (future)**

These include changes to the C4 in reporting loan and debt data.  All loan receipts included 
on carry-forward loans as well as cash loans received or carried-forward in the reporting period.
All debt has been restructured to clearly identify balance, payments, and origination. In addition,
there have been changes to the 'sums' metadata. The field 'expenses_under_50' was renamed to 'non_itemized_cash_expenses'
due to thresholds changing, and have added a field 'non_itemized_credit_expenses' to report under threshold credit 
expenditures. Any changes have been clearly annotated with (V1.1) in the yml file.

Currently, other than the version number there are no changes to the C3 data format. 

- [c3_complete.json](v_1.1/c3_complete.json)
- [c3_complete.yml](v_1.1/c3_complete.yml)
- [c4_complete.json](v_1.1/c4_complete.json)
- [c4_complete.yml](v_1.1/c4_complete.yml)

---
## Testing
The test endpoint is rebuilt every day. This means that if you submit reports as part of your testing and then try to 
submit an amendment the following day, the original report from the previous day will not exist. Everything is cleared 
out each day. If you are trying to test using real customer data to diagnose a problem, keep in mind that the state of 
the test system is from the previous day, so it may not exactly replicate the production system. 


