# Filer experience following vendor submission

The Public Disclosure Commission (PDC) provides an application programming interface (API) for the submission of
campaign finance reports by third parties. This allows independent software vendors to provide campaign
management solutions to candidates and committees who are required to file reports with the PDC. This 
documents a procedural and visual end-to-end walk-through of the process for the filer, using a fictitious campaign
and sample data.

In the examples below, the cURL command line client is used to simulate the vendor making an API to the PDC submission
endpoint. The actual implementation may use any technology that supports REST/JSON. All URLs in the examples below are 
for the demo system. You can submit reports to the demo system using a real customer API token or your vendor specific
test tokens. Consult the API documentation for the production endpoint URL.

## JSON data is submitted to PDC endpoint

Assuming the data for a report is contained in the file report.json, this is a sample cURL request to submit gzipped 
json data to the PDC endpoint. The <user-supplied-token> is an API token specific to the campaign/committee. Vendors are
provide test tokens for use in the demo system.

```shell
cat report.json | gzip | curl -X POST --data-binary @report.json \
-H "Content-Encoding: gzip" \
-H "Content-Type: application/json" \
-H "Authorization: Bearer <user-supplied-token>" \
'https://demo-apollo-d8.pantheonsite.io/public/service/campaign-finance/draft-report/send'
```

The API call will return a JSON response similar to the sample below. The `success` property will be `true` or `false`
and indicates if the draft was accepted. The `messages` property will contain warnings to the filer such as providing
insufficient details or a possible violation for contribution limits. In the case of `success` being false, the 
`messages` property will contain any error messages. The `messages` property may also be null.

If `success` is true, the API will return a `url` property. You can direct the filer to open this URL in a browser to 
preview and certify/submit the report.

```json
{
  "success": true,
  "messages": {
    "contacts.1.over_limit": "BENNETT HENRY is over the contribution limit of $1,000.00"
  },
  "url": "https://apollod8.lndo.site/public/registrations/campaign-finance/draft-report/view?jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2NzI4NDkwNzYsImV4cCI6MTY3NDA1ODY3NiwiZHJhZnRfaWQiOjkwfQ.8dh_4Khsc0oP62eMxHpbeHS8LlD0LbWzJKgzc6sM0o0"
}
```

When opening that URL, the filer will be taken to a web page that contains a preview of the report. At this point the 
report is a draft only and has not been accepted by the PDC. The filer can print the draft and file the report. If 
This page will present button options to allow the filer `LOGIN TO FILE` or to `PRINT` the draft report. If the vendor 
chooses to use the preview only API call, the `LOGIN TO FILE` button is not displayed.

![draft-view](images/draft.png)

When the user selects `LOGIN TO FILE`, they will be asked to login to the PDC electronic filing system and then given a 
chance to certify and submit the report.

![certify](images/certify.png)

Once they certify, they will be given a confirmation number and have a chance to print the final version of the report.

![confirmation](images/confirmation.png)

The filer also has a listing of their draft and submitted reports available in the PDC's electronic filing system. If 
the vendor sends a report that cannot be accepted because of a problem such as malformed content or missing required 
information, the filer can see the error message in their list of reports. They can also use this list to come back at
a later time and certify draft reports tht were submitted earlier.

You can see in the image below, the file has one draft C4 that was accepted, is in a draft state and needs to be 
certified by the filer. They also have one C3 that has an error that must be corrected. They must contact the vendor for
help in getting the report data sent correctly. Also, the filer can delete draft reports without certifying them.

Draft reports are kept for 2 weeks. If the filer does not certify them within the 2 weeks, they are deleted 
automatically.

![listing](images/listing.png)