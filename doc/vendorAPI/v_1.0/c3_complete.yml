# This document is a YAML representation of the JSON submission format for submitting C3 reports of deposit to the PDC.
# Converting the document from YAML to JSON will provide an example of the JSON while the YAML representation allows
# for comments to explain the requirements and sections of the JSON. The actual submission format is JSON.

# When this document is updated, please regenerate the JSON file from the YAML using the yq command on *NIX:
# yq eval c3_complete.yml -o=json -P > c3_complete.json

# There is no formal specification for the maximum length of the various strings. All the fields should be large enough
# to handle any reasonable values.

# Where codes are used such as "S" for candidate contacts, the codes are case-sensitive.

# Dates must be provided as yyyy-mm-dd

# Transactions that are a contribution or are considered a contribution, such as a loan require an "election" property.
# The possible values are "P" (primary), "G" (general), and "N" (none). If the campaign is not a candidate campaign,
# such as a PAC, a ballot measure campaign, or is a candidate not subject to contribution limits, use "N". If the
# campaign is a candidate campaign subject to limits, use "P" or "G" except for candidates that also have a filing
# requirement with Seattle Ethics and Elections. For these candidates, always specify "N".

## - Annotated C3 example ----------------------------------------------------------------------------------------------

# The "metadata" property must be provided.
metadata:

  # The "vendor_name" property must be provided. This is the name of the third party company that is preparing the
  # submission.
  vendor_name: VENDOR NAME

  # The "vendor_contacts" property must have one or more email addresses that the Public Disclosure Commission (PDC) can
  # use to contact the vendor regarding technical issues.
  vendor_contacts:
    - contact2@example.com
    - contact1@example.com

  # The "notification_emails" property must have one or more email addresses. The notification emails will be used to
  # send a confirmation notice directly to the filer at the point they log in to the PDC website and submit the report.
  notification_emails:
    - treasurer1@example.com

  # The "submission_version" is required. This value may change as the structure or required properties are changed.
  # Older submission versions may be deprecated over time and eventually rejected. This documentation describes 1.0.
  submission_version: 1.0

# The "report" property must be provided.
report:

  # For a C3 report of deposit, the report_type is "c3".
  report_type: c3

  # This value must be unique to the campaign for a given deposit. When submitting an amendment, the external_id value
  # must be the same as the external_id of the original report. This is how the report is identified as an amendment of
  # the previous report. The external_id can be any string up to 64 characters.
  external_id: edd8a1d8-c89b-4d68-9b58-2dbd51c25c42

  # the period_start and period_end must both be provided and must be the same for the C3 report of deposit. The value
  # is the date of the deposit.
  period_start: '2022-01-08'
  period_end: '2022-01-08'

  # The sums property must be provided.
  sums:
    # The "anonymous" property must be provided. When there are no anonymous contributions for a deposit, set the
    # values of "amount" and "date" to null. Set "aggregate" to the total aggregate for the campaign as of the date of
    # the deposit or 0 if there are no anonymous contributions as of the date of the deposit.
    anonymous:
      amount: 4.32
      aggregate: 9.76
      date: '2022-01-03'
    # The "small" property must be provided. When there are no small contributions to report for a deposit, set the
    # value of "amount" and "date" to null and the count to 0. If there is more than one small contribution, set the
    # date to null, otherwise provide the date.
    small:
      amount: 6.54
      count: 2
      date:
    # The candidate's "personal" funds property is only provided for candidate campaigns. If the report is for a
    # committee or if the candidate has not contributed any personal funds as of the date of the deposit, this property
    # can be omitted. The "amount" is the aggregate of personal funds contributed on this deposit or 0. The date is the
    # date the candidate contributed or null if count <> 1. The "count" is the count for this deposit. Set "aggregate"
    # to the total aggregate for the campaign as of the date of the deposit.
    personal:
      amount: 300
      date: '2022-01-02'
      count: 1
      aggregate: 300

  # Transactions are reported by using contact reference. For each transaction included in the report, there should be a
  # corresponding contact record that is referenced by "contact_key". If an entity is referenced in more than one
  # transaction, those transactions should reference a single contact record. The "contact_key" property can be any
  # string up to 64 characters. The same contact_key should be used for every report for a given campaign as an
  # identifier of the contact entity but does not need to be unique across campaigns.

  # All contact sub-properties must be provided. For any value that is not being reported, set the value to the
  # empty string. The "aggregate_general" and "aggregate_primary" values should always be provided. Set the value to 0
  # when there are no aggregate contributions. When the campaign does not have separate primary/general contribution
  # limits, provide the aggregate total as "aggregate_general" and set "aggregate_primary" to 0.

  # It is the responsibility of the campaign to determine when values of properties such as address and employer are
  # required to be reported as something other than the empty string.

  # If there are no transactions on the report that reference contacts, the "contacts" property can be omitted.
  contacts:
    - contact_key: C52
      aggregate_general: 300
      aggregate_primary: 0
      contributor_type: S
      name: MARY HAMILTON
      address: 123 Ham Way
      city: Hamsville
      state: WA
      postcode: '98504'
      employer:
        name: ''
        city: ''
        state: ''
        occupation: ''
    - contact_key: C53
      aggregate_general: 300
      aggregate_primary: 350
      contributor_type: I
      name: RANGER RON P MR III
      address: 123 Ron St SE
      city: Ronsville
      state: WA
      postcode: '98545'
      employer:
        name: Rons Wares
        city: Rondondo
        state: WA
        occupation: ACCOUNT SUPERVISOR
    - contact_key: C58
      aggregate_general: 551
      aggregate_primary: 698
      contributor_type: I
      name: PICKLES PAUL
      address: 985 Cucumber Ln
      city: Gerkinville
      state: WA
      postcode: '98501'
      employer:
        name: Dillco
        city: Leafington
        state: WA
        occupation: FARMER
    - contact_key: C93
      aggregate_general: 23
      aggregate_primary: 0
      contributor_type: I
      name: BILL BUYER
      address: ''
      city: Gerkinville
      state: WA
      postcode: '98501'
      employer:
        name: ''
        city: ''
        state: ''
        occupation: ''
    - contact_key: C94
      aggregate_general: 0
      aggregate_primary: 115
      contributor_type: I
      name: BARBARELLA BUYER
      address: 123 Barbed Ave
      city: Gerkinville
      state: WA
      postcode: '98501'
      employer:
        name: Barbie Inc
        city: Dollingston
        state: WA
        occupation: ACCOUNT EXECUTIVE
    - contact_key: C95
      aggregate_general: 0
      aggregate_primary: 25
      contributor_type: I
      name: DANIELSON DONOR
      address: ''
      city: Gerkinville
      state: WA
      postcode: '98501'
      employer:
        name: ''
        city: ''
        state: ''
        occupation: ''
    - contact_key: C96
      aggregate_general: 22
      aggregate_primary: 0
      contributor_type: I
      name: DAVIDSON DONOR
      address: ''
      city: Gerkinville
      state: WA
      postcode: '98501'
      employer:
        name: ''
        city: ''
        state: ''
        occupation: ''
    - contact_key: C2
      aggregate_general: 0
      aggregate_primary: 0
      contributor_type: I
      name: Bank Accounts
      address: ''
      city: ''
      state: ''
      postcode: ''
      employer:
        name: ''
        city: ''
        state: ''
        occupation: ''
    - contact_key: C55
      aggregate_general: 0
      aggregate_primary: 0
      contributor_type: B
      name: WA ASSN OF GUTTER FITTERS
      address: 457 Drain Ave
      city: Pouring
      state: WA
      postcode: '98545'
      employer:
        name: ''
        city: ''
        state: ''
        occupation: ''
    - contact_key: C106
      aggregate_general: 0
      aggregate_primary: 0
      contributor_type: I
      name: MAIN BANK
      address: 123 Main St
      city: Olympia
      state: WA
      postcode: '98504'
      employer:
        name: ''
        city: ''
        state: ''
        occupation: ''
    - contact_key: C120
      aggregate_general: 0
      aggregate_primary: 0
      contributor_type: I
      name: PROCEEDS FROM LOW COST FUNDRAISER
      address: ''
      city: ''
      state: ''
      postcode: ''
      employer:
        name: ''
        city: ''
        state: ''
        occupation: ''

  # Report all miscellaneous funds under the "misc" property. All properties require a value. For contributions of the
  # candidate's personal funds, include the additional property "personal_funds" with the value "true". Otherwise, this
  # property may be omitted. Each "contact_key" should reference an item in the "contacts" property. If there are no
  # miscellaneous transactions, the "misc" property can be omitted.
  misc:
    - date: '2022-01-02'
      amount: 300
      aggregate: 300
      personal_funds: true
      contact_key: C52
    - contact_key: C106
      amount: 45
      date: '2022-01-02'
      description: Credit for opening an account
    - contact_key: C2
      amount: 1.23
      date: '2022-01-04'
      description: Bank Interest
    - contact_key: C55
      amount: 1
      date: '2022-01-06'
      description: Got a $1 rebate

  # Report all monetary contributions under the "contributions" property. All sub-properties must be provided. If there
  # are no itemized contributions to report, the "contributions" property can be omitted.
  contributions:
    - amount: 120
      contact_key: C58
      date: '2022-01-01'
      election: P
    - amount: 5
      contact_key: C58
      date: '2022-01-04'
      election: P
    - amount: 51.21
      contact_key: C58
      date: '2022-01-04'
      election: G
    - amount: 30.5
      contact_key: C120
      date: '2022-01-03'
      election: N

  # Report all auctions under the "auctions" property. All sub-properties must be provided. The "number" of the auction
  # item should be sequential, starting at 1. The order is arbitrary. If there are no auctions to report, the "auctions"
  # property can be omitted.
  #
  # The market_value is the fair market value of the item donated. The sale_amount is the actual amount the item sold
  # for. The "donate" property is the donor of the auction item and the amount attributed to them as a contributor. The
  # "buy" property is the buyer of the auction item and the amount of the contribution attributed to them.
  #
  # The amounts for each property can be derived from two simple rules:
  # -- If market_value > sale_amount, donate.amount = sale_amount and buy.amount = 0
  # -- If sale_amount >= market_value, donate.amount = market_value and buy.amount = sale_amount - market_value.
  auctions:
    - date: '2022-01-01'
      items:
        - description: lamp
          market_value: 25.95
          number: 1
          donate:
            contact_key: C95
            amount: 25.95
            election: P
          buy:
            contact_key: C94
            amount: 114.05
            election: P
          sale_amount: 140
        - description: Pillow
          market_value: 22
          number: 2
          donate:
            contact_key: C96
            amount: 20
            election: N
          buy:
            contact_key: C93
            amount: 0
            election: N
          sale_amount: 20
        - description: Flowers
          market_value: 50
          number: 3
          donate:
            contact_key: C96
            amount: 50
            election: N
          buy:
            contact_key: C93
            amount: 0
            election: N
          sale_amount: 50

  # Report all cash loans under the "loans" property. All sub-properties must be provided. If the loan does not have a
  # due date, set the "due_date" to null in the JSON. If there are no endorsers, set the "endorser" property to the
  # empty array. If there are no loans to report, the "loans" property can be omitted.
  loans:
    - contact_key: C58
      endorser:
        - liable_amount: 250
          contact_key: C58
          election: P
        - liable_amount: 250
          contact_key: C58
          election: G
        - liable_amount: 250
          contact_key: C53
          election: P
        - liable_amount: 250
          contact_key: C53
          election: G
      amount: 250
      date: '2022-01-01'
      due_date:
      election: P
      interest_rate: 1
      payment_schedule: As funds become available
    - contact_key: C58
      endorser: []
      amount: 250
      date: '2022-01-01'
      due_date: '2022-11-01'
      election: G
      interest_rate: 1
      payment_schedule: As funds become available

  # The attachments property is an array of objects where each object represents a separate attached block of text.
  # The objects in the array should have a single `content` property where the value is the block of text. If there are
  # no attached text blocks, the attachments property may be null or omitted.
  attachments:
    - content: "This is an attached block of text. It can contain any additional information
        the filer would like to provide. \n\nIt is plain text and does not preserve
        any formatting except line feeds as shown in this example. Note that in the
        JSON, line feeds are \\\\n"
    - content: "This is another attached block of text. There is no limit on the number of attachments."