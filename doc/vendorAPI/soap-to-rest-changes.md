# Notable differences in the REST based submission protocol

This document is intended to highlight notable changes for vendors who are familiar with the SOAP API. The SOAP API will 
not be supported after February 2023. This is not intended to be a complete guide. Refer to the main documentation for
details not covered here.

## General structure and properties

The JSON data for the REST API is structured similarly to the XML document used with the SOAP. For example, contacts are
listed in a section of contacts and then referenced by ID on transactions. In many cases the names of properties have 
been changed to make them more readable.

## Authentication

The REST API uses the same tokens that were used for SOAP requests. The token is no longer sent as a parameter, it must
be provided as a bearer token in the HTTP authorization header. For example:
```shell
"Authorization: Bearer xxxxxxxxxxxxxxxxxxx"
```

## The API does not support finalized submission of reports, only preview and transmit

The REST API supports "send" and "send/preview". When a report is sent with the "send" API, it is saved in a draft state
and the filer must go to the PDC website and login to certify the report and finalize the submission. The "send" and 
"send/preview" APIs both return a URL that can be provided to the filer so that they can preview the report. The 
difference in the two methods is that "send" produces a URL that filer can use to certify and submit the report and 
"send/preview" is preview only. For the filer to certify and submit the report, you must send it again using "send".

## Filing amendments

The API does not use an "amdno" parameter to determine if a report is an amendment.

For C4 reports, any report for the campaign that has a reporting period that intersects the period of an already filed 
report is automatically considered an amendment of the previous report.

For C3 reports, see the required "external_id" property below.

## Report (C3), Cash receipts and monetary contributions requires "external_id"

All C3 reports must include an external_id property. This value must be unique to the campaign for a given C3 report. 
When submitting an amendment, the external_id value must be the same as the external_id of the original report. This is 
how the report is identified as an amendment of the previous report. The external_id is any string up to 64 characters 
and the value is entirely up to the vendor to decide. 

## Candidate contact ID when filing for a candidate campaign

In the XML document, the contact information for the candidate was specified in the cover section of the XML document,
not in the regular list of contacts. It the JSON submission, the candidate contact is in the list of all the other 
contacts and identified by the `"contributor_type": "S"` property on the contact property.

## Information that should not be sent

The API does not expect or support sending information about the campaign/committee such as the committee name, 
treasurer name, or election year. This information is already available to the PDC based on the token provided in the 
API request and will be displayed to the filer when they preview and/or certify the submission.

## Contact type codes have changed
Contact types in the XML are specified with three-letter codes such as "PAC". In the JSON, they are all single character
codes. For example, "PAC" becomes "P". All the possible codes are available by using the 
`GET /public/service/campaign-finance/contributor-types` API.

## Tools to aid in the transition

The PDC has provided a special API endpoint to aid testing during the transition to REST/JSON. This endpoint will allow
you to send an XML document, and it returns the equivalent JSON data that you would submit using the new API. This 
should not be used for production purposes, it is only intended to help demonstrate the difference in the APIs and will
be disabled without notice.

It only provides the JSON equivalent submission and does not demonstrate authentication or other necessary aspects of 
the REST/JSON submission. Be sure to refer to the main documentation on how to send a production submission.

The URL for the translation API is `https://https://demo-apollo-d8.pantheonsite.io/public/service/report-translator`. It 
does not require authentication or do any validation. It is only intended to demonstrate the difference between the old 
XML document format and the new JSON submission. You may submit any valid XML that would have been valid for the XML 
document. Also note that the translation API does not handle a full SOAP request, it is a REST API that simply accepts 
the XML report that would have been in the xrfZipBase64 parameter of the submit method, not zipped or base64 encoded.

**Here's an example using the curl command line http client:**

```shell
curl -X POST https://demo-apollo-d8.pantheonsite.io/public/service/report-translator \-H "Content-Type: application/xml" -H "Accept: application/xml" -d '<?xml version="1.0"?>
  <c3>
  <cover>
  <contId>P79</contId>
  <committee>Reluctant Friends of Dave</committee>
  <addr city="Olympia" st="WA" str="711 Capitol Way S #206" zip="98504"/>
  <officeSought offs1="0"/>
  <electnDate>2020</electnDate>
  <depositDate>02/29/2020</depositDate>
  <treasurer fst="Dewey" lst="Decimal" mdl="D." phone="3  605551212"/>
  <anon amt="0.00" totalAmt="10.00"/>
  <persFunds date="02/17/2020" amt="888.88" totalAmt="2888.86"/>
  <smallAmt amt="6401.25" count="2845\n"/>
  </cover>
  <contr amt="126.25" contId="C4300233" date="02/24/2020" electn="N"/>
  <contr amt="700.25" contId="C1234" date="02/25/2020" electn="N"/>
  <cont type="IND" id="C4300233" gAgg="27.00" pAgg="0">
  <name lst="Doe" fst="Jane" pre="Ms." suf="" mid="A"/>
  <addr zip="98498" st="WA" city="TACOMA" str="123 MAIN ST"/>
  <employer st="" city="" name="" occ=""/>
  </cont>
  <cont type="PAC" id="C1234" gAgg="0" pAgg="0">
  <name full="Daves Not Here PAC"/>
  <addr zip="98504" st="WA" city="Olympia" str="PO Box  1234567"/>
  </cont>
  </c3>' -o ./json_output.json
```

**This is the resulting JSON equivalent**

```json
{
  "report": {
    "report_type": "c3",
    "sums": {
      "anonymous": {
        "amount": 0,
        "aggregate": 10,
        "date": null
      },
      "small": {
        "amount": 6401.25,
        "count": 2845,
        "date": null
      },
      "personal": {
        "amount": 888.88,
        "date": "2020-02-17",
        "count": 1,
        "aggregate": 2888.86
      }
    },
    "period_start": "2020-02-29",
    "period_end": "2020-02-29",
    "election_code": "2020",
    "contacts": [
      {
        "contact_key": "C4300233",
        "aggregate_general": 27,
        "aggregate_primary": 0,
        "contributor_type": "I",
        "name": "Doe Jane A Ms.",
        "address": "123 MAIN ST",
        "city": "TACOMA",
        "state": "WA",
        "postcode": "98498",
        "employer": {
          "name": "",
          "city": "",
          "state": "",
          "occupation": ""
        }
      },
      {
        "contact_key": "C1234",
        "aggregate_general": 0,
        "aggregate_primary": 0,
        "contributor_type": "C",
        "name": "Daves Not Here PAC",
        "address": "PO Box  1234567",
        "city": "Olympia",
        "state": "WA",
        "postcode": "98504",
        "employer": {
          "name": "",
          "city": "",
          "state": "",
          "occupation": ""
        }
      }
    ],
    "misc": [
      {
        "date": "2020-02-17",
        "amount": 888.88,
        "aggregate": 2888.86,
        "personal_funds": true,
        "contact_key": "P79"
      }
    ],
    "contributions": [
      {
        "amount": 126.25,
        "contact_key": "C4300233",
        "date": "2020-02-24",
        "election": "N"
      },
      {
        "amount": 700.25,
        "contact_key": "C1234",
        "date": "2020-02-25",
        "election": "N"
      }
    ]
  },
  "metadata": {
    "vendor_name": "VENDOR NAME",
    "vendor_contacts": [
      "contact1@noreply.com",
      "contact2@noreply.com"
    ],
    "notification_emails": [
      "treasurer1@noreply.com"
    ],
    "submission_version": "1.0"
  }
}
```