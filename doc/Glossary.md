
This glossary includes terms that are within the scope of the Campaign Finance project. For other relevant terms, see the [Core Glossary](https://gitlab.com/wapdc/core/blob/master/Glossary.md).

# Glossary of Terms: Campaign Finance

**Ballot Proposition** - A measure, initiative, recall or referendum proposed to be submitted to voters of a jurisdiction.

**Bonafide Political Party** - 1) The statewide governing body of a major or state-recognized minor political party or 2) a major-party county central committee or legislative district committee.

**Campaign** - A time-bound effort by a Committee to affect the outcome of a Candidacy, Ballot Proposition or Issue.

**Candidate** - A person who seeks nomination for election or election to a public office.

**Committee** - A candidate, ballot measure, recall, political, or continuing committee.

* **Continuing Committee** - A Committee of continuing existence not established in anticipation of any particular election campaign.

* **Single-Year Committee** - A Committee established in anticipation of a particular election campaign.

**Committee Authorization** - The granting of a Committee Role to a User.

**Committee Contact** - Name, address and other information for a Committee or a Person associated with a Committee.

**Committee Proposal** - (Currently in Core.) A Committee's stance on a Ballot Proposition or Issue.

**Committee Relation** - A connection between two Committees. Used to connect Continuing Committees year over year and also to connect related-but-different Committees.

**Committee Role** - The authority a User has on a Committee. *Owners* have full access to the Committee, including the ability to grant Committee Roles to other Users. *Filers* have the ability to file campaign finance reports including registrations.

**Contest** - A set of elections that determine the outcome of a Race. Usually primary and general election, but could be a single election.

**Dissolution** - The formal process by which a Committee closes.

**Elected Official** - Any person elected at a general or special election to any public office, and any person appointed to fill a vacancy in any such office. (Not coded yet.)

**Election** - The process by which a population chooses a Person to hold public Office.

* **General Election** - The first Tuesday after the first Monday of November.
* **Primary Election** - The first Tuesday of August. A procedure for winnowing candidates to a final list for the General Election.
* **Special Election** - Any Election that is not the Primary Election or General Election. 

**Election Year** - The year for which a Candidacy or Ballot Proposition intends to appear on the ballot.

**Filer** - A Committee, Lobbyist or Public Official that files with the Commission.

**Issue** - The subject-matter of a Committee Proposal or Ballot Proposition.

**Organization** - A container that groups multiple Committees into a single entity, e.g. the same candidate or a school levy committee over multiple years.

**Registration** - The process by which Committees notify the Commission of their existence.

**Registration Change** - An update to a Registration Submission.

**Registration Log** - The archive of Registration Submissions.

**Registration Submission** - A single filing to create or amend a Committee.

**ReportingPeriod** - (Currently in Core.) An election-based temporal duration which modifies default reporting deadlines for Committees. Types include 21-day pre-election, 7-day pre-election, and post-election.



