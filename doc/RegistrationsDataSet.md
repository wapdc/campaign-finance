# Campaigns and Committee Registrations DataSet
 
**Columns in this Dataset**

|Column Name|Description|Type|
| ----- | ----- | ----- | 
| id | This is the unique identifier that represents this registration. | Plain Text |
| committee_id | This is the unique identifier assigned to the committee. | Plain Text |
| candidate_id | This is a unique identifier assigned to the candidate. | Plain Text |
| filer_id | The unique id assigned to a candidate or political committee. The filer id is consistent across election years with the exception that an individual running for a second office in the same election year will receive a second filer id. There is no correlation between the two. For a candidate and single-election-year committee such as a ballot committee, the combination of filer_id and election_year uniquely identifies a campaign. | Plain Text |
| filer_type | This column designates if this is a candidate committee (CA) or a political committee (CO). | Plain Text |
| receipt_date | The postmark date of the relevant registration. | Date & Time |
| election_year | The year of the election for a candidate or single-year committee participating in a particular election. The calendar year for a continuing political committee. | Date Time |
| filer_name | The candidate name as reported on the form C1 candidate registration or the political committee name as reported on the C1pc registration at the time they submitted the registration. | Plain Text |
| committee_acronym	| This field value of the most recent registration for the committee. If a political committee is known by an abbreviation it is placed in this column. This acronym is optional. An example would be the WA Education Association, whose acronym is WEA. | Plain Text |
| committee_address	|  This field value of the most recent registration for the committee. The street address of the committee. For candidates, this is the address provided for the candidate committee, not necessarily the address of the candidate. | Plain Text |
| committee_city |  This field value of the most recent registration for the committee. The city of the committee. For candidates, this is the address provided for the candidate committee, not necessarily the address of the candidate. | Plain Text |
| committee_county |  This field value of the most recent registration for the committee. The county of the committee. For candidates, this is the address provided for the candidate committee, not necessarily the address of the candidate. | Plain Text |
| committee_state |  This field value of the most recent registration for the committee. The state of the committee. For candidates, this is the address provided for the candidate committee, not necessarily the address of the candidate. | Plain Text |
| committee_zip	|  This field value of the most recent registration for the committee. The zip code of the committee. For candidates, this is the address provided for the candidate committee, not necessarily the address of the candidate. | Plain Text |
| committee_email |  This field value of the most recent registration for the committee. The email address of the committee. For candidates, this is the email address provided for the candidate committee, not necessarily the email address of the candidate. | Plain Text |
| candidate_email | The candidate’s personal e-mail. This email may be the same as the candidate’s committee email. | Plain Text |
| candidate_committee_phone	| The committee phone number. In the case of a candidate, this is the phone number of the candidate, not the candidate's committee. | Plain Text |
| committee_fax | This column show the fax machine number of the committee if any. | Plain Text |
| office | The office sought by the candidate. | Plain Text |
| office_code | The numeric code that defines the office. For example the office code 31 defines the office of Mayor. Combined with the jurisdiction_code it defines a particular office such as Mayor of Olympia. | Plain Text |
| jurisdiction | The political jurisdiction associated with the office of a candidate. | Plain Text |
| jurisdiction_code	| This is a unique code representing a jurisdiction with a reporting requirement. | Plain Text |
| jurisdiction_county | The county associated with the jurisdiction of a candidate. Multi-county jurisdictions are reported as the primary county. This field will be empty when a candidate jurisdiction is statewide. | Plain Text |
| jurisdiction_type	| The type of jurisdiction this office represents: Statewide, Local, Judicial, etc. | Plain Text |
| committee_category | This column contains a category for a political committee (PAC) for example: Continuing Political Committee, Party Committee, committees Affiliated with a party, Single election year committee, School levy committee, Local ballot initiative, Caucus political committee, Statewide Initiative committee. | Plain Text |
| political_committee_type | This column designates the persuasion of the PAC; for example, Business, union, PAC, etc. | Plain Text |
| bonafide_committee | An X in this column means this is a bona fide party committee | Plain Text |
| bonafide_type	| The type of bonafide party committee, for example State Party, County Party, Leg Dist Party, party Associated committee. | Plain Text |
| position | The position associated with an office. This field typically applies to jurisdictions that have multiple positions or seats. This field does not apply to political committees. | Plain Text |
| party_code | Unique code that represents a candidates party. i.e. R represents Republican. | Plain Text |
| party	| The political party as declared by the candidate on their C1 registration form. Contains only parties recognized by Washington State law. | Plain Text |
| election_date	| This is the calendar month/day/year of the election where a candidate is elected to office or the date of the election for a ballot initiative committee. | Date & Time |
| exempt_nonexempt | This column designate whether a party committee is exempt from contribution limits or is non-exempt which means it is subject to limits when giving contributions. | Plain Text |
| ballot_committee | An X in this column designates the committee as a ballot committee. | Plain Text |
| ballot_number	| The ballot number assigned to a Statewide ballot Initiative. Local ballot initiative will probably not have a ballot number because each county labels their initiatives starting at 1. So it is potentially possible for there to be 39 proposition 1's. | Plain Text |
| for_or_against| Designates whether the committee supports or opposes the initiative. | Plain Text |
| other_pac	| An X in this column designates the committee as a Continuing Political Committee. | Plain Text |
| treasurer_first_name | The first name of the treasurer as reported on the C1pc registration. | Plain Text |
| treasurer_last_name | The last name of the treasurer as reported on the C1pc registration. | Plain Text |
| treasurer_address	| The address of the treasurer as reported on the C1pc registration. | Plain Text |
| treasurer_city | The city of the treasurer as reported on the C1pc registration. | Plain Text |
| treasurer_state | The state of the treasurer as reported on the C1pc registration. | Plain Text |
| treasurer_zip	| The zip code of the treasurer as reported on the C1pc registration. | Plain Text |
| treasurer_phone | The phone number of the treasurer as reported on the C1pc registration. | Plain Text |
| url | A link to a registration version of the registration report as it was filed to the PDC. | Web URL |

**Dataset Ids**

| Stage | Dataset Id |
| ----- | ----- |
| live | . |
| demo | iz23-7xxj |

**Brief Description**

This data set is an index of all PDC Campaigns and Committee Registrations starting with 2006. Each record contains a link to view the actual document or a link on how to request the document.

The primary purpose of this dataset is for data consumers to examine the registrations of campaigns and committees. Refer to other dataset to get actual dollar values for any of the reports referenced herewith.

For Continuing committees and Surplus committees the committee id is the same year after year. 

All other committees are single year committees and will have a new committee_id representing a new registration for a different election year.


This dataset is a best-effort by the PDC to provide a complete set of records as described herewith and may contain incomplete or incorrect information. The PDC provides access to the original reports for the purpose of record verification.

Descriptions attached to this dataset do not constitute legal definitions; please consult RCW 42.17A and WAC Title 390 for legal definitions and additional information regarding political finance disclosure requirements.

CONDITION OF RELEASE: This publication constitutes a list of individuals prepared by the Washington State Public Disclosure Commission and may not be used for commercial purposes. This list is provided on the condition and with the understanding that the persons receiving it agree to this statutorily imposed limitation on its use. See RCW 42.56.070(9) and AGO 1975 No. 15.

**Row Label**
- Document image

**Categories and Tags**
- Category: Politics
- Tags/keywords: political finance, elections, contributions, campaign, political committee, disclosure, registrations

**License & Attribution**
- Public License Type: Public Domain
- Data Provided by: Washington State Public Disclosure Commission
- Source Link: http://www.pdc.wa.gov

**Temporal**
- Period of Time: Last 10 years. See description
- Post Frequency: Daily

