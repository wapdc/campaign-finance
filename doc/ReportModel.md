# Report Model

The authoritative source is the report table with the whole submission including in the user_date column. The data
stored in campaign_finance_submission is not an official record and transitory in nature.

## Report Validations
Make sure the following conditions are true for all reports:

* Date filed is a valid date.
* Report type is either C3 or C4.
* The committee for the report has been verified.
* For non-surplus committees, the election year (aka reporting year) is between the committee start year and end year. Note
  that single election committees always have the same start and end year. Continuing committees need to not have been ended. 
* A fund (per-election campaign account) exists for the committee or a fund is created if the report is not an amendment. 
* For continuing committees, the deposit date or reporting period end date is not after the election year.

If any of the above conditions are not true, the report will be rejected. 

## Amendment Validations
If the report provides a repno indicating an amendment, make sure that:

* The original report exists.
* The committee for the original report matches the committee for the amending report.
* The fund for the original report matches the fund for the amending report.
* The report type (C3 or C4) of the original report matches the report type of the amending report.
* The original report has not already been amended (superseded).

If any of the above conditions are not true, the report will be rejected.

# Contribution Report (C3) Validations
For contribution deposit reports, provide a notice if any of the following tests fail:

* If aggregate is over the 'itemized_contributions' amount in the thresholds table, contributor address exists.
* If aggregate is over the 'report_occupation' amount in the thresholds table (doubled for couples), contributor occupation, employer and employer address exist.
* Sum lines for candidate's personal funds (1B), loans (1C), misc + auctions (1D) match the sum of their transactions.
* Corrected amount exists for all corrections.
* For loan transactions, loan date and due date exist.
* check for duplicates for sums
* make sure the sum amounts are valid (in what way?)
* Dates are valid and not more than 30 years in the future for filing date, report period start, report period end, and "MON/PARM1 DATE"
* Amount code must be P (primary) or G (general).
* Reject duplicate contribution reports (conditions for determining duplicate status will be based on staff input)

Duplicate reports will be rejected. Otherwise, the report will be accepted even if any of these conditions fail.

# C4 Validations
For expenditure reports, provide a notice if any of the following tests fail:

* A duplicate report exists it is already processed (non amendments) for the same dates
* No processed reports cover any of the period covered by this report. (Verify this is desirable.)
* Sum line for (schedule A) expenditures (c4.11) matches sum of those transactions.
* Sum line for (schedule A) deposits matches the sum of those transactions.
* Sum line for (schedule B1) in-kind contributions (C4.3 or c4.12) matches the sum of those transactions.
* make sure B3 for debts + L4 equals total liabilities. 
* Expenditure code is maximum one character.
* Description is maximum 90 characters.
* Misc receipt description is maximum 80 characters.
* Debt description is maximum 60 characters.
* Auction item description is maximum 50 characters.

# Generic Validations

* Remove special characters that got converted to junk.
* all dates are formatted
* report period start and end (potentially move that into xrf section)
* make sure money is actually money
* Candidate name is a maximum 45 characters. (How much longer on the form?)
* Name does not equal "?" or "0" and does not contain "REQUESTED," "UNLISTED" or "UNKNOWN".
* Address, city, zip, occupation, employer name, employer address, employer city and employer state do not equal "?" or
  "0" and do not contain "NOT AVAILABLE", "NOT APPLICABLE", "NONE ", "N/A", "REQUESTED", "UNKNOWN" or "UNLISTED".
* Zip code is maximum ten characters. (Verify this is desirable.)

