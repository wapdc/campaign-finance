#!/usr/bin/env bash
mkdir -p /usr/share/man/man1 /usr/share/man/man7
apt-get -y update && apt-get -y install \
  apt-transport-https \
  apt-utils gdebi
curl http://http.us.debian.org/debian/pool/main/o/openssl/libssl1.0.0_1.0.1t-1+deb8u8_amd64.deb >libssl1.0.2.deb
gdebi libssl.0.2.deb
apt-get -y install \
  gnupg \
  git \
  locales \
  locales-all \
  libzip-dev \
  libpq-dev \
  postgresql-client \
  unzip \
  zlib1g-dev
docker-php-ext-install pdo_pgsql zip
