#!/usr/bin/env bash
# determine environment
## make sure revision scripts are runnable:
chmod a+x db/postgres/revisions/*
set -e
export PATH=/app/bin:$PATH
# Set server name so that debugging works when running build scripts.
export PHP_IDE_CONFIG="serverName=campaign-finance"
## If no CI assume a lando container so environment comes from lando
if [[ -z "$CI_COMMIT_REF_NAME" ]] ; then
  . vendor/wapdc/core/ci/ci_build_environment.sh
## Otherwise we assume that we're building in the devops image and we can get the environment
## from that project
else
  . ci_build_environment.sh
fi
echo "Running composer"
composer config -g gitlab-token.gitlab.com "$COMPOSER_ACCESS_TOKEN"
echo "Composer install"
composer install --quiet --no-progress

## Execute the build script.
echo "Executing build script"
ci/campaign_finance_upgrade.php rds

if [[ "$STAGE" != "live" ]] ; then
  ## create test users for vendors
  bin/pdc_cf.php import-committees /ci/data
  $PSQL_CMD wapdc -f ci/change-links-to-s3-files-to-dev-on-nightly-clone.sql
fi