#!/usr/bin/env bash
set -e

function install_ui()
{
  cd $project_dir/ui/$1
  if [[ ! -d "node_modules" ]] ; then
    npm i
  else
    npm update
  fi
  npm run build
}

if [[ -d vendor/wapdc/campaign-finance ]] ; then
  project_dir=vendor/wapdc/campaign-finance
else
  project_dir=`pwd`
fi
install_ui campaign
install_ui campaign_registration
install_ui public

