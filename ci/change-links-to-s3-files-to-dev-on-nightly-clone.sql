UPDATE registration_attachment
SET file_service_url = REPLACE(file_service_url, 'https://campaign-finance.', 'https://campaign-finance-dev.')
WHERE file_service_url LIKE 'https://campaign-finance.%';

UPDATE registration_attachment
SET file_service_url = REPLACE(file_service_url, 'https://campaign-finance-private.', 'https://campaign-finance-private-dev.')
WHERE file_service_url LIKE 'https://campaign-finance-private.%';