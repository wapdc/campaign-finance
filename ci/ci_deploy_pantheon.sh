#!/usr/bin/env bash
set -e
start_dir=`pwd`;
# Include pantheon build commands from dev ops
if [[ -f "/app/bin/ci_build_environment.sh" ]] ; then
. /app/bin/ci_build_environment.sh
fi
# Pull a copy of the apollo site
pull_site apollo
# This remove is required because the .git files are removed after the composer update.
cf_dir=vendor/wapdc/campaign-finance
composer clearcache
rm -rf "$cf_dir"
composer update --no-progress --with-all-dependencies wapdc/campaign-finance
rm -rf web/modules/custom/wapdc_registration
rm -rf web/modules/custom/campaign_finance_accounting
cp -r $cf_dir/drupal-8/. web/modules/custom/
#
# Build Vue UI
echo "Building Application"
cd "$cf_dir/ui/public"
cp -f ../.gitignore_production ./.gitignore
npm install -q --yes
npm run build
cd "../campaign"
cp -f ../.gitignore_production ./.gitignore
npm install -q --yes
npm run build
# Temporarily remove for go-live
cd "../ie"
cp -f ../.gitignore_production ./.gitignore
npm install -q --yes
npm run build
#
# Commit and push to master
commit_push