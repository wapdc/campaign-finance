#!/usr/bin/env php
<?php
require_once ('vendor/autoload.php');

// Instantiate a service
echo "Running upgrade scripts\n";
date_default_timezone_set('America/Los_Angeles');
$app = \WAPDC\CampaignFinance\CampaignFinance::service();

switch ($argv[1]) {
  case 'rds':
      $app->rdsUpgrade();
      break;
  case 'legacy':
      $app->legacyUpgrade();
      break;
  default:
      echo 'Unknown option!';
      exit(1);
}
